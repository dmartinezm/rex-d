(define (domain crossing)
  (:requirements :typing :strips :equality :probabilistic-effects :rewards)
  (:types xpos ypos)
  (:predicates 
    (robot-at ?X - xpos ?Y - ypos)
    (obstacle-at ?X - xpos ?Y - ypos)
  )
    (:action move-north
     :parameters ( )
     :precondition (exists (?X - xpos ?Y ?Y2 - ypos) (and (nORTH ?Y2 ?Y) (robot-at ?X ?Y2) (not (obstacle-at ?X ?Y2)) ) )
     :effect (and  (robot-at ?X ?Y) )
    )
    
    (:action move-south
     :parameters ( )
     :precondition (exists (?X - xpos ?Y ?Y2 - ypos) (and (nORTH ?Y ?Y2) (robot-at ?X ?Y2) (not (obstacle-at ?X ?Y2) )))
     :effect (and  (robot-at ?X ?Y) )
    )
    
    (:action move-west
     :parameters ( )
     :precondition (exists (?X ?X2 - xpos ?Y - ypos) (and (wEST ?X2 ?X) (robot-at ?X2 ?Y) (not (obstacle-at ?X2 ?Y) )))
     :effect (and  (robot-at ?X ?Y) )
    )
    
    (:action move-east
     :parameters ( )
     :precondition (exists (?X ?X2 - xpos ?Y - ypos) (and (wEST ?X ?X2) (robot-at ?X2 ?Y) (not (obstacle-at ?X2 ?Y) )))
     :effect (and  (robot-at ?X ?Y) )
    )
    
    (:action noaction
     :parameters ( )
     :precondition (exists (?X - xpos ?Y - ypos) (and (not (mIN-YPOS ?Y)) (not (mAX-YPOS ?Y)) (mAX-XPOS ?X)))
     :effect (probabilistic 0.3 (obstacle-at ?X ?Y) )
    )
    
    (:action noaction
     :parameters ( )
     :precondition (exists (?X ?X2 - xpos ?Y - ypos) (and (wEST ?X2 ?X) (obstacle-at ?X2 ?Y)))
     :effect (and (obstacle-at ?X ?Y) )
    )
)
