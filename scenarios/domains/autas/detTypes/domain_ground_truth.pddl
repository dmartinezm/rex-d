(define (domain autas)
  (:requirements :typing :strips :equality :probabilistic-effects :rewards)
  (:types squarePeg roundPeg squareHole roundHole)
  (:predicates (clear ?squarePeg)
               (clear ?roundPeg)
               (free ?squareHole)
               (free ?roundHole)
               (pegplaced ?squarePeg ?squareHole)
               (pegplaced ?roundPeg ?roundHole)
               (horizontal ?squarePeg)
               (horizontal ?roundPeg)
  )
    (:action placepeg
     :parameters (?peg - squarePeg ?hole - squareHole)
     :precondition (and (clear ?peg) (free ?hole) (not (separatorplacedSp)) (not (horizontal ?peg)) (not (pegplaced ?peg ?hole)))
     :effect (and  (pegplaced ?peg ?hole) (not (free ?hole)) )
    )
    
    (:action placepeg
     :parameters (?peg - roundPeg ?hole - roundHole)
     :precondition (and (clear ?peg) (free ?hole) (not (separatorplacedSp)) (not (horizontal ?peg)) (not (pegplaced ?peg ?hole)))
     :effect (and  (pegplaced ?peg ?hole) (not (free ?hole)) )
    )

    (:action placeshaft
     :parameters ( )
     :precondition (and (clearSh) (freeFbHsh) (not (shaftplacedSh)))
     :effect (and  (shaftplacedSh) (not (freeFbHsh)) )
    )

    (:action placeseparator
     :parameters ( )
     :precondition (and (clearSp) (not (free hole2)) (not (free hole1)) (not (separatorplacedSp)))
     :effect (and  (separatorplacedSp) )
    )

    (:action placependulum
     :parameters ( )
     :precondition (and (shaftplacedSh) (clearPn) (not (pendulumplacedPn)))
     :effect (and  (pendulumplacedPn) )
    )

    (:action screwpendulumhead
     :parameters ( )
     :precondition (and (pendulumplacedPn) (clearPh) (not (pendulumheadscrewedPh)))
     :effect (and  (pendulumheadscrewedPh) )
    )

    (:action placefaceplatefront
     :parameters ( )
     :precondition (and (separatorplacedSp) (pendulumheadscrewedPh) (clearFf) (not (free hole1)) (not (free hole2)) (not (free hole3)) (not (free hole4)) (not (faceplatefrontplacedFf)) (clear peg1) (clear peg2) (clear peg3) (clear peg4) (clearPn) (clearSp))
     :effect (and  (faceplatefrontplacedFf) (not (clearB1)) (not (clearB2)) (not (clearB3)) (not (clearB4)) (not (clearPn)) (not (clearSp)) )
    )

    (:action reverseseparator
     :parameters ( )
     :precondition (and (separatorplacedSp))
     :effect (and  (not (separatorplacedSp)) )
    )

    (:action reversependulum
     :parameters ( )
     :precondition (and (pendulumplacedPn))
     :effect (and  (not (pendulumplacedPn)) )
    )

    (:action reversependulumhead
     :parameters ( )
     :precondition (and (pendulumheadscrewedPh))
     :effect (and  (not (pendulumheadscrewedPh)) )
    )

    (:action reversefaceplatefront
     :parameters ( )
     :precondition (and (faceplatefrontplacedFf))
     :effect (and  (not (faceplatefrontplacedFf)) (clearB1) (clearB2) (clearB3) (clearB4) (clearPn) (clearSp) )
    )

    (:action repositionpeg1
     :parameters ( )
     :precondition (and (horizontalB1))
     :effect (and  (not (horizontalB1)) )
    )

    (:action repositionpeg2
     :parameters ( )
     :precondition (and (horizontalB2))
     :effect (and  (not (horizontalB2)) )
    )

    (:action repositionpeg3
     :parameters ( )
     :precondition (and (horizontalB3))
     :effect (and  (not (horizontalB3)) )
    )

    (:action repositionpeg4
     :parameters ( )
     :precondition (and (horizontalB4))
    :effect (and  (not (horizontalB4)) )
    )

    (:action placespecialplate
     :parameters ( )
     :precondition (and (separatorplacedSp) (not (pendulumplacedPn)) (not (pendulumheadscrewedPh)) (clearFf) (pegplacedB1FbHb1) (pegplacedB2FbHb2) (pegplacedB3FbHb3) (pegplacedB4FbHb4) (shaftplacedSh) (not (faceplatefrontplacedFf)) (not (specialplateplaced)) )
     :effect (and  (specialplateplaced) )
    )

)




