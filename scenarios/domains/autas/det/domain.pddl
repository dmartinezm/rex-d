(define (domain autas)
  (:requirements :typing :strips :equality :probabilistic-effects :rewards)
  (:types box)
  (:predicates (clearB1)
               (freeFbHb1)
               (separatorplacedSp)
  )

)
