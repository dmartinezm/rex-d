(define (domain autas)
  (:requirements :typing :strips :equality :probabilistic-effects :rewards)
  (:types box)
  (:predicates (clearB1)
               (freeFbHb1)
               (separatorplacedSp)
  )
    (:action placepeg1
     :parameters ( )
     :precondition (and (clearB1) (freeFbHb1) (not (separatorplacedSp)) (not (horizontalB1)) (not (pegplacedB1FbHb1)))
     :effect (and  (probabilistic 0.6 (and (pegplacedB1FbHb1) (not (freeFbHb1)) )))
    )

    (:action placepeg2
     :parameters ( )
     :precondition (and (clearB2) (freeFbHb2) (not (separatorplacedSp)) (not (horizontalB2)) (not (pegplacedB2FbHb2)))
     :effect (and  (probabilistic 0.6 (and (pegplacedB2FbHb2) (not (freeFbHb2)) )))
    )

    (:action placepeg3
     :parameters ( )
     :precondition (and (clearB3) (freeFbHb3) (not (horizontalB3)) (not (pegplacedB3FbHb3)))
     :effect (and  (probabilistic 0.6 (and (pegplacedB3FbHb3) (not (freeFbHb3)) )))
    )

    (:action placepeg4
     :parameters ( )
     :precondition (and (clearB4) (freeFbHb4) (not (horizontalB4)) (not (pegplacedB4FbHb4)))
     :effect (and  (probabilistic 0.6 (and (pegplacedB4FbHb4) (not (freeFbHb4)) )))
    )

    (:action placeshaft
     :parameters ( )
     :precondition (and (clearSh) (freeFbHsh) (not (shaftplacedSh)))
     :effect (and  (probabilistic 0.6 (and (shaftplacedSh) (not (freeFbHsh)) )))
    )

    (:action placeseparator
     :parameters ( )
     :precondition (and (clearSp) (pegplacedB1FbHb1) (pegplacedB2FbHb2) (not (separatorplacedSp)))
     :effect (and  (probabilistic 0.6 (and (separatorplacedSp) )))
    )

    (:action placependulum
     :parameters ( )
     :precondition (and (shaftplacedSh) (clearPn) (not (pendulumplacedPn)))
     :effect (and  (probabilistic 0.6 (and (pendulumplacedPn) )))
    )

    (:action screwpendulumhead
     :parameters ( )
     :precondition (and (pendulumplacedPn) (clearPh) (not (pendulumheadscrewedPh)))
     :effect (and  (probabilistic 0.6 (and (pendulumheadscrewedPh) )))
    )

    (:action placefaceplatefront
     :parameters ( )
     :precondition (and (separatorplacedSp) (pendulumheadscrewedPh) (clearFf) (pegplacedB1FbHb1) (pegplacedB2FbHb2) (pegplacedB3FbHb3) (pegplacedB4FbHb4) (not (faceplatefrontplacedFf)) (clearB1) (clearB2) (clearB3) (clearB4) (clearPn) (clearSp))
     :effect (and  (probabilistic 0.6 (and (faceplatefrontplacedFf) (not (clearB1)) (not (clearB2)) (not (clearB3)) (not (clearB4)) (not (clearPn)) (not (clearSp)) )))
    )

    (:action reverseseparator
     :parameters ( )
     :precondition (and (separatorplacedSp))
     :effect (and  (probabilistic 0.6 (and (not (separatorplacedSp)) )))
    )

    (:action reversependulum
     :parameters ( )
     :precondition (and (pendulumplacedPn))
     :effect (and  (probabilistic 0.6 (and (not (pendulumplacedPn)) )))
    )

    (:action reversependulumhead
     :parameters ( )
     :precondition (and (pendulumheadscrewedPh))
     :effect (and  (probabilistic 0.6 (and (not (pendulumheadscrewedPh)) )))
    )

    (:action reversefaceplatefront
     :parameters ( )
     :precondition (and (faceplatefrontplacedFf))
     :effect (and  (probabilistic 0.6 (and (not (faceplatefrontplacedFf)) (clearB1) (clearB2) (clearB3) (clearB4) (clearPn) (clearSp) )))
    )

    (:action repositionpeg1
     :parameters ( )
     :precondition (and (horizontalB1))
     :effect (and  (probabilistic 0.6 (and (not (horizontalB1)) )))
    )

    (:action repositionpeg2
     :parameters ( )
     :precondition (and (horizontalB2))
     :effect (and  (probabilistic 0.6 (and (not (horizontalB2)) )))
    )

    (:action repositionpeg3
     :parameters ( )
     :precondition (and (horizontalB3))
     :effect (and  (probabilistic 0.6 (and (not (horizontalB3)) )))
    )

    (:action repositionpeg4
     :parameters ( )
     :precondition (and (horizontalB4))
    :effect (and  (probabilistic 0.6 (and (not (horizontalB4)) )))
    )

    (:action placespecialplate
     :parameters ( )
     :precondition (and (separatorplacedSp) (not (pendulumplacedPn)) (not (pendulumheadscrewedPh)) (clearFf) (pegplacedB1FbHb1) (pegplacedB2FbHb2) (pegplacedB3FbHb3) (pegplacedB4FbHb4) (shaftplacedSh) (not (faceplatefrontplacedFf)) (not (specialplateplaced)) )
     :effect (and  (probabilistic 0.6 (and (specialplateplaced) )))
    )

)




