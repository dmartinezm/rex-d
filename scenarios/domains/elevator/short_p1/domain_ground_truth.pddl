(define (domain elevator)
  (:requirements :typing :strips :equality :probabilistic-effects :rewards)
  (:types elevator floor)
  (:predicates (elevatorClosed ?e - elevator)
               (elevatorAtFloor ?e - elevator ?f - floor)
               (elevatorDirUp ?e - elevator)
               (ADJACENTUP ?f1 ?f2 - floor)
               (personInElevatorGoingUp ?e - elevator)
               (personInElevatorGoingDown ?e - elevator)
               (BOTTOMFLOOR ?f - floor)
               (TOPFLOOR ?f - floor)
               )
  
  (:action moveCurrentDir
    :parameters (?e - elevator ?f - floor ?next - floor)
    :precondition (and (elevatorClosed ?e) (elevatorAtFloor ?e ?f) (elevatorDirUp ?e) (ADJACENTUP ?f ?next) (not (elevatorAtFloor ?e ?next)) )
    :effect (and (not (elevatorAtFloor ?e ?f)) (elevatorAtFloor ?e ?next) )
    )
    
  (:action moveCurrentDir
    :parameters (?e - elevator ?f - floor ?next - floor)
    :precondition (and (elevatorClosed ?e) (not (elevatorAtFloor ?e ?f)) (elevatorDirUp ?e) (ADJACENTUP ?f ?next)  )
    :effect (and  )
    )
    
  (:action moveCurrentDir
    :parameters (?e - elevator ?f - floor ?next - floor)
    :precondition (and (not (elevatorClosed ?e)) (elevatorAtFloor ?e ?f) (elevatorDirUp ?e) (ADJACENTUP ?f ?next)  )
    :effect (and  )
    )
    
  (:action moveCurrentDir
    :parameters (?e - elevator ?f - floor ?next - floor)
    :precondition (and (elevatorClosed ?e) (elevatorAtFloor ?e ?f) (not (elevatorDirUp ?e)) (ADJACENTUP ?f ?next)  )
    :effect (and  )
    )
    
   (:action moveCurrentDir
    :parameters (?e - elevator ?f - floor ?next - floor)
    :precondition (and (elevatorClosed ?e) (elevatorAtFloor ?e ?f) (elevatorDirUp ?e) (not (ADJACENTUP ?f ?next))  )
    :effect (and )
    )
    
    
  
  (:action moveCurrentDir
    :parameters (?e - elevator ?f - floor ?next - floor)
    :precondition (and (elevatorClosed ?e) (elevatorAtFloor ?e ?f) (not (elevatorDirUp ?e)) (ADJACENTUP ?next ?f) )
    :effect (and (not (elevatorAtFloor ?e ?f)) (elevatorAtFloor ?e ?next) )
    )
    
  (:action moveCurrentDir
    :parameters (?e - elevator ?f - floor ?next - floor)
    :precondition (and (elevatorClosed ?e) (elevatorAtFloor ?e ?f) (elevatorDirUp ?e) (ADJACENTUP ?next ?f) )
    :effect (and  )
    )
    
  (:action moveCurrentDir
    :parameters (?e - elevator ?f - floor ?next - floor)
    :precondition (and (elevatorClosed ?e) (not (elevatorAtFloor ?e ?f)) (not (elevatorDirUp ?e)) (ADJACENTUP ?next ?f)  )
    :effect (and  )
    )
    
  (:action moveCurrentDir
    :parameters (?e - elevator ?f - floor ?next - floor)
    :precondition (and (not (elevatorClosed ?e)) (elevatorAtFloor ?e ?f) (not (elevatorDirUp ?e)) (ADJACENTUP ?next ?f)  )
    :effect (and  )
    )
    
  (:action moveCurrentDir
    :parameters (?e - elevator ?f - floor ?next - floor)
    :precondition (and (elevatorClosed ?e) (elevatorAtFloor ?e ?f) (not (elevatorDirUp ?e)) (not (ADJACENTUP ?next ?f))  )
    :effect (and  )
    )
    
  
  (:action openDoorGoingUp
    :parameters (?e - elevator)
    :precondition (and (elevatorClosed ?e) (not (personInElevatorGoingDown ?e)) )
    :effect (and (not (elevatorClosed ?e)) (elevatorDirUp ?e) )
    )
  
  (:action openDoorGoingUp
    :parameters (?e - elevator)
    :precondition (and (elevatorClosed ?e) (not (BOTTOMFLOOR ?f)) )
    :effect (and (not (elevatorClosed ?e)) (elevatorDirUp ?e) )
    )
  
  (:action openDoorGoingUp
    :parameters (?e - elevator)
    :precondition (and (elevatorClosed ?e) (personInElevatorGoingDown ?e) (BOTTOMFLOOR ?f) )
    :effect (and (not (elevatorClosed ?e)) (elevatorDirUp ?e) (not (personInElevatorGoingDown ?e)) )
    )
    
    
  
  (:action openDoorGoingDown
    :parameters (?e - elevator)
    :precondition (and (elevatorClosed ?e) (not (personInElevatorGoingUp ?e)) )
    :effect (and (not (elevatorClosed ?e)) (not (elevatorDirUp ?e)) )
    )
    
  (:action openDoorGoingDown
    :parameters (?e - elevator)
    :precondition (and (elevatorClosed ?e) (not (TOPFLOOR ?f)) )
    :effect (and (not (elevatorClosed ?e)) (not (elevatorDirUp ?e)) )
    )
    
  (:action openDoorGoingDown
    :parameters (?e - elevator)
    :precondition (and (elevatorClosed ?e) (personInElevatorGoingUp ?e) (TOPFLOOR ?f) )
    :effect (and (not (elevatorClosed ?e)) (not (elevatorDirUp ?e)) (not (personInElevatorGoingUp ?e)) )
    )
    
    
    
  (:action closeDoor
    :parameters (?e - elevator)
    :precondition (and (not (elevatorClosed ?e)) )
    :effect (and (elevatorClosed ?e) )
    )
)
    