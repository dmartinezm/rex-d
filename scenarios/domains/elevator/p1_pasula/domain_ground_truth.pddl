(define (domain elevator)
  (:requirements :typing :strips :equality :probabilistic-effects :rewards)
  (:types elevator floor)
  (:predicates (elevatorClosed ?e - elevator)
               (elevatorAtFloor ?e - elevator ?f - floor)
               (elevatorDirUp ?e - elevator)
               (aDJACENTUP ?f1 ?f2 - floor)
               (personWaitingUp ?f - floor)
               (personWaitingDown ?f - floor)
               (personInElevatorGoingUp ?e - elevator)
               (personInElevatorGoingDown ?e - elevator)
               (bOTTOMFLOOR ?f - floor)
               (tOPFLOOR ?f - floor)
               )
  
  
  (:action moveCurrentDir
    :parameters (?e - elevator)
    :precondition (exists (?f ?next - floor) (and (elevatorClosed ?e) (elevatorDirUp ?e) (elevatorAtFloor ?e ?f) (aDJACENTUP ?f ?next) ) )
    :effect (and (not (elevatorAtFloor ?e ?f)) (elevatorAtFloor ?e ?next) )
    )
    
  (:action moveCurrentDir
    :parameters (?e - elevator)
    :precondition (exists (?f ?next - floor) (and (elevatorClosed ?e) (not (elevatorDirUp ?e)) (elevatorAtFloor ?e ?f) (aDJACENTUP ?next ?f) ) )
    :effect (and (not (elevatorAtFloor ?e ?f)) (elevatorAtFloor ?e ?next) )
    )
  
  (:action openDoorGoingUp
    :parameters (?e - elevator)
    :precondition (and (elevatorClosed ?e) )
    :effect (and (not (elevatorClosed ?e)) (elevatorDirUp ?e) )
    )
  
  (:action openDoorGoingDown
    :parameters (?e - elevator)
    :precondition (and (elevatorClosed ?e) )
    :effect (and (not (elevatorClosed ?e)) (not (elevatorDirUp ?e)) )
    )
  
    
  (:action closeDoor
    :parameters (?e - elevator)
    :precondition (and (not (elevatorClosed ?e)) )
    :effect (and (elevatorClosed ?e) )
    )
  
  
  
  (:action noaction
    :parameters ( )
    :precondition (exists (?e - elevator ?f - floor) (and (personInElevatorGoingDown ?e) (elevatorAtFloor ?e ?f) (bOTTOMFLOOR ?f) ) )
    :effect (and (not (personInElevatorGoingDown ?e) ) )
    )
    
  (:action noaction
    :parameters ( )
    :precondition (exists (?e - elevator ?f - floor) (and (personInElevatorGoingUp ?e) (elevatorAtFloor ?e ?f) (tOPFLOOR ?f) ) )
    :effect (and (not (personInElevatorGoingUp ?e) ) )
    )
    
  (:action noaction
    :parameters ( )
    :precondition (exists (?e - elevator ?f - floor) (and (elevatorAtFloor ?e ?f) (elevatorDirUp ?e) (not (elevatorClosed ?e)) (personWaitingUp ?f) ) )
    :effect (and (personInElevatorGoingUp ?e) (not (personWaitingUp ?f)) )
    )
    
  (:action noaction
    :parameters ( )
    :precondition (exists (?f - floor) (and (not (tOPFLOOR ?f)) (not (personWaitingUp ?f)) ) )
    :effect (probabilistic 0.14635538 (personWaitingUp ?f) )
    )
    
  (:action noaction
    :parameters ( )
    :precondition (exists (?e - elevator ?f - floor) (and (elevatorAtFloor ?e ?f) (not (elevatorDirUp ?e)) (not (elevatorClosed ?e)) (personWaitingDown ?f) ) )
    :effect (and (personInElevatorGoingDown ?e) (not (personWaitingDown ?f)) )
    )
    
  (:action noaction
    :parameters ( )
    :precondition (exists (?f - floor) (and (not (bOTTOMFLOOR ?f)) (not (personWaitingDown ?f)) ) )
    :effect (probabilistic 0.14635538 (personWaitingDown ?f) )
    )
    
)
    