(define (domain elevator)
  (:requirements :typing :strips :equality :probabilistic-effects :rewards)
  (:types elevator floor)
  (:predicates (elevatorClosed ?e - elevator)
               (elevatorAtFloor ?e - elevator ?f - floor)
               (elevatorDirUp ?e - elevator)
               (aDJACENTUP ?f1 ?f2 - floor)
               (personWaitingUp ?f - floor)
               (personWaitingDown ?f - floor)
               (personInElevatorGoingUp ?e - elevator)
               (personInElevatorGoingDown ?e - elevator)
               (bOTTOMFLOOR ?f - floor)
               (tOPFLOOR ?f - floor)
               )

)
