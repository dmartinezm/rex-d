(define (problem dangerous)
  (:domain exblock)
  (:objects b1 b3 b4 b6 b7 - box)
  (:init (emptyhand) (onTable b4) (onTable b6) (on b3 b4) (on b1 b3) (on b7 b1) (clear b6) (clear b7) (noDetonated b1) (noDestroyed b1) (noDetonated b3) (noDestroyed b3) (noDetonated b4) (noDestroyed b4) (noDetonated b6) (noDestroyed b6) (noDetonated b7) (noDestroyed b7) (noDestroyedTable))
  (:goal (finished) ) 
  (:goal-reward 0.05) 
  (:metric maximize (reward))
)

