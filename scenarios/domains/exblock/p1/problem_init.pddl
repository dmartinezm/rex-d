(define (problem p1)
  (:domain exblock)
  (:objects b1 b2 b3 b4 b5 - box)
  (:init (emptyhand) (on b1 b4) (onTable b2) (on b3 b2) (on b4 b5) (onTable b5) (clear b1) (clear b3) (noDetonated b1) (noDestroyed b1) (noDetonated b2) (noDestroyed b2) (noDetonated b3) (noDestroyed b3) (noDetonated b4) (noDestroyed b4) (noDetonated b5) (noDestroyed b5) (noDestroyedTable) )
  (:goal (and (on b2 b4) (onTable b4)) ) 
  (:goal-reward 1) 
  (:metric maximize (reward))
)
