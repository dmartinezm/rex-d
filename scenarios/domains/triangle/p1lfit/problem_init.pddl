(define (problem triangle)
  (:domain p1)
  (:objects l11 l12 l13 l21 l22 l31 - location)
  (:constants (ROAD l11 l12) (ROAD l12 l13) (ROAD l11 l21) (ROAD l12 l22) (ROAD l21 l12) (ROAD l22 l13) (ROAD l21 l31) (ROAD l31 l22) (GOAL l13) )
  (:init (vehicleAt l11) (ROAD l11 l12) (ROAD l12 l13) (ROAD l11 l21) (ROAD l12 l22) (ROAD l21 l12) (ROAD l22 l13) (spareIn l21) (spareIn l22) (ROAD l21 l31) (ROAD l31 l22) (spareIn l31) (notFlattire) (not(hasspare)) (GOAL l13) )
  (:goal (and (vehicleAt l13) ) )
  (:goal-reward 1) 
  (:metric maximize (reward))
)
