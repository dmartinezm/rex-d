(define (domain triangle)
  (:requirements :typing :strips :equality :probabilistic-effects :rewards)
  (:types location)
  (:predicates (vehicleAt ?loc - location)
               (GOAL      ?loc - location)
               (spareIn   ?loc - location)
               (ROAD      ?from - location ?to - location)
               (notFlattire) 
               (hasspare))
  (:action moveCar
    :parameters (?from - location ?to - location)
    :precondition (and (vehicleAt ?from) (ROAD ?from ?to) (notFlattire))
    :effect (and (vehicleAt ?to) (not (vehicleAt ?from))
         (probabilistic 0.49 (not (notFlattire)))))
  (:action loadtire
    :parameters (?loc - location)
    :precondition (and (vehicleAt ?loc) (spareIn ?loc))
    :effect (and (hasspare) (not (spareIn ?loc))))
  (:action changetire
    :precondition (hasspare)
    :effect (and (not (hasspare)) (notFlattire)))

)
