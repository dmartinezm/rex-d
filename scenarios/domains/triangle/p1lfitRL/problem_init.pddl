(define (problem p1)
  (:domain triangle)
  (:objects l11 l12 l13 l21 l22 l31 - location)
  (:constants (ROAD l11 l12) (ROAD l12 l13) (ROAD l11 l21) (ROAD l12 l22) (ROAD l21 l12) (ROAD l22 l13) (ROAD l21 l31) (ROAD l31 l22) (GOAL l13) )
  (:init (vehicleAt l11) (ROAD l11 l12) (ROAD l12 l13) (ROAD l11 l21) (ROAD l12 l22) (ROAD l21 l12) (ROAD l22 l13) (spareIn l21) (spareIn l22) (ROAD l21 l31) (ROAD l31 l22) (spareIn l31) (notFlattire) (not (hasspare)) (GOAL l13) (not (goalRewardReceived)) )
  (:reward (if (^ (~ (goalRewardReceived)) (exists ( (?l : location) ) (^ (vehicleAt ?l) (GOAL ?l) )) ) then 100 else (if (goalRewardReceived) then 0 else (- 0 1))))
  (:metric maximize (reward))
)
