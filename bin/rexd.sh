#!/bin/bash

loop_continue=1
while [  $loop_continue -eq 1 ]; do
    bin_file_path="../bin/execution_scripts/make_experiment.py"
    if [ -e $bin_file_path ]
    then
        $bin_file_path $1 $2
    else
        ../$bin_file_path $1 $2
    fi
    if [ $? -ne 0 ]
    then
        loop_continue=0
    else
        sleep 1
        echo ""
        echo ""
        echo ""
        echo "NEW"
    fi
done
