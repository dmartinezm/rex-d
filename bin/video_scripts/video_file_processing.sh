#!/usr/bin/env bash

ffmpeg -i video_editing.avs -vcodec libx264 -preset ultrafast -pix_fmt yuv420p -crf 1 -threads 3 -bf 0 video.mp4
