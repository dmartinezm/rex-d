#!/usr/bin/env bash

if [ $# -ge 1 ]; then
        GOURCE_LOGS_FOLDER=$1
else
        GOURCE_LOGS_FOLDER="./"
fi

if [ $# -ge 2 ]; then
        SPEED_FACTOR=$2
else
        SPEED_FACTOR=1
fi
if [ $SPEED_FACTOR -eq 1 ]; then
    SPEED="--realtime"
else
    SPEED=$((60*60*24/${SPEED_FACTOR}))
    SPEED="--seconds-per-day ${SPEED}"
fi
SPEED="--realtime"

if [ $# -ge 3 ]; then
        ENABLE_CAPTIONS=$3
else
        ENABLE_CAPTIONS=0
fi

CAPTION_SIZE=40
CAPTION_DURATION=$(( 20 / $SPEED_FACTOR ))

USER_SCALE=1.0 # scale (default 1.0)
USER_SPEED=200 # units (default: 500)
USER_FRICTION=0.67 # seconds (default: 0.67)
USER_IDLE_TIME=10.0 # seconds (default: 3.0)

#RESOLUTION="-1280x1024"
RESOLUTION=""

HIDE_ITEMS="filenames,date,usernames"
USER_IMAGE_DIR="avatars/"

BLOOM_MULTIPLIER=1.0
BLOOM_INTENSITY=0.5

if [ $ENABLE_CAPTIONS -eq 1 ]; then
    CAPTIONS_COMMAND="--caption-file ${GOURCE_LOGS_FOLDER}/gource_caption.log --caption-duration ${CAPTION_DURATION} --caption-size ${CAPTION_SIZE}"
fi

# without proper libass support
gource $GOURCE_LOGS_FOLDER/gource.log $CAPTIONS_COMMAND $RESOLUTION -hide $HIDE_ITEMS -file-idle-time 0 --user-image-dir $USER_IMAGE_DIR --bloom-multiplier 1.0 --bloom-intensity 0.5 --highlight-dirs --highlight-users --user-scale $USER_SCALE --max-user-speed $USER_SPEED -user-idle-time $USER_IDLE_TIME --user-friction $USER_FRICTION $SPEED -o - | avconv -y -r 60 -f image2pipe -vcodec ppm -i - -vcodec libx264 -preset ultrafast -pix_fmt yuv420p -crf 1 -threads 3 -bf 0 gource.mp4
cp $GOURCE_LOGS_FOLDER/visualization.ass .
cp $GOURCE_LOGS_FOLDER/video_editing.avs .

# with proper libass support (Recommended)
#gource $GOURCE_LOGS_FOLDER/gource.log $CAPTIONS_COMMAND $RESOLUTION -hide $HIDE_ITEMS -file-idle-time 0 --user-image-dir $USER_IMAGE_DIR --bloom-multiplier 1.0 --bloom-intensity 0.5 --highlight-dirs --highlight-users --user-scale $USER_SCALE --max-user-speed $USER_SPEED -user-idle-time $USER_IDLE_TIME --user-friction $USER_FRICTION $SPEED -o - | avconv -y -r 60 -f image2pipe -vcodec ppm -i - -vf "ass=prueba.ass${GOURCE_LOGS_FOLDER}/visualization.ass" -vcodec libx264 -preset ultrafast -pix_fmt yuv420p -crf 1 -threads 3 -bf 0 gource.mp4
# cp $GOURCE_LOGS_FOLDER/video_editing.avs .

# avconv to get faster playback
#avconv -i gource.mp4 -vcodec libx264 -preset ultrafast -pix_fmt yuv420p -crf 1 -threads 3 -bf 0 -filter setpts=4.0*PTS -r 60 gource_fast.mp4

# echo "Finished. Creating mkv with embedded subtitles."
# mkvmerge -o gource.mkv gource.mp4 $GOURCE_LOGS_FOLDER/visualization.ass
