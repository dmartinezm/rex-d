#!/usr/bin/env bash

ffmpeg -i gource.mp4 -vf "ass=visualization.ass"  -vcodec libx264 -preset ultrafast -pix_fmt yuv420p -crf 1 -threads 3 -bf 0 video_realtime.mp4
