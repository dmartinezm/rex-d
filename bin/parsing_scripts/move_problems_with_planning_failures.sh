#!/usr/bin/env bash

domain_file_numbers=`ls | grep 'ppddl_domain_file_' | cut -d \_ -f 4 | cut -d \. -f 1`
problem_file_numbers=`ls | grep 'simulator_ppddl_current_problem_file_' | cut -d \_ -f 6 | cut -d \. -f 1`
domain_name=`cat rexd_config.cfg | grep "domain" | grep -v "domain_" | cut -d \= -f 2  | tr -d ' '`
problem_name=`cat rexd_config.cfg | grep "problem" | grep -v "problem_" | cut -d \= -f 2  | tr -d ' '`

test_problem_dir="result/${domain_name}/${problem_name}/"
mkdir -p "$test_problem_dir/results"
mkdir -p "$test_problem_dir/configs"
cp rexd_config.cfg "$test_problem_dir/configs"
cp template_ppddl $test_problem_dir



for X in $domain_file_numbers
do
    printf "."
    dest_dir="${test_problem_dir}/domain/${X}/"
    mkdir -p $dest_dir
    mv "ppddl_domain_file_${X}.pddl" "${dest_dir}/domain.pddl"
    cp "problem_init.pddl" "${dest_dir}/problem_current.pddl"
    cp "rexd_config.cfg" "${dest_dir}/"
    mv "result_domain_${X}.txt" "${dest_dir}/result.txt"
done

for X in $problem_file_numbers
do
    printf "*"
    dest_dir="${test_problem_dir}/problem/${X}/"
    mkdir -p $dest_dir
    cp "domain_ground_truth.pddl" "${dest_dir}/domain.pddl"
    mv "simulator_ppddl_current_problem_file_${X}.pddl" "${dest_dir}/problem_current.pddl"
    cp "rexd_config.cfg" "${dest_dir}/"
    mv "result_problem_${X}.txt" "${dest_dir}/result.txt"
done

echo "Everything has been moved to \"result\" folder. Move it to excuses folder and benchmark the folder."