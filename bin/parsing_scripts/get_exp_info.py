#!/usr/bin/env python

from __future__ import print_function
import sys
import os



class FeatureReader:
    def __init__(self, name, identifier, execution_number):
        self.name = name
        self.identifier = identifier
        self.execution_number = execution_number
        self.current_execution = []
        self.current_episode = 0
        self.num_resets = 0
        self.reset_name = 'reset'
        self.ignore_first_reset = True
        self.max_action_warning = False
        self.max_action_warning_execution_number = []
    
    def execute_reset(self):
        if self.current_episode == 100:
            # Save warning if reached 100 actions limit in any episode
            self.max_action_warning = True
            self.max_action_warning_execution_number.append(self.execution_number)
        self.current_execution.append(self.current_episode)
        self.current_episode = 0
        self.num_resets+=1
    
    def parse_line(self, line):
        if self.reset_name in line:
            if self.ignore_first_reset:
                self.ignore_first_reset = False
            else:
                self.execute_reset()
        else:
            if self.identifier in line:
                self.add_match(line)
    
    def write_results(self):
        # python3 : print('.', end="")
        first = True
        for result in self.current_execution:
            if first:
                first = False
            else:
                print(',', end="")
            print('%5.2f' % result, end="")
        print('; %' + str(self.execution_number))
    
    def print_warnings(self, configuration):
        if self.max_action_warning:
            for exec_num in self.max_action_warning_execution_number:
                print("WARNING! Reached 100 actions in a episode. Configuration:", configuration, "Execution number:", exec_num, file=sys.stderr)


class CountFeatureReader(FeatureReader):
    def add_match(self, line):
        self.current_episode+=1

class SumFeatureReader(FeatureReader):
    def add_match(self, line):
        self.current_episode+=float(line.split(':')[1])


class ConfigurationReader:
    def __init__(self, scenario, problem, configuration):
        self.reset_string = "reset"
        self.readers_count = {'actions' : 'Action', 
                               'teacher' : 'Teacher', 
                               'failed'  : 'Failed', 
                               'explore' : 'Explore', 
                               'noise'   : 'Noise', 
                               'dead'    : 'Dead', 
                               'dangerous': 'Confirmation request',
                               'teacherSouth': 'Teacher: move-south()',
                               'teacherNoop': 'Teacher: noop()'}
                               
        self.readers_sum = {'planning': 'planning', 
                            'learning': 'learning', 
                            'reward'  : 'Reward'}
        
        self.data_results = {}
        for name in self.readers_count:
            self.data_results[name] = []
        for name in self.readers_sum:
            self.data_results[name] = []
        
        self.scenario = scenario
        self.problem = problem
        self.configuration = configuration
        self.warning_already_printed = False
        
    
    
    def write_results(self):
        for name in self.data_results:
            # show warning if data size is not equal for all values in array
            data_results_size = 0
            try:
                data_results_size = self.data_results[name][0].num_resets
            except:
                pass
            
            #printing_name = name + '_' + self.scenario + '_' + self.problem + '_' + self.configuration
            printing_name = name + '_' + self.configuration
            printing_name_array = printing_name + '_m'
            print(printing_name_array + "=[")
            for result in self.data_results[name]:
                # Print warnings
                if (result.num_resets != data_results_size) and not self.warning_already_printed:
                    print("WARNING! Wrong number of episodes in", self.configuration, result.execution_number, ": ", result.num_resets, "!=", data_results_size, file=sys.stderr)
                result.print_warnings(self.configuration)
                
                result.write_results()
            print("];")
            
            print(printing_name + '_v = var(' + printing_name_array + ');')
            print(printing_name + '_E = std(' + printing_name_array + ');')
            print(printing_name + '   = sum(' + printing_name_array + ')/size(' + printing_name_array + ',1);')
            self.warning_already_printed = True
        
        # other
        print('success_' + self.configuration + ' = 1 - dead_' + self.configuration + ';')


    def parse_experiment(self, logging_file, execution):
        f = open(logging_file, 'r')
        
        readers = {}
        for name in self.readers_count:
            reader = CountFeatureReader(name, self.readers_count[name], execution)
            readers[name] =reader
        
        for name in self.readers_sum:
            reader = SumFeatureReader(name, self.readers_sum[name], execution)
            readers[name] =reader
        
        num_resets = 0
        for line in f:
            for name in readers:
                readers[name].parse_line(line)
        
        for name in self.data_results:
            readers[name].execute_reset()
            self.data_results[name].append(readers[name])

    
class MyExperiments:
    def __init__(self):
        a = 1
            
    def read_configuration(self, configuration_folder):
        configuration_reader = None
        executions = sorted(os.walk(configuration_folder).next()[1])
        for execution_dir in executions:
            logging_file = configuration_folder + '/' + execution_dir + '/' + 'intellact_logging.txt'
            if os.path.exists(logging_file):
                [aux, scenario_name, problem_name, execution_number] = execution_dir.split('_')
                if (configuration_reader == None):
                    configuration_reader = ConfigurationReader(scenario_name, problem_name, configuration_folder)
                configuration_reader.parse_experiment(logging_file, execution_number)
        if (configuration_reader != None):
            configuration_reader.write_results()
                
    def read_experiments(self):
        configurations = os.walk('.').next()[1]
        for configuration_folder in configurations:
            print
            print
            print('%' + configuration_folder)
            self.read_configuration(configuration_folder)


def main():
    my_experiments = MyExperiments()
    my_experiments.read_experiments()


if __name__ == "__main__":
    main()
