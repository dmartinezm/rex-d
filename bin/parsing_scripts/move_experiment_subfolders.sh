#!/usr/bin/env bash


# check which is the last_experiment
# TODO 1 if none
last_experiment=`ls | grep 'experiment_' | tail -n1 | cut -d \_ -f 4 | bc -l`
let 'last_experiment++'

# first level -> different folders with the same configuration
for top_folder in `ls`
do
    # folder results
    if [ -d "${top_folder}/results" ]; then
        #cd "${X}/results"
        for experiment_folder in `ls ${top_folder}/results/`
        do
            # update experiment number to last_experiment
            updated_experiment_folder=`echo ${experiment_folder} | cut -d \_ -f 1,2,3`
            updated_experiment_folder=`printf %s_%04d ${updated_experiment_folder} ${last_experiment}`
            
#             echo "${top_folder}/results/${experiment_folder} ./${updated_experiment_folder}"
            cp -r "${top_folder}/results/${experiment_folder}" "./${updated_experiment_folder}"
            let 'last_experiment++'
        done
    fi
done