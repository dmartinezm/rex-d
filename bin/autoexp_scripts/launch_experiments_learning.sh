#!/bin/bash

# tmp_file="/tmp/$(basename $0).$$.tmp"
# current_dir=`pwd`
# echo $tmp_file

# Launch konsole with multiple tabs
# echo "title: first;; command: ls;; workdir: ${current_dir}" >> $tmp_file
# konsole --noclose -e ls
# konsole --noclose --tabs-from-file $tmp_file

# Launch byobu with multiple tabs
# screen backend
# http://askubuntu.com/questions/213600/setting-default-byobu-screens
# mkdir $tmp_file
# BYOBU_CONFIG_DIR=$tmp_file
# tmp_file2="${tmp_file}/windows.experiments"
# echo "screen -t localhost bash" >> $tmp_file2
# echo "screen -t hola bash" >> $tmp_file2

if [ $# -lt 8 ]
then
    echo "Usage: launch_experiments.sh domain problem init_experiment end_experiment num_transitions_per_exp=(1|n) num_repetitions num_simulations num_configs"
    exit
fi

if [ $# -lt 3 ]
then
    init_experiment_number=0
else
    init_experiment_number=$3
fi

if [ $# -lt 4 ]
then
    end_experiment_number=1000
else
    end_experiment_number=$4
fi

num_transitions_per_exp=$5

num_repetitions=$6

num_simulations=$7

num_configs=$8

cd "exp_$1_$2"
current_dir=`pwd`

BYOBU_SESSION_NAME="rexd"

i=0
experiment_number=1
for X in `ls`
do
    echo "$X"
    if [ $num_transitions_per_exp -eq 1 ]
    then
        min_transitions=${experiment_number}
        max_transitions=${experiment_number}
    else
        min_transitions=1
        max_transitions=${num_transitions_per_exp}
    fi
    
    if [ $experiment_number -eq $init_experiment_number ]
    then
        cd "$X"
        ../../bin/execution_scripts/start_experiment.py $1 $2
        cd ..
        echo "bin/learner_bench.py ${min_transitions} ${max_transitions} ${num_repetitions} ${num_simulations} ${num_configs}"
        byobu new-session -d -s $BYOBU_SESSION_NAME -n "$X" "cd $X/current; bin/autoexp_scripts/aux_launch_bash_command.sh bin/learner_bench.py ${min_transitions} ${max_transitions} ${num_repetitions} ${num_simulations} ${num_configs}"
        let "i++"
    elif [ $experiment_number -gt $init_experiment_number ] && [ $experiment_number -le $end_experiment_number ]
    then
        cd "$X"
        ../../bin/execution_scripts/start_experiment.py $1 $2
        cd ..
        echo "bin/learner_bench.py ${min_transitions} ${max_transitions} ${num_repetitions} ${num_simulations} ${num_configs}"
        byobu new-window -t $BYOBU_SESSION_NAME:${i} -n "$X" "cd $X/current; bin/autoexp_scripts/aux_launch_bash_command.sh bin/learner_bench.py ${min_transitions} ${max_transitions} ${num_repetitions} ${num_simulations} ${num_configs}"
        let "i++"
    fi
    let "experiment_number++"
done
byobu new-window -t $BYOBU_SESSION_NAME:${i} -n "Aux" "bash"

byobu -2 attach-session -t $BYOBU_SESSION_NAME

