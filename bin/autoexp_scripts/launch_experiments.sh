#!/bin/bash

# tmp_file="/tmp/$(basename $0).$$.tmp"
# current_dir=`pwd`
# echo $tmp_file

# Launch konsole with multiple tabs
# echo "title: first;; command: ls;; workdir: ${current_dir}" >> $tmp_file
# konsole --noclose -e ls
# konsole --noclose --tabs-from-file $tmp_file

# Launch byobu with multiple tabs
# screen backend
# http://askubuntu.com/questions/213600/setting-default-byobu-screens
# mkdir $tmp_file
# BYOBU_CONFIG_DIR=$tmp_file
# tmp_file2="${tmp_file}/windows.experiments"
# echo "screen -t localhost bash" >> $tmp_file2
# echo "screen -t hola bash" >> $tmp_file2

if [ $# -lt 2 ]
then
    echo "Usage: launch_experiments.sh domain problem [init_experiment number_experiments]"
    exit
fi

if [ $# -lt 3 ]
then
    init_experiment_number=0
else
    init_experiment_number=$3
fi

if [ $# -lt 4 ]
then
    end_experiment_number=1000
else
    end_experiment_number=$4
fi

cd "exp_$1_$2"
current_dir=`pwd`

BYOBU_SESSION_NAME="rexd"

i=0
experiment_number=0
for X in `ls`
do
    echo "$X"
    if [ $experiment_number -eq $init_experiment_number ]
    then
        byobu new-session -d -s $BYOBU_SESSION_NAME -n "$X" "cd $X;  ../../bin/autoexp_scripts/aux_launch_bash_command.sh ../../bin/rexd.sh $1 $2"
        let "i++"
        byobu new-window -t $BYOBU_SESSION_NAME:${i} -n "$X" "cd $X; ../../bin/autoexp_scripts/aux_launch_bash_command.sh ../../bin/simulator.sh"
        let "i++"
        byobu new-window -t $BYOBU_SESSION_NAME:${i} -n "$X" "cd $X; ../../bin/autoexp_scripts/aux_launch_bash_command.sh ls results"
        let "i++"
    elif [ $experiment_number -gt $init_experiment_number ] && [ $experiment_number -le $end_experiment_number ]
    then
        byobu new-window -t $BYOBU_SESSION_NAME:${i} -n "$X" "cd $X; ../../bin/autoexp_scripts/aux_launch_bash_command.sh ../../bin/rexd.sh $1 $2"
        let "i++"
        byobu new-window -t $BYOBU_SESSION_NAME:${i} -n "$X" "cd $X; ../../bin/autoexp_scripts/aux_launch_bash_command.sh ../../bin/simulator.sh"
        let "i++"
        byobu new-window -t $BYOBU_SESSION_NAME:${i} -n "$X" "cd $X; ../../bin/autoexp_scripts/aux_launch_bash_command.sh ls results"
        let "i++"
    fi
    let "experiment_number++"
done
byobu new-window -t $BYOBU_SESSION_NAME:${i} -n "Aux" "bash"

# BYOBU_SESSION_NAME="rexd"
# byobu new-session -d -s $BYOBU_SESSION_NAME
# byobu new-window -t $BYOBU_SESSION_NAME:1 -n 'window-1-title' 'command-1'
# byobu new-window -t $BYOBU_SESSION_NAME:2 -n 'window-2-title' 'command-2'
# byobu select-window -t $BYOBU_SESSION_NAME:0
byobu -2 attach-session -t $BYOBU_SESSION_NAME

# BYOBU_WINDOWS=experiments byobu


