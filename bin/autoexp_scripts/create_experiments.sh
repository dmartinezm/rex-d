#!/bin/bash

if [ $# -lt 2 ]
then
    echo "Usage: create_experiments.sh domain problem"
    exit
fi

mkdir "exp_$1_$2"
cd "exp_$1_$2"


conf_folder="aaaa" # initialize
# read until exit is typed
while [ $conf_folder != "exit" ]
do
    echo "Write conf folder name (type exit to exit)"
    read conf_folder
    if [ $conf_folder != "exit" ]
    then
        mkdir $conf_folder
    fi
done



for X in `ls`
do
    echo "Creating folders for $X"
    mkdir -p ${X}/domains/$1
    mkdir ${X}/results
    cp -r ../scenarios/domains/$1/$2 ${X}/domains/$1/
    cp -r ../scenarios/generic/ ${X}/
done


# Modify files

# Launch terminator with multiple tabs and execute everything