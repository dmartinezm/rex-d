#!/usr/bin/env python

from __future__ import print_function

import os
import subprocess
import shutil
import sys
import shutil
import time
import re
try:
    from gi.repository import Notify
    DISABLE_LIBNOTIFY=False
except ImportError:
    DISABLE_LIBNOTIFY=True

learners = ['lfit',
            'pasula']

# update_progress() : Displays or updates a console progress bar
## Accepts a float between 0 and 1. Any int will be converted to a float.
## A value under 0 represents a 'halt'.
## A value at 1 or bigger represents 100%
def update_progress(progress):
    barLength = 10 # Modify this to change the length of the progress bar
    status = ""
    if isinstance(progress, int):
        progress = float(progress)
    if not isinstance(progress, float):
        progress = 0
        status = "error: progress var must be float\r\n"
    if progress < 0:
        progress = 0
        status = "Halt...\r\n"
    if progress >= 1:
        progress = 1
        status = "Done...\r\n"
    block = int(round(barLength*progress))
    text = "\rPercent: [{0}] {1}% {2}".format( "#"*block + "-"*(barLength-block), progress*100, status)
    sys.stdout.write(text)
    sys.stdout.flush()


def get_bin_path(current_dir):
    bin_path = ''
    if os.path.exists(current_dir + '/bin'):
        bin_path = current_dir + '/bin/'
    elif os.path.exists(current_dir + '/../bin'):
        bin_path = current_dir + '/../bin/'
    elif os.path.exists(current_dir + '/../../bin'):
        bin_path = current_dir + '/../../bin/'
    else:
        bin_path = current_dir + '/../../../bin'
    return os.path.realpath(bin_path)

def execute_command(command):
    res = subprocess.call(command, shell=True)
    if (res != 0):
        print("Unexpected error")
        if not DISABLE_LIBNOTIFY:
            Notify.init("REX-D")
            notify_msg = Notify.Notification.new("REX-D","Unexpected error executing " + command + "! ","dialog-error")
            notify_msg.show ()
        raise RuntimeError

def read_error_probability(differences_file):
    f = open(differences_file, 'r')
    for line in f:
        if "Differences are" in line:
            return float(line.split("are")[1])

class ConfigBenchmarker:
    def __init__(self, config_index):
        self.config_index = config_index
        self.config_name = "rexd_config.cfg" + str(config_index)
        if 'learner = pasula' in open(self.config_name).read():
            self.learner = 'pasula'
        elif 'learner = lfit' in open(self.config_name).read():
            self.learner = 'lfit'
        else:
            raise Exception('Unrecognized learner in ' + self.config_name)
        
        self.result_vector = ""
        self.result_vector_v = ""
        self.result_vector_E = ""
        self.result_vector_t = ""
        self.reset_iteration()
        
    def reset_iteration(self):
        self.average_prob = 0
        self.resulting_probs = []
        self.resulting_times = []
        
    def write_iteration_results(self, out_file, name_current_iteration):
        name_current_iteration_complete = self.learner + name_current_iteration + '_' + str(self.config_index)
        
        out_file.write(name_current_iteration_complete + '_m' + '=[')
        for result_prob in self.resulting_probs[:-1]:
            out_file.write('%1.4f,' % result_prob)
        out_file.write('%1.4f];\n' % self.resulting_probs[-1])
        
    def write_iteration_times(self, out_file, name_current_iteration):
        name_current_iteration_complete = self.learner + name_current_iteration + '_' + str(self.config_index)
        
        out_file.write(name_current_iteration_complete + '_tm' + '=[')
        for result_time in self.resulting_times[:-1]:
            out_file.write('%1.4f,' % result_time)
        out_file.write('%1.4f];\n' % self.resulting_times[-1])
        
    def write_iteration_data_analysis_functions(self, out_file, name_current_iteration):
        name_current_iteration_complete = self.learner + name_current_iteration + '_' + str(self.config_index)
        
        out_file.write(name_current_iteration_complete + ' = mean(' + name_current_iteration_complete + '_m);\n')
        out_file.write(name_current_iteration_complete + '_t = mean(' + name_current_iteration_complete + '_tm);\n')
        out_file.write(name_current_iteration_complete + '_v' + ' = var(' + name_current_iteration_complete + '_m);\n')
        out_file.write(name_current_iteration_complete + '_E' + ' = std(' + name_current_iteration_complete + '_m);\n')
        out_file.flush() # save just in case
        
        # Write to result_vector string
        if (len(self.result_vector) != 0):
            self.result_vector += ','
            self.result_vector_v += ','
            self.result_vector_E += ','
            self.result_vector_t += ','
        self.result_vector += name_current_iteration_complete
        self.result_vector_v += name_current_iteration_complete + '_v'
        self.result_vector_E += name_current_iteration_complete + '_E'
        self.result_vector_t += name_current_iteration_complete + '_t'
    
    def write_final_results(self, out_file):
        out_file.write("%" + self.learner + str(self.config_index) + '\n');
        name_learner_experiment = self.learner + str(self.config_index)
        # Hack to be able to join results from different runs quickly
        out_file.write(name_learner_experiment +        '=[];\n')
        out_file.write(name_learner_experiment + "_v" + '=[];\n')
        out_file.write(name_learner_experiment + "_E" + '=[];\n')
        out_file.write(name_learner_experiment + "_t" + '=[];\n')
        # Useful results with hack
        out_file.write(name_learner_experiment +        '=[' + name_learner_experiment + ","   + self.result_vector   + '];\n')
        out_file.write(name_learner_experiment + "_v" + '=[' + name_learner_experiment + "_v," + self.result_vector_v + '];\n')
        out_file.write(name_learner_experiment + "_E" + '=[' + name_learner_experiment + "_E," + self.result_vector_E + '];\n')
        out_file.write(name_learner_experiment + "_t" + '=[' + name_learner_experiment + "_t," + self.result_vector_t + '];\n')
        # Useful results
        #out_file.write(name_learner_experiment +        '=[' + self.result_vector   + '];\n')
        #out_file.write(name_learner_experiment + "_v" + '=[' + self.result_vector_v + '];\n')
        #out_file.write(name_learner_experiment + "_E" + '=[' + self.result_vector_E + '];\n')
        #out_file.write(name_learner_experiment + "_t" + '=[' + self.result_vector_t + '];\n')
    
    def do_single_benchmark(self, num_transitions, number_simulations, extra_time):
        current_dir=os.getcwd()
        differences_file="tempo_differences.txt"
        lfit_rules_path="./lfit_rules.dat"
        binary_path = get_bin_path(current_dir) + '/learner_utils'
        
        shutil.copyfile(self.config_name, "rexd_config.cfg")
        
        if (self.learner == "lfit"):
            command_learn = binary_path + " lfit " + lfit_rules_path + " > ./tempo_learn.txt"
        else:
            command_learn = binary_path + " learn > ./tempo_learn.txt"
        
        command_test_probability = binary_path + " evaluate_rules learned_rules.ppddl " + str(number_simulations) + " > ./" + differences_file
        
        #print "Learning"
        finished = False
        try:
            start_time = time.time()
            execute_command(command_learn)
            diff_time = time.time() - start_time
            if (self.learner == "lfit"):
                diff_time += extra_time
            
            #print "Checking rules"
            execute_command(command_test_probability)
            error_prob = read_error_probability(differences_file)
            
            self.resulting_times.append(diff_time)
            self.resulting_probs.append(error_prob)
            self.average_prob = self.average_prob + error_prob
            
        except RuntimeError:
            print("Error learning!")
            self.resulting_times.append(99999.9)
            self.resulting_probs.append(1.0)
    

def prepare_benchmark(num_transitions, number_simulations):
    current_dir=os.getcwd()
    binary_path = get_bin_path(current_dir) + '/learner_utils'
    command_generate_transitions = binary_path + " transitions " + str(num_transitions) + " > ./tempo_transitions.txt"
    command_generate_lfit_rules = binary_path + " generate_lfit" + " > ./tempo_learn.txt"
    
    #print "Generating transitions"
    execute_command(command_generate_transitions)
    #print "Learning"
    finished = False
    while not finished:
        try:
            start_time = time.time()
            execute_command(command_generate_lfit_rules)
            total_time = time.time() - start_time
            finished = True
        except RuntimeError:
            print("Retrying learning...")
    return total_time

def do_set_benchmarks(min_transitions, max_transitions, num_repetitions, number_simulations, num_config_readers):
    out_file = open('learning_results.txt','w')
    #result_vector = learner + '=['
    
    list_benchmarkers=[]
    for i in range(0, num_config_readers):
        list_benchmarkers.append(ConfigBenchmarker(i+1))
    
    for number_transitions in range(min_transitions,max_transitions+1):
        out_file.write("\n% Transitions: " + str(number_transitions) + '\n');
        name_current_iteration = '_exp' + str(number_transitions)
        print("Number transitions: " + str(number_transitions))
        average_time = 0.0
        #out_file.write(name_current_iteration + '_m' + '=[')
        for i in range(0,num_repetitions):
            update_progress(float(i)/float(num_repetitions))
            prepare_time = prepare_benchmark(number_transitions ,number_simulations)
            average_time += prepare_time
            for benchmarker in list_benchmarkers:
                new_prob = benchmarker.do_single_benchmark(number_transitions, number_simulations, prepare_time)
            os.remove("./lfit_rules.dat")
            
        update_progress(1)
        average_time = average_time / num_repetitions
        print("Got avg lfit time: " + str(average_time))
        out_file.write("\nlfit_time" + name_current_iteration + " = " + str(average_time) + ';\n');

        # Show in terminal the average for current iteration
        for benchmarker in list_benchmarkers:
            benchmarker.write_iteration_results(out_file, name_current_iteration)
            average_prob = benchmarker.average_prob / num_repetitions
            print("Got avg prob: " + str(average_prob))
            
        for benchmarker in list_benchmarkers:
            benchmarker.write_iteration_times(out_file, name_current_iteration)
            
        for benchmarker in list_benchmarkers:
            benchmarker.write_iteration_data_analysis_functions(out_file, name_current_iteration)
            benchmarker.reset_iteration()

    for benchmarker in list_benchmarkers:
        benchmarker.write_final_results(out_file)
    out_file.close()


def main():
    if len(sys.argv) < 6:
        print("Not enough arguments.")
        print("learner_bench.py min_transitions max_transitions num_repetitions num_simulations num_configs")
    else:
        do_set_benchmarks(int(sys.argv[1]),int(sys.argv[2]),int(sys.argv[3]), int(sys.argv[4]), int(sys.argv[5]))

if __name__ == "__main__":
    main()
