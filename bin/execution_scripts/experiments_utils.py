#!/usr/bin/env python

import os
import subprocess
import shutil
import sys
import shutil
import re
try:
    from gi.repository import Notify
    DISABLE_LIBNOTIFY=False
except ImportError:
    DISABLE_LIBNOTIFY=True

generic_folder = '/generic/'
generic_files = ['template_ppddl',
                 'logger.conf']

scenario_files = []
                  
problem_files = ['domain.pddl',
                 'domain_ground_truth.pddl',
                 'problem_init.pddl',
                 'rexd_config.cfg',]

optional_problem_files = ['problem_init2.pddl',
                          'problem_init3.pddl',
                          'problem_init4.pddl',
                          'problem_init5.pddl',
                          'rexd_config.cfg1',
                          'rexd_config.cfg2',
                          'rexd_config.cfg3',
                          'rexd_config.cfg4',
                          'rexd_config.cfg5',
                          'rexd_config.cfg6',
                          'rexd_config.cfg7',
                          'rexd_config.cfg8',
                          'rexd_config.cfg9',
                          'rexd_config.cfg10',
                          'rexd_config.cfg11',
                          'rexd_config.cfg12',
                          'rexd_config.cfg13',
                          'rexd_config.cfg14',
                          'rexd_config.cfg15',
                          'rexd_config.cfg16',
                          'rexd_config.cfg17',
                          'rexd_config.cfg18',
                          'rexd_config.cfg19'
                          ]
existing_files = []
                 
orig_copy_files = ['problem_init.pddl']
dest_copy_files = ['problem_current.pddl']
                  
generated_files = ['intellact_logging.txt',
                   'gource.log',
                   'gource_caption.log',
                   'video_editing.avs',
                   'visualization.ass',
                   'visualization_verbose.ass',
                   'transitions_tmp.dat',
                   'transitions_full.dat',
                   'rules.ppddl']
                   
tmp_files = ['current_learned_rules.dat',
             'learned_rules.dat',
             'gen_symbols.dat',
             'learn.log',
             'learn.log.info',
             'MT.log',
             'info.html']


def load_scenario_and_problem(argv):
    scenario=""
    if len(sys.argv) <= 1:
        exit
    else:
        scenario=sys.argv[1]
        
    problem=""
    if len(sys.argv) <= 2:
        res = scenario.split('/')
        if len(res) >= 3:
            scenario = res[1]
            problem = res[2]
        else:
            exit
    else:
        problem=sys.argv[2]
    
    return [scenario, problem]


def start_experiment(scenario, problem):
    current_dir=os.getcwd()
    experiment_dir=current_dir + '/current'
    
    # create link "current" pointing to a tmp dir in tmpfs
    create_tmpfs_symlink_if_not_exists(experiment_dir)
    
    # COPY SCENARIO FILES
    scenario_files_dir=current_dir + '/domains/' + scenario + '/'
    copy_files(scenario_files, scenario_files_dir, experiment_dir)
    
    # COPY PROBLEM FILES
    problem_files_dir=current_dir + '/domains/' + scenario + '/' + problem + '/'
    copy_files(problem_files, problem_files_dir, experiment_dir)
    copy_files_optional(optional_problem_files, problem_files_dir, experiment_dir)
    
    # COPY GENERIC FILES
    generic_files_dir=current_dir + '/' + generic_folder + '/'
    copy_files(generic_files, generic_files_dir, experiment_dir)
    
    # GENERATE EMPTY REQUIRED FILES
    create_files(generated_files, experiment_dir)
    
    # CREATE COPIES
    copy_files_in_same_filefolder(orig_copy_files, dest_copy_files, experiment_dir)


def execute_experiment(scenario, problem, debug=False):
    current_dir=os.getcwd()
    experiment_dir=current_dir + '/current/'
    intellact_path = get_bin_path(current_dir) + '/intellact'
    
    os.chdir(experiment_dir)
    if debug:
        intellact_path = 'gdb -ex run --args ' + intellact_path
    res = subprocess.call(intellact_path, shell=True)
    os.chdir(current_dir)
    if (res != 0):
        print "Unexpected error"
        if not DISABLE_LIBNOTIFY:
            Notify.init("REX-D")
            notify_msg = Notify.Notification.new("REX-D","Unexpected error! ","dialog-error")
            notify_msg.show ()
        raise RuntimeError


def finish_experiment(scenario, problem):
    current_dir=os.getcwd()
    experiment_dir=current_dir + '/current/'
    results_dir=current_dir + '/results/'
    current_result_dir='experiment_' + scenario + '_' + problem + '_'

    ps=os.popen('ls '+results_dir+' | grep ' + current_result_dir + ' | cut -d "_" -f4 2>/dev/null')
    nums=ps.readlines()
    if len(nums):
        num=max([int(x) for x in nums])
    else:
        num=0
    num=num+1
    num_str = "%04d" % (num)
    
    # create folder results/experiment_scenario_problem_num
    if not os.path.exists(results_dir):
        os.mkdir(results_dir)
    copy_path=results_dir + '/' + current_result_dir + num_str
    os.mkdir(copy_path)
    print 'Moving to ' + copy_path
    
    # Add tmp rule files
    for files in (y for y in os.listdir(experiment_dir) if (y.startswith("rules") and (y.endswith(".dat")) and (len(y)>=10)) ):
        generated_files.append(files)
    
    #try:
    move_files(scenario_files + generic_files + problem_files + existing_files + generated_files + dest_copy_files, experiment_dir, copy_path)
    move_files(optional_problem_files, experiment_dir, copy_path, verbose = False)
    remove_files(tmp_files, experiment_dir, verbose = False)
    #except:
        #print "Unexpected error:", sys.exc_info()[0]

    print "Finished."




def copy_files(files, orig_path, dest_path):
    for a_file in files:
        shutil.copy2(orig_path + '/' + a_file, dest_path)

def copy_files_optional(files, orig_path, dest_path):
    for a_file in files:
        try:
            shutil.copy2(orig_path + '/' + a_file, dest_path)
            existing_files.append(a_file)
        except:
            a=1 # noop

def copy_files_in_same_filefolder(orig_files, dest_files, folder):
    dest_files_copy = list(dest_files)
    for orig_file in orig_files:
        dest_file = dest_files_copy.pop(0)
        shutil.copy2(folder + '/' + orig_file, folder + '/' + dest_file)

def create_files(files, path):
    for a_file in files:
        open(path + '/' + a_file, 'w').close()

def move_files(files, orig_path, dest_path, verbose = True):
    for a_file in files:
        try:
            shutil.move(orig_path + '/' + a_file, dest_path)
        except:
            if verbose:
                print("Warning: Couldn't move " + a_file)

def remove_files(files, path, verbose = True):
    for a_file in files:
        try:
            os.remove(path + '/' + a_file)
        except:
            if verbose:
                print("Warning: Couldn't remove " + a_file)


def create_tmpfs_symlink_if_not_exists(folder_path):
    USE_TMPFS = 1
    if USE_TMPFS:
        if os.path.islink(folder_path):
            # check broken link
            folder_path_tmp = '/run/shm/' + re.sub('/','_',folder_path)
            if (folder_path_tmp != os.readlink(folder_path)) or (not os.path.exists(folder_path_tmp)):
                print 'Removing old "current" folder link'
                os.unlink(folder_path)
        
        if not os.path.exists(folder_path):
            create_tmpfs_symlink(folder_path)
    else:
        if not os.path.exists(folder_path):
            os.mkdir(folder_path)


def create_tmpfs_symlink(folder_path):
    bin_path = get_bin_path(os.getcwd())
    
    # Since Ubuntu 11.10 /dev/shm is in /run/shm
    folder_path_tmp = '/run/shm/' + re.sub('/','_',folder_path)
    if not os.path.exists(folder_path):
        if not os.path.exists(folder_path_tmp):
            os.mkdir(folder_path_tmp)
        os.symlink(folder_path_tmp, folder_path)
    
    if not os.path.islink(folder_path_tmp + "/bin"):
        os.symlink(bin_path, folder_path_tmp + "/bin")
        

def get_bin_path(current_dir):
    bin_path = ''
    if os.path.exists(current_dir + '/../bin'):
        bin_path = current_dir + '/../bin/'
    elif os.path.exists(current_dir + '/../../bin'):
        bin_path = current_dir + '/../../bin/'
    else:
        bin_path = current_dir + '/../../../bin'
    return os.path.realpath(bin_path)

    
def main():
    print 'blah'

if __name__ == "__main__":
    main()
