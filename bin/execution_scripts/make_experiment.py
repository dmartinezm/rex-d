#!/usr/bin/env python

import os
import shutil
import sys
from experiments_utils import *

[scenario, problem] = load_scenario_and_problem(sys.argv)

start_experiment(scenario, problem)

try:
    execute_experiment(scenario, problem)
    finish_experiment(scenario, problem)
except KeyboardInterrupt:
    print 'Keyboard interrupt. Cancelling execution and not copying files. Run finish_experiment.py to copy files.'
    sys.exit(1)
except RuntimeError:
    print 'Runtime Error. Cancelling execution and not copying files. Run finish_experiment.py to copy files.'
    sys.exit(1)
except:
    raise    
