#!/usr/bin/env python

import os
import shutil
import sys
from experiments_utils import *

[scenario, problem] = load_scenario_and_problem(sys.argv)

finish_experiment(scenario, problem)
