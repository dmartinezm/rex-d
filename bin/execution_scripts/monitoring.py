#!/usr/bin/env python

import os
import shutil
import sys

current_dir=os.getcwd()
scenario=os.path.split(current_dir)[1]
experiments_dir=current_dir + '/experiments/'
experiment_type1="pasula"


experiment_type2="mon"


# COPY BASE FILES
init_files_dir=current_dir + '/init_files_' + experiment_type2 + '/'
shutil.copy2(init_files_dir + '/config', current_dir)
shutil.copy2(init_files_dir + '/intellact_logging.txt', current_dir)
shutil.copy2(init_files_dir + '/reward.dat', current_dir)
shutil.copy2(init_files_dir + '/symbols.dat', current_dir)

# EXECUTION
#os.system('../bin/intellact < basic_sequence.txt')
os.system('../bin/intellact ' + scenario)

