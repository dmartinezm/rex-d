#!/bin/bash

current_dir=`pwd`
simulator_exec_path_rel="../bin/simulator"
if [ -e $simulator_exec_path_rel ]
then
    simulator_exec_path="${current_dir}/$simulator_exec_path_rel"
else
    simulator_exec_path="${current_dir}/../$simulator_exec_path_rel"
fi

while [  1 ]; do
    cd $current_dir
    if [ -d "current" ]; then
        cd "current"
    fi
    $simulator_exec_path
    sleep 2
    echo ""
    echo ""
    echo ""
    echo "NEW"
done
