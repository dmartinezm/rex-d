#edit the following line to add the librarie's header files
FIND_PATH(rexd_INCLUDE_DIR symbolic/symbols.h /usr/include/rexd /usr/local/include/rexd)

FIND_LIBRARY(rexd_LIBRARY
    NAMES rexd
    PATHS /usr/lib /usr/local/lib /usr/local/lib/iridrivers) 

IF (rexd_INCLUDE_DIR AND rexd_LIBRARY)
    SET(rexd_LIBRARIES ${rexd_LIBRARY})
    SET(rexd_INCLUDE_DIRS ${rexd_INCLUDE_DIR})
   
    SET(rexd_FOUND TRUE)
ENDIF (rexd_INCLUDE_DIR AND rexd_LIBRARY)

IF (rexd_FOUND)
    IF (NOT rexd_FIND_QUIETLY)
        MESSAGE(STATUS "Found rexd: ${rexd_LIBRARY}")
    ENDIF (NOT rexd_FIND_QUIETLY)
ELSE (rexd_FOUND)
    IF (rexd_FIND_REQUIRED)
        MESSAGE(FATAL_ERROR "Could not find rexd")
    ENDIF (rexd_FIND_REQUIRED)
ENDIF (rexd_FOUND)

