#include "weighted_causal_graph.h"

using namespace std;
using namespace std::tr1;


/*
  An IntRelationBuilder constructs an IntRelation by adding one pair
  to the relation at a time. Duplicates are automatically handled
  (i.e., it is OK to add the same pair twice), and the pairs need not
  be added in any specific sorted order.

  Define the following parameters:
  - K: range of the IntRelation (i.e., allowed values {0, ..., K - 1})
  - M: number of pairs added to the relation (including duplicates)
  - N: number of unique pairs in the final relation
  - D: maximal number of unique elements (x, y) in the relation for given x

  Then we get:
  - O(K + N) memory usage during construction and for final IntRelation
  - O(K + M + N log D) construction time
*/

class WeightedIntRelationBuilder {
    typedef unordered_set<weighted_int> IntSet;
    vector<IntSet> int_sets;

    int get_range() const;
public:
    explicit WeightedIntRelationBuilder(int range);
    ~WeightedIntRelationBuilder();

    void add_pair(int u, int v, float weight);
    void compute_relation(WeightedIntRelation &result) const;
};


WeightedIntRelationBuilder::WeightedIntRelationBuilder(int range)
    : int_sets(range) {
}


WeightedIntRelationBuilder::~WeightedIntRelationBuilder() {
}


int WeightedIntRelationBuilder::get_range() const {
    return int_sets.size();
}


void WeightedIntRelationBuilder::add_pair(int u, int v, float weight) {
    assert(u >= 0 && u < get_range());
    assert(v >= 0 && v < get_range());
    int_sets[u].insert(weighted_int(v, weight));
}


void WeightedIntRelationBuilder::compute_relation(WeightedIntRelation &result) const {
    size_t range = get_range();
    result.clear();
    result.resize(range);
    for (size_t i = 0; i < range; ++i) {
        result[i].assign(int_sets[i].begin(), int_sets[i].end());
        sort(result[i].begin(), result[i].end());
    }
}


struct WeightedCausalGraphBuilder {
    WeightedIntRelationBuilder pre_eff_builder;
    WeightedIntRelationBuilder eff_pre_builder;
    WeightedIntRelationBuilder eff_eff_builder;

    WeightedIntRelationBuilder succ_builder;
    WeightedIntRelationBuilder pred_builder;

    explicit WeightedCausalGraphBuilder(int var_count)
        : pre_eff_builder(var_count),
          eff_pre_builder(var_count),
          eff_eff_builder(var_count),
          succ_builder(var_count),
          pred_builder(var_count) {
    }

    ~WeightedCausalGraphBuilder() {
    }

    void handle_pre_eff_arc(int u, int v, float weight) {
        assert(u != v);
        pre_eff_builder.add_pair(u, v, weight);
        succ_builder.add_pair(u, v, weight);
        eff_pre_builder.add_pair(v, u, weight);
        pred_builder.add_pair(v, u, weight);
    }

    void handle_eff_eff_edge(int u, int v, float weight) {
        assert(u != v);
        eff_eff_builder.add_pair(u, v, weight);
        eff_eff_builder.add_pair(v, u, weight);
        succ_builder.add_pair(u, v, weight);
        succ_builder.add_pair(v, u, weight);
        pred_builder.add_pair(u, v, weight);
        pred_builder.add_pair(v, u, weight);
    }

    void handle_operator(const Typed::Rule &rule, Domain& domain) {
        int DEBUG = 0;
        const PredicateGroup<TypedAtom> &preconditions = rule.get_preconditions();

        if (DEBUG > 0)
            std::cout << "CausalGraph::" << "handle_operator: " << "Adding rule " << rule << std::endl;

        
        BOOST_FOREACH(const Outcome<TypedAtom>& outcome, rule.outcomes) {
            if (outcome.probability > 0.0) {
                // Handle pre->eff links from preconditions.
                BOOST_FOREACH(const Predicate<TypedAtom>& outcome_predicate, outcome) {
                    BOOST_FOREACH(const Predicate<TypedAtom>& precondition_predicate, preconditions) {
                        int prec_id = domain.get_predicate_index(precondition_predicate);
                        int eff_id = domain.get_predicate_index(outcome_predicate);
                        if (prec_id != eff_id)
                            handle_pre_eff_arc(prec_id, eff_id, outcome.probability);
                    }
                }
                
                // Handle eff->eff links.
                BOOST_FOREACH(const Predicate<TypedAtom>& outcome_predicate1, outcome) {
                    BOOST_FOREACH(const Predicate<TypedAtom>& outcome_predicate2, outcome) {
                        int eff1_id = domain.get_predicate_index(outcome_predicate1);
                        int eff2_id = domain.get_predicate_index(outcome_predicate2);
                        if (eff1_id != eff2_id)
                            handle_eff_eff_edge(eff1_id, eff2_id, outcome.probability);
                    }
                }
            }
        }
    }
};

WeightedCausalGraph::WeightedCausalGraph() {}

void WeightedCausalGraph::init(Domain& domain, const Typed::RuleSet rules) {
    WeightedCausalGraphBuilder cg_builder(domain.get_predicates_size()*2); // hack because we are adding in pre-eff effects the negated effects

    for (size_t i = 0; i < rules.size(); ++i) {
        cg_builder.handle_operator(*(rules[i]), domain);
    }

//     for (size_t i = 0; i < g_axioms.size(); ++i)
//         cg_builder.handle_operator(g_axioms[i]);

    cg_builder.pre_eff_builder.compute_relation(pre_to_eff);
    cg_builder.eff_pre_builder.compute_relation(eff_to_pre);
    cg_builder.eff_eff_builder.compute_relation(eff_to_eff);

    cg_builder.pred_builder.compute_relation(predecessors);
    cg_builder.succ_builder.compute_relation(successors);

    // dump();
}


WeightedCausalGraph::~WeightedCausalGraph() {
}


void WeightedCausalGraph::dump(Domain domain) const {
    cout << "Causal graph: " << endl;
    for (size_t var = 0; var < domain.get_predicates_size(); ++var)
        cout << "#" << domain.get_predicate(var) << " [" << var << "]:" << endl
             << "    pre->eff arcs: " << pre_to_eff[var] << endl
             << "    eff->pre arcs: " << eff_to_pre[var] << endl
             << "    eff->eff arcs: " << eff_to_eff[var] << endl
             << "    successors: " << successors[var] << endl
             << "    predecessors: " << predecessors[var] << endl;
}
