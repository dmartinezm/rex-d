#include "failure_analyzer_tools.h"

#define DEBUG_EXCUSE_RULE_ALT 0


std::vector<Typed::RuleSet> FailureAnalyzerUtils::get_rules_that_overcome_planning_failure(const Typed::RuleSet rules, const PlanningFailureOpt& planning_failure_opt, const TransitionGroup<TypedAtom>& transitions) const
{
    if (!planning_failure_opt) {
        return std::vector<Typed::RuleSet>();
    }
    
    int DEBUG = DEBUG_EXCUSE_RULE_ALT;
    std::vector<Typed::RuleSet> extra_rulesets;

    double orig_score = rules.score(transitions, scenario.get_state().ppddl_object_manager);

    Typed::RuleSet rules_requiring_planning_failure;

    PlanningFailure symbolic_planning_failure(*planning_failure_opt);
    symbolic_planning_failure.make_symbolic();
    rules_requiring_planning_failure = rules.find_rules_with_a_precondition(symbolic_planning_failure);
    
    // TODO change/remove precondition
    CINFO("excuses") << "Changing " << rules_requiring_planning_failure.size() << " rules.";
    for ( Typed::RuleSet::iterator rule_it=rules_requiring_planning_failure.begin() ; rule_it != rules_requiring_planning_failure.end(); ++rule_it ) {
        Typed::RuleSet rules_copy(rules);
        rules_copy.remove_rule(*rule_it);

        Typed::RuleSet new_rules = create_new_rule_candidates_with_planning_failure(*rule_it, symbolic_planning_failure, transitions);
        CINFO("excuses") << "Checking " << new_rules.size() << " new rules for action " << (*rule_it)->name << " with score ( " << orig_score << " ): " << NOENDL;
        if (DEBUG > 1)
            CINFO("excuses") << "Original rule was " << **rule_it;
        for ( Typed::RuleSet::iterator new_rule_it=new_rules.begin() ; new_rule_it != new_rules.end(); ++new_rule_it ) {
            Typed::RuleSet new_generated_rules(rules_copy);
            new_generated_rules.push_back(*new_rule_it);
            double new_score = new_generated_rules.score(transitions, scenario.get_state().ppddl_object_manager);
            if (new_score != (-1. * std::numeric_limits<double>::max()) ) {
                if (DEBUG > 1)
                    CINFO("excuses") << "Got new rule with score " << new_score << " (orig score was " << orig_score << ")";
                if (DEBUG > 2)
                    CINFO("excuses") << **new_rule_it;
                if (new_score >= orig_score) {
                    CINFO("excuses") << "*" << NOENDL;
                    if (DEBUG > 0)
                        CINFO("excuses") << "Got new rule \"" << (*new_rule_it)->name
                                         << "\" with score " << new_score << " (orig score was " << orig_score << ")";
                    if (DEBUG > 1)
                        CINFO("excuses") << **new_rule_it;
                    extra_rulesets.push_back(new_generated_rules);
                }
                else {
                    CINFO("excuses") << "." << NOENDL;
                }
            }
        }
        CINFO("excuses");
    }

    return extra_rulesets;
}


Typed::RuleSet FailureAnalyzerUtils::create_new_rule_candidates_with_planning_failure(const Typed::RulePtr rule, const PredicateGroup<TypedAtom> symbolic_planning_failures, const TransitionGroup<TypedAtom>& transitions) const
{
    int DEBUG = DEBUG_EXCUSE_RULE_ALT;
    Typed::RuleSet result;
    if (DEBUG > 0)
        CINFO("excuses") << "Rule candidates for rule " << rule->name << " with symbolic planning_failures " << symbolic_planning_failures;
    
    if (DEBUG > 3)
        CINFO("excuses") << "Obtaining possible preconditions...";
    PredicateGroup<TypedAtom> new_preconditions;
    for ( TransitionSet<TypedAtom>::const_iterator transition_it = transitions.begin_set() ; transition_it != transitions.end_set(); ++transition_it ) {
        if (rule->covers_transition_preconditions(*transition_it, transitions.ppddl_object_manager) > 0.0) {
            new_preconditions.add_predicates(transition_it->get_prev_state());
        }
    }
    new_preconditions.erase(symbolic_planning_failures);
    if (DEBUG > 2)
        CINFO("excuses") << "Final possible preconditions are " << new_preconditions;

    if (DEBUG > 3)
        CINFO("excuses") << "Generating rules with new preconditions...";

    // TODO check if passing typed params is good!
    // WARNING PROBABLY SHOULD DO FILTERING BASED ON TYPES!
    PredicateGroup<TypedAtom> new_preconditions_symbolic = failure_analyzer_utils::get_symbolic_predicates_permutations(new_preconditions, rule->params);

    if (DEBUG > 3)
        CINFO("excuses") << "Adding symbolic predicates " << new_preconditions_symbolic;
    for ( PredicateGroup<TypedAtom>::iterator sym_pred_it=new_preconditions_symbolic.begin() ; sym_pred_it != new_preconditions_symbolic.end(); ++sym_pred_it) {
        #if __cplusplus > 199711L // C++11
        Typed::RulePtr new_rule_ptr = boost::make_shared<Typed::SymbolicRule>(Typed::SymbolicRule(*rule));
        #else
        Typed::RulePtr new_rule_ptr = Typed::RulePtr(boost::make_shared<Typed::SymbolicRule>(Typed::SymbolicRule(*rule)));
        #endif
        PredicateGroup<TypedAtom> new_rule_preconditions = new_rule_ptr->get_preconditions();
        new_rule_preconditions.erase(symbolic_planning_failures);
        if (!new_rule_preconditions.has_predicate(*sym_pred_it)) {
            if (DEBUG > 4)
                CINFO("excuses") << "Adding new rule candidate with precondition " << *sym_pred_it;
            new_rule_preconditions.insert(*sym_pred_it);
            new_rule_ptr->set_preconditions(new_rule_preconditions);
            result.push_back(new_rule_ptr);
        }
    }

    return result;
}

