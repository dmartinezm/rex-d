/*
 * AutoTeacher class to be used for automatic experiments.
 * Copyright (C) 2016  David Martínez (dmartinez@iri.upc.edu)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */


#ifndef AUTO_TEACHER_H
#define AUTO_TEACHER_H

#include <symbolic/scenario.h>
#include <symbolic/utils.h>
#include <parsers/pddl_parser.h>

#include <rexd/planner_factory.h>

/**
 * \class AutoTeacher
 * \brief Automatic teacher to generate demonstrations when they are requested.
 * 
 * Can only be used in simulated domains.\n
 * Uses a planner and the simulator rules to obtain the optimal policy.
 * When a demonstration is requested, the optimal action for that state is selected.\n
 * It has a few tweaks to select the desired demonstrations. 
 * The following policies can be enabled in the configuration:
 * - Only execute actions recommended by excuses, request an human teacher when fails to do so.
 * - Retry several times.
 * - Hacks and tricks for specific scenarios.
 * - Signal dead end when no action can be found.
 * 
 * Has to be enabled with the <em>auto_teacher</em> config option.
 */
class AutoTeacher
{

public:
    AutoTeacher(const ConfigReader& config_reader_);

    /**
     * \brief Gets an action. If it cannot find an appropiate action, help is requested to an human.
     * 
     * It plans an optimal action using the simulator rules.
     * If the planned action is not in the list of known actions (action_list), it is always selected, even if it has no relation with the excuses.\n
     * Depending on the configuration parameters, it only selects a known action as a valid one if it is recommended by the excuses.
     * (i.e. the action has to be in actions_requiring_excuse).
     * 
     * Configuration options:
     * - auto_teacher_infinite: enable retries if planned action is not selected.
     * - auto_teacher_infinite_retries: max number of retries getting new planned action.
     * - auto_teacher_infinite_full_retries: max number of full retries. A full retry is done after auto_teacher_infinite_retries normal retries. 
     *   A full retry consists in getting a new state and redoing all the system standard planning process (including generating excuses again).
     * - auto_teacher_forced: always select planned action.
     * - auto_teacher_excuse_autoexecute: select planned action if it is recommended by excuses.
     * - auto_teacher_autodead: signals a dead-end if no planned action is found.
     * 
     * \param scenario scenario in which to plan.
     * \param actions_requiring_excuse list of actions that require the excuse to be applied. In most scenarios the demonstrated action should almost always be in this list.
     * \retval planning_result reward obtained by the planner
     * \return planned action (depending on the configuration, it has to be in the excuses)
     */
    TypedAtomOpt get_action(const Scenario& scenario,
                            const std::vector<std::string> actions_requiring_excuse,
                            float& planning_result);

    /**
     * \brief Gets a planned action using the simulated rules.
     */
    TypedAtomOpt get_action_simple(const Scenario& scenario, const int verbose) const;

    /**
     * \brief Gets the expected success probability after executing an action.
     * 
     * For each outcome of the action, it checks if it is a dead end.
     * If it isn't the outcome probability is added to the expected success probability.
     */
    float get_expected_action_success_probability(const Scenario& scenario, const TypedAtom& action) const;
    
    bool check_valid_action(const TypedAtom& action) const;

private:
    PlannerPtr planner;
    Scenario stateless_scenario;
    uint complete_retries;

    TypedAtomOpt get_planned_action(const Scenario& scenario, const std::vector<std::string> actions_requiring_excuse, float& planning_result);

    void infinite_autoteacher(TypedAtomOpt& action_opt, uint& auto_teacher_retries);
};

typedef boost::shared_ptr<AutoTeacher> AutoTeacherPtr;

#endif // AUTO_TEACHER_H
