/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2013  <copyright holder> <email>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "auto_teacher.h"


AutoTeacher::AutoTeacher(const ConfigReader& config_reader_):
    stateless_scenario(config_reader_.get_path("ppddl_domain_file_ground_truth"), 
                       config_reader_.get_path("simulator_ppddl_current_problem_file"), 
                       config_reader_),
    complete_retries(0)
{
    // create planner
    planner = PlannerFactory::get_standard_planner(config_reader_);
}

bool AutoTeacher::check_valid_action(const TypedAtom& action) const
{
    if (action == MAKE_TYPED_ATOM_NOOP_ACTION) {
        return true;
    }
    else {
            BOOST_FOREACH(const RulePtr<TypedAtom>& rule_ptr, stateless_scenario.get_rules()) {
            if (action.symbolic_compare(*rule_ptr)) {
                return true;
            }
        }
    }
    return false;
}

float AutoTeacher::get_expected_action_success_probability(const Scenario& scenario, const TypedAtom& action) const
{
    Scenario planning_scenario(stateless_scenario);
    planning_scenario.set_state(scenario.get_state());
    planning_scenario.set_planning_horizon(scenario.get_planning_horizon());
    return planner->get_expected_action_success_probability(planning_scenario, action, false);
}

TypedAtomOpt AutoTeacher::get_action_simple(const Scenario& scenario, const int verbose) const
{
    Scenario planning_scenario(stateless_scenario);
    planning_scenario.set_state(scenario.get_state());
    planning_scenario.set_planning_horizon(scenario.get_planning_horizon());
    return planner->plan(planning_scenario, true, verbose);
}

TypedAtomOpt AutoTeacher::get_action(const Scenario& scenario,
                                     const std::vector<std::string> actions_requiring_excuse,
                                     float& planning_result)
{
    TypedAtomOpt action_opt = TypedAtomOpt();
    std::string action_string;
    CINFO("teacher") << "Auto Teacher!";

    uint auto_teacher_retries = 0;
    while (!action_opt) {
        action_opt = get_planned_action(scenario, actions_requiring_excuse, planning_result);
        
        if ((!action_opt)) {
            if (stateless_scenario.config_reader.get_variable<bool>("auto_teacher_infinite") ) {
                infinite_autoteacher(action_opt, auto_teacher_retries);
            }
            else {
                action_opt = TypedAtomOpt (MAKE_TYPED_ATOM_HELP); // read_new_action();
            }
        }
    }
    return action_opt;
}


TypedAtomOpt AutoTeacher::get_planned_action(const Scenario& scenario,
                                             const std::vector<std::string> actions_requiring_excuse,
                                             float& planning_result)
{
    TypedAtomOpt planned_action;
    // TODO stateless scenario is probably not the best idea
    RexdScenario rexd_scenario(stateless_scenario);
    rexd_scenario.set_state(scenario.get_state());
    rexd_scenario.set_planning_horizon(scenario.get_planning_horizon());
    rexd_scenario.add_reward(scenario.get_reward());
//     rexd_scenario.set_vmin();
    
    TypedAtomOpt teacher_plan = planner->plan(rexd_scenario, true, 0, planning_result);
    if (teacher_plan && (*teacher_plan != MAKE_TYPED_ATOM_TEACHER)) {
        TypedAtom action = *teacher_plan;
        planned_action = TypedAtomOpt(action);
        TypedAtom symbolic_action = TypedAtom(action);
        symbolic_action.make_symbolic();
        std::vector<TypedAtom> action_list = scenario.get_action_list();

        // always add if it is a new action
        if (stateless_scenario.config_reader.get_variable<bool>("auto_teacher_forced")) {
            CINFO("teacher") << "Auto teacher: " << action << " (" << planning_result << ") - forced mode.";
        }
        else if (std::find(action_list.begin(), action_list.end(), symbolic_action)!=action_list.end()) {
            // not a new action
            // add action only if it is in actions requiring excuse
            if (std::find(actions_requiring_excuse.begin(), actions_requiring_excuse.end(), planned_action->name)==actions_requiring_excuse.end()) {
                // action not found in actions_requiring_excuse
                CINFO("teacher") << "Auto teacher got action: " << action << " (" << planning_result << ") but is not recommended by excuses. Requesting human teacher help.";
                CINFO("teacher") << "Waiting for new action: ";
                planned_action = TypedAtomOpt();
            }
            else if (stateless_scenario.config_reader.get_variable<bool>("auto_teacher_excuse_autoexecute")) {
                CINFO("teacher") << "Action " << action << " (" << planning_result << ") matches excuses recommendation, executing it.";
            }
            else {
                CINFO("teacher") << "Action " << action << " (" << planning_result << ") matches excuses recommendation.";
                CINFO("teacher") << "Waiting for new action: ";
                planned_action = TypedAtomOpt();
            }
        }
        else {
            CINFO("teacher") << "Action " << action << " (" << planning_result << ") is a new action. Selecting it as this unknown action seems neccesary for this task.";
        }
    }
    else { // no plan or teacher action obtained (which means reward < Vmin -> no plan)
        CINFO("teacher") << "Auto teacher failed (dead end?)";
        if (stateless_scenario.config_reader.get_variable<bool>("auto_teacher_autodead")) {
            planned_action = TypedAtomOpt(MAKE_TYPED_ATOM_DEAD);
        }
        else {
            CINFO("teacher") << "Waiting for new action: ";
            planned_action = TypedAtomOpt();
        }
    }
    return planned_action;
}


void AutoTeacher::infinite_autoteacher(TypedAtomOpt& action_opt, uint& auto_teacher_retries)
{
    auto_teacher_retries ++;
    if (auto_teacher_retries > stateless_scenario.config_reader.get_variable<uint>("auto_teacher_infinite_retries")) {
        complete_retries++;
        if (complete_retries < stateless_scenario.config_reader.get_variable<uint>("auto_teacher_infinite_full_retries")) {
            action_opt = TypedAtomOpt(MAKE_TYPED_ATOM_RETRY);
        }
        else {
            complete_retries = 0;
            action_opt = TypedAtomOpt(MAKE_TYPED_ATOM_HELP);
        }
    }
}


