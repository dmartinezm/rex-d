/*
 * Class to find planning failures given a scenario, and perform tasks related to them.
 * Copyright (C) 2016  David Martínez (dmartinez@iri.upc.edu)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef INTELLACT_FAILURE_ANALYZER_H
#define INTELLACT_FAILURE_ANALYZER_H

#include <queue>

#include <teacher/weighted_causal_graph.h>
#include <teacher/failure_analyzer_utils.h>
#include <rexd/planner_factory.h>


/**
 * \class FailureAnalyzer
 * \brief Finds planning failures
 * 
 * Generates planning failure candidates and selects the best ones.
 */
class FailureAnalyzer {
public:
    FailureAnalyzer(const Scenario& scenario);

    // get planning_failure
    PlanningFailureOpt get_planning_failure();

protected:
    Scenario scenario;

private:
    WeightedCausalGraph causal_graph;
    Typed::RuleSet grounded_rules;
    PredicateGroup<TypedAtom> goal_predicates;
    
    bool planning_failure_already_generated;
    PlanningFailureOpt planning_failure;
    
    /** \brief Enable using predicates with more parameters as candidates (ex: from clear(X) obtain a on(X,Y) )
     * 
     * Problems: candidates with no parameters can obtain all possible grounded predicates.
     *           example: started() -> on(1,2), on(1,3), ... on(1,5), ... on(5,4)
     */
    bool enable_planning_failure_candidates_extended;
    
    /** \brief Enable using also eff->eff relations in causal graph
     */
    bool enable_planning_failure_candidates_eff_eff;
    
    /** \brief Don't dismiss candidates because they are already satisfied, they may also lead to other candidates that wouldn't be find otherwise.
     */
    bool config_allow_candidates_present_in_current_state;
    
    /** \brief Duplicate reward penalty for all excuses predicates after the first.
     */
    bool config_duplicate_penalty_after_first_predicate;

    static const std::string EXCUSE_RULE_PREFIX_ADD,
                             EXCUSE_RULE_PREFIX_NEGATE;
    static const char EXCUSE_RULE_PREFIX_SEPARATOR;

    // Get candidates
    PlanningFailureOpt generate_planning_failure();
    FailureCandidateList get_failure_candidates(const PredicateGroup<TypedAtom>& goal_predicates);
    
    // get relevant_domain
    FailureCandidateList get_failure_candidates_candidates_from_relevant_domain(const Predicate<TypedAtom>& goal_predicate, const bool extended);
    void get_failure_candidates_candidates_from_relevant_domain(const RelevantDomainPredicate& domain_predicate,
                                                                FailureCandidateList& failure_candidates, 
                                                                RelevantDomainPredicateMinimalSet& visited_predicates, 
                                                                std::vector< RelevantDomainPredicate > recursive_stack = std::vector<RelevantDomainPredicate>()
                                                               );
    std::vector< RelevantDomainPredicate > get_relevant_domain_within_next_level(const RelevantDomainPredicate& domain_predicate, FailureCandidateList& failure_candidates);

    // utils
    double get_reachable_probability(const Predicate<TypedAtom> predicate) const;
    bool check_valid_domain() const;

    // Distances
    float get_distance_to_goal(const size_t original_predicate_idx);
    float get_distance_to_predicate(const size_t original_predicate_idx, const size_t target_predicate_idx);

    // Temporary action for planning_failures
    PlanningFailureOpt find_planning_failures(const FailureCandidateList& planning_failure_candidates);
    PredicateGroup<TypedAtom> obtain_planning_failures_planning_with_aux_actions(const FailureCandidateList& planning_failure_candidates);
    
    std::vector<TypedAtom> obtain_plan_with_aux_actions(const Typed::RuleSet& new_rules);
    Typed::RuleSet get_rules_to_add_planning_failures(const FailureCandidateList& planning_failures) const;
    Typed::RulePtr create_planning_failure_change_predicate(const FailureCandidate& failure_candidate) const;
    PredicateGroup<TypedAtom> get_planning_failures_from_planning_failure_actions(std::vector<TypedAtom> planning_failure_plan);
    Predicate<TypedAtom>::Ptr get_atom_from_planning_failure_action(TypedAtom action);
};

#endif
