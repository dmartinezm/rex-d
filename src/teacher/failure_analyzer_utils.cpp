#include "failure_analyzer_utils.h"

#define DEBUG_EXCUSES_UTILS 0

#define REWARD_MODIFIER_WHEN_PRED_IS_GOAL 5

bool operator<(const RelevantDomainPredicate& l, const RelevantDomainPredicate& r)
{
    return l.idx < r.idx;
}

std::ostream& operator<<(std::ostream &out, const FailureCandidate &f)
{
    uint DEBUG = DEBUG_EXCUSES_UTILS;
    out << f.predicate;
    if (DEBUG >1) {
        out << "[";
        out << "R=" << f.reward_penalty;
        if (f.is_reachable())
            out << ", reach=" << f.reachable_probability;
        if (f.is_extended())
            out << ", ext"; 
        if (f.is_recursive())
            out << ", rec";
        if (f.is_temporal())
            out << ", temp=" << f.distance_temporal;
        else
            out << ", d=" << f.distance_graph;
        out << "]";
    }
    return out;
}

std::ostream& operator<<(std::ostream &out, const FailureCandidateList &f)
{
    bool first = true;
    BOOST_FOREACH(const FailureCandidateList::PredicateFailureMap::value_type& failure_candidate, f.failure_candidate_map) {
        if (!first) {
            out << ", "; 
        }
        else {
            first = false;
        }
        out << failure_candidate.second;
    }
    return out;
}

bool RelevantDomainPredicateMinimalSet::contains_equally_good_or_better (const RelevantDomainPredicate& val) const
{
    std::set<RelevantDomainPredicate>::const_iterator it = this->relevant_domain_predicate_set.find(val);
    if (it == relevant_domain_predicate_set.end()) { // doesn't contain
        return false;
    }
    else {
        if (it->extended <= val.extended) {
            return true; // not extended is better
        }
        else {
            return false;
        }
    }
}

std::pair<std::set<RelevantDomainPredicate>::iterator,bool> RelevantDomainPredicateMinimalSet::insert (const RelevantDomainPredicate& val)
{
    std::set<RelevantDomainPredicate>::iterator it = this->relevant_domain_predicate_set.find(val);
    if (it == relevant_domain_predicate_set.end()) { // doesn't contain
        return relevant_domain_predicate_set.insert(val);
    }
    else {
        if (it->extended > val.extended) { // extended is worse
            it->extended = val.extended;
            return std::pair<std::set<RelevantDomainPredicate>::iterator, bool> (it, true);
        }
        else {
            return std::pair<std::set<RelevantDomainPredicate>::iterator, bool> (it, false);
        }
    }
}


bool FailureCandidate::is_better(const FailureCandidate& failure_candidate) const
{
    // temporal != graph
    if (this->temporal_candidate != failure_candidate.temporal_candidate) {
        LOG(ERROR) << "WARNING!!! TODO";
        return false;
    }
    // graph
    else if (!this->temporal_candidate && !failure_candidate.temporal_candidate) { // not temporal -> graph distance
        if (this->reachable_candidate < failure_candidate.reachable_candidate) {
            return true;
        }
        else if (this->reachable_probability < failure_candidate.reachable_probability) {
            return true;
        }
        else if (this->recursive_candidate < failure_candidate.recursive_candidate) {
            return true;
        }
        else if (this->extended_candidate < failure_candidate.extended_candidate) {
            return true;
        }
        // distance
        else if (this->distance_graph > failure_candidate.distance_graph) {
            return true;
        }
        else if (this->distance_graph < failure_candidate.distance_graph) {
            return false;
        }
        else {
            return false;
        }
    }
    // temporal
    else if (this->temporal_candidate && failure_candidate.temporal_candidate) { // temporal -> temporal distance
        if (this->distance_temporal > failure_candidate.distance_temporal) {
            return false;
        }
        else if (this->distance_temporal < failure_candidate.distance_temporal) {
            return true;
        }
        else {
            return false;
        }
    }
    else {
        return false;
    }
}

void FailureCandidate::set_reward(float base_reward_penalty)
{
    if (is_temporal()) {
        CERROR("excuses") << "Not implemented!";
    }
    else {
        if (distance_graph > 0.0) {
            this->reward_penalty = base_reward_penalty / distance_graph;
        }
        else {
            this->reward_penalty = base_reward_penalty * REWARD_MODIFIER_WHEN_PRED_IS_GOAL;
        }
    }
}


void FailureCandidateList::add_candidate(const FailureCandidate& candidate)
{
    PredicateFailureMap::iterator failure_it = failure_candidate_map.find(candidate.predicate);
    if (failure_it != failure_candidate_map.end()) {
        if (candidate.is_better(failure_it->second)) {
            failure_candidate_map.insert( std::make_pair( candidate.predicate, candidate ) );
        }
    }
    else { // doesn't exit
        failure_candidate_map.insert( std::make_pair( candidate.predicate, candidate ) );
    }
}

void FailureCandidateList::concat_candidates(const FailureCandidateList& new_candidates)
{
    BOOST_FOREACH(const PredicateFailureMap::value_type& failure_candidate, new_candidates.failure_candidate_map) {
        add_candidate(failure_candidate.second);
    }
}

FailureCandidateList FailureCandidateList::get_candidates(const bool allow_reachable, const bool min_reachable_probability, 
                                                          const bool allow_extended, 
                                                          const bool allow_recursive, 
                                                          const bool allow_temporal) const
{
    FailureCandidateList result;
    
    BOOST_FOREACH( const PredicateFailureMap::value_type& failure_candidate, failure_candidate_map ) {
        if ( (allow_reachable || !failure_candidate.second.is_reachable(min_reachable_probability)) &&
             (allow_extended  || !failure_candidate.second.is_extended() ) &&
             (allow_recursive || !failure_candidate.second.is_recursive()) &&
             (allow_temporal  || !failure_candidate.second.is_temporal() )
           )
        {
            result.add_candidate(failure_candidate.second);
        }
    }
    
    return result;
}


PredicateGroup<TypedAtom> FailureCandidateList::get_predicates() const
{
    PredicateGroup<TypedAtom> result;
    
    BOOST_FOREACH( const PredicateFailureMap::value_type &failure_candidate, failure_candidate_map ) {
        result.insert(failure_candidate.second.predicate);
    }
    
    return result;
}

void FailureCandidateList::set_rewards(float base_reward_penalty, uint reward_penalty_modifier)
{
    float current_reward_penalty = base_reward_penalty / pow(2, reward_penalty_modifier);
    
    uint DEBUG = DEBUG_EXCUSES_UTILS;
    BOOST_FOREACH( PredicateFailureMap::value_type &failure_candidate, failure_candidate_map ) {
        failure_candidate.second.set_reward(current_reward_penalty);
        CLOG_IF(DEBUG > 0, INFO, "excuses") << "Reward for " << failure_candidate.second << " is " << failure_candidate.second.get_reward_penalty();
    }
}


std::string failure_analyzer_utils::planning_failure_to_string(PlanningFailureOpt planning_failure_opt) {
    if (planning_failure_opt) {
        return boost::lexical_cast<std::string>(*planning_failure_opt);
    }
    else {
        return "";
    }
}

PredicateGroup<TypedAtom> failure_analyzer_utils::get_symbolic_predicates_permutations(const PredicateGroup<TypedAtom>& input_predicates, const std::vector< PPDDL::FullObject >& symbolic_params) {
    PredicateGroup<TypedAtom> result;
    BOOST_FOREACH(const Predicate<TypedAtom>& predicate, input_predicates) {
        result.add_predicates(get_symbolic_predicates_permutations(predicate, symbolic_params));
    }
    return result;
}

PredicateGroup<TypedAtom> failure_analyzer_utils::get_symbolic_predicates_permutations(const Predicate<TypedAtom>& input_predicate, const std::vector<PPDDL::FullObject>& symbolic_params) {
    if (symbolic_params.size() < input_predicate.params.size()) {
        LOG(WARNING) << "Deictic paramaters not implemented in alternative rules!";
        return PredicateGroup<TypedAtom>();
        // TODO deictic references
//         while (symbolic_params_copy.size() < input_predicate.params.size()) { // add default parameters if there are not enough of them
//             symbolic_params_copy.push_back(DEFAULT_PARAM_NAMES[input_predicate.params.size()]);
//         }
    }
    PredicateGroup<TypedAtom> grounded_predicates;
    std::vector<PPDDL::FullObject> symbolic_params_copy(symbolic_params);
    std::sort(symbolic_params_copy.begin(), symbolic_params_copy.end());
    do {
        bool types_ok = true;
        Predicate<TypedAtom> predicate(input_predicate);
        for (size_t j = 0; j < input_predicate.params.size(); j++) {
            types_ok = types_ok && (predicate.params[j].type == symbolic_params_copy[j].type);
            predicate.params[j].name = symbolic_params_copy[j].name;
        }
        if (types_ok) {
            grounded_predicates.insert(predicate);
        }
    } while (std::next_permutation(symbolic_params_copy.begin(), symbolic_params_copy.end()));

    return grounded_predicates;
}

