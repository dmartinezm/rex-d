/*
 * Class to find rule alternatives given a scenario and planning_failures.
 * Copyright (C) 2016  David Martínez (dmartinez@iri.upc.edu)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef INTELLACT_FAILURE_ANALYZER_UTILS_H
#define INTELLACT_FAILURE_ANALYZER_UTILS_H

#include <symbolic/rules.h>
#include <symbolic/domain.h>

/**
 * \class FailureCandidate
 * \brief Planning failure candidate
 * 
 * Planning failure candidate.
 */
class FailureCandidate {
public:
    FailureCandidate(const Predicate<TypedAtom>& predicate, const float reachable_probability, const bool extended_candidate, const bool recursive_candidate, const bool temporal_candidate) :
        predicate(predicate),
        reward_penalty(0),
        reachable_candidate(reachable_probability > 0.0),
        reachable_probability(reachable_probability),
        extended_candidate(extended_candidate),
        recursive_candidate(recursive_candidate),
        temporal_candidate(temporal_candidate),
        distance_graph(-1.0),
        distance_temporal(-1)
    {};
    
    /*! \brief Check if it is a better candidate than the compared one */
    bool is_better(const FailureCandidate& failure_candidate) const;
    
    /*! \brief Is reachable with known rules? */
    bool is_reachable() const { return reachable_candidate; } ;
    
    /*! \brief Is reachable with a certain probability with known rules? */
    bool is_reachable(const float min_reachable_probability) const { return reachable_candidate && (reachable_probability > min_reachable_probability); } ;
    
    /*! \brief From goal does it requires to expand to arguments not present in goal predicates? */
    bool is_extended()  const { return extended_candidate; } ;
    
    /*! \brief Was it added when adding recursively the relevant domain */
    bool is_recursive() const { return recursive_candidate; } ;
    
    /*! \brief Was it added through temporal analysis */
    bool is_temporal()  const { return temporal_candidate; } ;
    
    /*! \brief Set base reward penalty (and apply modifiers afterwards) */
    void set_reward(float reward_penalty);
    
    /*! \brief Set distance in weighted causal graph */
    void set_distance_graph(float distance) { distance_graph = distance; };
    
    /*! \brief Set distance in timesteps */
    void set_distance_temporal(uint distance) { distance_temporal = distance; };
    
    /*! \brief Get reward penalty */
    float get_reward_penalty() const { return reward_penalty; };
    
    /*! \brief Candidate predicate */
    Predicate<TypedAtom> predicate;
    
    /*! \brief Operator \<\< */
    friend std::ostream& operator<<(std::ostream &out, const FailureCandidate &f);
    
private:
    float reward_penalty;
    bool reachable_candidate;
    float reachable_probability;
    bool extended_candidate;
    bool recursive_candidate;
    bool temporal_candidate;
    float distance_graph; // would be int in non-weighted causal graph
    uint distance_temporal;
};

/**
 * \class FailureCandidateList
 * \brief List of not-repeated failure candidates
 * 
 * When repeated predicates are added, only the best one is maintained
 */
class FailureCandidateList {
private:
    typedef std::map<Predicate<TypedAtom>, FailureCandidate> PredicateFailureMap;
    PredicateFailureMap failure_candidate_map;

public:
    FailureCandidateList() {};
    
    FailureCandidateList(const FailureCandidateList& other):
        failure_candidate_map(other.failure_candidate_map)
    {};
    
    /**
     * \brief Test whether vector is empty
     * Returns whether the vector is empty (i.e. whether its size is 0).
     */
    bool empty() const { return failure_candidate_map.empty(); };
    
    /**
     * \brief Add a candidate if the same candidate replacing worse equivalent candidates.
     */
    void add_candidate(const FailureCandidate& candidate);
    
    /**
     * \brief Concats candidates. Removes worse versions of repeated candidates.
     */
    void concat_candidates(const FailureCandidateList& new_candidates);
    
    /**
     * \brief set rewards in base to a base reward.
     * 
     * Applies all modifiers to the rewards based on each planning failure values.
     */
    void set_rewards(float base_reward_penalty, uint reward_penalty_modifier);
    
    /**
     * \brief Returns candidates satisfying requirements. Only lowest penalty reward candidate for each predicate is returned.
     */
    FailureCandidateList get_candidates(const bool allow_reachable, const bool min_reachable_probability, const bool allow_extended, const bool allow_recursive, const bool allow_temporal) const;
    
    /**
     * \brief Gets list of predicates that form the FailureCandidateList
     */
    PredicateGroup<TypedAtom> get_predicates() const;
    
    /*! \brief Operator \<\< */
    friend std::ostream& operator<<(std::ostream &out, const FailureCandidateList &f);
    
//     typedef iterator PredicateFailureMap::iterator;
    typedef PredicateFailureMap::const_iterator const_iterator;
    
    const_iterator begin() const { return failure_candidate_map.begin(); };
    const_iterator end()   const { return failure_candidate_map.end(); };
};


/**
 * \class RelevantDomainPredicate
 * \brief Predicate<TypedAtom> in the relevant domain
 * 
 * Contains index for causal graph and if it is an "extended" candidate
 */
class RelevantDomainPredicate {
public:
    /** \brief Constructor */
    RelevantDomainPredicate(const int idx, const Predicate<TypedAtom> predicate, const bool extended):
        idx(idx),
        predicate(predicate),
        extended(extended)
    {}
    
    int idx;
    Predicate<TypedAtom> predicate;
    mutable bool extended; // used in std::set which only cares about idx
    friend bool operator<(const RelevantDomainPredicate& l, const RelevantDomainPredicate& r);
};

/**
 * \class RelevantDomainPredicateMinimalSet
 * 
 * It uses a std::set of RelevantDomainPredicate
 */
class RelevantDomainPredicateMinimalSet {
public:
    /**
     * \brief True if the predicate is found with equal or better attribute values
     * 
     * Better values are:
     *  - Not extended is better than extended.
     */
    bool contains_equally_good_or_better (const RelevantDomainPredicate& val) const;
    
    /**
     * \brief Redefines std::set::insert. Only inserts if predicate doesn't exist, and replaces if predicate exists and has worse attribute values.
     * 
     * Better values are:
     *  - Not extended is better than extended.
     */
    std::pair<std::set<RelevantDomainPredicate>::iterator,bool> insert (const RelevantDomainPredicate& val);
    
private:
    std::set<RelevantDomainPredicate> relevant_domain_predicate_set;
};

typedef PredicateGroup<TypedAtom> PlanningFailure;
typedef boost::optional< PlanningFailure > PlanningFailureOpt;

namespace failure_analyzer_utils {
    std::string planning_failure_to_string(PlanningFailureOpt planning_failure_opt);
    PredicateGroup<TypedAtom> get_symbolic_predicates_permutations(const PredicateGroup<TypedAtom>& input_predicates, const std::vector< PPDDL::FullObject >& symbolic_params);
    PredicateGroup<TypedAtom> get_symbolic_predicates_permutations(const Predicate<TypedAtom>& input_predicate, const std::vector<PPDDL::FullObject>& symbolic_params);
}

#endif
