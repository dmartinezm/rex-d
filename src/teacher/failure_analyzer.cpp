#include "failure_analyzer.h"

#define USE_WEIGHTED_CAUSAL_GRAPH

#define DEBUG_EXCUSES 0
#define DEBUG_EXCUSE_GENERATION 0
#define DEBUG_EXCUSE_SELECTION 0
#define DEBUG_EXCUSE_SUBGOALS 0
#define DEBUG_PLANNING 0

// planning_failures candidates search
#define EXCUSES_CANDIDATES_MAX_DEPTH 8
#define SECOND_EXCUSE_PENALTY_MULTIPLIER 4

// Special strings for planning_failure selection
// WARNING the domain shouldn't have these strings to function properly
const std::string FailureAnalyzer::EXCUSE_RULE_PREFIX_ADD = "add";
const std::string FailureAnalyzer::EXCUSE_RULE_PREFIX_NEGATE = "neg";
const char FailureAnalyzer::EXCUSE_RULE_PREFIX_SEPARATOR = '_';


FailureAnalyzer::FailureAnalyzer(const Scenario& scenario_):
    scenario(scenario_),
    planning_failure_already_generated(false),
    enable_planning_failure_candidates_extended(true),
    enable_planning_failure_candidates_eff_eff(false)
{
    int DEBUG = DEBUG_EXCUSES;
    
    config_duplicate_penalty_after_first_predicate = scenario.config_reader.get_variable<bool>("excuses_duplicate_penalty_after_first_predicate");
    config_allow_candidates_present_in_current_state = scenario.config_reader.get_variable<bool>("excuses_allow_candidates_in_current_state");

    // grounded rules
    grounded_rules = scenario.get_rules().get_pddl_grounded_rules(scenario.get_state(), true);
    grounded_rules.remove_nop_rules();
    grounded_rules.add_negated_outcomes_as_preconditions();
    scenario.domain.index_rules(grounded_rules);

    CLOG_IF(DEBUG > 0, INFO, "excuses") << "Creating planning_failure analyzer!";
    CLOG_IF(DEBUG > 2, INFO, "excuses") << scenario.domain;
    CLOG_IF(DEBUG > 1, INFO, "excuses") << "Domain size is " << scenario.domain.get_predicates_size();

    // causal graph
    causal_graph.init(scenario.domain, grounded_rules);
    if (DEBUG > 1) {
        causal_graph.dump(scenario.domain);
    }

    // goals
    if (scenario.config_reader.get_variable<bool>("final_reward_action")) {
        Typed::RuleSet finish_rules = grounded_rules.get_action_rules(MAKE_TYPED_ATOM_FINISH_ACTION);
        goal_predicates = finish_rules.front()->goals.get_predicates_that_contribute_to_goal();
    }
    else {
        goal_predicates = scenario.get_goals().get_predicates_that_contribute_to_goal();
    }
    
    assert(check_valid_domain());

    CLOG_IF(DEBUG > 0, INFO, "excuses") << "Created planning_failure analyzer!";
}


PlanningFailureOpt FailureAnalyzer::get_planning_failure()
{
    if ( (!planning_failure_already_generated) && scenario.config_reader.get_variable<bool>("rule_analysis") ) {
        CINFO("excuses") << "Generating excuses... " << NOENDL;
        planning_failure = generate_planning_failure();
        CLOG_IF(planning_failure, INFO, "excuses") << "Best excuse is " << *planning_failure;
        CLOG_IF(!planning_failure, INFO, "excuses") << "No excuse could be obtained";
        planning_failure_already_generated = true;
    }
    return planning_failure;
}


PlanningFailureOpt FailureAnalyzer::generate_planning_failure()
{
    PlanningFailureOpt res_planning_failures;
    FailureCandidateList planning_failure_candidates = get_failure_candidates(goal_predicates);
    
    if (!planning_failure_candidates.empty()) {
        res_planning_failures = find_planning_failures(planning_failure_candidates);
        if (!res_planning_failures) {
            CINFO("excuses") << "No planning_failure found";
            // if no planning_failure found -> return the whole goal state
            // planning_failures = simple_goal.get_grounded_predicates().get_not_satisfied_by(scenario.get_state());
        }
    }
    else {
        CINFO("excuses") << "No planning_failure candidates";
    }
    
    return res_planning_failures;
}


FailureCandidateList FailureAnalyzer::get_failure_candidates(const PredicateGroup<TypedAtom>& goal_predicates)
{
    int DEBUG = DEBUG_EXCUSE_GENERATION;
    FailureCandidateList failure_candidates_list;

    // Get planning_failures
    if (scenario.config_reader.get_variable<std::string>("planner") == PLANNER_PROST) {
        /* Case where excuses can be symbolic.
         * Good way of doing it would be actually using grounded excuses:
         * - Get list of grounded candidates (easy)
         * - Make them work with PROST reward function (difficult, how to use grounded values there?)
         */
        BOOST_FOREACH(const Predicate<TypedAtom>& goal_predicate, goal_predicates) {
            CLOG_IF(DEBUG > 0, INFO, "excuses") << "Getting excuse candidates for goal " << goal_predicate;
            FailureCandidate failure_candidate(goal_predicate, 0.0, false, false, false);
            failure_candidate.set_distance_graph(1);
            failure_candidates_list.add_candidate(failure_candidate);
        }
    }
    else {
        BOOST_FOREACH(const Predicate<TypedAtom>& goal_predicate, goal_predicates) {
            CLOG_IF(DEBUG > 0, INFO, "excuses") << "Getting excuse candidates for goal " << goal_predicate;
            FailureCandidateList new_failure_candidates = get_failure_candidates_candidates_from_relevant_domain(goal_predicate, false);
            failure_candidates_list.concat_candidates( new_failure_candidates );
        }
    }
    CLOG_IF(DEBUG > 0, INFO, "excuses") << "Obtained excuse candidates are " << failure_candidates_list;

    return failure_candidates_list;
}


FailureCandidateList FailureAnalyzer::get_failure_candidates_candidates_from_relevant_domain(const Predicate<TypedAtom>& goal_predicate, const bool extended)
{
    RelevantDomainPredicateMinimalSet visited_predicates;
    FailureCandidateList failure_candidates;
    size_t goal_idx = scenario.domain.get_predicate_index(goal_predicate);
    RelevantDomainPredicate goal_domain_predicate(goal_idx, goal_predicate, extended);
    get_failure_candidates_candidates_from_relevant_domain(goal_domain_predicate, failure_candidates, visited_predicates);
    return failure_candidates;
}


void FailureAnalyzer::get_failure_candidates_candidates_from_relevant_domain(const RelevantDomainPredicate& domain_predicate,
                                                                             FailureCandidateList& failure_candidates,
                                                                             RelevantDomainPredicateMinimalSet& visited_predicates,
                                                                             std::vector<RelevantDomainPredicate> recursive_stack)
{
    int DEBUG = DEBUG_EXCUSE_GENERATION;
    if (recursive_stack.size() > EXCUSES_CANDIDATES_MAX_DEPTH)
        return;

    visited_predicates.insert(domain_predicate);
    recursive_stack.push_back(domain_predicate);

    if (!config_allow_candidates_present_in_current_state && scenario.get_state().satisfies(domain_predicate.predicate)) {
        // already in the state, ignoring....
        CLOG_IF(DEBUG > 1, INFO, "excuses") << "debug: " << domain_predicate.predicate << " already satisfied.";
    }
    else {
        CLOG_IF(DEBUG > 1, INFO, "excuses") << "debug: " << domain_predicate.predicate << " (" << domain_predicate.idx << "), param_names.size()";
        
        // add failure candidate
        // TODO add min prob to is_reachable
        double reachable_prob = get_reachable_probability(domain_predicate.predicate);
        FailureCandidate failure_candidate(domain_predicate.predicate, reachable_prob, domain_predicate.extended, false, false);
        failure_candidate.set_distance_graph(get_distance_to_goal(domain_predicate.idx));
        failure_candidates.add_candidate(failure_candidate);
        
        // Get relevant domain
        std::vector<RelevantDomainPredicate> relevant_domain = get_relevant_domain_within_next_level(domain_predicate, failure_candidates);
        
        BOOST_FOREACH(const RelevantDomainPredicate& next_level_domain_predicate, relevant_domain) {
            if (visited_predicates.contains_equally_good_or_better(next_level_domain_predicate)) {
                // add recursively dom_rel(f)
                std::vector<RelevantDomainPredicate>::iterator old_predicate_it = recursive_stack.end();
                old_predicate_it--; // now pointing to domain_predicate.idx
                while ( (old_predicate_it != recursive_stack.begin()) ) {
                    old_predicate_it--;
                    if (old_predicate_it->idx != domain_predicate.idx) {
                        FailureCandidate failure_candidate(old_predicate_it->predicate, get_reachable_probability(old_predicate_it->predicate), old_predicate_it->extended, true, false);
                        failure_candidate.set_distance_graph(get_distance_to_goal(old_predicate_it->idx));
                        failure_candidates.add_candidate(failure_candidate);
                        CLOG_IF(DEBUG > 1, INFO, "excuses") << "debug: Adding recursively dom_rel(f), predicate: " << old_predicate_it->predicate << " (" << old_predicate_it->idx << ")";
                    }
                }
            }
            else {
                // Not visited
                CLOG_IF(DEBUG > 2, INFO, "excuses") << "Recursive for " << next_level_domain_predicate.idx 
                                                    << " (" << next_level_domain_predicate.predicate << ")";
                get_failure_candidates_candidates_from_relevant_domain(next_level_domain_predicate, failure_candidates, 
                                                                       visited_predicates, recursive_stack);
            }
        }
    }
}

std::vector<RelevantDomainPredicate> FailureAnalyzer::get_relevant_domain_within_next_level(const RelevantDomainPredicate& domain_predicate, FailureCandidateList& failure_candidates)
{
    int DEBUG = DEBUG_EXCUSE_GENERATION;
    std::vector<RelevantDomainPredicate> variable_relevant_domain;
    
    std::vector<weighted_int> relevant_domain_candidates = causal_graph.get_eff_to_pre(domain_predicate.idx);
    if (enable_planning_failure_candidates_eff_eff) {
        std::vector<weighted_int> candidates_eff_to_eff = causal_graph.get_eff_to_eff(domain_predicate.idx);
        relevant_domain_candidates.insert( relevant_domain_candidates.end(), candidates_eff_to_eff.begin(), candidates_eff_to_eff.end() );
    }
    
    BOOST_FOREACH(const weighted_int& rel_dom_candidate, relevant_domain_candidates) {
        Predicate<TypedAtom> rel_dom_candidate_predicate = scenario.domain.get_predicate(rel_dom_candidate.first);
        
        if (!goal_predicates.has_predicate(rel_dom_candidate_predicate)) { // don't add goal predicates!
            if (domain_predicate.predicate.params.size() >= rel_dom_candidate_predicate.params.size()) {
                RelevantDomainPredicate relevant_domain_predicate(rel_dom_candidate.first, rel_dom_candidate_predicate, false);
                variable_relevant_domain.push_back(relevant_domain_predicate);
                CLOG_IF(DEBUG > 2, INFO, "excuses") << "debug: Checking " << rel_dom_candidate_predicate << " because has less or equal parameters than " << domain_predicate.predicate;
            }
            else if (enable_planning_failure_candidates_extended) {
                RelevantDomainPredicate relevant_domain_predicate(rel_dom_candidate.first, rel_dom_candidate_predicate, true);
                variable_relevant_domain.push_back(relevant_domain_predicate);
                CLOG_IF(DEBUG > 2, INFO, "excuses") << "debug: Not checking " << rel_dom_candidate_predicate << " because has more parameters than " << domain_predicate.predicate;
            }
        }
    }
    CLOG_IF(DEBUG > 1, INFO, "excuses") << "debug: " << "Variable relevant domain size is " << variable_relevant_domain.size();
    
    return variable_relevant_domain;
}





float FailureAnalyzer::get_distance_to_goal(const size_t original_predicate_idx)
{
    float min_dist = std::numeric_limits<float>::max();

    BOOST_FOREACH(const Predicate<TypedAtom>& goal_pred, goal_predicates) {
        size_t goal_idx = scenario.domain.get_predicate_index(goal_pred);
        float new_dist = get_distance_to_predicate(original_predicate_idx, goal_idx);
        if (new_dist < min_dist)
            min_dist = new_dist;
    }

    return min_dist;
}

// TODO from predicate to all goals at the same time
float FailureAnalyzer::get_distance_to_predicate(const size_t original_predicate_idx, const size_t target_predicate_idx)
{
    int DEBUG = DEBUG_EXCUSE_SUBGOALS;
    float result = 0;
    std::set<size_t> visited_predicates;
    typedef std::pair <size_t, float> PredicateAndDistance;
    std::queue<PredicateAndDistance> predicate_queue;
    PredicateAndDistance predicate_idx_dist (target_predicate_idx, 0);

    predicate_queue.push(predicate_idx_dist);
    visited_predicates.insert(target_predicate_idx);

    bool found = false;
    while ( (!found) && (!predicate_queue.empty()) ) {
        PredicateAndDistance current_predicate_idx_dist = predicate_queue.front();
        predicate_queue.pop();
        size_t current_predicate_idx = current_predicate_idx_dist.first;
        float current_predicate_dist = current_predicate_idx_dist.second;
        CLOG_IF(DEBUG > 3, INFO, "excuses") << "get_distance_to_predicate: " << "debug: " << scenario.domain.get_predicate(current_predicate_idx) << " (" << current_predicate_dist << ")";

        if (original_predicate_idx == current_predicate_idx) {
            CLOG_IF(DEBUG > 2, INFO, "excuses") << "Found with distance " << current_predicate_dist;
            result = current_predicate_dist;
            found = true;
        }
        else {
            std::vector<weighted_int> variable_relevant_domain;
            std::vector<weighted_int> eff_to_pre = causal_graph.get_eff_to_pre(current_predicate_idx);
            std::vector<weighted_int> eff_to_eff = causal_graph.get_eff_to_eff(current_predicate_idx);
            variable_relevant_domain = eff_to_pre;

            for (std::vector<weighted_int>::iterator it_p = variable_relevant_domain.begin(); it_p != variable_relevant_domain.end(); ++it_p) {
                if (visited_predicates.count(it_p->first) == 0) { // Not visited
                    assert(it_p->second > 0); // weight > 0 (prob > 0)
                    visited_predicates.insert(it_p->first);
#ifndef USE_WEIGHTED_CAUSAL_GRAPH
                    PredicateAndDistance new_predicate_idx_dist (it_p->first, current_predicate_dist + 1);
#else
                    PredicateAndDistance new_predicate_idx_dist (it_p->first, current_predicate_dist + (1 / it_p->second));
#endif
                    predicate_queue.push(new_predicate_idx_dist);
                }
            }
        }
    }
    if (!found) {
        if (DEBUG > 2)
            CINFO("excuses") << "get_distance_to_predicate: " << "debug: " << " Not found!";
        result = std::numeric_limits<float>::max();
    }

    return result;
}



/**
 *
 * Obtaining excuses by creating auxiliary actions that add predicates.
 *
 */

PlanningFailureOpt FailureAnalyzer::find_planning_failures(const FailureCandidateList& planning_failure_candidates)
{
    int DEBUG = DEBUG_EXCUSE_SELECTION;
    CLOG_IF(DEBUG > 0, INFO, "excuses") << "Planning failure candidates are " << planning_failure_candidates;
    
    State state_and_planning_failures(scenario.get_state());
    state_and_planning_failures.add_predicates(planning_failure_candidates.get_predicates());
    Typed::RuleSet grounded_rules = scenario.get_rules().get_pddl_grounded_rules(state_and_planning_failures, true);

    // get initial planning_failure reward = min reward - max reward
    float noop_reward = scenario.config_reader.get_variable<float>("planning_noop_reward") * scenario.config_reader.get_variable<uint>("planning_horizon");
    float max_reward = scenario.config_reader.get_variable<float>("vmax_value") - noop_reward;
    float min_reward = scenario.config_reader.get_variable<float>("vmin_value");
    
    // we are dividing at least by 2. Predicates that are difficult ot obtain give even lower rewards
    float reward_penalty = (min_reward - max_reward) * 2;
    float reward_penalty_reduction = 7; // max value
    
    PlanningFailureOpt min_planning_failure_opt;
    int min_reward_penalty_reduction = std::numeric_limits<int>::max();
    
    bool reachable = scenario.config_reader.get_variable<bool>("excuses_reachable");
    float reachable_prob = scenario.config_reader.get_variable<float>("excuses_reachable_prob_fixed");
    bool extended = scenario.config_reader.get_variable<bool>("excuses_extended");
    bool recursive = scenario.config_reader.get_variable<bool>("excuses_recursive");
    bool temporal = scenario.config_reader.get_variable<bool>("excuses_temporal");
    bool looking_for_excuse = true;
    
    while (looking_for_excuse) {
//         CINFO("excuses") << ", " << NOENDL;
        FailureCandidateList current_planning_failure_candidates = planning_failure_candidates.get_candidates(reachable, reachable_prob, extended, recursive, temporal);
        current_planning_failure_candidates.set_rewards(reward_penalty, reward_penalty_reduction);
        PredicateGroup<TypedAtom> planning_failure = obtain_planning_failures_planning_with_aux_actions(current_planning_failure_candidates);
        
        if (!planning_failure.empty()) {
            CLOG_IF(DEBUG > 0, INFO, "excuses") << "Obtained " << planning_failure;
            min_planning_failure_opt = planning_failure;
            looking_for_excuse = false;
        }
        else {
            if (!recursive) {
                CINFO("excuses") << "(recursive) " << NOENDL;
                recursive = true;
            }
            else if (!extended) {
                CINFO("excuses") << "(extended) " << NOENDL;
                extended = true;
            }
            else if (!reachable) {
                CINFO("excuses") << "(reachable) " << NOENDL;
                reachable = true;
            }
            else if (!temporal) {
//                 CINFO("excuses") << "(temporal) " << NOENDL;
                CINFO("excuses") << "(ignoring temporal) " << NOENDL;
                looking_for_excuse = false;
            }
            else {
                
                looking_for_excuse = false;
            }
        }
    }
    
    min_planning_failure_opt = PlanningFailureOpt();
    reward_penalty_reduction = 4; // middle value (from 1 to 7)
    
    for (int i = 2; i >= 0; i--) {
        if (i != 2) { // not first iteration
            CINFO("excuses") << "(retrying, reducing factor " << reward_penalty_reduction << ") " << NOENDL;
        }
        else {
            CINFO("excuses") << "(reducing factor " << reward_penalty_reduction << ") " << NOENDL;
        }
        
        FailureCandidateList current_planning_failure_candidates = planning_failure_candidates.get_candidates(reachable, reachable_prob, extended, recursive, temporal);
        current_planning_failure_candidates.set_rewards(reward_penalty, reward_penalty_reduction);
        PredicateGroup<TypedAtom> planning_failure = obtain_planning_failures_planning_with_aux_actions(current_planning_failure_candidates);
        
        if (planning_failure.empty()) {
            reward_penalty_reduction += i;
        }
        else {
            CLOG_IF(DEBUG > 0, INFO, "excuses") << "Obtained " << planning_failure;
            if (reward_penalty_reduction < min_reward_penalty_reduction) {
                min_planning_failure_opt = planning_failure;
                min_reward_penalty_reduction = reward_penalty_reduction;
            }
            reward_penalty_reduction -= i;
        }
        
//         if (min_planning_failure_opt && (min_planning_failure_opt->size() == 1)) {
            // HACK to speed up things
//             break;
//         }
    }
    
//     // if just 1 planning failure, consider it as an already good planning_failure
//     if (min_planning_failure.size() != 1) {
//         CINFO("excuses") << "(extended) " << NOENDL;
//         FailureCandidateList current_planning_failure_candidates = planning_failure_candidates.get_candidates(false, true, true, false);
//         current_planning_failure_candidates.set_rewards(reward_penalty, reward_penalty_reduction);
//         PredicateGroup<TypedAtom> planning_failure = obtain_planning_failures_planning_with_aux_actions(current_planning_failure_candidates);
//         
//         if (!planning_failure.empty()) {
//             CLOG_IF(DEBUG > 0, INFO, "excuses") << "Obtained extended " << planning_failure;
//             min_planning_failure = planning_failure;
//         }
//     }
    
    CINFO("excuses"); // endline after searching planning_failures and retrying X times
    CLOG_IF(DEBUG > 1 && min_planning_failure_opt, INFO, "excuses") << "Excuses obtained were " << min_planning_failure_opt;
    CLOG_IF(DEBUG > 1 && !min_planning_failure_opt, INFO, "excuses") << "No excuses obtained";

    return min_planning_failure_opt;
}


PredicateGroup<TypedAtom> FailureAnalyzer::obtain_planning_failures_planning_with_aux_actions(const FailureCandidateList& planning_failure_candidates)
{
    State state_and_planning_failures(scenario.get_state());
    state_and_planning_failures.add_predicates(planning_failure_candidates.get_predicates());
//     Typed::RuleSet grounded_rules = scenario.get_rules();//.get_pddl_grounded_rules(state_and_planning_failures, true);
    
    Typed::RuleSet extra_rules;
    extra_rules = get_rules_to_add_planning_failures(planning_failure_candidates);
    Typed::RuleSet all_rules = scenario.get_rules();
    all_rules.add_rules(extra_rules);
    if (scenario.config_reader.get_variable<std::string>("planner") != PLANNER_PROST) {
        all_rules = all_rules.get_pddl_grounded_rules(state_and_planning_failures, true);
    }
    
    std::vector<TypedAtom> planning_failure_plan = obtain_plan_with_aux_actions(all_rules);
    PredicateGroup<TypedAtom> planning_failure = get_planning_failures_from_planning_failure_actions(planning_failure_plan);
    
    return planning_failure;
}


std::vector<TypedAtom> FailureAnalyzer::obtain_plan_with_aux_actions(const Typed::RuleSet& new_rules)
{
    PlannerPtr new_planner = PlannerFactory::get_standard_planner(scenario.config_reader);
    RexdScenario new_scenario(scenario); // note: no Vmin!
    State new_state = new_scenario.get_state();
    new_state.insert(Predicate<TypedAtom>(MAKE_TYPED_ATOM_EXCUSE_GENERATED, false));
    new_scenario.set_state(new_state);
    new_scenario.set_rules(new_rules);
    new_scenario.set_vmin();
    std::vector<TypedAtom> plan;
    if (new_planner->plan_n_actions(new_scenario, plan, false, DEBUG_PLANNING, scenario.config_reader.get_variable<uint>("planning_horizon"), true)) {
//     if (new_planner->plan_n_actions(new_scenario, plan, false, DEBUG_PLANNING, 5, true)) {
        return plan;
    }
    else {
        return std::vector<TypedAtom>();
    }
}



/**
 * 
 *  GET INFO FROM AUX ACTIONS
 * 
 */
PredicateGroup<TypedAtom> FailureAnalyzer::get_planning_failures_from_planning_failure_actions(std::vector<TypedAtom> planning_failure_plan)
{
    PredicateGroup<TypedAtom> result;

    for (std::vector<TypedAtom>::iterator plan_it = planning_failure_plan.begin(); plan_it != planning_failure_plan.end(); ++plan_it) {
        Predicate<TypedAtom>::Ptr planning_failure_ptr = get_atom_from_planning_failure_action(*plan_it);
        if (planning_failure_ptr) {
            result.insert(*planning_failure_ptr);
        }
    }

    return result;
}


Predicate<TypedAtom>::Ptr FailureAnalyzer::get_atom_from_planning_failure_action(TypedAtom action)
{
    Predicate<TypedAtom>::Ptr planning_failure_ptr;

    if (action.name.find(EXCUSE_RULE_PREFIX_ADD) == 0) {
        std::stringstream ss(action.name);
        std::string item, predicate_name;
        bool positive = true, has_more_params;

        // notation: add_name[_neg]{_param1}
        // get string between EXCUSE_RULE_PREFIX_SEPARATOR
        std::getline(ss, item, EXCUSE_RULE_PREFIX_SEPARATOR);
        std::getline(ss, item, EXCUSE_RULE_PREFIX_SEPARATOR);
        if (item == EXCUSE_RULE_PREFIX_NEGATE) {
            positive = false;
            std::getline(ss, item, EXCUSE_RULE_PREFIX_SEPARATOR);
        }
        predicate_name = item;
        planning_failure_ptr = Predicate<TypedAtom>::Ptr(new Predicate<TypedAtom>(TypedAtom(predicate_name), positive));

        has_more_params = std::getline(ss, item, EXCUSE_RULE_PREFIX_SEPARATOR);
        while(has_more_params) {
            planning_failure_ptr->params.push_back(
                PPDDL::FullObject(item, scenario.get_state().ppddl_object_manager.get_object_type(item)));
            has_more_params = std::getline(ss, item, EXCUSE_RULE_PREFIX_SEPARATOR);
        }
        
        // Currently it only supports replacing none or all action.params
        assert(action.params.empty() || (action.params.size() == planning_failure_ptr->params.size()));
        for (size_t i = 0; i < action.params.size(); i++) {
            planning_failure_ptr->params[i] = action.params[i];
        }
    }

    return planning_failure_ptr;
}

/**
 * 
 * GENERATE RULES TO ADD EXCUSES
 * 
 */
Typed::RuleSet FailureAnalyzer::get_rules_to_add_planning_failures(const FailureCandidateList& planning_failure_candidates) const
{
    Typed::RuleSet extra_rules;

    for (FailureCandidateList::const_iterator candidate_it = planning_failure_candidates.begin(); candidate_it != planning_failure_candidates.end(); ++candidate_it) {
        Typed::RulePtr rule = create_planning_failure_change_predicate(candidate_it->second);
        extra_rules.push_back(rule);
    }

    return extra_rules;
}


Typed::RulePtr FailureAnalyzer::create_planning_failure_change_predicate(const FailureCandidate& failure_candidate) const
{
    std::stringstream rule_name;
    rule_name << EXCUSE_RULE_PREFIX_ADD;
    if (!failure_candidate.predicate.positive) {
        rule_name << EXCUSE_RULE_PREFIX_SEPARATOR << EXCUSE_RULE_PREFIX_NEGATE;
    }
    rule_name << EXCUSE_RULE_PREFIX_SEPARATOR << failure_candidate.predicate.name;
    BOOST_FOREACH(const PPDDL::FullObject& param, failure_candidate.predicate.params) {
        rule_name << EXCUSE_RULE_PREFIX_SEPARATOR << param.name;
    }
    TypedAtom symbolic_rule_action;
    // Planner PROST works with symbolic literals, but G-PACK works directly with the grounded representation
    // Note that it only works with all symbolic or all grounded params
    if ( (scenario.config_reader.get_variable<std::string>("planner") == PLANNER_PROST) && (failure_candidate.predicate.is_symbolic()) ){
        symbolic_rule_action = TypedAtom(rule_name.str(), failure_candidate.predicate.params);
    }
    else {
        symbolic_rule_action = TypedAtom(rule_name.str(), std::vector<std::string>(), std::vector<std::string>());
    }
    #if __cplusplus > 199711L // C++11
    Typed::RulePtr symbolic_rule = Typed::RulePtr(new Typed::SymbolicRule(symbolic_rule_action, PredicateGroup<TypedAtom>()));
    #else
    Typed::RulePtr symbolic_rule = Typed::RulePtr(boost::shared_ptr<Typed::Rule>(new Typed::SymbolicRule(symbolic_rule_action, PredicateGroup<TypedAtom>())));
    #endif
    
    PredicateGroup<TypedAtom> predicates;
    predicates.insert(failure_candidate.predicate);
    // uncomment line below to increase penalty reward in subsequent planning_failures
    if (config_duplicate_penalty_after_first_predicate) {
        predicates.insert(Predicate<TypedAtom>(MAKE_TYPED_ATOM_EXCUSE_GENERATED, true));
    }
    symbolic_rule->add_outcome(predicates, 1.0);
    
    // uncomment to limit to just 1 planning_failure
//     symbolic_rule->preconditions.push_back(Predicate<TypedAtom>(MAKE_TYPED_ATOM_EXCUSE_GENERATED, false));
    
    // Assign rewards to each rule
    PredicateGroup<TypedAtom> goal_preconditions;
    if (config_duplicate_penalty_after_first_predicate) {
        goal_preconditions.insert(Predicate<TypedAtom>(MAKE_TYPED_ATOM_EXCUSE_GENERATED, false));
    }
    Goal new_goal(goal_preconditions, failure_candidate.get_reward_penalty());
    symbolic_rule->goals.add_reward_function(new_goal);
    
    // uncomment for limiting number of planning_failures by increasing penalty reward in further iterations
    goal_preconditions.clear();
    if (config_duplicate_penalty_after_first_predicate) {
        goal_preconditions.insert(Predicate<TypedAtom>(MAKE_TYPED_ATOM_EXCUSE_GENERATED, true));
    }
    Goal new_goal2(goal_preconditions, failure_candidate.get_reward_penalty()*SECOND_EXCUSE_PENALTY_MULTIPLIER);
    symbolic_rule->goals.add_reward_function(new_goal2);
    
    return symbolic_rule;
}



/**
 * 
 * UTILS
 * 
 */



double FailureAnalyzer::get_reachable_probability(const Predicate<TypedAtom> predicate) const
{
    return std::min(grounded_rules.can_obtain_predicate(predicate), 1.0); // [0.0, 1.0]
}


bool FailureAnalyzer::check_valid_domain() const
{
    static const std::string INVALID_CHARACTERS[] = {"?", EXCUSE_RULE_PREFIX_ADD, EXCUSE_RULE_PREFIX_NEGATE, std::string(1,EXCUSE_RULE_PREFIX_SEPARATOR)};
    BOOST_FOREACH(const std::string& invalid_character, INVALID_CHARACTERS) {
        BOOST_FOREACH(const Predicate<TypedAtom>& predicate, scenario.domain.get_predicates()) {
            if (predicate.name.find(invalid_character) != std::string::npos) {
                LOG(WARNING) << "Found invalid symbol " << predicate << ". Character \"" << invalid_character << "\" is not supported.";
                return false;
            }
        }
        
        BOOST_FOREACH(const TypedAtom& action, scenario.domain.get_actions()) {
            if (!action.is_special() && (action.name.find(invalid_character) != std::string::npos)) {
                LOG(WARNING) << "Found invalid symbol " << action << ". Character \"" << invalid_character << "\" is not supported.";
                return false;
            }
        }
    }
    return true;
}

