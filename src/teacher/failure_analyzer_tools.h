/*
 * Class to find rule alternatives given a scenario and planning_failures.
 * Copyright (C) 2016  David Martínez (dmartinez@iri.upc.edu)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef INTELLACT_FAILURE_ANALYZER_TOOLS_H
#define INTELLACT_FAILURE_ANALYZER_TOOLS_H


#include <teacher/failure_analyzer.h>


/**
 * \class FailureAnalyzerUtils
 * \brief Extends FailureAnalyzer with rule alternatives and subgoals methods.
 * 
 * Generates subgoals and rule alternatives.
 */
class FailureAnalyzerUtils : public FailureAnalyzer {
public:
    FailureAnalyzerUtils(const FailureAnalyzer& failure_analyzer):
        FailureAnalyzer(failure_analyzer)
    {}
    
    std::vector<Typed::RuleSet> get_rules_that_overcome_planning_failure(const Typed::RuleSet rules, const PlanningFailureOpt& planning_failure_opt, const TransitionGroup<TypedAtom>& transitions) const;
    
private:
    Typed::RuleSet create_new_rule_candidates_with_planning_failure(const Typed::RulePtr rule, const PredicateGroup<TypedAtom> symbolic_planning_failures, const TransitionGroup<TypedAtom>& transitions) const;
};

#endif
