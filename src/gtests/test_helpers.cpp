#include "test_helpers.h"
#include <iostream>
#include <algorithm>
#include <boost/iostreams/device/mapped_file.hpp>
#include <gtest/gtest.h>


bool compare_two_files(const std::string& file1, const std::string& file2)
{
    boost::iostreams::mapped_file_source f1(file1);
    boost::iostreams::mapped_file_source f2(file2);

    if(    f1.size() == f2.size()
        && std::equal(f1.data(), f1.data() + f1.size(), f2.data())
       )
    {
//         std::cout << "The files are equal\n";
        return true;
    }
    else {
//         std::cout << "The files are not equal\n";
        return false;
    }
}

std::vector<std::string> get_actions_requiring_excuse(const Scenario& scenario, FailureAnalyzer& failure_analyzer)
{
    std::vector<std::string> actions_requiring_excuse;
    
    PlanningFailureOpt excuse_opt = failure_analyzer.get_planning_failure();
    if (excuse_opt) {
        Typed::RuleSet rules_requiring_excuse = scenario.get_rules().find_rules_with_a_precondition(*excuse_opt);
        rules_requiring_excuse.remove_nop_rules();
        actions_requiring_excuse = rules_requiring_excuse.get_action_names();
    }
    
    return actions_requiring_excuse;
}

ListPredicateGroup::ListPredicateGroup(std::string line, const PPDDLObjectManager<PPDDL::FullObject>& object_manager) 
{
    // remove leading and trailing spaces
    line = utils::trim(line);

    std::stringstream ss(line);

    std::string item;
    while (std::getline(ss, item, '|')) {
        item = utils::trim(item);

        if (item.size() > 1) { 
            PredicateGroup<NonTypedAtom> non_typed_preds(item);
            push_back(Predicates::transform_to_typed(non_typed_preds, object_manager));
        }
    }
}
    
bool ListPredicateGroup::has_predicate_group(const PredicateGroup<TypedAtom>& predicates) const
{
    bool result = false;
    
    BOOST_FOREACH(const PredicateGroup<TypedAtom>& predicates_it, *this) {
        if (predicates_it == predicates) {
            result = true;
        }
    }
    
    return result;
}


void test_excuses(const std::string& folder, const ListPredicateGroup& possible_expected_excuses, const std::string& action_containing_excuse)
{
        std::cout << std::endl;
        LOG(INFO) << "TEST: " << folder;
        // load config
        ConfigReader config_reader;
        config_reader.init(folder + "/rexd_config.cfg", folder);
        
        // load high_level_planner
        Scenario scenario = load_scenario(config_reader);
        FailureAnalyzer excuse_generator(scenario);
        PlanningFailureOpt excuse_opt = excuse_generator.get_planning_failure();
        std::vector<std::string> actions_requiring_excuse = get_actions_requiring_excuse(scenario, excuse_generator);
        LOG(INFO) << "Excuse: " << failure_analyzer_utils::planning_failure_to_string(excuse_opt);
        LOG(INFO) << "Actions requiring excuse: " << actions_requiring_excuse;
        
        // Check excuse
        EXPECT_EQ(excuse_opt.is_initialized(), true)
            << "No excuse found, when best excuse is " << possible_expected_excuses.front() << ". Test: " << folder;
        if (excuse_opt) {
            EXPECT_EQ(possible_expected_excuses.has_predicate_group(*excuse_opt), true)
                << "Obtained excuse " << failure_analyzer_utils::planning_failure_to_string(excuse_opt) << ", when best excuse is " << possible_expected_excuses.front() << ". Test: " << folder;
        }
        
        // Check actions required by excuse
        if (action_containing_excuse == "") {
            EXPECT_EQ(true, actions_requiring_excuse.empty())
                << "There should be no actions requiring excuse, instead of " << actions_requiring_excuse << ". Test: " << folder;
        }
        else {
            EXPECT_EQ(true, utils::belongs_to_vector<std::string>(action_containing_excuse, actions_requiring_excuse))
                << "Actions requiring excuse should have " << action_containing_excuse << ", but instead they are " << actions_requiring_excuse << ". Test: " << folder;
        }
}

void test_excuses(const FolderExcusePairs::value_type& folder_excuse)
{
    std::string test_folder_full = "excuse_tests/" + folder_excuse.first + "/";
    test_excuses(test_folder_full, folder_excuse.second.first, folder_excuse.second.second);
}

