/*
 * Tests based on google tests.
 * Copyright (C) 2016  David Martínez (dmartinez@iri.upc.edu)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

// external
#include <boost/assign/std/vector.hpp>
#include <boost/assign/list_inserter.hpp>
#include <boost/filesystem.hpp>

// rexd
#include "symbolic/predicates.h"
#include "symbolic/rules.h"
#include "symbolic/state.h"
#include "lfit/lfit_learner.h"
#include "rexd/rexd_scenario.h"
#include "rexd/pasula_learner.h"
#include <rexd/high_level_planner.h>

#include "apps/applications_helper.h"

// helpers
#include "test_helpers.h"


TEST(Predicates, Satisfying) { 
    PredicateGroup<NonTypedAtom> predicates1("plate(71) plate(72) plate(73) cup(74) onTable(71) onTable(72) onTable(73) onTable(74) clear(71) clear(72) clear(73) clear(74) -finished()");
    PredicateGroup<NonTypedAtom> predicates2("plate(71) plate(72) plate(73) cup(74) onTable(71) onTable(72) onTable(73) onTable(74) clear(71) clear(72) clear(73) clear(74)");
    PredicateGroup<NonTypedAtom> predicates3("plate(71) plate(72) plate(73) cup(74) onTable(71) onTable(72) onTable(73) onTable(74) clear(71) clear(72) clear(73) clear(74) finished()");
    PredicateGroup<NonTypedAtom> predicates4("plate(71) plate(72) plate(73) cup(74) onTable(71) onTable(72) onTable(73) onTable(74) clear(71) clear(72) clear(73) clear(74) -finished()");
    PredicateGroup<NonTypedAtom> predicates5("-broken(71) -broken(72) -broken(73) -broken(74) -broken(75) -broken(76) -broken(77) -onTable(71) -onTable(72) -onTable(73) -onTable(74) -onTable(75)");
    PredicateGroup<NonTypedAtom> predicates6("-broken(71) -broken(72) -broken(73) -broken(74) -broken(75) -broken(76) -broken(77)");
    PredicateGroup<NonTypedAtom> predicates7("-plate(71) plate(72) plate(73)");
    PredicateGroup<NonTypedAtom> predicates8;
    PredicateGroup<NonTypedAtom> predicates9("-plate(71) -plate(72) -plate(73) -cup(74) -onTable(71) -onTable(72) -onTable(73) -onTable(74) -clear(71) -clear(72) -clear(73) -clear(74) finished()");
    
    // get_negated()
    ASSERT_EQ( predicates9, predicates1.get_negated() );
    
    // satisfies
    ASSERT_EQ( true, predicates1.satisfies(*(PredicateGroup<NonTypedAtom>("plate(71)").begin())) );
    ASSERT_EQ( 0, predicates1.satisfies(*(PredicateGroup<NonTypedAtom>("-plate(71)").begin())) );
    ASSERT_EQ( 0, predicates1.satisfies(*(PredicateGroup<NonTypedAtom>("plate(74)").begin())) );
    
    // is_satisfied_by
    ASSERT_EQ( true,  predicates2.is_satisfied_by(predicates1) );
    ASSERT_EQ( 0, predicates3.is_satisfied_by(predicates1) );
    ASSERT_EQ( true,  predicates4.is_satisfied_by(predicates1) );
    ASSERT_EQ( 0, predicates5.is_satisfied_by(predicates1) );
    ASSERT_EQ( true,  predicates6.is_satisfied_by(predicates1) );
    ASSERT_EQ( 0, predicates7.is_satisfied_by(predicates1) );
    ASSERT_EQ( true,  predicates8.is_satisfied_by(predicates1) );
    
    ASSERT_EQ( 12, predicates2.count_satisfied_by(predicates1) );
    ASSERT_EQ( 12, predicates3.count_satisfied_by(predicates1) );
    ASSERT_EQ( 13, predicates4.count_satisfied_by(predicates1) );
    ASSERT_EQ( 8,  predicates5.count_satisfied_by(predicates1) );
    ASSERT_EQ( 7,  predicates6.count_satisfied_by(predicates1) );
    ASSERT_EQ( 2,  predicates7.count_satisfied_by(predicates1) );
    ASSERT_EQ( 0,  predicates8.count_satisfied_by(predicates1) );
    
    ASSERT_EQ( PredicateGroup<NonTypedAtom>(""),              predicates2.get_not_satisfied_by(predicates1) );
    ASSERT_EQ( PredicateGroup<NonTypedAtom>("finished()"),    predicates3.get_not_satisfied_by(predicates1) );
    ASSERT_EQ( PredicateGroup<NonTypedAtom>(""),              predicates4.get_not_satisfied_by(predicates1) );
    ASSERT_EQ( PredicateGroup<NonTypedAtom>("-onTable(71) -onTable(72) -onTable(73) -onTable(74)"), predicates5.get_not_satisfied_by(predicates1) );
    ASSERT_EQ( PredicateGroup<NonTypedAtom>(""),              predicates6.get_not_satisfied_by(predicates1) );
    ASSERT_EQ( PredicateGroup<NonTypedAtom>("-plate(71)"),    predicates7.get_not_satisfied_by(predicates1) );
    ASSERT_EQ( PredicateGroup<NonTypedAtom>(""),              predicates8.get_not_satisfied_by(predicates1) );
    
    ASSERT_EQ( predicates1,                             predicates8.get_differences(predicates1) );
    ASSERT_EQ( predicates1.get_negated(),    predicates1.get_differences(predicates8) );
    ASSERT_EQ( PredicateGroup<NonTypedAtom>("-plate(71) -cup(74) -onTable(71) -onTable(72) -onTable(73) -onTable(74) -clear(71) -clear(72) -clear(73) -clear(74) finished()"),  predicates1.get_differences(predicates7) );
}


TEST(Predicates, Logic) { 
    PredicateGroup<NonTypedAtom> predicates1("plate(71) plate(72) plate(73)");
    PredicateGroup<NonTypedAtom> predicates2("plate(71) cup(74) onTable(71)");
    PredicateGroup<NonTypedAtom> predicates3("plate(71) plate(72) plate(73)");
    PredicateGroup<NonTypedAtom> predicates4("plate(71) plate(72)");
    PredicateGroup<NonTypedAtom> predicates5("-plate(71) plate(72)");
    PredicateGroup<NonTypedAtom> predicates6;
    PredicateGroup<NonTypedAtom> predicates7("-plate(71)");
    PredicateGroup<NonTypedAtom> predicates8("plate(71) -fork(71)");
    std::vector<PredicateGroup<NonTypedAtom> > disjunction_of_preds;
    
    
    // set_and
    ASSERT_EQ( PredicateGroup<NonTypedAtom>("plate(71)"), predicates1.set_and(predicates2) );
    ASSERT_EQ( PredicateGroup<NonTypedAtom>("plate(71) plate(72) plate(73)"), predicates1.set_and(predicates3) );
    ASSERT_EQ( PredicateGroup<NonTypedAtom>("plate(71) plate(72)"), predicates1.set_and(predicates4) );
    ASSERT_EQ( PredicateGroup<NonTypedAtom>("plate(72)"), predicates1.set_and(predicates5) );
    ASSERT_EQ( PredicateGroup<NonTypedAtom>(), predicates1.set_and(predicates6) );
    ASSERT_EQ( PredicateGroup<NonTypedAtom>("-plate(71)"), predicates5.set_and(predicates7) );
    ASSERT_EQ( PredicateGroup<NonTypedAtom>(), predicates5.set_and(predicates8) );
    
    
    // get_logical_contains
    ASSERT_EQ( true,  predicates1.includes_predicates(predicates1) );
    ASSERT_EQ( false, predicates2.includes_predicates(predicates1) );
    ASSERT_EQ( true,  predicates3.includes_predicates(predicates1) );
    ASSERT_EQ( false, predicates4.includes_predicates(predicates1) );
    ASSERT_EQ( true,  predicates1.includes_predicates(predicates4) );
    ASSERT_EQ( false, predicates4.includes_predicates(predicates5) );
    ASSERT_EQ( false, predicates6.includes_predicates(predicates5) );
    ASSERT_EQ( true,  predicates5.includes_predicates(predicates6) );
    ASSERT_EQ( false, predicates6.includes_predicates(predicates7) );
    ASSERT_EQ( true,  predicates7.includes_predicates(predicates6) );
}


TEST(ExploreTest, CheckKnown)
{
    utils::init_loggers("generic/logger.conf");
    
    using namespace boost::assign;
    typedef std::map<std::string, bool> FolderActionBoolPairs;
    FolderActionBoolPairs test_folders; 
    insert( test_folders )
        ( "empty_with_actions", false )
        ( "1action_unknown"   , false )
        ( "almost_explored1"  , false )
        ( "almost_explored2"  , false )
        ( "almost_explored3"  , false )
        ( "almost_explored4"  , false )
        ( "almost_explored5"  , false )
        ( "empty_known"       , true  )
        ( "explored1"         , true  )
        ( "explored2"         , true  )
        ( "lfit_1action_exp"  , true  )
        ( "lfit_1action_unexp", false )
        ( "lfit_explored1"    , true  )
        ( "lfit_unexplored1"  , false )
        ;
    
    BOOST_FOREACH(FolderActionBoolPairs::value_type folder_action, test_folders) {
        LOG(INFO) << "TEST: " << folder_action.first;
        
        ConfigReader config_reader = load_config_reader("exploration_tests/" + folder_action.first + "/");
        Scenario scenario = load_scenario(config_reader);
        TransitionGroup<TypedAtom> transitions = load_transitions(config_reader, scenario);
        
        // load high_level_planner
        std::ofstream logging_file;
        logging_file.open ("/dev/null", std::ios::app);
        HighLevelPlanner high_level_planner(config_reader, &logging_file);
        PredicateGroup<TypedAtom> excuse;
        std::vector<std::string> actions_requiring_excuse;
        
        Explorer explorer;
        EXPECT_EQ(folder_action.second, explorer.is_state_known(scenario, transitions))
            << "State should be known. Test: " << folder_action.first;
        
        // test exploration without forced flag
        TypedAtomOpt explore_action = high_level_planner.explore(scenario, transitions, false);
        EXPECT_EQ(folder_action.second, !explore_action.is_initialized())
            << "State should be known (no action). Test: " << folder_action.first;
    }
}

TEST(ExploreTest, ActionSelection) 
{
    utils::init_loggers("generic/logger.conf");
    
    using namespace boost::assign;
    typedef std::map<std::string, TypedAtomOpt> FolderActionPairs;
    FolderActionPairs test_folders; 
    insert( test_folders )
        ( "1action_unknown"    , TypedAtomOpt(TypedAtom("putPlateOn(?X - plate ?Y - plate)")) )    
        ( "empty_with_actions" , TypedAtomOpt(TypedAtom("putPlateOn(?X - plate ?Y - plate)")) )    
        ( "almost_explored1"   , TypedAtomOpt(TypedAtom("putCupOn(?X - cup ?Y - plate)"))     )
        ( "almost_explored2"   , TypedAtomOpt(TypedAtom("putCupOn(?X - cup ?Y - plate)"))     )
        ( "almost_explored3"   , TypedAtomOpt(TypedAtom("removeCup(?X - cup ?Y - plate)"))    )
        ( "almost_explored4"   , TypedAtomOpt(TypedAtom("putPlateOn(?X - plate ?Y - cup)"))   )
        ( "almost_explored5"   , TypedAtomOpt(TypedAtom("removeCup(?X - cup ?Y - plate)"))    )
        ( "almost_explored6"   , TypedAtomOpt(TypedAtom("putPlateOn(?X - plate ?Y - cup)"))   )
        ( "empty_known"        , TypedAtomOpt()  )
        ( "explored1"          , TypedAtomOpt()  )
        ( "explored2"          , TypedAtomOpt()  )
        ( "lfit_1action_exp"   , TypedAtomOpt()  )
        ( "lfit_1action_unexp" , TypedAtomOpt(TypedAtom("move-west()"))                       )
        ( "lfit_explored1"     , TypedAtomOpt()                                               )
        ( "lfit_unexplored1"   , TypedAtomOpt(TypedAtom("move-east()"))                       )
        ;
    
    BOOST_FOREACH(FolderActionPairs::value_type folder_action, test_folders) {
        LOG(INFO) << "TEST: " << folder_action.first;
        
        ConfigReader config_reader = load_config_reader("exploration_tests/" + folder_action.first + "/");
        Scenario scenario = load_scenario(config_reader);
        TransitionGroup<TypedAtom> transitions = load_transitions(config_reader, scenario);
        
        // load high_level_planner
        std::ofstream logging_file;
        logging_file.open ("/dev/null", std::ios::app);
        HighLevelPlanner high_level_planner(config_reader, &logging_file);
        PlanningFailureOpt excuse_opt;
        std::vector<std::string> actions_requiring_excuse;
        
        // test exploration without forced flag
        TypedAtomOpt explore_action = high_level_planner.explore(scenario, transitions, false);
        EXPECT_EQ(folder_action.second.is_initialized(), explore_action.is_initialized())
                << "A exploration action should have been selected. Test: " << folder_action.first;
        if (explore_action.is_initialized()) {
            EXPECT_EQ(true, folder_action.second->symbolic_compare(*explore_action))
                << "Wrong action selected for exploration. Selected " << *explore_action << " instead of " << *folder_action.second << ". Test: " << folder_action.first;
        }
    }
}

TEST(ExploreTest, ActionSelectionForced) 
{
    utils::init_loggers("generic/logger.conf");
    
    using namespace boost::assign;
    typedef std::map<std::string, TypedAtomOpt> FolderActionPairs;
    FolderActionPairs test_folders; 
    insert( test_folders )
        ( "1action_unknown"    , TypedAtomOpt(TypedAtom("putPlateOn(?X - plate ?Y - plate)")) )    
        ( "empty_with_actions" , TypedAtomOpt(TypedAtom("putPlateOn(?X - plate ?Y - plate)")) )    
        ( "almost_explored1"   , TypedAtomOpt(TypedAtom("putCupOn(?X - cup ?Y - plate)"))     )
        ( "almost_explored2"   , TypedAtomOpt(TypedAtom("putCupOn(?X - cup ?Y - plate)"))     )
        ( "almost_explored3"   , TypedAtomOpt(TypedAtom("removeCup(?X - cup ?Y - plate)"))    )
        ( "almost_explored4"   , TypedAtomOpt(TypedAtom("putPlateOn(?X - plate ?Y - cup)"))   )
        ( "almost_explored5"   , TypedAtomOpt(TypedAtom("removeCup(?X - cup ?Y - plate)"))    )
        ( "almost_explored6"   , TypedAtomOpt(TypedAtom("putPlateOn(?X - plate ?Y - cup)"))   )
        ( "empty_known"        , TypedAtomOpt()                                               )
        ( "explored1"          , TypedAtomOpt(TypedAtom("putCupOn(?X - cup ?Y - plate)"))     )
        ( "explored2"          , TypedAtomOpt(TypedAtom("putPlateOn(?X - plate ?Y - plate)")) )
        ( "lfit_1action_exp"   , TypedAtomOpt(TypedAtom("move-west()"))                       )
        ( "lfit_1action_unexp" , TypedAtomOpt(TypedAtom("move-west()"))                       )
        ( "lfit_explored1"     , TypedAtomOpt(TypedAtom("move-east()"))                                               )
        ( "lfit_unexplored1"   , TypedAtomOpt(TypedAtom("move-east()"))                       )
        ;
    
    BOOST_FOREACH(FolderActionPairs::value_type folder_action, test_folders) {
        LOG(INFO) << "TEST: " << folder_action.first;
        
        ConfigReader config_reader = load_config_reader("exploration_tests/" + folder_action.first + "/");
        Scenario scenario = load_scenario(config_reader);
        TransitionGroup<TypedAtom> transitions = load_transitions(config_reader, scenario);
        
        // load high_level_planner
        std::ofstream logging_file;
        logging_file.open ("/dev/null", std::ios::app);
        HighLevelPlanner high_level_planner(config_reader, &logging_file);
        PlanningFailureOpt excuse_opt;
        std::vector<std::string> actions_requiring_excuse;
        
        // test exploration with forced flag
        TypedAtomOpt explore_action = high_level_planner.explore(scenario, transitions, true);
        EXPECT_EQ(folder_action.second.is_initialized(), explore_action.is_initialized())
                << "A exploration action should have been selected. Test: " << folder_action.first;
        if (explore_action.is_initialized()) {
            EXPECT_EQ(true, folder_action.second->symbolic_compare(*explore_action))
                << "Selected " << *explore_action << " instead of " << *folder_action.second << ". Test: " << folder_action.first;
        }
    }
}

TEST(ExploreTest, ActionSelectionRMAX) 
{
    utils::init_loggers("generic/logger.conf");
    
    using namespace boost::assign;
    typedef std::map<std::string, TypedAtomOpt> FolderActionPairs;
    FolderActionPairs test_folders; 
    insert( test_folders )
        ( "1action_unknown"    , TypedAtomOpt(TypedAtom("putPlateOn(?X - plate ?Y - plate)")) )
        ( "empty_with_actions" , TypedAtomOpt(TypedAtom("putPlateOn(?X - plate ?Y - plate)")) )
        ( "almost_explored1"   , TypedAtomOpt(TypedAtom("putCupOn(?X - cup ?Y - plate)"))     )
        ( "almost_explored2"   , TypedAtomOpt(TypedAtom("putCupOn(?X - cup ?Y - plate)"))     )
        ( "almost_explored3"   , TypedAtomOpt(TypedAtom("removeCup(?X - cup ?Y - plate)"))    )
        ( "almost_explored4"   , TypedAtomOpt(TypedAtom("putPlateOn(?X - plate ?Y - cup)"))   )
//         ( "almost_explored5"   , TypedAtomOpt(TypedAtom("removeCup(?X - cup ?Y - plate)"))    ) // putPlateOn(?X - plate ?Y - cup) is also valid (R-MAX can select both)
//         ( "almost_explored6"   , TypedAtomOpt(TypedAtom("putPlateOn(?X - plate ?Y - cup)"))   ) // removeCup(?X - cup ?Y - plate) is also valid (R-MAX can select both)
        ( "empty_known"        , TypedAtomOpt()                                               )
        ( "explored1"          , TypedAtomOpt(TypedAtom("finish_action()"))                   )
        ( "explored2"          , TypedAtomOpt(TypedAtom("putPlateOn(?X - plate ?Y - plate)")) )
//         ( "lfit_1action_exp"   , TypedAtomOpt()                                               ) // E3
//         ( "lfit_1action_unexp" , TypedAtomOpt(TypedAtom("move-west()"))                       ) // E3
//         ( "lfit_explored1"     , TypedAtomOpt()                                               ) // E3
//         ( "lfit_unexplored1"   , TypedAtomOpt(TypedAtom("move-east()"))                       ) // E3
        ( "lfit_explored2"          , TypedAtomOpt(TypedAtom("move-north()")) )
        ;
    
    BOOST_FOREACH(FolderActionPairs::value_type folder_action, test_folders) {
        LOG(INFO) << "TEST: " << folder_action.first;
        
        ConfigReader config_reader = load_config_reader("exploration_tests/" + folder_action.first + "/");
        Scenario scenario = load_scenario(config_reader);
        TransitionGroup<TypedAtom> transitions = load_transitions(config_reader, scenario);
        
        // load high_level_planner
        std::ofstream logging_file;
        logging_file.open ("/dev/null", std::ios::app);
        HighLevelPlanner high_level_planner(config_reader, &logging_file);
        PlanningFailureOpt excuse_opt;
        std::vector<std::string> actions_requiring_excuse;
        
        // test exploration in E3 exploration/exploitation algorithm
        TypedAtomOpt explore_action = high_level_planner.exploit_or_explore(scenario, transitions, excuse_opt, actions_requiring_excuse);
        EXPECT_EQ(folder_action.second.is_initialized(), explore_action.is_initialized())
                << "A exploration action should have been selected. Test: " << folder_action.first;
        if (folder_action.second.is_initialized()) {
            EXPECT_EQ(true, folder_action.second->symbolic_compare(*explore_action))
                << "Selected " << *explore_action << " instead of " << *folder_action.second << ". Test: " << folder_action.first;
        }
        else {
            
        }
    }
}


TEST(LearnerTest, LFITFull) 
{
    utils::init_loggers("generic/logger.conf");
    
    std::vector<std::string> test_folders = {"crossing_p1_t10",
                                             "crossing_p1rl_1",
                                             "crossing_p3rl_1",
                                             "crossing_p3rl_2",
                                             "triangle_p1lfit_t15",
                                             "triangle_p1rl_1",
                                             "triangle_p1rl_2",
                                             "triangle_p2rl_1"
    };
    
    BOOST_FOREACH(const std::string& test_folder, test_folders) {
        LOG(INFO) << "TEST: " << test_folder;
        
        std::string domain_folder = "learner_tests/" + test_folder + "/";
        ConfigReader config_reader = load_config_reader(domain_folder);
        Scenario scenario = load_scenario(config_reader);
        TransitionGroup<TypedAtom> transitions = load_transitions(config_reader, scenario);
        
        LearnerPtr learner_ptr = boost::make_shared<LFITLearner>(LFITLearner(config_reader));
        Typed::RuleSet learned_rules = learner_ptr->learn(transitions, Typed::RuleSet());
        scenario.set_rules(learned_rules);
        scenario.write_PDDL_domain(domain_folder + "learned_rules.ppddl");
        
        bool learned_rules_are_good = compare_two_files(domain_folder + "learned_rules.ppddl", domain_folder + "ground_truth_rules.ppddl");
        if (!learned_rules_are_good) { // try alternative good ruleset if exists
            if ( boost::filesystem::exists( domain_folder + "ground_truth_rules2.ppddl" ) ) {
                learned_rules_are_good = compare_two_files(domain_folder + "learned_rules.ppddl", domain_folder + "ground_truth_rules2.ppddl");
            }
        }
        if (!learned_rules_are_good) { // try alternative good ruleset if exists
            if ( boost::filesystem::exists( domain_folder + "ground_truth_rules3.ppddl" ) ) {
                learned_rules_are_good = compare_two_files(domain_folder + "learned_rules.ppddl", domain_folder + "ground_truth_rules3.ppddl");
            }
        }
        EXPECT_EQ(true, learned_rules_are_good)
                << "Unexpected rule set was learnt in " << test_folder << "!";
    }
}


TEST(LearnerTest, LFITLast) 
{
    utils::init_loggers("generic/logger.conf");
    
    std::vector<std::string> test_folders = {"crossing_p1_t10",
                                             "crossing_p1rl_1",
                                             "crossing_p3rl_1",
                                             "crossing_p3rl_2",
                                             "triangle_p1lfit_t15",
                                             "triangle_p1rl_1",
                                             "triangle_p1rl_2",
                                             "triangle_p2rl_1",
                                             "elevators_p1_t15_elevatorAtFloor",
                                             "elevators_p1_t15_personWaitingUp",
                                             "elevators_p1_t15_all",
                                             "elevators_p1_t25"};
    
    BOOST_FOREACH(const std::string& test_folder, test_folders) {
        LOG(INFO) << "TEST: " << test_folder;
        
        std::string domain_folder = "learner_tests/" + test_folder + "/";
        ConfigReader config_reader = load_config_reader(domain_folder);
        Scenario scenario = load_scenario(config_reader);
        TransitionGroup<TypedAtom> transitions = load_transitions(config_reader, scenario);
        TransitionGroup<TypedAtom> normalized_transitions = transitions.get_normalized_transitions();
        
        ReaderLFIT reader_lfit(config_reader.get_path("lfit_rules_path"));
        Typed::RuleSet learned_rules;
        reader_lfit.read_ruleset(learned_rules, 
                                 normalized_transitions, 
                                 config_reader);
        
        scenario.set_rules(learned_rules);
        scenario.write_PDDL_domain(domain_folder + "learned_rules.ppddl");
        
        bool learned_rules_are_good = compare_two_files(domain_folder + "learned_rules.ppddl", domain_folder + "ground_truth_rules.ppddl");
        if (!learned_rules_are_good) { // try alternative good ruleset if exists
            if ( boost::filesystem::exists( domain_folder + "ground_truth_rules2.ppddl" ) ) {
                learned_rules_are_good = compare_two_files(domain_folder + "learned_rules.ppddl", domain_folder + "ground_truth_rules2.ppddl");
            }
        }
        if (!learned_rules_are_good) { // try alternative good ruleset if exists
            if ( boost::filesystem::exists( domain_folder + "ground_truth_rules3.ppddl" ) ) {
                learned_rules_are_good = compare_two_files(domain_folder + "learned_rules.ppddl", domain_folder + "ground_truth_rules3.ppddl");
            }
        }
        EXPECT_EQ(true, learned_rules_are_good)
                << "Unexpected rule set was learnt in " << test_folder << "!";
    }
}


TEST(PlannerTest, PROST) 
{
    utils::init_loggers("generic/logger.conf");
    
    using namespace boost::assign;
    typedef std::map< std::string, std::string > MapFolderResult;
    MapFolderResult test_folders; 
    insert( test_folders )
        ( "crossing_p1RL"  , "move-west()" )
        ( "elevators_p1RL" , "closeDoor(e0 - elevator)" )
        ( "triangle_p1lfitRL" , "moveCar(l11 - location l21 - location)" )
        ;
    
    BOOST_FOREACH(const MapFolderResult::value_type& test_folder, test_folders) {
        LOG(INFO) << "TEST: " << test_folder.first;
        
        std::string domain_folder = "planner_tests/" + test_folder.first + "/";
        ConfigReader config_reader = load_config_reader(domain_folder);
        Scenario scenario = load_scenario(config_reader);
        float expected_value;
        PlannerPtr planner_ptr = PlannerFactory::get_standard_planner(config_reader);
        TypedAtomOpt action_opt =  planner_ptr->plan(scenario, true, 0, expected_value);
        
        EXPECT_EQ(true, action_opt.is_initialized())
                << "PROST planner failed!";
        if (action_opt.is_initialized()) {
            EXPECT_EQ(TypedAtom(test_folder.second), *action_opt)
                    << "Unexpected rule set was planned!";
        }
    }
}


TEST(PlannerTest, GPack) 
{
    utils::init_loggers("generic/logger.conf");
    
    using namespace boost::assign;
    typedef std::map< std::string, std::string > MapFolderResult;
    MapFolderResult test_folders; 
    insert( test_folders )
        ( "triangle_p1"              , "moveCar(l11 - location l21 - location)" )
        ( "triangle_p1_vmin"         , "moveCar(l11 - location l21 - location)" );
    
    BOOST_FOREACH(const MapFolderResult::value_type& test_folder, test_folders) {
        LOG(INFO) << "TEST: " << test_folder.first;
        
        std::string domain_folder = "planner_tests/" + test_folder.first + "/";
        ConfigReader config_reader = load_config_reader(domain_folder);
        Scenario scenario = load_scenario(config_reader);
        float expected_value;
        PlannerPtr planner_ptr = PlannerFactory::get_standard_planner(config_reader);
        TypedAtomOpt action_opt =  planner_ptr->plan(scenario, true, 0, expected_value);
        
        EXPECT_EQ(true, action_opt.is_initialized())
                << "PROST planner failed!";
        EXPECT_EQ(TypedAtom(test_folder.second), *action_opt)
                << "Unexpected rule set was planned!";
    }
}


TEST(ExcuseTest, GenerateExcusesAutas) {
    utils::init_loggers("generic/logger.conf");
    using namespace boost::assign;
    PPDDLObjectManager<PPDDL::FullObject> object_manager;
    FolderExcusePairs test_folders; 
    insert( test_folders )
        ( "autas_p06_1"   , ExcuseActionPair(ListPredicateGroup("freeFbHb1()", object_manager), "placependulum")     )
        ( "autas_p06_2"   , ExcuseActionPair(ListPredicateGroup("freeFbHb2()", object_manager), "screwpendulumhead") )
        ( "autas_horiz_2" , ExcuseActionPair(ListPredicateGroup("clearB1()", object_manager)  , "placepeg1") 
        );
    
    BOOST_FOREACH(FolderExcusePairs::value_type folder_excuse, test_folders) {
        test_excuses(folder_excuse);
    }
}

TEST(ExcuseTest, GenerateExcusesExBlock) {
    utils::init_loggers("generic/logger.conf");
    using namespace boost::assign;
    
    // get ppddl_object_manager
    PPDDLObjectManager<PPDDL::FullObject> object_manager;
    object_manager.add_object(PPDDL::FullObject("63", "default"));
    object_manager.add_object(PPDDL::FullObject("65", "default"));
    object_manager.add_object(PPDDL::FullObject("66", "default"));
    
    FolderExcusePairs test_folders; 
    insert( test_folders )
        ( "exblock_p5_1" , ExcuseActionPair(ListPredicateGroup("on(63, 66)", object_manager),          "")           )
        ( "exblock_p5_2" , ExcuseActionPair(ListPredicateGroup("on(63, 66) on(65,63)", object_manager), "")           )
        ( "exblock_p5_3" , ExcuseActionPair(ListPredicateGroup("-onTable(66)", object_manager),        "putOnBlock") )
        ( "exblock_p5_4" , ExcuseActionPair(ListPredicateGroup("-onTable(66)", object_manager),        "putOnBlock") /*)
        ( "exblock_dangerous_1" , ExcuseActionPair(ListPredicateGroup("noDestroyedTable()"), "putDown")*/
        );
    
    BOOST_FOREACH(FolderExcusePairs::value_type folder_excuse, test_folders) {
        test_excuses(folder_excuse);
    }
}

TEST(ExcuseTest, GenerateExcusesKitchen) {
    utils::init_loggers("generic/logger.conf");
    using namespace boost::assign;
    // get ppddl_object_manager
    PPDDLObjectManager<PPDDL::FullObject> object_manager;
    object_manager.add_object(PPDDL::FullObject("71", "default"));
    object_manager.add_object(PPDDL::FullObject("72", "default"));
    object_manager.add_object(PPDDL::FullObject("73", "default"));
    object_manager.add_object(PPDDL::FullObject("74", "default"));
    object_manager.add_object(PPDDL::FullObject("75", "default"));
    
    FolderExcusePairs test_folders; 
    insert( test_folders )
//         ( "kitchen_p1_1", ExcuseActionPair(ListPredicateGroup("-onTable(71) -onTable(72) -onTable(73) -onTable(74)", object_manager),               "")           )
        ( "kitchen_p1_2", ExcuseActionPair(ListPredicateGroup("-broken(71) -cup(74) clear(73)", object_manager),                                    "putPlateOn") )             
        ( "kitchen_p1_3", ExcuseActionPair(ListPredicateGroup("clear(71) -onTable(74)", object_manager),                                            "putPlateOn") )
        ( "kitchen_p1_4", ExcuseActionPair(ListPredicateGroup("clear(71) | -cup(74)", object_manager),                                              "putPlateOn") )
        ( "kitchen_p1_5", ExcuseActionPair(ListPredicateGroup("clear(71) | clear(72) | cup(71) | cup(72)", object_manager),                         "putPlateOn") ) 
        ( "kitchen_p1_6", ExcuseActionPair(ListPredicateGroup("-broken(73) cup(71) | -broken(73) clear(71)", object_manager),                       "putPlateOn") )
//         ( "kitchen_p1_7", ExcuseActionPair(ListPredicateGroup("-broken(75) cup(71) | -broken(75) cup(73) | -broken(75) clear(73)", object_manager), "putPlateOn") )
//         ( "kitchen_p1_8", ExcuseActionPair(ListPredicateGroup("clear(71) | clear(72)", object_manager),                                             "putPlateOn") ) 
//         ( "kitchen_p1_9", ExcuseActionPair(ListPredicateGroup("plate(74)", object_manager),                                                         "putPlateOn") )
        ;
    
    BOOST_FOREACH(FolderExcusePairs::value_type folder_excuse, test_folders) {
        test_excuses(folder_excuse);
    }
}

TEST(ExcuseTest, GenerateExcusesTriangle) {
    utils::init_loggers("generic/logger.conf");
    using namespace boost::assign;
    // get ppddl_object_manager
    PPDDLObjectManager<PPDDL::FullObject> object_manager;
    object_manager.add_object(PPDDL::FullObject("72", "default"));
    object_manager.add_object(PPDDL::FullObject("75", "default"));
    
    FolderExcusePairs test_folders; 
    insert( test_folders )
        ( "triangle_p1_1"    , ExcuseActionPair(ListPredicateGroup("notFlattire()", object_manager), "moveCar") )
        ( "triangle_p1_2"    , ExcuseActionPair(ListPredicateGroup("notFlattire() | notFlattire() vehicleAt(72) | notFlattire() vehicleAt(82)", object_manager), "moveCar") )
        ( "triangle_p1_3"    , ExcuseActionPair(ListPredicateGroup("notFlattire() | notFlattire() vehicleAt(72) | notFlattire() vehicleAt(82)", object_manager), "moveCar") )
        ( "triangle_p1_4"    , ExcuseActionPair(ListPredicateGroup("spareIn(72)()", object_manager), "loadtire") )
        
//         ( "triangle_p1vmin_1", ExcuseActionPair(ListPredicateGroup("vehicleAt(75)", object_manager), "") )
        ( "triangle_p1vmin_2", ExcuseActionPair(ListPredicateGroup("spareIn(75)", object_manager),   "moveCar") )
        ( "triangle_p1vmin_3", ExcuseActionPair(ListPredicateGroup("notFlattire()", object_manager), "moveCar") )
        ( "triangle_p1vmin_4", ExcuseActionPair(ListPredicateGroup("notFlattire()", object_manager), "moveCar") )
        ( "triangle_p1vmin_5", ExcuseActionPair(ListPredicateGroup("notFlattire()", object_manager), "moveCar") )
//         ( "triangle_p1vmin_6", ExcuseActionPair(ListPredicateGroup("spareIn(72)", object_manager),   "loadtire") )
        ;
    
    BOOST_FOREACH(FolderExcusePairs::value_type folder_excuse, test_folders) {
        test_excuses(folder_excuse);
    }
}
 
int main(int argc, char **argv) {
    // argv parsed by google tests
    // Remember: --gtest_filter=*Excuse* to execute just excuse tests
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}