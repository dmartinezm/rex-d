/*
 * Tests helpers
 * Copyright (C) 2015  David Martínez (dmartinez@iri.upc.edu)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef IRI_TEST_HELPERS_H
#define IRI_TEST_HELPERS_H 1


#include <iostream>
#include <algorithm>
#include <boost/iostreams/device/mapped_file.hpp>

#include <gtest/gtest.h>

#include <symbolic/scenario.h>
#include <teacher/failure_analyzer.h>

#include "apps/applications_helper.h"

/** \brief Compares the content of two files */
bool compare_two_files(const std::string& file1, const std::string& file2);

/** \brief Get the actions that require the changes done by an excuse */
std::vector<std::string> get_actions_requiring_excuse(const Scenario& scenario, FailureAnalyzer& failure_analyzer);

/** \brief Class that stores a list of PredicateGroup and checks if any of them is equal to the given one */
class ListPredicateGroup : public std::vector<PredicateGroup<TypedAtom> > {
public:
    ListPredicateGroup(std::string line, const PPDDLObjectManager<PPDDL::FullObject>& object_manager);
    
    bool has_predicate_group(const PredicateGroup<TypedAtom>& predicates) const;
};

typedef std::pair<ListPredicateGroup, std::string> ExcuseActionPair;
typedef std::map<std::string, ExcuseActionPair> FolderExcusePairs;


void test_excuses(const FolderExcusePairs::value_type& folder_excuse);
void test_excuses(const std::string& folder, const ListPredicateGroup& possible_expected_excuses, const std::string& action_containing_excuse = "");


#endif
