/*
 * LFIT probabilistic postprocessing
 * Copyright (C) 2015  David Martínez <dmartinez@iri.upc.edu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef REXD_PROBABILISTIC_LFIT_TEMPLATE_IMP_H
#define REXD_PROBABILISTIC_LFIT_TEMPLATE_IMP_H 1

// only for debug purposes
// #define IGNORE_NEGATIVE_RULES
// #define IGNORE_POSITIVE_RULES

#include "probabilistic_lfit.h"
#include <boost/graph/graph_concepts.hpp>

using namespace lfit;


template<typename AtomT>
RuleWithCoverageSet<AtomT> ProbabilisticLFITAnalyzer::select_best_ruleset(const RuleWithCoverageSet<AtomT>& current_candidates,
                                                                   const TransitionGroup<AtomT>& head_transitions,
                                                                   const int verbose
                                                                  )
{
    RuleConflictSolver<AtomT> conflict_solver(current_candidates, head_transitions, config_reader);
    return conflict_solver.get_best_subset(head_transitions, config_reader, config_reader.get_variable<bool>("lfit_learner_optimal"), verbose);
}

template<typename AtomT>
bool ProbabilisticLFITAnalyzer::remove_conflicting_leafs(const TransitionGroup<AtomT>& head_transitions, const RuleWithCoverageSet<AtomT>& rules_to_check, 
                                                         RuleSubsumptionGraph<AtomT>& subsumption_graph, 
                                                         RuleWithCoverageSet<AtomT>& best_operators, double& best_candidate_score)
{
    CLOG_IF(debug >=3, INFO, "trivial") << "Conflicting rules size is " << rules_to_check.size();
    RuleWithCoverageSet<AtomT> current_best_ruleset = select_best_ruleset(rules_to_check, head_transitions, debug);
    CLOG_IF(debug >=3, INFO, "trivial") << "Finished planning operator selection.";
    double new_score = current_best_ruleset.get_score(head_transitions, config_reader);
    CLOG_IF(debug >=4, INFO, "trivial") << "New score is: " << new_score;
    if (new_score > best_candidate_score) {
        best_candidate_score = new_score;
        best_operators = current_best_ruleset;
        if (debug >=2) {
            CLOG_IF(debug>=3, INFO, "trivial") << "---------";
            CLOG_IF(debug >=2, INFO, "trivial") << "New best candidate (1) with score: " << new_score;
            best_operators.get_score(head_transitions, config_reader, debug);
        }
    }
    return subsumption_graph.remove_leafs_not_in(current_best_ruleset);
}

template<typename AtomT>
RuleWithCoverageSet<AtomT> ProbabilisticLFITAnalyzer::get_promising_candidates(const RuleWithCoverageSet<AtomT>& head_rules,
                                                                               const TransitionGroup<AtomT>& head_transitions, 
                                                                               const bool all_values)
{
    RuleWithCoverageSet<AtomT> promising_head_rules;
    BOOST_FOREACH (const RuleWithCoverage<AtomT>& head_rule, head_rules) {
        // Covers at least one transition
        const TransitionsChangesCoverage& transitions_coverage = head_rule.get_covered_transitions_full(head_transitions);
        bool transition_covered = false;
        size_t i = 0;
        while ( (!transition_covered) && (i < transitions_coverage.size()) ) {
            if ( (transitions_coverage[i].size() > 0) &&
                 (transitions_coverage[i].get_likelihood(false) > std::numeric_limits<float>::epsilon())) {
                transition_covered = true;
            }
            ++i;
        }
        if (transition_covered) {
            typename RuleWithCoverage<AtomT>::Ptr new_head_rule_ptr;
            if ( all_values || utils::is_close(head_rule.rule_ptr->outcomes.begin()->probability,(float)1.0) ) {
                new_head_rule_ptr = boost::make_shared< RuleWithCoverage<AtomT> >(head_rule);
            }
            else {
                /* If not testing all Atom values at the same time
                 * Then set as precondition the head negated
                 * Otherwise it may intefere with the opposite head
                 * Example: 
                 *   a -> -b, and c -> b may make opposite changes b and -b in the same preconditions (a ^ b)
                 *   A change that goes -b -> b only gets tested with positive b head, so only the rule c -> b is tested
                 *   The rule a -> -b only gets tested in changes b -> -b, so if it works on them it would be selected
                 *   We add the precondition b to make sure this problem doesn't arise
                 */
                RulePtr<AtomT> new_rule_ptr = head_rule.rule_ptr->make_shared();
                PredicateGroup<AtomT> precs = new_rule_ptr->get_preconditions();
                precs.insert(head_rule.rule_ptr->outcomes.begin()->begin()->get_negated());
                new_rule_ptr->set_preconditions(precs);
                //new_rule_ptr->update_outcome_probabilities(head_transitions);
                new_head_rule_ptr = typename RuleWithCoverage<AtomT>::Ptr(new RuleWithCoverage<AtomT>(new_rule_ptr, head_rule.coverage));
            }
            
            RuleWithCoverageSet<AtomT> candidate_head_rule_set;
            candidate_head_rule_set.insert(*new_head_rule_ptr);
            /* min optimistic score is:
            * For each effect, the max error should be p=0.5 -> ln(0.5)
            * For each precondition, add the penalty
            * min_opt_score = ln(0.5) * MAX_NUM_CHANGES + MAX_PRECONDITIONS * PENALTY_PRECONDITION
            * min_opt_score = -0.69   *       2         +          10        *         0.05
            * Using min_opt_score = -2 as a safe value
            */
            float rule_optimistic_score = candidate_head_rule_set.get_score(head_transitions, config_reader, 0, config_reader.get_variable<float>("lfit_learner_score_optimistic_value"));
            if (rule_optimistic_score > -2.0) { // Arbitrary min optimistic value. Each effect, max 0.5 prob error -> ln(0.5) = -0.69
                promising_head_rules.insert(*new_head_rule_ptr);
            }
        }
    }
    return promising_head_rules;
}

template<typename AtomT>
RuleWithCoverageSet<AtomT> ProbabilisticLFITAnalyzer::select_rules_for_head(const RuleWithCoverageSet<AtomT>& head_rules,
                                                                            const TransitionGroup<AtomT>& head_transitions, 
                                                                            const bool all_values)
{
    CLOG_IF(debug >=1, INFO, "trivial") << "Selecting rules from " << head_rules.size() << " candidates.";
    Typed::RuleSet result;
    
    CLOG_IF(debug >=1, INFO, "trivial") << "Removing potentially bad candidates.";
    RuleWithCoverageSet<AtomT> promising_head_rules = get_promising_candidates(head_rules, head_transitions, all_values);
    CLOG_IF(debug >=1, INFO, "trivial") << "Selecting rules from " << promising_head_rules.size() << " promising candidates.";
    
    RuleWithCoverageSet<AtomT> best_operators;
    if (config_reader.get_variable<bool>("lfit_learner_use_subsumption_tree")) {
        RuleSubsumptionGraph<AtomT> subsumption_graph(promising_head_rules);
        CLOG_IF(debug >=4, INFO, "trivial") << "Starting graph " << subsumption_graph;
        
        double best_candidate_score = -1 * std::numeric_limits<float>::max();
        bool something_changed = true;
        while (something_changed) {
            // Solve conflicts with leafs
            something_changed = remove_conflicting_leafs(head_transitions, subsumption_graph.get_leafs(), subsumption_graph, best_operators, best_candidate_score);
            
            // Solve conflicts with leafs and their parents
            something_changed = remove_conflicting_leafs(head_transitions, subsumption_graph.get_leafs_and_first_parent(), subsumption_graph, best_operators, best_candidate_score) || 
                                something_changed;
        }
    }
    else {
        best_operators = select_best_ruleset(promising_head_rules, head_transitions, debug);
    }
    
    return best_operators;
}

template<typename AtomT>
boost::optional<RuleWithCoverageSet<AtomT> > ProbabilisticLFITAnalyzer::check_inverted_rules(const MapHeadRules<AtomT>& learned_rule_sets, 
                                                                                             const Predicate<AtomT>& current_head, 
                                                                                             const TransitionGroup<AtomT>& head_transitions)
{
    CLOG_IF(debug >=2, INFO, "trivial") << "Checking inverted rules...";
    // if already learned the negated_outcome, try negating the learned rules
    RuleWithCoverageSet<AtomT> inverted_rules;
    typename MapHeadRules<AtomT>::const_iterator learned_rule_sets_it = learned_rule_sets.find(current_head.get_negated());
    if (learned_rule_sets_it != learned_rule_sets.end()) {
        RuleWithCoverageSet<AtomT> learned_rule_set_for_head = learned_rule_sets_it->second;
        BOOST_FOREACH(const RuleWithCoverage<AtomT>& rule_to_invert, learned_rule_set_for_head) {
            RulePtr<AtomT> inverted_rule_ptr = rule_to_invert.rule_ptr->make_shared();
            OutcomeGroup<AtomT> inverted_outcome_group;
            Outcome<AtomT> inverted_outcome(inverted_rule_ptr->outcomes.begin()->probability);
            
            typename Predicate<AtomT>::Opt inverted_outcome_predicate_opt;
            BOOST_FOREACH(const Predicate<AtomT>& precondition, inverted_rule_ptr->get_preconditions()) {
                if (precondition.name == current_head.name) {
                    inverted_outcome_predicate_opt = precondition.get_negated();
                }
            }
            if (inverted_outcome_predicate_opt) {
                inverted_outcome.insert(*inverted_outcome_predicate_opt);
                inverted_outcome_group.insert(inverted_outcome);
                inverted_rule_ptr->outcomes = inverted_outcome_group;
                RuleWithCoverage<AtomT> inverted_rule_with_cov(inverted_rule_ptr, rule_to_invert.coverage);
                inverted_rules.insert(inverted_rule_with_cov);
            }
        }
    }
    if (inverted_rules.get_score(head_transitions, config_reader) > -1.0) {
        CLOG_IF(debug >=1, INFO, "trivial") << "Inverted rules succeeded! Skipping candidate selection.";
        if (debug >=2) {
            CLOG_IF(debug >=2, INFO, "trivial") << "Inverted rules score is: ";
            inverted_rules.get_score(head_transitions, config_reader, 3);
        }
        return inverted_rules;
    }
    else {
        return boost::optional<RuleWithCoverageSet<AtomT> >();
    }
}

template<typename AtomT>
TransitionGroup<AtomT> ProbabilisticLFITAnalyzer::obtain_head_transitions(const TransitionGroup<AtomT>& transitions, 
                                                                          const Predicate<AtomT>& head, 
                                                                          const bool variable_maintains_to_next_state, 
                                                                          const bool ignore_head_sign)
{
    CLOG_IF(debug >=1, INFO, "trivial") << "Getting transitions for change: " << head;
    TransitionGroup<AtomT> head_transitions(transitions.ppddl_object_manager);
    for (typename TransitionSet<AtomT>::const_iterator transition_it = transitions.begin_set(); transition_it != transitions.end_set(); ++transition_it) 
    {
        PredicateGroup<AtomT> post_state;
        if (variable_maintains_to_next_state) {
            // everything maintains as default
            BOOST_FOREACH(const Predicate<AtomT>& prev_predicate, transition_it->get_prev_state()) {
                post_state.update_predicate(prev_predicate);
            }
        }
        // add head predicates values
        BOOST_FOREACH(const Predicate<AtomT>& post_predicate, transition_it->get_post_state()) {
            if (post_predicate.name == head.name) {
                if ( (post_predicate.positive == head.positive) || ignore_head_sign) {
                    // learn only the effects
                    post_state.update_predicate(post_predicate);
                }
            }
        }
        
        Transition<AtomT> new_transition(transition_it->get_prev_state(), 
                                         transition_it->get_action(), 
                                         post_state, 
                                         transition_it->get_reward(), 
                                         variable_maintains_to_next_state);
        head_transitions.add_transition(new_transition, transition_it->get_number_repetitions());
    }
    return head_transitions;
}

template<typename AtomT>
bool ProbabilisticLFITAnalyzer::check_if_not_maintaining_atom_value_is_better(const MapHeadRules<AtomT>& map_head_rules,
                                                                              const float maintains_score, 
                                                                              const AtomT& badly_learned_atom,
                                                                              const TransitionGroup<AtomT>& atom_transitions_default_false,
                                                                              MapHeadRules<AtomT>& learned_rule_sets)
{
    CLOG_IF(debug >=1, INFO, "trivial") << "Checking if default = false yields a better model.";
    RuleWithCoverageSet<AtomT> ruleset_to_check;
    try {
        ruleset_to_check.insert(map_head_rules.at(Predicate<AtomT>(badly_learned_atom, true)).begin(),
                                map_head_rules.at(Predicate<AtomT>(badly_learned_atom, true)).end() );
    } catch (std::out_of_range ex) {}
    
    RuleWithCoverageSet<AtomT> ruleset_to_check_copy = ruleset_to_check.empty_copy();
    RuleWithCoverageSet<AtomT> new_rules_false = select_rules_for_head(ruleset_to_check_copy, atom_transitions_default_false, true);
    float new_score_def_false = new_rules_false.get_score(atom_transitions_default_false, config_reader, 0, 0.0);
    
    CLOG_IF(debug >=2, INFO, "trivial") << "Orig score is " << maintains_score;
    CLOG_IF(debug >=2, INFO, "trivial") << "New score def false is " << new_score_def_false;
    
    if (maintains_score < new_score_def_false) {
        CLOG_IF(debug >=1, INFO, "trivial") << "Better ruleset found for " << badly_learned_atom << " with default = false.";
        learned_rule_sets[Predicate<AtomT>(badly_learned_atom, true)] = new_rules_false;
        learned_rule_sets[Predicate<AtomT>(badly_learned_atom, false)] = RuleWithCoverageSet<AtomT>();
        return true;
    }
    else {
        return false;
    }
}

template<typename AtomT>
RuleSet<AtomT> ProbabilisticLFITAnalyzer::select_rules_for_each_head(const MapHeadRules<AtomT>& map_head_rules, 
                                                                     const TransitionGroup<AtomT>& transitions, 
                                                                     const bool check_default_false,
                                                                     std::set<AtomT>& default_false_atoms
                                                                    )
{
    float MIN_ACCEPTABLE_SCORE = -0.3;
    std::set<AtomT> badly_learned_atoms;
    MapHeadRules<AtomT> learned_rule_sets;
    std::map<Predicate<AtomT>, float> map_head_score;
    
    BOOST_FOREACH(const typename MapHeadRules<AtomT>::value_type& pair_head_rules, map_head_rules) {
#ifdef IGNORE_NEGATIVE_RULES
        // for debug purposes only
        if (!pair_head_rules.first.positive) { continue; }
#endif
#ifdef IGNORE_POSITIVE_RULES
        // for debug purposes only
        if (pair_head_rules.first.positive) { continue; }
#endif
        
        TransitionGroup<AtomT> head_transitions_default_true = obtain_head_transitions(transitions, pair_head_rules.first, true);
        
        boost::optional<RuleWithCoverageSet<AtomT> > inverted_rules_opt = check_inverted_rules(learned_rule_sets, pair_head_rules.first, head_transitions_default_true);
        if (inverted_rules_opt) {
            map_head_score[pair_head_rules.first] = inverted_rules_opt->get_score(head_transitions_default_true, config_reader, 0, 0.0);
            learned_rule_sets[pair_head_rules.first] = *inverted_rules_opt;
        }
        else {
            RuleWithCoverageSet<AtomT> new_rules_true = select_rules_for_head(pair_head_rules.second, head_transitions_default_true, false);
            map_head_score[pair_head_rules.first] = new_rules_true.get_score(head_transitions_default_true, config_reader, 0, 0.0);
            learned_rule_sets[pair_head_rules.first] = new_rules_true;
            
            if (check_default_false && (map_head_score[pair_head_rules.first] < MIN_ACCEPTABLE_SCORE) ) {
                badly_learned_atoms.insert(pair_head_rules.first);
            }
        }
    }
    
    // Check if default=false rules are better (only for badly learned atoms!)
    BOOST_FOREACH(const AtomT& badly_learned_atom, badly_learned_atoms) {
        TransitionGroup<AtomT> atom_transitions_default_false = obtain_head_transitions(transitions, Predicate<AtomT>(badly_learned_atom, true), false);
        RuleWithCoverageSet<AtomT> positive_outcome_rules = learned_rule_sets[Predicate<AtomT>(badly_learned_atom, true)].empty_copy();
        if (positive_outcome_rules.get_score(atom_transitions_default_false, config_reader, 0, 0.0) > MIN_ACCEPTABLE_SCORE) {
            CLOG_IF(debug >=1, INFO, "trivial") << "Better ruleset found for " << badly_learned_atom << " with default = false.";
            learned_rule_sets[Predicate<AtomT>(badly_learned_atom, false)] = RuleWithCoverageSet<AtomT>();
            default_false_atoms.insert(badly_learned_atom);
        }
        else {
            RuleWithCoverageSet<AtomT> maintain_rule_set = learned_rule_sets[Predicate<AtomT>(badly_learned_atom, true)];
            RuleWithCoverageSet<AtomT> maintain_rule_set2 = learned_rule_sets[Predicate<AtomT>(badly_learned_atom, false)];
            maintain_rule_set.insert(maintain_rule_set2.begin(), maintain_rule_set2.end());
            RuleWithCoverageSet<AtomT> ruleset_to_check_copy = maintain_rule_set.empty_copy();
            
            TransitionGroup<AtomT> atom_transitions_default_maintain = obtain_head_transitions(transitions, Predicate<AtomT>(badly_learned_atom, true), true ,true);
            float score_def_maintain = ruleset_to_check_copy.get_score(atom_transitions_default_maintain, config_reader, 0, 0.0);
            
            if (check_if_not_maintaining_atom_value_is_better(map_head_rules, score_def_maintain, badly_learned_atom, atom_transitions_default_false, learned_rule_sets)) {
                default_false_atoms.insert(badly_learned_atom);
            }
        }
    }
    
    RuleSet<AtomT> result;
    #if __cplusplus > 199711L // C++11
    for(auto pair_head_ruleset : learned_rule_sets) {
    #else
    BOOST_FOREACH(const typename MapHeadRules<AtomT>::value_type& pair_head_ruleset, learned_rule_sets) {
    #endif
      result.add_rules(pair_head_ruleset.second.get_rule_set());
    }
    return result;
}

template<typename AtomT>
RuleSet<AtomT> ProbabilisticLFITAnalyzer::select_rules(const std::set<lfit::RuleWithCoverage<AtomT> >& lfit_rules, const TransitionGroup<AtomT>& transitions, std::set<AtomT>& default_false_atoms)
{
    MapHeadRules<AtomT> map_head_rules;
    
    // group rules
    BOOST_FOREACH(const RuleWithCoverage<AtomT>& rule_coverage, lfit_rules) {
        map_head_rules[*rule_coverage.rule_ptr->outcomes.begin()->begin()].insert(rule_coverage);
    }
    
    return select_rules_for_each_head(map_head_rules, transitions, config_reader.get_variable<bool>("lfit_autodetect_if_lits_maintains_to_next_state"), default_false_atoms);
}

#endif