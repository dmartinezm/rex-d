/*
 * LFIT types
 * Copyright (C) 2015  David Martínez <dmartinez@iri.upc.edu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef REXD_LFIT_TYPES_TEMPLATE_IMPL_H
#define REXD_LFIT_TYPES_TEMPLATE_IMPL_H 1

#include "lfit_types.h"

#define PENALTY_PREDICATE 1
#define PENALTY_RULE 2

using namespace lfit;

namespace lfit {

template<typename AtomT>
bool operator<(const RuleWithCoverage<AtomT>& l, const RuleWithCoverage<AtomT>& r)
{
    if (l.rule_ptr < r.rule_ptr)
        return true;
    else if (r.rule_ptr < l.rule_ptr)
        return false;
    else
        return l.coverage < r.coverage;
}

template<typename AtomT>
std::ostream& operator<<(std::ostream &out, const RuleWithCoverage<AtomT>& rule_cov)
{
    out << "[";
    for (typename OutcomeGroup<AtomT>::const_iterator out_it = rule_cov.rule_ptr->outcomes.begin(); out_it != rule_cov.rule_ptr->outcomes.end(); ++out_it) {
        out << *out_it;
        if ( boost::next(out_it, 1) != rule_cov.rule_ptr->outcomes.end()) {
            out << " || ";
        }
    }
    out << "]";
    out << " <- "
        << rule_cov.rule_ptr->name << " & " 
        << rule_cov.rule_ptr->get_preconditions()
        << " (" << rule_cov.coverage.n_covered << "/" << rule_cov.coverage.n_total << ")";
    return out;
}
}

template<typename AtomT>
const TransitionsChangesCoverage& RuleWithCoverage<AtomT>::get_covered_transitions_full(const TransitionGroup<AtomT>& transitions) const
{
    if (!(*covered_transitions_full_opt_ptr)) {
        (*covered_transitions_full_opt_ptr) = TransitionsChangesCoverage(transitions.size_set());
        typename TransitionSet<AtomT>::iterator transition_set_it = transitions.begin_set();
        for (size_t i = 0; i < transitions.size_set(); ++i) {
            (**covered_transitions_full_opt_ptr)[i] = rule_ptr->covers_transition_effects(*transition_set_it, transitions.ppddl_object_manager);
            ++transition_set_it;
        }
    }
    
    return (**covered_transitions_full_opt_ptr);
}

template<typename AtomT>
bool RuleWithCoverage<AtomT>::is_good_candidate(const ConfigReader& config_reader) const
{
    uint max_preconditions = config_reader.get_variable<uint>("lfit_learner_max_preconditions");
    float MIN_PROBABLITY = 0.1; 
    
    if ( // no use for a 0.0 probability outcome
         (!utils::is_close(rule_ptr->outcomes.begin()->probability, (float)0.0)) &&
         // min probability
         (rule_ptr->outcomes.begin()->probability > MIN_PROBABLITY) &&
         // large probability but not 1.0
         ( (rule_ptr->outcomes.begin()->probability < (1.0 - MIN_PROBABLITY)) || utils::is_close(rule_ptr->outcomes.begin()->probability, (float)1.0) ) &&
//          (!rule_ptr->is_special()) &&
         // max preconditions
         (rule_ptr->get_preconditions().size() <= max_preconditions)
       ) 
    {
        return true;
    }
    else {
        return false;
    }
}

template<typename AtomT>
RuleWithCoverage<AtomT> RuleWithCoverage<AtomT>::empty_copy() const
{
    return RuleWithCoverage<AtomT>(rule_ptr, coverage);
}

template<typename AtomT>
RuleWithCoverage<AtomT> RuleWithCoverage<AtomT>::get_inverse_rule() const
{
    // rule_ptr
    RulePtr<AtomT> inverse_rule_ptr = rule_ptr->make_shared();
    PredicateGroup<AtomT> inverse_outcome_preds;
    inverse_outcome_preds.insert(inverse_rule_ptr->outcomes.begin()->begin()->get_negated());
    Outcome<AtomT> inverse_outcome(inverse_outcome_preds, 1.0 - inverse_rule_ptr->outcomes.begin()->probability);
    inverse_rule_ptr->outcomes.clear();
    inverse_rule_ptr->outcomes.insert(inverse_outcome);
    // coverage
    Coverage inverse_coverage(coverage.n_total - coverage.n_covered, coverage.n_total);
    // result
    return RuleWithCoverage<AtomT>(inverse_rule_ptr, inverse_coverage);
}

template<typename AtomT>
float RuleWithCoverage<AtomT>::get_confidence(const float confidence_interval) const
{
    // hoeffding inequality
    // http://en.wikipedia.org/wiki/Hoeffding%27s_inequality
    // \alpha <= 1 - (2 * e ^ (-2 n t^2))
    // n = number samples
    // t = confidence interval
    return utils::get_hoeffding_inequality_confidence(coverage.n_total, confidence_interval);
}

template<typename AtomT>
RuleSet<AtomT> RuleWithCoverageSet<AtomT>::get_rule_set() const
{
    RuleSet<AtomT> result;
    BOOST_FOREACH(const RuleWithCoverage<AtomT>& res_rule, *this) {
        result.push_back(res_rule.rule_ptr);
    }
    return result;
}

template<typename AtomT>
float RuleWithCoverageSet<AtomT>::get_confidence(const float confidence_interval) const
{
    if (this->empty()) {
        return 0.0;
    }
    // average??, sum??
    float rules_confidence = 0;
    BOOST_FOREACH(const RuleWithCoverage<AtomT>& rule, *this) {
        rules_confidence += rule.get_confidence(confidence_interval);
    }
    return rules_confidence / this->size();
}

template<typename AtomT>
float RuleWithCoverageSet<AtomT>::get_likelihood(const TransitionGroup<AtomT>& transitions, const float optimistic_value) const
{
    double total_likelihood = 0;
    
    uint transition_idx = 0;
    for (typename TransitionSet<AtomT>::const_iterator transition_it = transitions.begin_set(); transition_it != transitions.end_set(); ++transition_it) {
        Transitions::Coverage transition_likelihood(transition_it->get_changes().size());
        BOOST_FOREACH(const RuleWithCoverage<AtomT>& rule_cov, *this) {
            transition_likelihood += rule_cov.get_covered_transitions_full(transitions)[transition_idx];
        }
        total_likelihood += std::max(log(transition_likelihood.get_likelihood(true, optimistic_value)),-10000.0) 
                            * transition_it->get_number_repetitions();
        ++transition_idx;
    }
    return total_likelihood/transitions.size_historial();
}

template<typename AtomT>
float RuleWithCoverageSet<AtomT>::get_penalty() const
{
    if (this->empty()) {
        return 0.0;
    }
    uint number_predicates = 0;
//     uint number_rules = size();
    BOOST_FOREACH(const RuleWithCoverage<AtomT>& rule, *this) {
        number_predicates += rule.rule_ptr->get_preconditions().size();
    }
    
//     return (number_rules * PENALTY_RULE) + ((number_predicates * PENALTY_PREDICATE)/number_rules);
    return (number_predicates * PENALTY_PREDICATE);
}

template<typename AtomT>
float RuleWithCoverageSet<AtomT>::get_score(const TransitionGroup<AtomT>& transitions,
                                            const ConfigReader& config_reader,
                                            const int verbose,
                                            const float optimistic_value
                                           ) const
{
    float k_regu = config_reader.get_variable<float>("lfit_learner_score_regularization_scaling");
    
    CLOG_IF(verbose>=2, INFO, "trivial") << "Score rules with " << transitions.size_historial() << " transitions";
    BOOST_FOREACH(const RuleWithCoverage<AtomT>& current_rule, *this) {
        CLOG_IF(verbose>=2, INFO, "trivial") << current_rule;
    }
    
    float confidence = utils::get_hoeffding_inequality_confidence(transitions.size_historial(), config_reader.get_variable<float>("lfit_learner_score_confidence_interval"));
    float likelihood = get_likelihood(transitions, optimistic_value);
    float regularization = get_penalty();
    float score;
    if (config_reader.get_variable<bool>("lfit_learner_score_use_confidence")) {
        confidence = std::max(confidence, (float)0.0001); // hardcoded min
        score = likelihood - ((1/confidence)*k_regu*regularization);
        CLOG_IF(verbose>=2, INFO, "trivial") << "Conf: " << confidence << ", "
                                             << "Lik: " << likelihood << ", "
                                             << "Pen: " << (1/confidence) << "*" << regularization << "*" << k_regu << ", "
                                             << "All: " << score;
    }
    else {
        score = likelihood - (k_regu*regularization);
        CLOG_IF(verbose>=2, INFO, "trivial") << "Conf: " << confidence << "*" << ", "
                                             << "Lik: " << likelihood << ", "
                                             << "Pen: " << regularization << "*" << k_regu << ", "
                                            << "All: " << score;
    }
    
    return score;
}

template<typename AtomT>
RuleWithCoverageSet<AtomT> RuleWithCoverageSet<AtomT>::get_rules_covered_by(const RuleWithCoverage<AtomT>& rule) const
{
    RuleWithCoverageSet<AtomT> result;
    BOOST_FOREACH(const RuleWithCoverage<AtomT>& orig_rule, *this) {
        if (orig_rule.rule_ptr->get_preconditions().includes_predicates(rule.rule_ptr->get_preconditions())) {
            result.insert(orig_rule);
        }
    }
    return result;
}

template<typename AtomT>
RuleWithCoverageSet<AtomT> RuleWithCoverageSet<AtomT>::empty_copy() const
{
    RuleWithCoverageSet<AtomT> result;
    BOOST_FOREACH(const RuleWithCoverage<AtomT>& orig_rule, *this) {
        result.insert(orig_rule.empty_copy());
    }
    return result;
}

#endif
