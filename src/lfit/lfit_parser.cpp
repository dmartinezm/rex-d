/*
 * LFIT reader and parser
 * Copyright (C) 2015  David Martínez <dmartinez@iri.upc.edu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#define DEBUG_VALUE 0

#include "lfit_parser.h"
#include <boost/graph/graph_concepts.hpp>

namespace lfit {
bool operator<(const Coverage& l, const Coverage& r)
{
    return l.get_estimator() < r.get_estimator();
}
}

using namespace lfit;

void LFITRuleBuilder::set_effect(const Predicate<TypedAtom>& new_head, const Coverage& new_coverage) {
    head_ptr = Predicate<TypedAtom>::Ptr(new Predicate<TypedAtom>(new_head));
    coverage_ptr = Coverage::Ptr(new Coverage(new_coverage));
}

bool LFITRuleBuilder::is_valid(const bool aggressive_prunning) const {
    assert(action_ptr);
    assert(head_ptr);
    
    // if any param in outcome is not in rule nor preconditions, ignore the rule (it is not deictic!, cannot be grounded!)
    std::vector<PPDDL::FullObject> in_prec_params = preconditions.get_params();
    std::set<PPDDL::FullObject> in_all_params_set(action_ptr->params.begin(), action_ptr->params.end());
    in_all_params_set.insert(in_prec_params.begin(), in_prec_params.end());
    BOOST_FOREACH(const PPDDL::FullObject& out_param, head_ptr->params) {
        if (in_all_params_set.count(out_param) == 0) {
            return false; // not enough deictic params, the action cannot be grounded, not usable
        }
    }
    
    // if any precondition is negative and params are deictic and not in outcomes, ignore the rule
    // TODO Note that probably a negative precondition would be equivalent to a "not exists param such that ..."
    BOOST_FOREACH(const Predicate<TypedAtom>& precondition, preconditions) {
        if (!precondition.positive) {
            BOOST_FOREACH(const PPDDL::FullObject& prec_param, precondition.params) {
                if ( (!action_ptr->has_param(prec_param)) && (aggressive_prunning || !head_ptr->has_param(prec_param)) ) {
                    // Check if another precondition has the param
                    bool another_precondition_has_param = false;
                    BOOST_FOREACH(const Predicate<TypedAtom>& compared_precondition, preconditions) {
                        if (precondition != compared_precondition) {
                            if (compared_precondition.has_param(prec_param)) {
                                if (compared_precondition.positive) {
                                    another_precondition_has_param = true;
                                }
                            }
                        }
                    }
                    if (!another_precondition_has_param) {
                        return false; // deictic param can be grounded to everything as predicate is negated
                    }
                }
            }
        }
    }
    
    if ( (in_prec_params.size() == 1) && (head_ptr->params.empty())) { 
        // Special case where head has no param, but deictic may still be important
        // Limit to 1 precondition param, as all combinations of preconditions would be valid if no prec param limit
        return true;
    }
    
    // if positive deictic param not in head, action params, nor combination with another used param
    // ignore the rule
    BOOST_FOREACH(const PPDDL::FullObject& deictic_param, in_prec_params) {
        if ( (!action_ptr->has_param(deictic_param)) && (!head_ptr->has_param(deictic_param)) ) { // not in head/action params
            bool a_precondition_has_combination_with_the_param = false;
            BOOST_FOREACH(const Predicate<TypedAtom>& compared_precondition, preconditions) {
                if (compared_precondition.has_param(deictic_param)) {
                    if (compared_precondition.params.size() > 1) {
                        a_precondition_has_combination_with_the_param = true;
                    }
                }
            }
            if (!a_precondition_has_combination_with_the_param) {
                return false;
            }
        }
    }
    
    return true;
}

SymbolicRule<TypedAtom> LFITRuleBuilder::get_rule(const bool positive_head, Coverage& actual_coverage) { 
    assert(action_ptr);
    assert(head_ptr);
    assert(coverage_ptr);
    
    set_default_rule_variables();
    
    PredicateGroup<TypedAtom> effect_predicate_group;
    if (head_ptr->positive == positive_head) {
        effect_predicate_group.insert(*head_ptr);
        actual_coverage = *coverage_ptr;
    }
    else {
        // Complement of the head and coverage
        effect_predicate_group.insert(head_ptr->get_negated());
        actual_coverage = Coverage(coverage_ptr->n_total - coverage_ptr->n_covered, coverage_ptr->n_total);
    }
    Outcome<TypedAtom> effect(effect_predicate_group, 
                    (float)actual_coverage.n_covered / (float)actual_coverage.n_total);
    OutcomeGroup<TypedAtom> effects;
    effects.insert(effect);
    
    return Typed::SymbolicRule(*action_ptr, preconditions, effects);
}

void LFITRuleBuilder::set_default_rule_variables()
{
    assert(action_ptr);
    assert(head_ptr);
    
    // Get ordered params: 1- head_deictic, 2- action, 3- other deictic params
    // Example: action(?Z ?X) & prec1(?V ?Y ?W) :- out1(?X ?Y)
    std::vector<PPDDL::FullObject> ordered_params = preconditions.get_params();
    std::vector<PPDDL::FullObject> action_params_not_in_head = action_ptr->params;
    BOOST_FOREACH(const PPDDL::FullObject& lfit_head_param, head_ptr->params) {
        action_params_not_in_head.erase( std::remove( action_params_not_in_head.begin(), action_params_not_in_head.end(), lfit_head_param ),
                                            action_params_not_in_head.end() );
    }
    
    // Remove head and action params to add them later at the beginning of the vector
    BOOST_FOREACH(const PPDDL::FullObject& lfit_head_param, head_ptr->params) {
        ordered_params.erase( std::remove( ordered_params.begin(), ordered_params.end(), lfit_head_param ),
                                ordered_params.end() );
    }
    BOOST_FOREACH(const PPDDL::FullObject& action_param, action_params_not_in_head) {
        ordered_params.erase( std::remove( ordered_params.begin(), ordered_params.end(), action_param ),
                                ordered_params.end() );
    }
    // Add at the beginning the head and action params
    BOOST_REVERSE_FOREACH(const PPDDL::FullObject& lfit_action_param, action_params_not_in_head) {
        ordered_params.insert(ordered_params.begin(), lfit_action_param);
    }
    BOOST_REVERSE_FOREACH(const PPDDL::FullObject& lfit_head_param, head_ptr->params) {
        ordered_params.insert(ordered_params.begin(), lfit_head_param);
    }
    
    // change variables to aux numbers so no variable is overwritten
    for (size_t i = 0; i < ordered_params.size(); ++i) {
        change_variable(ordered_params[i].name, "?" + boost::lexical_cast<std::string>(i));
    }
    // change aux numbers to default variable names
    for (size_t i = 0; i < ordered_params.size(); ++i) {
        change_variable("?" + boost::lexical_cast<std::string>(i), 
                        DEFAULT_PARAM_NAMES[i]);
    }
}

void LFITRuleBuilder::change_variable(const std::string& orig_var, const std::string& new_var) {
    action_ptr->change_param_name(orig_var, new_var);
    preconditions.change_param_name(orig_var, new_var);
    head_ptr->change_param_name(orig_var, new_var);
}

template<>
RuleWithCoverage<TypedAtom> LFITRuleBuilder::get_rule_with_coverage(const bool positive_head, const Domain& domain) {
    Coverage actual_coverage(0,0);
    RulePtr<TypedAtom> rule_ptr = boost::make_shared<SymbolicRule<TypedAtom> >(get_rule(positive_head, actual_coverage));
    return RuleWithCoverage<TypedAtom>(rule_ptr, actual_coverage);
}

template<>
RuleWithCoverage<IndexedAtom> LFITRuleBuilder::get_rule_with_coverage(const bool positive_head, const Domain& domain) {
    Coverage actual_coverage(0,0);
    RulePtr<IndexedAtom> rule_ptr = boost::make_shared<SymbolicRule<IndexedAtom> >(domain.transform_symbolic_rule_to_indexed(get_rule(positive_head, actual_coverage)));
    return RuleWithCoverage<IndexedAtom>(rule_ptr, actual_coverage);
}


/**
 * \class ParserLFIT
 */

ParserLFIT::ParserLFIT():
    debug(DEBUG_VALUE)
{ }

NonTypedAtom ParserLFIT::parse_atom(const std::string& str) const
{
//     CLOG_IF(debug > 6, INFO, "trivial") << "Atom string is " << str;
    return NonTypedAtom::get_atom_from_grounded_name_item(str);
}

Predicate<NonTypedAtom> ParserLFIT::parse_predicate(const std::string& pred_str) const
{
//     CLOG_IF(debug > 6, INFO, "trivial") << "Predicate string is " << str;
    bool positive = false;
    size_t position_parenthesis = pred_str.find("(");
    if (pred_str[position_parenthesis+1] == '1') {
        positive = true;
    }
    else if (pred_str[position_parenthesis+1] == '0') {
        positive = false;
    }
    else {
        throw ReaderUnsupportedTypeException("RDDL", "Special param: " + pred_str.substr(position_parenthesis+1, 1));
    }
    
    Predicate<NonTypedAtom> predicate(parse_atom(pred_str.substr(0, position_parenthesis)), 
                                      positive);
//     CLOG_IF(debug > 5, INFO, "trivial") << "Predicate is " << predicate;
    return predicate;
}

void ParserLFIT::parse_head(const std::string& str, Predicate<NonTypedAtom>::Opt& head, Coverage& rule_coverage) const
{
    if (str.find(",T)::") == std::string::npos) { // Old format
        head = Predicate<NonTypedAtom>::Opt(parse_predicate(str));
        size_t pos_coverage = str.find(",T") + 3;
        std::string n_covered_str = str.substr(pos_coverage, str.find(")") - pos_coverage);
        
        size_t pos_comma = n_covered_str.find(",");
        rule_coverage.n_covered = boost::lexical_cast<uint>(n_covered_str.substr(0, pos_comma));
        rule_coverage.n_total = boost::lexical_cast<uint>(n_covered_str.substr(pos_comma + 1, std::string::npos));
        assert(rule_coverage.n_covered != 0);
        assert(rule_coverage.n_total != 0);
    }
    else { // new format
        head = Predicate<NonTypedAtom>::Opt(parse_predicate(str));
        size_t pos_coverage = str.find(",T") + 5;
        std::string n_covered_str = str.substr(pos_coverage, str.find(" :-") - pos_coverage);
        
        size_t pos_comma = n_covered_str.find("/");
        rule_coverage.n_covered = boost::lexical_cast<uint>(n_covered_str.substr(0, pos_comma));
        rule_coverage.n_total = boost::lexical_cast<uint>(utils::trim(n_covered_str.substr(pos_comma + 1, std::string::npos)));
        assert(rule_coverage.n_covered != 0);
        assert(rule_coverage.n_total != 0);
    }
}

PredicateGroup<NonTypedAtom> ParserLFIT::parse_predicate_group(const std::string& str) const
{
//     CLOG_IF(debug > 6, INFO, "trivial") << "PredGroup string is " << str;
    PredicateGroup<NonTypedAtom> result;
    std::stringstream ss_str(str);
    std::string item;
    char separator = ')';
    while (std::getline(ss_str, item, separator)) {
        // remove spaces and empty strings
        item.erase(remove_if(item.begin(), item.end(), isspace), item.end());
        if (item[0] == ',') {
            item = item.substr(1,std::string::npos);
        }
        // ignore action (can't be represented with predicate, doesn't take 0,1 values but name value).
        if ((item != "") && (item.find("action") == std::string::npos)) { 
            result.insert(parse_predicate(item));
        }
    }
    
//     CLOG_IF(debug > 5, INFO, "trivial") << "PredGroup is " << result;
    return result;
}

boost::optional<NonTypedAtom> ParserLFIT::parse_action_in_preconditions(const std::string& str) const
{
//     CLOG_IF(debug > 6, INFO, "trivial") << "Action str is " << str;
    size_t action_str_pos = str.find("action(");
    if (action_str_pos == std::string::npos) {
        return boost::optional<NonTypedAtom>();
    }
    std::string action_string = str.substr(action_str_pos + 7, std::string::npos);
    action_string = action_string.substr(0, action_string.find(","));
    NonTypedAtom action_atom = NonTypedAtom::get_atom_from_grounded_name_item(action_string);
    
//     CLOG_IF(debug > 5, INFO, "trivial") << "Recognized action is " << action_atom;
    return action_atom;
}


