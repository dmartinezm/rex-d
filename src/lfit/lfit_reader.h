/*
 * Parser for LFIT files.
 * Copyright (C) 2014  David Martínez (dmartinez@iri.upc.edu)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef IRI_RULE_LEARNER_READER_LFIT_H
#define IRI_RULE_LEARNER_READER_LFIT_H 1

#include <boost/optional.hpp>
#include <symbolic/reader_base.h>
#include <symbolic/domain.h>


/**
 * \class ReaderLFIT
 * \brief Reads LFIT files and extracts rules..
 * 
 */
// TODO remove FileReader
class ReaderLFIT : FileReader
{
private:
    uint debug; // 0 -> nothing, 1 -> basic algorithm, 2 -> data read, 3 -> data parsed, 4 -> details
    std::string file_path;
    
public:
    /**
     * \brief Constructor
     */
    ReaderLFIT(const std::string& file_path_);
    
    /**
     * \brief Read a rule set
     */
    virtual void read_ruleset(Typed::RuleSetPtr& ruleset) { throw ReaderUnsupportedTypeException("RDDL", "ruleset"); } ;
    
    void read_ruleset(Typed::RuleSet& read_ruleset, const TransitionGroup<TypedAtom>& transitions, const ConfigReader& config_reader);
    
    /**
     * \brief Read the domain types
     */
    virtual void read_domain_types(std::vector<PPDDL::FullObject::Type>& types)
    {
        throw ReaderUnsupportedTypeException("RDDL", "object_types");
    };
    
    /**
     * \brief Read the state and goals
     */
    virtual void read_state_and_goals(State& state, RewardFunctionGroup& goals)
    {
        throw ReaderUnsupportedTypeException("RDDL", "state, goals");
    };
    
    /**
     * \brief Read a set of transitions
     */
    virtual void read_transitions(TransitionGroup<TypedAtom>& transitions)
    {
        throw ReaderUnsupportedTypeException("RDDL", "transitions");
    };
    
};

#endif