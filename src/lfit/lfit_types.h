/*
 * LFIT types
 * Copyright (C) 2015  David Martínez (dmartinez@iri.upc.edu)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef REXD_LFIT_TYPES_H
#define REXD_LFIT_TYPES_H 1

#include <boost/optional.hpp>
#include <symbolic/reader_base.h>
#include <symbolic/domain.h>
#include <rexd/config_reader.h>


namespace lfit {
    struct Coverage {
        typedef boost::shared_ptr<Coverage> Ptr;
        
        uint n_covered;
        uint n_total;
//         float optimistic_score;
        Coverage(const uint n_covered_, const uint n_total_):
            n_covered(n_covered_),
            n_total(n_total_)/*,
            optimistic_score(0.0)*/
        {}
        float get_estimator() const { return (float(n_covered) / float(n_total)); }
        
        friend bool operator<(const Coverage& l, const Coverage& r);
    };
    
    typedef std::vector< Transitions::Coverage > TransitionsChangesCoverage;
    typedef boost::optional< TransitionsChangesCoverage > TransitionsChangesCoverageOpt;
    typedef boost::shared_ptr< TransitionsChangesCoverageOpt > TransitionsChangesCoverageOptPtr;
    
    /**
     * \class RuleWithCoverage
     * 
     * This class should be small because we may make a lot of copies of it when generating sets of rules
     * Therefore data things are declared as pointers
     */
    // friend operators with template calss
    template <typename AtomT> class RuleWithCoverage;
    template <typename AtomT>
    std::ostream& operator<<(std::ostream &out, const RuleWithCoverage<AtomT>& r);
    template <typename AtomT>
    bool operator<(const RuleWithCoverage<AtomT>& l, const RuleWithCoverage<AtomT>& r);
    
    template<typename AtomT>
    class RuleWithCoverage {
        private:
            TransitionsChangesCoverageOptPtr covered_transitions_full_opt_ptr;
        
        public:
            typedef boost::shared_ptr<RuleWithCoverage> Ptr;
            
            RulePtr<AtomT> rule_ptr;
            Coverage coverage;
            
            RuleWithCoverage(const RuleWithCoverage& rule_with_cov):
                covered_transitions_full_opt_ptr(rule_with_cov.covered_transitions_full_opt_ptr),
                rule_ptr(rule_with_cov.rule_ptr),
                coverage(rule_with_cov.coverage)
            { }
            
            RuleWithCoverage(const RulePtr<AtomT>& rule_ptr, const Coverage& coverage):
                covered_transitions_full_opt_ptr(new TransitionsChangesCoverageOpt() ),
                rule_ptr(rule_ptr),
                coverage(coverage)
            { }
            
            RuleWithCoverage empty_copy() const;
            
            float get_confidence(const float confidence_interval) const;
            
            const TransitionsChangesCoverage& get_covered_transitions_full(const TransitionGroup<AtomT>& transitions) const;
            
            bool is_good_candidate(const ConfigReader& config_reader) const;
            
            RuleWithCoverage get_inverse_rule() const;
            
            friend bool operator< <>(const RuleWithCoverage& l, const RuleWithCoverage& r);
            friend std::ostream& operator<< <>(std::ostream &out, const RuleWithCoverage& rule_cov);
    };
    
    template<typename AtomT>
    class RuleWithCoverageSet : public std::set<RuleWithCoverage<AtomT> > {
        public:
            typedef boost::shared_ptr<RuleWithCoverageSet> Ptr;
            
            float get_score(const TransitionGroup<AtomT>& transitions,
                            const ConfigReader& config_reader,
                            const int verbose = 0,
                            const float optimistic_value = 0.0
                           ) const;
            
            RuleWithCoverageSet get_rules_covered_by(const RuleWithCoverage<AtomT>& rule) const;
            
            RuleWithCoverageSet empty_copy() const;
            
            RuleSet<AtomT> get_rule_set() const;
            
        private:
            float get_penalty() const;
            float get_confidence(const float confidence_interval) const;
            float get_likelihood(const TransitionGroup<AtomT>& transitions, const float optimistic_value = 0.0) const;
    };
    
    typedef std::set<int> IndexedRuleSet;
    typedef std::pair<float, IndexedRuleSet> ScoreAndIndexedRuleSet;
    
    // probabilistic stuff
    #if __cplusplus > 199711L // C++11
    template <typename AtomT>
    using MapHeadRules = typename std::map<Predicate<AtomT>, lfit::RuleWithCoverageSet<AtomT> >;
    #else
    template <typename AtomT>
    struct MapHeadRules : public std::map<Predicate<AtomT>, lfit::RuleWithCoverageSet<AtomT> > {};
    #endif
    
}

#include <lfit/lfit_types_template_imp.h>


#endif