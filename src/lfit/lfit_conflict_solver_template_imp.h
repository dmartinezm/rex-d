/*
 * LFIT conflict solver
 * Copyright (C) 2015  David Martínez <dmartinez@iri.upc.edu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef REXD_LFIT_CONFLICT_SOLVER_TEMPLATE_IMP_H
#define REXD_LFIT_CONFLICT_SOLVER_TEMPLATE_IMP_H 1

#include "lfit_conflict_solver.h"

using namespace lfit;

template<typename AtomT>
ConflictArrayCoveringTransitions::ConflictArrayCoveringTransitions(const RuleWithCoverageSet<AtomT>& ruleset, const TransitionGroup<AtomT>& transitions)
{
    coverage_table.resize(ruleset.size());
    size_t rule_idx = 0;
    BOOST_FOREACH(const RuleWithCoverage<AtomT>& rule_with_cov, ruleset) {
        std::vector< Transitions::Coverage > coverages = rule_with_cov.get_covered_transitions_full(transitions);
        BOOST_FOREACH(const Transitions::Coverage& coverage, coverages) {
            for (size_t change_idx = 0; change_idx < coverage.size(); ++change_idx) {
                coverage_table[rule_idx].push_back(coverage.get_change_coverage(change_idx));
            }
        }
        ++rule_idx;
    }
}


/**
 * RuleConflictSolver
 */

template<typename AtomT>
RuleConflictSolver<AtomT>::RuleConflictSolver(const RuleWithCoverageSet<AtomT>& ruleset, const TransitionGroup<AtomT>& transitions, const ConfigReader& config_reader):
    ruleset(ruleset)
{
    generate_conflict_array_transitions(transitions);
}

template<typename AtomT>
void RuleConflictSolver<AtomT>::generate_conflict_array_transitions(const TransitionGroup<AtomT>& transitions)
{
    conflict_array_ptr = ConflictArrayPtr(new ConflictArrayCoveringTransitions(ruleset, transitions));
}

template<typename AtomT>
RuleWithCoverageSet<AtomT> RuleConflictSolver<AtomT>::transform_indexed_ruleset_to_standard(const IndexedRuleSet& indexed_ruleset) const
{
    RuleWithCoverageSet<AtomT> standard_ruleset;
    BOOST_FOREACH(const int rule_idx, indexed_ruleset) {
        typename RuleWithCoverageSet<AtomT>::iterator rule_it = ruleset.begin();
        std::advance(rule_it, rule_idx);
        standard_ruleset.insert(*rule_it);
    }
    return standard_ruleset;
}

template<typename AtomT>
RuleWithCoverageSet<AtomT> RuleConflictSolver<AtomT>::get_best_subset(const TransitionGroup<AtomT>& transitions, const ConfigReader& config_reader, const bool optimal, const int verbose)
{
    float lfit_score_optimistic_value = config_reader.get_variable<float>("lfit_learner_score_optimistic_value");
    uint max_number_rules_per_iter = config_reader.get_variable<uint>("lfit_learner_conflicts_heuristic_max_rules_per_iter");
    uint max_number_iterations = config_reader.get_variable<uint>("lfit_learner_conflicts_heuristic_max_iterations");
    
    IndexedRuleSet best_ruleset;
    float best_score;
    if (optimal) {
        // Use heuristic method to get estimate of best subset
        RuleWithCoverageSet<AtomT> best_suboptimal_subset = get_best_subset(transitions, config_reader, false, verbose);
        // Use a slightly worse value, maybe heuristic result is the optimal one
        best_score = best_suboptimal_subset.get_score(transitions, config_reader, 0) - 0.01;
    }
    else {
        best_score = -1 * std::numeric_limits<float>::max();
    }
    std::set<ScoreAndIndexedRuleSet> possibly_valid_indexed_results;
//     std::priority_queue<ScoreAndIndexedRuleSet> possibly_valid_indexed_results;
    
    for (int rule_idx = 0; rule_idx < (int)ruleset.size(); ++rule_idx) {
        IndexedRuleSet new_indexed_candidate;
        new_indexed_candidate.insert(rule_idx);
        RuleWithCoverageSet<AtomT> new_ruleset_candidate = transform_indexed_ruleset_to_standard(new_indexed_candidate);
        float new_score = new_ruleset_candidate.get_score(transitions, config_reader, 0);
        if (new_score > best_score) {
            best_ruleset = new_indexed_candidate;
            best_score = new_score;
            if (verbose>=3) {
                CLOG_IF(verbose>=3, INFO, "trivial") << "---------";
                CLOG_IF(verbose>=3, INFO, "trivial") << "New best score with";
                new_ruleset_candidate.get_score(transitions, config_reader, verbose);
            }
        }
                        
        float optimistic_score = new_ruleset_candidate.get_score(transitions, config_reader, 0, lfit_score_optimistic_value);
        /* TODO Increase optimistic score
        */
        
        
        if ( optimistic_score > best_score) {
            possibly_valid_indexed_results.insert(ScoreAndIndexedRuleSet(optimistic_score, new_indexed_candidate));
            if (verbose>=5) {
                CLOG_IF(verbose>=3, INFO, "trivial") << "---------";
                CLOG_IF(verbose>=3, INFO, "trivial") << "Optimistic score with ";
                new_ruleset_candidate.get_score(transitions, config_reader, verbose, lfit_score_optimistic_value);
            }
        }
    }
    
    std::set<IndexedRuleSet> already_added_sets;
    
    uint number_iterations_without_change = 0;
    uint number_rules_analyzed = 0;
    uint number_iterations = 0;
    while ( !possibly_valid_indexed_results.empty() &&
            ( optimal || 
              ( (number_iterations_without_change < (max_number_iterations/4)) &&
                (number_iterations < max_number_iterations) 
              )
            )
          ) {
//         ScoreAndIndexedRuleSet current_score_indexed_result = possibly_valid_indexed_results.top();
//         possibly_valid_indexed_results.pop();
        ScoreAndIndexedRuleSet current_score_indexed_result = *possibly_valid_indexed_results.rbegin(); // last element = biggest
        size_t number_erased = possibly_valid_indexed_results.erase(current_score_indexed_result);
        assert(number_erased==1);
        if ( (verbose >= 4) && (current_score_indexed_result.first > -0.4) ) {
            CLOG_IF(verbose>=4, INFO, "trivial") << "************************";
            CLOG_IF(verbose>=4, INFO, "trivial") << "Optimistic score was " << current_score_indexed_result.first;
            RuleWithCoverageSet<AtomT> new_ruleset_candidate = transform_indexed_ruleset_to_standard(current_score_indexed_result.second);
            new_ruleset_candidate.get_score(transitions, config_reader, verbose, lfit_score_optimistic_value);
        }
        
        if (current_score_indexed_result.first < best_score) {
            possibly_valid_indexed_results.clear();
        }
        
        int number_rules_iteration = 0;
        // for each new rule candidate
        for (std::set<ScoreAndIndexedRuleSet>::reverse_iterator set_it = possibly_valid_indexed_results.rbegin(); set_it != possibly_valid_indexed_results.rend(); ++ set_it) {
            if (set_it->first < best_score) {
                std::set<ScoreAndIndexedRuleSet>::reverse_iterator current = set_it++;
                possibly_valid_indexed_results.erase(*current);
            }
            else {
                bool conflicting = false;
                for (int rule_idx = 0; rule_idx < (int)ruleset.size(); ++rule_idx) { // check same rule
                    if (current_score_indexed_result.second.count(rule_idx) && set_it->second.count(rule_idx)) {
                        conflicting = true;
                    }
                }
                for (int rule_idx = 0; rule_idx < (int)ruleset.size(); ++rule_idx) {
                    for (int rule_idx2 = 0; rule_idx < (int)ruleset.size(); ++rule_idx) {
                        if (current_score_indexed_result.second.count(rule_idx)) {
                            if (set_it->second.count(rule_idx)) {
                                if (conflict_array_ptr->is_there_a_conflict(rule_idx, rule_idx2)) {
                                    conflicting = true;
                                }
                            }
                        }
                    }
                }
                if (!conflicting) {
                    number_rules_analyzed++;
                    number_rules_iteration++;
                    IndexedRuleSet new_indexed_candidate(current_score_indexed_result.second);
                    new_indexed_candidate.insert(set_it->second.begin(), set_it->second.end());
                    if (already_added_sets.count(new_indexed_candidate) == 0) {
                        RuleWithCoverageSet<AtomT> new_ruleset_candidate = transform_indexed_ruleset_to_standard(new_indexed_candidate);
                        float new_score = new_ruleset_candidate.get_score(transitions, config_reader, 0);
                        if (new_score > best_score) {
                            best_ruleset = new_indexed_candidate;
                            best_score = new_score;
                            number_iterations_without_change = 0;
                            if (verbose>=3) {
                                CLOG_IF(verbose>=3, INFO, "trivial") << "---------";
                                CLOG_IF(verbose>=3, INFO, "trivial") << "New best score with";
                                new_ruleset_candidate.get_score(transitions, config_reader, verbose);
                            }
                        }
                        
                        float optimistic_score = new_ruleset_candidate.get_score(transitions, config_reader, 0, lfit_score_optimistic_value);
                        // TODO Add min penalty for adding a extra rule
                        
                        already_added_sets.insert(new_indexed_candidate);
                        if ( optimistic_score > best_score) {
                            possibly_valid_indexed_results.insert(ScoreAndIndexedRuleSet(optimistic_score, new_indexed_candidate));
                        }
                    }
                }
            }
            if ( !optimal && (number_rules_iteration > (int)max_number_rules_per_iter) ) { // HACK Use only the top candidates
                break;
            }
        }
//         }
        number_iterations_without_change++;
        if (verbose >= 1) {
            LOG_EVERY_N(100,INFO) << "Current candidate set size: " << possibly_valid_indexed_results.size();
        }
        number_iterations++;
    }
    
    if (verbose>=2) {
        CLOG_IF(verbose>=2, INFO, "trivial") << "###############";
        CLOG_IF(verbose>=2, INFO, "trivial") << "Best score with";
        transform_indexed_ruleset_to_standard(best_ruleset).get_score(transitions, config_reader, 2);
        
        CLOG_IF(verbose>=2, INFO, "trivial") << "###############";
        if (possibly_valid_indexed_results.empty()) {
            CLOG_IF(verbose>=2, INFO, "trivial") << "Reason to finish: " << "Completed checking all candidates.";
        }
//         else if (number_rules_analyzed >= max_number_rules) {
//             CLOG_IF(verbose>=2, INFO, "trivial") << "Reason to finish: " << "Max number of rules analyzed reached (" << max_number_rules << ").";
//         }
        else if (number_iterations_without_change >= (max_number_iterations/4)) {
            CLOG_IF(verbose>=2, INFO, "trivial") << "Reason to finish: " << "Number of iterations without change >= max/4=" << (max_number_iterations/4) << ".";
        }
        else if (number_iterations >= max_number_iterations) {
            CLOG_IF(verbose>=2, INFO, "trivial") << "Reason to finish: " << "Number of iterations >= max=" << (max_number_iterations) << ".";
        }
        else {
            CLOG_IF(verbose>=2, WARNING, "trivial") << "Reason to finish: " << "WTF!";
        }
    }
    return transform_indexed_ruleset_to_standard(best_ruleset);
}

#endif
