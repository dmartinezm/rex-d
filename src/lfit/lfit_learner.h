/*
 * LFIT learner wrapper to be used in REX-D.
 * Copyright (C) 2015  David Martínez (dmartinez@iri.upc.edu)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef INTELLACT_LFIT_LEARNER_H
#define INTELLACT_LFIT_LEARNER_H

#include <symbolic/rules.h>
#include <symbolic/predicates.h>

#include <rexd/config_reader.h>
#include <rexd/learner.h>
#include <lfit/lfit_reader.h>

/**
 * \class LFITLearner
 * \brief LFIT learner wrapper. Uses LFIT implementation.
 * 
 * Inherits functionality from learner.h
 */
class LFITLearner : public Learner {
public:

    LFITLearner(const ConfigReader& config_reader);

    /**
     * \brief Learn rules with no logging information
     */
    virtual Typed::RuleSet learn_quiet(const TransitionGroup<TypedAtom>& transitions, const Typed::RuleSet& previous_rules);
    
    /**
     * \brief Learn rules
     */
    virtual Typed::RuleSet learn(const TransitionGroup<TypedAtom>& transitions, const Typed::RuleSet& previous_rules);
    
    /**
     * \brief Generate lfit propositional rules
     */
    void generate_lfit_rules(const TransitionGroup<TypedAtom>& transitions) const;
    
    /**
     * \brief Get rules score
     */
    float get_rules_score(const TransitionGroup<TypedAtom>& transitions, const Typed::RuleSet& rules, const uint verbose);


private:
    ConfigReader config_reader;
    
    // file paths
    std::string lfit_rules_path;
    std::string transitions_learning_path;
    std::string learner_path;
    
    uint max_action_variables;
    uint maximum_number_preconditions;
    
    uint iterations_skipping_learning;

    bool learn_lfit() const;
    void write_lfit_transitions_to_file(const TransitionGroup<TypedAtom>& transitions) const;
    bool check_if_learning_is_needed(const TransitionGroup<TypedAtom>& transitions, const Typed::RuleSet& previous_rules, const uint verbose);
};

#endif
