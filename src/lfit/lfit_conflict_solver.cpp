/*
 * LFIT conflict solver
 * Copyright (C) 2015  David Martínez <dmartinez@iri.upc.edu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "lfit_conflict_solver.h"

using namespace lfit;

bool ConflictArrayCoveringTransitions::is_there_a_conflict(const int rule_idx1, const int rule_idx2)
{
    for (size_t transition_change_idx = 0; transition_change_idx < coverage_table[0].size(); ++transition_change_idx) {
        if ( (coverage_table[rule_idx1][transition_change_idx] > 0.0) &&
             (coverage_table[rule_idx2][transition_change_idx] > 0.0) &&
             (coverage_table[rule_idx1][transition_change_idx] < 1.0) &&
             (coverage_table[rule_idx2][transition_change_idx] < 1.0) ) 
        {
            return true;
        }
    }
    return false;
}
