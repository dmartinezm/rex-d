/*
 * Graph representing subsumption relationships between rule preconditions.
 * Copyright (C) 2015  David Martínez (dmartinez@iri.upc.edu)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef REXD_LFIT_SUBSUMPTION_GRAPH_H
#define REXD_LFIT_SUBSUMPTION_GRAPH_H

#include <symbolic/rules.h>
#include <symbolic/predicates.h>

#include <lfit/lfit_types.h>

namespace lfit {

template <typename AtomT> struct Vertex;
template <typename AtomT>
bool operator<(const Vertex<AtomT>& l, const Vertex<AtomT>& r);

template<typename AtomT>
struct Vertex{
    typedef boost::shared_ptr<Vertex> Ptr;
    std::list<Vertex::Ptr> adjacency_list;
    std::list<Vertex::Ptr> inverse_adjacency_list;
    RuleWithCoverage<AtomT> rulecov;
    
    Vertex(const Vertex& vertex):
        adjacency_list(vertex.adjacency_list),
        inverse_adjacency_list(vertex.inverse_adjacency_list),
        rulecov(vertex.rulecov)
    {}
    
    Vertex(const RuleWithCoverage<AtomT>& rule_with_cov):
        rulecov(rule_with_cov)
    { }
    
    Vertex::Ptr make_shared() const { return Vertex::Ptr(new Vertex(*this)); }
    
    friend bool operator< <>(const Vertex& l, const Vertex& r);
};

#if __cplusplus > 199711L // C++11
template <typename AtomT>
using VertexMap = typename std::map<RulePtr<AtomT>, typename Vertex<AtomT>::Ptr>;
#else
template <typename AtomT>
struct VertexMap : public std::map<RulePtr<AtomT>, typename Vertex<AtomT>::Ptr> {};
#endif


/**
 * \class RuleSubsumptionGraph
 * \brief Graph representing subsumption relationships between rule preconditions.
 * 
 * If the set of states covered by one rule contains the set of states contained by another rule, the first rule is subsumed of the second.
 */
template <typename AtomT> class RuleSubsumptionGraph;
template <typename AtomT>
std::ostream& operator<<(std::ostream &out, const RuleSubsumptionGraph<AtomT>& graph);

template<typename AtomT>
class RuleSubsumptionGraph {
public:
    RuleSubsumptionGraph(const RuleWithCoverageSet<AtomT>& lfit_rules);
    
    RuleWithCoverageSet<AtomT> get_leafs() const;
    RuleWithCoverageSet<AtomT> get_leafs_and_first_parent() const;
    
    RuleWithCoverageSet<AtomT> get_rule_parents(const RuleWithCoverage<AtomT>& rule) const;
    
    void remove_leaf(const RuleWithCoverage<AtomT>& rule);
    bool remove_leafs_not_in(const RuleWithCoverageSet<AtomT>& ruleset);
    
    friend std::ostream& operator<< <>(std::ostream &out, const RuleSubsumptionGraph& graph);
    
private:
    VertexMap<AtomT> vertex_map;
    std::set<typename Vertex<AtomT>::Ptr> leafs;
    
    void add_vertex(const RuleWithCoverage<AtomT>& rule);
    void add_edge(const RuleWithCoverage<AtomT>& from, const RuleWithCoverage<AtomT>& to);
    void remove_vertex(const RuleWithCoverage<AtomT>& rule);
    
    /** \brief Remove nodes whose effects are already covered by a specialized rule (leaf) */
    void remove_completely_explained_nodes();
    void remove_completely_explained_nodes_by_child(const typename Vertex<AtomT>::Ptr child_vertex_ptr);
};

}

#include <lfit/subsumption_graph_template_imp.h>

#endif
