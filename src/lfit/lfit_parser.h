/*
 * Parser for LFIT files.
 * Copyright (C) 2014  David Martínez (dmartinez@iri.upc.edu)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef REXD_LFIT_PARSER_H
#define REXD_LFIT_PARSER_H 1

#include <boost/optional.hpp>
#include <symbolic/reader_base.h>
#include <symbolic/domain.h>
#include <lfit/probabilistic_lfit.h>


/**
 * \class ParserLFIT
 * \brief Parses LFIT strings.
 * 
 */
class ParserLFIT : FileParser
{
private:
    uint debug; // 0 -> nothing, 1 -> basic algorithm, 2 -> data read, 3 -> data parsed, 4 -> details
    
public:
    /*!
     *  \brief Constructor
     */
    ParserLFIT();
    
    virtual NonTypedAtom  parse_atom                  (const std::string& str) const;
    
    virtual TypedAtom     parse_typed_atom            (const std::string& str) const
    {
        throw ReaderUnsupportedTypeException("RDDL", "typed_atom");
    };
    
    void parse_head(const std::string& str, Predicate<NonTypedAtom>::Opt& head, lfit::Coverage& rule_coverage) const;
    
    virtual Predicate<NonTypedAtom> parse_predicate             (const std::string& str) const;
    
    virtual PredicateGroup<NonTypedAtom> parse_predicate_group  (const std::string& str) const;
    
    virtual Outcome<TypedAtom> parse_outcome            (const std::string& str) const
    {
        throw ReaderUnsupportedTypeException("RDDL", "outcome");
    };
    
    virtual OutcomeGroup<TypedAtom> parse_outcome_group (const std::string& str) const
    {
        throw ReaderUnsupportedTypeException("RDDL", "outcome_group");
    };
    
    template<typename AtomT>
    lfit::RuleWithCoverage<AtomT> parse_symbolic_rule (const std::string& str, const Domain& domain, const bool aggressive_prunning) const;
    
    virtual Typed::RuleSetPtr parse_symbolic_ruleset  (const std::string& str) const
    {
        throw ReaderUnsupportedTypeException("RDDL", "symbolic_ruleset");
    };
    
    virtual State parse_state (const std::string& str) const
    {
        throw ReaderUnsupportedTypeException("RDDL", "state");
    };
    
    virtual std::vector<PPDDL::FullObject::Type>  parse_pddl_types (const std::string& str) const
    {
        throw ReaderUnsupportedTypeException("RDDL", "types");
    };
    
    virtual RewardFunctionGroup  parse_global_goals (const std::string& str) const
    {
        throw ReaderUnsupportedTypeException("RDDL", "global_goals");
    };
    
    virtual PPDDLObjectManager<PPDDL::FullObject> parse_objects_from_simple_string(const std::string& str) const
    {
        throw ReaderUnsupportedTypeException("RDDL", "objects");
    };
    
    boost::optional<NonTypedAtom> parse_action_in_preconditions(const std::string& str) const;
    
    void set_default_rule_variables(Typed::RulePtr lfit_rule_ptr);
    
};

class LFITRuleBuilder {
public:
    template<typename AtomT>
    RuleWithCoverage<AtomT> get_rule_with_coverage(const bool positive_head, const Domain& domain);
    void set_action(const TypedAtom& new_action) { action_ptr = TypedAtomPtr(new TypedAtom(new_action)); }
    void set_preconditions(const PredicateGroup< TypedAtom >& new_preconditions) { preconditions = new_preconditions; }
    void set_effect(const Predicate<TypedAtom>& new_head, const Coverage& new_coverage);
    bool is_valid(const bool aggressive_prunning) const;
    
    
private:
    TypedAtomPtr action_ptr;
    PredicateGroup<TypedAtom> preconditions;
    Predicate<TypedAtom>::Ptr head_ptr;
    Coverage::Ptr coverage_ptr;
    
    SymbolicRule<TypedAtom> get_rule(const bool positive_head, Coverage& actual_coverage);
    void set_default_rule_variables();
    void change_variable(const std::string& orig_var, const std::string& new_var);
};

template<typename AtomT>
RuleWithCoverage<AtomT> ParserLFIT::parse_symbolic_rule(const std::string& original_rule_string, const Domain& domain, const bool aggressive_prunning) const
{
    CLOG_IF(debug > 6, INFO, "trivial") << "Rule string is " << original_rule_string;
    
    Coverage rule_coverage(0,0);
    Predicate<NonTypedAtom>::Opt effect_predicate;
    
    // if starts with "action(" -> ignore
    if (original_rule_string.find("action(") == 0) {
        return RuleWithCoverage<AtomT>(SymbolicRulePtr<AtomT>(), Coverage(0,0));
    }
    if (isupper(original_rule_string[0])) { // constant!
        return RuleWithCoverage<AtomT>(SymbolicRulePtr<AtomT>(), Coverage(0,0));
    }
    
    // Remove tailing "." as it is not needed
    std::string rule_string = original_rule_string.substr(0, original_rule_string.size()-1);
    
    // effect
    std::string effect_string = rule_string.substr(0, rule_string.find(":-"));
    parse_head(effect_string, effect_predicate, rule_coverage);
    if (rule_coverage.n_covered == 0) {
        LOG(WARNING) << "CHECK THIS!";
        return RuleWithCoverage<AtomT>(SymbolicRulePtr<AtomT>(), rule_coverage);
    }
    
    // preconditions
    LFITRuleBuilder lfit_rule_builder;
//     TypedAtom action;
    PredicateGroup<NonTypedAtom> preconditions;
    size_t preconditions_pos;
    if ((preconditions_pos = rule_string.find(":-")) != std::string::npos) {
        std::string preconditions_string = rule_string.substr(preconditions_pos+2, std::string::npos);
        preconditions = parse_predicate_group(preconditions_string);
        
        boost::optional<NonTypedAtom> non_typed_action_opt = parse_action_in_preconditions(preconditions_string);
        if (!non_typed_action_opt) {
            lfit_rule_builder.set_action(MAKE_TYPED_ATOM_NOACTION);
        }
        else if (non_typed_action_opt->name == ACTION_NAME_NOACTION) {
            // NoAction happen always -> thus no actions should be there
            // This can be a rule that only happens when noop is applied
            return RuleWithCoverage<AtomT>(SymbolicRulePtr<AtomT>(), rule_coverage);
        }
        else {
            lfit_rule_builder.set_action(domain.transform_to_typed(*non_typed_action_opt));
        }
    }
    else {
        return RuleWithCoverage<AtomT>(SymbolicRulePtr<AtomT>(), rule_coverage); // no preconditions also means no action
    }
    
    lfit_rule_builder.set_preconditions(domain.transform_to_typed(preconditions));
    lfit_rule_builder.set_effect(domain.transform_to_typed(*effect_predicate), rule_coverage);
    
    if (!lfit_rule_builder.is_valid(aggressive_prunning)) {
        return RuleWithCoverage<AtomT>(SymbolicRulePtr<AtomT>(), rule_coverage);
    }
    
//     CLOG_IF(debug > 5, INFO, "trivial") << "Rule is " << *result_rule_ptr;
    return lfit_rule_builder.get_rule_with_coverage<AtomT>(true, domain);
}

#endif
