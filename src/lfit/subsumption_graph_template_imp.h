
#ifndef REXD_LFIT_SUBSUMPTION_GRAPH_TEMPLATE_IMP_H
#define REXD_LFIT_SUBSUMPTION_GRAPH_TEMPLATE_IMP_H

#include "subsumption_graph.h"

namespace lfit {

template<typename AtomT>
bool operator<(const Vertex<AtomT>& l, const Vertex<AtomT>& r)
{
    return (l.rulecov < r.rulecov);
}

template<typename AtomT>
std::ostream& operator<<(std::ostream &out, const RuleSubsumptionGraph<AtomT>& graph)
{
    BOOST_FOREACH(const typename Vertex<AtomT>::Ptr& leaf, graph.leafs) {
        out << "---- New leaf ----" << std::endl;
        std::list<typename Vertex<AtomT>::Ptr> list_of_vertex_to_print;
        list_of_vertex_to_print.push_back(leaf);
        while (!list_of_vertex_to_print.empty()) {
            typename Vertex<AtomT>::Ptr new_vertex_ptr = list_of_vertex_to_print.front();
            list_of_vertex_to_print.pop_front();
            out << new_vertex_ptr->rulecov.rule_ptr->name << " <- " << new_vertex_ptr->rulecov.rule_ptr->get_preconditions() << " [" << new_vertex_ptr->adjacency_list.size() << "]" << std::endl;
            BOOST_FOREACH(const typename Vertex<AtomT>::Ptr& parent_vertex, new_vertex_ptr->inverse_adjacency_list) {
                list_of_vertex_to_print.push_front(parent_vertex);
            }
        }
    }
    return out;
}

template<typename AtomT>
void RuleSubsumptionGraph<AtomT>::add_vertex(const RuleWithCoverage<AtomT> &rule)
{
    if(vertex_map.count(rule.rule_ptr) == 0)
    {
        typename Vertex<AtomT>::Ptr vertex_ptr(new Vertex<AtomT>(rule));
        vertex_map[rule.rule_ptr] = vertex_ptr;
        leafs.insert(vertex_ptr);
    }
}

template<typename AtomT>
void RuleSubsumptionGraph<AtomT>::add_edge(const RuleWithCoverage<AtomT>& from, const RuleWithCoverage<AtomT>& to)
{
    typename Vertex<AtomT>::Ptr from_vertex = vertex_map[from.rule_ptr];
    typename Vertex<AtomT>::Ptr to_vertex = vertex_map[to.rule_ptr];
    from_vertex->adjacency_list.push_back(to_vertex);
    to_vertex->inverse_adjacency_list.push_back(from_vertex);
    leafs.erase(from_vertex);
}

template<typename AtomT>
void RuleSubsumptionGraph<AtomT>::remove_vertex(const RuleWithCoverage<AtomT>& rule)
{
    assert(vertex_map.find(rule.rule_ptr) != vertex_map.end());
    typename Vertex<AtomT>::Ptr rule_vertex = vertex_map[rule.rule_ptr];
    BOOST_FOREACH(typename Vertex<AtomT>::Ptr &child, rule_vertex->adjacency_list) {
        child->inverse_adjacency_list.remove(rule_vertex);
    }
    BOOST_FOREACH(typename Vertex<AtomT>::Ptr &parent, rule_vertex->inverse_adjacency_list) {
        parent->adjacency_list.remove(rule_vertex);
        if (parent->adjacency_list.empty()) {
            leafs.insert(parent);
        }
    }
    leafs.erase(rule_vertex);
    vertex_map.erase(rule.rule_ptr);
}

template<typename AtomT>
void RuleSubsumptionGraph<AtomT>::remove_completely_explained_nodes()
{
    BOOST_FOREACH(const typename Vertex<AtomT>::Ptr &leaf_vertex, leafs) {
        remove_completely_explained_nodes_by_child(leaf_vertex);
    }
}

template<typename AtomT>
void RuleSubsumptionGraph<AtomT>::remove_completely_explained_nodes_by_child(const typename Vertex<AtomT>::Ptr child_vertex_ptr)
{
    std::list<typename Vertex<AtomT>::Ptr> parents = child_vertex_ptr->inverse_adjacency_list;
    BOOST_FOREACH(const typename Vertex<AtomT>::Ptr &parent_vertex, parents) {
        remove_completely_explained_nodes_by_child(parent_vertex);
        if ( (child_vertex_ptr->rulecov.coverage.n_covered == parent_vertex->rulecov.coverage.n_covered) && 
             (child_vertex_ptr->rulecov.coverage.n_total < parent_vertex->rulecov.coverage.n_total) ) {
            remove_vertex(parent_vertex->rulecov);
        }
    }
}

template<typename AtomT>
RuleSubsumptionGraph<AtomT>::RuleSubsumptionGraph(const RuleWithCoverageSet<AtomT>& lfit_rules) {
        
    for (typename RuleWithCoverageSet<AtomT>::iterator rule_it1 = lfit_rules.begin(); rule_it1 != lfit_rules.end(); ++rule_it1) {
        add_vertex(*rule_it1);
        typename RuleWithCoverageSet<AtomT>::iterator rule_it2 = rule_it1;
        for (++rule_it2; rule_it2 != lfit_rules.end(); ++rule_it2) {
            add_vertex(*rule_it2);
            if (rule_it1->rule_ptr->name == rule_it2->rule_ptr->name) {
                if (abs(rule_it1->rule_ptr->get_preconditions().size() - rule_it2->rule_ptr->get_preconditions().size()) == 1) {
                    // only nodes when there is only one predicate difference
                    if (rule_it2->rule_ptr->get_preconditions().includes_predicates(rule_it1->rule_ptr->get_preconditions())) {
                        add_edge(*rule_it1, *rule_it2);
                    }
                    else if (rule_it1->rule_ptr->get_preconditions().includes_predicates(rule_it2->rule_ptr->get_preconditions())) {
                        add_edge(*rule_it2, *rule_it1);
                    }
                }
            }
        }
    }
    remove_completely_explained_nodes();
}

template<typename AtomT>
RuleWithCoverageSet<AtomT> RuleSubsumptionGraph<AtomT>::get_leafs() const {
    RuleWithCoverageSet<AtomT> result;
    BOOST_FOREACH(const typename Vertex<AtomT>::Ptr &leaf_vertex, leafs) {
        result.insert(leaf_vertex->rulecov);
    }
    return result;
}

template<typename AtomT>
RuleWithCoverageSet<AtomT> RuleSubsumptionGraph<AtomT>::get_leafs_and_first_parent() const {
    RuleWithCoverageSet<AtomT> result;
    BOOST_FOREACH(const typename Vertex<AtomT>::Ptr &leaf_vertex, leafs) {
        result.insert(leaf_vertex->rulecov);
        RuleWithCoverageSet<AtomT> parents = get_rule_parents(leaf_vertex->rulecov);
        result.insert(parents.begin(),parents.end());
    }
    return result;
}

template<typename AtomT>
RuleWithCoverageSet<AtomT> RuleSubsumptionGraph<AtomT>::get_rule_parents(const RuleWithCoverage<AtomT>& rule) const {
    RuleWithCoverageSet<AtomT> result;
    typename VertexMap<AtomT>::const_iterator rule_vertex_it = vertex_map.find(rule.rule_ptr);
    assert(rule_vertex_it != vertex_map.end());
    BOOST_FOREACH(const typename Vertex<AtomT>::Ptr &parent_vertex, rule_vertex_it->second->inverse_adjacency_list) {
        result.insert(parent_vertex->rulecov);
    }
    return result;
}

template<typename AtomT>
void RuleSubsumptionGraph<AtomT>::remove_leaf(const RuleWithCoverage<AtomT>& rule) {
    remove_vertex(rule);
}

template<typename AtomT>
bool RuleSubsumptionGraph<AtomT>::remove_leafs_not_in(const RuleWithCoverageSet<AtomT>& ruleset) {
    RuleWithCoverageSet<AtomT> leafs = get_leafs();
    BOOST_FOREACH(const RuleWithCoverage<AtomT>& good_rule, ruleset) {
        leafs.erase(good_rule);
    }
    BOOST_FOREACH(const RuleWithCoverage<AtomT>& bad_leaf, leafs) {
        remove_leaf(bad_leaf);
    }
    return !leafs.empty(); // if nothing to remove -> false
}

}

#endif