/*
 * LFIT conflict solver
 * Copyright (C) 2015  David Martínez (dmartinez@iri.upc.edu)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef REXD_LFIT_CONFLICT_SOLVER_H
#define REXD_LFIT_CONFLICT_SOLVER_H 1

#include <lfit/lfit_types.h>


namespace lfit {
    class ConflictArray {
      public:
        #if __cplusplus > 199711L // C++11
            virtual ~ConflictArray() = default;
        #else
            virtual ~ConflictArray() {}
        #endif
        virtual bool is_there_a_conflict(const int rule_idx1, const int rule_idx2) =0;
    };
    
    
    class ConflictArrayCoveringTransitions : public ConflictArray {
      public:
        template<typename AtomT>
        ConflictArrayCoveringTransitions(const RuleWithCoverageSet<AtomT>& ruleset, const TransitionGroup<AtomT>& transitions);
        virtual bool is_there_a_conflict(const int rule_idx1, const int rule_idx2);
      private:
        std::vector< std::vector<float> > coverage_table;
    };
    
    typedef boost::shared_ptr<ConflictArray> ConflictArrayPtr;
    
    
    template<typename AtomT>
    class RuleConflictSolver {
      private:
        RuleWithCoverageSet<AtomT> ruleset;
        ConflictArrayPtr conflict_array_ptr;
        
        RuleWithCoverageSet<AtomT> transform_indexed_ruleset_to_standard(const IndexedRuleSet& indexed_ruleset) const;
        
        void generate_conflict_array_transitions(const TransitionGroup<AtomT>& transitions);
        
      public:
        RuleConflictSolver(const lfit::RuleWithCoverageSet<AtomT>& ruleset, const TransitionGroup<AtomT>& transitions, const ConfigReader& config_reader);
        
        RuleWithCoverageSet<AtomT> get_best_subset(const TransitionGroup<AtomT>& transitions, const ConfigReader& config_reader, const bool optimal, const int verbose);
    };
}

#include <lfit/lfit_conflict_solver_template_imp.h>

#endif