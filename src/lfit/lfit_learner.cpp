#include "lfit_learner.h"
#include <lfit/lfit_types.h>

LFITLearner::LFITLearner(const ConfigReader& config_reader) :
    config_reader(config_reader),
    iterations_skipping_learning(config_reader.get_variable<uint>("lfit_learner_relearn_max_iterations")) // force relearn in the first iteration
{
    lfit_rules_path = config_reader.get_path("lfit_rules_path");
    transitions_learning_path = config_reader.get_path("transitions_tmp_path");
    learner_path = config_reader.get_binary_path("learner_path");
    max_action_variables = config_reader.get_variable<uint>("lfit_learner_max_action_variables");
    maximum_number_preconditions = config_reader.get_variable<uint>("lfit_learner_max_preconditions");
}

float LFITLearner::get_rules_score(const TransitionGroup<TypedAtom>& transitions, const Typed::RuleSet& rules, const uint verbose)
{
    lfit::RuleWithCoverageSet<TypedAtom> lfit_rules;
    // Only default false is supported
    std::set<TypedAtom> default_false_literals;
    BOOST_FOREACH(const Typed::RulePtr& rule_ptr, rules) {
        if (rule_ptr->name == ACTION_NAME_SET_LITERAL_DEFAULT) {
            default_false_literals.insert(RuleUtils::get_literal_default_from_rule(*rule_ptr));
        }
        else {
            uint n_total = rule_ptr->count_covering_transitions_preconditions(transitions);
            uint n_covered = n_total; // n_covered is actually not needed
            lfit::RuleWithCoverage<TypedAtom> new_lfit_rule(rule_ptr, lfit::Coverage(n_covered, n_total));
            lfit_rules.insert(new_lfit_rule);
        }
    }
    TransitionGroup<TypedAtom> score_transitions(transitions.ppddl_object_manager);
    for(TransitionSet<TypedAtom>::const_iterator transition_it = transitions.begin_set(); transition_it != transitions.end_set(); ++transition_it) {
        Transition<TypedAtom> new_transition(transition_it->get_prev_state(),
                                             transition_it->get_action(),
                                             transition_it->get_post_state(),
                                             transition_it->get_reward(),
                                             default_false_literals);
        score_transitions.add_transition(new_transition, transition_it->get_number_repetitions());
    }
    
    return lfit_rules.get_score(score_transitions, config_reader, verbose);
}

bool LFITLearner::check_if_learning_is_needed(const TransitionGroup<TypedAtom>& transitions, const Typed::RuleSet& previous_rules, const uint verbose)
{
    if (transitions.size_historial() < config_reader.get_variable<uint>("planning_horizon")) {
        return true;
    }
    
    float score = get_rules_score(transitions, previous_rules, verbose);
    std::cerr << "Prev rules learned score is " << score << std::endl;
    if (score > config_reader.get_variable<float>("lfit_learner_relearn_threshold")) {
        return false;
    }
    else {
        return true;
    }
}

Typed::RuleSet LFITLearner::learn_quiet(const TransitionGroup<TypedAtom>& transitions, const Typed::RuleSet& previous_rules)
{
    CINFO("planning") << "Learning... " << NOENDL;

    std::streambuf* cout_sbuf = std::cout.rdbuf(); // save original sbuf
    std::ofstream   fout("/dev/null");
    std::cout.rdbuf(fout.rdbuf()); // redirect 'cout' to a 'fout'
    Typed::RuleSet result = learn(transitions, previous_rules);
    std::cout.rdbuf(cout_sbuf); // restore the original stream buffer

    CINFO("planning") << "learned  " << result.size() << " rules.";

    return result;
}

void LFITLearner::write_lfit_transitions_to_file(const TransitionGroup<TypedAtom>& transitions) const
{
    TransitionGroup<TypedAtom> symbolic_transitions = transitions.get_symbolic_lfit_transitions_unordered(max_action_variables, 
                                                                                                          config_reader.get_variable<bool>("lfit_learner_noaction_vars_combinations"));
    symbolic_transitions.name_grounding();
    
    Domain domain_lfit(config_reader);
    domain_lfit.index_transitions(symbolic_transitions);
    TransitionGroup<TypedAtom> symbolic_transitions_non_closed_world = domain_lfit.get_transitions_no_closed_world_unordered(symbolic_transitions);
    
    std::stringstream ss_domain_types;
    domain_lfit.write_LFIT_predicate_types(ss_domain_types);
    symbolic_transitions_non_closed_world.write_lfit_transitions_to_file(transitions_learning_path, 
                                                                         ss_domain_types.str());
}


Typed::RuleSet LFITLearner::learn(const TransitionGroup<TypedAtom>& transitions, const Typed::RuleSet& previous_rules)
{
    if ( (iterations_skipping_learning++ < config_reader.get_variable<uint>("lfit_learner_relearn_max_iterations")) && 
         !check_if_learning_is_needed(transitions, previous_rules, 2)) {
        CINFO("planning") << "LFITLearner: " << "Skipping learning as rules are already good enough.";
        return previous_rules;
    }
    iterations_skipping_learning = 0;
    
    CINFO("planning") << "LFITLearner: " << "LFIT learner started";
    TransitionGroup<TypedAtom> normalized_transitions = transitions.get_normalized_transitions();
    write_lfit_transitions_to_file(normalized_transitions);
    
    learn_lfit();
    LOG(INFO) << "LFIT learner finished";
    
    // get learned rules
    ReaderLFIT reader_lfit(lfit_rules_path);
    Typed::RuleSet new_rules;
    LOG(INFO) << "Reading generated rules";
    reader_lfit.read_ruleset(new_rules, normalized_transitions, config_reader);
    
    return new_rules;
}

void LFITLearner::generate_lfit_rules(const TransitionGroup<TypedAtom>& transitions) const
{
    CINFO("planning") << "LFITLearner: " << "LFIT learner started";
    write_lfit_transitions_to_file(transitions);
    learn_lfit();
    LOG(INFO) << "LFIT learner finished";
}


bool LFITLearner::learn_lfit() const
{
    bool lf1t = true; // true = lf1t  |  false = lfkt
    std::string lfit_command = learner_path + " " + transitions_learning_path;
    if (lf1t) { // LF1T
        lfit_command += " 0";
        lfit_command += " 1"; // specialization
//         lfit_command += " 1"; // delay
//         lfit_command += " " + boost::lexical_cast<std::string>(maximum_number_preconditions);
    }
    else { // LFKT (LUST)
        lfit_command += " 1";
        lfit_command += " 3"; // probabilistic
    }
    // probabilistic
    lfit_command += " > " + lfit_rules_path;
//     if (verbose < 1) {
//         lfit_command += " &>/dev/null";
//     }
    
    LOG(INFO) << "Executing LFIT: " + lfit_command;
    int res = system(lfit_command.c_str());
    if ((res >> 8) == 127) {
        // /bin/sh not being able to find the program to execute.
        LOG(ERROR) << "Learner executable cannot be found!!!";
        throw std::ios_base::failure("Learner executable couldn't be found");
    }
    else if (res != 0) {
        LOG(WARNING) << "Learner exited with error " << res;
        throw std::runtime_error("LFIT learner failed");
    }
    
    return true;
}


