/*
 * LFIT reader and parser
 * Copyright (C) 2015  David Martínez <dmartinez@iri.upc.edu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#define DEBUG_VALUE 0

#include "lfit_reader.h"
#include <lfit/lfit_parser.h>

/**
 * \class ReaderLFIT
 */

template<typename AtomT>
std::set<RuleWithCoverage<AtomT> > read_lfit_rules(const ConfigReader& config_reader, const Domain& domain, const std::string& rules_file_content) {
    ParserLFIT lfit_parser;
    std::istringstream iss(rules_file_content);
    bool aggressive_prunning = config_reader.get_variable<bool>("lfit_learner_aggressive_prunning");
    
    std::string line;
    std::set<RuleWithCoverage<AtomT> > lfit_rules;
    while (std::getline(iss, line)) {
        if (line != "") {
            RuleWithCoverage<AtomT> lfit_rule_cov = lfit_parser.parse_symbolic_rule<AtomT>(line, domain, aggressive_prunning);
            if (lfit_rule_cov.rule_ptr) {
                if ( lfit_rule_cov.is_good_candidate(config_reader) ) {
                    lfit_rules.insert(lfit_rule_cov);
                }
                
                // Try inverse rule
                RuleWithCoverage<AtomT> inverse_lfit_rule_cov = lfit_rule_cov.get_inverse_rule();
                if ( inverse_lfit_rule_cov.is_good_candidate(config_reader) ) {
                    lfit_rules.insert(inverse_lfit_rule_cov);
                }
            }
        }
    }
    return lfit_rules;
}

ReaderLFIT::ReaderLFIT(const std::string& file_path_): 
    FileReader(file_path_),
    debug(DEBUG_VALUE),
    file_path(file_path_)
{}

void ReaderLFIT::read_ruleset(Typed::RuleSet& read_ruleset, 
                              const TransitionGroup<TypedAtom>& transitions,
                              const ConfigReader& config_reader)
{
    std::string rules_file_content = utils::get_file_contents(file_path);
    bool use_indexed = (DEBUG_VALUE == 0);
    Domain domain(config_reader);
    domain.index_transitions(transitions);
    std::set<TypedAtom> default_false_atoms;
    
    CLOG_IF(debug >= 1, INFO, "trivial") << "Reading rules";
    if (use_indexed) {
        std::set<RuleWithCoverage<IndexedAtom> > lfit_rules = read_lfit_rules<IndexedAtom>(config_reader, domain, rules_file_content);
        CLOG_IF(debug >= 0, INFO, "trivial") << "Rule selection with " << lfit_rules.size() << " candidates";
        
        ProbabilisticLFITAnalyzer probabilistic_analyzer(DEBUG_VALUE, config_reader);
        TransitionGroup<IndexedAtom> transitions_indexed = domain.transform_transition_group_to_indexed_unordered(transitions);
        std::set<IndexedAtom> default_false_atoms_indexed;
        RuleSet<IndexedAtom> indexed_ruleset = probabilistic_analyzer.select_rules<IndexedAtom>(lfit_rules, transitions_indexed, default_false_atoms_indexed);
        
        BOOST_FOREACH(const IndexedAtom& indexed_atom, default_false_atoms_indexed) {
            default_false_atoms.insert(domain.transform_atom_to_typed(indexed_atom));
        }
        read_ruleset = domain.transform_rule_set_to_typed(indexed_ruleset);
    }
    else {
        std::set<RuleWithCoverage<TypedAtom> > lfit_rules = read_lfit_rules<TypedAtom>(config_reader, domain, rules_file_content);
        CLOG_IF(debug >= 1, INFO, "trivial") << "Rule selection (slow) with " << lfit_rules.size() << " candidates";
        
        ProbabilisticLFITAnalyzer probabilistic_analyzer(DEBUG_VALUE, config_reader);
        read_ruleset = probabilistic_analyzer.select_rules<TypedAtom>(lfit_rules, transitions, default_false_atoms);
    }
    
//     read_ruleset.update_outcome_probabilities(transitions);
    // Add rules to set literal defaults
    BOOST_FOREACH(const TypedAtom& default_false_atom, default_false_atoms) {
        read_ruleset.push_back(RuleUtils::create_set_literal_default_rule(default_false_atom, false));
    }
    
    CLOG_IF(debug >= 1, INFO, "trivial") << "Finished selection of LFIT rules";
}

    
