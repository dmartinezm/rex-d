/*
 * LFIT probabilistic postprocessing
 * Copyright (C) 2015  David Martínez (dmartinez@iri.upc.edu)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef REXD_PROBABILISTIC_LFIT_H
#define REXD_PROBABILISTIC_LFIT_H 1

#include <boost/optional.hpp>
#include <boost/lambda/bind.hpp>
#include <boost/lambda/lambda.hpp>
#include <symbolic/reader_base.h>
#include <symbolic/domain.h>
#include <rexd/config_reader.h>
#include <lfit/lfit_types.h>
#include <lfit/subsumption_graph.h>
#include <lfit/lfit_conflict_solver.h>

/**
 * \class ProbabilisticLFITAnalyzer
 * \brief Chooses the best subset of LFIT rules to represent a domain
 * 
 */
class ProbabilisticLFITAnalyzer
{
public:
    ProbabilisticLFITAnalyzer(const uint debug, const ConfigReader& config_reader):
        debug(debug),
        config_reader(config_reader)
    {};
private:
    uint debug; // 0 -> nothing, 1 -> basic algorithm, 2 -> data read, 3 -> data parsed, 4 -> details
    ConfigReader config_reader;
    
    template<typename AtomT>
    float get_score(const lfit::RuleWithCoverageSet<AtomT>& ruleset);
    
    template<typename AtomT>
    RuleSet<AtomT> select_rules_for_each_head(const lfit::MapHeadRules<AtomT>& map_head_rules, 
                                              const TransitionGroup<AtomT>& action_transitions, 
                                              const bool check_default_false, 
                                              std::set<AtomT>& default_false_atoms);
    
    template<typename AtomT>
    lfit::RuleWithCoverageSet<AtomT> select_rules_for_head(const lfit::RuleWithCoverageSet<AtomT>& action_head_rules,
                                                 const TransitionGroup<AtomT>& transitions, 
                                                 const bool all_values);
    
    template<typename AtomT>
    lfit::RuleWithCoverageSet<AtomT> select_best_ruleset(const lfit::RuleWithCoverageSet<AtomT>& current_candidates,
                                                  const TransitionGroup<AtomT>& transitions,
                                                  const int verbose
                                                 );
    
    template<typename AtomT>
    bool remove_conflicting_leafs(const TransitionGroup<AtomT>& head_transitions, const lfit::RuleWithCoverageSet<AtomT>& rules_to_check, 
                                  lfit::RuleSubsumptionGraph<AtomT>& subsumption_graph, 
                                  lfit::RuleWithCoverageSet<AtomT>& best_operators, double& best_candidate_score);
    
    template<typename AtomT>
    boost::optional<RuleWithCoverageSet<AtomT> > check_inverted_rules(const lfit::MapHeadRules<AtomT>& learned_rule_sets, const Predicate<AtomT>& current_head, const TransitionGroup<AtomT>& head_transitions);
    
    template<typename AtomT>
    TransitionGroup<AtomT> obtain_head_transitions(const TransitionGroup<AtomT>& transitions, 
                                                   const Predicate<AtomT>& head, 
                                                   const bool variable_maintains_to_next_state, 
                                                   const bool ignore_head_sign = false);
    
    template<typename AtomT>
    bool check_if_not_maintaining_atom_value_is_better(const MapHeadRules<AtomT>& map_head_rules,
                                                       const float maintains_score, 
                                                       const AtomT& badly_learned_atom,
                                                       const TransitionGroup<AtomT>& transitions,
                                                       lfit::MapHeadRules<AtomT>& learned_rule_sets);
    
    template<typename AtomT>
    lfit::RuleWithCoverageSet<AtomT> get_promising_candidates(const lfit::RuleWithCoverageSet<AtomT>& head_rules,
                                                              const TransitionGroup<AtomT>& head_transitions, 
                                                              const bool all_values);
    
public:
    template<typename AtomT>
    RuleSet<AtomT> select_rules(const std::set< lfit::RuleWithCoverage<AtomT> >& lfit_rules, const TransitionGroup<AtomT>& transitions, std::set<AtomT>& default_false_atoms);
    
};

#include <lfit/probabilistic_lfit_template_imp.h>

#endif