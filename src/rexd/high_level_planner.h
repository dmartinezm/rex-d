/*
 * High level planner to select actions in a RL setup.
 * Copyright (C) 2016 David Martínez (dmartinez@iri.upc.edu)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef HIGHLEVELPLANNER_H
#define HIGHLEVELPLANNER_H

#include <rexd/planner.h>
#include <teacher/failure_analyzer.h>
#include <teacher/failure_analyzer_tools.h>
#include <rexd/explorer.h>
#include <rexd/rexd_scenario.h>

/*!
\class HighLevelPlanner
\brief High level planning methods. Uses a planner in combination with exploration and planning_failures.

It does all the planning related to a reinforcement learning algorithm, including the exploration vs exploitation. Also planning with the help of planning failures is included.
*/
class HighLevelPlanner
{
public:
    std::string transition_context; //!< Context in which the action was chosen: exploration, exploitation, teacher, etc...
    
    /**
     * \brief Constructor
     * 
     * \param config_reader configuration.
     * \param logging_file_ptr_ pointer to ofstream for logging to a file the planner results.
     */
    HighLevelPlanner(const ConfigReader& config_reader, ofstream* logging_file_ptr_);
    
    /**
     * \brief Decides between exploration and exploitation. Also uses planning failures to find alternative plans if enabled.
     * 
     * It does the following steps until an action is obtained:
     * - Explores if state is unknown.
     * - Plans.
     * - Plans with alternative rules (based on planning failures).
     * - Plans with subgoals (based on planning failures / or decreases Vmin).
     * - Confirms dangerous actions.
     * - Returns no plan.
     * 
     * \param scenario scenario in which to plan. Not const because rule alternatives may change rules
     * \param transitions used to see if state-action pairs are known, and validate rule alternatives.
     * \retval planning_failure_opt planning_failure obtained if exploration and standard planning failed.
     * \retval actions_requiring_planning_failure actions requiring planning_failure if any (used to display information to the teacher).
     * \return Planned action if any.
     */
    TypedAtomOpt exploit_or_explore(Scenario& scenario, 
                                      const TransitionGroup<TypedAtom>& transitions, 
                                      PlanningFailureOpt& planning_failure_opt, 
                                      std::vector<std::string>& actions_requiring_planning_failure
                                     );
    
    /**
     * \brief Decides between exploration and exploitation. Also uses planning failures to find alternative plans if enabled.
     * 
     * DEPRECATED. Use only for dangerous actions.
     */
    TypedAtomOpt exploit_or_explore(Scenario& scenario, 
                                      const TransitionGroup<TypedAtom>& transitions, 
                                      PlanningFailureOpt& planning_failure_opt, 
                                      std::vector<std::string>& actions_requiring_planning_failure,
                                      bool& is_dangerous
                                     );
    
    /**
     * \brief Looks for state-action pairs not known already. If any are found, a exploration action is selected.
     * 
     * \param scenario scenario
     * \param transitions previous experiences to check if state-action pairs are known.
     * \param forced if true a exploration action is selected even if state is known.
     * \return Action selected to be explored if any.
     */
    TypedAtomOpt explore(const Scenario& scenario, const TransitionGroup<TypedAtom>& transitions, bool forced);
    
    /**
     * \brief Returns a pointer to the task planner used internally.
     */
    inline PlannerPtr get_planner_ptr() { return planner; };
    
    /**
     * \brief Reset current episode
     * 
     */
    void reset();
    
    /**
     * \brief Disable subgoals for current episode
     * 
     */
    void inline disable_subgoals() { subgoals_disabled = true; };
    
    /**
     * \brief Set acceptable dead end probability
     * 
     */
    void inline set_acceptable_dead_end_probability() { /* acceptable_dead_end_probability = dead_end_probability; */ };


private:
    PlannerPtr planner;
    ofstream* logging_file_ptr;
    
    float expected_reward;
    bool already_confirmed_as_safe; // TODO remove
    bool subgoals_disabled;
    
    float dead_end_probability;
    
    /**
     * \brief Plans using the standard task planner.
     * 
     * \param scenario scenario.
     * \param transitions used for R-MAX to know if state-action pairs are known.
     * \return Planned action if any.
     */
    TypedAtomOpt standard_plan(const Scenario& scenario, const TransitionGroup<TypedAtom>& transitions);
    
    // TODO redo after reworking dangerous rules
    void avoid_exploration_of_dangerous_rules(const Scenario& scenario, TypedAtomOpt& planning_res);
    
    // TODO redo after reworking dangerous rules
    bool confirm_dangerous_actions(const Scenario& scenario, TypedAtomOpt& planning_res);
    bool replan_without_dangerous_actions(const Scenario& scenario, TypedAtomOpt& planning_res, const TransitionGroup<TypedAtom>& transitions);
    
    /**
     * \brief Plans with planning_failures to generate alternative rule sets and to try to obtain an alternative plan.
     * 
     * Using a planning_failure, searches for alternative equivalent rule sets that get a similar score using Pasula's function.
     */
    void check_teacher_demonstration_requested(TypedAtomOpt& planning_res);
    
    /**
     * \brief Plans with planning_failures to generate alternative rule sets and to try to obtain an alternative plan.
     * 
     * Using a planning_failure, searches for alternative equivalent rule sets that get a similar score using Pasula's function.
     */
    TypedAtomOpt planning_with_alternative_rules(Scenario& scenario, const TransitionGroup<TypedAtom>& transitions, FailureAnalyzer& failure_analyzer);
    
    /**
     * \brief Plans with subgoals depending on the algorithm.
     * 
     * Possible algorithms:
     *  - E3 - a set of subgoals (obtained with the planning_failure) as the reward state for the planner.
     *  - V-MIN - decreases temporary Vmin.
     */
    TypedAtomOpt planning_with_subgoals(const Scenario& scenario, FailureAnalyzer& failure_analyzer);
    TypedAtomOpt planning_with_subgoals_vmin(const Scenario& scenario);
    TypedAtomOpt plan_with_subgoals_planning_failures(const Scenario& scenario, FailureAnalyzer& failure_analyzer);
    
    /**
     * \brief Gets action that have the planning_failure predicates in their preconditions.
     */
    std::vector<std::string> get_actions_requiring_planning_failure(const Scenario& scenario, FailureAnalyzer& failure_analyzer);
};

typedef boost::shared_ptr<HighLevelPlanner> HighLevelPlannerPtr;


#endif // HIGHLEVELPLANNER_H
