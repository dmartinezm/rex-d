/*
    Class to manage the configuration parameters.
    Copyright (C) 2016  <David Martínez> <dmartinez@iri.upc.edu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "config_reader.h"

namespace po = boost::program_options;

ConfigReader::ConfigReader()
{

}

bool ConfigReader::init(const std::string& config_path, const std::string& files_path_)
{
    files_path = files_path_ + "/";

    po::options_description options("Allowed options");
    try {
        options.add_options()
        ("help", "produce help message")

        // simulator
        ("simulator",                           po::value<bool>()->default_value(true), "Using simulator")
        ("simulator_port",                      po::value<uint>()->default_value(0), "Simulator port (0 for auto)")
        ("simulator_use_std_cin",               po::value<bool>()->default_value(false), "If enabled, simulator input is received from std::cin.")
        ("simulator_ppddl_init_problem_file",   po::value<std::string>()->default_value("problem"), "Simulator PPDDL problem file path")
        ("simulator_ppddl_current_problem_file",po::value<std::string>()->default_value("current_problem.pddl"), "Simulator updated PPDDL problem file path")
        ("robot_executes_teacher_actions",      po::value<bool>()->default_value(false), "The robot executes actions suggested by the teacher (already included in \"simulator\" variable).")

        // main variables
        ("domain",      po::value<std::string>(), "Domain")
        ("problem",     po::value<std::string>(), "Problem")
        ("learning",    po::value<bool>()->default_value(true), "Use learning")
        ("monitoring",  po::value<bool>()->default_value(false), "Just monitor actions")

        // transitions
        ("transitions_backup_path", po::value<std::string>()->default_value("transitions_full.dat"), "Transitions backup path")
        ("transitions_tmp_path",    po::value<std::string>()->default_value("transitions_tmp.dat"), "Transitions tmp path")

        // visualizer
        ("visualization",                           po::value<bool>()->default_value(false), "Enable gource visualization")
        ("visualization_gource_log_path",           po::value<std::string>()->default_value("gource.log"), "Gource log path")
        ("visualization_gource_captions_log_path",  po::value<std::string>()->default_value("gource_caption.log"), "Gource caption log path")
        ("visualization_avisynth_log_path",         po::value<std::string>()->default_value("video_editing.avs"), "Avisynth script path")
        ("visualization_subtitle_path",             po::value<std::string>()->default_value("visualization.ass"), "Visualization subtitle path")
        ("visualization_subtitle_verbose_path",     po::value<std::string>()->default_value("visualization_verbose.ass"), "Visualization subtitle verbose path")
        ("visualization_enable_expected_reward",    po::value<bool>()->default_value(false), "Enable visualization of expected reward (subtitle)")
        ("visualization_enable_actual_reward",      po::value<bool>()->default_value(true), "Enable visualization of actual reward (subtitle)")
        ("visualization_enable_current_reward",     po::value<bool>()->default_value(false), "Enable visualization of current reward (subtitle)")

        // ppddl
        ("ppddl_domain_file",               po::value<std::string>()->default_value("domain.pddl"), "PDDL domain file path")
        ("ppddl_domain_file_ground_truth",  po::value<std::string>()->default_value("domain_ground_truth.pddl"), "Ground truth PPDDL domain file path (used by simulator)")
        ("ppddl_grounded_rules_path",       po::value<std::string>()->default_value("rules.ppddl"), "Current rules in ppddl format")
        ("ppddl_rules_template_path",       po::value<std::string>()->default_value("template_ppddl"), "Template to generate ppddl rules")
        ("ppddl_rules_generalize_first_param",     po::value<bool>()->default_value(false), "Generalize first param, permiting all types to be param 1 for any action.")
        ("ppddl_rules_generalize_second_param",    po::value<bool>()->default_value(false), "Generalize second param, permiting all types to be param 2 for any action.")
        
        // rddl
        ("rddl_domain_path",               po::value<std::string>()->default_value("domain.rddl_prefix"), "RDDL domain file path")
        ("rddl_domain_template_path",      po::value<std::string>()->default_value("template_mdp.rddl_prefix"), "RDDL domain template file path")
        ("rddl_instance_path",             po::value<std::string>()->default_value("inst.rddl_prefix"), "RDDL instance file path")
        ("rddl_one_action_constraint",     po::value<bool>()->default_value(false), "Add contraint of max 1 action to RDDL domain file.")
        
        // nid
        ("nid_rules_path",              po::value<std::string>()->default_value("rules_nid.dat"),       "Current NID rules")
        ("nid_finish_rule_path",        po::value<std::string>()->default_value("finish_rule.dat"),     "Finish NID rule")
        ("ground_truth_nid_rules_path", po::value<std::string>()->default_value("simulated_rules.dat"), "Ground truth rules")
        
        // lfit
        ("lfit_learner_max_action_variables",         po::value<uint>()->default_value(2),    "Max number of variables (parameters + deictic) in an action.")
        ("lfit_learner_max_preconditions",            po::value<uint>()->default_value(5),    "Max number of preconditions that a rule may have (note that action name is one precondition for LFIT).")
        ("lfit_learner_optimal",                      po::value<bool>()->default_value(false), "Use optimal method to select best subset of planning operators (slow).")
        ("lfit_learner_use_subsumption_tree",         po::value<bool>()->default_value(true),  "Use subsumption tree to speed up learning (not optimal).")
        ("lfit_learner_noaction_vars_combinations",   po::value<bool>()->default_value(false), "Add also combinations of vars not in the action.")
        ("lfit_learner_conflicts_heuristic_max_rules_per_iter", po::value<uint>()->default_value(10000),  "Maximum number of rules to analyze per iteration in heuristic mode.")
        ("lfit_learner_conflicts_heuristic_max_iterations", po::value<uint>()->default_value(100),  "Maximum number of iterations to analyze in heuristic mode.")
        ("lfit_learner_score_use_confidence",         po::value<bool>()->default_value(true), "Use likelihood, confidence and regularization for score function")
        ("lfit_learner_score_regularization_scaling", po::value<float>()->default_value(0.1), "Scaling parameter for regularization")
        ("lfit_learner_score_confidence_interval",    po::value<float>()->default_value(0.2), "Confidence interval considered valid for rules")
        ("lfit_learner_score_optimistic_value",       po::value<float>()->default_value(1.0), "Optimistic value to be used when calculating optimistic score and a change is not covered.")
        ("lfit_autodetect_if_lits_maintains_to_next_state", po::value<bool>()->default_value(false), "Autodetect if literals maintain their value as default to the next state.")
        ("lfit_learner_aggressive_prunning",          po::value<bool>()->default_value(true), "Use aggressive prunning to discard possibly bad rule candidates.")
        ("lfit_learner_relearn_threshold",            po::value<float>()->default_value(0),   "Quality threshold to trigger relearn.")
        ("lfit_learner_relearn_max_iterations",       po::value<uint>()->default_value(50),   "Max iterations until relearn is forced even is quality is below threshold.")
        
        // learner
        ("learner",                               po::value<std::string>()->default_value(LEARNER_PASULA), "Learner to be used")
        ("learner_path",                          po::value<std::string>()->default_value("learner"), "Learned executable path")
        ("pasula_alpha_pen",                      po::value<double>()->default_value(0.25), "Pasula algorithm alpha parameter")
        ("pasula_noise_lower_bound",              po::value<double>()->default_value(1e-9), "Pasula algorithm noise lower bound parameter")
        ("pasula_noise_lower_bound_default_rule", po::value<double>()->default_value(1e-11), "Pasula algorithm noise lower bound for default rule parameter")
        ("lfit_rules_path",                       po::value<std::string>()->default_value("lfit_rules.dat"), "Learned LFIT rules")

        // planner
        ("planner",             po::value<std::string>()->default_value(PLANNER_PRADA), "Planner used")
        ("goal_file",           po::value<std::string>()->default_value("reward.dat"), "Goal file path")
        ("final_reward_action", po::value<bool>()->default_value(false), "Use final reward action")
        ("noop_actions_allowed",po::value<bool>()->default_value(false), "Apply noop actions as a valid action.")

        ("planner_path",            po::value<std::string>()->default_value("gpack"),       "Planner executable path")
        ("planner_parser_path",     po::value<std::string>()->default_value("parser"),      "Planner parser executable path")
        ("planner_algorithm",       po::value<std::string>()->default_value("gourmand"),    "IPPC planner algorithm (gourmand or glutton)")
        ("planner_port",            po::value<uint>()->default_value(0),                    "Planner server port (0 for auto)")
        ("planning_time",           po::value<uint>(),                                      "Planning time in miliseconds")
        ("planning_fast_time",      po::value<uint>(),                                      "Fast planning time in miliseconds")
        ("planning_epsilon",        po::value<double>()->default_value(0.05),               "Planner epsilon")
        ("planning_horizon",        po::value<uint>(),                                      "Planner horizon")
        ("planning_noop_reward",    po::value<float>(),                                     "Planner noop reward")
        ("planning_reward_step",    po::value<float>(),                                     "Minimum reward change to consider that reward has changed")
        
        // prada DEPRECATED
        ("prada_config",            po::value<std::string>()->default_value("config"),  "Prada configuration file")
        ("prada_config_learner",    po::value<std::string>()->default_value("config"),  "Pasula configuration file")
        ("prada_config_teacher",    po::value<std::string>()->default_value("config"),  "Teacher prada configuration file")        

        // planning failure analysis
        ("remove_nop_rules",                po::value<bool>()->default_value(true),     "Remove nop rules to speed up planning")
        ("rule_analysis",                   po::value<bool>()->default_value(true),     "Analyze rules to explain planning failures")
        ("rule_analysis_equivalent_rules",  po::value<bool>()->default_value(false),     "Search for rule alternatives")
        ("rule_analysis_subgoals",          po::value<bool>()->default_value(false),     "Use subgoals")
        
        ("excuses_extended",                                po::value<bool>()->default_value(true),     "Excuses: Use extended candidates")
        ("excuses_recursive",                               po::value<bool>()->default_value(true),     "Excuses: Use recursive candidates")
        ("excuses_reachable",                               po::value<bool>()->default_value(true),     "Excuses: Use reachable candidates")
        ("excuses_reachable_prob_fixed",                    po::value<float>()->default_value(0.5),     "Excuses: Set min prob for a predicate to be reachable")
        ("excuses_reachable_prob_teacher",                  po::value<bool>()->default_value(false),    "Excuses: Learn min prob for reachable from a teacher")
        ("excuses_temporal",                                po::value<bool>()->default_value(false),    "Excuses: Use temporal candidates")
        ("excuses_learn_from_teacher",                      po::value<bool>()->default_value(false),    "Excuses: Learn predicates quality from the user")
        ("excuses_duplicate_penalty_after_first_predicate", po::value<bool>()->default_value(true),     "Excuses: Duplicate reward penalty after first excuse predicate")
        ("excuses_allow_candidates_in_current_state",       po::value<bool>()->default_value(false),    "Excuses: Allow selecting candidates present in current state")

        // exploration
        ("exploration",                 po::value<bool>()->default_value(true), "Use exploration")
        ("exploration_known_threshold", po::value<uint>()->default_value(2), "Number of experiences to consider a state-action as known")
        ("use_rmax",                    po::value<bool>()->default_value(false), "Use R-MAX algorithm")
        ("use_vmin",                    po::value<bool>()->default_value(false), "Use R-MAX with V-MIN algorithm")
        ("rmax_reward",                 po::value<float>()->default_value(15), "Rmax reward")
        ("vmin_value",                  po::value<float>()->default_value(0.1), "Vmin value")
        ("vmax_value",                  po::value<float>()->default_value(15), "Vmax value")
        ("confirm_dangerous_actions",   po::value<bool>()->default_value(false), "Ask for confirmation before executing dangerous actions.")

        // Teacher and auto-teacher
        ("teacher",                             po::value<bool>()->default_value(true), "Teacher available")
        ("teacher_can_fail",                    po::value<bool>()->default_value(true), "Teacher actions can fail.")
        ("auto_teacher",                        po::value<bool>()->default_value(true), "User automatic teacher")
        ("auto_teacher_excuse_autoexecute",     po::value<bool>()->default_value(true), "Automatically execute recommended action when matches excuse")
        ("auto_teacher_autodead",               po::value<bool>()->default_value(false), "Set as dead when autoteacher does not find a plan")
        ("auto_teacher_forced",                 po::value<bool>()->default_value(false), "All autoteacher plans are forced")
        ("auto_teacher_infinite",               po::value<bool>()->default_value(true), "Retry planning until a satisfactory solution is found")
        ("auto_teacher_infinite_retries",       po::value<uint>()->default_value(5), "Number of autoteacher retries)")
        ("auto_teacher_infinite_full_retries",  po::value<uint>()->default_value(3), "Number of full retries (with full planning")
        ("auto_teacher_dangerous_actions",      po::value<bool>()->default_value(false), "Use autoteacher to confirm dangerous actions")

        // Auto experiments
        ("auto_experiments",                po::value<bool>()->default_value(true), "Start new episodes automatically")
        ("auto_experiments_num",            po::value<uint>()->default_value(10), "Number of episodes per problem")
        ("auto_experiments_auto_finish",    po::value<bool>()->default_value(true), "Finish after completing a problem")
        ("auto_experiments_auto_next",      po::value<bool>()->default_value(false), "Continue with the following problem (if exists)")
        ("auto_experiments_auto_next_num",  po::value<uint>()->default_value(1), "Number of different problems to execute")
        ("auto_experiments_command",        po::value<bool>()->default_value(false), "Request new command after completing an scenario")
        ("auto_experiments_increasing_vmin",            po::value<bool>()->default_value(false), "Increasing Vmin")
        ("auto_experiments_increasing_vmin_step",       po::value<uint>()->default_value(3), "Number of episodes before increasing Vmin")
        ("auto_experiments_increasing_vmin_num",        po::value<uint>()->default_value(1), "Number of times to increase Vmin")
        ("auto_experiments_increasing_vmin_quantity",   po::value<float>()->default_value(1.0), "Quantity Vmin is increased")
        
        ;

        std::ifstream ifs(config_path.c_str());
        po::store(po::parse_config_file(ifs, options), vm);
        ifs.close();
//         po::store(po::parse_command_line(argc, argv, options), vm);
        po::notify(vm);

        if (vm.count("help")) {
            std::cout << options << "\n";
            exit(0);
        }
    }
    catch(std::exception& e) {
        std::cerr << "ConfigReader: " << config_path << " - ERROR: " << e.what() << "\n";
        std::cout << options << "\n";
        exit(-1);
    }
    catch(...) {
        std::cerr << "Exception of unknown type!\n";
        return false;
    }
    return true;
}

ConfigReader::ConfigReader(const ConfigReader& other):
    files_path(other.files_path)
{
    vm = other.vm;
}



