/*
 * REX_D scenario class to work with a RDDL representation
 * Copyright (C) 2015  David Martínez (dmartinez@iri.upc.edu)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#include "rddl_scenario.h"
#include <parsers/rddl_reward_parser.h>

using namespace RDDL;

RDDLScenario::RDDLScenario(const RexdScenario& rexd_scenario): 
    RexdScenario(rexd_scenario)
{ }

RDDLScenario::RDDLScenario(const Scenario& scenario): 
    RexdScenario(scenario)
{ }


void RDDLScenario::prepare_RMAX_rules(Typed::RuleSet& complete_rules, RewardFunctionGroup& goals_ppddl, std::set<TypedAtom>& empty_goal_actions) const
{
    goals_ppddl = RewardFunctionGroup(goals);
    
    // RMAX explore actions
    if (use_rmax) {
        Explorer explorer;
        complete_rules = explorer.get_rmax_ruleset(*this, *transitions_ptr, explore_threshold, rmax_reward);
    }
    else {
        complete_rules = rules;
    }
    
    // Add rule goals to global goals
    BOOST_FOREACH(const Typed::RulePtr& rule_ptr, complete_rules) {
        BOOST_FOREACH(const RewardFunction::Ptr& goal_ptr, rule_ptr->goals) {
            Goal goal = *static_cast<Goal*>(&*goal_ptr);
            
            PredicateGroup<TypedAtom> goal_preconditions = goal.get_predicates();
            if (rule_ptr->is_plannable()) {
                goal_preconditions.insert(Predicate<TypedAtom>(*rule_ptr, true));
            }
            
            static_cast<RDDLGoal*>(&**goals_ppddl.begin())->add_simple_reward(goal_preconditions, goal.get_internal_reward());
            
            if (rule_ptr->is_plannable()) {
                TypedAtom action = *rule_ptr;
                action.make_symbolic();
                empty_goal_actions.insert(action);
            }
        }
    }
    
    complete_rules.remove_nop_rules();
}

void RDDLScenario::obtain_rddl_symbolic_transition(Predicate<TypedAtom>& out_predicate,
                                                   PredicateGroup<TypedAtom>& preconditions,
                                                   TypedAtom& action) const
{
    std::vector<PPDDL::FullObject> head_params_v = out_predicate.params;
    std::vector<PPDDL::FullObject> prec_params_V = preconditions.get_params();
    std::vector<PPDDL::FullObject> action_params_V = action.params;
    std::set<PPDDL::FullObject> head_params_s;
    std::set<PPDDL::FullObject> body_params_s;
    std::set<PPDDL::FullObject> deictic_params_s;
    head_params_s.insert(head_params_v.begin(), head_params_v.end());
    body_params_s.insert(prec_params_V.begin(), prec_params_V.end());
    body_params_s.insert(action_params_V.begin(), action_params_V.end());
    // deictic_params_s = body_params_s - head_params_s;
    std::set_difference(body_params_s.begin(), body_params_s.end(), 
                        head_params_s.begin(), head_params_s.end(), 
                        std::inserter(deictic_params_s, deictic_params_s.end()));
    
    std::vector<PPDDL::FullObject> original_params;
    original_params.insert(original_params.end(), head_params_s.begin(), head_params_s.end());
    original_params.insert(original_params.end(), deictic_params_s.begin(), deictic_params_s.end());
    std::vector<PPDDL::FullObject> new_params(original_params);
    symbolic::make_vector_params_symbolic(new_params);
    
    // two step renaming so there are no overwrites
    for (size_t i = 0; i < new_params.size(); ++i) {
        out_predicate.change_param_name(original_params[i].name, new_params[i].name + "aux");
        preconditions.change_param_name(original_params[i].name, new_params[i].name + "aux");
        action.change_param_name(original_params[i].name, new_params[i].name + "aux");
    }
    for (size_t i = 0; i < new_params.size(); ++i) {
        out_predicate.change_param_name(new_params[i].name + "aux", new_params[i].name);
        preconditions.change_param_name(new_params[i].name + "aux", new_params[i].name);
        action.change_param_name(new_params[i].name + "aux", new_params[i].name);
    }
}


MapHeadBody RDDLScenario::get_rddl_transitions_from_rules(const Typed::RuleSet& rules, PredicateGroup<TypedAtom>& literal_default_values) const
{
    MapHeadBody result;
    literal_default_values = RuleUtils::get_literal_defaults_from_ruleset(rules);
    BOOST_FOREACH(const Typed::RulePtr& rule_ptr, rules) {
        if (*rule_ptr != MAKE_TYPED_ATOM_SET_LITERAL_DEFAULT) {
            // Get list predicates
            PredicateGroup<TypedAtom> outcomes_predicates;
            BOOST_FOREACH(const Outcome<TypedAtom>& outcome, rule_ptr->outcomes) {
                outcomes_predicates.add_predicates(outcome);
            }
            
            BOOST_FOREACH(const Predicate<TypedAtom>& out_predicate, outcomes_predicates) {
                Predicate<TypedAtom> new_out_predicate(out_predicate);
                PredicateGroup<TypedAtom> new_preconditions(rule_ptr->get_preconditions());
                TypedAtom new_action(*rule_ptr);
                
                float probability = 0.0;
                float negated_probability = 0.0;
                
                BOOST_FOREACH(const Outcome<TypedAtom>& outcome, rule_ptr->outcomes) {
                    BOOST_FOREACH(const Predicate<TypedAtom>& pred_in_outcome, outcome) {
                        if (pred_in_outcome == out_predicate) {
                            probability += outcome.probability;
                        }
                        else if (pred_in_outcome.get_negated() == out_predicate) {
                            negated_probability += outcome.probability;
                        }
                    }
                }
                if ((probability > 0.0) && (negated_probability > 0.0)) {
                    throw std::domain_error("Error: RDDL don't support probabilistic effects of same predicate and difference signs.");
                }
                if ( (probability > 0.0) && (probability < 1.0) &&
                     (!literal_default_values.has_predicate(out_predicate)) &&
                     (!literal_default_values.has_predicate(out_predicate.get_negated())) ) {
                    // (Bernoulli (prob)) means -> prob of getting pred, (1 - prob) of getting -pred
                    // Therefore add -pred as precondition so it can only change to become pred
                    // Note that if variable is not maintained, negative heads don't make sense so this isn't needed
                    new_preconditions.insert(new_out_predicate.get_negated());
                }
                
                obtain_rddl_symbolic_transition(new_out_predicate, new_preconditions, new_action);
                PairPreconditionsAction new_pair_preconditions_action(Outcome<TypedAtom>(new_preconditions, probability),
                                                                        new_action);
                if (new_out_predicate.positive) {
                    if (rule_ptr->name == ACTION_NAME_NOACTION) {
                        result[new_out_predicate].first.push_back(new_pair_preconditions_action);
                    }
                    else {
                        result[new_out_predicate].first.insert(result[new_out_predicate].first.begin(), new_pair_preconditions_action);
                    }
                }
                else {
                    if (rule_ptr->name == ACTION_NAME_NOACTION) {
                        result[new_out_predicate].second.push_back(new_pair_preconditions_action);
                    }
                    else {
                        result[new_out_predicate].second.insert(result[new_out_predicate].second.begin(), new_pair_preconditions_action);
                    }
                }
            }
        }
    }
    return result;
}

void RDDLScenario::write_domain_constraints(std::ostream& out) const
{
    if (config_reader.get_variable<bool>("rddl_one_action_constraint")) {
        using namespace boost::lambda;
        Typed::RuleSet rules = get_rules();
        // TODO needed??
//         rules.erase(std::remove_if(rules.begin(), rules.end(), bind(&Typed::Rule::is_special, *_1)), // remove special rules
//                     rules.end());
        
        std::set<NonTypedAtom> non_typed_actions;
        BOOST_FOREACH(const Typed::RulePtr& rule_ptr, rules) {
            if (rule_ptr->is_executable()) {
                NonTypedAtom non_typed_action(rule_ptr->name, rule_ptr->get_param_names());
                non_typed_action.make_symbolic();
                non_typed_actions.insert(non_typed_action);
            }
        }
        if (non_typed_actions.size() >= 2) { // at least 2 actions are requried for the contraint to make sense
            std::vector<PPDDL::FullObject> params = rules.front()->params; // assume all rules have same parameters
            symbolic::make_vector_params_symbolic(params);
            out << "  state-action-constraints {" << std::endl;
            // TODO support for more than 1 parameter
            out << "    (forall ( (" << params.begin()->name << " : " << params.begin()->type << ") ) (<= ";
            
            for (size_t i = 1; i < non_typed_actions.size(); ++i) {
                out << "(+ ";
            }
            // Write first action separately
            std::set<NonTypedAtom>::const_iterator non_typed_action_it = non_typed_actions.begin();
            non_typed_action_it->write_PPDDL(out);
            out << " ";
            for (++non_typed_action_it; non_typed_action_it != non_typed_actions.end(); ++non_typed_action_it) {
                non_typed_action_it->write_PPDDL(out);
                out << ") ";
            }
            out << "1));" << std::endl;
            out << "  };" << std::endl;
        }
    }
}

void RDDLScenario::write_domain_transition(std::ostream& out, 
                                           const PairPreconditionsAction& pair_prec_action, 
                                           const std::vector<PPDDL::FullObject>& head_params,
                                           const bool positive_outcome) const
{
    std::vector<PPDDL::FullObject> body_params_v = pair_prec_action.first.get_params();
    std::vector<PPDDL::FullObject> action_params_v = pair_prec_action.second.params;
    std::set<PPDDL::FullObject> head_params_s;
    std::set<PPDDL::FullObject> action_params_s;
    std::set<PPDDL::FullObject> body_params_s;
    std::set<PPDDL::FullObject> deictic_params_not_in_action_s;
    std::set<PPDDL::FullObject> deictic_params_in_action_s;
    head_params_s.insert(head_params.begin(), head_params.end());
    body_params_s.insert(body_params_v.begin(), body_params_v.end());
    body_params_s.insert(action_params_v.begin(), action_params_v.end());
    action_params_s.insert(action_params_v.begin(), action_params_v.end());
    // deictic_params_s = body_params_s - head_params_s;
    std::set_difference(action_params_s.begin(), action_params_s.end(), 
                        head_params_s.begin(), head_params_s.end(), 
                        std::inserter(deictic_params_in_action_s, deictic_params_in_action_s.end()));
    std::set_difference(body_params_s.begin(), body_params_s.end(), 
                        head_params_s.begin(), head_params_s.end(), 
                        std::inserter(deictic_params_not_in_action_s, deictic_params_not_in_action_s.end()));
    BOOST_FOREACH(const PPDDL::FullObject& deictic_param_in_action, deictic_params_in_action_s) {
        deictic_params_not_in_action_s.erase(deictic_param_in_action);
    }
    
    out << "if (^ ";
    if (!deictic_params_in_action_s.empty()) {
        out << " (exists ( ";
        BOOST_FOREACH(const PPDDL::FullObject& deictic_param_in_action, deictic_params_in_action_s) {
            out << "( " << deictic_param_in_action.name << " : " << deictic_param_in_action.type << ") ";
        }
        out << " ) (^ ";
    }
    if (pair_prec_action.second != MAKE_TYPED_ATOM_NOACTION) {
        pair_prec_action.second.write_RDDL(out);
    }
    if (!deictic_params_not_in_action_s.empty()) {
        out << " (exists ( ";
        BOOST_FOREACH(const PPDDL::FullObject& deictic_param, deictic_params_not_in_action_s) {
            out << "( " << deictic_param.name << " : " << deictic_param.type << ") ";
        }
        out << " ) (^ ";
    }
    BOOST_FOREACH(const Predicate<TypedAtom>& body_predicate, pair_prec_action.first) {
        out << " ";
        body_predicate.write_RDDL(out);
    }
    if (!deictic_params_not_in_action_s.empty()) {
        out << "))";
    }
    if (!deictic_params_in_action_s.empty()) {
        out << "))";
    }
    out << " )";
    if (positive_outcome) {
        if (pair_prec_action.first.probability >= 1.0) {
            out << " then (KronDelta true) ";
        }
        else {
            out << " then (Bernoulli (" << pair_prec_action.first.probability << ")) ";
        }
    }
    else {
        if (pair_prec_action.first.probability == 1.0) {
            out << " then (KronDelta false) ";
        }
        else {
            out << " then (Bernoulli (" << (1.0 - pair_prec_action.first.probability) << ")) ";
        }
    }
}

void RDDLScenario::write_domain_actions(std::ostream& out, const MapHeadBody &map_head_bodies, const std::set<TypedAtom>& other_heads, const PredicateGroup<TypedAtom>& literal_default_values, const std::set<TypedAtom>& empty_goal_actions) const
{
    out << "  cpfs {" << std::endl;
    // Prost requires action to applicable so it can obtain rewards from them.
    // If actions change nothing, this dummy predicate is added
    #if __cplusplus > 199711L // C++11
    for (auto emtpy_goal_action : empty_goal_actions) {
    #else
    BOOST_FOREACH(const TypedAtom& emtpy_goal_action, empty_goal_actions) {
    #endif
        TypedAtom dummy_atom("dummy" + emtpy_goal_action.name, emtpy_goal_action.params);
        out << "    ";
        dummy_atom.write_RDDL(out, true);
        out << " = (KronDelta ";
        emtpy_goal_action.write_RDDL(out, false);
        out << ");" << std::endl;
    }
    BOOST_FOREACH(const MapHeadBody::value_type& pair_head_bodies, map_head_bodies) {
        out << "    ";
        pair_head_bodies.first.write_RDDL(out, true);
        if ( (!pair_head_bodies.second.first.empty()) || (!pair_head_bodies.second.second.empty()) ) {
            out << " = (";
            bool first = true;
            uint number_else = 0;
            BOOST_FOREACH(const PairPreconditionsAction& pair_prec_action, pair_head_bodies.second.first) {
                // positive preconditions
                if (!first) {
                    out << " else (";
                }
                write_domain_transition(out, pair_prec_action, pair_head_bodies.first.params, true);
                if (!first) {
                    number_else++;
                }   
                first = false;
            }
            BOOST_FOREACH(const PairPreconditionsAction& pair_prec_action, pair_head_bodies.second.second) {
                // negative preconditions
                if (!first) {
                    out << " else (";
                }
                write_domain_transition(out, pair_prec_action, pair_head_bodies.first.params, false);
                if (!first) {
                    number_else++;
                }
                first = false;
            }
            out << " else (KronDelta ";
            if (literal_default_values.has_predicate(Predicate<TypedAtom>(pair_head_bodies.first, false))) {
                out << "false";
            }
            else if (literal_default_values.has_predicate(Predicate<TypedAtom>(pair_head_bodies.first, true))) {
                out << "true";
            }
            else {
                pair_head_bodies.first.write_RDDL(out, false);
            }
            out << ")";
            for (size_t i = 0; i < number_else; ++i) {
                out << ")";
            }
            out << ");" << std::endl;
        }
        else {
            out << " (KronDelta ";
            pair_head_bodies.first.write_RDDL(out, false);
            out << ");" << std::endl;
        }
    }
    BOOST_FOREACH(const TypedAtom& other_head, other_heads) {
        out << "    ";
        other_head.write_RDDL(out, true);
        out << " = (KronDelta ";
        other_head.write_RDDL(out, false);
        out << ");" << std::endl;
    }
    out << "  };" << std::endl;
}

void RDDLScenario::write_domain_reward(std::ostream& out, const RewardFunctionGroup& goals_ppddl) const
{
    out << "  reward = ";
    goals_ppddl.write_RDDL(out);
    out << ";" << std::endl;
}

void RDDLScenario::write_RDDL_domain(const std::string template_file, std::ostream &out) const
{
    Typed::RuleSet rules_ppddl;
    RewardFunctionGroup goal_ppddl;
    std::set<TypedAtom> empty_goal_actions;
    prepare_RMAX_rules(rules_ppddl, goal_ppddl, empty_goal_actions);
    
    // write init
    out << "domain rddl_mdp {" << std::endl;
    out << "  requirements = {" << std::endl;
    out << "    rewardDeterministic," << std::endl; 
    if (config_reader.get_variable<bool>("rddl_one_action_constraint")) {
        out << "    constrained-state" << std::endl;
    }
    out << "  };" << std::endl;

    // write domain
    domain.write_RDDL_objects(out, state.ppddl_object_manager);
    Domain new_domain_aux(domain);
    // Prost requires action to applicable so it can obtain rewards from them.
    // If actions change nothing, this dummy predicate is added
    #if __cplusplus > 199711L // C++11
    for (auto empty_goal_action : empty_goal_actions) {
    #else
    BOOST_FOREACH(const TypedAtom& empty_goal_action, empty_goal_actions) {
    #endif
        new_domain_aux.index_predicate(Predicate<TypedAtom>(TypedAtom("dummy" + empty_goal_action.name, empty_goal_action.params), true));
    }
    new_domain_aux.write_RDDL_pvariables(out, state.ppddl_object_manager);

    // write rules
    PredicateGroup<TypedAtom> literal_default_values;
    MapHeadBody map_head_bodies = get_rddl_transitions_from_rules(rules_ppddl, literal_default_values);
    // get also symbols that do not change
    std::set<TypedAtom> non_changing_heads;
    non_changing_heads.insert(domain.get_symbolic_atoms().begin(), domain.get_symbolic_atoms().end());
    BOOST_FOREACH(const MapHeadBody::value_type& pair_head_body, map_head_bodies) {
        non_changing_heads.erase(pair_head_body.first);
    }
    // remove constant predicates
    BOOST_FOREACH(Predicate<TypedAtom> constant_predicate, state.get_constants()) {
        constant_predicate.make_symbolic();
        non_changing_heads.erase(constant_predicate);
    }
    write_domain_actions(out, map_head_bodies, non_changing_heads, literal_default_values, empty_goal_actions);
    write_domain_reward(out, goal_ppddl);
    write_domain_constraints(out);
    out << "}" << std::endl;
    out << std::endl;
}

void RDDLScenario::write_RDDL_domain_to_file(const std::string& domain_template_file, const std::string& domain_file_path) const
{
    int DEBUG = 0;
    std::ofstream rules_file(domain_file_path.c_str());
    if (DEBUG > 0)
        CINFO("planning") << "Typed::RuleSet::write_rules_to_file - " << "Writting rules to file: '" << domain_file_path << "'";
    if (rules_file.is_open()) {
        write_RDDL_domain(domain_template_file, rules_file);
    } else
        std::cerr << "Couldn't write rules to " << domain_file_path << std::endl;
    rules_file.close();
}

void RDDLScenario::write_RDDL_instance(std::ostream &out) const
{
    out << "non-fluents nf_inst_mdp {" << std::endl;
    out << "  domain = rddl_mdp;" << std::endl;
    
    // write objects
    out << "  objects {" << std::endl;
    PPDDLObjectManager<PPDDL::FullObject>::TypeObjectsMap type_objects_map = state.ppddl_object_manager.get_ppddl_objects_per_type();
    BOOST_FOREACH(const PPDDLObjectManager<PPDDL::FullObject>::TypeObjectsMap::value_type & pair_type_objects, type_objects_map) {
        out << "    " << pair_type_objects.first << " : {";
        bool first = true;
        BOOST_FOREACH(const PPDDL::FullObject::Name& object_name, pair_type_objects.second) {
            if (!first) out << ", ";
            out << object_name;
            first = false;
        }
        out << "};" << std::endl;
    }
    out << "  };" << std::endl;
    
    // write non-fluents
    out << "  non-fluents {" << std::endl;
    PredicateGroup<NonTypedAtom> non_typed_constants = Predicates::transform_to_nontyped(state.get_constants());
    BOOST_FOREACH(const Predicate<NonTypedAtom> constant_predicate, non_typed_constants) {
        out << "    " << constant_predicate << ";" << std::endl;
    }
    out << "  };" << std::endl;
    out << "}" << std::endl;
    out << std::endl;
    
    // write instance
    out << "instance inst_mdp {" << std::endl;
    out << "  domain = rddl_mdp;" << std::endl;
    out << "  non-fluents = nf_inst_mdp;" << std::endl;
    out << "  init-state {" << std::endl;
    PredicateGroup<NonTypedAtom> non_typed_state = Predicates::transform_to_nontyped(state.get_non_constants());
    BOOST_FOREACH(const Predicate<NonTypedAtom>& non_typed_predicate, non_typed_state) { // initial state or current state??
        if (non_typed_predicate.positive) {
            out << "    " << non_typed_predicate << ";" << std::endl;
        }
    }
    out << "  };" << std::endl;
    out << "  max-nondef-actions = 1;" << std::endl;
    out << "  horizon = " << planning_horizon << ";" << std::endl;
    out << "  discount = " << "1.0" << ";" << std::endl; // hardcoded discount, PROST ignores it anyway...
    out << "}" << std::endl;
}

void RDDLScenario::write_RDDL_instance_to_file(const std::string& instance_file_path) const
{
    int DEBUG = 0;
    std::ofstream instance_file(instance_file_path.c_str());
    if (DEBUG > 0)
        CINFO("planning") << "Typed::RuleSet::write_rules_to_file - " << "Writting rules to file: '" << instance_file_path << "'";
    if (instance_file.is_open()) {
        write_RDDL_instance(instance_file);
    } else
        std::cerr << "Couldn't write rules to " << instance_file_path << std::endl;
    instance_file.close();
}

