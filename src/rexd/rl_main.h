/*
 * Class that implements the reinforcement learning algorithm.
 * Copyright (C) 2016  David Martínez (dmartinez@iri.upc.edu)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef INTELLACT_RL_H
#define INTELLACT_RL_H

#include <climits>
#include <boost/timer.hpp>
#include <boost/algorithm/string.hpp>

#include <symbolic/rules.h>
#include <symbolic/predicates.h>

#include "rexd/config_reader.h"
#include "teacher/auto_teacher.h"
#include "rexd/visualizer.h"
#include <rexd/high_level_planner.h>
#include <rexd/learner.h>

/**
 * \class ReinforcementLearner 
 * \brief Is the base class to be inherited to implement a RL algorithm.
 * 
 * Configuration is loaded from a config file through the init method.
 * All virtual methods have to be reimplemented. 
 * The show_info_* methods are optional though, and are used just to give information to the user.
 * 
 * It can be used with two main modes that are set in the configuration file:
 *  - Standard RL: learning the domain as new experiences are obtained, 
 *    and planning to achieve the desired behaviour while taking into account the selected exploration-exploitation policy.
 *  - Monitoring: The system just analyzes the state to give suggestions and monitor the performance of an user.
 *    No learning nor execution is done in this mode.
 */
class ReinforcementLearner {
public:
    ReinforcementLearner();
    #if __cplusplus > 199711L // C++11
        virtual ~ReinforcementLearner() = default;
    #else
        virtual ~ReinforcementLearner() {}
    #endif

    /**
     * \brief Executes a iteration of the main algorithm. The type of iteration execution is set in the config file.
     * 
     * Executes a RL iteration with planning and learning, or a monitoring iteration.
     * 
     * \retval True if algorithm continues
     * \retval False if algorithm finished
     */
    bool main_iteration();

    /**
     * \brief Learn updated rules for latest action based on all previous transitions.
     */
    void learning();

    /**
     * \brief Plans best action to be executed (taking into account exploration-exploitation).
     * 
     * \return True if everything is OK, false if dead-end.
     */
    bool planning();

    /**
     * \brief Learn updated rules for latest action based on all previous transitions.
     */
    void monitoring();

protected:
    ConfigReader config_reader; //!< Configuration class that is used to get the configuration variables during the whole execution.
    HighLevelPlannerPtr high_level_planner; //!< High level planner
    LearnerPtr learner; //!< learner
    ScenarioPtr scenario_ptr; //!< Scenario including the current set of rules, the current state and the goal
    
    PredicateGroup<TypedAtom> dead_end_excuses; //!< Excuses found when a dead end was reached. Used to avoid dangerous paths/actions.
    AutoTeacherPtr auto_teacher_ptr; //!< Auto teacher class used for doing automatic experiments.

    TransitionGroup<TypedAtom> transitions; //!< Previous experiences
    bool transition_obtained; //!< True if a transition was obtained during the last iteration
    bool repeat_iteration; //!< True if the same iteration has to be repeated without increasing counters nor sending any action / requesting states
    bool teacher_loop_enabled; //!< True if planning is replaced directly for teacher

    State previous_state; //!< State during the previous iteration. Used to create a new transition.
    TypedAtomOpt previous_action; //!< Action executed during the previous iteration. Used to create a new transition.
    float last_reward; //!< Reward obtained during the last iteration
    float expected_reward; //!< Expected reward to be obtained this episode

    Visualizer visualizer; //!< Visualizer class to create a gource parseable log file. (To create videos)
    ulong current_problem; //!< Problem number (for automatic sets of experiments)
    ulong current_episode; //!< Episode number (for automatic sets of experiments)
    ulong current_episode_iteration; //!< Iteration in this episode (for automatic sets of experiments, and detecting infinite loops)
    ulong total_number_iterations; //!< Total number of iterations (for automatic sets of experiments)

    ofstream logging_file; //!< Logging file to save the learner progress. (Can be parsed afterwards to generate matlab plots)

    /**
     * \brief Loads configuration, scenario information, and initilizes the required systems.
     * 
     * Reads configuration from the provided file name.
     * Reads transitions, rules, goals, from disk.
     * Creates planner and learner.
     * Initializes visualizer.
     * 
     * \param config_path path to config file.
     * \param files_path path to folder where files are read and saved.
     */
    bool init(const std::string& config_path, const std::string& files_path = "./");

    /**
     * \brief Saves las transition.
     * Updated transitions file on disk.
     */
    void update_transitions();

    /**
     * \brief Reads a new action that is executed by a teacher/operator.
     */
    virtual TypedAtomOpt read_action() =0;

    /**
     * \brief Obtains the current state.
     */
    virtual State read_state() =0;

    /**
     * \brief Reads obtained reward with last action executed.
     */
    virtual float read_reward() =0;

    /**
     * \brief Resets the scenario, prepares the algorithm for a new episode.
     * 
     * TODO move most functionality to simulator.
     * 
     * \retval true if scenario continues.
     * \retval false if program finished.
     */
    virtual bool reset_scenario() =0;

    /**
     * \brief Confirms with the teacher if the selected action should be executed
     */
    virtual TypedAtomOpt ask_confirmation_dangerous_action(const Scenario& scenario, const TypedAtomOpt& planning_res) =0;

    /*
     * Various helpers to show information to the user.
     */
    virtual void show_info_request_state() =0;
    virtual void show_info_is_goal() =0;
    virtual void show_info_start_learning() =0;
    virtual void show_info_end_learning(const Typed::RuleSet& removed_rules, const Typed::RuleSet& added_rules, std::string context) =0;
    virtual void show_info_start_planning() =0;
    virtual void show_info_end_planning(const TypedAtomOpt& planning_res, const std::string& excuse, const uint plan_length = 10000) =0;
    /**
     * \brief Shows information to the teacher to suggest what kind of changes would be needed by the system.
     */
    virtual void show_info_teacher_request(const std::string& excuse, const std::vector<std::string>& actions_requiring_excuse) =0;


private:
    // methods
    /**
     * \brief Main learning iteration
     */
    bool main_learning_iteration();
    
    /**
     * \brief Main monitoring iteration
     */
    bool main_monitoring_iteration();

    /**
     * \brief Initializes loggers.
     */
    void init_loggers();

    /**
     * Resets rexd temporal variables to start a new episode.
     * Request new command.
     */
    bool reset_rexd();

    /**
     * \brief Request helps to a teacher. The teacher will have to execute an action.
     * 
     * \return True if everything is OK, false if dead-end.
     */
    bool teacher(const PlanningFailureOpt& excuse_opt, std::vector<std::string> actions_requiring_excuse);

    /**
     * \brief Executes an action.
     * 
     * \param action action to be executed.
     * \param teacher true if the action was executed by the teacher. Just used in simulation to simplify the code.
     */
    virtual void execute_action(TypedAtom action, bool teacher = false) =0;

};

#endif

