/*
 * Class to implement exploration policies and utilities.
 * Copyright (C) 2016  David Martínez (dmartinez@iri.upc.edu)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */


#ifndef EXPLORER_H
#define EXPLORER_H

#include <symbolic/scenario.h>
#include <symbolic/transitions.h>


/*!
\class Explorer
\brief Class having all the RL exploring functionality.

It checks if state is known, selects actions for exploration and creates rmax exploration rules.
*/
class Explorer
{

public:
    /*!
    \brief Select an action to be explored.
    
    The rule that has been visited less times (according to transitions) is selected.\n
    If a state-action pair has no rule, a empty rule for it is created.

    \param forced if true, an exploration action is selected even if the state is known. If false and the state is known, no action is selected for exploration.
    \param visited_threshold number of times a rule has to be executed before it is considered as known.
    \return Grounded action to be explored. False if not forced and state is already known.
    */
    TypedAtomOpt select_action_for_exploration(const Scenario& scenario, const TransitionGroup<TypedAtom>& transitions, const bool forced, const uint visited_threshold) const;
    
    /*!
    \brief Check if current state is known given the transitions.

    \return True if state is known.
    */
    bool is_state_known(const Scenario& scenario, const TransitionGroup<TypedAtom>& transitions) const;

    /*!
    \brief Sets a Rmax reward to all unknown rules.

    \param visited_threshold number of times a rule has to be executed before it is considered as known.
    \param rmax Rmax reward to be added to unknown rules.
    \return Rules with an extra Rmax reward if they are unknown.
    */
    Typed::RuleSet get_rmax_ruleset(const Scenario& scenario, const TransitionGroup<TypedAtom>& transitions, const float visited_threshold, const float rmax) const;
    
    /*! \brief Get rules that are considered unknown */
    Typed::RuleSet get_unknown_rules(const Typed::RuleSet& rules, const TransitionGroup<TypedAtom>& transitions,const float visited_threshold) const;

private:
    static Typed::RulePtr create_explore_rule(const Typed::Rule& rule, const float rmax);
    bool is_rule_known(const Typed::Rule& rule, const TransitionGroup<TypedAtom>& transitions, const float visited_threshold) const;
    Typed::RulePtr get_least_explored_rule(const Scenario& scenario, const TransitionGroup<TypedAtom>& transitions, const TypedAtom& action, const int verbose, uint& min_rule_coverage) const;
    Typed::RuleSet get_rmax_rules_for_given_rules(const Typed::RuleSet& rules, const TransitionGroup<TypedAtom>& transitions, const float visited_threshold, const float rmax) const;
};

#endif // EXPLORER_H
