/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2013  dmartinez <email>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "explorer.h"
#include <boost/graph/graph_concepts.hpp>

#define DEBUG_EXPLORATION 0


bool Explorer::is_rule_known(const Typed::Rule& rule, const TransitionGroup<TypedAtom>& transitions, const float visited_threshold) const
{
    int DEBUG = DEBUG_EXPLORATION;
    if (DEBUG > 0) {
        if (rule.dangerous)
            CINFO("explore") << "Not exploring rule " << rule.name << " as it is dangerous.";
    }
    return ((rule.dangerous) || 
            (rule.is_special()) || 
            (rule.count_covering_transitions_preconditions(transitions) >= visited_threshold));
}


Typed::RulePtr Explorer::create_explore_rule(const Typed::Rule& rule, const float rmax)
{
    Typed::RulePtr resulting_rule = rule.make_shared();
    Goal explore_goal(resulting_rule->get_preconditions(), rmax);
    resulting_rule->goals.add_reward_function(explore_goal);
    return resulting_rule;
}


Typed::RuleSet Explorer::get_unknown_rules(const Typed::RuleSet& rules, const TransitionGroup<TypedAtom>& transitions,const float visited_threshold) const
{
    Typed::RuleSet unknown_rules;
    #if __cplusplus > 199711L // C++11
    BOOST_FOREACH(Typed::RuleConstPtr rule_ptr, rules) {
    #else
    BOOST_FOREACH(Typed::RulePtr rule_ptr, rules) {
    #endif
        if (!is_rule_known(*rule_ptr, transitions, visited_threshold)) {
            unknown_rules.push_back(rule_ptr->make_shared());
        }
    }
    return unknown_rules;
}

Typed::RuleSet Explorer::get_rmax_rules_for_given_rules(const Typed::RuleSet& rules, const TransitionGroup<TypedAtom>& transitions, const float visited_threshold, const float rmax) const
{
    Typed::RuleSet explore_rules;
    BOOST_FOREACH(Typed::RulePtr rule_ptr, rules) {
        if (is_rule_known(*rule_ptr, transitions, visited_threshold)) {
            explore_rules.push_back(rule_ptr->make_shared());
        }
        else {
            if (DEBUG_EXPLORATION > 0)
                CINFO("explore") << "Setting RMAX to rule " << rule_ptr->name;
            explore_rules.push_back( create_explore_rule(*rule_ptr, rmax) );
        }
    }
    return explore_rules;
}

Typed::RuleSet Explorer::get_rmax_ruleset(const Scenario& scenario, const TransitionGroup<TypedAtom>& transitions, const float visited_threshold, const float rmax) const
{
    if (DEBUG_EXPLORATION > 0)
        CINFO("explore") << "\033[1;35m" << "Start" << "\033[0m";

    Typed::RuleSet explore_rules;
    BOOST_FOREACH(const TypedAtom& action, scenario.get_action_list()) {
        if (!action.is_special()) {
            Typed::RuleSet action_rules = scenario.get_rules().get_action_rules(action);
            if (!action_rules.empty()) {
                explore_rules.add_rules(get_rmax_rules_for_given_rules(action_rules, transitions, visited_threshold, rmax));
            }
            else {
                if (transitions.get_action_transitions(action).size_historial() < visited_threshold) {
                    // create empty rule
                    if (DEBUG_EXPLORATION > 0)
                        CINFO("explore") << "Creating new empty RMAX rule of action " << action;
                    Typed::SymbolicRule action_rule(action, PredicateGroup<TypedAtom>());
                    explore_rules.push_back( create_explore_rule(action_rule, rmax) );
                }
            }
        }
        else {
            if (action.name == ACTION_NAME_NOACTION) { // Explore also state-only (i.e. no action) rules
                Typed::RuleSet noaction_rules = scenario.get_rules().get_action_rules(MAKE_TYPED_ATOM_NOACTION);
                explore_rules.add_rules(get_rmax_rules_for_given_rules(noaction_rules, transitions, visited_threshold, rmax));
            }
            else { // Add special rules with no changes
                explore_rules.add_rules(scenario.get_rules().get_action_rules(action));
            }
        }
    }
    return explore_rules;
}


Typed::RulePtr Explorer::get_least_explored_rule(const Scenario& scenario, const TransitionGroup<TypedAtom>& transitions, const TypedAtom& action, const int verbose, uint& min_rule_coverage) const
{
    assert(!action.is_special());
    bool avoid_dangerous_actions = scenario.config_reader.get_variable<bool>("confirm_dangerous_actions");
    Typed::RulePtr best_rule_ptr;
    
    Typed::RuleSet action_rules = scenario.get_rules().get_action_rules(action);
    Typed::RuleSet covering_rules = action_rules.get_covering_rules(scenario.get_state());
    if (covering_rules.size() >= 1) {
        CLOG_IF(verbose >=1, INFO, "explore") << "(There are many covering rules) " << NOENDL;
        for (Typed::RuleSet::iterator rule_it = covering_rules.begin(); rule_it != covering_rules.end(); ++rule_it) {
            if ( (!avoid_dangerous_actions) || 
                 (!((*rule_it)->dangerous)) ) 
            {
                uint num_explorations = (*rule_it)->count_covering_transitions_preconditions(transitions);
                if (num_explorations < min_rule_coverage) {
                    min_rule_coverage = num_explorations;
                    best_rule_ptr = boost::make_shared<Typed::SymbolicRule>(*static_cast<const Typed::SymbolicRule*>(&(**rule_it)));
                    CLOG_IF(verbose >=1, INFO, "explore") << "(Unexplored rule found for action: " << action << ", explored only " << num_explorations << " times) ";
                }
            }
        }
    }
    else if (covering_rules.size() == 0) {
        if (action_rules.empty()) { // Rules for this action have not been learned yet (e.g. noaction from LFIT-based learner)
            uint num_explorations = transitions.get_action_transitions(action).size_historial();
            if (num_explorations < min_rule_coverage) {
                CLOG_IF(verbose >=1, INFO, "explore") << "(Unexplored action without rules, not enough transitions for action " << action << ")";
                min_rule_coverage = num_explorations;
                #if __cplusplus > 199711L // C++11
                best_rule_ptr = Typed::RulePtr(new Typed::SymbolicRule(action, PredicateGroup<TypedAtom>()));
                #else
                best_rule_ptr = Typed::RulePtr(boost::shared_ptr<Typed::Rule>(new Typed::SymbolicRule(action, PredicateGroup<TypedAtom>())));
                #endif
            }
            else {
                CLOG_IF(verbose >=1, INFO, "explore") << "(Known rule, enough transitions for action " << action << ", but no rule yet)";
            }
        }
        else {
            // get action transitions  not covered by other action rules (action_rules not empty)
            TransitionGroup<TypedAtom> action_transitions = transitions.get_action_transitions(action);
            TransitionGroup<TypedAtom> not_covered_by_rule_transitions(transitions.ppddl_object_manager);
            for (TransitionHistorial<TypedAtom>::const_iterator action_transition_it = action_transitions.begin_historial();
                 action_transition_it != action_transitions.end_historial();
                 ++action_transition_it) {
                if (action_rules.covers_transition_preconditions(**action_transition_it, transitions.ppddl_object_manager) == 0.0) {
                    not_covered_by_rule_transitions.add_transition(**action_transition_it);
                }
            }
            if (not_covered_by_rule_transitions.size_historial() < min_rule_coverage) {
                CLOG_IF(verbose >=1, INFO, "explore") << "(Unexplored action without rule for current state, not enough transitions for action " << action << ")";
                min_rule_coverage = not_covered_by_rule_transitions.size_historial();
                #if __cplusplus > 199711L // C++11
                best_rule_ptr = Typed::RulePtr(new Typed::SymbolicRule(action, PredicateGroup<TypedAtom>()));
                #else
                best_rule_ptr = Typed::RulePtr(boost::shared_ptr<Typed::Rule>(new Typed::SymbolicRule(action, PredicateGroup<TypedAtom>())));
                #endif
            }
        }
    }
    return best_rule_ptr;
}


bool Explorer::is_state_known(const Scenario& scenario, const TransitionGroup<TypedAtom>& transitions) const
{
    uint known_threshold = scenario.config_reader.get_variable<uint>("exploration_known_threshold");
    CINFO("explore") << "Checking if state is known... " << NOENDL;
    BOOST_FOREACH(const TypedAtom& action, scenario.get_non_special_action_list()) {
        if (get_least_explored_rule(scenario, transitions, action, 1, known_threshold)) { // if rule is obtained, then it is unkown
            return false;
        }
    }
    CINFO("explore") << " True.";
    return true;
}


TypedAtomOpt Explorer::select_action_for_exploration(const Scenario& scenario, const TransitionGroup<TypedAtom>& transitions, const bool forced, const uint visited_threshold) const
{
    int DEBUG = DEBUG_EXPLORATION;
    if (DEBUG > 0)
        CINFO("explore") << "\033[1;35m" << "Start" << "\033[0m";
    TypedAtomOpt selected_action_opt;
    Typed::RulePtr best_rule_ptr;
    uint min_coverage = visited_threshold;
    if (forced) {
        min_coverage = std::numeric_limits< uint >::max(); // Any coverage is fine, doesn't have to be < visited_threshold
    }
    std::vector<TypedAtom> action_list_shuffle = scenario.get_non_special_action_list();
    std::random_shuffle ( action_list_shuffle.begin(), action_list_shuffle.end() );

    for (std::vector< TypedAtom >::const_iterator action_it = action_list_shuffle.begin(); action_it != action_list_shuffle.end(); ++action_it) {
        if (!action_it->is_special()) {
            Typed::RulePtr least_explored_rule = get_least_explored_rule(scenario, transitions, *action_it, 1, min_coverage);
            if (least_explored_rule) {
                best_rule_ptr = least_explored_rule;
            }
        }
    }

    TypedAtomOpt action_to_explore;
    if (forced || (min_coverage < visited_threshold)) {
        if (best_rule_ptr) {
            CINFO("explore") << "Selecting rule " << best_rule_ptr->name << " with min coverage " << min_coverage << NOENDL;
            if (DEBUG > 2)
                CINFO("explore") << "\033[1;35m" << "Best rule is" << *best_rule_ptr << "\033[0m";
            Typed::RuleSet grounded_rules = (*static_cast<const Typed::SymbolicRule*>(&(*best_rule_ptr))).get_pddl_grounded_rules(scenario.get_state(), false);
            std::random_shuffle ( grounded_rules.begin(), grounded_rules.end() );
            assert(!grounded_rules.empty());
            action_to_explore = TypedAtomOpt(*grounded_rules.front());
            CINFO("explore") << " (best grounding: " << *action_to_explore << ")";
        }
        else {
            CINFO("explore") << "\033[1;35m" << "There is no best rule" << "\033[0m";
            if (selected_action_opt) {
                action_to_explore = TypedAtomOpt(*selected_action_opt);
                std::vector<PPDDL::FullObject::Type> param_types = action_to_explore->get_param_types();
                PPDDLObjectManager<PPDDL::FullObject>::CombinationsPPDDLObjects combinations_of_params = *scenario.get_state().ppddl_object_manager.get_combinations_of_objecs_for_given_params(param_types);
                assert(!combinations_of_params.empty());
                std::random_shuffle ( combinations_of_params.begin(), combinations_of_params.end() );
                std::vector<PPDDL::FullObject> random_params;
                for (size_t param_idx = 0; param_idx < param_types.size(); ++param_idx) {
                    random_params.push_back(PPDDL::FullObject(combinations_of_params.front()[param_idx], param_types[param_idx]));
                }
                action_to_explore->ground(random_params);
                CINFO("explore") << " (random grounding for action " << *selected_action_opt << " is " << *action_to_explore << " )";
            }
            else { // no safe action to explore, explore a random grounded action
                Typed::RuleSet grounded_rules = scenario.get_rules().get_pddl_grounded_rules(scenario.get_state());
                grounded_rules.remove_special_rules();
                if (grounded_rules.size() >0) {
                    CINFO("explore") << " (random grounding)";
                    std::random_shuffle ( grounded_rules.begin(), grounded_rules.end() );
                    action_to_explore = TypedAtomOpt(*grounded_rules.front());
                }
                else {
                    CINFO("explore") << " (no action available!!)";
                    action_to_explore = TypedAtomOpt();
                }
            }
        }
    }
    return action_to_explore;
}

