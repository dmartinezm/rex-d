/*
 * REX_D scenario class to work with a PPDDL representation
 * Copyright (C) 2014  David Martínez (dmartinez@iri.upc.edu)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#include "ppddl_scenario.h"

PPDDLScenario::PPDDLScenario(const RexdScenario& rexd_scenario): 
    RexdScenario(rexd_scenario)
{ }

PPDDLScenario::PPDDLScenario(const Scenario& scenario): 
    RexdScenario(scenario)
{ }


void PPDDLScenario::prepare_IPPC_PPDDL_rules_and_state(Typed::RuleSet& complete_rules, StatePtr& state_ppddl_ptr) const
{
    using namespace boost::lambda;
    
    complete_rules = rules;
    state_ppddl_ptr = boost::make_shared<State>(state);
    
    // RMAX explore actions
    if (use_rmax) {
        Explorer explorer;
        complete_rules = explorer.get_rmax_ruleset(*this, *transitions_ptr, explore_threshold, rmax_reward);
    }
    
    complete_rules.remove_nop_rules(); // cannot do below, would remove min reward teacher action from rmax_vmin

    // V-MIN teacher action
    if (use_vmin) {
        // -started() required so teacher can just be called as the first rule
        complete_rules.add_predicate_to_outcomes(Predicate<TypedAtom>(MAKE_TYPED_ATOM_STARTED, true));
        state_ppddl_ptr->update_predicate(Predicate<TypedAtom>(MAKE_TYPED_ATOM_STARTED, false));
        // V-MIN Teacher action
        Typed::RulePtr rule_ppddl_teacher = create_rule_IPPC_PPDDL_teacher(vmin_value);
        complete_rules.push_back(rule_ppddl_teacher);
    }
    
    if (config_reader.get_variable<bool>("final_reward_action")) {
        // add current_reward to final_reward_action as it was already obtained
        Typed::RuleSet::iterator finish_action_rule_it = std::find_if(complete_rules.begin(), complete_rules.end(), *_1 == MAKE_TYPED_ATOM_FINISH_ACTION);
        assert(finish_action_rule_it != complete_rules.end()); // there should be always a finish_action
        assert(*finish_action_rule_it); // there should be always a finish_action
        assert(!(*finish_action_rule_it)->goals.empty()); // there should be always a finish_action with goals
//         (*finish_action_rule_it)->goals.front().set_reward( (*finish_action_rule_it)->goals.front().get_reward() + current_reward);
        (*finish_action_rule_it)->goals.modify_one_reward(current_reward);
    }
    
    complete_rules = complete_rules.get_pddl_grounded_rules(state);
    complete_rules.PPDDL_grounding();
}


void PPDDLScenario::create_IPPC_PPDDL_scenario(Typed::RuleSet& rules_ppddl, StatePtr& state_ppddl_ptr, RewardFunctionGroup& goals_ppddl, DomainPtr& domain_ppddl_ptr) const
{
    prepare_IPPC_PPDDL_rules_and_state(rules_ppddl, state_ppddl_ptr);

    // get PPDDL grounded goal
    goals_ppddl = goals;
    goals_ppddl.PPDDL_grounding();

    // get PPDDL domain.
    domain_ppddl_ptr = DomainPtr(new Domain(config_reader));
    domain_ppddl_ptr->index_rules(rules_ppddl);
    domain_ppddl_ptr->index_predicates(goals_ppddl.get_predicates());

    //optional: adding state. Info about the rules may be enough (exception if excuses available)
    state_ppddl_ptr = boost::make_shared<State>(state); // copy
    state_ppddl_ptr->PPDDL_grounding();
    domain_ppddl_ptr->index_state(*state_ppddl_ptr);

    // add special rules
    Typed::RulePtr rule_ppddl_noop = create_rule_IPPC_PPDDL_noop(domain_ppddl_ptr, goals_ppddl, noop_reward);
    rules_ppddl.push_back(rule_ppddl_noop);
}


void PPDDLScenario::write_IPPC_PPDDL(const std::string template_file, std::ostream &out) const
{
    Typed::RuleSet rules_ppddl;
    StatePtr state_ppddl_ptr;
    RewardFunctionGroup goal_ppddl;
    DomainPtr domain_ppddl_ptr;
    create_IPPC_PPDDL_scenario(rules_ppddl, state_ppddl_ptr, goal_ppddl, domain_ppddl_ptr);

    // write init and domain
    out << utils::get_file_contents(template_file, "<init>" , "<pre_domain>");
    domain_ppddl_ptr->write_PPDDL_grounded_predicates(out);

    // write rules
    std::map<std::string, Typed::RuleSet> rules_per_action = rules_ppddl.get_rules_per_action();
    write_actions_IPPC_PPDDL(out, rules_per_action, goal_ppddl);

    // write problem vars and end
    out << utils::get_file_contents(template_file, "<post_rules>" , "<horizon>");
    out << planning_horizon;
    out << utils::get_file_contents(template_file, "<horizon>" , "<end>");
    out << std::endl;
}


void PPDDLScenario::write_actions_IPPC_PPDDL(std::ostream& out, std::map<std::string, Typed::RuleSet> &rules_per_action, const RewardFunctionGroup& goals_ppddl) const
{
    // Several actions
    for (std::map<std::string, Typed::RuleSet>::iterator action_it = rules_per_action.begin(); action_it != rules_per_action.end(); ++action_it) {
        RewardFunctionGroup rule_goals;
        out << "  (:action " << action_it->first << std::endl;
        out << "    :effect (and" << std::endl;

        // Action has several rules
        for (Typed::RuleSet::iterator rule_it = action_it->second.begin(); rule_it != action_it->second.end(); ++rule_it) {
            // Action has several outcomes
            if ((*rule_it)->has_useful_outcome()) {
                if (!(*rule_it)->get_preconditions().empty()) {
                    out << "      (when ";
                    (*rule_it)->get_preconditions().write_PPDDL(out, (*rule_it)->params);
                }
                (*rule_it)->outcomes.write_PPDDL(out, (*rule_it)->params);
                if (!(*rule_it)->get_preconditions().empty()) {
                    out << ")" << std::endl;
                }
            }

            if (!(*rule_it)->goals.empty()) { // use rule goals
                BOOST_FOREACH(const RewardFunction::Ptr& reward_function_ptr, (*rule_it)->goals) {
                    rule_goals.add_reward_function(*reward_function_ptr);
                }
            }
        }
//         utils::remove_duplicates(rule_goals);
        rule_goals.write_PPDDL(out, std::vector<PPDDL::FullObject>());
        if ((action_it->first != ACTION_NAME_FINISH) && (action_it->first != ACTION_NAME_NOOP))
            goals_ppddl.write_PPDDL(out, std::vector<PPDDL::FullObject>());
        out << "    )" << std::endl;
        out << "  )" << std::endl;
    }
}


Typed::RulePtr PPDDLScenario::create_rule_IPPC_PPDDL_noop(const DomainPtr domain_ptr, const RewardFunctionGroup& goals_ppddl, const float noop_reward)
{
    #if __cplusplus > 199711L // C++11
    Typed::RulePtr rule_noop_ptr = Typed::RulePtr(new Typed::SymbolicRule(MAKE_TYPED_ATOM_NOOP_ACTION, PredicateGroup<TypedAtom>()));
    #else
    Typed::RulePtr rule_noop_ptr = Typed::RulePtr(boost::shared_ptr<Typed::Rule>(new Typed::SymbolicRule(MAKE_TYPED_ATOM_NOOP_ACTION, PredicateGroup<TypedAtom>())));
    #endif

    // obtain domain positive and negative predicates
    PredicateGroup<TypedAtom> positive_known_predicates, negated_known_predicates;
    BOOST_FOREACH(const Predicate<TypedAtom>& domain_pred, domain_ptr->get_predicates()) {
        Predicate<TypedAtom> predicate(domain_pred);
        predicate.positive = true;
        positive_known_predicates.insert(predicate);
        predicate.positive = false;
        negated_known_predicates.insert(predicate);
    }
    PredicateGroup<TypedAtom> rule_noop_preconditions = rule_noop_ptr->get_preconditions();
    rule_noop_preconditions.add_predicates(positive_known_predicates);
    rule_noop_preconditions.add_predicates(negated_known_predicates);
    rule_noop_ptr->set_preconditions(rule_noop_preconditions);
    Outcome<TypedAtom> outcome_pos(positive_known_predicates, 0.5);
    Outcome<TypedAtom> outcome_neg(negated_known_predicates, 0.5);
    rule_noop_ptr->add_outcome(outcome_pos);
    rule_noop_ptr->add_outcome(outcome_neg);

    rule_noop_ptr->goals = goals_ppddl;
    Goal noop_goal(PredicateGroup<TypedAtom>(), noop_reward); // noop reward is small
//     rule_noop_ptr->goals.push_back(goal_ppddl_ptr);
    rule_noop_ptr->goals.add_reward_function(noop_goal);

    return rule_noop_ptr;
}



Typed::RulePtr PPDDLScenario::create_rule_IPPC_PPDDL_teacher(const float min_reward)
{
    PredicateGroup<TypedAtom> rule_teacher_preconditions;
    rule_teacher_preconditions.insert(Predicate<TypedAtom>(MAKE_TYPED_ATOM_STARTED, false)); // not started()
    rule_teacher_preconditions.insert(Predicate<TypedAtom>(MAKE_TYPED_ATOM_FINISHED, false)); // not finished()
    Outcome<TypedAtom> rule_teacher_effect(1.0);
    rule_teacher_effect.insert(Predicate<TypedAtom>(MAKE_TYPED_ATOM_STARTED, true)); // started()
    rule_teacher_effect.insert(Predicate<TypedAtom>(MAKE_TYPED_ATOM_FINISHED, true)); // finished()
    
    #if __cplusplus > 199711L // C++11
    Typed::RulePtr rule_teacher_ptr = Typed::RulePtr(new Typed::SymbolicRule(MAKE_TYPED_ATOM_TEACHER, rule_teacher_preconditions));
    #else
    Typed::RulePtr rule_teacher_ptr = Typed::RulePtr(boost::shared_ptr<Typed::Rule>(new Typed::SymbolicRule(MAKE_TYPED_ATOM_TEACHER, rule_teacher_preconditions)));
    #endif
    rule_teacher_ptr->outcomes.insert(rule_teacher_effect);
    Goal teacher_goal(rule_teacher_preconditions, min_reward);
    rule_teacher_ptr->goals.add_reward_function(teacher_goal);
    return rule_teacher_ptr;
}

void PPDDLScenario::write_IPPC_PPDDL_to_file(const std::string template_file, const std::string file_path) const
{
    int DEBUG = 0;
    std::ofstream rules_file(file_path.c_str());
    if (DEBUG > 0)
        std::cout << "Typed::RuleSet::write_rules_to_file - " << "Writting rules to file: '" << file_path << "'" << std::endl;
    if (rules_file.is_open()) {
        write_IPPC_PPDDL(template_file, rules_file);
    } else
        std::cerr << "Couldn't write rules to " << file_path << std::endl;
    rules_file.close();
}

