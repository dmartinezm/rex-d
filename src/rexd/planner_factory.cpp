/*
    Factory to create planner based on config
    Copyright (C) 2013  David Martínez <dmartinez@iri.upc.edu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "planner_factory.h"

PlannerPtr PlannerFactory::get_standard_planner(const ConfigReader& config_reader)
{
    static PlannerPtr planner_ptr;
    static std::string planner_config_reader_path;
    // If config_reader path changes, create a new planner instance as it may be of a different type
    if (planner_ptr && (planner_config_reader_path != config_reader.files_path)) {
        planner_ptr.reset();
    }
    if (!planner_ptr) {
        planner_config_reader_path = config_reader.files_path;
        if (config_reader.get_variable<std::string>("planner") == PLANNER_PRADA) {
            CWARNING("Planning") << "PRADA planner is deprecated. IPPC is the recommended planner now";
            std::string prada_config_file = config_reader.get_path("prada_config");
            planner_ptr = boost::make_shared<PradaPlanner>(PradaPlanner(prada_config_file));
        }
        else if (config_reader.get_variable<std::string>("planner") == PLANNER_GPACK) {
            planner_ptr = boost::make_shared<IppcGpackPlanner>(IppcGpackPlanner(config_reader));
        }
        else if (config_reader.get_variable<std::string>("planner") == PLANNER_PROST) {
            planner_ptr = boost::make_shared<IppcProstPlanner>(IppcProstPlanner(config_reader));
        }
        else { // error
            std::cerr << "Planner Factory::" << "ERROR!!! UNKNOWN PLANNER TYPE " << config_reader.get_variable<std::string>("planner") << std::endl;
        }
    }
    return planner_ptr;
}
