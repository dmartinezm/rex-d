/*
 * REX_D scenario class to work with a PPDDL representation
 * Copyright (C) 2016  David Martínez (dmartinez@iri.upc.edu)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef PPDDL_SCENARIO_H
#define PPDDL_SCENARIO_H

#include <boost/lambda/lambda.hpp>
#include <rexd/rexd_scenario.h>
#include <rexd/explorer.h>

/**
 * \class PPDDLScenario
 * \brief Extended Scenario to generate PPDDL domains to use with IppcPlanner.
 * 
 * Writes PPDDL domains with all extra rules and predicates needed for R-MAX and V-MIN algorithms.
 */
class PPDDLScenario : public RexdScenario
{
public:
    
    /**
     * \brief Creates RexdScenario. R-MAX and V-MIN are disabled by default.
     */
    PPDDLScenario(const Scenario& scenario);
    
    /**
     * \brief Creates RexdScenario. R-MAX and V-MIN settings are copied. New object uses pointer to same transitions data.
     */
    PPDDLScenario(const RexdScenario& rexd_scenario);
    
    /** \brief Writes a PPDDL problem of the scenario to a file. Uses a template to copy most of the domain constants. */
    void write_IPPC_PPDDL_to_file(const std::string template_file, const std::string file_path) const;
    
    
    
private:
    void prepare_IPPC_PPDDL_rules_and_state(Typed::RuleSet& rmax_rules, StatePtr& state_ppddl_ptr) const;
    
    static Typed::RulePtr create_rule_IPPC_PPDDL_teacher(const float min_reward);
    
    void create_IPPC_PPDDL_scenario(Typed::RuleSet& rules_ppddl, StatePtr& state_ppddl_ptr, RewardFunctionGroup& goals_ppddl, DomainPtr& domain_ppddl_ptr) const;

    void write_IPPC_PPDDL(const std::string template_file, std::ostream &out) const;
    
    void write_actions_IPPC_PPDDL(std::ostream& out, std::map<std::string, Typed::RuleSet> &rules_per_action, const RewardFunctionGroup& goals) const;

    static Typed::RulePtr create_rule_IPPC_PPDDL_noop(const DomainPtr domain_ptr, const RewardFunctionGroup& goals_ppddl, const float noop_reward);
};

#endif // PPDDL_SCENARIO_H
