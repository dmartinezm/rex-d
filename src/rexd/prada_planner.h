/*
 * Wrapper to use PradaPlanner in REX-D.
 * Copyright (C) 2016  David Martínez (dmartinez@iri.upc.edu)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef INTELLACT_PRADA_PLANNER_H
#define INTELLACT_PRADA_PLANNER_H

#include <rexd/planner.h>

#define MT_IMPLEMENT_TEMPLATES

#include <relational/prada.h>
#include <vector>

#include <symbolic/rules.h>
#include <symbolic/predicates.h>
#include <symbolic/state.h>

#define PLAN_TYPE__PRADA 1
#define PLAN_TYPE__A_PRADA 2
#define PLAN_TYPE__UCT 3
#define PLAN_TYPE__SST 4


/**
 * \class PradaPlanner
 * \brief PRADA planner wrapper. Uses libprada implementation.
 * 
 * Inherits functionality from planner.h
 */
class PradaPlanner : public Planner {
public:

    PradaPlanner(std::string config_file_);

    virtual TypedAtomOpt plan(const RexdScenario& rexd_scenario,
                              const bool thoroughly, const int verbose,
                              float& planning_result);

    virtual bool plan_n_actions(const RexdScenario& rexd_scenario,
                                std::vector<TypedAtom>& plan,
                                const bool thoroughly = true, const int verbose = 0,
                                const uint n_actions = std::numeric_limits<uint>::max(),
                                const bool force_effect = false);

    virtual int estimate_distance_to_finish(const RexdScenario &rexd_scenario);

private:
    uint plan_type;
    double discountFactor;
    uint PRADA_horizon, PRADA_horizon_sp;
    uint PRADA_num_samples, PRADA_num_samples_sp;
    double PRADA_noise_softener;
    uint horizon;

    // config vars
    std::string prada_rules_filename;
    std::string prada_state_filename;
    std::string prada_reward_filename;
    std::string prada_symbols_filename;

    std::vector<TypedAtom> pradaPlan(bool thoughtful, float& planning_result, const PPDDLObjectManager<PPDDL::FullObject>& object_manager, bool plan_single_action = false) const;
    TypedAtom prada_action_to_atom(relational::Literal *action, const PPDDLObjectManager<PPDDL::FullObject>& object_manager) const;
};

#endif
