#include "prada_planner.h"


PradaPlanner::PradaPlanner(std::string config_file_)
{

    cout.precision(3);
    MT::String config_file;
    MT::getParameter(config_file, MT::String("confFile"), MT::String(config_file_.c_str()));
    //   cout << "Config-file: " << config_file << endl;
    MT::openConfigFile(config_file);

    // -------------------------------------
    // READ CONFIG
    // -------------------------------------
//     uint randSeed;
//     MT::getParameter(randSeed, "randSeed");
//     srand((unsigned)time(0));

    MT::getParameter(plan_type, "plan_type");
    MT::getParameter(discountFactor, "discountFactor");
    MT::getParameter(PRADA_horizon, "PRADA_horizon");
    MT::getParameter(PRADA_horizon_sp, "PRADA_horizon_sp");
    MT::getParameter(PRADA_num_samples, "PRADA_num_samples");
    MT::getParameter(PRADA_num_samples_sp, "PRADA_num_samples_sp");
    MT::getParameter(PRADA_noise_softener, "PRADA_noise_softener");

    horizon = PRADA_horizon;

    MT::String prada_rules_filename_mt, prada_state_filename_mt, prada_reward_filename_mt, prada_symbols_filename_mt;
    MT::getParameter(prada_rules_filename_mt, "file_rules");
    prada_rules_filename = std::string(prada_rules_filename_mt);
    MT::getParameter(prada_state_filename_mt, "file_state");
    prada_state_filename = std::string(prada_state_filename_mt);
    MT::getParameter(prada_reward_filename_mt, "file_reward");
    prada_reward_filename = std::string(prada_reward_filename_mt);
    MT::getParameter(prada_symbols_filename_mt, "file_symbols");
    prada_symbols_filename = std::string(prada_symbols_filename_mt);
}


int PradaPlanner::estimate_distance_to_finish(const RexdScenario &rexd_scenario)
{
    int plan_length = 0;
    std::vector<TypedAtom> planning_res;
    if (plan_n_actions(rexd_scenario, planning_res, false, false)) {
        plan_length = planning_res.size();
        for (int i = ((int)planning_res.size() - 1); i >= 0; i--) {
            if (planning_res[i].name == "doNothing") {
                plan_length = i; // begins in 0, but final action is previous action
            }
        }
    }
    return plan_length;
}


/**
 * Actual method to prepare planning
 */
TypedAtomOpt PradaPlanner::plan(const RexdScenario& rexd_scenario, const bool thoroughly, const int verbose, float& planning_result)
{
    rexd_scenario.get_state().write_to_file(prada_state_filename);
    Typed::RuleSet simplified_rules(rexd_scenario.get_rules());
    simplified_rules.remove_nop_rules();
    simplified_rules.write_rules_to_file(prada_rules_filename);
    rexd_scenario.get_goals().get_predicates().write_to_file(prada_reward_filename);
    
    // write symbols
    std::stringstream ss;
    rexd_scenario.domain.write_NID(ss);
    utils::write_string_to_file(prada_symbols_filename, ss.str());

    CINFO("planning") << "Planning... " << NOENDL;
    std::streambuf *cout_buf, *cerr_buf;
    if (!verbose) {
        cout_buf = stream_quiet(std::cout); // silent cout and cerr because prada library has too much output
        cerr_buf = stream_quiet(std::cerr);
    }
    std::vector< TypedAtom> result = pradaPlan(thoroughly, planning_result, rexd_scenario.get_state().ppddl_object_manager);
    if (!verbose) {
        stream_restore(std::cout, cout_buf);
        stream_restore(std::cerr, cerr_buf);
    }

    if (result.empty()) {
        CINFO("planning") << " No plan found!";
        return TypedAtomOpt();
    }
    else {
        CINFO("planning") << " Action: " << result.front() << " (" << planning_result << ")";
        return TypedAtomOpt(result.front());
    }
}


bool PradaPlanner::plan_n_actions(const RexdScenario& rexd_scenario, std::vector<TypedAtom>& resulting_plan, const bool thoroughly, const int verbose, const uint n_actions, const bool force_effect)
{
    rexd_scenario.get_state().write_to_file(prada_state_filename);
    Typed::RuleSet simplified_rules(rexd_scenario.get_rules());
    simplified_rules.remove_nop_rules();
    simplified_rules.write_rules_to_file(prada_rules_filename);
    rexd_scenario.get_goals().get_predicates().write_to_file(prada_reward_filename);
    
    // write symbols
    std::stringstream ss;
    rexd_scenario.domain.write_NID(ss);
    utils::write_string_to_file(prada_symbols_filename, ss.str());

    CINFO("planning") << "Planning " << n_actions << " actions... " << NOENDL;
    std::streambuf *cout_buf, *cerr_buf;
    if (!verbose) {
        cout_buf = stream_quiet(std::cout); // silent cout and cerr because prada library has too much output
        cerr_buf = stream_quiet(std::cerr);
    }
    float planning_result;
    resulting_plan = pradaPlan(thoroughly, planning_result, rexd_scenario.get_state().ppddl_object_manager, false);
    if (!verbose) {
        stream_restore(std::cout, cout_buf);
        stream_restore(std::cerr, cerr_buf);
    }
    CINFO("planning") << resulting_plan;
    if (resulting_plan.size()>0) {
        return true;
    }
    else {
        return false;
    }
}


std::vector<TypedAtom> PradaPlanner::pradaPlan(bool thoroughly, float& planning_result, const PPDDLObjectManager<PPDDL::FullObject>& object_manager, bool plan_single_action) const {
    std::cout << "\033[1;34m" << "********************************" << std::endl;
    std::cout << "* " << "libPRADA plan" << "\033[0m" << std::endl;
//     cout<<"********************************"<<endl;

    rnd.seed(rand());

    // -------------------------------------
    //  SET UP LOGIC
    // -------------------------------------

//     cout<<"Reading symbols from file \""<<symbols_filename<<"\"..."<<flush;
    relational::SymL symbols;
    relational::ArgumentTypeL types;
    relational::readSymbolsAndTypes(symbols, types, prada_symbols_filename.c_str());
//     cout<<"done!"<<endl;

    //relational::writeAtomsAndTypes("used_symbols.dat");


    // -------------------------------------
    //   STATE
    // -------------------------------------

//     cout<<"Reading state from file \""<<state_filename<<"\"... "<<flush;
    relational::SymbolicState s;
    ifstream in_state(prada_state_filename.c_str());
    s.read(in_state);
//     cout<<"done!"<<endl;
    //cout<<"State:"<<endl<<s<<endl<<endl;
    relational::reason::setConstants(s.state_constants);
    //cout<<"CONSTANTS:"<<endl;  PRINT(relational::reason::getConstants());
    //cout<<relational::reason::getConstants().N;


    // -------------------------------------
    // REWARD
    // -------------------------------------

//     cout<<"Reading reward from file \""<<reward_filename<<"\"... "<<flush;
    relational::Reward* reward = NULL;
    relational::LitL lits_reward;

    // read from file
    ifstream in_reward;
    std::stringstream ss;
    in_reward.open(prada_reward_filename.c_str());
    while (in_reward.good()) {
        std::string aux_string;
        in_reward >> aux_string;
        ss << aux_string << " ";
    }
    in_reward.close();

    relational::Literal::get(lits_reward, MT::String(ss.str().c_str()));
    reward = new relational::LiteralListReward(lits_reward);

//     cout<<"done!"<<endl;
    std::cout << "Reward is: ";
    reward->write();


    // -------------------------------------
    // RULES
    // -------------------------------------

    cout<<endl;
    relational::RuleSet rules;
    relational::RuleSet::read(prada_rules_filename.c_str(), rules);
//     cout<<"Typed::Rules successfully read from file \""<<rules_filename<<"\"."<<flush;
    //cout<<"Typed::Rules ("<<rules.num()<<"):"<<endl;   rules.write(cout);

#if 1
    // Manually create "doNothing"-rule if desired
    relational::Rule* rule_doNothing = relational::Rule::getDoNothingRule();
    rules.append(rule_doNothing);
    //rule_doNothing->write();
#endif

    // -------------------------------------
    // GROUND THE RULES
    // -------------------------------------

    relational::RuleSet ground_rules;
    relational::RuleSet::ground_with_filtering(ground_rules, rules, relational::reason::getConstants(), s);
//     cout<<endl;
//     cout<<"GROUND RULES: (plenty!!)"<<endl;
    //cout<<"# = "<<ground_rules.num()<<endl;
    //   ground_rules.write();



    // -------------------------------------
    // PLANNERs
    // -------------------------------------

    relational::NID_Planner* planner = NULL;
    if (plan_type == PLAN_TYPE__PRADA) {
        planner = new relational::PRADA_Planner();
        ((relational::PRADA_Planner* ) planner)->setNumberOfSamples(PRADA_num_samples);
        ((relational::PRADA_Planner* ) planner)->setNoiseSoftener(PRADA_noise_softener);
        cout<<"Planner: PRADA..."<<flush;
    }
    else if (plan_type == PLAN_TYPE__A_PRADA) {
        planner = new relational::A_PRADA();
        if (thoroughly) {
            ((relational::A_PRADA* ) planner)->setNumberOfSamples(PRADA_num_samples_sp);
        }
        else {
            ((relational::A_PRADA* ) planner)->setNumberOfSamples(PRADA_num_samples);
        }
        ((relational::A_PRADA* ) planner)->setNoiseSoftener(PRADA_noise_softener);
        cout<<"Planner: A-PRADA..."<<flush;
    }
    else {
        HALT("no planner defined");
    }
    planner->setDiscount(discountFactor);
    if (thoroughly) {
        planner->setHorizon(PRADA_horizon_sp);
    }
    else {
        planner->setHorizon(horizon);
    }
    planner->setGroundRules(ground_rules);
    planner->setReward(reward);
    std::cout << " has been fully set up." << std::endl;
    //PRINT(planner->ground_actions);


    // -------------------------------------
    //    PLANNING
    // -------------------------------------

//     cout<<endl;
    std::vector<TypedAtom> result;

    try {
        if (plan_single_action) {
            relational::Literal* action;
            // std::cout << std::endl << std::endl << "*** Planning for a single action." << std::endl;
            action = planner->plan_action(s);

            if (action != NULL) {
                std::cout << "The planner would like to kindly recommend the following action to you:" << std::endl << *action << std::endl;
                result.push_back( prada_action_to_atom(action, object_manager) );
            }
            else {
                std::cout << "No action has been found." << std::endl;
            }
        }
        else {
            //         cout<<"*** Planning for a complete plan."<<endl;
            relational::LitL plan;
            double value;

            ((relational::PRADA_Planner*) planner)->plan_full(plan, value, s);

            if (plan.d[0] == 0) {
                std::cout << "No plan!!!" << std::endl;
                planning_result = -1.0;
            }
            else {
                std::cout << std::endl;
                std::cout << "Generated plan with value " << value << " to you:" << std::endl << plan << std::endl;
                planning_result = value;

                // write plan
                ofstream myfile;
                myfile.open ("output_plan.txt");
                myfile << plan;
                myfile.close();

//                     relational::Literal *action;
                for (uint i = 0; i < plan.d[0]; i++) {
                    result.push_back(prada_action_to_atom(plan(i), object_manager));
                }
            }
        }

    }
    catch(...) {
        std::cerr << "Prada execution failed!" << std::endl;
        planning_result = -1.0;
    }
    delete reward;
    delete planner;

//     std::cout << "********************************" << std::endl;
//     std::cout << "* END libPRADA plan" << std::endl;
    std::cout << "\033[1;34m" << "********************************" << "\033[0m" << std::endl;
    return result;
}


TypedAtom PradaPlanner::prada_action_to_atom(relational::Literal *action, const PPDDLObjectManager<PPDDL::FullObject>& object_manager) const
{
    std::stringstream ss;
    ss.str("");
    ss << action->s->name << "(";
    for (size_t j = 0; j < action->args.d0; j ++) {
        ss << action->args(j);
        if ((j+1) < action->args.d0) { // if it isn't the last element
            ss << ",";
        }
    }
    ss << ")";
    NonTypedAtom non_typed_action(ss.str());
    return object_manager.get_typed_atom(non_typed_action);
}
