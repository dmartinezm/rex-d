/*
 * Pasula learner wrapper to be used in REX-D.
 * Copyright (C) 2016  David Martínez (dmartinez@iri.upc.edu)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef INTELLACT_PASULA_LEARNER_H
#define INTELLACT_PASULA_LEARNER_H

#define MT_IMPLEMENT_TEMPLATES

#include <relational/learn.h>

#include <symbolic/rules.h>
#include <symbolic/predicates.h>

#include <rexd/config_reader.h>
#include <rexd/learner.h>

/**
 * \class PasulaLearner
 * \brief Pasula learner wrapper. Uses libprada implementation.
 * 
 * Inherits functionality from learner.h
 */
class PasulaLearner : public Learner {
public:

    PasulaLearner(const ConfigReader& config_reader);

    /**
     * \brief Learn rules with no logging information
     * 
     * WARNING: Can only learn just one rule, not using PPDDL types (use learn_action_rules method)!!
     */
    virtual Typed::RuleSet learn_quiet(const TransitionGroup<TypedAtom>& transitions, const Typed::RuleSet& previous_rules);
    
    /**
     * \brief Learn rules
     * 
     * WARNING: Can only learn just one rule, not using PPDDL types (use learn_action_rules method)!!
     */
    virtual Typed::RuleSet learn(const TransitionGroup<TypedAtom>& transitions, const Typed::RuleSet& previous_rules);


private:
    ConfigReader config_reader;
    
    // file paths
    std::string symbols_filename;
    std::string rules_filename;
    std::string transitions_filename;
    
    // learning parameters
    double alpha_pen;
    double noise_lower_bound;
    double noise_lower_bound_default_rule;

    bool learn_pasula();
};

#endif
