/*
 * Class to create a planner according to the given configuration and parameters.
 * Copyright (C) 2016  David Martínez (dmartinez@iri.upc.edu)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */


#ifndef PLANNER_FACTORY_H
#define PLANNER_FACTORY_H

#include <rexd/planner.h>
#include <rexd/prada_planner.h>
#include <ippc_wrapper/ippc_gpack_planner.h>
#include <ippc_wrapper/ippc_prost_planner.h>
#include <rexd/config_reader.h>

/**
 * \class PlannerFactory
 * \brief Gets planner instances with the corresponding configuration.
 */
class PlannerFactory
{
public:
    /**
     * \brief Gets the (singleton) planner instance with the corresponding configuration parameters.
     */
    static PlannerPtr get_standard_planner(const ConfigReader& config_reader);
};

#endif // PLANNER_FACTORY_H
