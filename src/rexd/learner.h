/*
 * Basic learner class to be inherited by all REX-D learners.
 * Copyright (C) 2016  David Martínez (dmartinez@iri.upc.edu)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef INTELLACT_LEARNER_H
#define INTELLACT_LEARNER_H

#include <vector>

#include <symbolic/rules.h>
#include <symbolic/predicates.h>
#include <symbolic/domain.h>

/*!
\class Learner
\brief Virtual base learner class. It provides the interface that has to be used by the learners.

The base class for learners. Defines the methods to used to learn.
See rexd/pasula_leaner.h for an example implementation of a leaner.
*/
class Learner {
public:

    Learner();
    #if __cplusplus > 199711L // C++11
        virtual ~Learner() = default;
    #else
        virtual ~Learner() {}
    #endif

    /*!
    \brief Updates the rules for just one action given a list of transitions.
    
    Removes the rules representing the given action, learns the new action rules that explain the transitions, and adds them to the new created rule set.

    \param previous_rules original rule set to be updated.
    \param transitions previous experiences in the format s_t, a_t, s_t+1.
    \param action action whose rules have to be updated.
    \return Typed::RuleSet updating the given action rules according to the transitions.
    */
    Typed::RuleSet learn_action_rules(const Typed::RuleSet& previous_rules, const TransitionGroup<TypedAtom>& transitions, const TypedAtom action);
    
    /*!
    \brief Updates the rules for just one action given a list of transitions.
    
    Removes the rules representing the given action, learns the new action rules that explain the transitions, and adds them to the new created rule set.

    \param previous_rules original rule set to be updated.
    \param transitions previous experiences in the format s_t, a_t, s_t+1.
    \param action action whose rules have to be updated.
    \retval removed_rules_ptr actions rules removed (the ones in previous_rules).
    \retval added_rules_ptr actions rules added (the ones explaining the action in the transitions).
    \return Typed::RuleSet updating the given action rules according to the transitions.
    */
    Typed::RuleSet learn_action_rules(const Typed::RuleSet& previous_rules, const TransitionGroup<TypedAtom>& transitions, const TypedAtom& action, Typed::RuleSet& removed_rules_ptr, Typed::RuleSet& added_rules_ptr);
    
    /*!
    \brief Learns the rules in the given transitions. No output through std::cout.

    \param transitions previous experiences in the format s_t, a_t, s_t+1
    \return Typed::RuleSet that explains the transitions
    */
    virtual Typed::RuleSet learn_quiet(const TransitionGroup<TypedAtom>& transitions, const Typed::RuleSet& previous_rules) =0;
    
    /*!
    \brief Learns the rules in the given transitions. Outputs info trough std::cout.

    \param transitions previous experiences in the format s_t, a_t, s_t+1
    \return Typed::RuleSet that explains the transitions
    */
    virtual Typed::RuleSet learn(const TransitionGroup<TypedAtom>& transitions, const Typed::RuleSet& previous_rules) =0;
};

typedef boost::shared_ptr<Learner> LearnerPtr;

#endif
