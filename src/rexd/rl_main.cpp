#include "rl_main.h"
#include "rexd/pasula_learner.h"
#include "lfit/lfit_learner.h"

_INITIALIZE_EASYLOGGINGPP

ReinforcementLearner::ReinforcementLearner():
    transitions(PPDDLObjectManager<PPDDL::FullObject>()),
    transition_obtained(false),
    repeat_iteration(false),
    teacher_loop_enabled(false),
    current_problem(1),
    current_episode(1),
    current_episode_iteration(0),
    total_number_iterations(0)
{
}

bool ReinforcementLearner::init(const std::string& config_path, const std::string& files_path)
{
    bool init_ok = config_reader.init(config_path, files_path);
    utils::init_loggers(files_path + "logger.conf");
    LOG(INFO) << "Starting new execution.\n\n";

    // generated random seed
    utils::initialize_random_seed();
    
    scenario_ptr = ScenarioPtr(new Scenario(config_reader.get_path("ppddl_domain_file"), 
                                            config_reader.get_path("simulator_ppddl_current_problem_file"),
                                            config_reader));
    
    // load action list if needed
    std::vector<TypedAtom> action_list;
    if (!config_reader.get_variable<bool>("teacher")) { 
        // if no teacher, the list of actions have to be provided to learn
        // TODO
        LOG(ERROR) << "TODO: Implement and check reading an action list with typed actions.";
        assert(0);
        // read_rules
        // get action_list from rules
        LOG(INFO) << "Action list is " << action_list;
    }
    
    transitions = read_transitions_from_file(config_reader.get_path("transitions_backup_path"), 
                                             scenario_ptr->get_state().ppddl_object_manager, 
                                             scenario_ptr->get_state().get_constants());
    scenario_ptr->domain.index_transitions(transitions);
    
    // TODO read saved dead end excuses from previous execution
//     dead_end_excuses = PredicateGroup<Atom>("-broken(71) -broken(72) -broken(73) -broken(74) -broken(75) -broken(76) broken(71) broken(72) broken(73) broken(74) broken(75) broken(76) ");
//     rules_ptr->set_dangerous_rules(dead_end_excuses);

    // load planner and learner
    high_level_planner = boost::make_shared<HighLevelPlanner>(HighLevelPlanner(config_reader, &logging_file));
    if (config_reader.get_variable<std::string>("learner") == LEARNER_PASULA) {
        learner = boost::make_shared<PasulaLearner>(PasulaLearner(config_reader));
    }
    else if (config_reader.get_variable<std::string>("learner") == LEARNER_LFIT) {
        learner = boost::make_shared<LFITLearner>(LFITLearner(config_reader));
    }
    else {
        throw std::invalid_argument("Initializing learner: Unknown learner " + config_reader.get_variable<std::string>("learner"));
    }

    // load auto teacher if needed
    if (config_reader.get_variable<bool>("auto_teacher")) {
        auto_teacher_ptr = AutoTeacherPtr(new AutoTeacher(config_reader));
    }

    visualizer.init(config_reader);

    return init_ok;
}

bool ReinforcementLearner::main_iteration()
{
    if (config_reader.get_variable<bool>("monitoring")) {
        return main_monitoring_iteration();
    }
    else {
        return main_learning_iteration();
    }
}


bool ReinforcementLearner::reset_rexd()
{
    bool result;
    // total_number_iterations--; // special iteration, doesn't count as normal iteration -> anyway for visualization is fine to have a small pause
    logging_file << "Reward: " << scenario_ptr->get_reward() << std::endl;

    result = reset_scenario();
    scenario_ptr->reset();
    scenario_ptr->config_reader.set_variable<bool>("use_vmin", config_reader.get_variable<bool>("use_vmin"));
    repeat_iteration = false;
    teacher_loop_enabled = false;

    visualizer.write_episode_log(config_reader.get_variable<std::string>("domain"), current_problem, current_episode, scenario_ptr->get_reward(), total_number_iterations);
    current_episode_iteration = 0;
    high_level_planner->reset();
    return result;
}


bool ReinforcementLearner::main_learning_iteration()
{
    bool result;
    LOG(INFO) << "\n" << "--------------------------------------------------------#----------------------------------------------------"; // 2 newlines
    // new iteration
    if (repeat_iteration) {
        repeat_iteration = false;
    }
    else {
        total_number_iterations++;
        current_episode_iteration++;
        LOG(INFO) << "Iteration: " << current_episode_iteration << " / " << scenario_ptr->config_reader.get_variable<uint>("planning_horizon") 
                  << " (total iterations: " << total_number_iterations << "). "
                  << "Episode: " << current_episode << " / " << config_reader.get_variable<uint>("auto_experiments_num");

        // get state
        show_info_request_state();
        previous_state = scenario_ptr->get_state();
        scenario_ptr->set_state(read_state());

        // get reward
        last_reward = read_reward();
        scenario_ptr->add_reward(last_reward);
    }

    // check infinite loops
    if (scenario_ptr->get_planning_horizon() == 0) {
        transition_obtained = false;
        total_number_iterations--;
        result = reset_rexd();
    }
    else if (scenario_ptr->is_in_goal() && (!config_reader.get_variable<bool>("noop_actions_allowed"))) { // check goal
        if (transition_obtained) { // learn last action when arriving to the goal
            learning();
        }
        LOG(INFO) << START_COLOR_GREEN << "IS GOAL! Reward: " << scenario_ptr->get_reward() << END_COLOR;
        transition_obtained = false;
        show_info_is_goal();
        result = reset_rexd();
    }
    else { // normal behaviour (learn and plan)
        learning();
        result = planning(); // true if OK, false if dead-end
    }
    return result;
}


// WARNING This method is a infinite loop
bool ReinforcementLearner::main_monitoring_iteration()
{
    LOG(INFO) << "\n" << "********************************************************"; // 2 newlines

    // get state
    show_info_request_state();
    previous_state = scenario_ptr->get_state();
    scenario_ptr->set_state(read_state());
    read_reward();

    // check goal
    if (scenario_ptr->is_in_goal()) {
        LOG(INFO) << START_COLOR_GREEN << "IS GOAL!" << END_COLOR;
        transition_obtained = false;
        show_info_is_goal();

        // wait for action recognition
        read_action();

        return true;
    }
    else { // normal behaviour (learn and plan)
        monitoring();

        return true;
    }
}


void ReinforcementLearner::update_transitions()
{
    if (transition_obtained) {
        State current_state = scenario_ptr->get_state();
        current_state.remove_special_predicates();
        previous_state.remove_special_predicates();
        
        TypedAtom typed_previous_action = scenario_ptr->get_state().ppddl_object_manager.get_typed_atom(*previous_action);
        Transition<TypedAtom> transition_full(previous_state, 
                                              typed_previous_action, 
                                              current_state, 
                                              last_reward, 
                                              true);
        transitions.add_transition(transition_full);
        transitions.write_pasula_transitions_to_file(config_reader.get_path("transitions_backup_path"));
    }
}

void ReinforcementLearner::learning()
{
    update_transitions();

    struct timeval timer_start = utils::start_timer();
    if ((config_reader.get_variable<bool>("learning")) && (transition_obtained) && (transitions.size_set() > 0) && (previous_action->name.size()>1) ) {
        CINFO("planning") << START_COLOR_BLUE << "Learning" << END_COLOR;
        show_info_start_learning();
        Typed::RuleSet new_rules, removed_rules, added_rules;
        TypedAtom typed_previous_action = scenario_ptr->get_state().ppddl_object_manager.get_typed_atom(*previous_action);
        if (config_reader.get_variable<std::string>("learner") == LEARNER_PASULA) {
            new_rules = learner->learn_action_rules(scenario_ptr->get_rules(),
                                                    transitions, 
                                                    typed_previous_action, 
                                                    removed_rules, added_rules);
            new_rules.remove_nop_rules();
        }
        else if (config_reader.get_variable<std::string>("learner") == LEARNER_LFIT) {
            new_rules = learner->learn_quiet(transitions, scenario_ptr->get_rules());
        }
        else {
            throw std::invalid_argument("Learning rules: Unknown learner " + config_reader.get_variable<std::string>("learner"));
        }
        new_rules.set_dangerous_rules(dead_end_excuses);
        scenario_ptr->set_rules(new_rules);
        scenario_ptr->write_PDDL_domain(config_reader.get_path("ppddl_domain_file"));

        // TODO re-enable this part (if using LFIT removed and added rules have to be found manually)
//         show_info_end_learning(*removed_rules_ptr, *added_rules_ptr, high_level_planner->transition_context);
//         visualizer.write_learning_log(*removed_rules_ptr, *added_rules_ptr, high_level_planner->transition_context, total_number_iterations);
    }
    else {
        CINFO("planning") << "Skipping learning (no transitions or no previous action)";
    }
    logging_file << "Time learning: " << utils::get_elapsed_time(timer_start) << std::endl;
}


bool ReinforcementLearner::planning()
{
    show_info_start_planning();
    struct timeval timer_start = utils::start_timer();
    transition_obtained = true; // default
    expected_reward = 0; // default
    PlanningFailureOpt excuse_opt;
    std::vector< std::string > actions_requiring_excuse;
    CINFO("planning") << START_COLOR_BLUE << "Planning" << END_COLOR;

    TypedAtomOpt planning_res;
    if (teacher_loop_enabled) {
        CINFO("planning") << "Skipping planning, teacher loop enabled (use stopTeacher to stop it)";
    }
    else {
        // explore and plan
        bool is_dangerous;
        planning_res = high_level_planner->exploit_or_explore(*scenario_ptr, transitions, excuse_opt, actions_requiring_excuse, is_dangerous);
        if (is_dangerous) {
            planning_res = ask_confirmation_dangerous_action(*scenario_ptr, planning_res); // transition_context set inside if teacher changes action
        }
        logging_file << "Time planning: " << utils::get_elapsed_time(timer_start) << std::endl;
        show_info_end_planning(planning_res, failure_analyzer_utils::planning_failure_to_string(excuse_opt));
    }

    // If solution found execute it. Else teacher / explore
    if (planning_res) {
        execute_action(*planning_res);
    }
    else {
        // All trials to obtain a plan have failed, so it will request teacher demonstration 
        // Subgoals approach the goal as much as possible before requesting a teacher demonstration
        // After first teacher demonstration is done, subgoals no longer make sense, so the are disabled
        // This avoid loops with subgoals and auto_teacher
        // The same applies to random exploration
        high_level_planner->disable_subgoals();
        
        if (config_reader.get_variable<bool>("teacher")) {
            bool continue_ok = teacher(excuse_opt, actions_requiring_excuse);
            if (!continue_ok) {
                return false;
            }
        }
        else {
            planning_res = high_level_planner->explore(*scenario_ptr, transitions, true);
            execute_action(*planning_res);
        }
    }
    if ( planning_res && planning_res->is_special() ) {
        transition_obtained = false;
    }

    if (!repeat_iteration) {
        scenario_ptr->decrease_planning_horizon();
    }
    visualizer.write_status_log(scenario_ptr->get_reward(), expected_reward, total_number_iterations);
    return true;
}

bool ReinforcementLearner::teacher(const PlanningFailureOpt& excuse_opt, std::vector< std::string > actions_requiring_excuse)
{
    LOG(INFO) << START_COLOR_RED << "Teacher needed." << END_COLOR;
    LOG_IF(excuse_opt, INFO) << "Excuse is \"" << *excuse_opt << "\" requiring actions " << actions_requiring_excuse;
    LOG_IF(!excuse_opt, INFO) << "No excuse found.";
    show_info_teacher_request(failure_analyzer_utils::planning_failure_to_string(excuse_opt), actions_requiring_excuse);
    high_level_planner->transition_context = "Teacher";

    // obtain action from auto teacher or human teacher
    TypedAtomOpt action_opt;
    if (config_reader.get_variable<bool>("auto_teacher")) {
        float planning_result;
        action_opt = auto_teacher_ptr->get_action(*scenario_ptr, actions_requiring_excuse, planning_result);
        logging_file << "Expected value teacher: " << planning_result << std::endl;
        if ((!action_opt) || (*action_opt == MAKE_TYPED_ATOM_HELP)) {
            action_opt = read_action();
        }
        if (scenario_ptr->config_reader.get_variable<bool>("noop_actions_allowed")) {
            // If teacher value is too bad, disable teacher and plan
            // Only do if noop actions are allowed (otherwise planner may not have any idea of what to do, so a teacher dead-end is better)
            
            if (action_opt->name == ACTION_NAME_NOOP) {
                float noop_reward = scenario_ptr->get_goals().obtain_reward(scenario_ptr->get_state(), MAKE_TYPED_ATOM_NOOP_ACTION);
                if (noop_reward == 0) {
                    CINFO("teacher") << "Disabling teacher as no action is needed anyway.";
                    action_opt = MAKE_TYPED_ATOM_DISABLE;
                }
            }
            
            if ( ((planning_result + scenario_ptr->get_reward()) <= scenario_ptr->config_reader.get_variable<float>("vmin_value")) &&
                 (current_episode != 1) ) { // Dont disable teacher during the first episode
                // disable VMIN if teacher cannot find a solution either
                CINFO("teacher") << "Disabling teacher as no good solution can be found anyway.";
                action_opt = MAKE_TYPED_ATOM_DISABLE;
            }
        }
    }
    else {
        action_opt = read_action();
    }

    // Special cases (retry, dead...)
    if ((!action_opt) || (*action_opt == MAKE_TYPED_ATOM_UNKNOWN)) {
        transition_obtained = false;
    }
    else if (*action_opt == MAKE_TYPED_ATOM_RETRY) {
        transition_obtained = false;
        repeat_iteration = true;
    }
    else if (*action_opt == MAKE_TYPED_ATOM_DISABLE) {
        transition_obtained = false;
        repeat_iteration = true;
        CINFO("teacher") << "Disabling teacher.";
        scenario_ptr->config_reader.set_variable<bool>("use_vmin", false);
    }
    else if (*action_opt == MAKE_TYPED_ATOM_STOP_TEACHER) {
        transition_obtained = false;
        repeat_iteration = true;
        teacher_loop_enabled = false;
        CINFO("teacher") << "Stopping teacher loop";
    }
    else if (*action_opt == MAKE_TYPED_ATOM_START_TEACHER) {
        transition_obtained = false;
        repeat_iteration = true;
        teacher_loop_enabled = true;
        CINFO("teacher") << "Starting teacher loop";
    }
    // if autoteacher request finish action (and has not been requested by rexd planner)
    //   then finish action reward < Vmin -> dead_end!
    else if ((*action_opt == MAKE_TYPED_ATOM_DEAD) || (*action_opt == MAKE_TYPED_ATOM_FINISH_ACTION)) {
        transition_obtained = false;
        LOG(INFO) << START_COLOR_RED << "Dead end! Aborting iteration..." << END_COLOR;
        if (excuse_opt) {
            PredicateGroup<TypedAtom> generic_excuse(*excuse_opt);
            generic_excuse.make_symbolic();
            LOG(INFO) << "Adding " << generic_excuse << " as dangerous problem.";
            logging_file << "Dangerous: " << generic_excuse << std::endl;
            dead_end_excuses.add_predicates(generic_excuse);
            scenario_ptr->get_modificable_rules().set_dangerous_rules(dead_end_excuses);
        }
        logging_file << "Dead end." << std::endl;
        if (config_reader.get_variable<bool>("final_reward_action")) {
            if (config_reader.get_variable<bool>("simulator")) {
                execute_action(MAKE_TYPED_ATOM_FINISH_ACTION, false); // execute as if it isn't teacher
            }
            else {
                execute_action(MAKE_TYPED_ATOM_DEAD, false); // execute as if it isn't teacher
            }
        }
        else {
            // returning false just in case of dead end
            return reset_rexd();
        }
    }
    else { // Execution
        // Check that it is a valid action
        if (config_reader.get_variable<bool>("simulator")) {
            if (!auto_teacher_ptr) {
                auto_teacher_ptr = AutoTeacherPtr(new AutoTeacher(config_reader));
            }
            if (!auto_teacher_ptr->check_valid_action(*action_opt)) {
                LOG(WARNING) << "Selected action " << *action_opt << " is not valid";
                transition_obtained = false;
                repeat_iteration = true;
            }
        }
        
        if (!repeat_iteration) {
            scenario_ptr->domain.index_action(*action_opt);
            previous_action = *action_opt;

            if (config_reader.get_variable<bool>("simulator") || config_reader.get_variable<bool>("robot_executes_teacher_actions")) {
                execute_action(*action_opt, true);
            }
            logging_file << "Teacher: " << *action_opt << std::endl;
        }
    }
    return true;
}

void ReinforcementLearner::monitoring()
{
    LOG(INFO) << "Planning";
    show_info_start_planning();

    TransitionGroup<TypedAtom> transitions(scenario_ptr->get_state().ppddl_object_manager);
    PlanningFailureOpt excuse_opt;
    std::vector<std::string> actions_requiring_excuse;
    TypedAtomOpt planning_res = high_level_planner->exploit_or_explore(*scenario_ptr, transitions, excuse_opt, actions_requiring_excuse);
    std::string excuse_str = failure_analyzer_utils::planning_failure_to_string(excuse_opt);

    if (!planning_res) {
        show_info_end_planning(TypedAtomOpt(), excuse_str);
        LOG(INFO) << "No action";
    }
    else {
        int actions_until_finish = high_level_planner->get_planner_ptr()->estimate_distance_to_finish(*scenario_ptr);
        LOG(INFO) << "Recomended action " << *planning_res << " (actions to finish : " << actions_until_finish << ")";
        show_info_end_planning(planning_res, excuse_str, actions_until_finish);
    }

    // Wait for manipulation recognition
    read_action();
}

