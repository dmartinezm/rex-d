/*
 * REX_D scenario class to work with a RDDL representation
 * Copyright (C) 2016  David Martínez (dmartinez@iri.upc.edu)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef RDDL_SCENARIO_H
#define RDDL_SCENARIO_H

#include <boost/lambda/lambda.hpp>
#include <rexd/rexd_scenario.h>
#include <rexd/explorer.h>

namespace RDDL {
    typedef std::pair<Outcome<TypedAtom>, TypedAtom> PairPreconditionsAction;
    typedef std::vector<PairPreconditionsAction> ListPreconditionsAction;
    typedef std::pair<ListPreconditionsAction, ListPreconditionsAction> PositiveAndNegativePreconditions;
    typedef std::map<TypedAtom, PositiveAndNegativePreconditions > MapHeadBody;
}

/**
 * \class RDDLScenario
 * \brief Extended Scenario to generate RDDL domains to use with IppcPlanner.
 * 
 * Writes RDDL domains with all extra rules and predicates needed for R-MAX and V-MIN algorithms.
 */
class RDDLScenario : public RexdScenario
{
public:
    
    /**
     * \brief Creates RexdScenario. R-MAX and V-MIN are disabled by default.
     */
    RDDLScenario(const Scenario& scenario);
    
    /**
     * \brief Creates RexdScenario. R-MAX and V-MIN settings are copied. New object uses pointer to same transitions data.
     */
    RDDLScenario(const RexdScenario& rexd_scenario);
    
    /** \brief Writes a RDDL domain of the scenario to a file */
    void write_RDDL_domain_to_file(const std::string& template_file, const std::string& domain_file_path) const;
    
    /** \brief Writes a RDDL instance of the scenario to a file */
    void write_RDDL_instance_to_file(const std::string& instance_file_path) const;
    
    
    
private:
    void prepare_RMAX_rules(Typed::RuleSet& complete_rules, RewardFunctionGroup& goals_ppddl, std::set<TypedAtom>& empty_goal_actions) const;

    void write_RDDL_domain(const std::string template_file, std::ostream &out) const;
    void write_RDDL_instance(std::ostream &out) const;
    
    void write_domain_actions(std::ostream& out, const RDDL::MapHeadBody &rules_per_action, const std::set<TypedAtom>& other_heads, const PredicateGroup<TypedAtom>& literal_default_values, const std::set<TypedAtom>& empty_goal_actions) const;
    void write_domain_reward(std::ostream& out, const RewardFunctionGroup& goals_ppddl) const;
    void write_domain_constraints(std::ostream& out) const;
    
    RDDL::MapHeadBody get_rddl_transitions_from_rules(const Typed::RuleSet& rules, PredicateGroup<TypedAtom>& literal_default_values) const;
    
    void write_domain_transition(std::ostream& out, 
                                 const RDDL::PairPreconditionsAction& pair_prec_action, 
                                 const std::vector<PPDDL::FullObject>& head_params,
                                 const bool positive_outcome) const;
    
    void obtain_rddl_symbolic_transition(Predicate<TypedAtom>& out_predicate, PredicateGroup<TypedAtom>& preconditions, TypedAtom& action) const;
};

#endif // RDDL_SCENARIO_H
