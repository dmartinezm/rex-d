/*
 * Basic Planner class to be inherited to implement actual planners.
 * Copyright (C) 2016  David Martínez (dmartinez@iri.upc.edu)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef INTELLACT_PLANNER_H
#define INTELLACT_PLANNER_H

#include <symbolic/rules.h>
#include <symbolic/predicates.h>

#include "rexd/rexd_scenario.h"

/*!
\class Planner
\brief Virtual base planner class. It provides the interface that has to be used by the planners.

The base class for planners. Defines the methods to used to plan.
See ippc_wrapper/ippc_planner.h for an example implementation of a planner.
*/
class Planner {
public:
    Planner();
    #if __cplusplus > 199711L // C++11
        virtual ~Planner() = default;
    #else
        virtual ~Planner() {}
    #endif

    /*!
    \brief Estimates number of actions required to reach the goal

    \param rexd_scenario scenario used to plan
    \return Number of actions to reach the goal, 0 if no solution found
    */
    virtual int estimate_distance_to_finish(const RexdScenario &rexd_scenario) =0;

    /*!
    \brief plans an action in the scenario
    
    The planner obtains the recommended action give the scenario
    
    \param rexd_scenario scenario used to plan
    \param thoroughly if true, spends more time planning to get better results
    \param verbose
    \return planned action, false if no plan was found
    */
    TypedAtomOpt plan(RexdScenario rexd_scenario, 
                      const bool thoroughly = true, const int verbose = 0);
    
    /*!
    \brief plans an action in the scenario
    
    The planner obtains the recommended action give the scenario
    
    \param rexd_scenario scenario used to plan
    \param thoroughly if true, spends more time planning to get better results
    \param verbose
    \retval planning_result expected reward of the plan
    \return planned action, false if no plan was found
    */
    virtual TypedAtomOpt plan(const RexdScenario& rexd_scenario,
                              const bool thoroughly, const int verbose,
                              float& planning_result) =0;

    /*!
    \brief plans a sequence of actions

    Plans a sequence of actions. 
    For each planned action, its most probable effect is used to plan the following actions.
    Plans at most n_actions, until the goal is reached or no solution can be found.

    \param rexd_scenario scenario used to plan
    \retval plan Vector of actions defining the planned action sequence
    \param thoroughly if true, spends more time planning to get better results
    \param verbose verbose
    \param n_actions number of actions to plan
    \param force_effect after action is planned, force first effect that changes state to be considered
    \return true if the goal was reached
    */
    virtual bool plan_n_actions(const RexdScenario& rexd_scenario,
                                std::vector<TypedAtom>& plan,
                                const bool thoroughly = true, const int verbose = 0,
                                const uint n_actions = std::numeric_limits<uint>::max(),
                                const bool force_effect = false) =0;

    /*!
    \brief Gets the probability that a plan that reaches the goal exists if the action is applied
    
    Obtains the probability that a plan that reaches the goal exists after the action is applied.
    
    \param rexd_scenario scenario used to plan
    \param action action that is applied
    \param check_only_dangerous_effects true if only dangerous effects should be checked to speed up
    \return probability of reaching a goal
    */
    float get_expected_action_success_probability(const RexdScenario& rexd_scenario, const TypedAtom& action, const bool check_only_dangerous_effects);

protected:
    /*!
    \brief Makes a stream silent
    
    Changes the underlying buffer to be /dev/null
    
    \param ostream ostream to make silent
    \return original stream buffer associated to the stream
    */
    std::streambuf* stream_quiet(std::ostream& ostream) const;
    
    /*!
    \brief Restores a silent stream
    
    Changes the underlying buffer to be the original one
    
    \param ostream ostream to restore
    \param ostream_sbuf original buffer of the stream
    */
    void stream_restore(std::ostream& ostream, std::streambuf* ostream_sbuf) const;

private:
    boost::shared_ptr<std::ofstream> dev_null_ofs;

};

/*! Shared pointer to planner */
typedef boost::shared_ptr<Planner> PlannerPtr;
/*! Shared pointer to const planner */
typedef boost::shared_ptr< const Planner > PlannerConstPtr;

#endif
