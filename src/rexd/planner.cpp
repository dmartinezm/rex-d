#include "planner.h"


Planner::Planner():
    dev_null_ofs(new std::ofstream("/dev/null"))
{ }

TypedAtomOpt Planner::plan(RexdScenario rexd_scenario, const bool thoroughly, const int verbose)
{
    float planning_result;
    return plan(rexd_scenario, thoroughly, verbose, planning_result);
}

std::streambuf* Planner::stream_quiet(std::ostream& ostream) const
{
    std::streambuf* ostream_sbuf = ostream.rdbuf(); // save original sbuf
    ostream.rdbuf(dev_null_ofs->rdbuf());  // redirect 'cout'/'cerr'/... to a '/dev/null'
    return ostream_sbuf;
}

void Planner::stream_restore(std::ostream& ostream, std::streambuf* ostream_sbuf) const
{
    ostream.rdbuf(ostream_sbuf); // restore the original stream buffer
}

float Planner::get_expected_action_success_probability(const RexdScenario& rexd_scenario, const TypedAtom& action, const bool check_only_dangerous_effects)
{
    float dead_end_probability = 0.0;

    Typed::RuleSet action_rules = rexd_scenario.get_rules().get_grounded_action_rules(action, rexd_scenario.get_state());
    Typed::RuleSet covering_rules = action_rules.get_covering_rules(rexd_scenario.get_state(), rexd_scenario.get_state().ppddl_object_manager);
    covering_rules.remove_nop_rules();

    if (covering_rules.empty()) {
        dead_end_probability = 0.0; // there will be no changes -> so no new dead-ends
    }
    else if (covering_rules.size() != 1) {
        CWARNING("planning") << START_COLOR_RED << "Simulation of planned action failed. Number of covering rules != 1" << END_COLOR;
        CWARNING("planning") << "Covering rules are " << covering_rules;
        dead_end_probability = 1.0;
    }
    else {
        CINFO("planning") << "Checking dead end... " << NOENDL;
        OutcomeGroup<TypedAtom>::const_iterator outcome_it = covering_rules.front()->outcomes.begin();
        for (size_t i = 0; i < covering_rules.front()->outcomes.size(); ++i ) {
            if (!check_only_dangerous_effects || (outcome_it->is_dangerous)) {
                bool success;
                float reward;
                State new_state(rexd_scenario.get_state());
                rexd_scenario.apply_rule((*static_cast<const Typed::GroundedRule*>(&(*covering_rules.front()))), new_state, success, reward, true, i);
                RexdScenario new_scenario(rexd_scenario);
                new_scenario.set_state(new_state);
                if (!new_scenario.is_in_goal()) {
                    TypedAtomOpt planned_action_opt = plan(new_scenario, true);
                    if ( !planned_action_opt || (planned_action_opt->is_special()) ) {
                        dead_end_probability += outcome_it->probability;
                    }
                }
            }
            ++outcome_it;
        }

    }
    CINFO("planning") << "Dead end probability: " << dead_end_probability << " with action: " << action;

    return (1.0 - dead_end_probability);
}
