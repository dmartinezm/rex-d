/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2014  <copyright holder> <email>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#include "high_level_planner.h"


HighLevelPlanner::HighLevelPlanner(const ConfigReader& config_reader, ofstream* logging_file_ptr_):
    subgoals_disabled(false)
{
    logging_file_ptr = logging_file_ptr_;
    planner = PlannerFactory::get_standard_planner(config_reader);
}


TypedAtomOpt HighLevelPlanner::exploit_or_explore(Scenario& scenario, 
                                                      const TransitionGroup<TypedAtom>& transitions, 
                                                      PlanningFailureOpt& planning_failure_opt, 
                                                      std::vector< std::string >& actions_requiring_planning_failure
                                                     )
{
    bool is_dangerous;
    return exploit_or_explore(scenario, transitions, planning_failure_opt, actions_requiring_planning_failure, is_dangerous);
}

TypedAtomOpt HighLevelPlanner::exploit_or_explore(Scenario& scenario, 
                                                      const TransitionGroup<TypedAtom>& transitions, 
                                                      PlanningFailureOpt& planning_failure_opt, 
                                                      std::vector< std::string >& actions_requiring_planning_failure,
                                                      bool& is_dangerous
                                                     )
{
    TypedAtomOpt planning_res;
    expected_reward = 0.0;
    dead_end_probability = 0.0;
    transition_context = "None";
    is_dangerous = false;
    already_confirmed_as_safe = false; // TODO remove
    
    if (!scenario.config_reader.get_variable<bool>("use_rmax")) {
        planning_res = explore(scenario, transitions, false);
    }
    if (!planning_res) {
        planning_res = standard_plan(scenario, transitions);
        check_teacher_demonstration_requested(planning_res);
    }
    if (!scenario.config_reader.get_variable<bool>("rule_analysis")) { // from now on everything is excuse dependent
        return planning_res;
    }
    
    FailureAnalyzer failure_analyzer = FailureAnalyzer(scenario);
    if (!planning_res) {
        planning_res = planning_with_alternative_rules(scenario, transitions, failure_analyzer);
        check_teacher_demonstration_requested(planning_res);
    }
    if (!planning_res) {
        planning_res = planning_with_subgoals(scenario, failure_analyzer);
        check_teacher_demonstration_requested(planning_res);
    }
    
    if (planning_res) {
        is_dangerous = confirm_dangerous_actions(scenario, planning_res);
        if (is_dangerous) {
            // plan found, not dangerous any more
            // plan not found, still dangerous
            is_dangerous = !replan_without_dangerous_actions(scenario, planning_res, transitions);
        }
    }
    else {
        planning_failure_opt = failure_analyzer.get_planning_failure();
        actions_requiring_planning_failure = get_actions_requiring_planning_failure(scenario, failure_analyzer);
    }

    assert(transition_context != "None");
    return planning_res;
}


TypedAtomOpt HighLevelPlanner::explore(const Scenario& scenario, const TransitionGroup<TypedAtom>& transitions, bool forced)
{
    int DEBUG = 0;
    TypedAtomOpt planning_res;

    if (scenario.config_reader.get_variable<bool>("exploration")) {
        Explorer explorer;
        // TODO separate dangerous_actions and confirm_dangerous_actions
        if ( forced || (!explorer.is_state_known(scenario, transitions)) ) {
            planning_res = explorer.select_action_for_exploration(scenario, transitions, forced, scenario.config_reader.get_variable<uint>("exploration_known_threshold"));
            if (planning_res) {
                transition_context = "Exploring";
                (*logging_file_ptr) << "Explore: " << *planning_res << std::endl;
                if (DEBUG > 0)
                    LOG(INFO) << "Selected exploring action " << boost::lexical_cast<std::string>(*planning_res);
            }
        }
        else {
            if (DEBUG > 0)
                LOG(INFO) << "State is already known.";
        }
    }

    return planning_res;
}


TypedAtomOpt HighLevelPlanner::standard_plan(const Scenario& scenario, const TransitionGroup<TypedAtom>& transitions)
{
    TypedAtomOpt planning_res;
    transition_context = "Exploiting";
    RexdScenario rexd_scenario(scenario);
    rexd_scenario.set_rmax(boost::make_shared<TransitionGroup<TypedAtom> >(transitions));
    rexd_scenario.set_vmin();
    planning_res = planner->plan(rexd_scenario, true, 1, expected_reward);
    *(this->logging_file_ptr) << "Expected value planning: " << expected_reward << std::endl;
    
    // avoid exploration of dangerous actions with R-MAX
    if (scenario.config_reader.get_variable<bool>("use_rmax") 
        && (expected_reward > scenario.config_reader.get_variable<float>("rmax_reward"))
        && (*planning_res != MAKE_TYPED_ATOM_TEACHER) ) {
            transition_context = "Exploring";
            avoid_exploration_of_dangerous_rules(scenario, planning_res);
    }
    
    return planning_res;
}


void HighLevelPlanner::avoid_exploration_of_dangerous_rules(const Scenario& scenario, TypedAtomOpt& planning_res)
{
    assert(planning_res);
    // if dangerous -> main planning step without exploration
    if (scenario.config_reader.get_variable<bool>("confirm_dangerous_actions") && 
        scenario.get_rules().is_dangerous(*planning_res, scenario.get_state()) ) {
        float prob_success = planner->get_expected_action_success_probability(scenario, *planning_res, true); // TODO this is called twice if prob = 0.0
        if (prob_success < 1.0) {
            *logging_file_ptr << "Ignoring exploring " << *planning_res << " and retry without rmax." << std::endl;
            CINFO("planning") << "Not exploring " << *planning_res << " as it is dangerous. Replanning without RMAX.";
            RexdScenario rexd_scenario(scenario);
            rexd_scenario.set_vmin();
            planning_res = planner->plan(rexd_scenario, true, 1, expected_reward);
            transition_context = "Exploiting";
        }
        else {
            already_confirmed_as_safe = true;
        }
    }
}


bool HighLevelPlanner::confirm_dangerous_actions(const Scenario& scenario, TypedAtomOpt& planning_res)
{
    assert(planning_res);
    bool is_dangerous = false;
    // Check for dangerous actions
    if (scenario.config_reader.get_variable<bool>("confirm_dangerous_actions")) {
        if ((!already_confirmed_as_safe) && (scenario.get_rules().is_dangerous(*planning_res, scenario.get_state()))) {
            float prob_success = planner->get_expected_action_success_probability(scenario, *planning_res, true);
            if (prob_success < 1.0) {
                LOG(INFO) << START_COLOR_RED << "Dangerous action" << END_COLOR << " which may lead to a dead end with probability " << (1.0 - prob_success) << " detected: " << START_COLOR_RED << *planning_res << END_COLOR;
                *logging_file_ptr << "Confirmation request: " << *planning_res << std::endl;
                transition_context = "ExploitingDangerous";
                is_dangerous = true;
                dead_end_probability = 1.0 - prob_success;
            }
        }
    }
    return is_dangerous;
}


bool HighLevelPlanner::replan_without_dangerous_actions(const Scenario& scenario, TypedAtomOpt& planning_res, const TransitionGroup<TypedAtom>& transitions)
{
    Scenario new_scenario(scenario);
    Typed::RuleSet new_ruleset;
    BOOST_FOREACH(const Typed::RulePtr& rule_ptr, scenario.get_rules()) {
        if ( !((planning_res->name == rule_ptr->name) && rule_ptr->dangerous) ) {
            // if not dangerous rule with name equal to planned one
            new_ruleset.push_back(rule_ptr->make_shared());
        }
    }
    new_scenario.set_rules(new_ruleset);
    
    TypedAtomOpt new_planning_res = standard_plan(new_scenario, transitions);
    check_teacher_demonstration_requested(new_planning_res);
    if (new_planning_res) {
        LOG(INFO) << START_COLOR_CYAN << "Replanning without dangerous rules was successful" << END_COLOR;
        *logging_file_ptr << "Replan without dangerous rule: " << *new_planning_res << std::endl;
        planning_res = new_planning_res;
        return true; // found new plan, not dangerous any more
        // TODO Make a recursive loop until no dangerous action is planned
        // There may may more than one dangerous action, here we have just removed the first one
    }
    else {
        return false; // plan not found, still dangerous
    }
}


void HighLevelPlanner::check_teacher_demonstration_requested(TypedAtomOpt& planning_res)
{
    if ((planning_res) && (*planning_res == MAKE_TYPED_ATOM_TEACHER)) {
        planning_res = TypedAtomOpt();
        transition_context = "Teacher";
    }
}


TypedAtomOpt HighLevelPlanner::planning_with_alternative_rules(Scenario& scenario, const TransitionGroup<TypedAtom>& transitions, FailureAnalyzer& failure_analyzer)
{
    int DEBUG = 0;
    if (!scenario.config_reader.get_variable<bool>("rule_analysis_equivalent_rules")) {
        return TypedAtomOpt();
    }
    
    PlanningFailureOpt planning_failure_opt = failure_analyzer.get_planning_failure();
    TypedAtomOpt planning_res;
    Typed::RuleSet best_ruleset;

    FailureAnalyzerUtils failure_analyzer_utils(failure_analyzer);
    std::vector<Typed::RuleSet> extra_rulesets = failure_analyzer_utils.get_rules_that_overcome_planning_failure(scenario.get_rules(), planning_failure_opt, transitions);

    // Plan with newer rulesets
    if (!extra_rulesets.empty()) {
        CINFO("excuses") << "Testing " << extra_rulesets.size() << " rules: " << NOENDL;
    }
    double min_score = -10000;
    for (std::vector<Typed::RuleSet>::iterator it_ruleset= extra_rulesets.begin(); it_ruleset!=extra_rulesets.end(); ++it_ruleset) {
        float score;
        RexdScenario alt_scenario(scenario);
        // TODO may be a good idea to enable it, although careful testing required
        // alt_scenario.set_rmax(boost::make_shared<TransitionGroup>(transitions));
        alt_scenario.set_vmin();
        alt_scenario.set_rules(*it_ruleset);
        TypedAtomOpt new_res = planner->plan(alt_scenario, false, 0, score);
        if (new_res && (!new_res->is_special()) && (score > min_score)) {
            min_score = score;
            planning_res = new_res;
            best_ruleset = *it_ruleset;
            CINFO("excuses") << "*" << NOENDL;
            if (DEBUG > 0) {
                CINFO("excuses");
                CINFO("excuses") << "Better ruleset found! Result is " << *new_res;
            }
        }
        else {
            CINFO("excuses") << "." << NOENDL;
        }
    }
    if (!extra_rulesets.empty())
        CINFO("excuses");
    
    // don't select dangerous actions with rule alternatives
    if (planning_res &&
        scenario.config_reader.get_variable<bool>("confirm_dangerous_actions")
        && scenario.get_rules().is_dangerous(*planning_res, scenario.get_state()) ) {
        float prob_success = planner->get_expected_action_success_probability(scenario, *planning_res, true);
        if (prob_success < 1.0) {
            (*logging_file_ptr) << "Ignoring dangerous alternative due to possible dead end: " << *planning_res << std::endl;
            CINFO("excuses") << "Ignoring dangerous alternative due to possible dead end: " << *planning_res;
            planning_res = TypedAtomOpt();
        } 
    }

    // if found good rules -> save them
    if (planning_res) {
        CINFO("excuses") << "Typed::Rule alternative found! New plan is " << *planning_res << "(" << min_score << ")";
        this->expected_reward = min_score;
        scenario.set_rules(best_ruleset); // needed as only executed action is learned, rule alternatives may have changed 2+ action rule on the plan
        best_ruleset.write_rules_to_file(scenario.config_reader.get_path("nid_rules_path"));
        transition_context = "ExploitingAlternatives"; 
        (*logging_file_ptr) << "Using rule alternatives." << std::endl;
    }

    return planning_res;
}

TypedAtomOpt HighLevelPlanner::planning_with_subgoals(const Scenario& scenario, FailureAnalyzer& failure_analyzer)
{
    TypedAtomOpt planning_res;
    
    if (scenario.config_reader.get_variable<bool>("rule_analysis_subgoals")) {
        if (scenario.config_reader.get_variable<bool>("use_vmin")) {
            if (!subgoals_disabled) {
                planning_res = planning_with_subgoals_vmin(scenario);
                subgoals_disabled = true;
            }
            else {
                CINFO("planning") << "Skipping subgoals to avoid infinite loops.";
                subgoals_disabled = false;
            }
        }
        else if (scenario.config_reader.get_variable<bool>("rule_analysis")) {
            planning_res = plan_with_subgoals_planning_failures(scenario, failure_analyzer);
        }
        
        if (planning_res) {
            transition_context = "ExploitingSubgoals";
            (*logging_file_ptr) << "Using subgoals." << std::endl;
        }
    }
    
    return planning_res;
}

TypedAtomOpt HighLevelPlanner::planning_with_subgoals_vmin(const Scenario& scenario)
{
    TypedAtomOpt planning_res;
    
    CINFO("planning") << "Decreasing Vmin by " << scenario.config_reader.get_variable<float>("auto_experiments_increasing_vmin_quantity");
    RexdScenario new_scenario(scenario);
    new_scenario.set_vmin();
    new_scenario.modify_vmin_value(-1 * scenario.config_reader.get_variable<float>("auto_experiments_increasing_vmin_quantity"));
    planning_res = planner->plan(new_scenario, true, 1, expected_reward);
    if (planning_res && (*planning_res == MAKE_TYPED_ATOM_FINISH_ACTION)) {
        // Subgoals are for approaching the goal, so
        // don't finish with subgoals!
        planning_res = TypedAtomOpt();
    }
    return planning_res;
}

TypedAtomOpt HighLevelPlanner::plan_with_subgoals_planning_failures(const Scenario& scenario, FailureAnalyzer& failure_analyzer)
{
    TypedAtomOpt planning_res;
    
    // TODO FIX
    // this type of subgoals require rule_analysis
//     PredicateGroup<Atom> planning_failure = failure_analyzer.get_planning_failure();
//     CINFO("planning") << "Planning using subgoals with planning_failure " << planning_failure;
//     if (scenario.get_goals().size() > 1) {
//         CWARNING("planning") << "Goals > 1 not working with subgoals!!!";
//     }
//     Goal new_goal = failure_analyzer.get_subgoal(scenario.get_goals().get_predicates(), planning_failure);
//     GoalGroup new_goals;
//     new_goals.push_back(new_goal);
//     CINFO("planning") << "New goal is " << new_goal;
//     Scenario new_scenario(scenario);
//     new_scenario.set_goals(new_goals);
//     planning_res = planner->plan(new_scenario, expected_reward, false);
    return planning_res;
}


std::vector<std::string> HighLevelPlanner::get_actions_requiring_planning_failure(const Scenario& scenario, FailureAnalyzer& failure_analyzer)
{
    std::vector<std::string> actions_requiring_planning_failure;
    
    PlanningFailureOpt planning_failure_opt = failure_analyzer.get_planning_failure();
    if (planning_failure_opt) {
        Typed::RuleSet rules_requiring_planning_failure = scenario.get_rules().find_rules_with_a_precondition(*planning_failure_opt);
        rules_requiring_planning_failure.remove_nop_rules();
        actions_requiring_planning_failure = rules_requiring_planning_failure.get_action_names();
    }
    
    return actions_requiring_planning_failure;
}


void HighLevelPlanner::reset()
{
    subgoals_disabled = false;
}

