/*
 * Class to create gource log files to generate videos.
 * Copyright (C) 2016  David Martínez (dmartinez@iri.upc.edu)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */


#ifndef VISUALIZER_H
#define VISUALIZER_H


#include <symbolic/utils.h>
#include <symbolic/rules.h>
#include <rexd/config_reader.h>


/**
 * \class Visualizer
 * \brief Class to create gource log files to generate videos.
 * 
 * It creates a gource log file that can be used to visualize the learning process in gource.\n
 * It also creates a .avs file to process the created gource video file.\n
 * The subtitles can be attached to the gource video to provide more information about the learning process.\n
 * The video scripts in the bin folder can use this files to generate the videos.
 * 
 * The following configuration options are used to enable visualization logs.
 * - visualization -> Enable gource log creation
 * - visualization_gource_log_path -> Gource log path
 * - visualization_gource_captions_log_path -> Gource caption log path
 * - visualization_avisynth_log_path -> Avisynth script path
 * - visualization_subtitle_path -> Path for visualization subtitle (with basic information)
 * - visualization_subtitle_verbose_path -> Path for visualization verbose subtitle (with extended information)
 * - visualization_enable_expected_reward -> Enable visualization of expected reward (subtitle)
 * - visualization_enable_actual_reward -> Enable visualization of actual reward (subtitle)
 * - visualization_enable_current_reward -> Enable visualization of current reward (subtitle)
 */
class Visualizer
{
public:
    Visualizer(): enabled(false) {};
    Visualizer(const ConfigReader& config_reader);
    ~Visualizer();

    /**
     * \brief Inits the different visualization logs enabled.
     */
    void init(const ConfigReader& config_reader);

    /**
     * \brief Logs episode info that will be used to generate videos.
     */
    void write_episode_log(const std::string& scenario, const ulong problem_number, const ulong episode_number, const float reward, const ulong total_number_iterations);
    
    /**
     * \brief Logs episode info that will be used to generate videos.
     */
    void write_status_log(const float accumulated_reward, const float expected_reward, const ulong total_number_iterations);
    void write_learning_log(const Typed::RuleSet& removed_rules, const Typed::RuleSet& added_rules, const std::string& context, const ulong total_number_iterations);

private:
    bool enabled;
    bool enabled_expected_reward;
    bool enabled_actual_reward;
    bool enabled_current_reward;

    // saved old values
    ulong previous_problem_number;
    ulong previous_episode_number;
    ulong previous_episode_start_time;
    ulong previous_episode_reward;

    std::ofstream gource_log_file;
    std::ofstream gource_caption_log_file;
    std::ofstream avisynth_script_file;
    std::ofstream subtitle_log_file;
    std::ofstream subtitle_verbose_log_file;

    // incremental values
    std::string avisynth_files_to_concat;

    void init_subtitle(std::ostream& subtitle_stream);
    void init_avisynth_script(std::ostream& avisynth_stream);
    void end_avisynth_script(std::ostream& avisynth_stream);

    void write_learning_log_rule(const Typed::Rule& rule_ptr, const std::string& context, const std::string& type, const ulong total_number_iterations);
};

#endif // VISUALIZER_H
