#include "pasula_learner.h"



PasulaLearner::PasulaLearner(const ConfigReader& config_reader) :
    config_reader(config_reader)
{
    cout.precision(3);
    rules_filename = config_reader.get_path("nid_rules_path");
    symbols_filename = config_reader.files_path + "/gen_symbols.dat";
    transitions_filename = config_reader.get_path("transitions_tmp_path");
    alpha_pen = config_reader.get_variable<double>("pasula_alpha_pen");
    noise_lower_bound = config_reader.get_variable<double>("pasula_noise_lower_bound");
    noise_lower_bound_default_rule = config_reader.get_variable<double>("pasula_noise_lower_bound_default_rule");
}




Typed::RuleSet PasulaLearner::learn_quiet(const TransitionGroup<TypedAtom>& transitions, const Typed::RuleSet& previous_rules)
{
    CINFO("planning") << "Learning... " << NOENDL;

    std::streambuf* cout_sbuf = std::cout.rdbuf(); // save original sbuf
    std::ofstream   fout("/dev/null");
    std::cout.rdbuf(fout.rdbuf()); // redirect 'cout' to a 'fout'
    Typed::RuleSet result = learn(transitions, previous_rules);
    std::cout.rdbuf(cout_sbuf); // restore the original stream buffer

    CINFO("planning") << "learned  " << result.size() << " rules.";

    return result;
}


Typed::RuleSet PasulaLearner::learn(const TransitionGroup<TypedAtom>& transitions, const Typed::RuleSet& previous_rules)
{
    std::cout << "PasulaLearner: " << "Pasula learner started" << endl;
    Domain domain(config_reader);
    domain.index_transitions(transitions);
    
    TransitionGroup<TypedAtom> indexed_transitions = domain.get_transitions_with_indexed_objects(transitions);
    indexed_transitions.remove_negated_predicates();
    indexed_transitions.write_pasula_transitions_to_file(transitions_filename);

    // write symbols
    std::stringstream ss;
    domain.write_NID(ss);
    utils::write_string_to_file(symbols_filename, ss.str());

    try {
        learn_pasula();
    }
    catch ( std::exception& e )
    {
        std::cerr << "Exception was caught:" << e.what() << "\nLearner failed.\n";
    }
    std::cout << "PasulaLearner: " << "Pasula learner finished" << endl;
    // get learned rules
    Typed::RuleSet new_rules;
    new_rules.read_NID_rules("learned_rules.dat", domain);
    // remove no change rules
#ifdef REMOVE_NOP_RULES
    new_rules.remove_nop_rules();
#endif
    return new_rules;
}


bool PasulaLearner::learn_pasula()
{
    // Rule learning algorithm is heuristic and makes some random choices.
//     srand (time(NULL));
    rnd.seed(rand());

    // -------------------------------------
    //  PARAMETERS
    // -------------------------------------

    // Log-file
    MT::String logfile("/tmp/learn.log");

    // Atoms
    relational::SymL symbols;
    relational::ArgumentTypeL types;
    relational::reason::resetConstants();
    relational::readSymbolsAndTypes(symbols, types, symbols_filename.c_str() );

    // Data
    relational::StateTransitionL transitions = relational::StateTransition::read_SAS_SAS(transitions_filename.c_str() );
    std::cout << "TRANSITIONS N";
    PRINT(transitions.N);
    //   write(transitions);
    if (transitions.N == 0) {
        std::cerr << "No transitions provided" << std::endl;
        return false;
    }

    // -------------------------------------
    //  PREVIOUS RULES
    // -------------------------------------
//     cout<<endl;
//     relational::RuleSet rules;
//     relational::RuleSet::read(rules_filename.c_str(), rules);
//     cout<<"Rules successfully read from file \""<< rules_filename.c_str() <<"\"."<<std::endl<<flush;

    // -------------------------------------
    //  LEARN
    // -------------------------------------

    relational::learn::set_penalty(alpha_pen);
    relational::learn::set_p_min(noise_lower_bound, noise_lower_bound_default_rule);
    relational::RuleSetContainer rulesC;
    cout << "Starting learning" << std::endl;
    relational::learn::learn_rules(rulesC, transitions);

//         relational::write(rulesC.rules, rulesFile_name);
//         rulesC.rules.write(cout);
    relational::write(rulesC.rules, "learned_rules.dat");
    cout << "Rules learned" << std::endl;

    return true;
}

