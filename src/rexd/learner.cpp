#include "learner.h"


Learner::Learner() {
}


Typed::RuleSet Learner::learn_action_rules(const Typed::RuleSet& previous_rules, const TransitionGroup<TypedAtom>& transitions, const TypedAtom action)
{
    Typed::RuleSet removed_rules, added_rules;
    return learn_action_rules(previous_rules, transitions, action, removed_rules, added_rules);
}

Typed::RuleSet Learner::learn_action_rules(const Typed::RuleSet& previous_rules, const TransitionGroup<TypedAtom>& transitions, const TypedAtom& action, Typed::RuleSet& removed_rules, Typed::RuleSet& added_rules)
{
    int DEBUG = 0;
    if (action.name == ACTION_NAME_NOOP) { // HACK for Pasula
        return previous_rules;
    }
    TypedAtom symbolic_action(action);
    symbolic_action.make_symbolic();
    TransitionGroup<TypedAtom> transitions_action = transitions.get_action_transitions(symbolic_action);
    if (DEBUG > 0) {
        LOG(INFO) << "Learner::learn_action_rules - " << "Starting learning...";
    }
    if (DEBUG > 1) {
        LOG(INFO) << "Action " << action << " transitions are " << "\n" << transitions_action;
        added_rules = learn(transitions_action, previous_rules);
    }
    else {
        added_rules = learn_quiet(transitions_action, previous_rules);
    }
    if (DEBUG > 0) {
        LOG(INFO) << "Learner::learn_action_rules - " << "Finished learning.";
    }
    
    Typed::RuleSet result_rules = previous_rules;

    removed_rules = result_rules.remove_action_rules(symbolic_action);
    
    // maintain same identifiers as much as possible in newer rules
    added_rules.set_identifiers(removed_rules);

    // Add new rules to current rules
    result_rules.add_rules(added_rules);
    
    return result_rules;
}


