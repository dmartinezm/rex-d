/*
 * Scenario class that has all information needed to create planning models to be used by REX-D.
 * Copyright (C) 2014  David Martínez (dmartinez@iri.upc.edu)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#include "rexd_scenario.h"

RexdScenario::RexdScenario(const RexdScenario& rexd_scenario): 
    Scenario(rexd_scenario),
    use_rmax(rexd_scenario.use_rmax),
    use_vmin(rexd_scenario.use_vmin),
    vmin_value(rexd_scenario.vmin_value)
{
    if (use_rmax) {
        set_rmax(rexd_scenario.transitions_ptr);
    }
}

RexdScenario::RexdScenario(const Scenario& scenario): 
    Scenario(scenario),
    use_rmax(false),
    use_vmin(false)
{
    reset_vmin_value();
}


void RexdScenario::set_rmax(const TransitionGroupPtr<TypedAtom> transitions_ptr_)
{
    use_rmax = false;
    if (config_reader.get_variable<bool>("use_rmax")) {
        this->use_rmax = true;
        this->rmax_reward = config_reader.get_variable<float>("rmax_reward");
        this->explore_threshold = config_reader.get_variable<uint>("exploration_known_threshold");
        this->transitions_ptr = transitions_ptr_;
    }
}


void RexdScenario::set_vmin()
{
    use_vmin = false;
    if (config_reader.get_variable<bool>("use_vmin") && (get_planning_horizon() > 1)) { // HACK, with horizon==1 sometimes the teacher gets called
        if ( (!config_reader.get_variable<bool>("final_reward_action")) && (config_reader.get_variable<std::string>("planner") == "gpack") )
            LOG(WARNING) << "V-MIN requires final_reward_action to be enabled if G-PACK is used.";
        this->use_vmin = true;
    }
}

void RexdScenario::modify_vmin_value(const double& value_modification) {
    this->vmin_value = vmin_value + value_modification;
}

bool RexdScenario::is_planning_state_equal(const RexdScenario& other) const
{
    if (get_planning_horizon() != other.get_planning_horizon()) {
        return false;
    }
    else if (this->get_rules() != other.get_rules()) {
        return false;
    }
    else if (this->use_rmax != other.use_rmax) {
        return false;
    }
    else if (this->use_vmin != other.use_vmin) {
        return false;
    }
    else if (goals != other.goals) {
        return false;
    }
    else if (use_rmax != other.use_rmax) {
        return false;
    }
    else if (use_rmax) {
        Explorer explorer;
        Typed::RuleSet this_unknown_rules = explorer.get_rmax_ruleset(*this, *transitions_ptr, explore_threshold, rmax_reward);
        Typed::RuleSet other_unknown_rules = explorer.get_rmax_ruleset(other, *other.transitions_ptr, other.explore_threshold, other.rmax_reward);
        return this_unknown_rules == other_unknown_rules;
    }
    else {
        return true;
    }
}

void RexdScenario::apply_vmin(TypedAtom& action, float& action_value) const
{
    if (use_vmin && (config_reader.get_variable<std::string>("planner") == PLANNER_PROST) ) {
        if (action_value < get_updated_vmin()) {
            CINFO("planning") << "Previous action was " << action << "(" << action_value << ")";
            action = MAKE_TYPED_ATOM_TEACHER;
            action_value = get_updated_vmin();
        }
    }
}

float RexdScenario::get_updated_vmin() const
{
    return vmin_value - current_reward;
}
