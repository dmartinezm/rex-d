/*
 * Scenario class that has all information needed to create planning models to be used by REX-D.
 * Copyright (C) 2016  David Martínez (dmartinez@iri.upc.edu)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef REXD_SCENARIO_H
#define REXD_SCENARIO_H

#include <boost/lambda/lambda.hpp>
#include <symbolic/scenario.h>
#include <rexd/explorer.h>

/**
 * \class RexdScenario
 * \brief Extended Scenario to generate include REX-D information for exploration-exploitation-demonstration.
 * 
 */
class RexdScenario : public Scenario
{
public:
    
    /**
     * \brief Creates RexdScenario. R-MAX and V-MIN are disabled by default.
     */
    RexdScenario(const Scenario& scenario);
    
    /**
     * \brief Creates RexdScenario. R-MAX and V-MIN settings are copied. New object uses pointer to same transitions data.
     */
    RexdScenario(const RexdScenario& rexd_scenario);
    
    /**
     * \brief Initializes V-MIN if enabled in config file.
     */
    void set_vmin();
    
    /*!
    \brief Resets internal variables.
    */
    virtual void reset() { 
        reset_vmin_value(); 
        Scenario::reset(); 
    };
    
    /**
     * \brief Resets internal v-min value in the scenario.
     */
    void reset_vmin_value() {
        this->vmin_value = config_reader.get_variable<float>("vmin_value");
    }
    
    /**
     * \brief Modifies internal v-min value in the scenario.
     */
    void modify_vmin_value(const double& value_modification);
    
    /**
     * \brief Initializes R-MAX if enabled in config file. Transitions are not copied, just using the pointer to the same data.
     */
    void set_rmax(const TransitionGroupPtr<TypedAtom> transitions_ptr_);
    
    /**
     * \brief Checks if the planner input would be the same with both scenarios.
     */
    bool is_planning_state_equal(const RexdScenario& other) const;
    
    /**
     * \brief Sets a "teacher" action if the value < Vmin.
     */
    void apply_vmin(TypedAtom& action, float& action_value) const;
    
    /**
     * \brief Get updated Vmin value (adding all rewards obtained during the episode).
     */
    float get_updated_vmin() const;
    
    
protected:
    // rmax, vmin stuff
    bool use_rmax;
    bool use_vmin;
    uint explore_threshold;
    float rmax_reward;
    float vmin_value;
    TransitionGroupPtr<TypedAtom> transitions_ptr;
    
};

#endif // REXD_SCENARIO_H
