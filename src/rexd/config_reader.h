/*
 * Class to manage the configuration parameters.
 * Copyright (C) 2016  <David Martínez> <dmartinez@iri.upc.edu>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */


#ifndef CONFIG_READER_H
#define CONFIG_READER_H

#include <fstream>
#include <boost/program_options.hpp>
#include <symbolic/utils.h>

// define some constant variables
#define LEARNER_PASULA "pasula"
#define LEARNER_LFIT   "lfit"
#define PLANNER_PRADA  "prada"
#define PLANNER_GPACK  "gpack"
#define PLANNER_PROST  "prost"

#define DOMAIN_AUTAS "autas"
#define DOMAIN_CROSSING_TRAFFIC "crossing"
#define DOMAIN_ELEVATORS "elevator"
#define DOMAIN_TRIANGLE_TIREWORLD "triangle"
#define DOMAIN_KITCHEN "kitchen"
#define DOMAIN_TABLEWARE "tableware"

/*!
\class ConfigReader
\brief Class to read the configuration file and command line params.

It uses boost::program_options.\n
Command line params should be trivial to add, but they are not parsed at this moment.

TODO create a class ConfigReaderIntellact : public ConfigReader, and move there all specific IntellAct configs.
*/
class ConfigReader
{

public:
    typedef boost::shared_ptr<ConfigReader> Ptr;
    typedef boost::shared_ptr<const ConfigReader> ConstPtr;

    std::string files_path;
    
    ConfigReader();
    ConfigReader(const ConfigReader& other);
    #if __cplusplus > 199711L // C++11
        virtual ~ConfigReader() = default;
    #else
        virtual ~ConfigReader() {}
    #endif

    /** 
     * \brief read the config file from a path, and sets the default file path from which to get relative paths 
     * 
     * \param config_path path of the config file
     * \param files_path_ path to be added to relative paths to get absolute paths
     * \return True if initilization was successful.
     */
    bool init(const std::string& config_path, const std::string& files_path_ = "./");

    /** 
     * \brief Gets the value of a config variable
     * 
     * \param variable variable whose value is to be obtained.
     * \return Value of the variable.
     */
    template<typename T>
    T get_variable(std::string variable) const {
        if (vm.count(variable)) {
            return vm[variable].as<T>();
        }
        else {
            std::cerr << "ConfigReader::ERROR: " << "Variable " << variable << " not found!" << std::endl;
            assert(0);
            exit(-1);
        }
    }

    /** 
     * \brief Gets an absolute path by appending config path to a relative path given by the variable (does nothing if it is already absolute path)
     * 
     * \param variable variable that has the path.
     * \return Value of the absolute path.
     */
    std::string get_path(std::string variable) const {
        if (vm.count(variable)) {
            std::string path = vm[variable].as<std::string>();
            if (utils::is_absolute_path(path)) {
                return path;
            }
            else {
                return files_path + path;
            }
        }
        else {
            std::cerr << "ConfigReader::ERROR: " << "Variable " << variable << " not found!" << std::endl;
            assert(0);
            exit(-1);
        }
    }
    
    /** 
     * \brief Gets an absolute path by appending executable path folder to the relative path given by the variable (does nothing if it is already absolute path)
     * 
     * \param variable variable that has the path.
     * \return Value of the absolute path.
     */
    std::string get_binary_path(std::string variable) const {
        if (vm.count(variable)) {
            std::string path = vm[variable].as<std::string>();
            if (utils::is_absolute_path(path)) {
                return path;
            }
            else {
                return utils::get_selfpath() + "/" + path;
            }
        }
        else {
            std::cerr << "ConfigReader::ERROR: " << "Variable " << variable << " not found!" << std::endl;
            assert(0);
            exit(-1);
        }
    }

    /** 
     * \brief Changes a variable value.
     * 
     * HACK? Not sure if I am supposed to do this...
     * 
     * \param variable variable to be changed.
     * \param new_value new value to be set to the variable.
     */
    template<typename T>
    void set_variable(std::string variable, T new_value) {
        boost::program_options::variable_value aux = boost::program_options::variable_value(new_value, false);
        boost::program_options::variables_map::iterator it=vm.find(variable);
        it->second = aux;
    }

private:
    boost::program_options::variables_map vm;
};

#endif // CONFIG_READER_H
