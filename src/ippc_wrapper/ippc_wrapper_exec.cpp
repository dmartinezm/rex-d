#include "ippc_wrapper_exec.h"


int main(int argc, char** argv) {
    ConfigReader config_reader;
    config_reader.init("rexd_config.cfg");
    Scenario scenario(config_reader.get_path("ppddl_domain_file"), config_reader.get_path("simulator_ppddl_current_problem_file"), config_reader);
    float planning_result;

    XMLServer_t mserver;
    mserver.start_session(2323);
    mserver.start_round();
    while (scenario.get_planning_horizon() > 0) {
        std::cout << "Sending state " << scenario.get_state() << std::endl;
        NonTypedAtom action = mserver.get_action(scenario, planning_result);
        std::cout << "Obtained action " << action << "with result " << planning_result << std::endl;
        TypedAtom typed_action = scenario.get_state().ppddl_object_manager.get_typed_atom(action);
        scenario.transition_next_state_simple(typed_action);
        scenario.decrease_planning_horizon();
        std::cout << "Press enter to request a new action" << std::endl;
        utils::read_string_from_stream(std::cin);
    }
    mserver.end_round();
    mserver.end_session();
    
    std::cout << "Finishing" << std::endl;
    return 0;
}
