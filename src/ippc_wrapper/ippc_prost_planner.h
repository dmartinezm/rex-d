/*
 * IPPC PROST planner wrapper.
 * Copyright (C) 2016  David Martínez (dmartinez@iri.upc.edu)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef REXD_IPPC_PROST_PLANNER_H
#define REXD_IPPC_PROST_PLANNER_H

#include <ippc_wrapper/ippc_planner.h>

#include <rexd/rddl_scenario.h>


/**
 * \class IppcProstPlanner
 * \brief IPPC planner wrapper. Launches and connects to a planner using the IPPC 2011 interface.
 * 
 * Inherits functionality from ippc_planner.h.\n
 * Works with prost planner from IPPC 2014.\n
 */
class IppcProstPlanner : public IppcPlanner {
public:
    IppcProstPlanner(const ConfigReader& config_reader);

private:
    std::string planner_parser_path;

    std::string rddl_domain_path;
    std::string rddl_instance_file;
    std::string rddl_template_file;
    
    std::string current_path;
    
    std::string planner_algorithm;
    uint planning_miliseconds;
    uint planning_fast_miliseconds;

    virtual void execute_planner_command(const bool thoroughly, const int verbose) const;
    virtual void write_problem_files(const RexdScenario& rexd_scenario) const;
};

#endif
