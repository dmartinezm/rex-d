#include "ippc_planner.h"
#include "boost/filesystem/operations.hpp"

IppcPlanner::IppcPlanner(const ConfigReader& config_reader)
{
    noop_actions_allowed = config_reader.get_variable<bool>("noop_actions_allowed");
    
    // check existance of binary files
    planner_path = config_reader.get_binary_path("planner_path");
    if ( !boost::filesystem::exists( planner_path ) ) {
        CERROR("planning") << "Planner executable " << planner_path << " does not exist!";
    }
    
    // port
    planner_port = config_reader.get_variable<uint>("planner_port");
    if (planner_port == 0) { // auto selection
        planner_port = utils::get_socket_from_current_dir_hash(1);
    }
}

IppcPlanner::~IppcPlanner() {
    shut_down_planner();
}

void IppcPlanner::shut_down_planner() {
    if (planner_running) {
        ippcserver_ptr->end_round();
        ippcserver_ptr->end_session();
        int status;
        wait(&status);
        ippcserver_ptr.reset();
        usleep(100000); // Give time to the planner to actually shut down
        planner_running = false;
    }
}

int IppcPlanner::estimate_distance_to_finish(const RexdScenario &rexd_scenario)
{
    std::vector< TypedAtom > planning_res;
    if (!plan_n_actions(rexd_scenario, planning_res, false)) {
        return 0;
    }
    LOG(INFO) << "Estimated distance to goal: " << planning_res.size();
    return planning_res.size();
}


bool IppcPlanner::plan_n_actions(const RexdScenario& rexd_scenario, std::vector<TypedAtom>& plan_result, const bool thoroughly, const int verbose, const uint n_actions, const bool force_effect)
{
    RexdScenario updated_scenario(rexd_scenario);
    TypedAtomOpt current_action_opt;
    bool reached_goal = false;

    if (rexd_scenario.is_in_goal()) {
        if (verbose)
            CINFO("planning") << "Is goal. Not planning.";
        return true;
    }

    uint max_iterations = std::min(n_actions, rexd_scenario.get_planning_horizon());
    uint iteration = 0;
    bool finished_planning = false;
    while ( (iteration < max_iterations) && !finished_planning ) {
        if (verbose) {
            CINFO("planning") << "Plan action " << iteration << " with state: " << Predicates::transform_to_nontyped(updated_scenario.get_state());
            CINFO("planning") << "     and objects: " << updated_scenario.get_state().ppddl_object_manager;
        }
        else
            std::cout << "." << std::flush;
        
        // plan
        float current_reward;
        current_action_opt = this->plan(updated_scenario, thoroughly, verbose, current_reward);
        
        // apply current action
        if (current_action_opt) {
            plan_result.push_back(*current_action_opt);
            if (current_action_opt->is_special()) {
                if (*current_action_opt == MAKE_TYPED_ATOM_TEACHER) {
                    if (verbose) {
                        CINFO("planning") << "Failed: Teacher action planned.";
                    }
                    finished_planning = true;
                }
            }
            bool success; // not used
            float reward; // not used
            updated_scenario.transition_next_state(*current_action_opt, success, reward, force_effect);
            updated_scenario.decrease_planning_horizon();
        }
        else { // no solution -> finished
            finished_planning = true;
            reached_goal = true;
            if (verbose) {
                CINFO("planning") << "No more solutions.";
            }
        }

        // check goal
        if (updated_scenario.is_in_goal()) {
            finished_planning = true;
            reached_goal = true;
            if (verbose)
                CINFO("planning") << "Reached a goal!";
        }
        
        if (verbose >= 1) {
            LOG(INFO) << "Current reward is " << updated_scenario.get_reward();
        }
        ++iteration;
    }
    return reached_goal;
}


TypedAtomOpt IppcPlanner::plan(const RexdScenario& rexd_scenario, const bool thoroughly, const int verbose, float& planning_result)
{
    TypedAtomOpt resulting_plan;

    if (verbose) {
        if (thoroughly)
            CINFO("planning") << "Planning full... " << NOENDL;
        else
            CINFO("planning") << "Planning fast... " << NOENDL;
    }
    
    resulting_plan = ippcPlan(rexd_scenario, thoroughly, verbose, planning_result);

    if (verbose) {
        if (resulting_plan) {
            CINFO("planning") << " Action: " << START_COLOR_BLUE << *resulting_plan << END_COLOR << " (" << planning_result << ")";
        }
        else {
            CINFO("planning") << " No plan";
        }
    }
    return resulting_plan;
}


TypedAtomOpt IppcPlanner::ippcPlan(const RexdScenario& rexd_scenario, const bool thoroughly, const int verbose, float& planning_result)
{
    if (rexd_scenario.get_planning_horizon() == 0) {
        planning_result = 0;
        return TypedAtomOpt();
    }
    
    // Close previous planner instance if running and the planning state has changed or using GPACK
    if ( planner_running && 
         ( (!running_scenario_ptr->is_planning_state_equal(rexd_scenario)) || // rules changed
           (rexd_scenario.config_reader.get_variable<std::string>("planner") == PLANNER_GPACK) // G-PACK planner not supported to reuse planning instance
         )
       ) {
        shut_down_planner();
    }
    
    write_problem_files(rexd_scenario);
    
    std::streambuf *cout_buf;
    if (!verbose) {
        cout_buf = stream_quiet(std::cout);
    }
    
    if (!planner_running) {
        int pid = fork();
        if ( pid == -1 ) {
            perror("Cannot proceed. fork() error");
            exit(-1);
        }
        if (pid == 0)
        {
            // child process
            utils::initialize_random_seed(); // regenerate rand in the child process
            execute_planner_command(thoroughly, verbose);
            exit(0);
        }
        
        planner_running = true;
        running_scenario_ptr = boost::make_shared<RexdScenario>(rexd_scenario);
    
        ippcserver_ptr = boost::shared_ptr<XMLServer_t>(new XMLServer_t());
        ippcserver_ptr->start_session(planner_port);
        ippcserver_ptr->start_round();
    }

    NonTypedAtom non_typed_action = ippcserver_ptr->get_action(rexd_scenario, planning_result);
    running_scenario_ptr->decrease_planning_horizon();
    TypedAtom action = rexd_scenario.get_state().ppddl_object_manager.get_typed_atom(non_typed_action);
    rexd_scenario.apply_vmin(action, planning_result);
    
    if (noop_actions_allowed) {
        if (action.name == "") {
            action = MAKE_TYPED_ATOM_NOOP_ACTION;
        }
        if (planning_result < -100000.0) {
            action = TypedAtom("");
        }
    }
    else {
        if (action == MAKE_TYPED_ATOM_NOOP_ACTION) {
            action = TypedAtom("");
        }
    }
    
    if (!verbose) {
        stream_restore(std::cout, cout_buf);
    }

    if (utils::trim(action.name) == "") {
        planning_result = -100000.0;
        return TypedAtomOpt();
    }

    return TypedAtomOpt(action);
}

