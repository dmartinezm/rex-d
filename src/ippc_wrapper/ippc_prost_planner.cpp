#include "ippc_prost_planner.h"
#include "boost/filesystem/operations.hpp"

IppcProstPlanner::IppcProstPlanner(const ConfigReader& config_reader):
    IppcPlanner(config_reader)
{
    rddl_template_file = config_reader.get_path("rddl_domain_template_path");
    rddl_domain_path = config_reader.get_path("rddl_domain_path");
    rddl_instance_file = config_reader.get_path("rddl_instance_path");
    current_path = config_reader.files_path;
    
    planning_miliseconds = config_reader.get_variable<uint>("planning_time");
    planning_fast_miliseconds = config_reader.get_variable<uint>("planning_fast_time");
    planner_algorithm = config_reader.get_variable<std::string>("planner_algorithm");
    
    // check existance of binary files
    planner_parser_path = config_reader.get_binary_path("planner_parser_path");
    if ( !boost::filesystem::exists( planner_parser_path ) ) {
        CERROR("planning") << "Planner parser executable " << planner_parser_path << " does not exist!";
    }
}

void IppcProstPlanner::write_problem_files(const RexdScenario& rexd_scenario) const
{
    RDDLScenario rddl_scenario(rexd_scenario);
    rddl_scenario.write_RDDL_domain_to_file(rddl_template_file, rddl_domain_path);
    rddl_scenario.write_RDDL_instance_to_file(rddl_instance_file);
}

void IppcProstPlanner::execute_planner_command(const bool thoroughly, const int verbose) const
{
    std::string port_str = boost::lexical_cast<std::string>(planner_port);
    std::string hostname = "localhost";
    std::string parser_out_file = "inst_mdp";
    
    std::string seconds_str;
    if (thoroughly) {
        seconds_str = boost::lexical_cast<std::string>( (float)planning_miliseconds / (float)1000 );
    }
    else {
        seconds_str = boost::lexical_cast<std::string>( (float)planning_fast_miliseconds / (float)1000 );
    }
    std::string search_config = planner_algorithm + " -t " + seconds_str;
    
    std::string rddl_parser_command = planner_parser_path + " " + rddl_domain_path + " " + rddl_instance_file + " " + current_path;
    std::string prost_command = planner_path + " " + current_path + parser_out_file + " -h " + hostname + " -p " + port_str + " [PROST -se [" + search_config + "]]";
    
    // silence the planner
    if (verbose < 2) {
        int tmpFd = open( "/dev/null", O_WRONLY );
        dup2( tmpFd, 1 );
        close( tmpFd );
    }

    // wait for server to be ready
    usleep(1000*10); // 10 ms
    
    int system_res = system(rddl_parser_command.c_str());
    if ( (system_res) || (!boost::filesystem::exists(current_path + "/" + parser_out_file)) ) {
        std::cerr << START_COLOR_RED << "PROST Planner: Failed to execute parser: " << rddl_parser_command << END_COLOR;
    }
    LOG(INFO) << "Executing: " << prost_command;
    system_res = system(prost_command.c_str());
    if (system_res) {
        std::cerr << START_COLOR_RED << "PROST Planner: Failed to execute planner: " << prost_command << END_COLOR;
    }
    
    // remove tmp files
//     LOG(INFO) << "Removing: " << parser_out_file_path;
//     boost::filesystem::path parser_out_file_path = "./" + parser_out_file;
//     boost::filesystem::remove(parser_out_file_path);
}

