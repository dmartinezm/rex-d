/*
 * IPPC planner wrapper.
 * Copyright (C) 2016  David Martínez (dmartinez@iri.upc.edu)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef REXD_IPPC_PLANNER_H
#define REXD_IPPC_PLANNER_H

#include <rexd/planner.h>

#include <fcntl.h>
#include <sys/wait.h>

#include <vector>

#include <symbolic/rules.h>
#include <symbolic/predicates.h>
#include <symbolic/state.h>
#include <symbolic/scenario.h>

#include <boost/thread.hpp>

#include <rexd/rddl_scenario.h>
#include <rexd/config_reader.h>
#include <ippc_wrapper/ippc_server.h>


/**
 * \class IppcPlanner
 * \brief IPPC planner wrapper. Launches and connects to a planner using the IPPC 2011/2014 interface.
 * 
 * Inherits functionality from planner.h.
 */
class IppcPlanner : public Planner {
public:
    IppcPlanner(const ConfigReader& config_reader);
    ~IppcPlanner();

    using Planner::plan; // Once you overload a function from Base class in Derived class all functions with the same name in the Base class get hidden in Derived class.
    virtual TypedAtomOpt plan(const RexdScenario& rexd_scenario, const bool thoroughly, const int verbose, float& planning_result);
    virtual bool plan_n_actions(const RexdScenario& rexd_scenario,
                                std::vector<TypedAtom>& plan,
                                const bool thoroughly = true, const int verbose = 0,
                                const uint n_actions = std::numeric_limits<uint>::max(),
                                const bool force_effect = false);

    virtual int estimate_distance_to_finish(const RexdScenario &rexd_scenario);

protected:
    std::string planner_path;
    uint planner_port;
    
private:
    bool noop_actions_allowed;
    bool planner_running = false;
    boost::shared_ptr<RexdScenario> running_scenario_ptr;
    boost::shared_ptr<XMLServer_t> ippcserver_ptr;
    
    TypedAtomOpt ippcPlan(const RexdScenario& rexd_scenario, const bool thoroughly, const int verbose, float& planning_result);
    
    void shut_down_planner();
    
    virtual void execute_planner_command(const bool thoroughly, const int verbose) const =0;
    virtual void write_problem_files(const RexdScenario& rexd_scenario) const =0;
};

#endif
