/*
 * IPPC G-pack planner wrapper.
 * Copyright (C) 2016  David Martínez (dmartinez@iri.upc.edu)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef REXD_IPPC_GPACK_PLANNER_H
#define REXD_IPPC_GPACK_PLANNER_H

#include <ippc_wrapper/ippc_planner.h>

#include <rexd/ppddl_scenario.h>


/**
 * \class IppcGpackPlanner
 * \brief IPPC planner wrapper. Launches and connects to a planner using the IPPC 2011 interface.
 * 
 * Inherits functionality from ippc_planner.h.\n
 * Works with glutton/gourmand planner from IPPC 2011.
 */
class IppcGpackPlanner : public IppcPlanner {
public:
//     std::string rules_file_path;

    IppcGpackPlanner(const ConfigReader& config_reader);

private:
    std::string planner_algorithm;

    std::string ppddl_rules_file;
    std::string ppddl_template_file;

    uint planning_miliseconds;
    uint planning_fast_miliseconds;
    double planning_epsilon;

    virtual void execute_planner_command(const bool thoroughly, const int verbose) const;
    virtual void write_problem_files(const RexdScenario& rexd_scenario) const;
};

#endif
