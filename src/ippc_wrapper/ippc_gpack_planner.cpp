#include "ippc_gpack_planner.h"

IppcGpackPlanner::IppcGpackPlanner(const ConfigReader& config_reader):
    IppcPlanner(config_reader)
{
    ppddl_rules_file = config_reader.get_path("ppddl_grounded_rules_path");
    ppddl_template_file = config_reader.get_path("ppddl_rules_template_path");
    planning_miliseconds = config_reader.get_variable<uint>("planning_time");
    planning_fast_miliseconds = config_reader.get_variable<uint>("planning_fast_time");
    planning_epsilon = config_reader.get_variable<double>("planning_epsilon");
    planner_algorithm = config_reader.get_variable<std::string>("planner_algorithm");
}


void IppcGpackPlanner::write_problem_files(const RexdScenario& rexd_scenario) const
{
    PPDDLScenario ppddl_scenario(rexd_scenario);
    ppddl_scenario.write_IPPC_PPDDL_to_file(ppddl_template_file, ppddl_rules_file);
}

void IppcGpackPlanner::execute_planner_command(const bool thoroughly, const int verbose) const
{
    int DEBUG = 0;
    
    std::string miliseconds_str;
    //     c++0x
    //     std::string s = std::to_string(42);
    if (thoroughly) {
        miliseconds_str = static_cast<std::ostringstream*>( &(std::ostringstream() << planning_miliseconds) )->str();
    }
    else {
        miliseconds_str = static_cast<std::ostringstream*>( &(std::ostringstream() << planning_fast_miliseconds) )->str();
    }
    std::string epsilon_str = static_cast<std::ostringstream*>( &(std::ostringstream() << planning_epsilon) )->str();
    std::string seed_str = static_cast<std::ostringstream*>( &(std::ostringstream() << rand()) )->str();
    std::string port_str = static_cast<std::ostringstream*>( &(std::ostringstream() << planner_port) )->str();

    std::string glutton_planner_path = planner_path;
    std::string glutton_planner_algorithm = planner_algorithm;
    std::string server = "localhost:" + port_str;
    std::string problem_instance = "ppddl_problem";

    char* glutton_planner_c = utils::string_to_char(glutton_planner_path);
    char* glutton_planner_algorithm_c = utils::string_to_char(glutton_planner_algorithm);
    char* miliseconds_str_c = utils::string_to_char(miliseconds_str);
    char* epsilon_str_c = utils::string_to_char(epsilon_str);
    char* seed_str_c = utils::string_to_char(seed_str);
    char* server_c = utils::string_to_char(server);
    char* ppddl_rules_file_c = utils::string_to_char(ppddl_rules_file);
    char* problem_instance_c = utils::string_to_char(problem_instance);
    char t_param_c[] = "-t";
    char r_param_c[] = "-r";
    char e_param_c[] = "-e";
    char p_param_c[] = "-p";

    char* args[] = { glutton_planner_c, t_param_c, miliseconds_str_c, e_param_c, epsilon_str_c, p_param_c, glutton_planner_algorithm_c, r_param_c, seed_str_c, server_c, ppddl_rules_file_c, problem_instance_c, NULL };
    
    if (DEBUG > 0) {
        LOG(INFO) << "Executing: " << glutton_planner_c << " " << t_param_c << " " << miliseconds_str_c << " " << e_param_c << " " << epsilon_str_c << " " << p_param_c << " " << glutton_planner_algorithm_c << " " << r_param_c << " " << seed_str_c << " " << server_c << " " << ppddl_rules_file_c << " " << problem_instance_c;
    }

    // silence the planner
    if (verbose < 2) {
        int tmpFd = open( "/dev/null", O_WRONLY );
        dup2( tmpFd, 1 );
        close( tmpFd );
    }

    // wait for server to be ready
    usleep(1000*10); // 10 ms

    // Now execute it
    int res = execvp( args[ 0 ], args );
    if (res == -1)
        printf("ERROR executing execvp. child's execvp: %s\n", strerror(errno));
}

