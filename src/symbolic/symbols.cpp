#include "symbols.h"
#include "symbolic/utils.h"

// OPERATORS

bool operator== (const NonTypedAtom &s1, const NonTypedAtom &s2)
{
    return ((s1.name == s2.name) && (s1.params == s2.params));
}

bool operator<(const NonTypedAtom& l, const NonTypedAtom& r)
{
    if (l.name < r.name) {
        return true;
    } else if (r.name < l.name) {
        return false;
    } else { //equal names
        if (l.params.size() < r.params.size())
            return true;
        else if (r.params.size() < l.params.size())
            return false;
        else { // equal number of parameters
            std::vector< NonTypedAtom::ParamType >::const_iterator l_it, r_it;
            r_it = r.params.begin();
            for (l_it = l.params.begin(); l_it != l.params.end(); ++l_it) {
                if (l_it->name < r_it->name) {
                    return true;
                } else if (r_it->name < l_it->name) {
                    return false;
                }
                ++r_it;
            }
        }
    }
    return false;
}

std::ostream& operator<<(std::ostream &out, const NonTypedAtom &s)
{
    out << s.name << "(";
    for (size_t i = 0; i < s.params.size(); i++) {
        out << s.params[i].name;
        if ((i + 1) < s.params.size())
            out << ", ";
    }
    out << ")";
    return out;
}

template<>
void TypedAtom::write_PPDDL(std::ostream &out) const
{
    out << "(" << name << " ";
    write_typed_parameters(out);
    out << ")";
}

void NonTypedAtom::write_PPDDL(std::ostream &out) const
{
    out << "(" << name << " ";
    BOOST_FOREACH(const ParamType& param, params) {
        out << param.name << " ";
    }
    out << ")";
}

template<>
void TypedAtom::write_RDDL(std::ostream &out, const bool future_state) const
{
    out << "(" << name;
    if (future_state) {
        out << "'";
    }
    BOOST_FOREACH(const ParamType& param, params) {
        out << " " << param.name;
    }
    out << ")";
}


// FullAtom
template<>
bool FullAtom<PPDDL::FullObject>::is_symbolic() const
{
    BOOST_FOREACH(const PPDDL::FullObject& param, params) {
        if (symbolic::is_param_symbolic(param.name)) { // found!
            return true;
        }
    }
    return false;
}

// FullAtom
template<>
bool FullAtom<PPDDL::IndexedObject>::is_symbolic() const
{
    BOOST_FOREACH(const PPDDL::IndexedObject& param, params) {
        if (param.name < MAX_SYMBOLIC_PARAMS) { // found!
            return true;
        }
    }
    return false;
}

template<>
std::string FullAtom<PPDDL::FullObject>::load_atom_name_from_string(const std::string &line)
{
    std::stringstream ss(line);
    std::string item;

    if (std::getline(ss, item, '(')) {
        //std::cout << "Atom name: " << item << std::endl;
    }

    return item;
}

template<>
std::vector< std::string > FullAtom<PPDDL::FullObject>::get_param_names_from_nid_parameters_string(const std::string &nid_parameters)
{
    std::stringstream ss;
    std::string item;
    std::vector< std::string> param_names;
    ss.str(nid_parameters);
    
    char separator = utils::choose_variable_separator(nid_parameters);
    while (std::getline(ss, item, separator)) {
        // remove spaces and empty strings
        item.erase(remove_if(item.begin(), item.end(), isspace), item.end());
        if (item.compare("") != 0) {
            param_names.push_back(item);
//             std::cout << "Param names: " << item << std::endl;
        }
    }

    return param_names;
}


template<>
std::vector< std::string > FullAtom<PPDDL::FullObject>::get_atom_param_names_from_string(const std::string &line)
{
    if (line.find('(') == std::string::npos) { // doesn't have params
        return std::vector< std::string >();
    }
    
    std::stringstream ss(line);
    std::string item;

    // get string between ()
    std::getline(ss, item, '(');
    std::getline(ss, item, ')');
    
    assert((item.find('?') == std::string::npos) && (item.find(" - ") == std::string::npos));
//     if ( (item.find('?') != std::string::npos) && (item.find(" - ") != std::string::npos)) {
        // if has '?', then it should be PPDDL rules
//         return get_param_names_from_ppddl_parameters_string(item);
//     }
//     else {
        return get_param_names_from_nid_parameters_string(item);
//     }
}

NonTypedAtom::NonTypedAtom(const std::string &name_, const std::vector<std::string> &param_names_)
{
    name = name_;
    BOOST_FOREACH(const std::string& param_name, param_names_) {
        params.push_back(ParamType(param_name, DEFAULT_OBJECT_TYPE));
    }
}

NonTypedAtom::NonTypedAtom(const std::string &line)
{
    name = load_atom_name_from_string(line);
    std::vector<std::string> read_param_names = get_atom_param_names_from_string(line);
    BOOST_FOREACH(const std::string& param_name, read_param_names) {
        params.push_back(ParamType(param_name, DEFAULT_OBJECT_TYPE));
    }
}

template<>
bool TypedAtom::is_valid() const
{
    return (name != "");
}

void symbolic::make_vector_params_symbolic(std::vector<PPDDL::FullObject::Name>& vector_params)
{
    for (size_t i = 0; i < vector_params.size(); ++i) {
        vector_params[i] = DEFAULT_PARAM_NAMES[i];
    }
}

void symbolic::make_vector_params_symbolic(std::vector<PPDDL::FullObject>& vector_params)
{
    for (size_t i = 0; i < vector_params.size(); ++i) {
        vector_params[i].name = DEFAULT_PARAM_NAMES[i];
    }
}

int symbolic::get_symbolic_param_index(const PPDDL::FullObject::Name& param) {
    for (size_t i = 0; i < DEFAULT_PARAM_NAMES_NUMBER; i++) {
        if (param == DEFAULT_PARAM_NAMES[i]) {
            return i;
        }
    }
    return -1;
}

NonTypedAtom symbolic::transform_to_nontyped(const TypedAtom& typed_atom) {
    return NonTypedAtom(typed_atom.name, typed_atom.get_param_names());
}


template<>
typename PPDDL::FullObject::Name FullAtom<PPDDL::FullObject>::get_no_action_name() {
    return ACTION_NAME_NOACTION;
}

template<>
typename PPDDL::IndexedObject::Name FullAtom<PPDDL::IndexedObject>::get_no_action_name() {
    return 0;
}

template<>
void TypedAtom::get_typed_params_from_ppddl_parameters_string(const std::string &ppddl_parameters)
{
    std::stringstream ss;
    std::string item;
    ss.str(ppddl_parameters);
    this->params.clear();
    
    // get elements between ( )
    std::vector<std::string> elements;
    char separator = ' ';
    while (std::getline(ss, item, separator)) {
        // remove spaces and empty strings
        item.erase(remove_if(item.begin(), item.end(), isspace), item.end());
        if (item.compare("") != 0) {
            elements.push_back(item);
            //std::cout << "Param names: " << item << std::endl;
        }
    }
    
    for(size_t i = 0; i + 2 < elements.size(); i=i+3) {
        // elements[i+1] == '-'
        params.push_back(ParamType(elements[i], elements[i+2]));
    }
}


NonTypedAtom NonTypedAtom::get_atom_from_grounded_name_item(const std::string& grounded_atom_str)
{
    size_t pos = grounded_atom_str.find(MAKE_GROUNDED_NAME_ATOM_SEPARATOR);
    NonTypedAtom parsed_atom(grounded_atom_str.substr(0, pos), 
                             std::vector<std::string>());
    
    if (grounded_atom_str.size() > pos) {
        pos+=2;
        size_t prev = pos;
        
        while ((pos = grounded_atom_str.find(MAKE_GROUNDED_NAME_ATOM_SEPARATOR, prev)) != std::string::npos)
        {
            parsed_atom.params.push_back(ParamType(grounded_atom_str.substr(prev, pos - prev),DEFAULT_OBJECT_TYPE));
            prev = pos + 1;
        }
        parsed_atom.params.push_back(ParamType(grounded_atom_str.substr(prev),DEFAULT_OBJECT_TYPE));
    }
    
    return parsed_atom;
}


template<>
bool IndexedAtom::is_special() const
{
    return false;
}

template<>
bool IndexedAtom::is_plannable() const
{
    return false;
}

template<>
bool IndexedAtom::is_executable() const
{
    return false;
}

/**
 * TYPED ATOM
 */
template<>
void TypedAtom::load_typed_param_names_from_string(const std::string &line)
{
    if (line.find('(') == std::string::npos) { // doesn't have params
        return;
    }
    
    std::stringstream ss(line);
    std::string item;

    // get string between ()
    std::getline(ss, item, '(');
    std::getline(ss, item, ')');
    ss.str(item);
    
    if ( (item.find('?') == std::string::npos) && (item.find(" - ") == std::string::npos) ) {
        std::vector<std::string> param_names = get_param_names_from_nid_parameters_string(item);
//         LOG(ERROR) << "Unsupported non typed params!";
//         assert(param_names.empty() && "Unsupported non typed params!");
        BOOST_FOREACH(const std::string& param_name, param_names) {
            params.push_back(ParamType(param_name, DEFAULT_OBJECT_TYPE));
        }
//         for (size_t i = 0; i < param_names.size(); ++i) {
//             param_types.push_back(DEFAULT_OBJECT_TYPE);
//         }
//         // HACK log once
        static bool hacky_bool = true;
        if (!param_names.empty()) {
            LOG_IF(hacky_bool,WARNING) << "NID compatibility, DEPRECATED. Reading: " << line;
        }
        hacky_bool = false;
    }
    else {
        get_typed_params_from_ppddl_parameters_string(item);
    }
}

template<>
TypedAtom::BaseTypedAtom(const std::string &line)
{
    name = load_atom_name_from_string(line);
    load_typed_param_names_from_string(line);
}

template<>
void TypedAtom::PPDDL_grounding()
{
    if (params.size() > 0) // first parameter has two _
        name = name + "_";
    for (std::vector<ParamType>::iterator param_it = params.begin(); param_it != params.end(); ++param_it) {
        name = name + "_" + param_it->name;
    }
    params.clear();
}

template<>
TypedAtom PPDDLObjectManager<PPDDL::FullObject>::get_typed_atom(const FullAtom<PPDDL::FullObject>& atom) const
{
    std::vector<PPDDL::FullObject> typed_params;
    BOOST_FOREACH(const std::string& param_name, atom.get_param_names()) {
        typed_params.push_back(PPDDL::FullObject(param_name, get_object_type(param_name)));
    }
    return TypedAtom(atom.name, typed_params);
}

bool NonTypedAtom::is_special() const
{
    bool result = ((*this == MAKE_NONTYPED_ATOM_DEAD) ||
                   (*this == MAKE_NONTYPED_ATOM_RETRY) ||
                   (*this == MAKE_NONTYPED_ATOM_HELP) ||
                   (*this == MAKE_NONTYPED_ATOM_DISABLE) ||
                   (*this == MAKE_NONTYPED_ATOM_START_TEACHER) ||
                   (*this == MAKE_NONTYPED_ATOM_STOP_TEACHER) ||
                   (*this == MAKE_NONTYPED_ATOM_UNKNOWN) ||
                   (*this == MAKE_NONTYPED_ATOM_FINISH_ACTION) ||
                   (*this == MAKE_NONTYPED_ATOM_TEACHER) ||
                   (*this == MAKE_NONTYPED_ATOM_SET_LITERAL_DEFAULT) ||
                   (*this == MAKE_NONTYPED_ATOM_FINISHED) ||
                   (*this == MAKE_NONTYPED_ATOM_NOACTION) );
    return result;
}

template<>
bool TypedAtom::is_special() const
{
    bool result = ((*this == MAKE_TYPED_ATOM_DEAD) || 
                   (*this == MAKE_TYPED_ATOM_RETRY) || 
                   (*this == MAKE_TYPED_ATOM_HELP) ||
                   (*this == MAKE_TYPED_ATOM_DISABLE) ||
                   (*this == MAKE_TYPED_ATOM_START_TEACHER) ||
                   (*this == MAKE_TYPED_ATOM_STOP_TEACHER) ||
                   (*this == MAKE_TYPED_ATOM_UNKNOWN) || 
                   (*this == MAKE_TYPED_ATOM_FINISH_ACTION) ||
                   (*this == MAKE_TYPED_ATOM_TEACHER) ||
                   (*this == MAKE_TYPED_ATOM_SET_LITERAL_DEFAULT) ||
                   (*this == MAKE_TYPED_ATOM_FINISHED) ||
                   (*this == MAKE_TYPED_ATOM_NOACTION) );
    return result;
}

bool NonTypedAtom::is_plannable() const
{
    bool result = ((*this == MAKE_NONTYPED_ATOM_DEAD) ||
                   (*this == MAKE_NONTYPED_ATOM_RETRY) ||
                   (*this == MAKE_NONTYPED_ATOM_HELP) ||
                   (*this == MAKE_NONTYPED_ATOM_DISABLE) ||
                   (*this == MAKE_NONTYPED_ATOM_START_TEACHER) ||
                   (*this == MAKE_NONTYPED_ATOM_STOP_TEACHER) ||
                   (*this == MAKE_NONTYPED_ATOM_UNKNOWN) ||
                   (*this == MAKE_NONTYPED_ATOM_SET_LITERAL_DEFAULT) ||
                   (*this == MAKE_NONTYPED_ATOM_FINISHED) ||
                   (*this == MAKE_NONTYPED_ATOM_NOACTION) );
    return !result;
}

template<>
bool TypedAtom::is_plannable() const
{
    bool result = ((*this == MAKE_TYPED_ATOM_DEAD) || 
                   (*this == MAKE_TYPED_ATOM_RETRY) || 
                   (*this == MAKE_TYPED_ATOM_HELP) ||
                   (*this == MAKE_TYPED_ATOM_DISABLE) ||
                   (*this == MAKE_TYPED_ATOM_START_TEACHER) ||
                   (*this == MAKE_TYPED_ATOM_STOP_TEACHER) ||
                   (*this == MAKE_TYPED_ATOM_UNKNOWN) ||
                   (*this == MAKE_TYPED_ATOM_SET_LITERAL_DEFAULT) ||
                   (*this == MAKE_TYPED_ATOM_FINISHED) ||
                   (*this == MAKE_TYPED_ATOM_NOACTION) );
    return !result;
}

bool NonTypedAtom::is_executable() const
{
    bool result = ((*this == MAKE_NONTYPED_ATOM_DEAD) ||
                   (*this == MAKE_NONTYPED_ATOM_RETRY) ||
                   (*this == MAKE_NONTYPED_ATOM_HELP) ||
                   (*this == MAKE_NONTYPED_ATOM_DISABLE) ||
                   (*this == MAKE_NONTYPED_ATOM_START_TEACHER) ||
                   (*this == MAKE_NONTYPED_ATOM_STOP_TEACHER) ||
                   (*this == MAKE_NONTYPED_ATOM_UNKNOWN) ||
                   (*this == MAKE_NONTYPED_ATOM_TEACHER) ||
                   (*this == MAKE_NONTYPED_ATOM_SET_LITERAL_DEFAULT) ||
                   (*this == MAKE_NONTYPED_ATOM_FINISHED) ||
                   (*this == MAKE_NONTYPED_ATOM_NOACTION) );
    return !result;
}

template<>
bool TypedAtom::is_executable() const
{
    bool result = ((*this == MAKE_TYPED_ATOM_DEAD) || 
                   (*this == MAKE_TYPED_ATOM_RETRY) || 
                   (*this == MAKE_TYPED_ATOM_HELP) ||
                   (*this == MAKE_TYPED_ATOM_DISABLE) ||
                   (*this == MAKE_TYPED_ATOM_START_TEACHER) ||
                   (*this == MAKE_TYPED_ATOM_STOP_TEACHER) ||
                   (*this == MAKE_TYPED_ATOM_UNKNOWN) ||
                   (*this == MAKE_TYPED_ATOM_TEACHER) ||
                   (*this == MAKE_TYPED_ATOM_SET_LITERAL_DEFAULT) ||
                   (*this == MAKE_TYPED_ATOM_FINISHED) ||
                   (*this == MAKE_TYPED_ATOM_NOACTION) );
    return !result;
}