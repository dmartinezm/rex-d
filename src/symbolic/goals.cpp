#include "goals.h"
#include <parsers/rddl_reward_parser.h>

/**
 * Operators
 */

bool operator==(const Goal &left, const Goal &right)
{
    return (left.reward == right.reward) && (left.goal_predicates == right.goal_predicates);
}

bool operator==(const RDDLGoal &left, const RDDLGoal &right)
{
    // HACK to make code simpler (although less efficient)
    std::stringstream ss_left, ss_right;
    left.write_RDDL(ss_left);
    right.write_RDDL(ss_right);
    return (ss_left.str() == ss_right.str()) ? true : false;
}

bool Goal::is_equal(const RewardFunction& other) const
{
    return *this == *static_cast<const Goal*>(&other);
}

bool RDDLGoal::is_equal(const RewardFunction& other) const
{
    return *this == *static_cast<const RDDLGoal*>(&other);
}

bool operator==(const RewardFunctionGroup &left, const RewardFunctionGroup &right)
{
    std::vector<RewardFunction::Ptr>::const_iterator left_it = left.begin();
    std::vector<RewardFunction::Ptr>::const_iterator right_it = right.begin();
    while ( (left_it != left.end()) && (right_it != right.end()) ) {
        if ((*left_it)->is_equal(**right_it)) {
            return true;
        }
        ++left_it;
        ++right_it;
    }
    return (left_it == left.end()) && (right_it == right.end());
}

bool operator!=(const RewardFunctionGroup &left, const RewardFunctionGroup &right)
{
    return !(left == right);
}

/***
 * Goal
 */

Goal::Goal(const std::string& goal_string):
    reward(0)
{
    std::string ss_item;
    std::stringstream goal_ss(goal_string);
    std::getline(goal_ss, ss_item, ' ');
    try {
        reward = boost::lexical_cast<float>(ss_item);
        std::getline(goal_ss, ss_item);
        if (goal_ss.str() == ss_item) { // if no predicates getline does not obtain anything new
            ss_item = "";
        }
        add_predicates(PredicateGroup<TypedAtom>(ss_item));
    } catch (boost::bad_lexical_cast &) {
        LOG(ERROR) << "Error parsing goal " << goal_string;
        LOG(WARNING) << "Compatibility mode, setting reward to 1.0 and goal predicates to " << goal_string;
        reward = 1.0;
        add_predicates(PredicateGroup<TypedAtom>(goal_string));
    }
}

float Goal::obtain_reward(const State& state, const TypedAtom& action) const
{
    if (goal_predicates.is_satisfied_by(state)) {
        return reward;
    }
    else {
        return 0.0;
    }
}

void Goal::change_param_name(const std::string& orig_var, const std::string& new_var)
{
    this->goal_predicates.change_param_name(orig_var, new_var);
}

PredicateGroup<TypedAtom> Goal::get_predicates_that_contribute_to_goal() const
{
    if (this->reward > 0) {
        return goal_predicates;
    }
    else if (this->reward < 0) {
        return goal_predicates.get_negated();
    }
    else {
        return PredicateGroup<TypedAtom>();
    }
}

RewardFunctionGroup Goal::get_pddl_grounded_goals(const std::vector<std::string>& symbolic_params, 
                                                   const std::vector<std::string>& grounded_params, 
                                                   const PPDDLObjectManager<PPDDL::FullObject>& object_manager) const
{
    RewardFunctionGroup result_goals;
    
    Goal copy_goal(*this);
    copy_goal.goal_predicates.change_param_names(symbolic_params, grounded_params);
    
    // get symbolic params with their corresponding type
    PPDDLObjectManager<PPDDL::FullObject>::ObjectTypeMap symbolic_object_type_params;
    BOOST_FOREACH(const Predicate<TypedAtom>& goal_pred, copy_goal.goal_predicates) {
        BOOST_FOREACH(const PPDDL::FullObject& param, goal_pred.params) {
            if ( symbolic::is_param_symbolic(param.name) ) {
                symbolic_object_type_params[param.name] = param.type;
            }
        }
    }
    result_goals.add_reward_function(copy_goal);
    
    // create all groundings for each param based on its types
    BOOST_FOREACH(const PPDDLObjectManager<PPDDL::FullObject>::ObjectTypeMap::value_type& object_type_param, symbolic_object_type_params) {
        RewardFunctionGroup new_goals;
        BOOST_FOREACH(const RewardFunction::Ptr& result_goal, result_goals) {
            BOOST_FOREACH(const std::string& object, object_manager.get_type_objects(object_type_param.second)) {
                Goal new_goal(*static_cast<const Goal*>(&(*result_goal)));
                new_goal.goal_predicates.change_param_name(object_type_param.first, object);
                new_goals.add_reward_function(new_goal);
            }
        }
        result_goals = new_goals;
    }
    
//     LOG(INFO) << "Grounded goals are " << result_goals;
    return result_goals;
}

/**
 * RDDLGoal
 */

float RDDLGoal::obtain_reward(const State& state, const TypedAtom& action) const { return goal_component_ptr_->get_reward(state, action); };

bool RDDLGoal::is_goal(const State& state) const { return obtain_reward(state, MAKE_TYPED_ATOM_NOOP_ACTION) > 0.0; };

PredicateGroup<TypedAtom> RDDLGoal::get_predicates() const { return goal_component_ptr_->get_predicates(); };

void RDDLGoal::change_param_name(const std::string& orig_var, const std::string& new_var) { goal_component_ptr_->change_param_name(orig_var, new_var); };

PredicateGroup<TypedAtom> RDDLGoal::get_predicates_that_contribute_to_goal() const { return goal_component_ptr_->get_predicates_that_contribute_to_goal(); };

void RDDLGoal::write_PPDDL_problem(std::ostream &out) const
{
    out << "  (:reward ";
    goal_component_ptr_->write_RDDL(out);
    out << ")" << std::endl;
}

void RDDLGoal::write_PPDDL(std::ostream &out, const std::vector<PPDDL::FullObject>& rule_params) const
{
    throw std::logic_error("Unimplemented RDDLGoal::write_PPDDL");
}

void RDDLGoal::PPDDL_grounding() { throw std::logic_error("Unimplemented RDDLGoal::PPDDL_grounding"); };

void RDDLGoal::modify_reward(const float modification) { throw std::logic_error("Unimplemented RDDLGoal::modify_reward (but can be implemented)"); };

RewardFunctionGroup RDDLGoal::get_pddl_grounded_goals(const std::vector<std::string>& symbolic_params, 
                                                      const std::vector<std::string>& grounded_params, 
                                                      const PPDDLObjectManager<PPDDL::FullObject>& object_manager) const 
{ throw std::logic_error("Unimplemented RDDLGoal::get_pddl_grounded_goals"); return RewardFunctionGroup(); };
                                                      
void RDDLGoal::add_simple_reward(const PredicateGroup<TypedAtom>& preconditions, const float reward_value) {
    RDDLGoalComponent::Ptr reward_value_component_ptr(new RDDLGoalNumericLeaf(reward_value));
    RDDLGoalComponent::Ptr zero_value_component_ptr(new RDDLGoalNumericLeaf(0));
    
    RDDLGoalComponent::Ptr preconditions_component_ptr = rddl_goal_utils::get_simple_preconditions(preconditions);
    
    RDDLGoalComponent::Ptr if_preconditions_component_ptr(new RDDLGoalCompositeIfOperator(preconditions_component_ptr, 
                                                                                          reward_value_component_ptr, 
                                                                                          zero_value_component_ptr));
    
    std::vector<RDDLGoalComponent::Ptr> goal_components;
    goal_components.push_back(if_preconditions_component_ptr);
    goal_components.push_back(goal_component_ptr_->make_shared());
    
    this->goal_component_ptr_ = RDDLGoalCompositeListOperator::Ptr(new RDDLGoalCompositeListOperator(RDDLGoalCompositeListOperator::ADD, goal_components));
}


/*
 * RewardFunctionGroup methods
 */

RewardFunctionGroup::RewardFunctionGroup(const RewardFunctionGroup& other)
{ 
    BOOST_FOREACH(const RewardFunction::Ptr& reward_function_ptr, other.reward_function_v) {
        reward_function_v.push_back(reward_function_ptr->make_shared());
    }
}
    
float RewardFunctionGroup::obtain_reward(const State& state, const TypedAtom& action) const
{
    float obtained_reward = 0.0;
    BOOST_FOREACH(const RewardFunction::Ptr& reward_function_ptr, reward_function_v) {
        obtained_reward += reward_function_ptr->obtain_reward(state, action);
    }
    return obtained_reward;
}

bool RewardFunctionGroup::is_goal(const State& state) const
{
    if (empty()) { // no goal
        return false;
    }
    bool result = true;
    if (!state.has_predicate(Predicate<TypedAtom>(MAKE_TYPED_ATOM_FINISHED, true))) { // if not finished
        for (RewardFunctionGroup::const_iterator goal_it = begin(); goal_it != end(); ++goal_it) { // check if all goals are satisfied
            if (!(*goal_it)->is_goal(state)) {
                result = false;
            }
        }
    }
    return result;
}

PredicateGroup<TypedAtom> RewardFunctionGroup::get_predicates() const
{
    PredicateGroup<TypedAtom> predicates;
    BOOST_FOREACH(const RewardFunction::Ptr& reward_function_ptr, reward_function_v) {
        predicates.add_predicates(reward_function_ptr->get_predicates());
    }
    return predicates;
}

PredicateGroup<TypedAtom> RewardFunctionGroup::get_predicates_that_contribute_to_goal() const
{
    PredicateGroup<TypedAtom> predicates;
    BOOST_FOREACH(const RewardFunction::Ptr& reward_function_ptr, reward_function_v) {
        predicates.add_predicates(reward_function_ptr->get_predicates_that_contribute_to_goal());
    }
    return predicates;
}

void RewardFunctionGroup::change_param_name(const std::string& orig_var, const std::string& new_var)
{
    BOOST_FOREACH(const RewardFunction::Ptr& reward_function_ptr, reward_function_v) {
        reward_function_ptr->change_param_name(orig_var, new_var);
    }
}

void RewardFunctionGroup::PPDDL_grounding()
{
    BOOST_FOREACH(const RewardFunction::Ptr& reward_function_ptr, reward_function_v) {
        reward_function_ptr->PPDDL_grounding();
    }
}

RewardFunctionGroup RewardFunctionGroup::get_pddl_grounded_goals(const std::vector<std::string>& symbolic_params, 
                                                                 const std::vector<std::string>& grounded_params, 
                                                                 const PPDDLObjectManager<PPDDL::FullObject>& pddl_object_manager) const
{
    RewardFunctionGroup result;
    BOOST_FOREACH(const RewardFunction::Ptr& reward_function_ptr, reward_function_v) {
        RewardFunctionGroup new_grounded_goals = reward_function_ptr->get_pddl_grounded_goals(symbolic_params, grounded_params, pddl_object_manager);
        result.add_reward_functions(new_grounded_goals);
    }
    return result;
}

/*
 * Write PPDDL
 */

void Goal::write_PPDDL_problem(std::ostream &out) const
{
    out << "  (:goal ";
    Predicates::transform_to_nontyped(goal_predicates).write_PPDDL(out, std::vector<PPDDL::FullObject>());
    out << ")" << std::endl;
    // goal reward
    out << "  (:goal-reward " << reward << ")" << std::endl;
}

void Goal::write_PPDDL(std::ostream &out, const std::vector<PPDDL::FullObject>& rule_params) const
{
    if (!goal_predicates.empty()) {
        out << "                (when  ";
        goal_predicates.write_PPDDL(out, rule_params);
    }
    out << " (";
    if (reward >= 0)
        out << "increase ";
    else
        out << "decrease ";
    out << "(reward) ";
    out << std::abs(reward);
    out << ")";
    if ( !goal_predicates.empty() ) {
        out << ")";
    }
    out << std::endl;
}

void RewardFunctionGroup::write_PPDDL(std::ostream& out, const std::vector< PPDDL::FullObject >& rule_params) const
{
    BOOST_FOREACH(const RewardFunction::Ptr& reward_function_ptr, reward_function_v) {
        reward_function_ptr->write_PPDDL(out, rule_params);
    }
}

void RewardFunctionGroup::write_PPDDL_problem(std::ostream& out) const
{
    BOOST_FOREACH(const RewardFunction::Ptr& reward_function_ptr, reward_function_v) {
        reward_function_ptr->write_PPDDL_problem(out);
    }
}

void RewardFunctionGroup::write_RDDL(std::ostream& out) const
{
    BOOST_FOREACH(const RewardFunction::Ptr& reward_function_ptr, reward_function_v) {
        reward_function_ptr->write_RDDL(out);
    }
}
