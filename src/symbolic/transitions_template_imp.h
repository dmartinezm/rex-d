#include "transitions.h"

#ifndef REXD_TRANSITIONS_TEMPLATE_IMPL_H
#define REXD_TRANSITIONS_TEMPLATE_IMPL_H 1

#define HACK_FOR_BUGGY_LFIT 1

template<typename AtomT>
bool operator<(const Transition<AtomT> &l, const Transition<AtomT> &r)
{
    if (l.action < r.action)
        return true;
    else if (r.action < l.action)
        return false;
    else if (l.prev_state < r.prev_state)
        return true;
    else if (r.prev_state < l.prev_state)
        return false;
    else if (l.next_state < r.next_state)
        return true;
    else if (r.next_state < l.next_state)
        return false;
    else if (l.reward < r.reward)
        return true;
    else if (l.reward < r.reward)
        return false;
    else
        return false;
}

/**
 * Transition
 */
template<typename AtomT>
Transition<AtomT>::Transition(const PredicateGroup<AtomT>& prev_state, 
                              const AtomT& action, 
                              const PredicateGroup<AtomT>& next_state, 
                              const float& reward, 
                              const bool& variable_maintains_to_next_state) :
    prev_state(prev_state),
    next_state(next_state),
    action(action),
    reward(reward),
    number_repetitions(1)
{
    update_changes(variable_maintains_to_next_state);
}

template<typename AtomT>
Transition<AtomT>::Transition(const PredicateGroup<AtomT>& prev_state, 
                              const AtomT& action, 
                              const PredicateGroup<AtomT>& next_state, 
                              const float& reward, 
                              const std::set<AtomT>& default_false_literals) :
    prev_state(prev_state),
    next_state(next_state),
    action(action),
    reward(reward),
    number_repetitions(1)
{
    update_changes(default_false_literals);
}

template<typename AtomT>
void Transition<AtomT>::remove_negated_predicates()
{
    prev_state.remove_negated_predicates();
    next_state.remove_negated_predicates();
}

template<typename AtomT>
bool Transition<AtomT>::nothing_changed() const
{
    return prev_state == next_state;
}

template<typename AtomT>
void Transition<AtomT>::update_changes(const bool& variable_maintains_to_next_state)
{
    // closed world assumption
    BOOST_FOREACH(Predicate<AtomT> prev_state_atom, prev_state) {
        prev_state_atom.positive = true;
        if (!next_state.has_grounded_predicate(prev_state_atom)) {
            prev_state_atom.positive = false;
            next_state.update_predicate(prev_state_atom);
        }
    }
    BOOST_FOREACH(Predicate<AtomT> next_state_atom, next_state) {
        next_state_atom.positive = true;
        if (!prev_state.has_grounded_predicate(next_state_atom)) {
            next_state_atom.positive = false;
            prev_state.update_predicate(next_state_atom);
        }
    }
    
    if (variable_maintains_to_next_state) {
        changes = prev_state.get_differences(next_state);
    }
    else {
        changes.clear();
        BOOST_FOREACH(const Predicate<AtomT>& next_state_pred, next_state) {
            if ( next_state_pred.positive && !next_state_pred.is_constant() ) {
                changes.insert(next_state_pred);
            }
        }
    }
}

template<typename AtomT>
void Transition<AtomT>::update_changes(const std::set<AtomT>& default_false_literals)
{
    // closed world assumption
    BOOST_FOREACH(Predicate<AtomT> prev_state_atom, prev_state) {
        prev_state_atom.positive = true;
        if (!next_state.has_grounded_predicate(prev_state_atom)) {
            prev_state_atom.positive = false;
            next_state.update_predicate(prev_state_atom);
        }
    }
    BOOST_FOREACH(Predicate<AtomT> next_state_atom, next_state) {
        next_state_atom.positive = true;
        if (!prev_state.has_grounded_predicate(next_state_atom)) {
            next_state_atom.positive = false;
            prev_state.update_predicate(next_state_atom);
        }
    }
    
    changes.clear();
    BOOST_FOREACH(const Predicate<AtomT>& next_state_atom, next_state) {
        bool maintains = true;
        BOOST_FOREACH(const AtomT& default_false_literal, default_false_literals) {
            if (default_false_literal.symbolic_compare(next_state_atom)) { maintains = false; }
        }
        if (!maintains) { // not maintains
            if (next_state_atom.positive) {
                changes.insert(next_state_atom);
            }
        }
        else { // maintains
            if (!prev_state.has_grounded_predicate(next_state_atom)) {
                changes.insert(next_state_atom);
            }
        }
    }
}


/**
 * TransitionGroup
 */
template<typename AtomT>
TransitionGroup<AtomT>::TransitionGroup(const TransitionGroup<AtomT>& other):
    ppddl_object_manager(other.ppddl_object_manager) 
{
    add_transitions(other);
}

template<typename AtomT>
TransitionGroup<AtomT>& TransitionGroup<AtomT>::operator=(const TransitionGroup<AtomT>& other)
{
    if (this != &other) { // self-assignment check expected
        transition_set.clear();
        transition_historial.clear();
        ppddl_object_manager = other.ppddl_object_manager;
        add_transitions(other);
    }
    return *this;
}

#if __cplusplus > 199711L // C++11
template<typename AtomT>
TransitionGroup<AtomT>::TransitionGroup(TransitionGroup<AtomT>&& other):
    ppddl_object_manager(std::move(other.ppddl_object_manager))
{
    transition_set.swap(other.transition_set);
    transition_historial.swap(other.transition_historial);
}

template<typename AtomT>
TransitionGroup<AtomT>& TransitionGroup<AtomT>::operator=(TransitionGroup<AtomT>&& other)
{
    transition_set.swap(other.transition_set);
    transition_historial.swap(other.transition_historial);
    ppddl_object_manager = std::move(other.ppddl_object_manager);
    return *this;
}
#endif

template<typename AtomT>
TransitionGroup<AtomT> TransitionGroup<AtomT>::get_action_transitions(const AtomT& action) const
{
    TransitionGroup<AtomT> important_transitions(ppddl_object_manager);
    for(typename TransitionHistorial<AtomT>::const_iterator transition_it_it = begin_historial(); transition_it_it != end_historial(); ++transition_it_it) {
        if ((*transition_it_it)->get_action().symbolic_compare(action) || (action == MAKE_TYPED_ATOM_NOACTION)) {
            important_transitions.add_transition(**transition_it_it);
        }
    }

    return important_transitions;
}

template<typename AtomT>
void TransitionGroup<AtomT>::add_transition(const Transition<AtomT>& transition, const uint repetitions)
{
    // Add new objects (if there are any new)
    std::vector<typename AtomT::ParamType> transition_objects = transition.get_prev_state().get_params();
    ppddl_object_manager.add_objects(transition_objects.begin(), transition_objects.end());
    transition_objects = transition.get_post_state().get_params();
    ppddl_object_manager.add_objects(transition_objects.begin(), transition_objects.end());
    
    // Add transition
    Transition<AtomT> transition_copy(transition);
    transition_copy.set_number_repetitions(repetitions);
    std::pair<typename TransitionSet<AtomT>::iterator, bool> pair_setit_bool;
    pair_setit_bool = transition_set.insert(transition_copy);
    if (!pair_setit_bool.second) { // transition already existed
        pair_setit_bool.first->increase_number_repetitions(repetitions);
    }
    for (uint i = 0; i < repetitions; ++i) {
        transition_historial.push_back(pair_setit_bool.first);
    }
}

template<typename AtomT>
void TransitionGroup<AtomT>::add_transitions(const TransitionGroup<AtomT>& transitions)
{
    // We want to maintain the historial
    for(typename TransitionHistorial<AtomT>::const_iterator transition_it_it = transitions.begin_historial(); transition_it_it != transitions.end_historial(); ++transition_it_it) {
        add_transition(**transition_it_it);
    }
}

template<typename AtomT>
const Transition<AtomT>& TransitionGroup<AtomT>::get_transition_from_set(const int index) const
{
    typename TransitionSet<AtomT>::iterator transition_it = transition_set.begin();
    std::advance(transition_it, index);
    return *transition_it;
}

template<typename AtomT>
void TransitionGroup<AtomT>::remove_negated_predicates()
{
    TransitionSet<AtomT> original_transition_set;
    original_transition_set.swap(transition_set);
    TransitionHistorial<AtomT> original_transition_historial;
    original_transition_historial.swap(transition_historial);
    for (typename TransitionHistorial<AtomT>::const_iterator transition_it_it = original_transition_historial.begin(); 
         transition_it_it != original_transition_historial.end(); 
         ++transition_it_it) 
    {
        Transition<AtomT> new_transition(**transition_it_it);
        new_transition.remove_negated_predicates();
        add_transition(new_transition);
    }
}

#endif

