/*
 * Template implementations for predicates.h.
 * Copyright (C) 2014  David Martínez (dmartinez@iri.upc.edu)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef IRI_RULE_LEARNER_PREDICATES_TEMPLATE_IMPL_H
#define IRI_RULE_LEARNER_PREDICATES_TEMPLATE_IMPL_H 1

#include <symbolic/predicates.h>
#include "symbolic/utils.h"

// Operators
template<typename AtomT>
bool operator==(const Predicate<AtomT> &p1, const Predicate<AtomT> &p2)
{
    return ((*static_cast<const AtomT*>(&p1) == p2) && (p1.positive == p2.positive));
}

template<typename AtomT>
bool Predicate<AtomT>::symbolic_compare(const Predicate<AtomT>& p) const
{
    return ((static_cast<const AtomT*>(&*this)->symbolic_compare(p)) && (positive == p.positive));
}

template<typename AtomT>
bool operator==(const PredicateGroup<AtomT> &p1, const PredicateGroup<AtomT> &p2)
{
    return p1.internal_vector == p2.internal_vector;
    // check all p \in p1 are also \in p2
    // remove preds from p2 after finding them, so p2 has to be empty in the end
    bool result = true;
    PredicateGroup<AtomT> p2_copy(p2);
    for (typename PredicateGroup<AtomT>::const_iterator it_pred = p1.begin() ; it_pred != p1.end(); it_pred++) {
        bool present = false;
        for (typename PredicateGroup<AtomT>::iterator it_pred2 = p2_copy.begin() ; it_pred2 != p2_copy.end();) {
            if (*it_pred == *it_pred2) {
                present = true;
                p2_copy.erase(it_pred2++);
            } else {
                ++it_pred2;
            }
        }
        if (!present) {
            result = false;
            break;
        }
    }
    if (p2_copy.size() != 0) { // all predicates should have been removed
        result = false;
    }
    return result;
}

template<typename AtomT>
std::ostream& operator<<(std::ostream &out, const Predicate<AtomT>& p)
{
    if (not p.positive)
        out << "-";
    out << *static_cast<const AtomT*>(&p);
    return out;
}

template<typename AtomT>
std::ostream& operator<<(std::ostream &out, const PredicateGroup<AtomT>& p)
{
    for (typename PredicateGroup<AtomT>::iterator p_it = p.begin(); p_it != p.end(); ) {
        out << *p_it;
        if ((++p_it) != p.end()) {
            out << " ";
        }
    }
    return out;
}

template<typename AtomT>
bool operator<(const Predicate<AtomT>& l, const Predicate<AtomT>& r)
{
    if (*static_cast<const AtomT*>(&l) < *static_cast<const AtomT*>(&r))
        return true;
    else if (*static_cast<const AtomT*>(&r) < *static_cast<const AtomT*>(&l))
        return false;
    else if (l.positive > r.positive) // want positive first!
        return true;
    else
        return false;
}

template<typename AtomT>
bool operator<(const PredicateGroup<AtomT>& l, const PredicateGroup<AtomT>& r)
{
    return l.internal_vector < r.internal_vector;
}

template<typename AtomT>
void Predicate<AtomT>::write_PPDDL(std::ostream &out) const
{
    if (!positive) {
        out << "(not ";
    }
    (static_cast<const AtomT*>(this))->write_PPDDL(out);
    if (!positive) {
        out << ")";
    }
}

template<typename AtomT>
void Predicate<AtomT>::write_RDDL(std::ostream &out, const bool future_state) const
{
    if (!positive) {
        out << "(~ ";
    }
    (static_cast<const AtomT*>(this))->write_RDDL(out, future_state);
    if (!positive) {
        out << ")";
    }
}

template<typename AtomT>
void Predicate<AtomT>::write_LFIT(std::ostream &out) const
{
    out << (static_cast<const AtomT*>(this))->name;
    out << "=";
    if (positive) {
        out << "1";
    }
    else {
        out << "0";
    }
}

template<typename AtomT>
void PredicateGroup<AtomT>::write_LFIT(std::ostream &out) const
{
    // Maximun of 15 predicates per line (to not exceed character limit in editors)
    uint number_predicates = 0;
    for (typename PredicateGroup<AtomT>::const_iterator it_pred = this->begin() ; it_pred != this->end(); ++it_pred) {
        it_pred->write_LFIT(out);
        out << " ";
        if (number_predicates++ > 20) { // max predicates = 20 (performance issues)
            LOG_EVERY_N(1000,WARNING) << "More than 20 predicates! Check if it is safe with LFIT??";
        }
    }
}

template<typename AtomT>
void PredicateGroup<AtomT>::write_negated_PPDDL(std::ostream &out) const
{
    // Maximun of 15 predicates per line (to not exceed character limit in editors)
    uint number_predicates = 0;
    out << "(or ";
    for (typename PredicateGroup<AtomT>::const_iterator it_pred = this->begin() ; it_pred != this->end(); ++it_pred) {
        it_pred->get_negated().write_PPDDL(out);
        out << " ";
        if (number_predicates++ > 20) { // max predicates = 20 (performance issues)
            number_predicates = 0;
            out << std::endl;
        }
    }
    out << ")";
}

template<typename AtomT>
void PredicateGroup<AtomT>::write_simplified_PPDDL(std::ostream &out) const
{
    BOOST_FOREACH(const Predicate<AtomT>& predicate, *this) {
        predicate.write_PPDDL(out);
        out << " ";
    }
}

template<typename AtomT>
void PredicateGroup<AtomT>::PPDDL_grounding()
{
    std::set< Predicate<AtomT> > old_set;
    internal_vector.swap(old_set);
    BOOST_FOREACH(Predicate<AtomT> predicate, old_set) {
        predicate.PPDDL_grounding();
        internal_vector.insert(predicate);
    }
}


/**
 * ***************
 * PredicateGroup
 */

// PredicateGroup

template<typename AtomT>
PredicateGroup<AtomT>::PredicateGroup(const std::string& predicates_)
{
    // DEPRECATED
    std::string item, aux, line;
    bool positive;

    // remove leading and trailing spaces
    line = utils::trim(predicates_);

    std::stringstream ss(line);

    // case of empty string
    if ((line == "") || (line == "--")) {
        return;
    }

    while (std::getline(ss, item, ')')) {
        item = utils::trim(item);
//         std::cout << "Generating predicate for: " << item << std::endl;

        if (item.size() > 1) { // if something remains
            // add ) to the end to complete it
            item = item + ')';

            // if begins with , remove it
            aux = item.substr(0, 1);
            if (aux.compare(",") == 0)
                item = item.substr(1);

            item = utils::trim(item);
            aux = item.substr(0, 1);
            if (aux.compare("-") == 0) {
                item = item.substr(1);
                positive = false;
            } else
                positive = true;

            this->insert(Predicate<AtomT>(AtomT(item), positive));
        }
    }
}

template<typename AtomT>
void PredicateGroup<AtomT>::erase(const PredicateGroup<AtomT>& predicates_to_remove)
{
    BOOST_FOREACH(const Predicate<AtomT>& predicate, predicates_to_remove) {
        erase(predicate);
    }
}

template<typename AtomT>
bool PredicateGroup<AtomT>::satisfies(const Predicate<AtomT>& predicate_to_satisfy) const
{
    BOOST_FOREACH(const Predicate<AtomT>& pred, *this) {
        // if (*static_cast<const AtomT*>(&predicate_to_satisfy) == *static_cast<const AtomT*>(&pred)) { // slower, has to call operator==
        if ((predicate_to_satisfy.name == pred.name) && (predicate_to_satisfy.params == pred.params)) { // performance reasons, avoid copy for get_negated()
            if ( predicate_to_satisfy.positive == pred.positive ) {
                return true;
            }
            else { // assume that same predicate cannot be positive and negative at the same time
                return false;
            }
        }
//         if (predicate_to_satisfy.name < pred.name) { // hack to speed up a little
//             !predicate_to_satisfy.positive;
//         }
    }
    // negated predicates don't have to be present (closed-world assumption)
    return !predicate_to_satisfy.positive;
}

template<typename AtomT>
void PredicateGroup<AtomT>::set_constants(const PredicateGroup<AtomT>& new_constant_predicates) { 
    BOOST_FOREACH(const Predicate<AtomT>& predicate_constant, new_constant_predicates) { 
        BOOST_FOREACH(const Predicate<AtomT>& predicate, *this) { 
            if (predicate_constant == predicate) {
                predicate.set_constant();
            }
        }
    }
}

template<typename AtomT>
bool PredicateGroup<AtomT>::is_satisfied_by(const PredicateGroup<AtomT>& current_predicates) const
{
    BOOST_FOREACH(const Predicate<AtomT>& predicate_it, *this) {
        if ( !current_predicates.satisfies(predicate_it) ) {
            return false;
        }
    }
    return true;
}

template<typename AtomT>
int PredicateGroup<AtomT>::count_satisfied_by(const PredicateGroup<AtomT>& current_predicates) const
{
    int n_satisfied = 0;
    BOOST_FOREACH(const Predicate<AtomT>& predicate_it, *this) {
        if ( current_predicates.satisfies(predicate_it) ) {
            n_satisfied++;
        }
    }
    return n_satisfied;
}

template<typename AtomT>
PredicateGroup<AtomT> PredicateGroup<AtomT>::get_not_satisfied_by(const PredicateGroup<AtomT>& predicates) const
{
    PredicateGroup<AtomT> predicates_not_satisfied;
    BOOST_FOREACH(const Predicate<AtomT>& predicate, *this) {
        if ( !predicates.satisfies(predicate) ) {
            predicates_not_satisfied.insert(predicate);
        }
    }
    return predicates_not_satisfied;
}

template<typename AtomT>
bool PredicateGroup<AtomT>::has_predicate(const Predicate<AtomT>& predicate) const
{
    BOOST_FOREACH(const Predicate<AtomT>& predicate_it, *this) {
        if (predicate.is_symbolic() || predicate_it.is_symbolic()) {
            if ( has_symbolic_predicate(predicate) ) {
                return true;
            }
        }
        else {
            if ( has_grounded_predicate(predicate) ) {
                return true;
            }
        }
    }
    return false;
}

template<typename AtomT>
std::vector<typename AtomT::ParamType> PredicateGroup<AtomT>::get_params() const
{
    std::vector<typename AtomT::ParamType> result;
    std::set<typename AtomT::ParamType> variables;
    BOOST_FOREACH(const Predicate<AtomT>& predicate, *this) {
        BOOST_FOREACH(const typename AtomT::ParamType& param, predicate.params) {
            variables.insert(param);
        }
    }
    result.assign(variables.begin(), variables.end());
    return result;
}

template<typename AtomT>
PredicateGroup<AtomT> PredicateGroup<AtomT>::get_predicates_with_var(const typename AtomT::ParamType& param) const
{
    PredicateGroup<AtomT> result;
    BOOST_FOREACH(const Predicate<AtomT>& predicate, *this) {
        if (predicate.has_param(param)) {
            result.insert(predicate);
        }
    }
    return result;
}

template<typename AtomT>
bool PredicateGroup<AtomT>::has_grounded_predicate(const Predicate<AtomT>& predicate) const
{
    BOOST_FOREACH(const Predicate<AtomT>& predicate_it, *this) {
        if (predicate_it == predicate) {
            return true;
        }
    }
    return false;
}

template<typename AtomT>
bool PredicateGroup<AtomT>::has_symbolic_predicate(const Predicate<AtomT>& predicate) const
{
    BOOST_FOREACH(const Predicate<AtomT>& predicate_it, *this) {
        if (predicate_it.symbolic_compare(predicate)) {
            return true;
        }
    }
    return false;
}

template<typename AtomT>
void PredicateGroup<AtomT>::change_param_name(const typename AtomT::ParamType::Name& orig_var, const typename AtomT::ParamType::Name& new_var)
{
    std::set< Predicate<AtomT> > old_set;
    internal_vector.swap(old_set);
    BOOST_FOREACH(Predicate<AtomT> predicate, old_set) {
        predicate.change_param_name(orig_var, new_var);
        internal_vector.insert(predicate);
    }
}

template<typename AtomT>
void PredicateGroup<AtomT>::change_param(const typename AtomT::ParamType& orig_var, const typename AtomT::ParamType& new_var)
{
    std::set< Predicate<AtomT> > old_set;
    internal_vector.swap(old_set);
    BOOST_FOREACH(Predicate<AtomT> predicate, old_set) {
        predicate.change_param(orig_var, new_var);
        internal_vector.insert(predicate);
    }
}

template<typename AtomT>
void PredicateGroup<AtomT>::change_param_names(const std::vector<typename AtomT::ParamType::Name>& orig_var_v, const std::vector<typename AtomT::ParamType::Name>& new_var_v)
{
    std::set< Predicate<AtomT> > old_set;
    internal_vector.swap(old_set);
    BOOST_FOREACH(Predicate<AtomT> predicate, old_set) {
        for (size_t i = 0; i < orig_var_v.size(); ++i) {
            predicate.change_param_name(orig_var_v[i], new_var_v[i]);
        }
        internal_vector.insert(predicate);
    }
}

template<typename AtomT>
void PredicateGroup<AtomT>::change_param_names(const std::vector<typename AtomT::ParamType>& orig_var_v, const std::vector<typename AtomT::ParamType>& new_var_v)
{
    std::set< Predicate<AtomT> > old_set;
    internal_vector.swap(old_set);
    BOOST_FOREACH(Predicate<AtomT> predicate, old_set) {
        for (size_t i = 0; i < orig_var_v.size(); ++i) {
            predicate.change_param_name(orig_var_v[i].name, new_var_v[i].name);
        }
        internal_vector.insert(predicate);
    }
}

template<typename AtomT>
void PredicateGroup<AtomT>::change_params(const std::vector<typename AtomT::ParamType>& orig_var_v, const std::vector<typename AtomT::ParamType>& new_var_v)
{
    std::set< Predicate<AtomT> > old_set;
    internal_vector.swap(old_set);
    BOOST_FOREACH(Predicate<AtomT> predicate, old_set) {
        for (size_t i = 0; i < orig_var_v.size(); ++i) {
            predicate.change_param(orig_var_v[i], new_var_v[i]);
        }
        internal_vector.insert(predicate);
    }
}

template<typename AtomT>
void PredicateGroup<AtomT>::update_predicate(const Predicate<AtomT>& new_predicate)
{
    erase(new_predicate.get_negated()); // first erase negated predicate if it exists
    insert(new_predicate);
}

template<typename AtomT>
void PredicateGroup<AtomT>::update_predicates(const PredicateGroup<AtomT>& new_predicates)
{
    BOOST_FOREACH(const Predicate<AtomT> & predicate, new_predicates) {
        update_predicate(predicate);
    }
}

template<typename AtomT>
PredicateGroup<AtomT> PredicateGroup<AtomT>::get_differences(const PredicateGroup& compared_group) const {
    PredicateGroup<AtomT> differences;

    // Add negated diferences of *this - compared_group
    std::set_difference(begin(), end(),
                        compared_group.begin(), compared_group.end(),
                        std::inserter(differences, differences.end()) );
    differences = differences.get_negated();
    
    // Add diferences of compared_group - *this
    std::set_difference(compared_group.begin(), compared_group.end(),
                        begin(), end(),
                        std::inserter(differences, differences.end()) );

    return differences;
}

template<typename AtomT>
bool PredicateGroup<AtomT>::has_negated_predicates(const PredicateGroup<AtomT>& compared_predicates) const {
    BOOST_FOREACH(const Predicate<AtomT>& predicate, compared_predicates) {
        if (internal_vector.count(predicate.get_negated())) {
            return true;
        }
    }
    return false;
}

template<typename AtomT>
PredicateGroup<AtomT> PredicateGroup<AtomT>::set_and(const PredicateGroup& compared_group) const {    
    PredicateGroup<AtomT> result;
    std::set_intersection (begin(), end(), 
                           compared_group.begin(), compared_group.end(), 
                           std::inserter(result,result.begin()));
    return result;
}

template<typename AtomT>
bool PredicateGroup<AtomT>::get_logical_intersects(const PredicateGroup& pred_group) const
{
    // if (a ^ -a) -> 0    |    otherwise result true
    return !has_negated_predicates(pred_group);
}

template<typename AtomT>
bool PredicateGroup<AtomT>::includes_predicates(const PredicateGroup& pred_group) const 
{
    return std::includes(begin(), end(),
                         pred_group.begin(), pred_group.end());
}

template<typename AtomT>
void PredicateGroup<AtomT>::make_symbolic() 
{ 
    std::set< Predicate<AtomT> > old_set;
    internal_vector.swap(old_set);
    BOOST_FOREACH(Predicate<AtomT> predicate, old_set) {
        predicate.make_symbolic();
        internal_vector.insert(predicate);
    } 
}

template<typename AtomT>
bool PredicateGroup<AtomT>::is_symbolic() const
{
    bool symbolic = false;
    PredicateGroup<AtomT>::iterator it = begin();
    while ((!symbolic) && (it != end())) {
        if (it->is_symbolic()) {
            symbolic = true;
        }
        ++it;
    }
    return symbolic;
}

template<typename AtomT>
PredicateGroup<AtomT> PredicateGroup<AtomT>::get_negated() const
{
    PredicateGroup<AtomT> result;
    BOOST_FOREACH(const Predicate<AtomT>& predicate, *this) {
        result.insert(predicate.get_negated());
    }
    return result;
}

template<typename AtomT>
void PredicateGroup<AtomT>::remove_negated_predicates()
{
    for (typename PredicateGroup<AtomT>::iterator it_pred = this->begin() ; it_pred != this->end();) {
        if (!it_pred->positive) {
            this->erase(it_pred++);
        } else {
            ++it_pred;
        }
    }
}

template<typename AtomT>
void PredicateGroup<AtomT>::remove_predicates_with_params_not_in(const std::vector<typename AtomT::ParamType>& list_of_params)
{
    for (typename PredicateGroup<AtomT>::iterator it_pred = this->begin() ; it_pred != this->end();) {
        if (it_pred->params_belong_to(list_of_params)) {
            ++it_pred;
        } else {
            erase(it_pred++);
        }
    }
}

template<typename AtomT>
void PredicateGroup<AtomT>::write_to_file(const std::string file_path) const
{
    int DEBUG = 0;
    std::ofstream predicates_file(file_path.c_str());
    std::cout << "PredicateGroup::" << "write_to_file: " << "Writting predicates to file " << file_path << std::endl;
    if (DEBUG)
        std::cout << "PredicateGroup::" << "write_to_file: " << "Predicates are " << *this << std::endl << std::endl;
    if (predicates_file.is_open()) {
        predicates_file << *this;
    } else
        std::cerr << "PredicateGroup::" << "write_to_file: " << "Couldn't write predicates to " << file_path << std::endl;
    predicates_file.close();
}

/**
 * Outcomes
 */

template<typename AtomT>
bool operator==(const Outcome<AtomT> &o1, const Outcome<AtomT> &o2)
{
    return ( (*static_cast<const PredicateGroup<AtomT>*>(&o1) == o2) && (utils::is_close(o1.probability, o2.probability)) );
}

template<typename AtomT>
std::ostream& operator<<(std::ostream &out, const OutcomeGroup<AtomT>& outcome_group)
{
    BOOST_FOREACH(const Outcome<AtomT>& outcome, outcome_group) {
        out << outcome << std::endl;
    }
    return out;
}

template<typename AtomT>
bool operator<(const Outcome<AtomT>& l, const Outcome<AtomT>& r)
{
    if (l.noisy && !r.noisy) // noisy is last!
        return false;
    else if (!l.noisy && r.noisy) // noisy is last!
        return true;
    else if (l.empty() && !r.empty()) // empty is last!
        return false;
    else if (!l.empty() && r.empty()) // empty is last!
        return true;
    else if (utils::is_significantly_less(l.probability, r.probability)) // larger prob is first!
        return false;
    else if (utils::is_significantly_less(r.probability, l.probability)) // larger prob is first!
        return true;
    else if (*static_cast<const PredicateGroup<AtomT>*>(&l) < *static_cast<const PredicateGroup<AtomT>*>(&r))
        return true;
    else if (*static_cast<const PredicateGroup<AtomT>*>(&r) < *static_cast<const PredicateGroup<AtomT>*>(&l))
        return false;
    else
        return false;
}

template<typename AtomT>
bool operator<(const OutcomeGroup<AtomT>& l, const OutcomeGroup<AtomT>& r)
{
    return (*static_cast<const std::set<Outcome<AtomT> >* >(&l)) < r;
}

template<typename AtomT>
void Outcome<AtomT>::write_PPDDL(std::ostream &out, const std::vector<PPDDL::FullObject>& rule_params) const
{
    out << " " << probability << " ";
    Predicates::transform_to_nontyped(*this).write_PPDDL(out, rule_params);
}

template<typename AtomT>
void OutcomeGroup<AtomT>::write_PPDDL(std::ostream &out, const std::vector<PPDDL::FullObject>& rule_params) const
{
    out << "(probabilistic ";
    BOOST_FOREACH(Outcome<AtomT> outcome, *this) {
        if (outcome.is_useful()) {
            outcome.write_PPDDL(out, rule_params);
        }
    }
    out << ")";
}


/**
 * Outcome
 */
template<typename AtomT>
double Outcome<AtomT>::probability_obtain_predicate(const Predicate<AtomT> predicate) const
{
    if (this->has_predicate(predicate))
        return probability;
    else
        return 0.0;
}



template<typename AtomT>
bool Outcome<AtomT>::changes_predicates() const
{
    if ((this->size() > 0) && (!noisy)) {
        return true;
    } else {
        return false;
    }
}

template<typename AtomT>
bool Outcome<AtomT>::is_useful() const
{
    if (changes_predicates() && (probability > 0.0)) {
        return true;
    } else {
        return false;
    }
}

template<typename AtomT>
bool Outcome<AtomT>::apply(PredicateGroup<AtomT>& input_predicates) const
{
    if (changes_predicates()) {
        input_predicates.update_predicates(*this);
        return true;
    } else
        return false;
}

template<typename AtomT>
bool OutcomeGroup<AtomT>::changes_predicates() const
{
    bool result = false;
    for (typename OutcomeGroup<AtomT>::const_iterator outcome_it = this->begin(); outcome_it != this->end(); outcome_it++) {
        if (outcome_it->changes_predicates()) {
            result = true;
        }
    }
    return result;
}

template<typename AtomT>
void OutcomeGroup<AtomT>::add_predicate_to_outcomes(const Predicate<AtomT>& new_predicate)
{
    std::set<Outcome<AtomT> > old_set;
    this->swap(old_set);
    BOOST_FOREACH(Outcome<AtomT> outcome, old_set) {
        outcome.update_predicate(new_predicate);
        this->insert(outcome);
    }
}

template<typename AtomT>
void OutcomeGroup<AtomT>::change_param_name(const std::string& current_name, const std::string& new_name)
{
    std::set<Outcome<AtomT> > old_set;
    this->swap(old_set);
    BOOST_FOREACH(Outcome<AtomT> outcome, old_set) {
        outcome.change_param_name(current_name, new_name);
        this->insert(outcome);
    }
}

template<typename AtomT>
void OutcomeGroup<AtomT>::PPDDL_grounding()
{
    std::set<Outcome<AtomT> > old_set;
    this->swap(old_set);
    BOOST_FOREACH(Outcome<AtomT> outcome, old_set) {
        outcome.PPDDL_grounding();
        this->insert(outcome);
    }
}

template<typename AtomT>
void OutcomeGroup<AtomT>::add_new_outcomes(OutcomeGroup<AtomT>& new_outcomes)
{
    this->insert( new_outcomes.begin(), new_outcomes.end() );
}

template<typename AtomT>
void OutcomeGroup<AtomT>::add_empty_outcome_to_fill_probability()
{
    float total_probability = 0.0;
    BOOST_FOREACH(const Outcome<AtomT>& outcome, *this) {
        total_probability += outcome.probability;
    }
    if (total_probability < 1.0) {
        this->insert(Outcome<AtomT>(1.0 - total_probability));
    }
}


#endif
