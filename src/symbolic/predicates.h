/*
 * Clases representing predicates and goals.
 * Copyright (C) 2016  David Martínez (dmartinez@iri.upc.edu)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef IRI_RULE_LEARNER_PREDICATES_H
#define IRI_RULE_LEARNER_PREDICATES_H 1


#include <sys/types.h>
#include <vector>
#include <set>
#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>

#include <boost/lexical_cast.hpp>

#include <symbolic/symbols.h>


/* This is needed to declare operators as friends 
 * http://stackoverflow.com/questions/1297609/overloading-friend-operator-for-template-class
 */
template <typename AtomT> class Predicate;
template <typename AtomT>
bool operator==(const Predicate<AtomT> &p1, const Predicate<AtomT> &p2);
template <typename AtomT>
bool operator<(const Predicate<AtomT>& l, const Predicate<AtomT>& r);
template <typename AtomT>
std::ostream& operator<<(std::ostream &out, const Predicate<AtomT>& p);

template <typename AtomT> class PredicateGroup;

/*!
\class Predicate
\brief A Predicate is a Atom with a sign.
*/
template<typename AtomT>
class Predicate : public AtomT
{
private:
    mutable bool constant;
    
public:
    typedef boost::shared_ptr<Predicate<AtomT> > Ptr;
    typedef boost::shared_ptr<const Predicate<AtomT> > ConstPtr;
    typedef boost::optional<Predicate<AtomT> > Opt;
    
    bool positive;

    Predicate(const AtomT& symbol_t, const bool positive, const bool constant = false):
        AtomT(symbol_t),
        constant(constant),
        positive(positive)
    { }

    /**
     * \brief obtains predicate value as a string
     * 
     * WARNING: At this moment only bool values are supported
     */
    std::string get_value_string() const {
        if (positive)
            return "true";
        else
            return "false";
    }

    void negate() {
        positive = !positive;
    }

    Predicate get_negated() const {
        return Predicate(*this, !positive);
    }
    
    bool is_constant() const { return constant; }
    void set_constant() const { constant = true; }

    /** \brief Writes in standard PPDDL format */
    void write_PPDDL(std::ostream &out) const;
    /** \brief Writes in standard RDDL format. When defining transitions, future_state means the value for the next time step */
    void write_RDDL(std::ostream &out, const bool future_state = false) const;
    /** \brief Writes in standard LFIT format */
    void write_LFIT(std::ostream &out) const;

    /**
     * \brief compares two predicates as if they were symbolic, so parameter names are not compared
     */
    bool symbolic_compare(const Predicate& p) const;
    
    /**
     * Operator (already declared before
     */
    friend std::ostream& operator<< <>(std::ostream &out, const Predicate<AtomT>& p);
    friend bool operator== <>(const Predicate<AtomT> &p1, const Predicate<AtomT> &p2);
    friend bool operator< <>(const Predicate<AtomT>& l, const Predicate<AtomT>& r);
};



/* This is needed to declare operators as friends 
 * http://stackoverflow.com/questions/1297609/overloading-friend-operator-for-template-class
 */
template <typename AtomT>
std::ostream& operator<<(std::ostream &out, const PredicateGroup<AtomT>& p);
template <typename AtomT>
bool operator==(const PredicateGroup<AtomT> &p1, const PredicateGroup<AtomT> &p2);
template <typename AtomT>
bool operator<(const PredicateGroup<AtomT>& l, const PredicateGroup<AtomT>& r);


/*!
\class PredicateGroup
\brief A vector of Predicate.
*/
template<typename AtomT>
class PredicateGroup 
{
private:
    std::set< Predicate<AtomT> > internal_vector;
    
public:
    /** Begin of methods exposing std::vector stuff */
    typedef Predicate<AtomT> value_type;
    typedef size_t size_type;
    typedef size_t index_type;
    // iterators
    typedef typename std::set< value_type >::iterator iterator;
    typedef typename std::set< value_type >::const_iterator const_iterator;
    typedef typename std::set< value_type >::reference reference;
    typedef typename std::set< value_type >::const_reference const_reference;
    iterator        begin()         { return internal_vector.begin(); };
    const_iterator  begin() const   { return internal_vector.begin(); };
    iterator        end()           { return internal_vector.end(); };
    const_iterator  end() const     { return internal_vector.end(); };
    
    // change elements
//     void        push_back (const value_type& val)   { internal_vector.push_back(val); };
    // clear
    void clear() { internal_vector.clear(); };
    // swap
    void swap (PredicateGroup& pg) { internal_vector.swap(pg.internal_vector); };
    // erase
    void      erase (iterator position)               { internal_vector.erase(position); };
    size_type erase (const value_type& val)           { return internal_vector.erase(val); };
    void      erase (iterator first, iterator last)   { internal_vector.erase(first, last); };
    // insert
//     iterator    insert (iterator position, const value_type& val)                   { return internal_vector.insert(position, val); };
//     void        insert (iterator position, size_type n, const value_type& val)      { internal_vector.insert(position, n, val); };
    std::pair<iterator,bool> insert (const value_type& val) { return internal_vector.insert(val); };
    iterator                 insert (iterator position, const value_type& val) { return internal_vector.insert(position,val); };
    template <class InputIterator>
    void                     insert (InputIterator first, InputIterator last) { while (first != last) { update_predicate(*first); ++first; } };
        
    // info
    bool empty() const { return internal_vector.empty(); };
    size_type size() const { return internal_vector.size(); }
    std::set< Predicate<AtomT> > get_internal_vector_copy() { return internal_vector; }
    std::set< Predicate<AtomT> >& get_internal_vector_ref() { return internal_vector; }
    const std::set< Predicate<AtomT> >& get_internal_vector_ref() const { return internal_vector; }
    /** End of methods exposing std::vector stuff */
    
    // special
    void erase(const PredicateGroup<AtomT>& predicates_to_remove);
    
    PredicateGroup() { }
    explicit PredicateGroup(const std::string& predicates_);

    void set_constants(const PredicateGroup<AtomT>& new_constant_predicates);
    
    /**
     * \brief checks if current_predicates satisfy all predicates in PredicateGroup. Uses closed-world assumption.
     * 
     * \return True if PredicateGroup is satisfied by current_predicates
     */
    bool is_satisfied_by(const PredicateGroup& current_predicates) const;

    /**
     * \brief checks if PredicateGroup satisfies the Predicate predicate. Uses closed-world assumption.
     * 
     * \return True if predicate is satisfied by PredicateGroup
     */
    bool satisfies(const Predicate<AtomT>& predicate) const;
    
    /**
     * \brief checks if any predicate in *this is equivalent to a negated one from compared_predicates
     */
    bool has_negated_predicates(const PredicateGroup<AtomT>& compared_predicates) const;

    /**
     * \brief adds predicate to the PredicateGroup
     */
    inline void add_predicates(const PredicateGroup<AtomT>& new_predicates) {
        this->insert(new_predicates.begin(), new_predicates.end());
    }
    
    /**
     * \brief Removes current predicates and adds the new ones.
     */
    inline void replace_predicates(const PredicateGroup& new_predicates) {
        this->clear();
        insert(new_predicates.begin(), new_predicates.end());
    }

    /**
     * \brief Count number of predicates satisfied by current_predicates. Uses closed-world assumption.
     */
    int count_satisfied_by(const PredicateGroup& current_predicates) const;

    /**
     * \brief Get predicates not satisfied by argument. Uses closed-world assumption.
     */
    PredicateGroup get_not_satisfied_by(const PredicateGroup& predicates) const;
    
    /**
     * \brief Get if has the predicate doing symbolic comparison
     */
    bool has_predicate(const Predicate<AtomT>& predicate) const;
    
    void change_param(const typename AtomT::ParamType& orig_var, const typename AtomT::ParamType& new_var);
    void change_params(const typename std::vector<typename AtomT::ParamType>& orig_var_v, const std::vector<typename AtomT::ParamType>& new_var_v);
    void change_param_name(const typename AtomT::ParamType::Name& orig_var, const typename AtomT::ParamType::Name& new_var);
    void change_param_names(const std::vector<typename AtomT::ParamType::Name>& orig_var_v, const std::vector<typename AtomT::ParamType::Name>& new_var_v);
    void change_param_names(const std::vector<typename AtomT::ParamType>& orig_var_v, const std::vector<typename AtomT::ParamType>& new_var_v);
    
    /**
     * \brief Adds new_predicates and removes their negated counterparts if exists
     * TODO remove
     */
    void update_predicates(const PredicateGroup& new_predicates);
    
    /**
     * \brief Adds new_predicate and removes its negated counterpart if exists
     * TODO remove
     */
    void update_predicate(const Predicate<AtomT>& new_predicate);

    /*!
     * \brief Makes the predicates symbolic
     *
     * Changes the params to be the default symbolic params: X, Y, Z, V, W, Q.
     */
    void make_symbolic();
    
    bool is_symbolic() const;
    
    std::vector<typename AtomT::ParamType> get_params() const;
    
    PredicateGroup get_predicates_with_var(const typename AtomT::ParamType& param) const;

    /**
     * \brief Get differences with compared PredicateGroup
     *
     * Returns:
     * - Predicates in compared_group that are not present in current predicates.
     * - Predicates in *this that are not present in compared_group will be added negated.
     */
    PredicateGroup get_differences(const PredicateGroup& compared_group) const;
    
    /**
     * \brief Get common elements with compared PredicateGroup
     *
     *  Get predicates that are both in this and compared_group
     */
    PredicateGroup set_and(const PredicateGroup& compared_group) const;
    
    /**
     * \brief True if *this has all predicates in pred_group
     */
    bool includes_predicates(const PredicateGroup& pred_group) const;
    
    /**
     * \brief True if intersection is not empty with the predicate group
     */
    bool get_logical_intersects(const PredicateGroup& pred_group) const;
    
    /** \brief Remove predicates whose params are not in list_of_params  */
    void remove_predicates_with_params_not_in(const std::vector<typename AtomT::ParamType>& list_of_params);

    /** \brief Gets predicates negated */
    PredicateGroup get_negated() const;
    /** \brief Removes negated predicates */
    void remove_negated_predicates();

    /** \brief Does a PPDDL grounding for IPPC */
    void PPDDL_grounding();
    /** \brief Writes in standard PPDDL format */
    void write_PPDDL(std::ostream &out, const std::vector<PPDDL::FullObject>& rule_params) const;
    /** \brief Writes in PPDDL format without leading and */
    void write_simplified_PPDDL(std::ostream &out) const;
    /** \brief Writes (or (not p1) (not p2) ... ) */
    void write_negated_PPDDL(std::ostream &out) const;
    /** \brief Writes in LFIT format */
    void write_LFIT(std::ostream &out) const;

    void write_to_file(const std::string file_path) const;
    
    bool has_grounded_predicate(const Predicate<AtomT>& predicate) const;

    /**
     * Operators (already declared before)
     */
    friend std::ostream& operator<< <> (std::ostream &out, const PredicateGroup<AtomT>& p);
    friend bool operator== <>(const PredicateGroup<AtomT> &p1, const PredicateGroup<AtomT> &p2);
    friend bool operator< <>(const PredicateGroup<AtomT>& l, const PredicateGroup<AtomT>& r);
    
private:
    bool has_symbolic_predicate(const Predicate<AtomT>& predicate) const;
};



template <typename AtomT> class Outcome;
template <typename AtomT>
bool operator==(const Outcome<AtomT> &o1, const Outcome<AtomT> &o2);
template <typename AtomT>
bool operator<(const Outcome<AtomT>& l, const Outcome<AtomT>& r);
template <typename AtomT>
std::ostream& operator<<(std::ostream &out, const Outcome<AtomT>& o);

/*!
\class Outcome
\brief A PredicateGroup with an associated probability

The predicates define the changes made to the state when the outcome is applied. The probability defines the chances that this outcome is selected in a rule.
*/
template<typename AtomT>
class Outcome : public PredicateGroup<AtomT>
{   
public:
    float probability;
    bool noisy;
    bool is_dangerous;
    
    /**
     * Constructors
     */
    explicit Outcome(std::string line);

    explicit Outcome(const float probability_, const bool noisy_ = false) :
        PredicateGroup<AtomT>(),
        probability(probability_),
        noisy(noisy_),
        is_dangerous(false)
    { }

    Outcome(const PredicateGroup<AtomT>& predicates_, const float probability_) :
        PredicateGroup<AtomT>(predicates_),
        probability(probability_),
        noisy(false),
        is_dangerous(false)
    { }

    /**
     * Helpers
     */
    double probability_obtain_predicate(const Predicate<AtomT> predicate) const;
    /** \brief true if changes any predicate */
    bool changes_predicates() const;
    /** \brief true if changes any predicate and has a probability > 0.0 */
    bool is_useful() const;
    /** \brief Changes predicates by updating them with its PredicateGroup */
    bool apply(PredicateGroup<AtomT>& predicates) const;

    /** \brief Writes in standard PPDDL format */
    void write_PPDDL(std::ostream& out, const std::vector< PPDDL::FullObject >& rule_params) const;

    /**
     * Operators
     */
    friend std::ostream& operator<< <>(std::ostream &out, const Outcome<AtomT>& o);
    friend bool operator== <>(const Outcome<AtomT> &o1, const Outcome<AtomT> &o2);
    friend bool operator< <>(const Outcome<AtomT>& l, const Outcome<AtomT>& r);
};

// typedef boost::optional<Outcome> OutcomeOpt;


template <typename AtomT> class OutcomeGroup;
template <typename AtomT>
bool operator<(const OutcomeGroup<AtomT>& l, const OutcomeGroup<AtomT>& r);
template <typename AtomT>
std::ostream& operator<<(std::ostream &out, const OutcomeGroup<AtomT>& o);

/*!
\class OutcomeGroup
\brief A vector of outcomes
*/
template<typename AtomT>
class OutcomeGroup : public std::set<Outcome<AtomT> >
{
public:
    /** \brief true if changes any predicate */
    bool changes_predicates() const;
    
    void add_predicate_to_outcomes(const Predicate<AtomT>& new_predicate);
    
    void PPDDL_grounding();
    
    void change_param_name(const std::string& current_name, const std::string& new_name);
    
    /** \brief Add new outcomes */
    void add_new_outcomes(OutcomeGroup& new_outcomes);
    
    /** \brief Add empty outcome to fill probabilities up to 1.0 */
    void add_empty_outcome_to_fill_probability();
    
    void write_PPDDL(std::ostream& out, const std::vector< PPDDL::FullObject >& rule_params) const;

    friend bool operator< <>(const OutcomeGroup& l, const OutcomeGroup& r);  // WARNING does not compare probabilities
    friend std::ostream& operator<< <>(std::ostream &out, const OutcomeGroup& o);
};

typedef Predicate<TypedAtom> PredicateTyped;
typedef PredicateGroup<TypedAtom> PredicateGroupTyped;
typedef Outcome<TypedAtom> OutcomeTyped;
typedef OutcomeGroup<TypedAtom> OutcomeGroupTyped;

// get data from string
namespace Predicates {
//     Predicate<Atom> get_predicate_from_string(std::string item);
    PredicateGroup<TypedAtom> read_predicates_from_file(const std::string& file_path);
    
    /**
     * \brief Transforms to typed based on objects (doesn't work with symbolic predicates!)
     */
    PredicateGroup<TypedAtom> transform_to_typed(const PredicateGroup<NonTypedAtom>& non_typed_preds, const PPDDLObjectManager<PPDDL::FullObject>& object_manager);
    PredicateGroup<NonTypedAtom> transform_to_nontyped(const PredicateGroup<TypedAtom>& typed_preds);
    
    void update_object_types(PredicateGroup<TypedAtom>& preds_to_update, PPDDLObjectManager<PPDDL::FullObject>& object_manager);
}

#include <symbolic/predicates_template_imp.h>

#endif

