/*
 * Clases reprenting symbolic and grounded rules.
 * Copyright (C) 2016  David Martínez (dmartinez@iri.upc.edu)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef IRI_RULE_LEARNER_RULES_H
#define IRI_RULE_LEARNER_RULES_H 1

#include <limits>
#include <vector>
#include <fstream>
#include <cmath>
#include <map>

#include <boost/lambda/lambda.hpp>
#include <boost/lambda/bind.hpp>

#include <symbolic/symbols.h>
#include <symbolic/predicates.h>
#include <symbolic/goals.h>
#include <symbolic/transitions.h>
#include <symbolic/state.h>

class Domain;

template<typename AtomT> class RuleSet;

// friend operators with template calss
template <typename AtomT> class Rule;
template <typename AtomT>
bool operator==(const Rule<AtomT> &l, const Rule<AtomT> &r);
template <typename AtomT>
bool operator!=(const Rule<AtomT> &l, const Rule<AtomT> &r);
template <typename AtomT>
std::ostream& operator<<(std::ostream &out, const Rule<AtomT>& r);
template <typename AtomT>
bool operator<(const Rule<AtomT>& l, const Rule<AtomT>& r);
template <typename AtomT>
bool operator>(const Rule<AtomT>& l, const Rule<AtomT>& r);
template <typename AtomT>
bool operator<=(const Rule<AtomT>& l, const Rule<AtomT>& r);
template <typename AtomT>
bool operator>=(const Rule<AtomT>& l, const Rule<AtomT>& r);


template <typename AtomT>
class Rule;
#if __cplusplus > 199711L // C++11
template <typename AtomT>
using RulePtr = typename boost::shared_ptr<Rule<AtomT> >;
#else
template <typename AtomT>
class SymbolicRule;
template <typename AtomT>
class GroundedRule;
template <typename AtomT>
class SymbolicRulePtr;
template <typename AtomT>
class GroundedRulePtr;
template <typename AtomT>
struct RulePtr : public boost::shared_ptr<Rule<AtomT> > {
    RulePtr():
        boost::shared_ptr<Rule<AtomT> >()
    { }
    RulePtr(const RulePtr<AtomT>& rule_ptr):
        boost::shared_ptr<Rule<AtomT> >(rule_ptr)
    { }
    RulePtr(const boost::shared_ptr<Rule<AtomT> >& rule_ptr):
        boost::shared_ptr<Rule<AtomT> >(rule_ptr)
    { }
    RulePtr(const SymbolicRulePtr<AtomT>& rule_ptr):
        boost::shared_ptr<Rule<AtomT> >(rule_ptr)
    { }
    RulePtr(const SymbolicRule<AtomT>* rule_ptr):
        boost::shared_ptr<Rule<AtomT> >(rule_ptr)
    { }
    RulePtr(const boost::shared_ptr<SymbolicRule<AtomT> >& rule_ptr):
        boost::shared_ptr<Rule<AtomT> >(rule_ptr)
    { }
    RulePtr(const GroundedRulePtr<AtomT>& rule_ptr):
        boost::shared_ptr<Rule<AtomT> >(rule_ptr)
    { }
    RulePtr(const boost::shared_ptr<GroundedRule<AtomT> >& rule_ptr):
        boost::shared_ptr<Rule<AtomT> >(rule_ptr)
    { }
};
#endif

/**
 * \class Rule
 * \brief Define basic rule functionality, used in both symbolic and grounded rules.
 * 
 * Note that the code to ground rules is a mess. Should be replaced with prolog to have a cleaner and better implementation.
 */
template<typename AtomT>
class Rule : public AtomT
{
protected:
    PredicateGroup<AtomT> preconditions;
    std::set<typename AtomT::ParamType> deictic_params;
    
public:
    OutcomeGroup<AtomT> outcomes;
    RewardFunctionGroup goals;
    bool dangerous;
    ulong identifier;

    /*
     * Constructors
     */
    explicit Rule(const AtomT &symbol, 
                  const PredicateGroup<AtomT> &preconditions = PredicateGroup<AtomT>(), 
                  const OutcomeGroup<AtomT>& outcomes = OutcomeGroup<AtomT>(), 
                  const RewardFunctionGroup& goals = RewardFunctionGroup());
    
    /*!
     * \brief Copy the rule to the heap and return a smart pointer.
     * 
     * The changes of the returned rule are not mirrored back to this one.
     * 
     * \return shared pointer to the copy of the rule
     */
    virtual RulePtr<AtomT> make_shared() const =0;

    /**** Getter and setters *****/
    void set_preconditions(const PredicateGroup<AtomT>& new_preconditions);
    const PredicateGroup<AtomT>& get_preconditions() const { return preconditions; }

    void add_outcome(const PredicateGroup<AtomT> predicates, const float init_probability);
    void add_outcome(const Outcome<AtomT> outcome);
    
    void add_goal(const RewardFunction& goal) {
        goals.add_reward_function(goal);
    }
    
    /**
     * \brief Get number of literals in preconditions and outcomes.
     */
    int get_number_literals() const;

    /**
     * \brief Check if at least one outcome changes predicates and is not noisy.
     */
    bool has_useful_outcome() const;
    
    /** \brief Gets vector of deictic params for the rule */
    std::vector<typename AtomT::ParamType> get_deictic_params_v() const;
    
    /** \brief Gets deictic params for the rule */
    const std::set<typename AtomT::ParamType>& get_deictic_params() const { return deictic_params; };
    
    /** \brief Updates deictic params based on the preconditions */
    void update_deictic_params();
    
    /** \brief Gets the names of the parameters and the deictic parameters in a vector */
    std::vector<std::string> get_param_and_deictic_names() const;
    
    /**
     * \brief adds negated outcome predicates to the preconditions predicates.
     */
    void add_negated_outcomes_as_preconditions();
    
    void add_predicate_to_preconditions(const Predicate<AtomT >& new_predicate);
    
    /**
     * \brief adds the predicate to every outcome.
     */
    void add_predicate_to_outcomes(const Predicate<AtomT>& new_predicate);

    /**
     * \brief Obtain the probability of getting a predicate with the rule, assuming that the preconditions are satisfied.
     */
    double probability_obtain_predicate(const Predicate<AtomT> predicate) const;
    
    /**
     * \brief Sanitizes probabilities to make them sum 1.0.
     */
    void sanitize_probabities();
    
    /** \brief Transform a Pasula rule to a symbolic PPDDL rule */
    void transform_nid_params_to_pddl_params();
    
    
    /**
     * \brief Changes the name of a parameter in action, preconditions, outcomes and goals
     */
    void change_variable(const std::string& current_name, const std::string& new_name);
    
    /*
     * Transitions
     */
    /**
     * \brief counts the number of transitions initial states and actions that this rule covers.
     * 
     * A transition is covered when it has the same action name and the preconditions are satisfied by the transition's initial state.
     */
    uint count_covering_transitions_preconditions(const TransitionGroup<AtomT>& transitions) const;
    
    /**
     * \brief gets transitions covered by the rule.
     */
//     TransitionVector get_reduced_covering_transitions_preconditions(const TransitionGroup& transitions) const;

    /**
     * \brief Analyze rule differences in number of precondition predicates, outcomes and probabilities.
     * 
     * \param compared_rule rule to be compared.
     * \retval preconditions_diff_n number of different preconditions predicates.
     * \retval outcomes_diff_n number of different outcomes.
     * \retval probabilities_diff difference in the first outcome probability.
     */
    void analyze_differences(const Rule& compared_rule, int& preconditions_diff_n, int& outcomes_diff_n,  double& probabilities_diff) const;
    
    void update_outcome_probabilities(const TransitionGroup<AtomT>& transitions);

    /**
     * \brief counts the number of transitions initial states and actions that this rule covers.
     * 
     * A transition is covered when it has the same action name and the preconditions are satisfied by the transition's initial state.
     */
    virtual float covers_transition_preconditions(const Transition<AtomT>& transition, const PPDDLObjectManager<typename AtomT::ParamType>& object_manager) const = 0;
    
    virtual Transitions::Coverage covers_transition_effects(const Transition<AtomT>& transition, const PPDDLObjectManager<typename AtomT::ParamType>& object_manager, const bool check_preconditions = true) const = 0;
    
    /**
     * \brief checks if rule is grounded.
     */
    virtual bool is_symbolic() const = 0;
    
    /**
     * \brief check if prev_state satisfies rule preconditions.
     */
    virtual bool satisfiesPreconditions(const PredicateGroup<AtomT>& prev_state, const PPDDLObjectManager<typename AtomT::ParamType>& pddl_object_manager) const = 0;

    /**
     * \brief Gets a vector where element i indicates the number of transitions that outcome[i] covers
     */
    virtual std::vector<int> experiences_per_outcome(const Transition<AtomT> transition, const PPDDLObjectManager<typename AtomT::ParamType>& object_manager) const = 0;

    /**
     * \brief obtains the grounded rule for the rule given a current state.
     * 
     * By default only grounded rules valid for the current state are obtained.\n
     * The objects used to ground are the ones present in the state.
     * 
     * \param state current state where the rule will be grounded.
     * \param full if true, all grounded rules are obtained, even if their preconditions don't satisfy the state.
     */
    
    virtual RuleSet<AtomT> get_pddl_grounded_rules(const State& state, const bool full) const =0;

    // PPDDL
    /**
     * \brief Does a grounding to be used with PPDDL IPPC planners.
     */
    void PPDDL_grounding();
    
    /**
     * \brief Write in PPDDL format
     */
    void write_PPDDL(std::ostream& out) const;

    friend std::ostream& operator<< <>(std::ostream &out, const Rule& r);
    friend bool operator== <>(const Rule &r1, const Rule &r2);

    /**
     * \brief Compares two rules.
     * 
     * Comparison priority:
     * - 1º - name
     * - 2º - outcomes.size()
     * - 3º - outcomes predicates
     * - 4º - outcomes.probability
     * - 5º - preconditions
     */
    friend bool operator< <>(const Rule& l, const Rule& r);
    friend bool operator> <>(const Rule& l, const Rule& r);
    friend bool operator<= <>(const Rule& l, const Rule& r);
    friend bool operator>= <>(const Rule& l, const Rule& r);
    
private:
    virtual RuleSet<AtomT> get_pddl_grounded_rules(const PredicateGroup<AtomT>& state, const PPDDLObjectManager<typename AtomT::ParamType>& pddl_object_manager, const bool full) const =0;
};



/**
 * \class SymbolicRule
 * \brief Symbolic rule implementation. All parameters are considered to be symbolic.
 */
template<typename AtomT>
class SymbolicRule : public Rule<AtomT>
{
public:
    explicit SymbolicRule(const Rule<AtomT>& rule);
    SymbolicRule(const SymbolicRule& rule);
    explicit SymbolicRule(const AtomT &symbol_, 
                          const PredicateGroup<AtomT> &preconditions_ = PredicateGroup<AtomT>(), 
                          const OutcomeGroup<AtomT>& outcomes_ = OutcomeGroup<AtomT>(), 
                          const RewardFunctionGroup& goals_ = RewardFunctionGroup());
    
    #if __cplusplus > 199711L // C++11
    virtual RulePtr<AtomT> make_shared() const { return RulePtr<AtomT>(new SymbolicRule<AtomT>(*this)); }
    #else
    virtual RulePtr<AtomT> make_shared() const { return RulePtr<AtomT>(boost::shared_ptr<SymbolicRule<AtomT> >(new SymbolicRule<AtomT>(*this))); }
    #endif

    virtual float covers_transition_preconditions(const Transition<AtomT>& transition, const PPDDLObjectManager<typename AtomT::ParamType>& object_manager) const;
    
    virtual Transitions::Coverage covers_transition_effects(const Transition<AtomT>& transition, const PPDDLObjectManager<typename AtomT::ParamType>& object_manager, const bool check_preconditions = true) const;
    
    virtual std::vector<int> experiences_per_outcome(const Transition<AtomT> transition, const PPDDLObjectManager<typename AtomT::ParamType>& object_manager) const;
    virtual inline bool is_symbolic() const {
        return true;
    };

    virtual bool satisfiesPreconditions(const PredicateGroup<AtomT>& predicate_group, const PPDDLObjectManager<typename AtomT::ParamType>& pddl_object_manager) const;
    
    virtual RuleSet<AtomT> get_pddl_grounded_rules(const State& state, const bool full) const;

private:
    virtual RuleSet<AtomT> get_pddl_grounded_rules(const PredicateGroup<AtomT>& pred_group, const PPDDLObjectManager<typename AtomT::ParamType>& pddl_object_manager, const bool full) const;
};


/**
 * \class GroundedRule
 * \brief Grounded rule implementation. All parameters are considered to be grounded.
 */
template<typename AtomT>
class GroundedRule : public Rule<AtomT>
{
public:
    /**
     * \brief Gets the set of possible rules by grounded all deictic references based on the predicate_group.
     * 
     * By default, only candidates satisfied by the predicate_group are returned.
     * 
     * \param state grounded predicates that have to be satisfied by the rules. They also provide the list of objects available.
     * \param full returns all possible grounded rule sets, even if the predicate_group doesn't satisfy them.
     * \return list of rules with all deictic references grounded.
     */
    
    virtual RuleSet<AtomT> get_pddl_grounded_rules(const State& state, const bool full) const;

    GroundedRule(const Rule<AtomT>& rule, 
                 const std::vector< typename AtomT::ParamType::Name >& grounded_params_, 
                 const PPDDLObjectManager<typename AtomT::ParamType>& pddl_object_manager);
    GroundedRule(const Rule<AtomT>& rule, 
                 const std::vector< typename AtomT::ParamType::Name >& symbolic_params_, 
                 const std::vector< typename AtomT::ParamType::Name >& grounded_params_, 
                 const PPDDLObjectManager<typename AtomT::ParamType>& pddl_object_manager);
    GroundedRule(const GroundedRule& grounded_rule);
    
    #if __cplusplus > 199711L // C++11
    virtual RulePtr<AtomT> make_shared() const { return RulePtr<AtomT>(new GroundedRule<AtomT>(*this)); }
    #else
    virtual RulePtr<AtomT> make_shared() const { return RulePtr<AtomT>(boost::shared_ptr<GroundedRule<AtomT> >(new GroundedRule<AtomT>(*this))); }
    #endif

    #pragma GCC diagnostic push
    #pragma GCC diagnostic ignored "-Wunused-parameter"
    virtual RuleSet<AtomT> get_grounded_rules(const PredicateGroup<AtomT> predicate_group, const bool full = false) const {
        RuleSet<AtomT> result;
        boost::shared_ptr<GroundedRule<AtomT> > rule_ptr = boost::make_shared<GroundedRule<AtomT> >(*this);
        result.push_back(rule_ptr);
        return result;
    };
    #pragma GCC diagnostic pop

    // redefined virtual methods
    virtual float covers_transition_preconditions(const Transition<AtomT>& transition, const PPDDLObjectManager<typename AtomT::ParamType>& object_manager) const;
    
    virtual Transitions::Coverage covers_transition_effects(const Transition<AtomT>& transition, const PPDDLObjectManager<typename AtomT::ParamType>& object_manager, const bool check_preconditions = true) const;
    
    virtual inline bool is_symbolic() const {
        return false;
    };
    virtual std::vector<int> experiences_per_outcome(const Transition<AtomT> transition, const PPDDLObjectManager<typename AtomT::ParamType>& object_manager) const;

    virtual bool satisfiesPreconditions(const PredicateGroup<AtomT>& predicates, const PPDDLObjectManager<typename AtomT::ParamType>& pddl_object_manager) const;
    bool satisfiesPreconditions(const PredicateGroup<AtomT>& predicates) const;
    
private:
    virtual RuleSet<AtomT> get_pddl_grounded_rules(const PredicateGroup<AtomT>& pred_group, const PPDDLObjectManager<typename AtomT::ParamType>& pddl_object_manager, const bool full) const;
    
    void save_ground_outcomes(const OutcomeGroup<AtomT>& symbolic_outcomes, 
                              const std::vector< typename AtomT::ParamType::Name >& used_symbolic_params, 
                              const std::vector< typename AtomT::ParamType::Name >& used_grounded_params);
    void save_ground_preconditions(const PredicateGroup< AtomT >& symbolic_preconditions, 
                                   const std::vector< typename AtomT::ParamType::Name >& used_symbolic_params, 
                                   const std::vector< typename AtomT::ParamType::Name >& used_grounded_params);
    void save_ground_goals(const RewardFunctionGroup& symbolic_goals, 
                           const std::vector< typename AtomT::ParamType::Name >& used_symbolic_params, 
                           const std::vector< typename AtomT::ParamType::Name >& used_grounded_params, 
                           const PPDDLObjectManager<typename AtomT::ParamType>& pddl_object_manager);

    RuleSet<AtomT> get_deictic_grounded_rules(const PredicateGroup<TypedAtom> predicate_group, const std::vector< std::vector <int> >& variables_per_param, bool remove_invalid = true) const;
};

#if __cplusplus > 199711L // C++11
template <typename AtomT>
using SymbolicRulePtr = typename boost::shared_ptr<SymbolicRule<AtomT> >;
template <typename AtomT>
using GroundedRulePtr = typename boost::shared_ptr<GroundedRule<AtomT> >;

template <typename AtomT>
using RuleConstPtr = typename boost::shared_ptr<const Rule<AtomT> >;
template <typename AtomT>
using SymbolicRuleConstPtr = typename boost::shared_ptr<const SymbolicRule<AtomT> >;
template <typename AtomT>
using GroundedRuleConstPtr = typename boost::shared_ptr<const GroundedRule<AtomT> >;
#else
template <typename AtomT>
struct SymbolicRulePtr : public boost::shared_ptr<SymbolicRule<AtomT> > {};
template <typename AtomT>
struct GroundedRulePtr : public boost::shared_ptr<GroundedRule<AtomT> > {};

template <typename AtomT>
struct RuleConstPtr : public boost::shared_ptr<const Rule<AtomT> > {};
template <typename AtomT>
struct SymbolicRuleConstPtr : public boost::shared_ptr<const SymbolicRule<AtomT> > {};
template <typename AtomT>
struct GroundedRuleConstPtr : public boost::shared_ptr<const GroundedRule<AtomT> > {};
#endif



// friend operators with template calss
template <typename AtomT>
bool operator==(const RuleSet<AtomT> &l, const RuleSet<AtomT> &r);
template <typename AtomT>
std::ostream& operator<<(std::ostream &out, const RuleSet<AtomT>& r);
template <typename AtomT>
bool operator!=(const RuleSet<AtomT>& l, const RuleSet<AtomT>& r);


// declare in advance
template<typename AtomT>
class RuleSet;
#if __cplusplus > 199711L // C++11
template <typename AtomT>
using RuleSetPtr = typename boost::shared_ptr<RuleSet<AtomT> >;
template <typename AtomT>
using RuleSetConstPtr = typename boost::shared_ptr<const RuleSet<AtomT> >;
#else
template <typename AtomT>
struct RuleSetPtr : public boost::shared_ptr<RuleSet<AtomT> > { };
template <typename AtomT>
struct RuleSetConstPtr : public boost::shared_ptr<const RuleSet<AtomT> > {};
#endif


/**
 * \class RuleSet
 * \brief A vector of RulePtr with extended functionality.
 */
template<typename AtomT>
class RuleSet : public std::vector< RulePtr<AtomT> >
{
public:
    // constructors
    RuleSet();
    RuleSet(const RuleSet& rules_);
    #if __cplusplus > 199711L // C++11
    RuleSet(RuleSet&& rules_);
    #endif

    // i/o with files
    void read_NID_rules(std::string file_path, const Domain& domain);
    void write_rules_to_file(std::string file_path) const;

    /**
     * \brief Sanitizes rule probabilities so they sum 1.0.
     */
    void sanitize_probabities();
    
    /**
     * \brief Gets the action names represented by the rules.
     */
    std::vector<std::string> get_action_names() const;
    
    /**
     * \brief Gets the actions represented by the rules.
     */
    std::vector<AtomT> get_actions() const;

    // rules info
    /**
     * \brief Gets the summed probability that the rules can obtain the predicate considering that all the preconditions are satisfied.
     */
    double can_obtain_predicate(const Predicate<AtomT> predicate) const;

    // add and remove rules
    void add_rules(const RuleSet& new_rules);
    inline void set_rules(const RuleSet& new_rules) {
        this->clear();
        add_rules(new_rules);
    }
    RuleSet remove_action_rules(AtomT action);
    bool remove_rule(const RulePtr<AtomT> rule_to_remove);
    void remove_special_rules();
    
    /**
     * \brief Removes rules that cannot change nothing (no useful outcomes).
     */
    void remove_nop_rules();

    // Actions stuff
    /**
     * \brief Gets a map that for each action has the vector of rules that represent it.
     */
    std::map<std::string, RuleSet> get_rules_per_action() const;
    
    /**
     * \brief Gets the rules whose action name is the same as the action parameter.
     * 
     * Uses a symbolic action.
     */
    RuleSet get_action_rules(const AtomT& action) const;
    
    /**
     * \brief Get grounded action rules. The state is used just for deictic references.
     * 
     * The state is used just for deictic references.
     * 
     * \param grounded_action a grounded action.
     * \param state state from which to obtain objects to generate the deictic references.
     * \return grounded rules for the grounded action and state
     */
    RuleSet get_grounded_action_rules(const AtomT& grounded_action, const State& state) const;
    
    /**
     * \brief Get grounded rules using predicates' objects. They don't have to be satisfied by predicates.
     */
    RuleSet get_pddl_grounded_rules(const State& state, const bool full = true) const;

    // get info
    /**
     * \brief Get number of literals, considering all preconditions and outcomes.
     */
    int get_number_literals() const;
    
    /**
     * \brief Get rules that cover the transition preconditions and have the same action name.
     */
    RuleSet get_covering_rules(const Transition<AtomT>& transition, const PPDDLObjectManager<typename AtomT::ParamType>& pddl_object_manager) const;
    
    /**
     * \brief Get rules whose preconditions are satisfied by the predicates.
     */
    RuleSet get_covering_rules(const PredicateGroup<AtomT>& predicates, const PPDDLObjectManager<typename AtomT::ParamType>& pddl_object_manager) const;
    
    /**
     * \brief Get rules whose preconditions are satisfied by the predicates.
     */
    inline RuleSet get_covering_rules(const State& state) const 
        { return get_covering_rules(state, state.ppddl_object_manager); };
    
    /**
     * \brief Check if rule is dangerous.
     * 
     * A rule is dangerous when a dead end was obtained, and the negated excuse predicates where in the outcomes of the rule.
     */
    bool is_dangerous(const AtomT action, const State& state) const;

    // set info
    /**
     * \brief Sets the dangerous flag in all rules that can obtain the negated dead_end_excuses.
     */
    void set_dangerous_rules(const PredicateGroup<AtomT>& dead_end_excuses);
    
    /**
     * \brief Add all negated outcomes predicates to the preconditions.
     */
    void add_negated_outcomes_as_preconditions();
    
    void add_predicate_to_preconditions(const Predicate<AtomT>& new_predicate);
    void add_predicate_to_outcomes(const Predicate<AtomT>& new_predicate);
    void add_goal(const RewardFunction& goal);

    // rule analysis
    /**
     * \brief Set unique identifiers to rules.
     * 
     * Equal rules maintain the same identifier. Else, the identifiers are passed to the most similar rule if applicable.
     * New different rules get a new identifier.
     * This is just used for visualization purposes.\n
     */
    void set_identifiers(RuleSet<AtomT>& new_rules_ptr);
    
    /**
     * \brief Get info about the learning step to be shown to the teacher.
     */
    void analyze_differences(RuleSet compared_ruleset, int& rule_number_diff, int& preconditions_diff, int& outcomes_num_diff, double& first_outcome_prob_diff);
    
    /**
     * \brief Get info about the learning step to be shown to the teacher.
     */
    void analyze_differences(RuleSet compared_ruleset, RuleSet& added_rules, RuleSet& removed_rules,
                             RuleSet& original_changed_rules, RuleSet& compared_changed_rules,
                             std::vector<int>& preconditions_diff, std::vector<int>& outcomes_diff,  std::vector<double>& probabilities_diff
                            ) const;
                            
    RuleSet find_rules_with_a_precondition(const Predicate<AtomT>& precondition) const;
    RuleSet find_rules_with_a_precondition(const PredicateGroup<AtomT>& possible_preconditions) const;
    
    /**
     * \brief RuleSet score metric (Pasula et al JAIR 07)
     *
     * S(R) = sum(rules) sum(transitions) log(P(s'|s,a,r)) - ( alpha * PEN(r) )
     */
    double score(const TransitionGroup<AtomT> transitions, const PPDDLObjectManager<typename AtomT::ParamType>& object_manager) const;
    
    float covers_transition_preconditions(const Transition<AtomT>& transition, const PPDDLObjectManager<typename AtomT::ParamType>& object_manager) const;
    
    Transitions::Coverage covers_transition_effects(const Transition<AtomT>& transition, const PPDDLObjectManager<typename AtomT::ParamType>& object_manager, const bool check_preconditions = true) const;
    
    void update_outcome_probabilities(const TransitionGroup<AtomT>& transitions) 
    { for (auto& rule_ptr : *this) { rule_ptr->update_outcome_probabilities(transitions); } }

    /**
     * \brief Does a grounding to be used with PPDDL IPPC planners.
     */
    void PPDDL_grounding();
    
    /**
     * \brief Write RuleSet in PPDDL format
     */
    void write_PPDDL(std::ostream& out) const;

    // operators
    friend bool operator== <>(const RuleSet &r1, const RuleSet &r2);
    friend bool operator!= <>(const RuleSet &r1, const RuleSet &r2);
    friend std::ostream& operator<< <>(std::ostream &out, const RuleSet& r);
    RuleSet& operator=(RuleSet const & rhs);
    #if __cplusplus > 199711L // C++11
    RuleSet& operator=(RuleSet&& rhs);
    #endif

private:
    void transform_symbolic_pasula_to_symbolic_PDDL();
};

namespace Typed {
    typedef Rule<TypedAtom> Rule;
    typedef RulePtr<TypedAtom> RulePtr;
    typedef RuleConstPtr<TypedAtom> RuleConstPtr;
    typedef SymbolicRule<TypedAtom> SymbolicRule;
    typedef GroundedRule<TypedAtom> GroundedRule;
    typedef SymbolicRulePtr<TypedAtom> SymbolicRulePtr;
    typedef GroundedRulePtr<TypedAtom> GroundedRulePtr;
    typedef RuleSet<TypedAtom> RuleSet;
    typedef RuleSetPtr<TypedAtom> RuleSetPtr;
}

namespace RuleUtils {
    Predicate<TypedAtom> get_literal_default_from_rule(const Typed::Rule& rule);
    PredicateGroup<TypedAtom> get_literal_defaults_from_ruleset(const Typed::RuleSet& rule_set);
    Typed::RulePtr create_set_literal_default_rule(const TypedAtom& literal, const bool default_true);
}

#include <symbolic/rules_template_imp.h>

#endif
