#include "rules.h"
#include <symbolic/domain.h>

template<>
std::ostream& operator<<(std::ostream &out, const Rule<TypedAtom>& r)
{
    out << "ACTION: # " << r.identifier << "\n";
    out << *static_cast<const TypedAtom*>(&r);
    out << "\n";
    out << "CONTEXT:\n";
    out << r.preconditions;
    out << "\n";
    out << "OUTCOMES:\n";
    BOOST_FOREACH(const Outcome<TypedAtom>& outcome, r.outcomes) {
        out << outcome << std::endl;
    }
    if (!r.goals.empty()) {
        out << "REWARD:\n";
        r.goals.write_PPDDL(out, r.params);
    }
    out << std::endl;

    return out;
}

template<>
std::ostream& operator<<(std::ostream &out, const Rule<IndexedAtom>& r)
{
    out << "ACTION: # " << r.identifier << "\n";
    out << *static_cast<const IndexedAtom*>(&r);
    out << "\n";
    out << "CONTEXT:\n";
    out << r.preconditions;
    out << "\n";
    out << "OUTCOMES:\n";
    BOOST_FOREACH(const Outcome<IndexedAtom>& outcome, r.outcomes) {
        out << outcome << std::endl;
    }
    out << std::endl;

    return out;
}

template<>
RuleSet<IndexedAtom> SymbolicRule<IndexedAtom>::get_pddl_grounded_rules(const State& state, const bool full) const
{
    LOG(ERROR) << "Cannot use State (with TypedAtoms) and Indexed atoms at the same time";
    return RuleSet<IndexedAtom>();
}

template<>
RuleSet<TypedAtom> SymbolicRule<TypedAtom>::get_pddl_grounded_rules(const State& state, const bool full) const
{
    return get_pddl_grounded_rules(state, state.ppddl_object_manager, full);
}

template<>
void GroundedRule<TypedAtom>::save_ground_goals(const RewardFunctionGroup& symbolic_goals, 
                                            const std::vector<TypedAtom::ParamType::Name>& used_symbolic_params, 
                                            const std::vector<TypedAtom::ParamType::Name>& used_grounded_params, 
                                            const PPDDLObjectManager<TypedAtom::ParamType>& pddl_object_manager)
{
    this->goals = symbolic_goals.get_pddl_grounded_goals(used_symbolic_params, used_grounded_params, pddl_object_manager);
}

template<>
void GroundedRule<IndexedAtom>::save_ground_goals(const RewardFunctionGroup& symbolic_goals, 
                                            const std::vector<IndexedAtom::ParamType::Name>& used_symbolic_params, 
                                            const std::vector<IndexedAtom::ParamType::Name>& used_grounded_params, 
                                            const PPDDLObjectManager<IndexedAtom::ParamType>& pddl_object_manager)
{
    // DO NOTHING, goals with IndexedAtom are not supported
}

template<>
RuleSet<IndexedAtom> GroundedRule<IndexedAtom>::get_pddl_grounded_rules(const State& state, const bool full) const
{
    LOG(ERROR) << "Cannot use State (with TypedAtoms) and Indexed atoms at the same time";
    return RuleSet<IndexedAtom>();
}

template<>
RuleSet<TypedAtom> GroundedRule<TypedAtom>::get_pddl_grounded_rules(const State& state, const bool full) const
{
    return get_pddl_grounded_rules(state, state.ppddl_object_manager, full);
}

template<>
void RuleSet<TypedAtom>::read_NID_rules(std::string file_path, const Domain& domain)
{
    // DEPRECATED
    std::string line;
    std::ifstream rules_file(file_path.c_str());

    // Clear previous rules
    this->clear();

    if (rules_file.is_open()) {
        std::string line_content;
        std::string line_content2;
        std::string line_title;

        getline(rules_file, line_title);
        while (rules_file.good()) {
            // ACTION:
            // read until ACTION: found
            while (line_title.find("ACTION:") == std::string::npos) {
                getline(rules_file, line_title);
            }
            
            boost::optional<uint> rule_id;
            // Get action id from ACTION: #X title
            if (line_title.find("#") != std::string::npos) { // if has id
                std::string ss_item;
                std::stringstream line_title_ss(line_title);
                std::getline(line_title_ss, ss_item, '#');
                std::getline(line_title_ss, ss_item, '#');
                try {
                    ss_item = utils::trim(ss_item);
                    rule_id = boost::lexical_cast<uint>(ss_item);
                } catch (boost::bad_lexical_cast &) {
                    // ignore
                    LOG(WARNING) << "Error when loading rule. Is there a # in a \"Action:\" line?";
                    LOG(WARNING) << "Rule line is " << line_title;
                }
            }

            // Action name
            getline(rules_file, line_content);

            // CONTEXT:
            getline(rules_file, line_title);

            // Preconditions
            getline(rules_file, line_content2);

            NonTypedAtom action = NonTypedAtom(utils::trim(line_content));
            PredicateGroup<NonTypedAtom> preconditions(line_content2);
            SymbolicRule<TypedAtom> rule = SymbolicRule<TypedAtom>(domain.transform_to_typed(action),
                                             domain.transform_to_typed(preconditions));
            if (rule_id) {
                rule.identifier = *rule_id;
            }

            // OUTCOMES:
            getline(rules_file, line_title);

            // Outcome i
            getline(rules_file, line_content);
            while ((line_content != "") && (line_content != "REWARD:")) {
                Outcome<TypedAtom> outcome_non_typed(line_content);
                Outcome<TypedAtom> outcome(outcome_non_typed.probability, outcome_non_typed.noisy);
                outcome.add_predicates(domain.transform_to_typed(Predicates::transform_to_nontyped(outcome_non_typed)));
                rule.add_outcome(outcome);
                getline(rules_file, line_content);
                line_content = utils::trim(line_content);
            }

            // check if rule has rewards
            line_title = line_content;
            if (line_title == "REWARD:") {
                getline(rules_file, line_content);
                line_content = utils::trim(line_content);
                while (line_content != "") {
                    // Add rule preconditions to rule goals
                    Goal::Ptr goal_ptr(new Goal(line_content));
                    goal_ptr->add_predicates(rule.get_preconditions());
                    rule.goals.add_reward_function(*goal_ptr);
                    // Get next line
                    getline(rules_file, line_content);
                    line_content = utils::trim(line_content);
                }
            }

            // Create rule
            if (rule.name != DEFAULT_ACTION_NAME) {
                this->push_back(rule.make_shared());
            }

            // read lines until not empty line
            getline(rules_file, line_title);
            line_title = utils::remove_spaces(line_title);
            while ((not rules_file.eof()) && rules_file.good() && (line_title.compare("") == 0)) {
                getline(rules_file, line_title);
            }
        }
    }
    else {
        LOG(ERROR) << "Couldn't open file to read rules: \"" << file_path << "\"";
    }

    sanitize_probabities();
    transform_symbolic_pasula_to_symbolic_PDDL();

//         std::cout << "Read rules are " << *this << std::endl;
    rules_file.close();
}

/**
 * RuleUtils
 */
Predicate<TypedAtom> RuleUtils::get_literal_default_from_rule(const Typed::Rule& rule)
{
    assert(rule.name == ACTION_NAME_SET_LITERAL_DEFAULT);
    return *rule.outcomes.begin()->begin();
}

PredicateGroup<TypedAtom> RuleUtils::get_literal_defaults_from_ruleset(const Typed::RuleSet& rule_set)
{
    PredicateGroup<TypedAtom> result;
    Typed::RuleSet set_default_rules = rule_set.get_action_rules(MAKE_TYPED_ATOM_SET_LITERAL_DEFAULT);
    BOOST_FOREACH(const Typed::RulePtr& rule_ptr, set_default_rules) {
        result.insert(get_literal_default_from_rule(*rule_ptr));
    }
    return result;
}

Typed::RulePtr RuleUtils::create_set_literal_default_rule(const TypedAtom& literal, const bool default_true)
{
    #if __cplusplus > 199711L // C++11
    Typed::SymbolicRulePtr set_literal_default_rule_ptr(new Typed::SymbolicRule(MAKE_TYPED_ATOM_SET_LITERAL_DEFAULT));
    #else
    Typed::RulePtr set_literal_default_rule_ptr = Typed::RulePtr(boost::shared_ptr<Typed::SymbolicRule>(new Typed::SymbolicRule(MAKE_TYPED_ATOM_SET_LITERAL_DEFAULT)));
    #endif
    PredicateGroup<TypedAtom> atom_pred_group;
    atom_pred_group.insert(Predicate<TypedAtom>(literal, true));
    set_literal_default_rule_ptr->set_preconditions(atom_pred_group);
    if (!default_true) {
        atom_pred_group = atom_pred_group.get_negated();
    }
    set_literal_default_rule_ptr->outcomes.insert(Outcome<TypedAtom>(atom_pred_group, 1.0));
    return set_literal_default_rule_ptr;
}
