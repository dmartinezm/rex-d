/*
 * Clases representing transitions (previous experiences).
 * Copyright (C) 2016  David Martínez (dmartinez@iri.upc.edu)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef IRI_RULE_LEARNER_TRANSITIONS_H
#define IRI_RULE_LEARNER_TRANSITIONS_H 1

// #define DEBUG_COVERAGE_ERRORS

#include <vector>
#include <fstream>
#include <algorithm>    // std::min
#include <boost/graph/graph_concepts.hpp>

#include <symbolic/symbols.h>
#include <symbolic/predicates.h>

namespace Transitions {
struct Coverage {
  private:
    typedef size_t size_type;
    typedef size_t index_type;
    
    std::vector< std::vector<float> > coverage_vector;
    
#ifdef DEBUG_COVERAGE_ERRORS
    std::vector<float> coverage_errors_vector;
#else
    float coverage_error;
#endif
    bool is_empty;
    
  public:
    Coverage() : is_empty(true) {}
    
    explicit Coverage(const uint changes_size):
        coverage_vector(changes_size),
#ifndef DEBUG_COVERAGE_ERRORS
        coverage_error(0.0),
#endif
        is_empty(true)
    { }
    
    size_type size() const { return coverage_vector.size(); }

#ifdef DEBUG_COVERAGE_ERRORS
    void add_error(const float new_error_prob) {coverage_errors_vector.push_back(new_error_prob); is_empty = false; }
#else
    void add_error(const float new_error_prob) {coverage_error += (1.0 - coverage_error) * new_error_prob; is_empty = false; }
#endif

    void add_change_coverage(const index_type change_idx, const float new_coverage) 
        { coverage_vector[change_idx].push_back(new_coverage); is_empty = false; };    
    float get_error_probability() const;
    float get_change_coverage(const index_type change_idx, const float optimistic_value = 0.0) const;
    float get_coverage(const bool complete, const float optimistic_value = 0.0) const;
    float get_likelihood(const bool complete, const float optimistic_value = 0.0) const;
    
    Coverage& operator+=(const Coverage& rhs);
    friend Coverage operator+(const Coverage &c1, const Coverage &c2);
    friend std::ostream& operator<<(std::ostream &out, const Coverage &c);
};
}

// required by Transition method template
template<typename AtomT>
class TransitionGroup;

template <typename AtomT> class Transition;
template <typename AtomT>
bool operator<(const Transition<AtomT>& l, const Transition<AtomT>& r);
template <typename AtomT>
std::ostream& operator<<(std::ostream &out, const Transition<AtomT>& p);

/*!
\class Transition
\brief Class representing previous experiences of the form: state_t, action_t, state_t+1, obtained reward
*/
template<typename AtomT>
class Transition
{
private:
    PredicateGroup<AtomT> prev_state, next_state;
    AtomT action;
    float reward;
    PredicateGroup<AtomT> changes;
    mutable int number_repetitions; //! Number of times that the transition has appeared
    
public:
    Transition(const PredicateGroup<AtomT>& prev_state, 
               const AtomT& action, 
               const PredicateGroup<AtomT>& next_state, 
               const float& reward, 
               const bool& variable_maintains_to_next_state);
    
    Transition(const PredicateGroup<AtomT>& prev_state, 
               const AtomT& action, 
               const PredicateGroup<AtomT>& next_state, 
               const float& reward, 
               const std::set<AtomT>& default_false_literals);

    // get info
    const PredicateGroup<AtomT>& get_prev_state() const { return prev_state; }
    const PredicateGroup<AtomT>& get_post_state() const { return next_state; }
    const AtomT& get_action() const { return action; }
    const float& get_reward() const { return reward; }
    const PredicateGroup<AtomT>& get_changes() const { return changes; };
    
    void increase_number_repetitions(const uint new_repetitions = 1) const { number_repetitions += new_repetitions; /* is mutable */ }
    int get_number_repetitions() const { return number_repetitions; }
    void set_number_repetitions(const uint new_repetitions) { number_repetitions = new_repetitions; }
    
    /**
     * \brief Get symbolic transitions
     * 
     * Gets the symbolic transitions, taking as many symbolic variables as action params
     * When a predicate is modified, related variables are grouped into the same symbolic transition,
     * otherwise a new transition is generated for every combination of params.
     * \n
     * When using action params, the action is maintained.
     * When using other params, the noop action is set.
     */
    TransitionGroup<AtomT> get_symbolic_lfit_transitions(const PPDDLObjectManager<typename AtomT::ParamType>& object_manager, 
                                                         const uint max_action_variables, 
                                                         const bool nonaction_vars_combinations) const;

    void name_grounding();

    bool nothing_changed() const;
    
    /**
     * \brief As we are using closed-world assumption, negated predicates don't have to be specified directly
     */
    void remove_negated_predicates();

    friend std::ostream& operator<< <>(std::ostream &out, const Transition &s);
    friend bool operator< <>(const Transition &l, const Transition &r);
    
private:
    void update_changes(const bool& variable_maintains_to_next_state);
    void update_changes(const std::set<AtomT>& default_false_literals);
    void make_lfit_relational_transition(const std::vector<typename AtomT::ParamType>& vars_combination);
};

#if __cplusplus > 199711L // C++11
template<typename AtomT>
using TransitionVector = typename std::vector< Transition<AtomT> >;
template<typename AtomT>
using TransitionSet = typename std::set< Transition<AtomT> >;
template<typename AtomT>
using TransitionHistorial = typename std::vector< typename TransitionSet<AtomT>::iterator >;
#else
template<typename AtomT>
struct TransitionVector : public std::vector< Transition<AtomT> > {};
template<typename AtomT>
struct TransitionSet : public std::set< Transition<AtomT> > {};
template<typename AtomT>
struct TransitionHistorial : public std::vector< typename TransitionSet<AtomT>::iterator > {};
#endif


template <typename AtomT>
std::ostream& operator<<(std::ostream &out, const TransitionGroup<AtomT>& p);

/*!
\class TransitionGroup
\brief Vector of transitions.

Its transitions can be accessed in two different ways:
 - Historial: All the transitions ordered by time.
 - Set: Set of non-repeated transitions (no time ordering).
*/
template<typename AtomT>
class TransitionGroup
{
public:
    typedef Transition<AtomT>& reference;
    typedef const Transition<AtomT>& const_reference;
    
    PPDDLObjectManager<typename AtomT::ParamType> ppddl_object_manager;
    
    explicit TransitionGroup(const PPDDLObjectManager<typename AtomT::ParamType>& object_manager) : ppddl_object_manager(object_manager) {};
    
    TransitionGroup(const TransitionGroup& other);
    TransitionGroup& operator=(const TransitionGroup& other);
    
    #if __cplusplus > 199711L // C++11
    TransitionGroup(TransitionGroup&& other);
    TransitionGroup& operator=(TransitionGroup&& other);
    #endif
    
    void name_grounding();
    
    void add_transition(const Transition<AtomT>& transition, const uint repetitions = 1);
    void add_transitions(const TransitionGroup& transitions);
    
    size_t size_set() const { return transition_set.size(); }
    size_t size_historial() const { return transition_historial.size(); }
    
    const Transition<AtomT>& get_transition_from_set(const int index) const;
    
    /*
    reference operator[] (size_t n);
    const_reference operator[] (size_t n) const;*/
    
    typename TransitionHistorial<AtomT>::iterator begin_historial() { return transition_historial.begin(); };
    typename TransitionHistorial<AtomT>::const_iterator begin_historial() const { return transition_historial.begin(); };
    typename TransitionHistorial<AtomT>::iterator end_historial() { return transition_historial.end(); };
    typename TransitionHistorial<AtomT>::const_iterator end_historial() const { return transition_historial.end(); };
    
    typename TransitionSet<AtomT>::iterator begin_set() { return transition_set.begin(); };
    typename TransitionSet<AtomT>::const_iterator begin_set() const { return transition_set.begin(); };
    typename TransitionSet<AtomT>::iterator end_set() { return transition_set.end(); };
    typename TransitionSet<AtomT>::const_iterator end_set() const { return transition_set.end(); };
    
    /**
     * \brief Write transitions to file with all the important information
     */
    void write_transitions_to_file(const std::string& file_path) const;
    
    /**
     * \brief Write transitions to file in a format compatible with Pasula learner
     */
    void write_pasula_transitions_to_file(const std::string& transitions_filename) const;
    
    /**
     * \brief Write transitions to file in a format compatible with LFIT learner
     */
    void write_lfit_transitions_to_file(const std::string& file_path, 
                                        const std::string& domain_types) const;
    
    /**
     * \brief Get transitions that include the given action (symbolic comparison done to check if the action is the same as the transitiona action).
     * 
     * \param action Action (AtomT) whose transitions are returned
     * \return list of transitions with the given action name
     */
    TransitionGroup get_action_transitions(const AtomT& action) const;
        
    /**
     * \brief Get symbolic transitions
     * 
     * Gets the symbolic transitions for each transition, taking as many symbolic variables as action params
     * When a predicate is modified, related variables are grouped into the same symbolic transition,
     * otherwise a new transition is generated for every combination of params.
     * \n
     * When using action params, the action is maintained.
     * When using other params, the noop action is set.
     */
    TransitionGroup get_symbolic_lfit_transitions_unordered(const uint max_action_variables, const bool nonaction_vars_combinations) const;
    
    /**
     * \brief Get transitions that are normalized with respect to the number of times each prev_state-action pair occurs.
     */
    TransitionGroup get_normalized_transitions() const;
    
    /**
     * \brief As we are using closed-world assumption, negated predicates don't have to be specified directly
     */
    void remove_negated_predicates();

    friend std::ostream& operator<< <>(std::ostream &out, const TransitionGroup &s);
    
private:
    TransitionSet<AtomT> transition_set;
    TransitionHistorial<AtomT> transition_historial;
};

#if __cplusplus > 199711L // C++11
template<typename AtomT>
using TransitionGroupPtr = typename boost::shared_ptr<TransitionGroup<AtomT> >;
template<typename AtomT>
using TransitionGroupConstPtr = typename boost::shared_ptr<const TransitionGroup<AtomT> >;
#else
template<typename AtomT>
struct TransitionGroupPtr : public boost::shared_ptr<TransitionGroup<AtomT> > {
    TransitionGroupPtr(){};
    TransitionGroupPtr(const boost::shared_ptr<TransitionGroup<AtomT> >& other):
        boost::shared_ptr<TransitionGroup<AtomT> >(other)
    {}
};
template<typename AtomT>
struct TransitionGroupConstPtr : public boost::shared_ptr<const TransitionGroup<AtomT> > {};
#endif

// other


// write/read to file
TransitionGroup<TypedAtom> read_transitions_from_file(const std::string& file_path, const PPDDLObjectManager<PPDDL::FullObject>& object_manager, const PredicateGroup<TypedAtom>& constant_preds);

#include <symbolic/transitions_template_imp.h>

#endif
