/*
 * Base class for defining a reader
 * Copyright (C) 2016  dmartinez <email>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef REXD_READER_BASE_H
#define REXD_READER_BASE_H


#include <symbolic/predicates.h>
#include <symbolic/rules.h>
#include <symbolic/state.h>
#include <symbolic/transitions.h>


struct ReaderUnsupportedTypeException : public std::exception
{
    std::string unsupported_type;
    std::string language;
    
    ReaderUnsupportedTypeException(std::string language_, std::string unsupported_type_) throw(): 
        std::exception(), 
        unsupported_type(unsupported_type_),
        language(language_)
    {};
    
    ~ReaderUnsupportedTypeException() throw() {};
    
    const char * what () const throw ()
    {
        return std::string("Error, type \"" + unsupported_type + "\" is not supported in \"" + language + "\".").c_str();
    }
};


class FileParser
{
public:
    #if __cplusplus > 199711L // C++11
        virtual ~FileParser() = default;
    #else
        virtual ~FileParser() {}
    #endif
    
    virtual NonTypedAtom    parse_atom               (const std::string& str) const =0;
    virtual TypedAtom       parse_typed_atom         (const std::string& str) const =0;
    virtual Predicate<NonTypedAtom> parse_predicate            (const std::string& str) const =0;
    virtual PredicateGroup<NonTypedAtom> parse_predicate_group (const std::string& str) const =0;
    virtual Outcome<TypedAtom> parse_outcome              (const std::string& str) const =0;
    virtual OutcomeGroup<TypedAtom> parse_outcome_group        (const std::string& str) const =0;
//     virtual SymbolicRulePtr   parse_symbolic_rule        (const std::string& str) const =0;
    virtual Typed::RuleSetPtr        parse_symbolic_ruleset     (const std::string& str) const =0;
    virtual State             parse_state                (const std::string& str) const =0;
    virtual std::vector<PPDDL::FullObject::Type> parse_pddl_types           (const std::string& str) const =0;
    virtual RewardFunctionGroup parse_global_goals         (const std::string& str) const =0;
    virtual PPDDLObjectManager<PPDDL::FullObject> parse_objects_from_simple_string(const std::string& str) const =0;

private:
    
};


class FileReader
{
public:
    explicit FileReader(const std::string& file_path_) :
        input_file_path(file_path_)
    { };
    
    ~FileReader() {
        if (input_file.is_open()) {
            input_file.close();
        }
    };
    
    /**
     * \brief Read a rule set
     */
    virtual void read_ruleset(Typed::RuleSetPtr& ruleset) =0;
    
    /**
     * \brief Read the domain types
     */
    virtual void read_domain_types(std::vector<PPDDL::FullObject::Type>& types) =0;
    
    /**
     * \brief Read the state and goals
     */
    virtual void read_state_and_goals(State& state, RewardFunctionGroup& goals) =0;
    
    /**
     * \brief Read a set of transitions
     */
    virtual void read_transitions(TransitionGroup<TypedAtom>& transitions) =0;
    
    
protected:
//     FileParser base_parser;
    std::string input_file_path;
    std::ifstream input_file;
    
    void open_file()
    {
        input_file.open(input_file_path.c_str());
    }
    
    boost::optional<std::string> read_line()
    {
        boost::optional<std::string> result;
        if (input_file.is_open()) {
            std::string line;
            if (input_file.good()) {
                getline(input_file, line);
                result = line;
            }
        }
        return result;
    }
    
private:
    
    
};

#endif // READER_BASE_H
