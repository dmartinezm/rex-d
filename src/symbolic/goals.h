/*
 * Clases representing predicates and goals.
 * Copyright (C) 2016  David Martínez (dmartinez@iri.upc.edu)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef IRI_RULE_LEARNER_GOALS_H
#define IRI_RULE_LEARNER_GOALS_H 1


#include <boost/lexical_cast.hpp>

#include <symbolic/state.h>

class RewardFunctionGroup;

class RewardFunction
{
public:
    typedef boost::shared_ptr<RewardFunction> Ptr;
    
    /** \brief returns the reward corresponding to the given state */
    virtual float obtain_reward(const State& state, const TypedAtom& action) const =0;
    
    /** \brief true state satisfies the reward function target */
    virtual bool is_goal(const State& state) const =0;
    
    virtual PredicateGroup<TypedAtom> get_predicates() const =0;
    
    virtual void change_param_name(const std::string& orig_var, const std::string& new_var) =0;
    
    virtual Ptr make_shared() const =0;
    
    /** \brief Gets the predicates that provide positive reward (or avoid getting negative reward)
     *
     * \return A set of predicates which give positive rewards or avoid negative rewards
     */
    virtual PredicateGroup<TypedAtom> get_predicates_that_contribute_to_goal() const =0;
    
    virtual void write_PPDDL(std::ostream& out, const std::vector< PPDDL::FullObject >& rule_params) const =0;
    
    virtual void write_PPDDL_problem(std::ostream& out) const =0;
    
    virtual void write_RDDL(std::ostream& out) const =0;
    
    virtual void PPDDL_grounding() =0;
    
    virtual void modify_reward(const float modification) =0;
    
    /**
     * \brief returns all grounded goals
     * 
     * symbolic_params are grounded with grounded_params, while all other params are grounded to each of in the list_of_objects
     */
    virtual RewardFunctionGroup get_pddl_grounded_goals(const std::vector<std::string>& symbolic_params, 
                                                        const std::vector<std::string>& grounded_params, 
                                                        const PPDDLObjectManager<PPDDL::FullObject>& object_manager) const =0;
                                                        
    virtual bool is_equal(const RewardFunction& other) const =0;
};


class Goal : public RewardFunction
{
private:
    float reward;
    PredicateGroup<TypedAtom> goal_predicates;
    
public:
    typedef boost::shared_ptr<Goal> Ptr;
    
    /**
     * Constructors
     */
    explicit Goal(PredicateGroup<TypedAtom> goal_predicates_, float reward_ = 1.0) :
        reward(reward_),
        goal_predicates(goal_predicates_)
    { }
    
    Goal(const Goal& other):
        reward(other.reward),
        goal_predicates(other.goal_predicates)
    { }
    
    explicit Goal(const std::string& goal_string);
    
    /*
     * Virtual methods
     */
    
    float get_internal_reward() const { return reward; }
    
    /** \brief returns the goal reward  if predicates satisfies the PredicateGroup */
    virtual float obtain_reward(const State& state, const TypedAtom& action) const;
    
    /** \brief true state satisfies the PredicateGroup */
    virtual bool is_goal(const State& state) const {
        return goal_predicates.is_satisfied_by(state);
    }
    
    /** \brief get predicates */
    virtual PredicateGroup<TypedAtom> get_predicates() const {return goal_predicates; };
    
    virtual RewardFunction::Ptr make_shared() const { return RewardFunction::Ptr(new Goal(*this)); }
    
    /*! \brief Change variable name */
    virtual void change_param_name(const std::string& orig_var, const std::string& new_var);
    
    virtual PredicateGroup<TypedAtom> get_predicates_that_contribute_to_goal() const;
    
    /** \brief do ppddl grounding */
    virtual void PPDDL_grounding() { goal_predicates.PPDDL_grounding(); }
    
    virtual void modify_reward(const float modification) { reward += modification; }
    
    /**
     * \brief returns all grounded goals
     * 
     * symbolic_params are grounded with grounded_params, while all other params are grounded to each of in the list_of_objects
     */
    virtual RewardFunctionGroup get_pddl_grounded_goals(const std::vector<std::string>& symbolic_params, 
                                                        const std::vector<std::string>& grounded_params, 
                                                        const PPDDLObjectManager<PPDDL::FullObject>& object_manager) const;
    
    /*
     * Goal specific methods
     */
    
    bool empty() const { return goal_predicates.empty(); }
    
    void add_predicates(const PredicateGroup<TypedAtom>& predicates)
        { goal_predicates.add_predicates(predicates); };

    virtual void write_PPDDL(std::ostream& out, const std::vector< PPDDL::FullObject >& rule_params) const;
    virtual void write_PPDDL_problem(std::ostream& out) const;
    virtual void write_RDDL(std::ostream& out) const { throw std::logic_error("Goal::writeRDDL() Not implemented!"); };
    
    virtual bool is_equal(const RewardFunction& other) const;
    friend bool operator==(const Goal &l, const Goal &r);
};


class RDDLGoalComponent {
public:
    typedef boost::shared_ptr<RDDLGoalComponent> Ptr;
    
    virtual float get_reward(const State& state, const TypedAtom& action) const =0;
    virtual RDDLGoalComponent::Ptr make_shared() const =0;
    virtual PredicateGroup<TypedAtom> get_predicates() const =0;
    virtual PredicateGroup<TypedAtom> get_predicates_that_contribute_to_goal() const =0;
    virtual void change_param_name(const std::string& orig_var, const std::string& new_var) =0;
    virtual void change_param_type(const std::string& orig_var, const std::string& new_type) =0;
    virtual void write_RDDL(std::ostream& out) const =0;
};

class RDDLGoal : public RewardFunction
{
public:
    RDDLGoal(const RDDLGoalComponent::Ptr& goal_component_ptr) : goal_component_ptr_(goal_component_ptr->make_shared()) {}
    RDDLGoal(const RDDLGoal& other) : goal_component_ptr_(other.goal_component_ptr_->make_shared()) {}
    
//     static RDDLGoal read_goal_from_string(const std::string& rddl_str, const PPDDLObjectManager& object_manager);
    
    virtual float obtain_reward(const State& state, const TypedAtom& action) const;
    
    virtual bool is_goal(const State& state) const;
    
    virtual PredicateGroup<TypedAtom> get_predicates() const;
    
    virtual void change_param_name(const std::string& orig_var, const std::string& new_var);
    
    virtual RewardFunction::Ptr make_shared() const { return RewardFunction::Ptr(new RDDLGoal(*this)); }
    
    virtual PredicateGroup<TypedAtom> get_predicates_that_contribute_to_goal() const;
    
    virtual void write_PPDDL(std::ostream& out, const std::vector< PPDDL::FullObject >& rule_params) const;
    
    virtual void write_PPDDL_problem(std::ostream& out) const;
    
    virtual void PPDDL_grounding();
    
    virtual void write_RDDL(std::ostream& out) const { goal_component_ptr_->write_RDDL(out); };
    
    virtual void modify_reward(const float modification);
    
    virtual RewardFunctionGroup get_pddl_grounded_goals(const std::vector<std::string>& symbolic_params, 
                                                        const std::vector<std::string>& grounded_params, 
                                                        const PPDDLObjectManager<PPDDL::FullObject>& object_manager) const;
                                                        
    RDDLGoalComponent::Ptr get_goal_component() const { return goal_component_ptr_; }
    
    void add_simple_reward(const PredicateGroup<TypedAtom>& preconditions, const float reward_value);
    
    virtual bool is_equal(const RewardFunction& other) const;
    friend bool operator==(const RDDLGoal &l, const RDDLGoal &r);
                                                        
private:
    RDDLGoalComponent::Ptr goal_component_ptr_;
};


class RewardFunctionGroup
{
private:
    std::vector<RewardFunction::Ptr> reward_function_v;
    
public:
    typedef std::vector<RewardFunction::Ptr>::iterator iterator;
    typedef std::vector<RewardFunction::Ptr>::const_iterator const_iterator;
    
    iterator        begin()         { return reward_function_v.begin(); };
    const_iterator  begin() const   { return reward_function_v.begin(); };
    iterator        end()           { return reward_function_v.end(); };
    const_iterator  end() const     { return reward_function_v.end(); };
    
    RewardFunctionGroup() {};
    RewardFunctionGroup(const RewardFunctionGroup& other);
    
    float obtain_reward(const State& state, const TypedAtom& action) const;
    bool is_goal(const State& state) const;
    
    void add_reward_function(const RewardFunction& reward_function) {
        reward_function_v.push_back(reward_function.make_shared());
    }
    
    void add_reward_functions(const RewardFunctionGroup& goals) {
        reward_function_v.insert(reward_function_v.end(), goals.begin(), goals.end());
    }
    
    bool empty() const { return reward_function_v.empty(); };
    
    size_t size() const { return reward_function_v.size(); };
    
    /** \brief get predicates */
    PredicateGroup<TypedAtom> get_predicates() const;
    
    /** \brief Adapts a goal defined by a finish function to a goal defined by a set of predicates giving a positive reward when satisfied
     *
     * DEPRECATED
     * Just used for backward compatibility.
     * Gets a set of goals which are the rewards obtained by a finish function.\n
     *  - Ex: [+1.5 onTable(1), -1.0 free(2)]
     * \return A set of predicates which give positive rewards
     */
    PredicateGroup<TypedAtom> get_predicates_that_contribute_to_goal() const;
    
    void modify_one_reward(const float modification) { if (!empty()) { reward_function_v.front()->modify_reward(modification); } }
    
    /**
     * \brief returns all grounded goals
     * 
     * symbolic_params are grounded with grounded_params, while all other params are grounded to each of in the list_of_objects
     */
    RewardFunctionGroup get_pddl_grounded_goals(const std::vector< std::string >& symbolic_params, 
                                                const std::vector< std::string >& grounded_params, 
                                                const PPDDLObjectManager<PPDDL::FullObject>& pddl_object_manager) const;
    
    /*!
     \brief Change variable name
     */
    void change_param_name(const std::string& orig_var, const std::string& new_var);

    void PPDDL_grounding();
    
    void write_PPDDL(std::ostream& out, const std::vector< PPDDL::FullObject >& rule_params) const;
    void write_PPDDL_problem(std::ostream& out) const;
    
    void write_RDDL(std::ostream& out) const;
    
    friend bool operator==(const RewardFunctionGroup &l, const RewardFunctionGroup &r);
    friend bool operator!=(const RewardFunctionGroup &l, const RewardFunctionGroup &r);
};



#endif

