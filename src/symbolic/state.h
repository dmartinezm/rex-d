/*
 * Class representing a state (as a set of predicates).
 * Copyright (C) 2016  David Martínez (dmartinez@iri.upc.edu)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef INTELLACT_STATE_H
#define INTELLACT_STATE_H

#include <iterator>
#include "symbolic/predicates.h"

/*!
\class State
\brief Class defining the current scenario state.

At this moment it is just a PredicateGroup. In the future it may be enriched with partial observability.
*/
class State : public PredicateGroup<TypedAtom>
{
private:
    PredicateGroup<TypedAtom> constant_predicates;
    
public:
    PPDDLObjectManager<PPDDL::FullObject> ppddl_object_manager;
    
    State() {};
    explicit State(const std::string& state_string): PredicateGroup<TypedAtom>(state_string) {};
    State(const PredicateGroup<TypedAtom>& pred_group, const PPDDLObjectManager<PPDDL::FullObject>& pddl_object_manager): 
        PredicateGroup<TypedAtom>(pred_group),
        ppddl_object_manager(pddl_object_manager)
    {};
        
    void clear();
    
    void clear_nonconstant_predicates();
    
    void set_constants(const PredicateGroup<TypedAtom>& new_constant_predicates);
    const PredicateGroup<TypedAtom>& get_constants() const { return constant_predicates; };
    PredicateGroup<TypedAtom> get_non_constants() const;

//     void read_from_file(std::string file_path);

    void write_to_file(std::string file_path) const;
    
    void update_predicate(Predicate<TypedAtom> new_predicate);
    
    /*!
    \brief Removes special predicates such as +-finished() and +-started()
    */
    void remove_special_predicates();

private:
    
    
};

typedef boost::shared_ptr<State> StatePtr;
typedef boost::shared_ptr<const State> StateConstPtr;


#endif
