#include "domain.h"

/**
 * OPERATORS
 *
 */

std::ostream& operator<<(std::ostream &out, const Domain& dom)
{
    out << "Domain::" << "<<" << " - " << "Domain is :\n";
    for (size_t i = 0; i < dom.known_predicates.size(); i++)
        out << "Domain::" << "<<" << " - " << "Predicate<TypedAtom> " << i << ": " << dom.known_predicates[i] << "\n";

    return out;
}

/**
 * Domain
 *
 */

Domain::Domain(const ConfigReader& config_reader):
    generalize_first_action_param(config_reader.get_variable<bool>("ppddl_rules_generalize_first_param")),
    generalize_second_action_param(config_reader.get_variable<bool>("ppddl_rules_generalize_second_param"))
{
    index_action(MAKE_TYPED_ATOM_NOACTION); // Indexed learner requires NOACTION to be the first one
}

template<class T>
int Domain::index_item(const T& item, std::vector<T>& known_items)
{
    int idx;
    bool found = false;
    for (size_t i = 0; i < known_items.size(); i++) {
        if (item == known_items[i]) {
            idx = i;
            found = true;
        }
    }
    if (!found) {
        idx = known_items.size();
        known_items.push_back(item);
    }
    return idx;
}

template<class T>
int Domain::get_item_index(const T& item, const std::vector<T>& known_items) const
{
    for (size_t i = 0; i < known_items.size(); i++) {
        if (item == known_items[i]) {
            return i;
        }
    }
    return -1;
}

std::vector< TypedAtom > Domain::get_non_special_actions() const {
    std::vector<TypedAtom> non_special_action_list;
    BOOST_FOREACH(const TypedAtom& action, get_actions()) {
        if (!action.is_special()) {
            non_special_action_list.push_back(action);
        }
    }
    return non_special_action_list;
}

std::vector< TypedAtom > Domain::get_plannable_actions() const {
    std::vector<TypedAtom> planificable_action_list;
    BOOST_FOREACH(const TypedAtom& action, get_actions()) {
        if (action.is_plannable()) {
            planificable_action_list.push_back(action);
        }
    }
    return planificable_action_list;
}


std::vector< TypedAtom > Domain::get_executable_actions() const {
    std::vector<TypedAtom> executable_action_list;
    BOOST_FOREACH(const TypedAtom& action, get_actions()) {
        if (action.is_executable()) {
            executable_action_list.push_back(action);
        }
    }
    return executable_action_list;
}

void Domain::index_state(const State& state)
{
    index_predicates(state);
    index_objects(state.ppddl_object_manager.get_ppddl_objects());
}

void Domain::index_objects(const std::set< PPDDL::FullObject >& ppddl_objects_per_type)
{
    BOOST_FOREACH(const PPDDL::FullObject& object, ppddl_objects_per_type) {
        index_item<PPDDL::FullObject>(object, known_objects);
    }
}

void Domain::index_predicates(const PredicateGroup<TypedAtom>& predicates)
{
    for (PredicateGroup<TypedAtom>::const_iterator predicate = predicates.begin(); predicate != predicates.end(); ++predicate) {
        index_predicate(*predicate);
    }
}

void Domain::index_goals(const RewardFunctionGroup& goals)
{
    // TODO Disabled as goals can now be either predicates or actions (do something about it in the future)
//     index_predicates(goals.get_predicates());
}

void Domain::index_transitions(const TransitionGroup<TypedAtom>& transitions)
{
    for (TransitionSet<TypedAtom>::const_iterator trans_it = transitions.begin_set(); trans_it != transitions.end_set(); ++trans_it) {
        index_predicates(trans_it->get_prev_state());
        index_predicates(trans_it->get_post_state());
        index_action(trans_it->get_action());
    }
}

int Domain::index_type(const PPDDL::FullObject::Type& ppddl_type)
{
    return index_item<PPDDL::FullObject::Type>(ppddl_type, known_types);
}

int Domain::index_atom(const TypedAtom& symbol)
{
    int id = index_item<TypedAtom>(symbol, known_atoms);
    TypedAtom symbolic_atom(symbol);
    symbolic_atom.make_symbolic();
    index_item<TypedAtom>(symbolic_atom, known_symbolic_atoms);

    // add objects if GroundedPredicate
    if (!symbol.is_symbolic()) {
        BOOST_FOREACH(const PPDDL::FullObject& param, symbol.params) {
            index_item<PPDDL::FullObject>(param, known_objects);
            index_item<PPDDL::FullObject::Type>(param.type, known_types);
        }
    }
    return id;
}

int Domain::index_predicate(const Predicate<TypedAtom>& predicate)
{
    // Also index symbolic predicate
    Predicate<TypedAtom> sym_predicate(predicate);
    sym_predicate.make_symbolic();
    sym_predicate.positive = true;
    index_item<Predicate<TypedAtom> >(sym_predicate, known_symbolic_predicates);
    
    int id = index_item<Predicate<TypedAtom> >(predicate, known_predicates);
    // also index the symbol
    index_atom(predicate);

    // add objects if GroundedPredicate
    if (!predicate.is_symbolic()) {
        BOOST_FOREACH(const PPDDL::FullObject& param, predicate.params) {
            index_item<PPDDL::FullObject>(param, known_objects);
        }
    }
    return id;
}

int Domain::index_action(const TypedAtom& action)
{
    TypedAtom generic_action(action);
    generic_action.make_symbolic();
    int res_index =  index_item<TypedAtom>(generic_action, known_actions);
    BOOST_FOREACH(const PPDDL::FullObject& param, action.params) {
        index_type(param.type);
    }
    
    if (generalize_first_action_param && (generic_action.params.size() >= 1) ) {
        BOOST_FOREACH(PPDDL::FullObject::Type& pddl_type, known_types) {
            generic_action.params[0].type = pddl_type;
            index_item<TypedAtom>(generic_action, known_actions);
        }
    }
    
    if (generalize_second_action_param && (generic_action.params.size() >= 2) ) {
        BOOST_FOREACH(PPDDL::FullObject::Type& pddl_type, known_types) {
            generic_action.params[1].type = pddl_type;
            index_item<TypedAtom>(generic_action, known_actions);
        }
    }
    
    return res_index;
}

void Domain::index_rules(const RuleSet<TypedAtom>& rules)
{
    BOOST_FOREACH(const RulePtr<TypedAtom>& rule_ptr, rules) {
        index_rule(*rule_ptr);
    }
}

void Domain::index_rule(const Rule<TypedAtom>& rule)
{
    index_action(rule);
    index_predicates(rule.get_preconditions());
    BOOST_FOREACH(const Outcome<TypedAtom>& out_it, rule.outcomes) {
        index_predicates(out_it);
    }
}


TransitionGroup<TypedAtom> Domain::get_transitions_with_indexed_objects(const TransitionGroup<TypedAtom>& transitions)
{
    TransitionGroup<TypedAtom> indexed_transitions(transitions.ppddl_object_manager);
    // TODO change ppddl_object_manager
    for ( TransitionHistorial<TypedAtom>::const_iterator transition_it_it = transitions.begin_historial(); 
          transition_it_it != transitions.end_historial();
          ++transition_it_it) 
          {
        Transition<TypedAtom> indexed_transition = get_transition_with_indexed_objects(**transition_it_it);
        indexed_transitions.add_transition(indexed_transition);
    }
    return indexed_transitions;
}

Transition<TypedAtom> Domain::get_transition_with_indexed_objects(const Transition<TypedAtom>& transition)
{
    return Transition<TypedAtom>(get_predicates_with_indexed_objects(transition.get_prev_state()),
                                 get_typed_atom_with_indexed_objects(transition.get_action()),
                                 get_predicates_with_indexed_objects(transition.get_post_state()),
                                 transition.get_reward(),
                                 true
                                );
}

PredicateGroup<TypedAtom> Domain::transform_to_typed(const PredicateGroup<NonTypedAtom>& non_typed_preds) const
{
    PredicateGroup<TypedAtom> result;
    BOOST_FOREACH(const Predicate<NonTypedAtom>& non_typed_pred, non_typed_preds) {
        result.insert(transform_to_typed(non_typed_pred));
    }
    return result;
}

Predicate<TypedAtom> Domain::transform_to_typed(const Predicate<NonTypedAtom>& non_typed_predicate) const
{
    return Predicate<TypedAtom>(transform_to_typed(static_cast<const NonTypedAtom>(non_typed_predicate)), non_typed_predicate.positive);
}

PPDDL::FullObject Domain::transform_object_to_typed(const PPDDL::IndexedObject& indexed_object)
{
    if (indexed_object.name < MAX_SYMBOLIC_PARAMS) {
        return PPDDL::FullObject(DEFAULT_PARAM_NAMES[indexed_object.name], get_type(indexed_object.type));
    }
    else {
        return get_object(indexed_object.name - MAX_SYMBOLIC_PARAMS);
    }
}

TypedAtom Domain::transform_atom_to_typed(const IndexedAtom& indexed_atom) {
//     LOG(INFO) << indexed_atom.name;
    TypedAtom res_atom = known_symbolic_atoms[indexed_atom.name];
//     LOG(INFO) << "Not Typed atom is " << res_atom;
    for (size_t i = 0; i < res_atom.params.size(); ++i) {
//         LOG(INFO) << "Indexed Object is " << indexed_atom.params[i].name;
        // Don't need to set types, they are already obtained from known_symbolic_atoms
        res_atom.params[i].name = transform_object_to_typed(indexed_atom.params[i]).name;
//         LOG(INFO) << "Typed Object is " << res_atom.params[i].name;
    }
//     LOG(INFO) << "Typed atom is " << res_atom;
    return res_atom;
}

TypedAtom Domain::transform_action_to_typed(const IndexedAtom& indexed_atom) {
    TypedAtom res_action = known_actions[indexed_atom.name];
    for (size_t i = 0; i < indexed_atom.params.size(); ++i) {
        // Don't need to set types, they are already obtained from known_symbolic_atoms
        res_action.params[i].name = transform_object_to_typed(indexed_atom.params[i]).name;
    }
    return res_action;
}

Predicate<TypedAtom> Domain::transform_predicate_to_typed(const Predicate<IndexedAtom>& indexed_predicate) {
    return Predicate<TypedAtom>(transform_atom_to_typed(indexed_predicate), indexed_predicate.positive);
}

PredicateGroup<TypedAtom> Domain::transform_predicate_group_to_typed(const PredicateGroup<IndexedAtom>& indexed_predicates) {
    PredicateGroup<TypedAtom> result;
    BOOST_FOREACH(const Predicate<IndexedAtom>& indexed_predicate, indexed_predicates) {
        result.insert(transform_predicate_to_typed(indexed_predicate));
    }
    return result;
}

Outcome<TypedAtom> Domain::transform_outcome_to_typed(const Outcome<IndexedAtom>& indexed_outcome)
{
    return Outcome<TypedAtom>(transform_predicate_group_to_typed(indexed_outcome), indexed_outcome.probability);
}

OutcomeGroup<TypedAtom> Domain::transform_outcome_group_to_typed(const OutcomeGroup<IndexedAtom>& indexed_outcomes)
{
    OutcomeGroup<TypedAtom> res_outcomes;
    BOOST_FOREACH(const Outcome<IndexedAtom>& indexed_outcome, indexed_outcomes) {
        res_outcomes.insert(transform_outcome_to_typed(indexed_outcome));
    }
    return res_outcomes;
}

SymbolicRule<TypedAtom> Domain::transform_symbolic_rule_to_typed(const SymbolicRule<IndexedAtom>& indexed_rule) {
    return SymbolicRule<TypedAtom>(transform_action_to_typed(indexed_rule),
                                   transform_predicate_group_to_typed(indexed_rule.get_preconditions()),
                                   transform_outcome_group_to_typed(indexed_rule.outcomes),
                                   indexed_rule.goals);
}

RuleSet<TypedAtom> Domain::transform_rule_set_to_typed(const RuleSet<IndexedAtom>& indexed_rules) {
    RuleSet<TypedAtom> ruleset;
    BOOST_FOREACH(const RulePtr<IndexedAtom>& rule_ptr, indexed_rules) {
        ruleset.push_back(transform_symbolic_rule_to_typed(*static_cast<SymbolicRule<IndexedAtom>*>(&(*rule_ptr))).make_shared());
    }
    return ruleset;
}

PPDDLObjectManager<PPDDL::FullObject> Domain::transform_object_manager_to_typed(const PPDDLObjectManager<PPDDL::IndexedObject>& indexed_object_manager) {
    PPDDLObjectManager<PPDDL::FullObject> typed_object_manager;
    BOOST_FOREACH(const PPDDL::IndexedObject& indexed_object, indexed_object_manager.get_ppddl_objects()) {
        typed_object_manager.add_object(transform_object_to_typed(indexed_object));
    }
    return typed_object_manager;
}

Transition<TypedAtom> Domain::transform_transition_to_typed(const Transition<IndexedAtom>& indexed_transition) {
    return Transition<TypedAtom>(transform_predicate_group_to_typed(indexed_transition.get_prev_state()),
                                 transform_action_to_typed(indexed_transition.get_action()),
                                 transform_predicate_group_to_typed(indexed_transition.get_post_state()),
                                 indexed_transition.get_reward(),
                                 true
                                );
}

TransitionGroup<TypedAtom> Domain::transform_transition_group_to_typed(const TransitionGroup<IndexedAtom>& indexed_transitions) {
    TransitionGroup<TypedAtom> res_transitions(transform_object_manager_to_typed(indexed_transitions.ppddl_object_manager));
    for (TransitionHistorial<IndexedAtom>::const_iterator indexed_transition_it = indexed_transitions.begin_historial(); 
                                                          indexed_transition_it != indexed_transitions.end_historial(); 
                                                          ++ indexed_transition_it) {
        res_transitions.add_transition(transform_transition_to_typed(**indexed_transition_it));
    }
    return res_transitions;
}

PPDDL::IndexedObject Domain::transform_object_to_indexed(const PPDDL::FullObject& typed_object) const
{
    if (symbolic::is_param_symbolic(typed_object.name)) {
        return PPDDL::IndexedObject(symbolic::get_symbolic_param_index(typed_object.name), get_type_index(typed_object.type));
    }
    else {
        return PPDDL::IndexedObject(get_object_index(typed_object) + MAX_SYMBOLIC_PARAMS, get_type_index(typed_object.type));
    }
}

IndexedAtom Domain::transform_atom_to_indexed(const TypedAtom& typed_atom) const
{
    std::vector<IndexedAtom::ParamType> indexed_params;
    BOOST_FOREACH(const PPDDL::FullObject& param, typed_atom.params) {
        indexed_params.push_back(transform_object_to_indexed(param));
    }
    TypedAtom symbolic_atom = typed_atom;
    symbolic_atom.make_symbolic();
    if (get_atom_symbolic_index_const(symbolic_atom) > 1000) {
        LOG(ERROR) << "Wrong atom when indexing " << symbolic_atom;
    }
    return IndexedAtom(get_atom_symbolic_index_const(symbolic_atom), indexed_params);
}

IndexedAtom Domain::transform_action_to_indexed(const TypedAtom& typed_action) const
{
    std::vector<IndexedAtom::ParamType> indexed_params;
    BOOST_FOREACH(const PPDDL::FullObject& param, typed_action.params) {
        indexed_params.push_back(transform_object_to_indexed(param));
    }
    TypedAtom symbolic_action = typed_action;
    symbolic_action.make_symbolic();
    if (get_atom_symbolic_index_const(symbolic_action) < 0) {
        LOG(ERROR) << "Wrong atom when indexing " << symbolic_action;
    }
    return IndexedAtom(get_action_symbolic_index_const(symbolic_action), indexed_params);
}

Predicate<IndexedAtom> Domain::transform_predicate_to_indexed(const Predicate<TypedAtom>& predicate) const
{
    return Predicate<IndexedAtom>(transform_atom_to_indexed(predicate), predicate.positive);
}

PredicateGroup<IndexedAtom> Domain::transform_predicate_group_to_indexed(const PredicateGroup<TypedAtom>& pred_group) const
{
    PredicateGroup<IndexedAtom> indexed_predicates;
    BOOST_FOREACH(const Predicate<TypedAtom>& predicate, pred_group) {
        indexed_predicates.insert(transform_predicate_to_indexed(predicate));
    }
    return indexed_predicates;
}

Outcome<IndexedAtom> Domain::transform_outcome_to_indexed(const Outcome<TypedAtom>& typed_outcome) const
{
    return Outcome<IndexedAtom>(transform_predicate_group_to_indexed(typed_outcome), typed_outcome.probability);
}

OutcomeGroup<IndexedAtom> Domain::transform_outcome_group_to_indexed(const OutcomeGroup<TypedAtom>& typed_outcomes) const
{
    OutcomeGroup<IndexedAtom> res_outcomes;
    BOOST_FOREACH(const Outcome<TypedAtom>& typed_outcome, typed_outcomes) {
        res_outcomes.insert(transform_outcome_to_indexed(typed_outcome));
    }
    return res_outcomes;
}

SymbolicRule<IndexedAtom> Domain::transform_symbolic_rule_to_indexed(const SymbolicRule<TypedAtom>& typed_rule) const {
    return SymbolicRule<IndexedAtom>(transform_action_to_indexed(typed_rule),
                                     transform_predicate_group_to_indexed(typed_rule.get_preconditions()),
                                     transform_outcome_group_to_indexed(typed_rule.outcomes),
                                     typed_rule.goals);
}

PPDDLObjectManager<PPDDL::IndexedObject> Domain::transform_object_manager_to_indexed(const PPDDLObjectManager<PPDDL::FullObject>& typed_object_manager) const {
    PPDDLObjectManager<PPDDL::IndexedObject> indexed_object_manager;
    BOOST_FOREACH(const PPDDL::FullObject& typed_object, typed_object_manager.get_ppddl_objects()) {
        indexed_object_manager.add_object(transform_object_to_indexed(typed_object));
    }
    return indexed_object_manager;
}

Transition<IndexedAtom> Domain::transform_transition_to_indexed(const Transition<TypedAtom>& typed_transition) const {
    return Transition<IndexedAtom>(transform_predicate_group_to_indexed(typed_transition.get_prev_state()),
                                   transform_action_to_indexed(typed_transition.get_action()),
                                   transform_predicate_group_to_indexed(typed_transition.get_post_state()),
                                   typed_transition.get_reward(),
                                   true);
}

TransitionGroup<IndexedAtom> Domain::transform_transition_group_to_indexed_unordered(const TransitionGroup<TypedAtom>& typed_transitions) const {
    TransitionGroup<IndexedAtom> res_transitions(transform_object_manager_to_indexed(typed_transitions.ppddl_object_manager));
    for (TransitionSet<TypedAtom>::const_iterator typed_transition_it = typed_transitions.begin_set(); 
                                                  typed_transition_it != typed_transitions.end_set(); 
                                                  ++ typed_transition_it) {
        res_transitions.add_transition(transform_transition_to_indexed(*typed_transition_it), typed_transition_it->get_number_repetitions());
    }
    return res_transitions;
}

TypedAtom Domain::transform_to_typed(const NonTypedAtom& non_typed_atom) const
{
    TypedAtom typed_atom(TypedAtom(non_typed_atom.name, std::vector<PPDDL::FullObject>()));
    bool done = false;
    // First check known symbols
    BOOST_FOREACH(const TypedAtom& known_atom, known_symbolic_atoms) {
        if (non_typed_atom.name == known_atom.name) {
            assert(non_typed_atom.params.size() == known_atom.params.size());
            for (size_t i = 0; i < non_typed_atom.params.size(); ++i) {
                typed_atom.params.push_back(PPDDL::FullObject(non_typed_atom.params[i].name, known_atom.params[i].type));
            }
            done = true;
            break;
        }
    }
    if (!done) {
        // Else check known actions
        BOOST_FOREACH(const TypedAtom& known_atom, known_actions) {
            if (non_typed_atom.name == known_atom.name) {
                assert(non_typed_atom.params.size() == known_atom.params.size());
                for (size_t i = 0; i < non_typed_atom.params.size(); ++i) {
                    typed_atom.params.push_back(PPDDL::FullObject(non_typed_atom.params[i].name, known_atom.params[i].type));
                }
                done = true;
                break;
            }
        }
    }
    if (!done) {
        // Else set default type
        for (size_t i = 0; i < non_typed_atom.params.size(); ++i) {
            typed_atom.params.push_back(PPDDL::FullObject(non_typed_atom.params[i].name, DEFAULT_OBJECT_TYPE));
        }
    }
    return typed_atom;
}

PredicateGroup<TypedAtom> Domain::get_predicates_with_indexed_objects(const PredicateGroup<TypedAtom>& predicates)
{
    PredicateGroup<TypedAtom> indexed_predicates;
    BOOST_FOREACH(const Predicate<TypedAtom>& predicate, predicates) {
        indexed_predicates.insert(get_predicate_with_indexed_objects(predicate));
    }
    return indexed_predicates;
}

Predicate<TypedAtom> Domain::get_predicate_with_indexed_objects(const Predicate<TypedAtom>& predicate)
{
    return Predicate<TypedAtom>(get_typed_atom_with_indexed_objects(predicate), predicate.positive);
}

TypedAtom Domain::get_typed_atom_with_indexed_objects(const TypedAtom& symbol)
{
    TypedAtom indexed_atom(symbol);
    BOOST_FOREACH(PPDDL::FullObject& param, indexed_atom.params) {
        // TODO UGLY HACK !!!
        param.name = boost::lexical_cast<std::string>(get_object_index(param) + 60);
    }
    return indexed_atom;
}

size_t Domain::get_atom_index(const TypedAtom& symbol)
{
    int id = get_item_index(symbol, known_atoms);
    if (id < 0) { // not found
        id = index_atom(symbol);
    }
    return id;
}

size_t Domain::get_atom_symbolic_index_const(const TypedAtom& symbol) const
{
    // TODO WARNING use throw catch better than return special values
    // NOTE THAT it returns uint, but error code is -1
    return get_item_index(symbol, known_symbolic_atoms);
}

size_t Domain::get_predicate_index(const Predicate<TypedAtom>& predicate)
{
    Predicate<TypedAtom> sym_predicate(predicate);
    sym_predicate.make_symbolic();
    int id = get_item_index(predicate, known_predicates);
    if (id < 0) { // not found
        id = index_predicate(predicate);
    }
    return id;
}

size_t Domain::get_predicate_index_const(const Predicate<TypedAtom>& predicate) const
{
    Predicate<TypedAtom> sym_predicate(predicate);
    sym_predicate.make_symbolic();
    int id = get_item_index(predicate, known_predicates);
    return id;
}

std::vector<size_t> Domain::get_predicate_group_indexes_const(const PredicateGroup<TypedAtom>& predicates) const
{
    std::vector<size_t> result;
    BOOST_FOREACH(const Predicate<TypedAtom>& predicate, predicates) {
        result.push_back(get_predicate_index_const(predicate));
    }
    return result;
}

size_t Domain::get_action_symbolic_index(const TypedAtom& action)
{
    int id = get_item_index(action, known_actions);
    if (id < 0) { // not found
        id = index_action(action);
    }
    return id;
}

size_t Domain::get_action_symbolic_index_const(const TypedAtom& action) const
{
    return get_item_index(action, known_actions);
}

size_t Domain::get_type_index(const PPDDL::FullObject::Type& type) const
{
    return get_item_index(type, known_types);
}

size_t Domain::get_object_index(const PPDDL::FullObject& object) const
{
    return get_item_index(object, known_objects);
}

PredicateGroup<TypedAtom> Domain::get_constant_predicates() const
{
    PredicateGroup<TypedAtom> result;
    BOOST_FOREACH(const Predicate<TypedAtom>& known_predicate, known_predicates) {
        if (known_predicate.is_constant()) {
            result.insert(known_predicate);
        }
    }
    return result;
}

void Domain::make_predicate_group_no_closed_world(PredicateGroup<TypedAtom>& predicates, const bool symbolic) const
{
    std::set< Predicate<TypedAtom> > predicates_to_add;
    if (symbolic) {
        PPDDLObjectManager<PPDDL::FullObject> ppddl_object_manager;
        ppddl_object_manager.add_objects(known_objects.begin(), known_objects.end());
        std::set< Predicate<TypedAtom> > symbolic_predicates_to_add;
        BOOST_FOREACH(Predicate<TypedAtom> copy_predicate, known_predicates) {
            copy_predicate.make_symbolic();
            symbolic_predicates_to_add.insert(copy_predicate);
        }
        BOOST_FOREACH(Predicate<TypedAtom> symbolic_predicate, symbolic_predicates_to_add) {
            PPDDLObjectManager<PPDDL::FullObject>::CombinationsPPDDLObjectsPtr combinations_of_params =
                        ppddl_object_manager.get_combinations_of_objecs_for_given_params(symbolic_predicate.get_param_types());
            // get all groundings
            
            BOOST_FOREACH(const std::vector< std::string >& objects, *combinations_of_params) {
                std::vector<PPDDL::FullObject> new_params(symbolic_predicate.params);
                for (size_t i = 0; i < new_params.size(); ++i) {
                    new_params[i].name = objects[i];
                }
                Predicate<TypedAtom> new_predicate(symbolic_predicate);
                new_predicate.params = new_params;
                predicates_to_add.insert(new_predicate);
            }
        }
    }
    else {
        predicates_to_add.insert(known_predicates.begin(), known_predicates.end());
    }
    BOOST_FOREACH(const Predicate<TypedAtom>& predicate, predicates) {
        std::set< Predicate<TypedAtom> >::iterator pred_found_iterator;
        pred_found_iterator = predicates_to_add.find(predicate);
        if (pred_found_iterator != predicates_to_add.end()) {
            predicates_to_add.erase(pred_found_iterator);
        }
        pred_found_iterator = predicates_to_add.find(predicate.get_negated());
        if (pred_found_iterator != predicates_to_add.end()) {
            predicates_to_add.erase(pred_found_iterator);
        }
    }
    
    BOOST_FOREACH(Predicate<TypedAtom> predicate_to_add, predicates_to_add) {
        predicate_to_add.positive = false;
        predicates.insert(predicate_to_add);
    }
}

TransitionGroup<TypedAtom> Domain::get_transitions_no_closed_world_unordered(const TransitionGroup<TypedAtom>& symbolic_transitions) const
{
    TransitionGroup<TypedAtom> result(symbolic_transitions.ppddl_object_manager);
    
    for ( TransitionSet<TypedAtom>::const_iterator transition_it = symbolic_transitions.begin_set(); 
          transition_it != symbolic_transitions.end_set();
          ++transition_it) 
          {
        PredicateGroup<TypedAtom> prev_state = transition_it->get_prev_state();
        make_predicate_group_no_closed_world(prev_state);
        PredicateGroup<TypedAtom> post_state = transition_it->get_post_state();
        make_predicate_group_no_closed_world(post_state);
        Transition<TypedAtom> new_transition(prev_state,
                                             transition_it->get_action(),
                                             post_state,
                                             transition_it->get_reward(),
                                             true
                                            );
        result.add_transition(new_transition, transition_it->get_number_repetitions());
    }
    return result;
}

void Domain::write_LFIT_predicate_types(std::ostream& out) const
{
    std::set<TypedAtom> predicate_atom_set; // to write domain we don't need sign(+/-). Predicate<TypedAtom> is just a symbol with a sign(+/-)
    BOOST_FOREACH(const Predicate<TypedAtom> & predicate, known_predicates) {
        predicate_atom_set.insert(predicate);
    }
    
    /*
     * See below
    PredicateGroup<TypedAtom> constant_predicates = get_constant_predicates();
    constant_predicates.make_symbolic();
    */

    BOOST_FOREACH(TypedAtom symbol, predicate_atom_set) {
        /*
         * This should be the proper way
         *   Problem is, generating random transitions would be more complicated
         *   There are grounded literals that are constant, but the corresponding symbolic literal is not constant
        
        bool is_constant = false;
        BOOST_FOREACH(const Predicate<TypedAtom>& constant_predicate, constant_predicates) {
            if (constant_predicate.name == symbol.name) {
                is_constant = true;
            }
        }
        if (is_constant) {
            out << "VAR* ";
        }
        else {
            out << "VAR ";
        }
        */
        
        // HACK, using uppercase to identify constant symbolic literals
        if (isupper(symbol.name[0])) { // constant
            out << "VAR* ";
        }
        else {
            out << "VAR ";
        }
        out << symbol.name;
        out << " 0 1";
        out << std::endl;
    }
    
    out << "VAR* action ";
    BOOST_FOREACH(TypedAtom symbol, get_non_special_actions()) {
        out << symbol.name << " ";
    }
    out << ACTION_NAME_NOACTION;
    out << std::endl;
}

void Domain::write_PPDDL_grounded_predicates(std::ostream& out) const
{
    std::set<TypedAtom> predicate_atom_set; // to write domain we don't need sign(+/-). Predicate<TypedAtom> is just a symbol with a sign(+/-)
    BOOST_FOREACH(const Predicate<TypedAtom> & predicate, known_predicates) {
        predicate_atom_set.insert(predicate);
    }

    out << "  (:predicates " << std::endl;
    BOOST_FOREACH(const TypedAtom & symbol, predicate_atom_set) {
        out << "    ";
        symbol.write_PPDDL(out);
        out << std::endl;
    }
    out << "  )" << std::endl;
}

void Domain::write_PPDDL_symbolic_predicates(std::ostream& out, const PPDDLObjectManager<PPDDL::FullObject>& pddl_object_manager) const
{
    std::set<TypedAtom> typed_atom_set; // to write domain we don't need sign(+/-). Predicate<TypedAtom> is just a symbol with a sign(+/-)
    BOOST_FOREACH(const Predicate<TypedAtom> & predicate, known_predicates) {
        TypedAtom typed_atom(predicate);
        typed_atom.make_symbolic();
        if ( (typed_atom.params.empty()) || (typed_atom.params.front().type != DEFAULT_OBJECT_TYPE) ) {
            // duplicates predicates because we don't know type for ?X or ?Y in some symbolic predicates
            typed_atom_set.insert(typed_atom);
        }
    }

    out << "  (:predicates ";
    BOOST_FOREACH(const TypedAtom & typed_atom, typed_atom_set) {
        out << "               ";
        typed_atom.write_PPDDL(out);
        out << std::endl;
    }
    out << "  )" << std::endl;
}

void Domain::write_RDDL_objects(std::ostream& out, const PPDDLObjectManager<PPDDL::FullObject>& pddl_object_manager) const
{
    out << "  types {" << std::endl;
    BOOST_FOREACH(const typename PPDDLObjectManager<PPDDL::FullObject>::TypeObjectsMap::value_type & pair_type_objects, pddl_object_manager.get_ppddl_objects_per_type()) {
        out << "    " << pair_type_objects.first << " : object;" << std::endl;
    }
    out << "  };" << std::endl;
}

void Domain::write_RDDL_pvariables(std::ostream& out, const PPDDLObjectManager<PPDDL::FullObject>& pddl_object_manager) const
{
    out << "  pvariables {" << std::endl;
    BOOST_FOREACH(const Predicate<TypedAtom> & predicate, known_symbolic_predicates) {
        if (predicate.is_constant()) {
            out << "    " << NonTypedAtom(predicate.name, predicate.get_param_types()) << " : {non-fluent, bool, default = false};" << std::endl;
        }
        else {
            out << "    " << NonTypedAtom(predicate.name, predicate.get_param_types()) << " : {state-fluent, bool, default = false};" << std::endl;
        }
    }
    BOOST_FOREACH(const TypedAtom& action, get_plannable_actions()) {
        out << "    " << NonTypedAtom(action.name, action.get_param_types()) << " : {action-fluent, bool, default = false};" << std::endl;
    }
    out << "  };" << std::endl;
}

void Domain::write_NID(std::ostream& out) const
{
    std::set<TypedAtom> predicate_atom_set, action_atom_set; // to write domain we don't need sign(+/-). Predicate<TypedAtom> is just a symbol with a sign(+/-)
    BOOST_FOREACH(const Predicate<TypedAtom> & predicate, known_symbolic_predicates) {
        predicate_atom_set.insert(predicate);
    }

    BOOST_FOREACH(const TypedAtom & action, get_plannable_actions()) {
        action_atom_set.insert(action);
    }

    out << "# predicates" << std::endl;
    BOOST_FOREACH(const TypedAtom & primitive, predicate_atom_set) {
        out << primitive.name << " " << primitive.params.size() << " primitive binary" << std::endl;
    }

    out << "# actions" << std::endl;
    BOOST_FOREACH(const TypedAtom & action, action_atom_set) {
        out << action.name << " " << action.params.size() << " action binary" << std::endl;
    }
}





