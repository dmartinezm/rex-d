/*
 * Class representing a logic atom with parameters.
 * Copyright (C) 2016  David Martínez (dmartinez@iri.upc.edu)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef IRI_RULE_LEARNER_ATOMS_H
#define IRI_RULE_LEARNER_ATOMS_H 1

#include <algorithm>
#include <vector>
#include <set>
#include <map>
#include <fstream>
#include <sstream>
#include <iostream>
#include <iomanip>

#include <boost/foreach.hpp>
#include <boost/optional.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/lexical_cast.hpp>

// Typed objects
#define DEFAULT_OBJECT_TYPE "default"

// special
#define MAKE_GROUNDED_NAME_ATOM_SEPARATOR  '_'

// action names
#define ACTION_NAME_NOACTION "noaction"
#define ACTION_NAME_NOOP "noop"
#define ACTION_NAME_FINISH "finish_action"
#define ACTION_NAME_SET_LITERAL_DEFAULT "setLiteralDefault"

#define PREDICATE_NAME_DIFFERENT_AUX "DifferentAux"
#define PREDICATE_NAME_GOAL_AUX "GOALaux"
#define PREDICATE_NAME_GOAL_REWARD_RECEIVED_AUX "goalRewardReceivedAux"

// actions
#define MAKE_NONTYPED_ATOM_DEAD          NonTypedAtom("dead()")
#define MAKE_NONTYPED_ATOM_RETRY         NonTypedAtom("retry()")
#define MAKE_NONTYPED_ATOM_HELP          NonTypedAtom("help()")
#define MAKE_NONTYPED_ATOM_DISABLE       NonTypedAtom("disable()")
#define MAKE_NONTYPED_ATOM_START_TEACHER NonTypedAtom("startTeacher()")
#define MAKE_NONTYPED_ATOM_STOP_TEACHER  NonTypedAtom("stopTeacher()")
#define MAKE_NONTYPED_ATOM_UNKNOWN       NonTypedAtom("unknownManipulation()")
#define MAKE_NONTYPED_ATOM_TEACHER       NonTypedAtom("teacher()")
#define MAKE_NONTYPED_ATOM_FINISH_ACTION NonTypedAtom(ACTION_NAME_FINISH)
#define MAKE_NONTYPED_ATOM_NOOP_ACTION   NonTypedAtom(ACTION_NAME_NOOP)
#define MAKE_NONTYPED_ATOM_NOACTION      NonTypedAtom(ACTION_NAME_NOACTION)
#define MAKE_NONTYPED_ATOM_SET_LITERAL_DEFAULT NonTypedAtom(ACTION_NAME_SET_LITERAL_DEFAULT)

#define MAKE_TYPED_ATOM_DEAD          TypedAtom("dead", std::vector<std::string>(), std::vector<std::string>())
#define MAKE_TYPED_ATOM_RETRY         TypedAtom("retry", std::vector<std::string>(), std::vector<std::string>())
#define MAKE_TYPED_ATOM_HELP          TypedAtom("help", std::vector<std::string>(), std::vector<std::string>())
#define MAKE_TYPED_ATOM_DISABLE       TypedAtom("disable", std::vector<std::string>(), std::vector<std::string>())
#define MAKE_TYPED_ATOM_START_TEACHER TypedAtom("startTeacher", std::vector<std::string>(), std::vector<std::string>())
#define MAKE_TYPED_ATOM_STOP_TEACHER  TypedAtom("stopTeacher", std::vector<std::string>(), std::vector<std::string>())
#define MAKE_TYPED_ATOM_UNKNOWN       TypedAtom("unknownManipulation", std::vector<std::string>(), std::vector<std::string>())
#define MAKE_TYPED_ATOM_TEACHER       TypedAtom("teacher", std::vector<std::string>(), std::vector<std::string>())
#define MAKE_TYPED_ATOM_FINISH_ACTION TypedAtom(ACTION_NAME_FINISH, std::vector<std::string>(), std::vector<std::string>())
#define MAKE_TYPED_ATOM_NOOP_ACTION   TypedAtom(ACTION_NAME_NOOP, std::vector<std::string>(), std::vector<std::string>())
#define MAKE_TYPED_ATOM_NOACTION      TypedAtom(ACTION_NAME_NOACTION, std::vector<std::string>(), std::vector<std::string>())
#define MAKE_TYPED_ATOM_DEFAULT       TypedAtom("default", std::vector<std::string>(), std::vector<std::string>())
#define MAKE_TYPED_ATOM_SET_LITERAL_DEFAULT TypedAtom(ACTION_NAME_SET_LITERAL_DEFAULT, std::vector<std::string>(), std::vector<std::string>())

// predicates
#define MAKE_NONTYPED_ATOM_FINISHED      NonTypedAtom("finished", std::vector<std::string>())
#define MAKE_TYPED_ATOM_FINISHED         TypedAtom("finished", std::vector<std::string>(), std::vector<std::string>())
#define MAKE_TYPED_ATOM_STARTED          TypedAtom("started", std::vector<std::string>(), std::vector<std::string>())
#define MAKE_TYPED_ATOM_EXCUSE_GENERATED TypedAtom("excuseGenerated", std::vector<std::string>(), std::vector<std::string>())

// action_ptr
#define MAKE_ATOM_DEAD_PTR    boost::make_shared<Atom>(MAKE_ATOM_DEAD)
#define MAKE_ATOM_RETRY_PTR   boost::make_shared<Atom>(MAKE_ATOM_RETRY)
#define MAKE_ATOM_HELP_PTR    boost::make_shared<Atom>(MAKE_ATOM_HELP)
#define MAKE_ATOM_UNKNOWN_PTR boost::make_shared<Atom>(MAKE_ATOM_UNKNOWN)
#define MAKE_ATOM_TEACHER_PTR boost::make_shared<Atom>(MAKE_ATOM_TEACHER)


#define MAX_SYMBOLIC_PARAMS 6
static const std::string DEFAULT_PARAM_NAMES[] = {"?X", "?Y", "?Z", "?V", "?W","?O", "?P","?Q", "?R","?S","?T"}; //!< Default param names that are considered symbolic.
static const std::string DEFAULT_NID_PARAM_NAMES[] = {"?X", "?Y", "?Z", "?V", "?W","?O", "?P","?Q", "?R","?S","?T"}; //!< Default NID param names that are considered symbolic.
static const uint DEFAULT_PARAM_NAMES_NUMBER = MAX_SYMBOLIC_PARAMS;
static const std::string DEFAULT_ACTION_NAME = "default";
static std::vector<std::string> DEFAULT_PARAM_NAMES_VECTOR(DEFAULT_PARAM_NAMES, DEFAULT_PARAM_NAMES + DEFAULT_PARAM_NAMES_NUMBER);


namespace PPDDL {
    template <typename BaseType> struct Object;
    template <typename BaseType>
    bool operator==(const Object<BaseType> &l, const Object<BaseType> &r);
    template <typename BaseType>
    bool operator<(const Object<BaseType>& l, const Object<BaseType>& r);
    template <typename BaseType>
    std::ostream& operator<<(std::ostream &out, const Object<BaseType>& o);
    
    template<typename BaseType>
    struct Object {
        typedef BaseType Type;
        typedef BaseType Name;
        
        Name name;
        Type type;
        
        Object(Name object_name, Type object_type):
            name(object_name),
            type(object_type)
        { }
        
        /*! \brief Operator == */
        friend bool operator== <>(const Object &s1, const Object &s2);
        /*! \brief Operator <= */
        friend bool operator< <>(const Object& l, const Object& r);
        /*! \brief Operator << */
        friend std::ostream& operator<< <>(std::ostream &out, const Object& o);
    };
    
    typedef Object<std::string> FullObject;
    typedef Object<uint> IndexedObject;
    
//     typedef std::vector< Type > TypeVector;
//     typedef std::map< Type, std::vector< Name > > TypeObjectsMap;
//     typedef std::map< Name, Type > ObjectTypeMap;
}


namespace symbolic {
    inline bool is_param_symbolic(const PPDDL::FullObject::Name& param) { return (param[0] =='?'); };
    int get_symbolic_param_index(const PPDDL::FullObject::Name& param);
    void make_vector_params_symbolic(std::vector<PPDDL::FullObject::Name>& vector_params);
    void make_vector_params_symbolic(std::vector<PPDDL::FullObject>& vector_params);
}




/*!
\class FullAtom
\brief Class representing a atom. Atoms consist of a name and a vector of parameters.
*/
template <typename ObjectT> class FullAtom;
template <typename ObjectT>
std::ostream& operator<<(std::ostream &out, const FullAtom<ObjectT> &s);

template<typename ObjectT>
class FullAtom
{
public:
    typedef ObjectT ParamType;
    
    typename ObjectT::Name name; //!< Name
    std::vector< ObjectT > params; //!< Object-Type pairs of the parameters

    /*!
    \brief Empty constructor
    */
    FullAtom() { }
    
    /*!
    \brief Checks if atom is symbolic, and updates its is_symbolic member

    If one of the default param names (X, Y, Z, ...) is found,
    is_symbolic is set to true.
    */
    bool is_symbolic() const;
    
    /*!
    \brief Makes the atom symbolic

    Changes the params to be the default symbolic params: X, Y, Z, V, W, Q.
    */
    void make_symbolic();
    
    /*!
    \brief Set the param names of the given vector
    
    Used to enhance performance of some routines
    */
    void set_param_names(const std::vector<ParamType>& new_names);
    
    static typename ParamType::Name get_no_action_name();
    
    virtual bool is_special() const =0;
    
    virtual bool is_plannable() const =0;
    
    virtual bool is_executable() const =0;
    
    /*! \brief Get vector of param names */
    std::vector<typename ObjectT::Name> get_param_names() const;
    
    /*! \brief Get vector of param types */
    std::vector<typename ObjectT::Type> get_param_types() const;
    
    friend std::ostream& operator<< <>(std::ostream &out, const FullAtom &s);

protected:
    
    static std::string load_atom_name_from_string(const std::string &line);
    static std::vector< std::string > get_atom_param_names_from_string(const std::string &line);
    static std::vector< std::string > get_param_names_from_nid_parameters_string(const std::string &nid_parameters);
    
};


/*!
\class IndexedAtom
\brief Class representing an index that corresponds to a atom.
*/
// class IndexedAtom
// {   
// public:
//     typedef PPDDL::IndexedObject ParamType;
//     
//     uint name; //!< Name
//     std::vector< ParamType > params; //!< Object-Type pairs of the parameters
// 
//     /*! \brief Empty constructor */
//     IndexedAtom(uint index, std::vector< ParamType > indexed_params): 
//         name(index),
//         params(indexed_params)
//     {}
//     
//     static ParamType::Name get_no_action_name() { return 0; }
//     
//     /*! \brief Get vector of param names */
//     std::vector<std::string> get_param_names() const;
//     
//     /*! \brief Get vector of param types */
//     std::vector<std::string> get_param_types() const;
// 
//     /*! \brief Operator == */
//     friend bool operator== (const IndexedAtom &s1, const IndexedAtom &s2);
//     /*! \brief Operator != */
//     friend bool operator!= (const IndexedAtom &s1, const IndexedAtom &s2);
//     /*! \brief Operator <= */
//     friend bool operator<(const IndexedAtom& l, const IndexedAtom& r);
//     /*! \brief Operator \<\< */
//     friend std::ostream& operator<<(std::ostream &out, const IndexedAtom &s);
// };


/*!
\class NonTypedAtom
\brief Class representing a atom. Atoms consist of a name and a vector of parameters.
*/
class NonTypedAtom : public FullAtom<PPDDL::FullObject>
{
public:
    /*!
    \brief Empty constructor
    */
    NonTypedAtom() { }

    /*!
    \brief constructor

    Parses a string that is assumed to have a atom.
    Examples of the syntax supported:
     "clear(X Y)", "clear(X,Y)", "clear()".

    \param line string having a valid representation of a atom
    */
    explicit NonTypedAtom(const std::string &line);
    
    /*!
    \brief Constructor

    Constructs the atom.

    \param name_ name
    \param param_names_ vector of param names
    */
    NonTypedAtom(const std::string &name_, const std::vector<std::string> &param_names_);

    /*!
    \brief Check if TypedAtom is a special atom

    Checks if atom is a special atom: dead(), retry(), help(), unknown(), finish(), teacher().

    \return true if it is special
    */
    virtual bool is_special() const;
    
    /*!
    \brief Check if the action can be used for planning

    \return true if it can be used for planning
    */
    virtual bool is_plannable() const;
    
    /*!
    \brief Check if the atom is an executable atom

    \return true if it is executable
    */
    virtual bool is_executable() const;
    
    void write_PPDDL(std::ostream &out) const;

    /*! \brief Operator == */
    friend bool operator== (const NonTypedAtom &s1, const NonTypedAtom &s2);
//     /*! \brief Operator != */
//     friend bool operator!= (const NonTypedAtom &s1, const NonTypedAtom &s2);
    /*! \brief Operator <= */
    friend bool operator<(const NonTypedAtom& l, const NonTypedAtom& r);
    /*! \brief Operator \<\< */
    friend std::ostream& operator<<(std::ostream &out, const NonTypedAtom &s);
    
    static NonTypedAtom get_atom_from_grounded_name_item(const std::string& grounded_name_atom);
};


template <typename ObjectT> class BaseTypedAtom;
template <typename ObjectT>
bool operator== (const BaseTypedAtom<ObjectT> &s1, const BaseTypedAtom<ObjectT> &s2);
template <typename ObjectT>
bool operator!= (const BaseTypedAtom<ObjectT> &s1, const BaseTypedAtom<ObjectT> &s2);
template <typename ObjectT>
bool operator<(const BaseTypedAtom<ObjectT>& l, const BaseTypedAtom<ObjectT>& r);
template <typename ObjectT>
std::ostream& operator<<(std::ostream &out, const BaseTypedAtom<ObjectT> &s);

template<typename ObjectT>
class BaseTypedAtom : public FullAtom<ObjectT>
{
public:
    /*!
    \brief Empty constructor
    */
    BaseTypedAtom() : FullAtom<ObjectT>() { }

    /*!
    \brief constructor

    Parses a string that is assumed to have a Atom.
    Examples of the syntax supported:
     "clear(?X - location ?Y - thing)", "clear()".

    \param line string having a valid representation of a atom
    */
    explicit BaseTypedAtom(const std::string &line);

    /*!
    \brief Constructor

    Constructs the Atom.

    \param name_ name
    \param param_names_ vector of param names
    \param param_types_ vector of param types
    */
    BaseTypedAtom(const typename ObjectT::Name &name_, const std::vector<typename ObjectT::Name> &param_names_, const std::vector<typename ObjectT::Type> &param_types_);
    
    /*!
    \brief Constructor

    Constructs the Atom.

    \param name_ name
    \param ppddl_params vector of params
    */
    BaseTypedAtom(const typename ObjectT::Name &name_, const std::vector<ObjectT> &ppddl_params);
    
    /*!
    \brief Compares if not grounded versions of the atoms are equal

    Compares the name and the number of parameters, but not the parameters content

    \return true if not grounded atoms are equal
    */
    bool symbolic_compare(const BaseTypedAtom &s) const;

    /*!
    \brief Check if the atom is a special atom

    \return true if it is special
    */
    virtual bool is_special() const;
    
    /*!
    \brief Check if the action can be used for planning

    \return true if it can be used for planning
    */
    virtual bool is_plannable() const;
    
    /*!
    \brief Check if the atom is an executable atom

    \return true if it is executable
    */
    virtual bool is_executable() const;
    
    /*!
    \brief Creat an atom with a longer name that encodes params
    
    Parameters are removed, and instead are encoded within the name
     */
    void PPDDL_grounding();
    
    /*!
    \brief Check if atom is initialized

    Check if atom is initialized, that is that its name is not empty.

    \return true if initialized, false if empty
    */
    bool is_valid() const;
    
    /*!
    \brief Check if a parameter with the given name exists

    \return true if there is a parameter with the given name
    */
    bool has_param(const ObjectT &required_param) const;

    /*!
    \brief Check if parameter with all the given name exist

    \return true if all of them exist
    */
    bool params_belong_to(const std::vector<ObjectT> &list_of_params) const;

    /*!
    \brief Gets the index of a param

    \param param name of the param to look for its index
    \return true if it is special
    */
    int get_param_name_idx(const typename ObjectT::Name &param) const;
    
    /*!
     \brief Change parameters
     */
    void change_params(const std::vector<ObjectT>& orig_var_v, const std::vector<ObjectT>& new_var_v);
    
    /*!
     \brief Change parameter
     */
    void change_param(const ObjectT& orig_var, const ObjectT& new_var);
    
    /*!
     \brief Change parameter name
     */
    void change_param_name(const typename ObjectT::Name& orig_var, const typename ObjectT::Name& new_var);
    
    /*!
     \brief Change parameter names
     */
    void change_param_names(const std::vector<typename ObjectT::Name>& orig_var_v, const std::vector<typename ObjectT::Name>& new_var_v);
    
    /*!
     \brief Change parameter names
     */
    void change_param_names(const std::vector<ObjectT>& orig_var_v, const std::vector<ObjectT>& new_var_v);
    
    /*!
    \brief Grounds the atom with the given parameters
    */
    void ground(std::vector<typename ObjectT::Name> variables);
    
    /*!
    \brief Grounds the atom with the given parameters
    */
    bool ground(std::vector<ObjectT> variables);
    
    /*!
    \brief Writes PPDDL typed atom
    */
    void write_PPDDL(std::ostream &out) const;
    
    /*! \brief Writes RDDL typed atom. When defining transitions, future_state means the value for the next time step */
    void write_RDDL(std::ostream &out, const bool future_state = false) const;

    /*! \brief Write typed params */
    void write_typed_parameters(std::ostream &out) const;
    
    /*! \brief Operator == */
    friend bool operator== <>(const BaseTypedAtom &s1, const BaseTypedAtom &s2);
    /*! \brief Operator != */
    friend bool operator!= <>(const BaseTypedAtom &s1, const BaseTypedAtom &s2);
    /*! \brief Operator <= */
    friend bool operator< <>(const BaseTypedAtom& l, const BaseTypedAtom& r);
    /*! \brief Operator \<\< */
    friend std::ostream& operator<< <>(std::ostream &out, const BaseTypedAtom &s);

private:
    void load_typed_param_names_from_string(const std::string &line);
    void get_typed_params_from_ppddl_parameters_string(const std::string &ppddl_parameters);
};


typedef BaseTypedAtom<PPDDL::IndexedObject> IndexedAtom;
typedef BaseTypedAtom<PPDDL::FullObject> TypedAtom;

typedef boost::optional<TypedAtom> TypedAtomOpt;
typedef boost::shared_ptr<TypedAtom> TypedAtomPtr;
typedef boost::shared_ptr<const TypedAtom> TypedAtomConstPtr;

namespace symbolic {
    NonTypedAtom transform_to_nontyped(const TypedAtom& typed_atom);
}


/*!
\class PPDDLObjectManager
\brief Class to manage objects and types.
*/

template <typename ObjectT> class PPDDLObjectManager;
template <typename ObjectT>
std::ostream& operator<<(std::ostream &out, const PPDDLObjectManager<ObjectT> &pom);

template<typename ObjectT>
class PPDDLObjectManager
{
public:
    typedef typename ObjectT::Type ObjectType;
    typedef typename ObjectT::Name ObjectName;
    
    PPDDLObjectManager() {};
    
    PPDDLObjectManager(const PPDDLObjectManager& other):
        ppddl_objects_per_type(other.ppddl_objects_per_type),
        ppddl_objects_types(other.ppddl_objects_types)
    {};
    
    typedef std::vector<ObjectType> ListPPDDLTypes;
    typedef std::vector< std::vector<ObjectName> > CombinationsPPDDLObjects;
    typedef boost::shared_ptr<CombinationsPPDDLObjects> CombinationsPPDDLObjectsPtr;
    typedef std::map<ListPPDDLTypes, CombinationsPPDDLObjectsPtr> CacheCombinationsObjects;
    
    typedef std::vector< ObjectType > TypeVector;
    typedef std::map< ObjectType, std::vector< ObjectName > > TypeObjectsMap;
    typedef std::map< ObjectName, ObjectType > ObjectTypeMap;
    
    /**
     * \brief Gets typed atom by searching is corresponding param types
     */
    TypedAtom get_typed_atom(const FullAtom<PPDDL::FullObject>& atom) const;
    
    /**
     * \brief Gets the type of the object
     */
    ObjectType get_object_type(const ObjectName& object) const;
    
    /**
     * \brief Gets the objects with type type
     */
    std::vector<ObjectName> get_type_objects(const ObjectType& type) const;
    
    /**
     * \brief Gets vectors of ppddl objects associated to each type
     */
    const TypeObjectsMap& get_ppddl_objects_per_type() const { return ppddl_objects_per_type; };
    
    /**
     * \brief Gets the type associated to each object
     */
    const ObjectTypeMap& get_ppddl_objects_types() const { return ppddl_objects_types; };
    
    /**
     * \brief Add object-type pairs from the added_object_manager
     */
    void add_objects(const PPDDLObjectManager& added_object_manager);
    
    /**
     * \brief Add object-type pairs from a std container
     */
    template <typename Iter>
    void add_objects(Iter added_objects_it, Iter added_objects_end) {
        for (; added_objects_it != added_objects_end; ++added_objects_it) {
            add_object(*added_objects_it);
        }
    }
    
    /**
     * \brief Add a object-type pair
     */
    void add_object(const ObjectT& ppddl_object) {
        if (ppddl_objects_types.count(ppddl_object.name) == 0) { // object doesn't exist
            ppddl_objects_types[ppddl_object.name] = ppddl_object.type;
            ppddl_objects_per_type[ppddl_object.type].push_back(ppddl_object.name);
        }
    }
    
    /**
     * \brief Get list of objects
     */
    std::set<ObjectT> get_ppddl_objects() const { 
        std::set<ObjectT> res; 
        BOOST_FOREACH(typename ObjectTypeMap::value_type object_type, ppddl_objects_types) {
            res.insert(ObjectT(object_type.first, object_type.second));
        } 
        return res; 
    };
    
    /**
     * \brief Get list of object names
     */
    std::vector<ObjectName> get_ppddl_object_names() const { 
        std::vector<std::string> res; 
        BOOST_FOREACH(typename ObjectTypeMap::value_type object_type, ppddl_objects_types) {
            res.push_back(object_type.first);
        } 
        return res; 
    };
    
    CombinationsPPDDLObjectsPtr get_combinations_of_objecs_for_given_params(const std::vector<ObjectType>& list_param_types) const;
    
    /*! \brief Operator \<\< */
    friend std::ostream& operator<< <>(std::ostream &out, const PPDDLObjectManager &pom);
    
private:
    TypeObjectsMap ppddl_objects_per_type;
    ObjectTypeMap ppddl_objects_types;
    mutable CacheCombinationsObjects cache_combinations_objects;
};

#include <symbolic/symbols_template_imp.h>

#endif
