#include "utils.h"
#include "boost/filesystem/operations.hpp"

bool utils::is_close(const float f1, const float f2) {
    return std::abs(f1 - f2) < (std::numeric_limits<float>::epsilon()*10);
}

bool utils::is_close(const double d1, const double d2) {
    return std::abs(d1 - d2) < (std::numeric_limits<float>::epsilon()*10);
}

bool utils::is_significantly_less(const float f1, const float f2) {
    return (f1 - f2) < (-1*std::numeric_limits<float>::epsilon()*10);
}

bool utils::is_significantly_less(const double d1, const double d2) {
    return (d1 - d2) < (-1*std::numeric_limits<float>::epsilon()*10);
}

std::string utils::get_selfpath() {
    char buff[1024];
    ssize_t len = ::readlink("/proc/self/exe", buff, sizeof(buff)-1);
    if (len != -1) {
      buff[len] = '\0';
//       return std::string(buff);
      boost::filesystem::path exec_path(buff);
      return exec_path.parent_path().generic_string();
    } 
    else {
        std::cerr << "Error reading self path." << std::endl;
        return std::string();
    }
}

bool utils::is_absolute_path(const std::string& path)
{
        if (path.empty()) 
            return false;
        if (path[0] == '/') {
            return true;
        }
        else {
            return false;
        }
}

void utils::send_notification_msg(const std::string& title_msg, const std::string& main_msg)
{
    /* Should be an optional feature.
     * Not using C++ API to don't having a mandatory dependency on libnotify.
     * Using bash command instead inside in a try-catch
     * 
     * C++ libnotify integration code:
    #include <libnotifymm.h>
    Notify::init("Hello world!");
    Notify::Notification Hello("Hello world", "This is an example notification.", "dialog-information");
    Hello.show();
    */
    std::string system_command = "notify-send --urgency=low \"" + title_msg + "\" \"" + main_msg + "\" --icon=dialog-information";
    try {
        int res = system(system_command.c_str());
        if (res != 0) {
            std::cerr << "Failed to send notify message." << std::endl;
        }
    }
    catch(int e) { 
        std::cerr << title_msg << " [WARNING: libnotify not present in the system...]" << std::endl;
    }
}

uint utils::get_socket_from_current_dir_hash(ushort port_index)
{
    boost::filesystem::path full_path = boost::filesystem::current_path();
    boost::hash<std::string> string_hash;
    uint hash_port = (string_hash(full_path.generic_string()) % 20000) + 20000; // [20.000, 50.000] || Unix range: 0-65,535 || 1-1023 are privileged ports (root)
    hash_port += 20000 * port_index;
    assert(hash_port < 65535);
    return hash_port;
}

void utils::initialize_random_seed()
{
    struct timeval time; 
    gettimeofday(&time,NULL);
    std::srand((time.tv_sec * 1000) + (time.tv_usec / 1000));
}

float utils::get_hoeffding_inequality_confidence(const int number_samples, const float confidence_interval)
{
    float significance_single_side = exp((-2.0) * (float)number_samples * confidence_interval * confidence_interval);
    return 1 - significance_single_side;
}

void utils::print_progress(const uint current_progress, const uint total)
{
    float progress = float(current_progress) / float(total);
    int bar_width = 100;

    std::cout << "[";
    int pos = bar_width * progress;
    for (int i = 0; i < bar_width; ++i) {
        if (i < pos) {
            std::cout << "=";
        }
        else if (i == pos) {
            std::cout << ">";
        }
        else {
            std::cout << " ";
        }
    }
    std::cout << "] " << int(progress * 100.0) << " %" << "\r";
//     std::cout.flush();
}

std::string utils::get_parsed_time_from_seconds(const ulong total_seconds)
{
    std::stringstream ss_seconds, ss_minutes;
    uint seconds = total_seconds % 60;
    uint minutes = ((total_seconds - seconds) / 60) % 60;
    uint hours = (total_seconds - seconds - (60 * minutes)) / (60 * 60);

    ss_seconds << std::setfill('0') << std::setw(2) << seconds;
    ss_minutes << std::setfill('0') << std::setw(2) << minutes;
    return boost::lexical_cast<std::string>(hours) + ":" + ss_minutes.str() + ":" + ss_seconds.str();
}

bool utils::is_number(const std::string& s)
{
    std::string::const_iterator it = s.begin();
    while (it != s.end() && std::isdigit(*it)) ++it;
    return !s.empty() && it == s.end();
}

bool utils::compare_vector_strings(const std::vector<std::string>& v1, const std::vector<std::string>& v2)
{
    if (v1.size() != v2.size())
        return false;
    std::set<std::string> s1(v1.begin(), v1.end());
    std::set<std::string> s2(v2.begin(), v2.end());
    std::vector<std::string> v3;
    std::set_intersection(s1.begin(), s1.end(), s2.begin(), s2.end(), std::back_inserter(v3));
    if (v1.size() == v3.size())
        return true;
    else
        return false;
}

bool utils::compare_ordered_vector_strings(const std::vector<std::string>& v1, const std::vector<std::string>& v2)
{
    bool equal = true;
    if (v1.size() != v2.size())
        return false;
    size_t i = 0;
    while ((i < v1.size()) && equal) {
        if (v1[i] != v2[i]) {
            equal = false;
        }
        ++i;
    }
    return equal;
}


char utils::choose_variable_separator(const std::string& str)
{
    // get params separated by ,
    char separator;
    size_t found = str.find(',');
    if (found != std::string::npos) {
        separator = ',';
    } else {
        separator = ' ';
    }
    return separator;
}

std::string utils::trim(const std::string& str,
                        const std::string& whitespace)
{
    const size_t strBegin = str.find_first_not_of(whitespace);
    if (strBegin == std::string::npos)
        return ""; // no content

    const size_t strEnd = str.find_last_not_of(whitespace);
    const size_t strRange = strEnd - strBegin + 1;

    return str.substr(strBegin, strRange);
}

std::string utils::remove_spaces(std::string str)
{
    str.erase(std::remove_if(str.begin(), str.end(), ::isspace), str.end());
    return str;
}


std::vector<std::string> utils::split(const std::string& str, const char separator) 
{
    std::vector<std::string> result;
    std::string item;
    std::stringstream ss;
    ss.str(str);
    while (std::getline(ss, item, separator)) {
        // remove spaces and empty strings
        utils::trim(item);
        if (item.compare("") != 0) {
            result.push_back(item);
        }
    }
    return result;
}

std::string utils::get_file_contents(const std::string& file_path, const bool ignore_warnings)
{
    std::ifstream in(file_path.c_str(), std::ios::in);
    if (in) {
        std::string contents;
        in.seekg(0, std::ios::end);
        contents.resize(in.tellg());
        in.seekg(0, std::ios::beg);
        in.read(&contents[0], contents.size());
        in.close();
        return(contents);
    } else {
        LOG_IF(!ignore_warnings, ERROR) << "Couldn't open file " << file_path;
        throw std::runtime_error(std::string("Couldn't open file " + file_path));
    }
}


std::string utils::get_file_contents(const std::string& file_path, const std::string& init_keyword , const std::string& end_keyword, bool ignore_warnings)
{
    std::string file_contents = get_file_contents(file_path, ignore_warnings);
    uint init_pos = file_contents.find(init_keyword) + init_keyword.size();
    return file_contents.substr(init_pos, file_contents.find(end_keyword) - init_pos);
}


void utils::write_string_to_file(const std::string& file_path, const std::string& out_str, bool quiet)
{
    std::ofstream file_w(file_path.c_str());
    if (!quiet)
        std::cout << "utils::write_string_to_file:: " << "Writting string to file " << file_path << std::endl;
    if (file_w.is_open()) {
        file_w << out_str;
    } else {
        std::cerr << "Couldn't write to file " << file_path << std::endl;
    }
    file_w.close();
}

void utils::append_string_to_file(const std::string& file_path, const std::string& out_str, bool quiet)
{
    std::ofstream file_w(file_path.c_str(), std::ofstream::out | std::ofstream::app);
    if (!quiet)
        std::cout << "utils::write_string_to_file:: " << "Writting string to file " << file_path << std::endl;
    if (file_w.is_open()) {
        file_w << out_str;
    } else {
        std::cerr << "Couldn't write to file " << file_path << std::endl;
    }
    file_w.close();
}

char* utils::string_to_char(const std::string& str)
{
    char * writable = new char[str.size() + 1];
    std::copy(str.begin(), str.end(), writable);
    writable[str.size()] = '\0';
    return writable;
}

void utils::wait_for_enter_key()
{
    std::string str_aux;
    std::cout << "Press enter to continue:" << std::flush;
    std::getline (std::cin, str_aux);
}

std::string utils::read_string_from_stream(std::istream& input_stream)
{
    std::string result;
    std::getline (input_stream, result);
    return result;
}

void utils::init_loggers(const std::string& config_path)
{
//     easyloggingpp::Loggers::setFilename("log");
    easyloggingpp::Configurations confFromFile(config_path);
    easyloggingpp::Loggers::reconfigureAllLoggers(confFromFile);

    std::map<std::string, std::string> logger_colors;
    logger_colors["planning"] = "1;34m";
    logger_colors["explore"] = "1;35m";
    logger_colors["excuses"] = "1;36m";
    logger_colors["teacher"] = "1;32m";
    logger_colors["application"] = "1;36m";

    for (std::map<std::string, std::string>::iterator logger_it = logger_colors.begin(); logger_it != logger_colors.end(); ++logger_it) {
        // little hack: make all logger names the same length
        std::string logger_string = logger_it->first.substr(0, 8);
        while (logger_string.length() < 9)
            logger_string += " ";

        easyloggingpp::Loggers::getLogger(logger_it->first);
        confFromFile.parseFromText("*ALL:\nFORMAT = %level [" + logger_it->second + logger_string + "%func#[0m %log");
        confFromFile.parseFromText("*WARNING:\nFORMAT = [1;33m%level! [" + logger_it->second + logger_string + "%func#[0m %log");
        confFromFile.parseFromText("*ERROR:\nFORMAT = [1;31m**%level!!!! [" + logger_it->second + logger_string + "%func#[0m %log");
        easyloggingpp::Loggers::reconfigureLogger(logger_it->first, confFromFile);
    }

    confFromFile.clear();
}

double utils::get_elapsed_time(struct timeval& timer_start)
{
    struct timeval end, elapsed_time;

    gettimeofday(&end, NULL);
    timersub(&end, &timer_start, &elapsed_time);

    return elapsed_time.tv_sec + ((double)elapsed_time.tv_usec / (1000 * 1000));
}

struct timeval;
timeval utils::start_timer() {
    timeval timer_start;
    gettimeofday(&timer_start, NULL);
    return timer_start;
}
