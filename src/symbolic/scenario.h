/*
 * Class having the information needed to represent an scenario (rules, state, goals, configuration, ...)
 * Copyright (C) 2016  David Martínez (dmartinez@iri.upc.edu)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef SCENARIO_H
#define SCENARIO_H

#include <symbolic/domain.h>
#include <symbolic/rules.h>
#include <symbolic/state.h>
#include <rexd/config_reader.h>
#include <parsers/pddl_parser.h>



/*!
\class Scenario
\brief Class having all the information to define the current scenario status.

Class having all the information to define the current scenario status.\n
Includes the domain, the configuration options, the current state, rules, goals and action list.
*/
class Scenario
{
public:
    Domain domain; //!< Domain
    ConfigReader config_reader; //!< Configuration
    
    /*!
    \brief Creates an scenario.
    
    If final reward is set in config, the finish rule is read from "nid_finish_rule_path" and added to the rule set.
    This special rule is not learned, so it is read separately from the rest.
    */
    Scenario(const Typed::RuleSet& rules, const State& state, const RewardFunctionGroup& goals, 
             const ConfigReader& config_reader
            );
    
    /*!
    \brief Creates an scenario.
    
    If final reward is set in config, the finish rule is read from "nid_finish_rule_path" and added to the rule set.
    This special rule is not learned, so it is read separately from the rest.
    
    The problem file is mandatory because it specifies the goals.
    */
    Scenario(const Typed::RuleSet& rules_, const std::string& pddl_problem_file, const ConfigReader& config_reader_);
    
    /*!
    \brief Creates an scenario.
    
    If final reward is set in config, the finish rule is read from "nid_finish_rule_path" and added to the rule set.
    This special rule is not learned, so it is read separately from the rest.
    
    The problem file is mandatory because it specifies the goals.
    */
    Scenario(const std::string& pddl_domain_file, const std::string& pddl_problem_file, const ConfigReader& config_reader_);
    
    /*!
    \brief Resets internal variables.
    */
    virtual void reset();
    
    /*!
    \brief Reset state with a PPDDL file problem.
    
    Reads the objects, initial state and goals.

    \param ppddl_file_path PPDDL file path.
    */
    void reset_problem_file(const std::string& ppddl_file_path) { read_ppddl_problem_file(ppddl_file_path); };

    /*!
    \brief Applies a grounded action in the current scenario.
    
    Updates the state to reflect the action changes. Ponderate random selection of outcome.

    \param action action to be applied.
    \return True if action was applied, False otherwise.
    */
    bool transition_next_state_simple(const TypedAtom& action);
    
    /*!
    \brief Applies a grounded action in the current scenario.
    
    Updates the state to reflect the action changes.

    \param action action to be applied.
    \retval success True if the first outcome was applied, False otherwise.
    \retval reward reward obtained by applying the action.
    \param force_outcome force the first outcome that changes something to be applied. If false, outcomes probabilites are used to decide which one to use.
    \param force_outcome_idx index of the outcome to be applied. If -1, outcomes probabilites are used to decide which one to use.
    \return True if action was applied, False otherwise.
    */
    bool transition_next_state(const TypedAtom& action, bool& success, float& reward, const bool force_outcome = false, const int force_outcome_idx = -1);

    /*!
    \brief Applies a grounded rule in the current scenario.
    
    Updates the state to reflect the grounded rule changes.
    Note that the "noaction" rules are not applied

    \param grounded_rule grounded rule to be applied.
    \param new_state state that is going to be modified by grounded_rule effects
    \retval success True if the first outcome was applied, False otherwise.
    \retval reward reward obtained by applying the rule.
    \param force_outcome force the first outcome that changes something to be applied. If false, outcomes probabilites are used to decide which one to use.
    \param force_outcome_idx index of the outcome to be applied. If -1, outcomes probabilites are used to decide which one to use.
    \return True if rule was applied, False otherwise.
    */
    bool apply_rule(const Typed::GroundedRule& grounded_rule, State& new_state, bool& success, float& reward, const bool force_outcome, const int force_outcome_idx) const;
    
    /*!
    \brief Checks if scenario is in goal state.

    \return True if scenario is in goal state.
    */
    bool is_in_goal() const;
    
    void decrease_planning_horizon() { if (planning_horizon != 0) { planning_horizon --; } }
    
    void set_planning_horizon(const uint new_planning_horizon) { planning_horizon = new_planning_horizon; }

    void add_reward(const float new_reward) { current_reward += new_reward; }
    
    // gets
    inline const float& get_reward() const {
        return current_reward;
    }
    inline const Typed::RuleSet& get_rules() const {
        return rules;
    };
    inline Typed::RuleSet& get_modificable_rules() {
        return rules;
    };
    inline const RewardFunctionGroup& get_goals() const {
        return goals;
    };
    inline const State& get_state() const {
        return state;
    };
    inline State& get_modificable_state() {
        return state;
    }
//     inline const GoalGroup& get_goals() const {
//         return goals;
//     };
    inline const std::vector<TypedAtom>& get_action_list() const {
        return domain.get_actions();
    }
    inline std::vector<TypedAtom> get_non_special_action_list() const {
        return domain.get_non_special_actions();
    }
    inline std::vector<TypedAtom> get_executable_action_list() const {
        return domain.get_executable_actions();
    }
    inline std::vector<TypedAtom> get_plannable_action_list() const {
        return domain.get_plannable_actions();
    }
    inline const uint get_planning_horizon() const {
        return planning_horizon;
    }

    // sets
    void set_rules(const Typed::RuleSet& rules_);
    void set_state(const State& state_);
    void set_goals(const RewardFunctionGroup& goals_);
    
    void write_PDDL_domain(const std::string& file_path);
    void write_PDDL_domain(std::ostream& out);
    void write_PDDL_problem(const std::string& file_path);
    void write_PDDL_problem(std::ostream& out);

    
protected:
    Typed::RuleSet rules;
    State state;
    RewardFunctionGroup goals;
    float current_reward;
    float noop_reward;
    uint planning_horizon;
    
    
private:
    std::string get_ppddl_field_content(const std::string& ppddl_field_str, const std::string& field_identifier_str) const;
    
    void read_ppddl_problem_file(const std::string& ppddl_file_path);
    void read_ppddl_domain_file(const std::string& pddl_file_path);
    
    void init();
};

typedef boost::shared_ptr<Scenario> ScenarioPtr; //!< Shared pointer to scenario

#endif // SCENARIO_H


