/*
 * Class that represents the predicates and rules domain. It stores the different existing objects and symbols.
 * Copyright (C) 2016  David Martínez (dmartinez@iri.upc.edu)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef IRI_RULE_LEARNER_DOMAIN_H
#define IRI_RULE_LEARNER_DOMAIN_H 1

#include <vector>
#include <fstream>
#include <cmath>

#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>

#include <symbolic/symbols.h>
#include <symbolic/predicates.h>
#include <symbolic/transitions.h>
#include <symbolic/state.h>
#include <symbolic/rules.h>
#include <rexd/config_reader.h>

template<typename AtomT> class Rule;
template<typename AtomT> class RuleSet;

/*!
\class Domain
\brief Class indexing all domain symbols, actions and objects.

At this moment it just indexes symbols, actions and objects. This indexing is just used to generate excuses, but it would be nice to use it in other parts also. If it was used for planning and learning, symbol arguments wouldn't have to be just one character or a number to be compatible with libprada implementation.
*/
class Domain
{
public:

    Domain(const ConfigReader& config_reader);

    void clear() {
        known_symbolic_predicates.clear();
        known_atoms.clear();
        known_symbolic_atoms.clear();
        known_actions.clear();
        known_objects.clear();
        known_types.clear();
    }

    int index_type(const PPDDL::FullObject::Type& ppddl_type);
    int index_atom(const TypedAtom& symbol);
    int index_predicate(const Predicate<TypedAtom>& predicate);
    int index_action(const TypedAtom& action);
    void index_state(const State& state);
    void index_predicates(const PredicateGroup<TypedAtom>& predicates);
    void index_goals(const RewardFunctionGroup& goals);
    void index_transitions(const TransitionGroup<TypedAtom>& transitions);
    void index_rule(const Rule<TypedAtom>& rule);
    void index_rules(const RuleSet<TypedAtom>& rules);
    
    /**
     * \brief Index a set of PDDL object-type
     * 
     * \param ppddl_objects_per_type Vector of objects per type
     */
    void index_objects(const std::set<PPDDL::FullObject>& ppddl_objects_per_type);
    
    /**
     * \brief Get transitions with object indexes
     * 
     * Substitutes objects by the indexes
     */
    TransitionGroup<TypedAtom> get_transitions_with_indexed_objects(const TransitionGroup<TypedAtom>& transitions);

    size_t get_type_index(const PPDDL::FullObject::Type& type) const;
    size_t get_object_index(const PPDDL::FullObject& object) const;
    size_t get_action_symbolic_index(const TypedAtom& action);
    size_t get_action_symbolic_index_const(const TypedAtom& action) const;
    size_t get_atom_index(const TypedAtom& symbol);
    size_t get_atom_symbolic_index_const(const TypedAtom& symbol) const;
    size_t get_predicate_index(const Predicate<TypedAtom>& predicate);
    size_t get_predicate_index_const(const Predicate<TypedAtom>& predicate) const;
    std::vector<size_t> get_predicate_group_indexes_const(const PredicateGroup<TypedAtom>& predicates) const;
    
    /**
     * \brief Transforms to typed based on knwon symbols
     */
    PredicateGroup<TypedAtom> transform_to_typed(const PredicateGroup<NonTypedAtom>& non_typed_preds) const;
    Predicate<TypedAtom> transform_to_typed(const Predicate<NonTypedAtom>& non_typed_predicate) const;
    TypedAtom transform_to_typed(const NonTypedAtom& non_typed_atom) const;
    
    /**
     * Transformation from indexed to typed
     */
    PPDDL::FullObject transform_object_to_typed(const PPDDL::IndexedObject& indexed_object);
    TypedAtom transform_atom_to_typed(const IndexedAtom& indexed_atom);
    TypedAtom transform_action_to_typed(const IndexedAtom& indexed_action);
    Predicate<TypedAtom> transform_predicate_to_typed(const Predicate<IndexedAtom>& indexed_predicate);
    PredicateGroup<TypedAtom> transform_predicate_group_to_typed(const PredicateGroup<IndexedAtom>& indexed_predicates);
    Outcome<TypedAtom> transform_outcome_to_typed(const Outcome<IndexedAtom>& indexed_outcome);
    OutcomeGroup<TypedAtom> transform_outcome_group_to_typed(const OutcomeGroup<IndexedAtom>& indexed_outcomes);
    SymbolicRule<TypedAtom> transform_symbolic_rule_to_typed(const SymbolicRule<IndexedAtom>& indexed_rule);
    RuleSet<TypedAtom> transform_rule_set_to_typed(const RuleSet<IndexedAtom>& indexed_rules);
    PPDDLObjectManager<PPDDL::FullObject> transform_object_manager_to_typed(const PPDDLObjectManager<PPDDL::IndexedObject>& indexed_object_manager);
    Transition<TypedAtom> transform_transition_to_typed(const Transition<IndexedAtom>& indexed_transition);
    TransitionGroup<TypedAtom> transform_transition_group_to_typed(const TransitionGroup<IndexedAtom>& indexed_transitions);
    
    /**
     * Transformation from typed to indexed
     */
    PPDDL::FullObject::Type get_type(size_t index) { return known_types[index]; }
    PPDDL::IndexedObject transform_object_to_indexed(const PPDDL::FullObject& typed_object) const;
    IndexedAtom transform_atom_to_indexed(const TypedAtom& typed_atom) const;
    IndexedAtom transform_action_to_indexed(const TypedAtom& typed_action) const;
    Predicate<IndexedAtom> transform_predicate_to_indexed(const Predicate<TypedAtom>& predicate) const;
    PredicateGroup<IndexedAtom> transform_predicate_group_to_indexed(const PredicateGroup<TypedAtom>& pred_group) const;
    Outcome<IndexedAtom> transform_outcome_to_indexed(const Outcome<TypedAtom>& typed_outcome) const;
    OutcomeGroup<IndexedAtom> transform_outcome_group_to_indexed(const OutcomeGroup<TypedAtom>& typed_outcomes) const;
    SymbolicRule<IndexedAtom> transform_symbolic_rule_to_indexed(const SymbolicRule<TypedAtom>& typed_rule) const;
    PPDDLObjectManager<PPDDL::IndexedObject> transform_object_manager_to_indexed(const PPDDLObjectManager<PPDDL::FullObject>& typed_object_manager) const;
    Transition<IndexedAtom> transform_transition_to_indexed(const Transition<TypedAtom>& typed_transition) const;
    TransitionGroup<IndexedAtom> transform_transition_group_to_indexed_unordered(const TransitionGroup<TypedAtom>& typed_transitions) const;

    /**
     * Get indexes
     */
    Predicate<TypedAtom> get_predicate(const size_t index) { return known_predicates[index]; }
    TypedAtom get_action_symbolic(size_t index) { return known_actions[index]; }
    PPDDL::FullObject get_object(size_t index) { return known_objects[index]; }
    
    size_t get_predicates_size() {
        return known_predicates.size();
    }

    const std::vector< Predicate<TypedAtom> >& get_predicates() const {
        return known_predicates;
    }
    PredicateGroup< TypedAtom > get_constant_predicates() const;
    const std::vector< TypedAtom >& get_actions() const {
        return known_actions;
    }
    std::vector< TypedAtom > get_non_special_actions() const;
    std::vector< TypedAtom > get_executable_actions() const;
    std::vector< TypedAtom > get_plannable_actions() const;
    const std::vector< TypedAtom >& get_atoms() const {
        return known_atoms;
    }
    const std::vector< TypedAtom >& get_symbolic_atoms() const {
        return known_symbolic_atoms;
    }
    const std::vector< PPDDL::FullObject >& get_objects() const {
        return known_objects;
    }
    const std::vector< PPDDL::FullObject::Type >& get_ppddl_types() const {
        return known_types;
    }
    void make_predicate_group_no_closed_world(PredicateGroup<TypedAtom>& predicates, const bool symbolic = false) const;
    TransitionGroup<TypedAtom> get_transitions_no_closed_world_unordered(const TransitionGroup<TypedAtom>& transitions) const;

    void write_LFIT_predicate_types(std::ostream& out) const;
    void write_PPDDL_grounded_predicates(std::ostream& out) const;
    void write_PPDDL_symbolic_predicates(std::ostream& out, const PPDDLObjectManager<PPDDL::FullObject>& pddl_object_manager) const;
    void write_RDDL_objects(std::ostream& out, const PPDDLObjectManager<PPDDL::FullObject>& pddl_object_manager) const;
    void write_RDDL_pvariables(std::ostream& out, const PPDDLObjectManager<PPDDL::FullObject>& pddl_object_manager) const;
    void write_NID(std::ostream& out) const;

    friend std::ostream& operator<<(std::ostream &out, const Domain& dom);

private:
    std::vector< Predicate<TypedAtom> > known_predicates;
    std::vector< Predicate<TypedAtom> > known_symbolic_predicates;
    std::vector< TypedAtom > known_atoms;
    std::vector< TypedAtom > known_symbolic_atoms;
    std::vector< TypedAtom > known_actions;
    std::vector< PPDDL::FullObject > known_objects;
    std::vector< PPDDL::FullObject::Type> known_types;
    bool generalize_first_action_param;
    bool generalize_second_action_param;
    
    template<class T>
    int index_item(const T& item, std::vector<T>& known_items);
    template<class T>
    int get_item_index(const T& item, const std::vector<T>& known_items) const;
    
    // Other
    Transition<TypedAtom> get_transition_with_indexed_objects(const Transition<TypedAtom>& transition);
    TypedAtom get_typed_atom_with_indexed_objects(const TypedAtom& symbol);
    PredicateGroup<TypedAtom> get_predicates_with_indexed_objects(const PredicateGroup<TypedAtom>& predicates);
    Predicate<TypedAtom> get_predicate_with_indexed_objects(const Predicate<TypedAtom>& predicate);
};

typedef boost::shared_ptr<Domain> DomainPtr;


#endif
