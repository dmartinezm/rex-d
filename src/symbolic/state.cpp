#include "state.h"

/**
 * State
 *
 */

// void State::read_from_file(std::string file_path)
// {
//     LOG(WARNING) << "Deprecated";
//     *this = Predicates::read_predicates_from_file(file_path);
// }

void State::write_to_file(std::string file_path) const
{
    std::stringstream ss;
    ss << *this;
    utils::write_string_to_file(file_path, ss.str());
}

/**
 * Changes
 */
void State::update_predicate(Predicate<TypedAtom> new_predicate)
{
    PredicateGroup<TypedAtom>::update_predicate(new_predicate);
}

void State::remove_special_predicates()
{
    for (State::iterator it = begin(); it != end(); ) {
        if (it->name == PREDICATE_NAME_GOAL_AUX) {
            erase(it++);
        }
        else if (it->name == PREDICATE_NAME_GOAL_REWARD_RECEIVED_AUX) {
            erase(it++);
        }
        else {
            ++it;
        }
    }
    
    PredicateGroup<TypedAtom> special_predicates;
    special_predicates.insert(Predicate<TypedAtom>(MAKE_TYPED_ATOM_FINISHED, true));
    special_predicates.insert(Predicate<TypedAtom>(MAKE_TYPED_ATOM_FINISHED, false));
    special_predicates.insert(Predicate<TypedAtom>(MAKE_TYPED_ATOM_STARTED, true));
    special_predicates.insert(Predicate<TypedAtom>(MAKE_TYPED_ATOM_STARTED, false));
    PredicateGroup<TypedAtom>::erase(special_predicates);
}

void State::clear()
{
    PredicateGroup<TypedAtom>::clear();
    ppddl_object_manager = PPDDLObjectManager<PPDDL::FullObject>();
    constant_predicates.clear();
}

void State::clear_nonconstant_predicates()
{
    PredicateGroup<TypedAtom>::clear();
    add_predicates(constant_predicates);
}

void State::set_constants(const PredicateGroup<TypedAtom>& new_constant_predicates) { 
    constant_predicates = new_constant_predicates;
    PredicateGroup<TypedAtom>::set_constants(new_constant_predicates);
}

PredicateGroup<TypedAtom> State::get_non_constants() const
{
    PredicateGroup<TypedAtom> non_constants(*this);
    BOOST_FOREACH(Predicate<TypedAtom> constant_predicate, constant_predicates) {
        // the symbol is constant, so remove both positive and negative versions of the predicate
        non_constants.erase(constant_predicate);
        non_constants.erase(constant_predicate.get_negated());
    }
    return non_constants;
}
