#include "predicates.h"
#include "symbolic/utils.h"

template<>
std::ostream& operator<<(std::ostream &out, const Outcome<TypedAtom>& o)
{
    out << o.probability << " ";
    if (o.noisy) {
        out << "<noise>";
    } else {
        out << Predicates::transform_to_nontyped(o); // *static_cast<const PredicateGroup<TypedAtom>*>(&o);
    }
    return out;
}

template<>
std::ostream& operator<<(std::ostream &out, const Outcome<NonTypedAtom>& o)
{
    out << o.probability << " ";
    if (o.noisy) {
        out << "<noise>";
    } else {
        out << *static_cast<const PredicateGroup<NonTypedAtom>*>(&o);
    }
    return out;
}

template<>
std::ostream& operator<<(std::ostream &out, const Outcome<IndexedAtom>& o)
{
    out << o.probability << " ";
    if (o.noisy) {
        out << "<noise>";
    } else {
        out << *static_cast<const PredicateGroup<IndexedAtom>*>(&o);
    }
    return out;
}

template<>
void PredicateGroup<NonTypedAtom>::write_PPDDL(std::ostream &out, const std::vector<PPDDL::FullObject>& rule_params) const
{
    // Maximun of 15 predicates per line (to not exceed character limit in editors)
    uint number_predicates = 0;
    out << "(and ";
    for (typename PredicateGroup<NonTypedAtom>::const_iterator it_pred = this->begin() ; it_pred != this->end(); ++it_pred) {
        it_pred->write_PPDDL(out);
        out << " ";
        if (number_predicates++ > 20) { // max predicates = 20 (performance issues)
            number_predicates = 0;
            out << std::endl;
        }
    }
    out << ")";
}

template<>
void PredicateGroup<TypedAtom>::write_PPDDL(std::ostream &out, const std::vector<PPDDL::FullObject>& rule_params) const
{
    std::vector<PPDDL::FullObject> pred_group_params = get_params();
    std::vector<PPDDL::FullObject> deictic_params;
    BOOST_FOREACH(const PPDDL::FullObject& pred_param, pred_group_params) {
        if (!utils::belongs_to_vector<PPDDL::FullObject>(pred_param, rule_params)) {
            deictic_params.push_back(pred_param);
        }
    }
    
    // (exists (?X - xpos ?Y ?Y2 - ypos) (and .... ))
    if (!deictic_params.empty()) {
        out << "(exists (";
        BOOST_FOREACH(const PPDDL::FullObject& deictic_param, deictic_params) {
            out << deictic_param.name << " - " << deictic_param.type << " ";
        }
        out << ") ";
    }
    Predicates::transform_to_nontyped(*this).write_PPDDL(out, rule_params);
    if (!deictic_params.empty()) {
        out << ")";
    }
}

template<>
Outcome<TypedAtom>::Outcome(std::string line):
    is_dangerous(false)
{
    std::string item;
    noisy = false;

    // remove leading spaces
    line = line.substr(line.find_first_not_of(" \t"));
    std::stringstream ss(line);

    if (std::getline(ss, item, ' ')) {
        //std::cout << "Probability: " << item << std::endl;
    }
    probability = atof(item.c_str());

    if (ss.eof())
        return;

    // get rest of the line
    std::getline(ss, item);
    if (item.find("<noise>") != std::string::npos) {
        noisy = true;
    } else if (item.find("<no-change>") != std::string::npos) {

    } else {
        replace_predicates(PredicateGroup<TypedAtom>(item));
    }
}


// NON-CLASS FUNCTIONS
namespace Predicates {
PredicateGroup<TypedAtom> read_predicates_from_file(const std::string& file_path)
{
    LOG(WARNING) << "Deprecated";
    std::string predicates_string;

    std::ifstream predicates_file(file_path.c_str());
    if (predicates_file.is_open()) {
        if (predicates_file.good()) {
            std::getline(predicates_file, predicates_string);
        }
        predicates_file.close();
    } else LOG(ERROR) << "Unable to open file: " << file_path;

    return PredicateGroup<TypedAtom>(predicates_string);
}

PredicateGroup<TypedAtom> transform_to_typed(const PredicateGroup<NonTypedAtom>& non_typed_preds, const PPDDLObjectManager<PPDDL::FullObject>& object_manager)
{
    PredicateGroup<TypedAtom> result;
    BOOST_FOREACH(const Predicate<NonTypedAtom>& non_typed_pred, non_typed_preds) {
        assert(!non_typed_pred.is_symbolic());
        Predicate<TypedAtom> typed_pred(TypedAtom(non_typed_pred.name, std::vector<std::string>(), std::vector<std::string>()), non_typed_pred.positive);
        BOOST_FOREACH(const std::string& param_name, non_typed_pred.get_param_names()) {
            typed_pred.params.push_back(PPDDL::FullObject(param_name, object_manager.get_object_type(param_name)));
        }
        result.insert(typed_pred);
    }
    return result;
}

PredicateGroup<NonTypedAtom> transform_to_nontyped(const PredicateGroup<TypedAtom>& typed_preds)
{
    PredicateGroup<NonTypedAtom> result;
    BOOST_FOREACH(const Predicate<TypedAtom>& typed_pred, typed_preds) {
        result.insert(Predicate<NonTypedAtom>(symbolic::transform_to_nontyped(typed_pred), typed_pred.positive));
    }
    return result;
}

void update_object_types(PredicateGroup<TypedAtom>& preds_to_update, PPDDLObjectManager<PPDDL::FullObject>& object_manager)
{
    PredicateGroup<TypedAtom> result;
    BOOST_FOREACH(Predicate<TypedAtom> new_pred_updated, preds_to_update) {
        BOOST_FOREACH(PPDDL::FullObject& param, new_pred_updated.params) {
            param.type = object_manager.get_object_type(param.name);
        }
        result.insert(new_pred_updated);
    }
    result.swap(preds_to_update);
}

// Predicate<Atom> get_predicate_from_string(std::string item)
// {
//     std::string aux;
//     bool positive;
// 
//     item = utils::trim(item);
// //     std::cout << "Generating predicate for: " << item << std::endl;
// 
//     aux = item.substr(0, 1);
//     if (aux.compare("-") == 0) {
//         item = item.substr(1);
//         positive = false;
//     } else
//         positive = true;
// 
//     return Predicate<Atom>(Atom(item), positive);
// }

}


