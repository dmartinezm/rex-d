/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2013  David Martínez <dmartinez@iri.upc.edu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "scenario.h"


Scenario::Scenario(const Typed::RuleSet& rules, const State& state, const RewardFunctionGroup& goals, 
                   const ConfigReader& config_reader
                  ):
    domain(config_reader),
    config_reader(config_reader),
    rules(rules),
    state(state),
    goals(goals),
    current_reward(0)
{
    init();
}

Scenario::Scenario(const Typed::RuleSet& rules_, const std::string& pddl_problem_file, const ConfigReader& config_reader):
    domain(config_reader),
    config_reader(config_reader),
    rules(rules_),
    current_reward(0)
{
    read_ppddl_problem_file(pddl_problem_file);
    init();
}

Scenario::Scenario(const std::string& pddl_domain_file, const std::string& pddl_problem_file, const ConfigReader& config_reader):
    domain(config_reader),
    config_reader(config_reader),
    current_reward(0)
{
    read_ppddl_problem_file(pddl_problem_file);
    read_ppddl_domain_file(pddl_domain_file);
    init();
}

void Scenario::init()
{
    if (domain.get_ppddl_types().empty()) {
        BOOST_FOREACH(const PPDDLObjectManager<PPDDL::FullObject>::TypeObjectsMap::value_type& type_objects, state.ppddl_object_manager.get_ppddl_objects_per_type())
        {
            domain.index_type(type_objects.first);
        }
    }
    
    // Always index first the state, as it has the constants
    domain.index_state(state);
    domain.index_goals(goals);
    domain.index_rules(rules);
    
    if (config_reader.get_variable<bool>("noop_actions_allowed")) {
        domain.index_action(MAKE_TYPED_ATOM_NOOP_ACTION);
    }
    
    if (config_reader.get_variable<bool>("final_reward_action")) { // add -finished precondition
        this->rules.add_predicate_to_preconditions(Predicate<TypedAtom>(MAKE_TYPED_ATOM_FINISHED, false));
    }
    
    this->noop_reward = config_reader.get_variable<float>("planning_noop_reward");
    this->planning_horizon = config_reader.get_variable<uint>("planning_horizon");
    
    if (config_reader.get_variable<bool>("final_reward_action")) {
        std::vector<TypedAtom>::const_iterator finish_action_it = std::find(get_action_list().begin(), get_action_list().end(), MAKE_TYPED_ATOM_FINISH_ACTION);
        if (finish_action_it == get_action_list().end()) { // if finish_action wasn't found, load it manually
            // DEPRECATED This is for compatibility with old way of define domains
            LOG(WARNING) << START_COLOR_RED << "Special action " << MAKE_TYPED_ATOM_FINISH_ACTION << " not found!!" << END_COLOR;
            LOG(ERROR) << START_COLOR_RED << "Required because \"final_reward_action\" parameter is activated." << END_COLOR;
            LOG(ERROR) << "Rules are " << rules;
            exit(-1);
        }
    }
}

void Scenario::reset() { 
    current_reward = 0;
    this->planning_horizon = config_reader.get_variable<uint>("planning_horizon");
}

void Scenario::set_rules(const Typed::RuleSet& rules_) {
    this->rules = rules_;
    if (config_reader.get_variable<bool>("final_reward_action")) { // add -finished precondition
        this->rules.add_predicate_to_preconditions(Predicate<TypedAtom>(MAKE_TYPED_ATOM_FINISHED, false));
    }
    domain.index_rules(rules);
}

void Scenario::set_state(const State& state_) {
    this->state = state_;
    domain.index_state(state);
}

void Scenario::set_goals(const RewardFunctionGroup& goals) {
    this->goals = goals;
    domain.index_goals(goals);
}

bool Scenario::is_in_goal() const {
    return goals.is_goal(state);
};

bool Scenario::apply_rule(const Typed::GroundedRule& grounded_rule, State& new_state, bool& success, float& reward, const bool force_outcome, const int force_outcome_idx) const
{
    // Apply rule reward before effects. 
    // Note that reward is only obtained if goal preconditions are satisfied (goal preconditions have to be properly defined)
    reward += grounded_rule.goals.obtain_reward(state, grounded_rule);
    
    // Then, apply rule effects
    bool result = false;
    if ((!grounded_rule.get_preconditions().is_satisfied_by(state)) || (!grounded_rule.outcomes.changes_predicates())) { 
        result = false;
    } else if (force_outcome) {
        OutcomeGroup<TypedAtom>::const_iterator outcome_it;
        if (force_outcome_idx < 0) { // autoselect outcome
            outcome_it = grounded_rule.outcomes.begin();
            while ( (outcome_it != grounded_rule.outcomes.end()) && outcome_it->empty() ) {
                outcome_it++;
            }
        }
        else {
            outcome_it = grounded_rule.outcomes.begin();
            std::advance(outcome_it, force_outcome_idx);
        }
        if (outcome_it != grounded_rule.outcomes.end()) { // check if valid outcome was selected
            outcome_it->apply(new_state);
        }
        success = true;
        result = true;
    } else {
        double rand_01;
        rand_01 = ((double) rand() / (RAND_MAX));

        double accumulated_prob = 0.0;

        OutcomeGroup<TypedAtom>::const_iterator outcome_it = grounded_rule.outcomes.begin();
        size_t out_idx = 1;
        accumulated_prob += outcome_it->probability;
        while (accumulated_prob < rand_01) {
            ++outcome_it;
            ++out_idx;
            accumulated_prob += outcome_it->probability;
        }
        outcome_it->apply(new_state);
        if (out_idx == 1) // first outcome is the successful one
            success = true;
        else
            success = false;

        result = true;
    }
    
    return result;
}

bool Scenario::transition_next_state_simple(const TypedAtom& action)
{
    bool success;
    float reward;
    return transition_next_state(action, success, reward);
}

bool Scenario::transition_next_state(const TypedAtom& action, bool& success, float& reward, const bool force_outcome, const int force_outcome_idx)
{
    int DEBUG = 0;
    CLOG_IF(DEBUG >=1, INFO, "trivial") << "State before: " << get_state();
    CLOG_IF(DEBUG >=1, INFO, "trivial") << "Action applied: " << action;
    
    reward = 0;
    // First, apply global rewards
    reward += goals.obtain_reward(state, action);
    
    State copied_state(get_state());
    
    // find if variables have default value
    // WARNING Only default = false is working
    PredicateGroup<TypedAtom> default_literal_values = RuleUtils::get_literal_defaults_from_ruleset(rules);
    BOOST_FOREACH(const Predicate<TypedAtom>& default_literal, default_literal_values) {
        for (State::iterator predicate_it = copied_state.begin(); predicate_it != copied_state.end(); ) {
            if (predicate_it->symbolic_compare(default_literal.get_negated())) {
                copied_state.erase(predicate_it++);
            }
            else {
                ++predicate_it;
            }
        }
    }
    
    success = false;
    bool rules_applied = false;
    if (action.name != ACTION_NAME_NOOP) {
        Typed::RuleSet grounded_action_rules = rules.get_grounded_action_rules(action, state);
        BOOST_FOREACH(Typed::RulePtr rule_ptr, grounded_action_rules) {
            #if __cplusplus > 199711L // C++11
                Typed::GroundedRulePtr grounded_rule_ptr = boost::make_shared<Typed::GroundedRule>(*static_cast<const Typed::GroundedRule*>(&(*rule_ptr)));
                if (apply_rule(*grounded_rule_ptr, copied_state, success, reward, force_outcome, force_outcome_idx)) {
                    rules_applied = true;
                }
            #else
                GroundedRule<TypedAtom> grounded_rule = *static_cast<const Typed::GroundedRule*>(&(*rule_ptr));
                if (apply_rule(grounded_rule, copied_state, success, reward, force_outcome, force_outcome_idx)) {
                    rules_applied = true;
                }
            #endif
        }
    }
    else {
        success = true;
        rules_applied = true;
    }
    CLOG_IF(DEBUG >=2, INFO, "trivial") << "State after applying only action: " << copied_state;
    
    // apply all noaction rules
    CLOG_IF(DEBUG >=2, INFO, "trivial") << "Obtaining noaction rules";
    Typed::RuleSet grounded_no_action_rules = rules.get_grounded_action_rules(MAKE_TYPED_ATOM_NOACTION, state);
    BOOST_FOREACH(Typed::RulePtr rule_ptr, grounded_no_action_rules) {
        Typed::GroundedRule grounded_rule = *static_cast<const Typed::GroundedRule*>(&(*rule_ptr));
        bool success_aux; // not used, just to fill params
        CLOG_IF(DEBUG >=2, INFO, "trivial") << "Applying noaction" << grounded_rule;
        apply_rule(grounded_rule, copied_state, success_aux, reward, false, 0);
        CLOG_IF(DEBUG >=2, INFO, "trivial") << "State after applying noaction: " << copied_state;
    }
    
    set_state(copied_state);
    current_reward += reward;
    CLOG_IF(DEBUG >=1, INFO, "trivial") << "State after: " << get_state();

    return rules_applied;
}

void Scenario::read_ppddl_domain_file(const std::string& ppddl_file_path)
{
    PDDLFileReader pddl_file_reader(ppddl_file_path);
    // types
    std::vector<PPDDL::FullObject::Type> ppddl_types;
    pddl_file_reader.parse_domain_file(ppddl_types);
    BOOST_FOREACH(const PPDDL::FullObject::Type& ppddl_type, ppddl_types) {
        domain.index_type(ppddl_type);
    }
    //rules
    pddl_file_reader.parse_domain_file(this->rules);
}

void Scenario::read_ppddl_problem_file(const std::string& ppddl_file_path)
{
    PDDLFileReader pddl_file_reader(ppddl_file_path);
    pddl_file_reader.parse_problem_file(this->state, this->goals);
}

void Scenario::write_PDDL_domain(const std::string& file_path)
{
    std::stringstream ss;
    write_PDDL_domain(ss);
    utils::write_string_to_file(file_path, ss.str());
}

void Scenario::write_PDDL_problem(const std::string& file_path)
{
    std::stringstream ss;
    write_PDDL_problem(ss);
    utils::write_string_to_file(file_path, ss.str());
}

bool compare_rule_shared_ptr(const Typed::RulePtr& l, const Typed::RulePtr& r) { return (*l < *r); } // used below with std::sort

void Scenario::write_PDDL_domain(std::ostream& out)
{
    std::sort(rules.begin(), rules.end(), compare_rule_shared_ptr); // This is used just so that the output is consistent, needed for automatic tests
    out << "(define (domain " << config_reader.get_variable<std::string>("domain") << ")" << std::endl;
    out << "  (:requirements :typing :strips :equality :probabilistic-effects :rewards)" << std::endl;
    // types
    out << "  (:types";
    BOOST_FOREACH(PPDDL::FullObject::Type pddl_type, domain.get_ppddl_types()) {
        out << " " << pddl_type;
    }
    out << ")" << std::endl;
    // predicates
    domain.write_PPDDL_symbolic_predicates(out, state.ppddl_object_manager);
    // rules
    rules.write_PPDDL(out);
    out << ")" << std::endl;
}

void Scenario::write_PDDL_problem(std::ostream& out)
{
    out << "(define (problem " << config_reader.get_variable<std::string>("problem") << ")" << std::endl;
    out << "  (:domain " << config_reader.get_variable<std::string>("domain") << ")" << std::endl;
    // objects
    out << "  (:objects ";
    BOOST_FOREACH(const PPDDLObjectManager<PPDDL::FullObject>::TypeObjectsMap::value_type& objects_per_type, state.ppddl_object_manager.get_ppddl_objects_per_type()) {
        BOOST_FOREACH(const std::string& object, objects_per_type.second) {
            out << object << " ";
        }
        out << " - ";
        out << objects_per_type.first << " ";
    }
    out << ")" << std::endl;
    // constants
    out << "  (:constants ";
    Predicates::transform_to_nontyped(state.get_constants()).write_simplified_PPDDL(out);
    out << ")" << std::endl;
    // init
    out << "  (:init ";
    Predicates::transform_to_nontyped(state).write_simplified_PPDDL(out);
    out << ")" << std::endl;
    assert(goals.size() == 1);
    goals.write_PPDDL_problem(out);
    // metric
    out << "  (:metric maximize (reward))" << std::endl;
    out << ")" << std::endl;
}


