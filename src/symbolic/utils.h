/*
 * Useful and general functions used in the code.
 * Copyright (C) 2016  David Martínez (dmartinez@iri.upc.edu)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef IRI_RULE_LEARNER_UTILS_H
#define IRI_RULE_LEARNER_UTILS_H 1

#include <algorithm>
#include <vector>
#include <set>
#include <fstream>
#include <sstream>
#include <errno.h>
#include <iostream>
#include <iomanip>
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>
#include <boost/foreach.hpp>
#include "boost/filesystem/path.hpp"
#include <boost/functional/hash.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/iterator/counting_iterator.hpp>

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Weffc++"
#include <symbolic/easylogging++.h>
#pragma GCC diagnostic pop

#define START_COLOR_RED    "\033[1;31m"
#define START_COLOR_GREEN  "\033[1;32m"
#define START_COLOR_YELLOW "\033[1;33m"
#define START_COLOR_BLUE   "\033[1;34m"
#define START_COLOR_PURPLE "\033[1;35m"
#define START_COLOR_CYAN   "\033[1;36m"
#define START_COLOR_WHITE  "\033[1;37m"
#define END_COLOR          "\033[0m"

namespace utils
{
    // utils
    bool is_close(const float f1, const float f2);
    bool is_close(const double d1, const double d2);
    bool is_significantly_less(const float f1, const float f2);
    bool is_significantly_less(const double d1, const double d2);
    bool is_number(const std::string& s);
    bool is_absolute_path(const std::string& path);
    bool compare_vector_strings(const std::vector<std::string>& v1, const std::vector<std::string>& v2);
    bool compare_ordered_vector_strings(const std::vector<std::string>& v1, const std::vector<std::string>& v2);
    char choose_variable_separator(const std::string& str);
    
    // std::string helpers
    std::string trim(const std::string& str, const std::string& whitespace = " \t");
    std::string remove_spaces(std::string str);
    std::vector<std::string> split(const std::string& str, const char separator = ' ');
    
    std::string get_file_contents(const std::string& file_path, const bool ignore_warnings = false);
    std::string get_file_contents(const std::string& file_path, const std::string& init_keyword , const std::string& end_keyword, const bool ignore_warnings = false);
    void write_string_to_file(const std::string& file_path, const std::string& out_str, bool quiet = true);
    void append_string_to_file(const std::string& file_path, const std::string& out_str, bool quiet = true);
    
    /**
     * \brief Initialize random seed to use std::rand()
     * 
     * Using time miliseconds
     */
    void initialize_random_seed();
    
    /**
     * \brief Hoeffding inequality confidence
     * 
     * Hoeffding inequality
     * http://en.wikipedia.org/wiki/Hoeffding%27s_inequality
     * \alpha <= 1 - (2 * e ^ (-2 n t^2))
     * n = number samples
     * t = confidence interval
     */
    float get_hoeffding_inequality_confidence(const int number_samples, const float confidence_interval);
    
    /**
     * \brief Converts string to char*
     * 
     * The difference with the c_str() method is that the returned char* is not const.
     */
    char* string_to_char(const std::string& str);
    
    std::string get_parsed_time_from_seconds(const ulong total_seconds);
    
    void print_progress(const uint current_progress, const uint total);
    
    /**
     * \brief Get port from current dir hash
     * 
     * \param port_index each port_index will be in a different range (0:[20.000,40.000], 1:[40.000,60.000], 2+:undefined because unix max port is 65.535)
     */
    uint get_socket_from_current_dir_hash(ushort port_index = 0);
    
    /**
     * \brief wait until enter is pressed
     */
    void wait_for_enter_key();
    
    /**
     * \brief Reads a string from input_stream
     */
    std::string read_string_from_stream(std::istream& input_stream);
    
    /**
     * \brief Sends notification through libnotify
     * 
     * Using bash command instead inside a try-catch.\n
     * If libnotify is not present just a warning is shown.\n
     * 
     * TODO: Should be an optional compile feature instead with C++ API.\n
     * Not using C++ API, don't want a mandatory dependency on libnotify.
     * 
     */
    void send_notification_msg(const std::string& msg, const std::string& main_msg);
    
    /**
     * \brief Gets executable path
     * 
     * Only works on Linux
     */
    std::string get_selfpath();

//     void print_std_vector_string(std::vector<std::string> strings, std::ostream &out);

    void init_loggers(const std::string& config_path);

    template <typename T>
    void remove_duplicates(std::vector<T>& vec)
    {
        std::sort(vec.begin(), vec.end());
        vec.erase(std::unique(vec.begin(), vec.end()), vec.end());
    }

    template<class T>
    bool belongs_to_vector(const T& obj, const std::vector<T>& v)
    {
        return (std::find(v.begin(), v.end(), obj) != v.end());
    }
    
    template <typename I>
    I get_random_element(I begin, I end) {
        const uint n = std::distance(begin, end);
        int k;
        k = std::rand() % n;
        std::advance(begin, k);
        return begin;
    }
    
    // Thanks to http://stackoverflow.com/questions/9501742/c-next-permutation-algorithm
    template <typename Iterator>
    inline bool next_combination(const Iterator first, Iterator k, const Iterator last)
    {
        if ((first == last) || (first == k) || (last == k))
            return false;
        Iterator itr1 = first;
        Iterator itr2 = last;
        ++itr1;
        if (last == itr1)
            return false;
        itr1 = last;
        --itr1;
        itr1 = k;
        --itr2;
        while (first != itr1)
        {
            if (*--itr1 < *itr2)
            {
                Iterator j = k;
                while (!(*itr1 < *j)) ++j;
                std::iter_swap(itr1,j);
                ++itr1;
                ++j;
                itr2 = k;
                std::rotate(itr1,j,last);
                while (last != j)
                {
                    ++j;
                    ++itr2;
                }
                std::rotate(k,itr2,last);
                return true;
            }
        }
        std::rotate(first,k,last);
        return false;
    }
    
    template<typename T>
    std::vector< std::vector<T> > get_all_combinations(const std::vector<T> orig_vector, const uint combination_size) {
        if (orig_vector.size() < combination_size) {
            return std::vector< std::vector<T> >();
        }
        std::vector< std::vector<T> > results;
        // C++11
//         std::vector<int> v(orig_vector.size());
//         std::iota (std::begin(v), std::end(v), 0);
        std::vector<int> v( boost::counting_iterator<int>( 0 ),
                       boost::counting_iterator<int>( orig_vector.size() ) );
        
        std::vector<int>::iterator v_it_k_combination;
        do {
            std::vector<int>::iterator v_it_end_permutation;
            do {
                std::vector<T> new_result;
                for (int i = 0; i < combination_size; ++i) {
                    new_result.push_back(orig_vector[v[i]]);
                }
                results.push_back(new_result);
                v_it_end_permutation = v.begin();
                std::advance(v_it_end_permutation, combination_size);
            } while(std::next_permutation(v.begin(), v.begin() + combination_size));
            v_it_k_combination = v.begin();
            std::advance(v_it_k_combination, combination_size);
        } while(next_combination(v.begin(), v.begin() + combination_size, v.end()));
        return results;
    }
    
    template<typename T>
    std::vector< std::vector<T> > get_all_permutations(const std::vector<T> orig_vector, const uint combination_size) {
        if (orig_vector.size() < combination_size) {
            return std::vector< std::vector<T> >();
        }
        std::vector< std::vector<T> > results;
        std::vector<bool> v(orig_vector.size());
        std::fill(v.begin() + orig_vector.size() - combination_size, v.end(), true);

        do {
            std::vector<T> new_result;
            for (int i = 0; i < (int)orig_vector.size(); ++i) {
                if (v[i]) {
                    new_result.push_back(orig_vector[i]);
                }
            }
            results.push_back(new_result);
        } while (std::next_permutation(v.begin(), v.end()));
        return results;
    }
    
    struct timeval start_timer();
    double get_elapsed_time(struct timeval& timer_start);

} // namespace utils


typedef std::vector<std::string> vector_string;

namespace std
{   
    template<class T>
    ostream & operator<<(ostream &stream, const vector<T> &vec)
    {
        stream << "[";
        for (size_t i = 0; i < vec.size(); ++i) {
            if (i != 0)
                stream << ", ";
            stream << vec[i];
        }
        stream << "]";
        return stream;
    }

    template<class T, class V>
    ostream & operator<<(ostream &stream, const pair<T,V> &a_pair)
    {
        stream << "{";
        stream << a_pair.first;
        stream << ", " << a_pair.second;
        stream << "}";
        return stream;
    }

    template<class T>
    vector<T> vector_intersection(vector<T> v1, vector<T> v2)
    {
    //         int first[] = {5,10,15,20,25};
    //         int second[] = {50,40,30,20,10};
        std::vector<T> v(v1.size() + v2.size());                      // 0  0  0  0  0  0  0  0  0  0
        typename std::vector<T>::iterator it;

        std::sort(v1.begin(), v1.end());     //  5 10 15 20 25
        std::sort(v2.begin(), v2.end());   // 10 20 30 40 50

        it = std::set_intersection(v1.begin(), v1.end(), v2.begin(), v2.end(), v.begin());
        // 10 20 0  0  0  0  0  0  0  0
        v.resize(it - v.begin());                    // 10 20
        return v;
    }
}


#endif
