#include "symbols.h"

#ifndef REXD_SYMBOLIC_TEMPLATE_IMP_H
#define REXD_SYMBOLIC_TEMPLATE_IMP_H 1

#include "symbolic/utils.h"

// OPERATORS

namespace PPDDL {
    template<typename BaseType>
    bool operator== (const Object<BaseType> &o1, const Object<BaseType> &o2) {
        return ((o1.name == o2.name) && (o1.type == o2.type));
    }
    
    template<typename BaseType>
    bool operator<(const Object<BaseType>& l, const Object<BaseType>& r) {
        if  (l.type < r.type) {
            return true;
        }
        else if (r.type < l.type) {
            return false;
        }
        else {
            return l.name < r.name;
        }
    }
    
    template<typename BaseType>
    std::ostream& operator<<(std::ostream &out, const PPDDL::Object<BaseType>& o) {
        out << o.name;
        out << " - ";
        out << o.type;
        return out;
    }
}

template<typename ObjectT>
bool operator== (const BaseTypedAtom<ObjectT> &s1, const BaseTypedAtom<ObjectT> &s2)
{
    return ((s1.name == s2.name) && (s1.params == s2.params));
}

// bool operator!= (const FullAtom &s1, const FullAtom &s2)
// {
//     return !(s1 == s2);
// }

template<typename ObjectT>
bool operator!= (const BaseTypedAtom<ObjectT> &s1, const BaseTypedAtom<ObjectT> &s2)
{
    return !(s1 == s2);
}

template<typename ObjectT>
bool BaseTypedAtom<ObjectT>::symbolic_compare(const BaseTypedAtom<ObjectT> &s) const
{
    bool equal = (s.name == this->name);
    if (this->is_symbolic() || s.is_symbolic()) { // if symbolic compare number of params
        equal = (equal && (this->params.size() == s.params.size()));
        if (equal) { // compare types
            for (size_t param_idx = 0; param_idx < this->params.size(); ++param_idx) {
                equal = (equal && (this->params[param_idx].type == s.params[param_idx].type));
            }
        }
    } else { // if grounded compare params content
        equal = (equal && (this->params == s.params));
    }
    return equal;
}

template<typename ObjectT>
bool operator<(const BaseTypedAtom<ObjectT>& l, const BaseTypedAtom<ObjectT>& r)
{
    if (l.name < r.name) {
        return true;
    } else if (r.name < l.name) {
        return false;
    } else { //equal
        return (l.params < r.params);
    }
}

template<typename ObjectT>
std::ostream& operator<<(std::ostream &out, const FullAtom<ObjectT> &s)
{
    // equivalent to NonTypedAtom
    out << s.name << "(";
    for (size_t i = 0; i < s.params.size(); i++) {
        out << s.params[i].name;
        if ((i + 1) < s.params.size())
            out << " ";
    }
    out << ")";
    return out;
}

template<typename ObjectT>
std::ostream& operator<<(std::ostream &out, const BaseTypedAtom<ObjectT> &s)
{
    out << s.name << "(";
    s.write_typed_parameters(out);
    out << ")";
    return out;
}


template<typename ObjectT>
std::ostream& operator<<(std::ostream &out, const PPDDLObjectManager<ObjectT> &pom)
{
    out << "[";
    BOOST_FOREACH(typename PPDDLObjectManager<ObjectT>::ObjectTypeMap::value_type object_type_pair, pom.ppddl_objects_types) {
        out << object_type_pair.first << " - " << object_type_pair.second << ", ";
    }
    out << "]";
    return out;
}

template<typename ObjectT>
void BaseTypedAtom<ObjectT>::write_typed_parameters(std::ostream &out) const
{
    for (size_t i = 0; i < this->params.size(); i++) {
        out << this->params[i];
        if ((i + 1) < this->params.size())
            out << " ";
    }
}

template<typename ObjectT>
void FullAtom<ObjectT>::set_param_names(const std::vector<FullAtom::ParamType>& new_names) { 
    assert(params.size() == new_names.size()); 
    for(size_t i = 0; i < params.size(); ++i) { 
        params[i].name = new_names[i].name; 
    }  
}

template<typename ObjectT>
void FullAtom<ObjectT>::make_symbolic()
{
    symbolic::make_vector_params_symbolic(params);
}

template<typename ObjectT>
bool BaseTypedAtom<ObjectT>::has_param(const ObjectT &required_param) const
{
    bool result = false;
    BOOST_FOREACH(const ObjectT& param, this->params) {
        if (param == required_param) {
            result = true;
        }
    }
    return result;
}

template<typename ObjectT>
int BaseTypedAtom<ObjectT>::get_param_name_idx(const typename ObjectT::Name &param) const
{
    int result = -1;
    for (size_t i = 0; i < this->params.size(); i++) {
        if (this->params[i].name == param) {
            result = i;
        }
    }
    return result;
}

template<typename ObjectT>
bool BaseTypedAtom<ObjectT>::params_belong_to(const std::vector<ObjectT> &list_of_params) const
{
    bool result = true;
    BOOST_FOREACH(const ObjectT& param, this->params) {
        bool present = false;
        for (size_t j = 0; j < list_of_params.size(); j++) {
            if (param == list_of_params[j]) {
                present = true;
            }
        }
        if (!present) {
            result = false;
        }
    }
    return result;
}

template<typename ObjectT>
void BaseTypedAtom<ObjectT>::ground(std::vector<typename ObjectT::Name> variables)
{
    for (size_t i = 0; i < this->params.size(); ++i) {
        if (variables.size() > i) {
            this->params[i].name = variables[i];
        } else {
            std::cout << "\033[1;31m" << "Atoms:: ground - Incorrect number of parameters." << "\033[0m" << std::endl;
            std::cout << "\033[1;31m" << "Atom params are: " << this->params << "\033[0m" << std::endl;
            std::cout << "\033[1;31m" << "Arguments are: " << variables << "\033[0m" << std::endl;
        }
    }
}

template<typename ObjectT>
bool BaseTypedAtom<ObjectT>::ground(std::vector<ObjectT> variables)
{
    for (size_t i = 0; i < this->params.size(); ++i) {
        if (variables.size() > i) {
            if (this->params[i].type == variables[i].type) {
                this->params[i].name = variables[i].name;
            }
            else {
                LOG(WARNING) << "WARNING probably error!";
                return false;
            }
        } else {
            std::cout << "\033[1;31m" << "Atoms:: ground - Incorrect number of parameters." << "\033[0m" << std::endl;
            std::cout << "\033[1;31m" << "Atom params are: " << this->params << "\033[0m" << std::endl;
            std::cout << "\033[1;31m" << "Arguments are: " << variables << "\033[0m" << std::endl;
        }
    }
    return true;
}

template<typename ObjectT>
void BaseTypedAtom<ObjectT>::change_param_name(const typename ObjectT::Name& orig_var, const typename ObjectT::Name& new_var)
{
    BOOST_FOREACH(ObjectT& param, this->params) {
        if (param.name == orig_var) {
            param.name = new_var;
        }
    }
}

template<typename ObjectT>
void BaseTypedAtom<ObjectT>::change_param_names(const std::vector<typename ObjectT::Name>& orig_var_v, const std::vector<typename ObjectT::Name>& new_var_v)
{
    BOOST_FOREACH(ObjectT& param, this->params) {
        for (size_t i = 0; i < orig_var_v.size(); ++i) {
            if (param.name == orig_var_v[i]) {
                param.name = new_var_v[i];
            }
        }
    }
}

template<typename ObjectT>
void BaseTypedAtom<ObjectT>::change_param(const ObjectT& orig_var, const ObjectT& new_var)
{
    BOOST_FOREACH(ObjectT& param, this->params) {
        if (param == orig_var) {
            param = new_var;
        }
    }
}

template<typename ObjectT>
void BaseTypedAtom<ObjectT>::change_params(const std::vector<ObjectT>& orig_var_v, const std::vector<ObjectT>& new_var_v)
{
    BOOST_FOREACH(ObjectT& param, this->params) {
        for (size_t i = 0; i < orig_var_v.size(); ++i) {
            if (param == orig_var_v[i]) {
                param = new_var_v[i];
            }
        }
    }
}

template<typename ObjectT>
void BaseTypedAtom<ObjectT>::change_param_names(const std::vector<ObjectT>& orig_var_v, const std::vector<ObjectT>& new_var_v)
{
    BOOST_FOREACH(ObjectT& param, this->params) {
        for (size_t i = 0; i < orig_var_v.size(); ++i) {
            if (param.name == orig_var_v[i].name) {
                param.name = new_var_v[i].name;
            }
        }
    }
}

template<typename ObjectT>
std::vector<typename ObjectT::Name> FullAtom<ObjectT>::get_param_names() const
{
    std::vector<typename ObjectT::Name> param_names;
    BOOST_FOREACH(const ParamType& param, this->params) {
        param_names.push_back(param.name);
    }
    return param_names;
}

template<typename ObjectT>
std::vector<typename ObjectT::Type> FullAtom<ObjectT>::get_param_types() const
{
    std::vector<typename ObjectT::Type> param_types;
    BOOST_FOREACH(const ParamType& param, this->params) {
        param_types.push_back(param.type);
    }
    return param_types;
}


/**
 * TYPED ATOM
 */
template<typename ObjectT>
BaseTypedAtom<ObjectT>::BaseTypedAtom(const typename ObjectT::Name &name_, const std::vector<typename ObjectT::Name> &param_names_, const std::vector<typename ObjectT::Type> &param_types_)
{
    this->name = name_;
    assert(param_names_.size() == param_types_.size());
    for (size_t i = 0; i < param_names_.size(); ++i) {
        this->params.push_back(ObjectT(param_names_[i], param_types_[i]));
    }
}

template<typename ObjectT>
BaseTypedAtom<ObjectT>::BaseTypedAtom(const typename ObjectT::Name &name_, const std::vector<ObjectT> &ppddl_params)
{
    this->name = name_;
    this->params = ppddl_params;
}

template<typename ObjectT>
typename ObjectT::Type PPDDLObjectManager<ObjectT>::get_object_type(const typename ObjectT::Name& object) const
{
    typename ObjectTypeMap::const_iterator iter = ppddl_objects_types.find(object);
    if (iter != ppddl_objects_types.end()) {
        return iter->second;
    }
    else {
        return DEFAULT_OBJECT_TYPE;
    }
}

template<typename ObjectT>
std::vector< typename ObjectT::Name > PPDDLObjectManager<ObjectT>::get_type_objects(const typename ObjectT::Type& type) const
{
    typename TypeObjectsMap::const_iterator iter = ppddl_objects_per_type.find(type);
    if (iter != ppddl_objects_per_type.end()) {
        return iter->second;
    }
    else {
        return std::vector<typename ObjectT::Name>();
    }
}

template<typename ObjectT>
void PPDDLObjectManager<ObjectT>::add_objects(const PPDDLObjectManager& added_object_manager)
{
    BOOST_FOREACH(const typename ObjectTypeMap::value_type& object_type_pair, added_object_manager.ppddl_objects_types) {
        if (ppddl_objects_types.count(object_type_pair.first) == 0) { // object doesn't exist already
            ppddl_objects_types[object_type_pair.first] = object_type_pair.second;
            ppddl_objects_per_type[object_type_pair.second].push_back(object_type_pair.first);
        }
    }
}

template<typename ObjectT>
typename PPDDLObjectManager<ObjectT>::CombinationsPPDDLObjectsPtr PPDDLObjectManager<ObjectT>::get_combinations_of_objecs_for_given_params(const std::vector<typename ObjectT::Type>& list_param_types) const
{
    typename CacheCombinationsObjects::const_iterator cache_it = cache_combinations_objects.find(list_param_types);
    if (cache_it != cache_combinations_objects.end()) {
        return cache_it->second;
    }
    else {
        std::vector< std::vector< typename ObjectT::Name > > combinations_of_params;
        combinations_of_params.push_back(std::vector<typename ObjectT::Name>()); // add empty one to begin with
        BOOST_FOREACH(const typename ObjectT::Type& ppddl_type, list_param_types) { // for each parameter
            std::vector< std::vector< typename ObjectT::Name > > new_combinations_of_params;
            std::vector<typename ObjectT::Name> pddl_objects = get_type_objects(ppddl_type);
            BOOST_FOREACH(const typename ObjectT::Name& ppddl_object, pddl_objects) { // for each object of the parameter type
                BOOST_FOREACH(const std::vector< typename ObjectT::Name >& a_combination_of_params, combinations_of_params) { 
                    // create combinations: current_combinations x number_objects
                    // Don't repeat the same object in the arguments
                    if (std::find(a_combination_of_params.begin(), a_combination_of_params.end(), ppddl_object) == a_combination_of_params.end()) {
                        new_combinations_of_params.push_back(a_combination_of_params);
                        new_combinations_of_params.back().push_back(ppddl_object);
                    }
                }
            }
            combinations_of_params.swap(new_combinations_of_params);
        }
        
        CombinationsPPDDLObjectsPtr combinations_of_params_ptr =boost::make_shared<CombinationsPPDDLObjects>(combinations_of_params);
        cache_combinations_objects[list_param_types] = combinations_of_params_ptr;
        return combinations_of_params_ptr;
    }
}

#endif
