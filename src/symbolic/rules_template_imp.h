#include "rules.h"

#ifndef REXD_RULE_TEMPLATE_IMP_H
#define REXD_RULE_TEMPLATE_IMP_H 1


/**
 * OPERATORS
 *
 */

template<typename AtomT>
std::ostream& operator<<(std::ostream &out, const RuleSet<AtomT>& r)
{
    BOOST_FOREACH(const RulePtr<AtomT>& rule_ptr, r) {
        out << *rule_ptr;
    }

    // if no rules, write default rule
    if (r.empty()) {
        out << "ACTION:" << std::endl << "  default()" << std::endl
            << "CONTEXT:" << std::endl << "  --" << std::endl
            << "OUTCOMES:" << std::endl << "  0.5 <no-change>" << std::endl << "  0.5 <noise>" << std::endl;
    }

    return out;
}

template<typename AtomT>
bool operator==(const Rule<AtomT> &r1, const Rule<AtomT> &r2)
{
    bool result;
    result = (*static_cast<const AtomT*>(&r1) == r2);
    result = (result && (r1.deictic_params == r2.deictic_params));
    result = (result && (r1.preconditions == r2.preconditions));
    result = (result && (r1.outcomes == r2.outcomes));
    result = (result && (r1.goals == r2.goals));
    return result;
}

template<typename AtomT>
bool operator!=(const Rule<AtomT> &r1, const Rule<AtomT> &r2)
{
    return !(r1 == r2);
}

template<typename AtomT>
bool operator<(const Rule<AtomT>& l, const Rule<AtomT>& r)
{
    if (l.is_special() && (!r.is_special())) { // special actions go last
        return false;
    }
    else if ((!l.is_special()) && r.is_special()) {
        return true;
    }
    else if (*static_cast<const AtomT*>(&l) < r)
        return true;
    else if (*static_cast<const AtomT*>(&r) < l)
        return false;
    else if (l.deictic_params < r.deictic_params)
        return true;
    else if (r.deictic_params < l.deictic_params)
        return false;
    else if (l.outcomes < r.outcomes)
        return true;
    else if (r.outcomes < l.outcomes)
        return false;
    else if (l.preconditions < r.preconditions)
        return true;
    else if (r.preconditions < l.preconditions)
        return false;
    else
        return false;
}

template<typename AtomT>
bool operator>(const Rule<AtomT>& l, const Rule<AtomT>& r)
{
    return r < l;
}

template<typename AtomT>
bool operator<=(const Rule<AtomT>& l, const Rule<AtomT>& r)
{
    return ((l < r) || (!(l > r)));
}

template<typename AtomT>
bool operator>=(const Rule<AtomT>& l, const Rule<AtomT>& r)
{
    return ((l > r) || (!(l < r)));
}

template<typename AtomT>
bool operator==(const RuleSet<AtomT> &r1, const RuleSet<AtomT> &r2)
{
    if (r1.size() != r2.size()) {
        return false;
    }
    // make copies to sort them
    RuleSet<AtomT> r1_copy(r1), r2_copy(r2);
    std::sort(r1_copy.begin(), r1_copy.end(), [](RulePtr<AtomT> a, RulePtr<AtomT> b) { return *a < *b; });
    std::sort(r2_copy.begin(), r2_copy.end(), [](RulePtr<AtomT> a, RulePtr<AtomT> b) { return *a < *b; });
    
    typename RuleSet<AtomT>::iterator r1_it, r2_it;
    r2_it = r2_copy.begin();
    for (r1_it = r1_copy.begin(); r1_it != r1_copy.end(); ++r1_it) {
        if (**r1_it != **r2_it) {
            return false;
        }
        r2_it++;
    }
    return true;
}

template<typename AtomT>
bool operator!=(const RuleSet<AtomT> &r1, const RuleSet<AtomT> &r2)
{
    return !(r1 == r2);
}

template<typename AtomT>
RuleSet<AtomT>& RuleSet<AtomT>::operator=(const RuleSet<AtomT>& rhs) {
    this->clear();
    add_rules(rhs);
    return *this;
}

#if __cplusplus > 199711L // C++11
template<typename AtomT>
RuleSet<AtomT>& RuleSet<AtomT>::operator=(RuleSet<AtomT>&& rhs) {
    std::vector< boost::shared_ptr<Rule<AtomT> > >::operator =(rhs);
    return *this;
}
#endif

template<typename AtomT>
void Rule<AtomT>::write_PPDDL(std::ostream& out) const
{
    out << "  (:action " << this->name << std::endl;
    // parameters
    out << "   :parameters (";
    this->write_typed_parameters(out);
    out << ")" << std::endl;
    // preconditions
    out << "   :precondition ";
    if (!deictic_params.empty()) {
        out << "(exists ";
        out << "(";
        BOOST_FOREACH(const typename AtomT::ParamType& deictic_param, deictic_params) {
            out << deictic_param.name << " - " << deictic_param.type << " ";
        }
        out << ") ";
    }
    Predicates::transform_to_nontyped(preconditions).write_PPDDL(out, this->params);
    if (!deictic_params.empty()) {
        out << " )";
    }
    out << std::endl;
    // effects
    out << "   :effect (and ";
    outcomes.write_PPDDL(out, this->params);
    out << std::endl;
    goals.write_PPDDL(out, this->params);
    out << "           )" << std::endl;
    out << "  )" << std::endl;
}

template<typename AtomT>
void RuleSet<AtomT>::write_PPDDL(std::ostream& out) const
{
    BOOST_FOREACH(const RulePtr<AtomT>& rule_ptr, *this) {
        rule_ptr->write_PPDDL(out);
    }
}

/**
 * Rule
 *
 */

template<typename AtomT>
Rule<AtomT>::Rule(const AtomT &symbol, const PredicateGroup<AtomT> &preconditions, const OutcomeGroup<AtomT>& outcomes, const RewardFunctionGroup& goals) :
    AtomT(symbol),
    preconditions(preconditions),
    outcomes(outcomes),
    goals(goals),
    dangerous(false)
{
    update_deictic_params();
    static ulong current_identifier = 0;
    identifier = current_identifier++;
}

template<typename AtomT>
void Rule<AtomT>::add_outcome(const PredicateGroup<AtomT> predicates, const float probability)
{
    outcomes.insert(Outcome<AtomT>(predicates, probability));
}

template<typename AtomT>
void Rule<AtomT>::add_outcome(const Outcome<AtomT> outcome)
{
    outcomes.insert(outcome);
}

template<typename AtomT>
double Rule<AtomT>::probability_obtain_predicate(const Predicate<AtomT> predicate) const
{
    double result = 0.0;
    BOOST_FOREACH(const Outcome<AtomT>& outcome, this->outcomes) {
        result += outcome.probability_obtain_predicate(predicate);
    }
    return result;
}

template<typename AtomT>
void Rule<AtomT>::sanitize_probabities()
{
    // Adds or reduces probability of the empty outcome to get a sum of probabilities = 1.0
    float sum_probs = 0.0;
    typename OutcomeGroup<AtomT>::iterator empty_outcome_it = this->outcomes.end();
    for (typename OutcomeGroup<AtomT>::iterator it = this->outcomes.begin(); it != this->outcomes.end(); it++) {
        sum_probs += it->probability;
        if (!it->is_useful()) {
            empty_outcome_it = it;
        }
    }
    if (sum_probs < 1.0) { // add missing probability to empty outcome
        if (empty_outcome_it == this->outcomes.end()) {
            outcomes.insert(Outcome<AtomT>(1.0 - sum_probs));
        } else {
            Outcome<AtomT> empty_outcome(*empty_outcome_it);
            outcomes.erase(empty_outcome);
            empty_outcome.probability += 1.0 - sum_probs;
            outcomes.insert(empty_outcome);
        }
    } else { // remove exceding probability from first outcome
        Outcome<AtomT> first_outcome(*outcomes.begin());
        outcomes.erase(first_outcome);
        first_outcome.probability -= (sum_probs - 1.0);
        outcomes.insert(first_outcome);
    }
}

template<typename AtomT>
bool Rule<AtomT>::has_useful_outcome() const
{
    bool result = false;
    BOOST_FOREACH(const Outcome<AtomT>& outcome, this->outcomes) {
        if (outcome.changes_predicates()) {
            result = true;
        }
    }
    return result;
}

template<typename AtomT>
std::vector<std::string> Rule<AtomT>::get_param_and_deictic_names() const
{
    std::vector<std::string> symbolic_params_with_deictic = this->get_param_names();
    BOOST_FOREACH(const typename AtomT::ParamType& deictic_param, get_deictic_params()) {
        symbolic_params_with_deictic.push_back(deictic_param.name);
    }
    return symbolic_params_with_deictic;
}

template<typename AtomT>
uint Rule<AtomT>::count_covering_transitions_preconditions(const TransitionGroup<AtomT>& transitions) const
{
    int num = 0;
    for (typename TransitionSet<AtomT>::const_iterator transition_it = transitions.begin_set() ; transition_it != transitions.end_set(); ++transition_it) {
        if (covers_transition_preconditions(*transition_it, transitions.ppddl_object_manager) > 0.0) {
            num += transition_it->get_number_repetitions();
        }
    }
    return num;
}

template<typename AtomT>
void Rule<AtomT>::transform_nid_params_to_pddl_params() 
{
    for (size_t i = 0; i < DEFAULT_PARAM_NAMES_NUMBER; ++i) {
        change_variable(DEFAULT_NID_PARAM_NAMES[i], DEFAULT_PARAM_NAMES[i]);
    }
    update_deictic_params();
}

template<typename AtomT>
int Rule<AtomT>::get_number_literals() const
{
    int result = 0;
    result += preconditions.size();
    BOOST_FOREACH(const Outcome<AtomT>& outcome, outcomes) {
        result += outcome.size();
    }
    return result;
}

template<typename AtomT>
void Rule<AtomT>::add_negated_outcomes_as_preconditions()
{
    BOOST_FOREACH(const Outcome<AtomT>& outcome, this->outcomes) {
        BOOST_FOREACH(const Predicate<AtomT>& outcome_pred, outcome) {
            add_predicate_to_preconditions(outcome_pred.get_negated());
        }
    }
}

template<typename AtomT>
void Rule<AtomT>::set_preconditions(const PredicateGroup<AtomT>& new_preconditions) 
{ 
    preconditions = new_preconditions; 
    update_deictic_params();
}

template<typename AtomT>
void Rule<AtomT>::add_predicate_to_preconditions(const Predicate<AtomT>& new_predicate)
{
    BOOST_FOREACH(const typename AtomT::ParamType& param, new_predicate.params) {
        if (symbolic::is_param_symbolic(param.name)) {
            if (std::find(this->params.begin(), this->params.end(), param) == this->params.end() ) {
                deictic_params.insert(typename AtomT::ParamType(param.name, param.type));
            }
        }
    }
    preconditions.update_predicate(new_predicate);
    update_deictic_params();
}

template<typename AtomT>
void Rule<AtomT>::update_deictic_params()
{
    deictic_params.clear();
    if (!preconditions.empty()) {
        std::vector<typename AtomT::ParamType> all_vars_v = preconditions.get_params();
        std::set<typename AtomT::ParamType> action_vars_s(this->params.begin(), this->params.end());
        std::set<typename AtomT::ParamType> all_vars_s(all_vars_v.begin(), all_vars_v.end());
        
        std::set_difference(all_vars_s.begin(), all_vars_s.end(), 
                            action_vars_s.begin(), action_vars_s.end(),
                            std::inserter(deictic_params, deictic_params.end()));
    }
}

template<typename AtomT>
void Rule<AtomT>::add_predicate_to_outcomes(const Predicate<AtomT>& new_predicate)
{
    outcomes.add_predicate_to_outcomes(new_predicate);
}

template<typename AtomT>
std::vector<typename AtomT::ParamType> Rule<AtomT>::get_deictic_params_v() const
{
    return std::vector<typename AtomT::ParamType>(deictic_params.begin(), deictic_params.end());
}

template<typename AtomT>
void Rule<AtomT>::change_variable(const std::string& current_name, const std::string& new_name)
{
    (static_cast<TypedAtom*>(this))->change_param_name(current_name, new_name);
    preconditions.change_param_name(current_name, new_name);
    outcomes.change_param_name(current_name, new_name);
    goals.change_param_name(current_name, new_name);
}

template<typename AtomT>
void Rule<AtomT>::update_outcome_probabilities(const TransitionGroup<AtomT>& transitions)
{
    assert(outcomes.size() == 1);
    uint outcome_coverage = 0;
    uint empty_coverage = 0;
    for (typename TransitionSet<AtomT>::const_iterator transition_it = transitions.begin_set() ; transition_it != transitions.end_set(); ++transition_it) {
        if ( (transition_it->get_action().name == this->name) || (this->name == AtomT::get_no_action_name()) ) {
            RuleSet<AtomT> grounded_deictic_rules(get_pddl_grounded_rules(transition_it->get_prev_state(), transitions.ppddl_object_manager, false));
            BOOST_FOREACH(const RulePtr<AtomT>& grounded_deictic_rule_ptr, grounded_deictic_rules) {
                if ( (transition_it->get_action() == *grounded_deictic_rule_ptr) || (grounded_deictic_rule_ptr->name == AtomT::get_no_action_name())) {
                    if (grounded_deictic_rule_ptr->outcomes.begin()->is_satisfied_by(transition_it->get_post_state())) {
                        ++outcome_coverage;
                    }
                    else {
                        ++empty_coverage;
                    }
                }
            }
        }
    }
    float updated_probability = (float)outcome_coverage / (float)(outcome_coverage + empty_coverage);
    Outcome<AtomT> outcome = *outcomes.begin();
    outcome.probability = updated_probability;
    outcomes.clear();
    outcomes.insert(outcome);
}


/**
 * SymbolicRule
 *
 */
template<typename AtomT>
SymbolicRule<AtomT>::SymbolicRule(const Rule<AtomT>& rule):
    Rule<AtomT>(rule)
{
}

template<typename AtomT>
SymbolicRule<AtomT>::SymbolicRule(const SymbolicRule<AtomT>& rule):
    Rule<AtomT>(rule)
{
}

template<typename AtomT>
SymbolicRule<AtomT>::SymbolicRule(const AtomT &symbol_, const PredicateGroup<AtomT> &preconditions_, const OutcomeGroup<AtomT>& outcomes_, const RewardFunctionGroup& goals_):
    Rule<AtomT>(symbol_, preconditions_, outcomes_, goals_)
{
}

template<typename AtomT>
float SymbolicRule<AtomT>::covers_transition_preconditions(const Transition<AtomT>& transition, const PPDDLObjectManager<typename AtomT::ParamType>& object_manager) const
{
    float covering = 0;
    if ( (transition.get_action().name == this->name) || (this->name == AtomT::get_no_action_name()) ) {
        RuleSet<AtomT> grounded_deictic_rules = get_pddl_grounded_rules(transition.get_prev_state(), object_manager, false);
        covering = grounded_deictic_rules.covers_transition_preconditions(transition, object_manager);
    }
    return covering;
}

template<typename AtomT>
Transitions::Coverage SymbolicRule<AtomT>::covers_transition_effects(const Transition<AtomT>& transition, const PPDDLObjectManager<typename AtomT::ParamType>& object_manager, const bool check_preconditions) const
{
    if ( (transition.get_action().name == this->name) || (this->name == AtomT::get_no_action_name()) ) {
        RuleSet<AtomT> grounded_deictic_rules(get_pddl_grounded_rules(transition.get_prev_state(), object_manager, false));
        // if action (excluding noaction) groundings cover transition preconditions more than once, then rule is bad and coverage error is set as 1
        uint number_groundings = 0;
        BOOST_FOREACH(const RulePtr<AtomT>& grounded_deictic_rule_ptr, grounded_deictic_rules) {
            if (transition.get_action() == *grounded_deictic_rule_ptr) {
                ++number_groundings;
            }
        }
        if (number_groundings > 1) {
            Transitions::Coverage coverage(transition.get_changes().size());
            coverage.add_error(1.0);
            return coverage;
        }
        else { // everything is ok
            return grounded_deictic_rules.covers_transition_effects(transition, object_manager, false);
        }
    }
    return Transitions::Coverage(transition.get_changes().size());
}

template<typename AtomT>
std::vector<int> SymbolicRule<AtomT>::experiences_per_outcome(const Transition<AtomT> transition, const PPDDLObjectManager<typename AtomT::ParamType>& object_manager) const
{
    if ( (transition.get_action().name == this->name) || (this->name == AtomT::get_no_action_name()) ) {
        RuleSet<AtomT> grounded_deictic_rules(get_pddl_grounded_rules(transition.get_prev_state(), object_manager, false));
        // if action (excluding noaction) groundings cover transition preconditions more than once, then rule is bad and coverage error is set as 1
        uint number_groundings = 0;
        BOOST_FOREACH(const RulePtr<AtomT>& grounded_deictic_rule_ptr, grounded_deictic_rules) {
            if (transition.get_action() == *grounded_deictic_rule_ptr) {
                ++number_groundings;
            }
        }
        if (number_groundings == 1) {
            return (*grounded_deictic_rules.begin())->experiences_per_outcome(transition, object_manager);
        }
    }
    return std::vector<int>(this->outcomes.size(), 0);
}

template<typename AtomT>
bool SymbolicRule<AtomT>::satisfiesPreconditions(const PredicateGroup<AtomT>& predicate_group, const PPDDLObjectManager<typename AtomT::ParamType>& pddl_object_manager) const
{
    RuleSet<AtomT> grounded_rules = get_pddl_grounded_rules(predicate_group, pddl_object_manager, false);
    return !grounded_rules.empty();
}

template<typename AtomT>
RuleSet<AtomT> SymbolicRule<AtomT>::get_pddl_grounded_rules(const PredicateGroup<AtomT>& state, const PPDDLObjectManager<typename AtomT::ParamType>& pddl_object_manager, const bool full) const
{
    int DEBUG = 0;
    RuleSet<AtomT> grounded_rules;
    
    std::vector<typename AtomT::ParamType::Name> list_param_names = this->get_param_names();
    std::vector<typename AtomT::ParamType::Type> list_param_types = this->get_param_types();
    std::vector<typename AtomT::ParamType> deictic_params = this->get_deictic_params_v();
    // Add deictic param names
    BOOST_FOREACH(const typename AtomT::ParamType& deictic_param, deictic_params) {
        list_param_names.push_back(deictic_param.name);
    }
    // Add deictic param types
    BOOST_FOREACH(const typename AtomT::ParamType& deictic_param, deictic_params) {
        list_param_types.push_back(deictic_param.type);
    }
    CLOG_IF(DEBUG >=2, INFO, "trivial") << "Action param types are " << this->get_param_types();
    CLOG_IF(DEBUG >=2, INFO, "trivial") << "Param types are " << list_param_types;
    typename PPDDLObjectManager<typename AtomT::ParamType>::CombinationsPPDDLObjectsPtr combinations_of_params(pddl_object_manager.get_combinations_of_objecs_for_given_params(list_param_types));
    
    CLOG_IF(DEBUG >=2, INFO, "trivial") << "Combinations of params are " << *combinations_of_params;

    // this vector is used to save copies and speed up
    std::vector<Predicate<AtomT> > list_preconditions(this->get_preconditions().begin(), this->get_preconditions().end());
    BOOST_FOREACH ( const std::vector<typename AtomT::ParamType::Name>& a_combination_of_params, *combinations_of_params) {
        if (full) {
            #if __cplusplus > 199711L // C++11
            RulePtr<AtomT> new_rule_ptr = RulePtr<AtomT>(new GroundedRule<AtomT>(*this, list_param_names, a_combination_of_params, pddl_object_manager));
            #else
            RulePtr<AtomT> new_rule_ptr = RulePtr<AtomT>(boost::shared_ptr<GroundedRule<AtomT> >(new GroundedRule<AtomT>(*this, list_param_names, a_combination_of_params, pddl_object_manager)));
            #endif
            grounded_rules.push_back(new_rule_ptr);
            CLOG_IF(DEBUG >=3, INFO, "trivial") << "Grounded rules is: " << *new_rule_ptr;
        } else {
            // before creating grounded rule, check that the grounded preconditions are satisfied by the state
            bool preconditions_satisfied = true;
            size_t list_preconditions_idx = 0;
            typename PredicateGroup<AtomT>::const_iterator preconditions_it = this->get_preconditions().begin();
            while (preconditions_satisfied && (preconditions_it != this->get_preconditions().end())) {
//                 Predicate<AtomT> new_precondition = *preconditions_it;
                list_preconditions[list_preconditions_idx].set_param_names(preconditions_it->params);
                list_preconditions[list_preconditions_idx].change_param_names(list_param_names, a_combination_of_params);
                if (!state.satisfies(list_preconditions[list_preconditions_idx])) {
                    preconditions_satisfied = false;
                }
                ++preconditions_it;
                ++list_preconditions_idx;
            }
            
            if (preconditions_satisfied) {
                #if __cplusplus > 199711L // C++11
                RulePtr<AtomT> new_rule_ptr = RulePtr<AtomT>(new GroundedRule<AtomT>(*this, list_param_names, a_combination_of_params, pddl_object_manager));
                #else
                RulePtr<AtomT> new_rule_ptr = RulePtr<AtomT>(boost::shared_ptr<GroundedRule<AtomT> >(new GroundedRule<AtomT>(*this, list_param_names, a_combination_of_params, pddl_object_manager)));
                #endif
                grounded_rules.push_back(new_rule_ptr);
                CLOG_IF(DEBUG >=3, INFO, "trivial") << "Grounded rules is: " << *new_rule_ptr;
            }
        }
    }

    CLOG_IF(DEBUG >=1, INFO, "trivial") << "Number of grounded rules is " << grounded_rules.size();
    
    return grounded_rules;
}



/**
 * GroundedRule
 *
 */
template<typename AtomT>
GroundedRule<AtomT>::GroundedRule(const GroundedRule<AtomT>& grounded_rule):
    Rule<AtomT>(grounded_rule)
{
}

template<typename AtomT>
GroundedRule<AtomT>::GroundedRule(const Rule<AtomT>& rule, 
                                  const std::vector< typename AtomT::ParamType::Name >& symbolic_param_names, 
                                  const std::vector< typename AtomT::ParamType::Name >& grounded_param_names, 
                                  const PPDDLObjectManager<typename AtomT::ParamType>& pddl_object_manager):
    Rule<AtomT>(rule, PredicateGroup<AtomT>()) // Initialize only TypedAtom, don't initialize everything for performance reasons
{
    assert(grounded_param_names.size() == symbolic_param_names.size());
    this->dangerous = rule.dangerous;
    save_ground_preconditions(rule.get_preconditions(), symbolic_param_names, grounded_param_names);
    save_ground_outcomes(rule.outcomes, symbolic_param_names, grounded_param_names);
    save_ground_goals(rule.goals, symbolic_param_names, grounded_param_names, pddl_object_manager);
    
    // update params
    for(size_t i = 0; i < this->params.size(); ++i) { 
        this->params[i].name = grounded_param_names[i]; 
    }
    // update_deictic_params(); not needed, it is faster to do it manually
    assert(this->deictic_params.empty());
    size_t grounded_param_idx = this->params.size();
    BOOST_FOREACH(const typename AtomT::ParamType& symbolic_deictic_param, rule.get_deictic_params()) {
        this->deictic_params.insert(typename AtomT::ParamType(grounded_param_names[grounded_param_idx], symbolic_deictic_param.type));
        ++grounded_param_idx;
    }
}

#if __cplusplus > 199711L
template<typename AtomT>
GroundedRule<AtomT>::GroundedRule(const Rule<AtomT>& rule, 
                                  const std::vector< typename AtomT::ParamType::Name >& grounded_param_names, 
                                  const PPDDLObjectManager<typename AtomT::ParamType>& pddl_object_manager):
    GroundedRule<AtomT>(rule, rule.get_param_and_deictic_names(), grounded_param_names, pddl_object_manager)
{ }
#else
template<typename AtomT>
GroundedRule<AtomT>::GroundedRule(const Rule<AtomT>& rule, 
                                  const std::vector< typename AtomT::ParamType::Name >& grounded_param_names, 
                                  const PPDDLObjectManager<typename AtomT::ParamType>& pddl_object_manager):
    Rule<AtomT>(rule, PredicateGroup<AtomT>()) // Initialize only TypedAtom, don't initialize everything for performance reasons
{
    std::vector< typename AtomT::ParamType::Name > symbolic_param_names = rule.get_param_and_deictic_names();
    assert(grounded_param_names.size() == symbolic_param_names.size());
    this->dangerous = rule.dangerous;
    save_ground_preconditions(rule.get_preconditions(), symbolic_param_names, grounded_param_names);
    save_ground_outcomes(rule.outcomes, symbolic_param_names, grounded_param_names);
    save_ground_goals(rule.goals, symbolic_param_names, grounded_param_names, pddl_object_manager);
    
    // update params
    for(size_t i = 0; i < this->params.size(); ++i) { 
        this->params[i].name = grounded_param_names[i]; 
    }
    // update_deictic_params(); not needed, it is faster to do it manually
    assert(this->deictic_params.empty());
    size_t grounded_param_idx = this->params.size();
    BOOST_FOREACH(const typename AtomT::ParamType& symbolic_deictic_param, rule.get_deictic_params()) {
        this->deictic_params.insert(typename AtomT::ParamType(grounded_param_names[grounded_param_idx], symbolic_deictic_param.type));
        ++grounded_param_idx;
    }
}
#endif

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
template<typename AtomT>
RuleSet<AtomT> GroundedRule<AtomT>::get_pddl_grounded_rules(const PredicateGroup<AtomT>& pred_group, 
                                                            const PPDDLObjectManager<typename AtomT::ParamType>& pddl_object_manager,
                                                            const bool full) const
{
    RuleSet<AtomT> result;
    result.push_back(boost::make_shared<GroundedRule<AtomT> >(*this));
    return result;
}
#pragma GCC diagnostic pop

template<typename AtomT>
void GroundedRule<AtomT>::save_ground_preconditions(const PredicateGroup<AtomT>& symbolic_preconditions, 
                                                    const std::vector<typename AtomT::ParamType::Name>& used_symbolic_params, 
                                                    const std::vector<typename AtomT::ParamType::Name>& used_grounded_params)
{
    BOOST_FOREACH(Predicate<AtomT> new_precondition, symbolic_preconditions) {
        new_precondition.change_param_names(used_symbolic_params, used_grounded_params);
        this->preconditions.insert(new_precondition);
    }
}

template<typename AtomT>
void GroundedRule<AtomT>::save_ground_outcomes(const OutcomeGroup<AtomT>& symbolic_outcomes, 
                                               const std::vector<typename AtomT::ParamType::Name>& used_symbolic_params, 
                                               const std::vector<typename AtomT::ParamType::Name>& used_grounded_params)
{
    BOOST_FOREACH(const Outcome<AtomT>& symbolic_outcome, symbolic_outcomes) {
        Outcome<AtomT> new_outcome(symbolic_outcome.probability, symbolic_outcome.noisy);
        BOOST_FOREACH(Predicate<AtomT> new_outcome_pred, symbolic_outcome) {
            new_outcome_pred.change_param_names(used_symbolic_params, used_grounded_params);
            new_outcome.insert(new_outcome_pred);
        }
        this->outcomes.insert(new_outcome);
    }
}

template<typename AtomT>
float GroundedRule<AtomT>::covers_transition_preconditions(const Transition<AtomT>& transition, const PPDDLObjectManager<typename AtomT::ParamType>& object_manager) const
{
    if ( ((transition.get_action() == *this) || (this->name == AtomT::get_no_action_name())) && satisfiesPreconditions(transition.get_prev_state())) {
        return 1;
    }
    else {
        return 0;
    }
}

template<typename AtomT>
Transitions::Coverage GroundedRule<AtomT>::covers_transition_effects(const Transition<AtomT>& transition, const PPDDLObjectManager<typename AtomT::ParamType>& object_manager, const bool check_preconditions) const
{
    Transitions::Coverage result(transition.get_changes().size());
    if ( ( (this->name == AtomT::get_no_action_name()) || (transition.get_action() == *this) ) && 
         ( (!check_preconditions) || 
           this->preconditions.is_satisfied_by(transition.get_prev_state()) // equivalent to (but faster): satisfiesPreconditions(transition.get_prev_state())
         ) ) 
    {
        float possible_error_probability = 0.0; // only to be considered when rule does not cover any change
        float real_error_probability = 0.0;
        std::vector<float> vector_sum_probabilities(transition.get_changes().size());
        BOOST_FOREACH(const Outcome<AtomT>& outcome, this->outcomes) {
            float error_probability = 0;
            bool outcome_covers_a_change = false;
            int change_idx = 0;
            BOOST_FOREACH(const Predicate<AtomT>& change, transition.get_changes()) {
                if (outcome.has_grounded_predicate(change)) {
                    vector_sum_probabilities[change_idx] += outcome.probability;
                    outcome_covers_a_change = true;
                }
                ++change_idx;
            }
            if (!outcome.is_satisfied_by(transition.get_post_state())) {
                // something is wrong in outcome!!!
                error_probability += outcome.probability;
            }
            if (outcome_covers_a_change) {
                real_error_probability += error_probability;
            }
            else {
                possible_error_probability += error_probability;
            }
        }
        
        bool rule_adds_coverage = false;
        for(size_t change_idx = 0; change_idx < transition.get_changes().size(); ++change_idx) {
            if (vector_sum_probabilities[change_idx] > std::numeric_limits<float>::epsilon()) {
                result.add_change_coverage(change_idx, vector_sum_probabilities[change_idx]);
                rule_adds_coverage = true;
            }
        }
        if (rule_adds_coverage) { // only add error if rule didn't cover effects (otherwise this error is implicit!)
            result.add_error(real_error_probability);
        }
        else {
            result.add_error(possible_error_probability);
        }
        return result;
    }
    else {
        return result;
    }
}

template<typename AtomT>
float RuleSet<AtomT>::covers_transition_preconditions(const Transition<AtomT>& transition, const PPDDLObjectManager<typename AtomT::ParamType>& object_manager) const
{
    float covering = 0.0;
    int n_covered_rules = 0;
    int n_action_covered_rules = 0;
    BOOST_FOREACH(RulePtr<AtomT> rule_ptr, *this) {
        if ( (*rule_ptr == transition.get_action()) || (rule_ptr->name == AtomT::get_no_action_name()) ) {
            covering += rule_ptr->covers_transition_preconditions(transition, object_manager);
            ++n_covered_rules;
            if (rule_ptr->name != AtomT::get_no_action_name()) {
                ++n_action_covered_rules;
            }
        }
    }
    if (n_covered_rules > 1) {
        covering = covering / (float)n_covered_rules; // if more than 2 rules cover it, it may be bigger than 1.0
//         if (n_action_covered_rules > 1) { // Now it happens with LFIT-based learner rules (there is one rule per effect)
//             LOG(WARNING) << "Bad defined rules, more than 1 cover the same transition";
//             LOG(WARNING) << *this;
//             covering = 0.0;
//         }
    }
    return covering;
}

template<typename AtomT>
Transitions::Coverage RuleSet<AtomT>::covers_transition_effects(const Transition<AtomT>& transition, const PPDDLObjectManager<typename AtomT::ParamType>& object_manager, const bool check_preconditions) const
{
    Transitions::Coverage covering(transition.get_changes().size());
    BOOST_FOREACH(RulePtr<AtomT> rule_ptr, *this) {
        if ( (rule_ptr->is_symbolic() && (rule_ptr->symbolic_compare(transition.get_action()))) || 
             (*rule_ptr == transition.get_action()) || 
             (rule_ptr->name == AtomT::get_no_action_name()) )
        {
            covering = covering + rule_ptr->covers_transition_effects(transition, object_manager, check_preconditions);
        }
    }
    return covering;
}

template<typename AtomT>
std::vector<int> GroundedRule<AtomT>::experiences_per_outcome(const Transition<AtomT> transition, const PPDDLObjectManager<typename AtomT::ParamType>& object_manager) const
{
    // supposes that preconditions are satisfied
    std::vector<int> result;

    BOOST_FOREACH(const Outcome<AtomT>& outcome, this->outcomes) {
        if (outcome == transition.get_changes()) {
            result.push_back(1);
        } else {
            result.push_back(0);
        }
    }

    return result;
}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
template<typename AtomT>
bool GroundedRule<AtomT>::satisfiesPreconditions(const PredicateGroup<AtomT>& predicates, const PPDDLObjectManager<typename AtomT::ParamType>& pddl_object_manager) const
{
    if (this->preconditions.is_satisfied_by(predicates))
        return true;
    else
        return false;
}
#pragma GCC diagnostic pop

template<typename AtomT>
bool GroundedRule<AtomT>::satisfiesPreconditions(const PredicateGroup<AtomT>& predicates) const
{
    return satisfiesPreconditions(predicates, PPDDLObjectManager<typename AtomT::ParamType>());
}



/**
 * RuleSet<AtomT>
 *
 */
template<typename AtomT>
RuleSet<AtomT>::RuleSet()
{ }

#if __cplusplus > 199711L
template<typename AtomT>
RuleSet<AtomT>::RuleSet(RuleSet<AtomT>&& rules_):
    std::vector< boost::shared_ptr<Rule<AtomT> > >(rules_) 
    {};
#endif

template<typename AtomT>
RuleSet<AtomT>::RuleSet(const RuleSet<AtomT>& rules_)
{
    add_rules(rules_);
}

template<typename AtomT>
void RuleSet<AtomT>::add_rules(const RuleSet<AtomT>& new_rules)
{
    #if __cplusplus > 199711L // C++11
    BOOST_FOREACH(const RuleConstPtr<AtomT>& rule_ptr, new_rules) {
    #else
    BOOST_FOREACH(const RulePtr<AtomT>& rule_ptr, new_rules) {
    #endif
        this->push_back(rule_ptr->make_shared());
    }
}


template<typename AtomT>
void RuleSet<AtomT>::write_rules_to_file(std::string file_path) const
{
    int DEBUG = 0;
    std::ofstream rules_file(file_path.c_str());
    if (DEBUG > 0)
        std::cout << "RuleSet<AtomT>::write_rules_to_file - " << "Writting rules to file: '" << file_path << "'" << std::endl;
    if (rules_file.is_open()) {
        rules_file << *this;
    } else {
        std::cerr << "Couldn't write rules to " << file_path << std::endl;
    }
    rules_file.close();
}

template<typename AtomT>
void RuleSet<AtomT>::sanitize_probabities()
{
    BOOST_FOREACH(RulePtr<AtomT>& rule_ptr, *this) {
        rule_ptr->sanitize_probabities();
    }
}

template<typename AtomT>
double RuleSet<AtomT>::can_obtain_predicate(const Predicate<AtomT> predicate) const
{
    double result = 0.0;
    BOOST_FOREACH(const RulePtr<AtomT>& rule_ptr, *this) {
        result += rule_ptr->probability_obtain_predicate(predicate);
    }
    return result;
}

template<typename AtomT>
bool RuleSet<AtomT>::remove_rule(const RulePtr<AtomT> rule_to_remove)
{
    bool result = false;
    for (typename RuleSet::iterator rule_it = this->begin() ; rule_it != this->end();) {
        if ((*rule_to_remove) == (**rule_it)) {
            result = true;
            rule_it = this->erase(rule_it);
        } else {
            ++rule_it;
        }
    }
    return result;
}

template<typename AtomT>
RuleSet<AtomT> RuleSet<AtomT>::get_action_rules(const AtomT& action) const
{
    RuleSet<AtomT> action_rules;

    BOOST_FOREACH(const RulePtr<AtomT>& rule_ptr, *this) {
        if (action.is_symbolic()) {
            if (rule_ptr->symbolic_compare(action)) {
                action_rules.push_back(rule_ptr);
            }
        }
        else if ((*rule_ptr) == action) {
            action_rules.push_back(rule_ptr);
        }
    }
    return action_rules;
}

template<typename AtomT>
RuleSet<AtomT> RuleSet<AtomT>::get_grounded_action_rules(const AtomT& grounded_action, const State& state) const
{
    RuleSet<AtomT> grounded_action_rules;
    BOOST_FOREACH(const RulePtr<AtomT>& rule_ptr, *this) {
        if ((!rule_ptr->is_symbolic()) && ((*rule_ptr) == grounded_action)) {  // if grounded and actions are equal
            #if __cplusplus > 199711L // C++11
            GroundedRulePtr<AtomT> grounded_rule_ptr = boost::make_shared<GroundedRule<AtomT> >(*static_cast<const GroundedRule<AtomT> *>(&(*rule_ptr)));
            #else
            RulePtr<AtomT> grounded_rule_ptr = RulePtr<AtomT>(boost::make_shared<GroundedRule<AtomT> >(*static_cast<const GroundedRule<AtomT> *>(&(*rule_ptr))));
            #endif
            grounded_action_rules.push_back(grounded_rule_ptr);
        } else if ((rule_ptr->is_symbolic()) && (rule_ptr->name == grounded_action.name)) { // if symbolic and actions names are equal (not arguments)
            RuleSet<AtomT> rule_it_grounded_rules = rule_ptr->get_pddl_grounded_rules(state, false);
            BOOST_FOREACH(RulePtr<AtomT> grounded_rule_ptr, rule_it_grounded_rules) {
                if (*grounded_rule_ptr == grounded_action) {
                    grounded_action_rules.push_back(grounded_rule_ptr);
                }
            }
        }
    }
    return grounded_action_rules;
}

template<typename AtomT>
RuleSet<AtomT> RuleSet<AtomT>::remove_action_rules(AtomT action)
{
    RuleSet<AtomT> removed_rules;

    for (typename RuleSet<AtomT>::iterator rule = this->begin() ; rule != this->end();) {
        if ((*rule)->symbolic_compare(action)) {
            removed_rules.push_back(*rule);
            rule = this->erase(rule);
        } else {
            ++rule;
        }
    }
    return removed_rules;
}

template<typename AtomT>
void RuleSet<AtomT>::remove_nop_rules()
{
    for (typename RuleSet<AtomT>::iterator rule = this->begin() ; rule != this->end();) {
        if ( (*rule)->has_useful_outcome() || (!(*rule)->goals.empty()) ) {
            ++rule;
        } else {
            rule = this->erase(rule);
        }
    }
}

template<typename AtomT>
void RuleSet<AtomT>::remove_special_rules()
{
    for (typename RuleSet<AtomT>::iterator rule = this->begin() ; rule != this->end();) {
        if ( (*rule)->is_special()) {
            rule = this->erase(rule);
        } else {
            ++rule;
        }
    }
}

template<typename AtomT>
void Rule<AtomT>::analyze_differences(const Rule<AtomT>& compared_rule, int& preconditions_diff_n, int& outcomes_diff_n,  double& probabilities_diff) const
{
    // preconditions
    PredicateGroup<AtomT> preconditions_diff = preconditions.get_differences(compared_rule.preconditions);
    preconditions_diff_n = preconditions_diff.size();

    // outcomes
    outcomes_diff_n = outcomes.size() - compared_rule.outcomes.size();

    // probabilities
    probabilities_diff = outcomes.begin()->probability - compared_rule.outcomes.begin()->probability;
}

template<typename AtomT>
void RuleSet<AtomT>::analyze_differences(RuleSet<AtomT> compared_ruleset, int& rule_number_diff, int& preconditions_diff, int& outcomes_num_diff, double& first_outcome_prob_diff)
{
    preconditions_diff = 0;
    outcomes_num_diff = 0;
    first_outcome_prob_diff = 0;
    rule_number_diff = - this->size();
    BOOST_FOREACH(const RulePtr<AtomT>& rule_ptr, *this) {
        preconditions_diff -= rule_ptr->get_preconditions().size();
        outcomes_num_diff -= rule_ptr->outcomes.size();
        first_outcome_prob_diff -= rule_ptr->outcomes.begin()->probability;
    }

    rule_number_diff += compared_ruleset.size();
    BOOST_FOREACH(const RulePtr<AtomT>& compared_rule_ptr, compared_ruleset) {
        preconditions_diff += compared_rule_ptr->get_preconditions().size();
        outcomes_num_diff += compared_rule_ptr->outcomes.size();
        first_outcome_prob_diff += compared_rule_ptr->outcomes.begin()->probability;
    }
}

template<typename AtomT>
void RuleSet<AtomT>::analyze_differences(RuleSet<AtomT> compared_ruleset, RuleSet<AtomT>& added_rules, RuleSet<AtomT>& removed_rules,
                                  RuleSet<AtomT>& original_changed_rules, RuleSet<AtomT>& compared_changed_rules,
                                  std::vector<int>& preconditions_diff, std::vector<int>& outcomes_diff,  std::vector<double>& probabilities_diff
                                 ) const
{
    RuleSet<AtomT> original_rules(*this);
    std::sort(original_rules.rbegin(), original_rules.rend());  // reverse iterators! -> sorting bigger -> smaller
    std::sort(compared_ruleset.rbegin(), compared_ruleset.rend());  // reverse iterators! -> sorting bigger -> smaller

    typename RuleSet<AtomT>::iterator rule_it = original_rules.begin();
    typename RuleSet<AtomT>::iterator comp_rule_it = compared_ruleset.begin();
    while ((rule_it != original_rules.end()) && (comp_rule_it != compared_ruleset.end())) {
        if (**rule_it == **comp_rule_it) {
            preconditions_diff.push_back(0);
            outcomes_diff.push_back(0);
            probabilities_diff.push_back(0.0);
            original_changed_rules.push_back(*rule_it);
            compared_changed_rules.push_back(*comp_rule_it);
            ++rule_it;
            ++comp_rule_it;
        } else if (**rule_it > **comp_rule_it) {
            if (((rule_it + 1) != original_rules.end()) && (**(rule_it + 1) >= **comp_rule_it)) {   // exists rule_it+1 && rule_it+1 >= comp_rule_it
                removed_rules.push_back(*rule_it);
                ++rule_it;
            } else { // compare both
                int rule_preconditions_diff, rule_outcomes_diff;
                double rule_probabilities_diff;
                (*rule_it)->analyze_differences(**comp_rule_it, rule_preconditions_diff, rule_outcomes_diff, rule_probabilities_diff);
                preconditions_diff.push_back(rule_preconditions_diff);
                outcomes_diff.push_back(rule_outcomes_diff);
                probabilities_diff.push_back(rule_probabilities_diff);
                original_changed_rules.push_back(*rule_it);
                compared_changed_rules.push_back(*comp_rule_it);
                ++rule_it;
                ++comp_rule_it;
            }
        } else { // if (**comp_rule_it > **rule_it) {
            if (((comp_rule_it + 1) != compared_ruleset.end()) && (**(comp_rule_it + 1) >= **rule_it)) {     // exists comp_rule_it+1 && comp_rule_it+1 >= rule_it
                added_rules.push_back(*comp_rule_it);
                ++comp_rule_it;
            } else { // compare both
                int rule_preconditions_diff, rule_outcomes_diff;
                double rule_probabilities_diff;
                (*rule_it)->analyze_differences(**comp_rule_it, rule_preconditions_diff, rule_outcomes_diff, rule_probabilities_diff);
                preconditions_diff.push_back(rule_preconditions_diff);
                outcomes_diff.push_back(rule_outcomes_diff);
                probabilities_diff.push_back(rule_probabilities_diff);
                original_changed_rules.push_back(*rule_it);
                compared_changed_rules.push_back(*comp_rule_it);
                ++rule_it;
                ++comp_rule_it;
            }
        }
    }

    // add remaining rules
    while (rule_it != original_rules.end()) {
        removed_rules.push_back(*rule_it);
        ++rule_it;
    }
    while (comp_rule_it != compared_ruleset.end()) {
        added_rules.push_back(*comp_rule_it);
        ++comp_rule_it;
    }
}

template<typename AtomT>
void RuleSet<AtomT>::set_identifiers(RuleSet<AtomT>& old_rules)
{
    // Reasons to reverse sort:
    //  - Important rules are those with outcomes
    //  - If rules with outcomes, the identifier will asign better identifiers to rules with outcomes
    //  - Actually, this method could be improved a lot using distances instead of < relations, but not worth the time
    //  as identfiers are used just for gource logging.
    std::sort(this->rbegin(), this->rend());  // reverse iterators! -> sorting bigger -> smaller
    std::sort(old_rules.rbegin(), old_rules.rend());  // reverse iterators! -> sorting bigger -> smaller

    typename RuleSet<AtomT>::iterator rule_it = this->begin();
    typename RuleSet<AtomT>::const_iterator old_rule_it = old_rules.begin();
    while ((rule_it != this->end()) && (old_rule_it != old_rules.end())) {
        if (**rule_it == **old_rule_it) {
            (*rule_it)->identifier = (*old_rule_it)->identifier;
            ++rule_it;
            ++old_rule_it;
        } else if (**rule_it > **old_rule_it) {
            if (((rule_it + 1) != this->end()) && (**(rule_it + 1) >= **old_rule_it)) {     // exists rule_it+1 && rule_it+1 >= old_rule_it
                ++rule_it;
            } else { // just similar
                (*rule_it)->identifier = (*old_rule_it)->identifier;
                ++rule_it;
                ++old_rule_it;
            }
        } else { // if (**old_rule_it > **rule_it) {
            if (((old_rule_it + 1) != old_rules.end()) && (**(old_rule_it + 1) >= **rule_it)) {     // exists old_rule_it+1 && old_rule_it+1 >= rule_it
                ++old_rule_it;
            } else { // just similar
                (*rule_it)->identifier = (*old_rule_it)->identifier;
                ++rule_it;
                ++old_rule_it;
            }
        }
    }
}

template<typename AtomT>
RuleSet<AtomT> RuleSet<AtomT>::find_rules_with_a_precondition(const Predicate<AtomT>& precondition) const
{
    RuleSet<AtomT> result;
    BOOST_FOREACH(const RulePtr<AtomT>& rule_ptr, *this) {
        if (rule_ptr->get_preconditions().has_predicate(precondition)) {
            result.push_back(rule_ptr->make_shared());
        }
    }
    return result;
}

template<typename AtomT>
RuleSet<AtomT> RuleSet<AtomT>::find_rules_with_a_precondition(const PredicateGroup<AtomT>& possible_preconditions) const
{
    RuleSet<AtomT> result;
    BOOST_FOREACH(const Predicate<AtomT>& a_possible_precondition, possible_preconditions) {
        result.add_rules(find_rules_with_a_precondition(a_possible_precondition));
    }
    return result;
}

template<typename AtomT>
std::vector<AtomT> RuleSet<AtomT>::get_actions() const
{
    std::vector<TypedAtom> result;
    std::set<TypedAtom> action_atoms;
    BOOST_FOREACH(RulePtr<AtomT> rule_ptr, *this) {
        action_atoms.insert(*rule_ptr);
    }
    // std::set to std::vector
    result.assign(action_atoms.begin(), action_atoms.end());
    return result;
}

template<typename AtomT>
std::vector<std::string> RuleSet<AtomT>::get_action_names() const
{
    std::vector<std::string> result;
    std::set<std::string> action_names;
    BOOST_FOREACH(RulePtr<AtomT> rule_ptr, *this) {
        action_names.insert(rule_ptr->name);
    }
    // std::set to std::vector
    result.assign(action_names.begin(), action_names.end());
    return result;
}

template<typename AtomT>
double RuleSet<AtomT>::score(const TransitionGroup<AtomT> transitions, const PPDDLObjectManager<typename AtomT::ParamType>& object_manager) const
{
    double cutting_threshold = -1. * std::numeric_limits<double>::max();
    std::vector<float> experience_weights(transitions.size_historial());
    std::fill(experience_weights.begin(), experience_weights.end(), 1);

    // learning parameters
    double alpha = 0.5;
    double p_min = 1e-9;
    double p_min_noisyDefaultRule = 1e-11;

    // (1) Penalty
    double penalty = 0.0;
    penalty += get_number_literals();
    penalty *= alpha;

    // (2) Log-Likelihood
    double loglikelihood = 0.0, exLik;

    uint i = 0;
    for (typename TransitionHistorial<AtomT>::const_iterator trans_it_it = transitions.begin_historial(); 
         trans_it_it != transitions.end_historial(); 
         ++trans_it_it) {

        RuleSet<AtomT> covering_rules = get_covering_rules(**trans_it_it, object_manager);

        // only one non-default rule covers (good case)
        if (covering_rules.size() == 1) {
            RulePtr<AtomT> rule_ptr = covering_rules.front();

            std::vector<int> exp_per_out = rule_ptr->experiences_per_outcome(**trans_it_it, object_manager);
            exLik = 0.0;
            typename OutcomeGroup<AtomT>::iterator out_it = rule_ptr->outcomes.begin();
            for (size_t j = 0; j < exp_per_out.size(); j++) {
                if ((exp_per_out[j] == 1) && (!out_it->noisy)) {
                    exLik += out_it->probability;
                }
                // Noise-outcome:  always covers
                if (out_it->noisy) {
                    exLik += p_min * out_it->probability;
                }
                ++out_it;
            }
            if (exLik <= 0.) {
                std::cerr << "!";
                exLik += p_min_noisyDefaultRule * 0.01;
            }
        }
        // only default covers or more than one non-default covers
        // --> apply default rule
        else {
            /*
            ACTION:
            default()
            CONTEXT:
            --
            OUTCOMES:
            0.5 <no-change>
            0.5 <noise>
             */
            if ((*trans_it_it)->nothing_changed())
                exLik = 0.5 * (100.0 * p_min_noisyDefaultRule);   // Avoid modeling persistence by means of noise default rule.
            else
                exLik = 0.5 * p_min_noisyDefaultRule;
        }

        loglikelihood += experience_weights[i] * log(exLik); // weight the experiences differently
        if (loglikelihood - penalty  <  cutting_threshold) {
            return (-1. * std::numeric_limits<double>::max());
        }
        i++;
    }
    
    return loglikelihood - penalty;
}

template<typename AtomT>
int RuleSet<AtomT>::get_number_literals() const
{
    int result = 0;
    BOOST_FOREACH(const RulePtr<AtomT>& rule_ptr, *this) {
        result += rule_ptr->get_number_literals();
    }
    return result;
}

template<typename AtomT>
RuleSet<AtomT> RuleSet<AtomT>::get_covering_rules(const Transition<AtomT>& transition, const PPDDLObjectManager<typename AtomT::ParamType>& object_manager) const
{
    RuleSet<AtomT> result;
    BOOST_FOREACH(const RulePtr<AtomT>& rule_ptr, *this) {
        if (rule_ptr->covers_transition_preconditions(transition, object_manager) > 0.0) {
            result.push_back(rule_ptr->make_shared());
        }
    }

    return result;
}

template<typename AtomT>
RuleSet<AtomT> RuleSet<AtomT>::get_covering_rules(const PredicateGroup<AtomT>& predicates, const PPDDLObjectManager<typename AtomT::ParamType>& pddl_object_manager) const
{
    RuleSet<AtomT> result;
    BOOST_FOREACH(const RulePtr<AtomT>& rule_ptr, *this) {
        if (rule_ptr->satisfiesPreconditions(predicates, pddl_object_manager)) {
            result.push_back(rule_ptr);
        }
    }

    return result;
}

template<typename AtomT>
RuleSet<AtomT> RuleSet<AtomT>::get_pddl_grounded_rules(const State& state, const bool full) const
{
    RuleSet<AtomT> grounded_rules;

    BOOST_FOREACH(const RulePtr<AtomT>& rule_ptr, *this) {
        RuleSet new_grounded_rules = rule_ptr->get_pddl_grounded_rules(state, full);
        grounded_rules.add_rules(new_grounded_rules);
    }

    return grounded_rules;
}

template<typename AtomT>
void RuleSet<AtomT>::set_dangerous_rules(const PredicateGroup<AtomT>& dead_end_excuses)
{
    BOOST_FOREACH(const RulePtr<AtomT>& rule_ptr, *this) {
        BOOST_FOREACH(const Predicate<AtomT>& dead_end_excuse, dead_end_excuses) {
            BOOST_FOREACH(const Outcome<AtomT>& outcome, rule_ptr->outcomes) {
                if (outcome.has_predicate(dead_end_excuse.get_negated()) && (outcome.probability > 0.0)) {
                    // outcome.is_dangerous = true
                    Outcome<AtomT> dangerous_outcome(outcome);
                    rule_ptr->outcomes.erase(outcome);
                    dangerous_outcome.is_dangerous = true;
                    rule_ptr->outcomes.insert(dangerous_outcome);
                    
                    rule_ptr->dangerous = true;
                }
            }
        }
    }
}

template<typename AtomT>
bool RuleSet<AtomT>::is_dangerous(const AtomT action, const State& state) const
{
    RuleSet<AtomT> action_rules = get_grounded_action_rules(action, state);
    RuleSet<AtomT> covering_rules = action_rules.get_covering_rules(state, state.ppddl_object_manager);
    BOOST_FOREACH(const RulePtr<AtomT>& covering_rule_ptr, covering_rules) {
        if (covering_rule_ptr->dangerous) {
            return true;
        }
    }
    return false;
}

template<typename AtomT>
void RuleSet<AtomT>::add_negated_outcomes_as_preconditions()
{
    BOOST_FOREACH(RulePtr<AtomT>& rule_ptr, *this) {
        rule_ptr->add_negated_outcomes_as_preconditions();
    }
}

template<typename AtomT>
void RuleSet<AtomT>::add_predicate_to_preconditions(const Predicate<AtomT>& new_predicate)
{
    BOOST_FOREACH(RulePtr<AtomT>& rule_ptr, *this) {
        rule_ptr->add_predicate_to_preconditions(new_predicate);
    }
}

template<typename AtomT>
void RuleSet<AtomT>::add_predicate_to_outcomes(const Predicate<AtomT>& new_predicate)
{
    BOOST_FOREACH(RulePtr<AtomT>& rule_ptr, *this) {
            rule_ptr->add_predicate_to_outcomes(new_predicate);
    }
}

template<typename AtomT>
void RuleSet<AtomT>::add_goal(const RewardFunction& goal)
{
    BOOST_FOREACH(RulePtr<AtomT>& rule_ptr, *this) {
        rule_ptr->add_goal(goal);
    }
}

template<typename AtomT>
void RuleSet<AtomT>::transform_symbolic_pasula_to_symbolic_PDDL()
{
    BOOST_FOREACH(RulePtr<AtomT>& rule_ptr, *this) {
        rule_ptr->transform_nid_params_to_pddl_params();
    }
}


/**
 * PPDDL
 */
template<typename AtomT>
void RuleSet<AtomT>::PPDDL_grounding()
{
    BOOST_FOREACH(RulePtr<AtomT>& rule_ptr, *this) {
        rule_ptr->PPDDL_grounding();
    }
}

template<typename AtomT>
void Rule<AtomT>::PPDDL_grounding()
{
    preconditions.PPDDL_grounding();
    outcomes.PPDDL_grounding();
    goals.PPDDL_grounding();
    (static_cast<TypedAtom*>(this))->PPDDL_grounding();
}

template<typename AtomT>
std::map<std::string, RuleSet<AtomT> > RuleSet<AtomT>::get_rules_per_action() const
{
    using namespace boost::lambda;
    std::map<std::string, RuleSet<AtomT> > result;
    if (this->empty()) {
        return result;
    }
    RuleSet<AtomT> ordered_rules(*this);
    std::sort(ordered_rules.begin(), ordered_rules.end(), 
        boost::lambda::bind(&Rule<AtomT>::name, *boost::lambda::_1 ) < boost::lambda::bind(&Rule<AtomT>::name, *boost::lambda::_2 )
    );

    std::string current_action = ordered_rules[0]->name;
    RuleSet<AtomT> current_ruleset;
    BOOST_FOREACH(const RulePtr<AtomT>& rule_ptr, ordered_rules) {
        if (rule_ptr->name == current_action) {
            current_ruleset.push_back(rule_ptr->make_shared());
        } else {
            result[current_action] = current_ruleset;
            current_action = rule_ptr->name;
            current_ruleset = RuleSet<AtomT>();
            current_ruleset.push_back(rule_ptr->make_shared());
        }
    }
    result[current_action] = current_ruleset;
    return result;
}

#endif