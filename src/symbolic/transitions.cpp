#include "transitions.h"

namespace Transitions {
Coverage& Coverage::operator+=(const Coverage& other)
{
    /* addition of other to *this takes place here */
    assert(size() == other.size());
    for (size_t i = 0; i < other.size(); ++i) {
        coverage_vector[i].insert(coverage_vector[i].end(), other.coverage_vector[i].begin(), other.coverage_vector[i].end());
    }
#ifdef DEBUG_COVERAGE_ERRORS
    coverage_errors_vector.insert(coverage_errors_vector.end(), other.coverage_errors_vector.begin(), other.coverage_errors_vector.end());
#else
    add_error(other.coverage_error);
#endif
    return *this;
}
    
Coverage operator+(const Coverage &c1, const Coverage &c2)
{
    assert(c1.size() == c2.size());
    if (c2.is_empty) {
        return c1;
    }
    else if (c1.is_empty) {
        return c2;
    }
    Coverage result(c1);
    for (size_t i = 0; i < c2.size(); ++i) {
        result.coverage_vector[i].insert(result.coverage_vector[i].end(), c2.coverage_vector[i].begin(), c2.coverage_vector[i].end());
    }
#ifdef DEBUG_COVERAGE_ERRORS
    result.coverage_errors_vector.insert(result.coverage_errors_vector.end(), c2.coverage_errors_vector.begin(), c2.coverage_errors_vector.end());
#else
    result.add_error(c2.coverage_error);
#endif
    return result;
}

std::ostream& operator<<(std::ostream &out, const Coverage &c)
{
#ifdef DEBUG_COVERAGE_ERRORS
    out << "Cov: " << c.coverage_vector << ", Err: " << c.coverage_errors_vector;
#else
    out << "Cov: " << c.coverage_vector << ", Err: " << c.coverage_error;
#endif
    return out;
}

float Coverage::get_change_coverage(const index_type change_idx, const float optimistic_value) const
{
    if (coverage_vector[change_idx].size() > 1) {
        // If uncommented
        // if two rule groundings cover a transition, there is a conflict, coverage is 0 as a result
        // return 0.0;
        // Otherwise, penalize coverage for each rule that does not cover perfectly
        float res = 1.0;
        BOOST_FOREACH(const float coverage_value, coverage_vector[change_idx]) {
            res*=coverage_value;
        }
        return res;
    }
    else if (coverage_vector[change_idx].empty()) {
        return optimistic_value; // will be 0 in normal case, (0,1] in a more optimistic case
    }
    else {
        assert(coverage_vector[change_idx].size() == 1);
        return coverage_vector[change_idx][0];
    }
}

float Coverage::get_coverage(const bool complete, const float optimistic_value) const
{
    if (coverage_vector.empty()) {
        return 1.0 - get_error_probability();
    }
    
    float result;
    if (complete) {
        result = 1.0;
    }
    else {
        result = 0.0;
    }
    for (size_t change_idx = 0; change_idx < coverage_vector.size(); ++change_idx) {
        if (complete) {
            result *= get_change_coverage(change_idx, optimistic_value);
        }
        else {
            result += get_change_coverage(change_idx, optimistic_value);
        }
    }
    if (complete) {
        result *= (1 - get_error_probability());
    }
    else {
        result /= coverage_vector.size();
    }
    return result;
}

float Coverage::get_error_probability() const {
#ifdef DEBUG_COVERAGE_ERRORS
    float result = 0.0;
    BOOST_FOREACH(const float coverage_error ,coverage_errors_vector) { 
        result += (1.0 - result) * coverage_error;
    }
    return result;
#else
    return coverage_error;
#endif
}


float Coverage::get_likelihood(const bool complete, const float optimistic_value) const
{
    return get_coverage(complete, optimistic_value); // if complete == true, already removing error probability from result
}

}

template<>
std::ostream& operator<<(std::ostream &out, const Transition<TypedAtom> &t)
{
    t.prev_state.write_PPDDL(out, t.get_action().params);
    out << std::endl;
    t.action.write_PPDDL(out);
    out << std::endl;
    t.next_state.write_PPDDL(out, t.get_action().params);
    out << std::endl;
    out << t.reward << std::endl;
    return out;
}

template<>
std::ostream& operator<<(std::ostream &out, const TransitionGroup<TypedAtom> &t)
{
    BOOST_FOREACH(const std::set<Transition<TypedAtom> >::iterator& transition_it, t.transition_historial) {
        out << *transition_it;
    }
    return out;
}

TransitionGroup<TypedAtom> read_transitions_from_file(const std::string& file_path, const PPDDLObjectManager<PPDDL::FullObject>& object_manager, const PredicateGroup<TypedAtom>& constant_preds)
{
    TransitionGroup<TypedAtom> transitions(object_manager);
    std::ifstream transitions_file(file_path.c_str());
    if (transitions_file.is_open()) {
        std::string line_prev, line_action, line_next, line_reward;

        while (transitions_file.good()) {
            getline(transitions_file, line_prev);
            getline(transitions_file, line_action);
            getline(transitions_file, line_next);
            getline(transitions_file, line_reward);

            // avoid adding twice last transition
            if (transitions_file.good()) {
                try {
                    float transition_reward = boost::lexical_cast<float>(line_reward);
                    PredicateGroup<NonTypedAtom> prev_s(line_prev);
                    PredicateGroup<TypedAtom> prev_s_typed = Predicates::transform_to_typed(prev_s, object_manager);
                    prev_s_typed.set_constants(constant_preds);
                    
                    NonTypedAtom action(line_action);
                    PredicateGroup<NonTypedAtom> next_s(line_next);
                    PredicateGroup<TypedAtom> next_s_typed = Predicates::transform_to_typed(next_s, object_manager);
                    next_s_typed.set_constants(constant_preds);
                    
                    Transition<TypedAtom> transition(prev_s_typed,
                                                     object_manager.get_typed_atom(action),
                                                     next_s_typed,
                                                     transition_reward,
                                                     true
                                                     );
                    transitions.add_transition(transition);
                } catch (boost::bad_lexical_cast &) {
                    LOG(WARNING) << "Error reading transition " << transitions.size_historial() + 1;
                }
            }
        }
    } else {
        LOG(ERROR) << "Couldn't read transitions from " << file_path;
    }
    transitions_file.close();
    return transitions;
}


/**
 * TransitionGroup
 */

template<>
void TransitionGroup<TypedAtom>::write_transitions_to_file(const std::string& file_path) const
{
    std::ofstream transitions_file(file_path.c_str());
    if (transitions_file.is_open()) {
        transitions_file << *this;
    } else
        std::cerr << "Couldn't write transitions to " << file_path << std::endl;
    transitions_file.close();
}

template<>
void TransitionGroup<TypedAtom>::write_pasula_transitions_to_file(const std::string& file_path) const
{
    std::ofstream transitions_file(file_path.c_str());
    if (transitions_file.is_open()) {
        for (TransitionHistorial<TypedAtom>::const_iterator transition_it = begin_historial(); transition_it != end_historial(); ++transition_it) {
            transitions_file << Predicates::transform_to_nontyped((*transition_it)->get_prev_state()) << std::endl;
            transitions_file << NonTypedAtom((*transition_it)->get_action().name, (*transition_it)->get_action().get_param_names()) << std::endl;
            transitions_file << Predicates::transform_to_nontyped((*transition_it)->get_post_state()) << std::endl;
            transitions_file << (*transition_it)->get_reward() << std::endl;
        }
    } else
        std::cerr << "Couldn't write transitions to " << file_path << std::endl;
    transitions_file.close();
}

template<>
void TransitionGroup<TypedAtom>::write_lfit_transitions_to_file(const std::string& file_path, const std::string& domain_types) const
{
    std::ofstream transitions_file(file_path.c_str());
    if (transitions_file.is_open()) {
        transitions_file << domain_types << std::endl;
        
        for (TransitionSet<TypedAtom>::const_iterator transition_it = begin_set() ; transition_it != end_set(); ++transition_it) {
            transitions_file << std::endl;
            transition_it->get_prev_state().write_LFIT(transitions_file);
            transitions_file << " action=" << transition_it->get_action().name;
            transitions_file << " : ";
            transition_it->get_post_state().write_LFIT(transitions_file);
            transitions_file << " action=" << transition_it->get_action().name;
            if (transition_it->get_number_repetitions() > 1) {
                transitions_file << std::endl;
                transitions_file << "# " << transition_it->get_number_repetitions();
            }
        }
    } else
        std::cerr << "Couldn't write transitions to " << file_path << std::endl;
    transitions_file.close();
}

template<>
void Transition<TypedAtom>::make_lfit_relational_transition(const std::vector<PPDDL::FullObject>& vars_combination)
{
    prev_state.remove_predicates_with_params_not_in(vars_combination);
    next_state.remove_predicates_with_params_not_in(vars_combination);
    for (size_t i = 0; i < vars_combination.size(); ++i) {
        prev_state.change_param_name(vars_combination[i].name, DEFAULT_PARAM_NAMES[i]);
        next_state.change_param_name(vars_combination[i].name, DEFAULT_PARAM_NAMES[i]);
    }
    action.make_symbolic();
    if (action == MAKE_TYPED_ATOM_NOOP_ACTION) {
        action = MAKE_TYPED_ATOM_NOACTION;
    }
}

template<>
TransitionGroup<TypedAtom> Transition<TypedAtom>::get_symbolic_lfit_transitions(const PPDDLObjectManager<TypedAtom::ParamType>& object_manager, 
                                                                                const uint max_action_variables, 
                                                                                const bool nonaction_vars_combinations) const
{
    typedef std::vector<typename TypedAtom::ParamType> ParamCombination;
    typedef std::vector< ParamCombination > VectorParamCombinations;
    
    TransitionGroup<TypedAtom> result(object_manager);
    
    ParamCombination action_params = action.params;
    
    // Get non-action vars
    ParamCombination other_vars = prev_state.get_params();
//     std::random_shuffle ( other_vars.begin(), other_vars.end() );
    
    // Add deictic vars combinations
    int n_deictic_vars = std::max(0, (int)max_action_variables - (int)action_params.size());
    // possible deictic vars
    BOOST_FOREACH(const typename TypedAtom::ParamType& action_param, action_params) {
        typename ParamCombination::iterator it = std::find (other_vars.begin(), other_vars.end(), action_param);
        if (it != other_vars.end()) {
            other_vars.erase(it);
        }
    }
    VectorParamCombinations deictic_vars_combinations = utils::get_all_permutations<typename TypedAtom::ParamType>(other_vars, n_deictic_vars);
    BOOST_FOREACH(const ParamCombination& deictic_vars_combination, deictic_vars_combinations) {
        ParamCombination vars_combination(action_params);
        vars_combination.insert(vars_combination.end(), deictic_vars_combination.begin(), deictic_vars_combination.end());
        Transition<TypedAtom> new_transition(*this);
        new_transition.make_lfit_relational_transition(vars_combination);
        // only add predicates if prev state is not empty (i.e. if we can obtain any rule)
        if (!new_transition.prev_state.empty()) {
#ifdef HACK_FOR_BUGGY_LFIT
            PredicateGroup<TypedAtom> non_negative_preds;
            non_negative_preds.add_predicates(new_transition.get_prev_state());
            non_negative_preds.remove_negated_predicates();
            if (! ((new_transition.get_prev_state() == new_transition.get_post_state()) && (non_negative_preds.size() <=2)) ) {
#endif
                result.add_transition(new_transition);
#ifdef HACK_FOR_BUGGY_LFIT
            }
#endif
        }
    }
    
    // Add one random transition with params not in action.params
    // Otherwise if the domain has few actions (i.e. only 2 actions), actions effects may be learned as "noaction"
    if ((!action_params.empty()) && nonaction_vars_combinations) {
        VectorParamCombinations other_vars_combinations = utils::get_all_permutations<typename TypedAtom::ParamType>(other_vars, max_action_variables);
        BOOST_FOREACH(const ParamCombination& random_vars_combination, other_vars_combinations) {
//         for (size_t i = 0; i < max_nonaction_vars_combinations; ++i) {
//             ParamCombination random_vars_combination = *utils::get_random_element< typename VectorParamCombinations::iterator >(other_vars_combinations.begin(), other_vars_combinations.end());
            Transition new_transition(*this);
            new_transition.action = MAKE_TYPED_ATOM_NOACTION;
            new_transition.make_lfit_relational_transition(random_vars_combination);
            // only add predicates if prev state is not empty (i.e. if we can obtain any rule)
            if (!new_transition.prev_state.empty()) {
                result.add_transition(new_transition);
            }
        }
    }
    
    return result;
}

template<>
TransitionGroup<TypedAtom> TransitionGroup<TypedAtom>::get_normalized_transitions() const
{
    TransitionGroup<TypedAtom> result(ppddl_object_manager);
    
    // Get normalized transitions
    typedef std::pair<PredicateGroup<TypedAtom>, TypedAtom> PairStateAction;
    typedef std::map<PairStateAction, uint> MapTransitionRepetitions;
    MapTransitionRepetitions map_transition_repetitions;
    for(TransitionSet<TypedAtom>::const_iterator transition_it = begin_set(); transition_it != end_set(); ++transition_it) {
        PairStateAction pair_state_action(transition_it->get_prev_state(), transition_it->get_action());
        if (map_transition_repetitions.find(pair_state_action) == map_transition_repetitions.end()) {
            map_transition_repetitions[pair_state_action] = 0;
        }
        map_transition_repetitions[pair_state_action]+=transition_it->get_number_repetitions();
    }
    #if __cplusplus > 199711L // C++11
    uint max_repetitions = std::max_element(map_transition_repetitions.begin(), map_transition_repetitions.end(),
                                            [](const MapTransitionRepetitions::value_type& s1, const MapTransitionRepetitions::value_type& s2) {
                                                return (s1.second < s2.second);
                                            })->second;
    #else
    uint max_repetitions = 0;
    BOOST_FOREACH(const MapTransitionRepetitions::value_type& transition_repetitions, map_transition_repetitions) {
        max_repetitions = std::max(transition_repetitions.second, max_repetitions);
    }
    #endif
    
    for(TransitionSet<TypedAtom>::const_iterator transition_it = begin_set(); transition_it != end_set(); ++transition_it) {
        PairStateAction transition_state_action(transition_it->get_prev_state(), transition_it->get_action());
        uint transition_normalizer = round( (float)transition_it->get_number_repetitions() * (float)max_repetitions/(float)map_transition_repetitions[transition_state_action]);
        assert(transition_normalizer>=1);
        result.add_transition( *transition_it, transition_it->get_number_repetitions() * transition_normalizer );
    }
    return result;
}

template<>
TransitionGroup<TypedAtom> TransitionGroup<TypedAtom>::get_symbolic_lfit_transitions_unordered(const uint max_action_variables, const bool nonaction_vars_combinations) const
{
    TransitionGroup<TypedAtom> result(ppddl_object_manager);
    for(TransitionSet<TypedAtom>::const_iterator transition_it = begin_set(); transition_it != end_set(); ++transition_it) {
        TransitionGroup<TypedAtom> new_transitions = transition_it->get_symbolic_lfit_transitions(ppddl_object_manager, max_action_variables, nonaction_vars_combinations );
        for(TransitionSet<TypedAtom>::const_iterator new_transition_it = new_transitions.begin_set(); new_transition_it != new_transitions.end_set(); ++new_transition_it) {
                result.add_transition( *new_transition_it, new_transition_it->get_number_repetitions() * transition_it->get_number_repetitions());
        }
    }
    return result;
}

template<>
void Transition<TypedAtom>::name_grounding()
{
    prev_state.PPDDL_grounding();
    next_state.PPDDL_grounding();
    action.PPDDL_grounding();
}

template<>
void TransitionGroup<TypedAtom>::name_grounding()
{
    TransitionSet<TypedAtom> original_transition_set;
    original_transition_set.swap(transition_set);
    TransitionHistorial<TypedAtom> original_transition_historial;
    original_transition_historial.swap(transition_historial);
    for(typename TransitionSet<TypedAtom>::const_iterator transition_it = original_transition_set.begin(); transition_it != original_transition_set.end(); ++transition_it) {
        Transition<TypedAtom> name_grounded_transition(*transition_it);
        name_grounded_transition.name_grounding();
        add_transition(name_grounded_transition, transition_it->get_number_repetitions());
    }
}
