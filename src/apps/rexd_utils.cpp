#include "apps/applications_helper.h"
// #include <../lib/swi-prolog/include/SWI-Prolog.h>
#include "rexd/rexd_scenario.h"
#include "rexd/planner_factory.h"
#include "teacher/failure_analyzer.h"
#include "rexd/high_level_planner.h"


#define LOGA(Level_, Logger_, ...)   \
    while (Log::Enabled(Level_)) {  \
      Log_Write(Level_, Logger_, __func__, __FILE__, __LINE__, __VA_ARGS__);  \
      break;  \
    }

void Log_Write(uint level,
               char const* logger,
               char const* function,
               char const* file,
               int line,
               std::string msg)
{
    std::cout << msg;
}


void generate_problem_with_incomplete_state(Scenario scenario_to_modify, const uint file_number, const PlannerPtr planner)
{
    uint DEBUG = 1;
    PredicateGroup<TypedAtom>::const_iterator predicate_it;
    do {
        predicate_it = utils::get_random_element<PredicateGroup<TypedAtom>::const_iterator>(scenario_to_modify.get_state().begin(), scenario_to_modify.get_state().end());
    } while (predicate_it->is_special()); // obtain non-special symbol
    
    scenario_to_modify.get_modificable_state().update_predicate(predicate_it->get_negated());
    CLOG_IF(DEBUG > 1, INFO, "excuses") << "Changing " << *predicate_it << " to " << predicate_it->get_negated();
    
    // check if plan can be found
    float planning_result;
    RexdScenario rexd_scenario(scenario_to_modify);
    rexd_scenario.set_vmin();
    TypedAtomOpt new_res = planner->plan(rexd_scenario, true, 0, planning_result);
    if ( (!new_res) || (*new_res == MAKE_TYPED_ATOM_TEACHER) ) { // if no solution, then save the scenario to find excuses
        CLOG_IF(DEBUG > 0, INFO, "excuses") << ".";
        scenario_to_modify.write_PDDL_problem("simulator_ppddl_current_problem_file_" + boost::lexical_cast<std::string>(file_number) + ".pddl");
        utils::write_string_to_file("result_problem_" + boost::lexical_cast<std::string>(file_number) + ".txt", boost::lexical_cast<std::string>(predicate_it->get_negated()));
    }
}


void generate_problem_with_wrong_rules(Scenario scenario_to_modify, const uint file_number, const PlannerPtr planner)
{
    uint DEBUG = 1;
    Typed::RulePtr random_rule;
    do {
        random_rule = *utils::get_random_element<Typed::RuleSet::iterator>(scenario_to_modify.get_modificable_rules().begin(), scenario_to_modify.get_modificable_rules().end());
    } while (*random_rule == MAKE_TYPED_ATOM_FINISH_ACTION); // Don't change finish action!
    
    PredicateGroup<TypedAtom> solution;
    if ((std::rand() % 2) == 0) { // 50% add random precondition
        CLOG_IF(DEBUG > 1, INFO, "excuses") << "Adding precondition " << NOENDL;
        PredicateGroup<TypedAtom> predicates = scenario_to_modify.get_state();
        predicates.make_symbolic();
        Predicate<TypedAtom> random_predicate = *utils::get_random_element<PredicateGroup<TypedAtom>::iterator>(predicates.begin(), predicates.end());
        if ((std::rand() % 2) == 0) { // 50% true
            random_predicate.positive = true;
        }
        else {
            random_predicate.positive = false;
        }
        PredicateGroup<TypedAtom> random_rule_preconditions = random_rule->get_preconditions();
        random_rule_preconditions.update_predicate(random_predicate);
        random_rule->set_preconditions(random_rule_preconditions);
        solution.insert(random_predicate);
    }
    else { // 50% remove random outcome predicate
        CLOG_IF(DEBUG > 1, INFO, "excuses") << "Removing outcome " << NOENDL;
        OutcomeGroup<TypedAtom>::iterator random_outcome_it = utils::get_random_element<OutcomeGroup<TypedAtom>::iterator>(random_rule->outcomes.begin(), random_rule->outcomes.end());
        if (!random_outcome_it->empty()) { //if a predicate to be removed exists
            PredicateGroup<TypedAtom>::iterator random_outcome_predicate_it = utils::get_random_element<PredicateGroup<TypedAtom>::iterator>(random_outcome_it->begin(), random_outcome_it->end());
            CLOG_IF(DEBUG > 0, INFO, "excuses") << "Going to remove predicate " << *random_outcome_predicate_it << " from " << *random_outcome_it;
            solution.insert(*random_outcome_predicate_it); // has to be added before it is removed!
            PredicateGroup<TypedAtom> new_outcome_predicates = *random_outcome_it;
            new_outcome_predicates.erase(*random_outcome_predicate_it);
            Outcome<TypedAtom> new_outcome(new_outcome_predicates, random_outcome_it->probability);
            random_rule->outcomes.erase(*random_outcome_it);
            random_rule->outcomes.insert(new_outcome);
            CLOG_IF(DEBUG > 0, INFO, "excuses") << "Removing predicate " << *solution.begin() << " from " << *random_outcome_it;
        }
    }
    CLOG_IF(DEBUG > 1, INFO, "excuses") << solution;
    
    // check if plan can be found
    float planning_result;
    RexdScenario rexd_scenario(scenario_to_modify);
    rexd_scenario.set_vmin();
    TypedAtomOpt new_res = planner->plan(rexd_scenario, true, 0, planning_result);
    if ( (!new_res) || (*new_res == MAKE_TYPED_ATOM_TEACHER) ) { // if no solution, then save the scenario to find excuses
        CLOG_IF(DEBUG > 0, INFO, "excuses") << "*";
        scenario_to_modify.write_PDDL_domain("ppddl_domain_file_" + boost::lexical_cast<std::string>(file_number) + ".pddl");
        utils::write_string_to_file("result_domain_" + boost::lexical_cast<std::string>(file_number) + ".txt", boost::lexical_cast<std::string>(solution));
    }
}


void generate_set_of_problems_with_planning_failures()
{
    LOG(INFO) << "Generating domain problems with planning failures";
    ConfigReader config_reader = load_config_reader();
    RexdScenario scenario(load_scenario_ground_truth(config_reader));
    scenario.set_vmin();
    
    uint file_number = 100;
    PlannerPtr planner = PlannerFactory::get_standard_planner(config_reader);
    while (!scenario.is_in_goal()) {
        for (uint i = 0; i < 10; i++ ) {
            generate_problem_with_incomplete_state(scenario, file_number + i, planner);
            generate_problem_with_wrong_rules(scenario, file_number + i, planner);
        }
        file_number += 100;
        
        float planning_result;
        TypedAtomOpt new_res = planner->plan(scenario, true, 1, planning_result);
        if (new_res) {
            scenario.transition_next_state_simple(*new_res);
        }
        else {
            CERROR("planning") << "Couldn\'t obtain a plan to reach the goal.";
        }
    }
}

void explore()
{
    ConfigReader config_reader = load_config_reader();
    RexdScenario scenario(load_scenario(config_reader));
    TransitionGroup<TypedAtom> transitions = load_transitions(config_reader, scenario);
    Explorer explorer;
    Typed::RuleSet unknown_rules = explorer.get_unknown_rules(scenario.get_rules(), transitions, config_reader.get_variable<uint>("exploration_known_threshold"));
    LOG(INFO) << "Unknown rules are: " << unknown_rules;
}

void exploit_or_explore()
{
    ConfigReader config_reader = load_config_reader();
    RexdScenario scenario(load_scenario(config_reader));
    TransitionGroup<TypedAtom> transitions = load_transitions(config_reader, scenario);
    
    // load high_level_planner
    std::ofstream logging_file;
    logging_file.open ("/dev/null", std::ios::app);
    HighLevelPlanner high_level_planner(config_reader, &logging_file);
    PlanningFailureOpt excuse_opt;
    std::vector<std::string> actions_requiring_excuse;
    
    // test exploration in E3 exploration/exploitation algorithm
    TypedAtomOpt explore_action = high_level_planner.exploit_or_explore(scenario, transitions, excuse_opt, actions_requiring_excuse);
    LOG(INFO) << "Selected action " << explore_action;
}

void generate_excuses()
{
    LOG(INFO) << "Excuses";
    ConfigReader config_reader = load_config_reader();
    RexdScenario scenario(load_scenario(config_reader));
    scenario.set_vmin();
    
    FailureAnalyzer excuse_generator(scenario);

    PlanningFailureOpt excuse_opt = excuse_generator.get_planning_failure();
    LOG(INFO)  << "Best excuse is " << failure_analyzer_utils::planning_failure_to_string(excuse_opt);
    
    if (excuse_opt) {
        PredicateGroup<TypedAtom> symbolic_excuse(*excuse_opt);
        symbolic_excuse.make_symbolic();
        std::vector<std::string> actions_requiring_excuse = scenario.get_rules().find_rules_with_a_precondition(*excuse_opt).get_action_names();

        LOG(INFO) << "Actions requiring excuse: " << actions_requiring_excuse;

        TypedAtomOpt action_opt;
        if (config_reader.get_variable<bool>("rule_analysis_equivalent_rules")) {
            CINFO("excuses") << "Looking for equivalent rules!";
            TransitionGroup<TypedAtom> transitions = read_transitions_from_file("transitions_full.dat", 
                                                                                scenario.get_state().ppddl_object_manager,
                                                                                scenario.get_state().get_constants());
    //         float expected_reward;
    //         action_opt = excuse_generator.search_for_equivalent_useful_rules(excuse, transitions, boost::make_shared<Typed::RuleSet>(rules), expected_reward);
            // TODO
            LOG(ERROR) << "TODO " << "Typed::Rule alternatives";
        }

        // predicate subgoals are incompatible with vmin
        if ((config_reader.get_variable<bool>("rule_analysis_subgoals")) && (!config_reader.get_variable<bool>("use_rmax")) ) {
            CINFO("excuses") << "Trying subgoals";
            CERROR("excuses") << "Subgoals not working!!";
//             action_opt = plan_with_subgoals(rules, state, excuse_generator, *excuse_opt, planner, goals.get_predicates(), config_reader);
        }
    }
    else {
        CINFO("excuses") << "No excuse found!";
    }
}


void print_help()
{
    std::cout << "Usage: rexd_utils [option] [args]" << std::endl
              << "Options:" << std::endl
              << "\texcuses \t\t- Plans with excuses using current folder files" << std::endl
              << "\texplore \t\t- explores using current folder files" << std::endl
              << "\trl \t\t- explores or exploits as if RL was being executed" << std::endl
              << "\tfailures \t\t- generate random failures for the domain" << std::endl;
}


int main(int argc, char** argv) {
    // generated random seed
    srand((unsigned)time(0));
    utils::init_loggers("logger.conf");

    if (argc < 2) {
        print_help();
    }
    else {
        std::string input_arg(argv[1]);
        if (input_arg == "excuses") {
            generate_excuses();
        }
        else if (input_arg == "explore") {
            explore();
        }
        else if (input_arg == "rl") {
            exploit_or_explore();
        }
        else if (input_arg == "failures") {
            generate_set_of_problems_with_planning_failures();
        }
        else {
            print_help();
        }
    }

    return 0;
}
