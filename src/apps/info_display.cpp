#include "info_display.h"


const std::string InfoDisplay::RED_LIGHT = "red_light";
const std::string InfoDisplay::YELLOW_LIGHT = "yellow_light";
const std::string InfoDisplay::GREEN_LIGHT = "green_light";

const std::string InfoDisplay::HTML_PANEL =             "<div style=\"margin-bottom:0.4em;padding-bottom:0.1em;min-height:5em; background-color: rgb(225, 225, 225);border-radius: 15px;\">\n";
const std::string InfoDisplay::HTML_BIG_PANEL =         "<div style=\"margin-bottom:0.4em;padding-bottom:0.1em;min-height:5.5em; background-color: rgb(225, 225, 225);border-radius: 15px;\">\n";
const std::string InfoDisplay::HTML_HIGHLIGHTED_PANEL = "<div style=\"margin-bottom:0.4em;padding-bottom:0.1em;min-height:5em; background-color: rgb(195, 195, 225);border-radius: 15px;color:#0000FF;\">\n";
const std::string InfoDisplay::HTML_PANEL_END = "</div>\n";
const std::string InfoDisplay::HTML_TITLE = "<h2 style=\"font-size:130%;background-color: rgba(0, 0, 0,0.1);margin:0em;padding:0.1em;padding-left:0.4em;border-top-left-radius: 15px;border-top-right-radius: 15px;\">";
const std::string InfoDisplay::HTML_TITLE_END = "</h2>\n";

// lists
const std::string InfoDisplay::HTML_LIST_BEGIN = "<ul style=\"margin:0.2em;padding-left:15px;\">";
const std::string InfoDisplay::HTML_LIST_END = "</ul>";
// const std::string InfoDisplay::HTML_LIST_BEGIN = "";
// const std::string InfoDisplay::HTML_LIST_END = "";
const std::string InfoDisplay::HTML_ITEM_BEGIN =             "<li style=\"margin:0.2em;margin-left:1.5em;\">";
const std::string InfoDisplay::HTML_IMPORTANT_ITEM_BEGIN =   "<div style=\"margin:0.2em;margin-left:0.5em;font-size:130%;\">";
const std::string InfoDisplay::HTML_HIGHLIGHTED_ITEM_BEGIN = "<li style=\"margin:0.2em;margin-left:1.5em;color:#FF0000;\">";
const std::string InfoDisplay::HTML_SUBITEM_BEGIN =          "<li style=\"margin:0.2em;margin-left:2.5em;\">";
const std::string InfoDisplay::HTML_ITEM_END = "</li>\n";
const std::string InfoDisplay::HTML_IMPORTANT_ITEM_END =   "</div>";

const std::string InfoDisplay::HTML_SIZE = "120";

InfoDisplay::InfoDisplay():
    last_plan_length(1000)
{

}

void InfoDisplay::generate_planning_html_info(bool still_planning, const TypedAtomOpt& planning_res, std::string excuse, uint plan_length)
{
    std::stringstream planning_html_ss;
    if (still_planning) {
        planning_html_ss << HTML_HIGHLIGHTED_PANEL;
        planning_html_ss << HTML_TITLE << "Planning..." << HTML_TITLE_END;
        planning_html_ss << HTML_PANEL_END;
    }
    else {
        planning_html_ss << HTML_PANEL;
        planning_html_ss << HTML_TITLE << "Planning results" << HTML_TITLE_END;
        planning_html_ss << HTML_LIST_BEGIN;

        if (!planning_res) {
            traffic_light_color = RED_LIGHT;
            last_plan_length = 10000;
            planning_html_ss << HTML_HIGHLIGHTED_ITEM_BEGIN << "<b>No plan</b>" << HTML_ITEM_END;
//             planning_html_ss << HTML_ITEM_BEGIN << "<b>Estimated actions to finish: --</b>" << HTML_ITEM_END;
        }
        else {
            planning_html_ss << HTML_ITEM_BEGIN << "Best action: <b>" << *planning_res << "</b>" << HTML_ITEM_END;
            
            if (plan_length < 10000) {
                if (plan_length < last_plan_length) {
                    traffic_light_color = GREEN_LIGHT;
                }
                else {
                    traffic_light_color = YELLOW_LIGHT;
                }
                last_plan_length = plan_length;
                
                planning_html_ss << HTML_ITEM_BEGIN << "Estimated actions to finish: <b>" << plan_length << "</b>" << HTML_ITEM_END;
            }
        }
        if (excuse.size() > 2) { // if using some excuse -> using partial goals
            planning_html_ss << HTML_ITEM_BEGIN << "Using partial goals" << HTML_ITEM_END;
        }
        planning_html_ss << HTML_LIST_END << HTML_PANEL_END;
    }

    last_planning_result_html = planning_html_ss.str();
}


void InfoDisplay::generate_learning_html_info(bool still_learning, std::string action_name, int number_rules, Typed::RuleSet removed_rules, Typed::RuleSet added_rules)
{
    std::stringstream learning_html_ss;

    if (still_learning) {
        learning_html_ss << HTML_HIGHLIGHTED_PANEL;
        learning_html_ss << HTML_TITLE << "Learning..." << HTML_TITLE_END;
        learning_html_ss << HTML_PANEL_END;
    }
    else {
        learning_html_ss << HTML_PANEL;
        learning_html_ss << HTML_TITLE << "Learning results" << HTML_TITLE_END;
        learning_html_ss << HTML_LIST_BEGIN;

        learning_html_ss << HTML_ITEM_BEGIN << "Refined action: <b>" << action_name << "</b>" << HTML_ITEM_END;

        int rule_number_diff, preconditions_diff, outcomes_num_diff;
        double first_outcome_prob_diff;
        removed_rules.analyze_differences(added_rules, rule_number_diff, preconditions_diff, outcomes_num_diff, first_outcome_prob_diff);

        // count rules
        if (rule_number_diff == 1)
            learning_html_ss << HTML_ITEM_BEGIN << "Added <b>" << rule_number_diff << " rule</b>" << HTML_ITEM_END;
        if (rule_number_diff > 1)
            learning_html_ss << HTML_ITEM_BEGIN << "Added <b>" << rule_number_diff << " rules</b>" << HTML_ITEM_END;
        else if (rule_number_diff < 0)
            learning_html_ss << HTML_ITEM_BEGIN << "Removed <b>" << -rule_number_diff << " rules</b>" << HTML_ITEM_END;

        if ((rule_number_diff) == 0) { // no rules added -> changed other things?
            // count preconditions
            if (preconditions_diff > 0)
                learning_html_ss << HTML_ITEM_BEGIN << "Added <b>" << preconditions_diff << " preconditions</b>" << HTML_ITEM_END;
            else if (preconditions_diff < 0)
                learning_html_ss << HTML_ITEM_BEGIN << "Removed <b>" << -preconditions_diff << " preconditions</b>" << HTML_ITEM_END;

            // outcome probabilities
            if (outcomes_num_diff > 0)
                learning_html_ss << HTML_ITEM_BEGIN << "Added <b>" << outcomes_num_diff << " outcomes</b>" << HTML_ITEM_END;
            else if (outcomes_num_diff < 0)
                learning_html_ss << HTML_ITEM_BEGIN << "Removed <b>" << -outcomes_num_diff << " outcomes</b>" << HTML_ITEM_END;

            if (first_outcome_prob_diff != 0)
                learning_html_ss << HTML_ITEM_BEGIN << "Changed outcome probabilities" << HTML_ITEM_END;
        }

        learning_html_ss << HTML_LIST_END << HTML_PANEL_END;

        generate_additional_html_info(number_rules);
    }

    last_learning_result_html = learning_html_ss.str();
}


void InfoDisplay::generate_additional_html_info(int number_rules)
{
    std::stringstream additional_html_ss;

    additional_html_ss << HTML_PANEL;
    additional_html_ss << HTML_TITLE << "Additional info" << HTML_TITLE_END;
    additional_html_ss << HTML_LIST_BEGIN;

    additional_html_ss << HTML_ITEM_BEGIN << "Number of rules: <b>" << number_rules << "</b>" << HTML_ITEM_END;
    additional_html_ss << HTML_LIST_END << HTML_PANEL_END;

    last_additional_html = additional_html_ss.str();
}


void InfoDisplay::generate_status_html_info(int status, std::string status_info, std::vector<std::string> additional_status_info)
{
    std::stringstream status_html_ss;

    status_html_ss << HTML_BIG_PANEL;
    status_html_ss << HTML_TITLE << "Status" << HTML_TITLE_END;
    status_html_ss << HTML_LIST_BEGIN;

    switch (status)
    {
    case InfoDisplay::REQ_STATE:
        status_html_ss << HTML_IMPORTANT_ITEM_BEGIN << "<b>" << "Requesting state" << "</b>" << HTML_IMPORTANT_ITEM_END;
        break;
    case InfoDisplay::PLANNING:
        status_html_ss << HTML_IMPORTANT_ITEM_BEGIN << "<b>" << "Planning" << "</b>" << HTML_IMPORTANT_ITEM_END;
        break;
    case InfoDisplay::LEARNING:
        status_html_ss << HTML_IMPORTANT_ITEM_BEGIN << "<b>" << "Learning" << "</b>" << HTML_IMPORTANT_ITEM_END;
        status_html_ss << HTML_ITEM_BEGIN << "Learning action: <b>" << status_info << "</b>" << HTML_ITEM_END;
        break;
    case InfoDisplay::REQ_TEACHER:
        status_html_ss << HTML_IMPORTANT_ITEM_BEGIN << "<b>" << "Requesting teacher action" << "</b>" << HTML_IMPORTANT_ITEM_END;
        status_html_ss << HTML_ITEM_BEGIN << "Missing: <b>" << status_info << "</b>" << HTML_ITEM_END;
        for (std::vector< std::string >::iterator it = additional_status_info.begin(); it != additional_status_info.end(); ++it) {
            status_html_ss << HTML_SUBITEM_BEGIN << "Required by action: <b>" << *it << "</b>" << HTML_ITEM_END;
        }
        break;
    case InfoDisplay::EXEC_ACTION:
        status_html_ss << HTML_IMPORTANT_ITEM_BEGIN << "<b>" << "Executing action" << "</b>" << HTML_IMPORTANT_ITEM_END;
        status_html_ss << HTML_ITEM_BEGIN << "Executing action: <b>" << status_info << "</b>" << HTML_ITEM_END;
        break;
    case InfoDisplay::WAITING_USER_ACTION:
        status_html_ss << HTML_IMPORTANT_ITEM_BEGIN << "<b>" << "Waiting for user interaction" << "</b>" << HTML_IMPORTANT_ITEM_END;
//             status_html_ss << HTML_ITEM_BEGIN << "Recommended action: <b>" << status_info << "</b>" << HTML_TITLE_END;
        break;
    case InfoDisplay::IS_GOAL:
        last_plan_length = 0;
        last_planning_result_html = "";
        status_html_ss << HTML_IMPORTANT_ITEM_BEGIN << "<b>" << "Goal state!" << "</b>" << HTML_IMPORTANT_ITEM_END;
        break;
    default:
        status_html_ss << HTML_IMPORTANT_ITEM_BEGIN << "<b>" << "Status unknown" << "</b>" << HTML_IMPORTANT_ITEM_END;
        break;
    }

    status_html_ss << HTML_LIST_END << HTML_PANEL_END;

    last_status_html = status_html_ss.str();
}


std::string InfoDisplay::get_html_info()
{
    std::stringstream result;

//     result << "<font size=\"" << HTML_FONT_SIZE << "\">";
    result << "<body style=\"font-size:" << HTML_SIZE << "%;\">";
    result << last_status_html << last_planning_result_html << last_learning_result_html; //<< last_additional_html;
    result << "</body>";
//     result << "</font>";

    return result.str();
}


void InfoDisplay::set_green_light()
{
    traffic_light_color = GREEN_LIGHT;
}
