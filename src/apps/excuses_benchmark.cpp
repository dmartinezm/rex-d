#include "excuses_benchmark.h"
#include "boost/filesystem/operations.hpp"

#define LOGA(Level_, Logger_, ...)   \
    while (Log::Enabled(Level_)) {  \
      Log_Write(Level_, Logger_, __func__, __FILE__, __LINE__, __VA_ARGS__);  \
      break;  \
    }



Scenario load_scenario(const ConfigReader& config_reader)
{
    return Scenario(config_reader.get_path("ppddl_domain_file"), config_reader.get_path("simulator_ppddl_current_problem_file"), config_reader);
}

std::vector<std::string> get_actions_requiring_excuse(const Scenario& scenario, FailureAnalyzer& failure_analyzer)
{
    std::vector<std::string> actions_requiring_excuse;
    
    PlanningFailureOpt excuse_opt = failure_analyzer.get_planning_failure();
    if (excuse_opt) {
        Typed::RuleSet rules_requiring_excuse = scenario.get_rules().find_rules_with_a_precondition(*excuse_opt);
        rules_requiring_excuse.remove_nop_rules();
        actions_requiring_excuse = rules_requiring_excuse.get_action_names();
    }
    
    return actions_requiring_excuse;
}

ListPredicateGroup::ListPredicateGroup(std::string predicates_file_str) 
{
    std::stringstream ss(predicates_file_str);

    std::string item;
    while (std::getline(ss, item)) {
        item = utils::trim(item);

        if (item.size() > 1) { 
            push_back(PredicateGroup<TypedAtom>(item));
        }
    }
}
    
bool ListPredicateGroup::has_predicate_group(const PredicateGroup<TypedAtom>& predicates) const
{
    bool result = false;
    
    BOOST_FOREACH(const PredicateGroup<TypedAtom>& predicates_it, *this) {
        if (predicates_it == predicates) {
            result = true;
        }
    }
    
    return result;
}

ListPredicateGroup ListPredicateGroup::read_from_file(const std::string& file_path)
{
    std::string partial_results_str;
    try {
        partial_results_str = utils::get_file_contents(file_path, true);
    }
    catch (std::runtime_error& exception) { // the file may not exist
        partial_results_str = "";
    }
    return ListPredicateGroup(partial_results_str);
}

void ExcuseBenchmark::test_excuses(const boost::filesystem::path& folder, 
                                   const std::string& config_file, 
                                   const ListPredicateGroup& possible_expected_excuses,
                                   const ListPredicateGroup& possible_partial_excuses)
{
        LOG(INFO) << "";
        LOG(INFO) << "Analyzing: " << folder;
        // load config
        ConfigReader config_reader;
        config_reader.init(config_file, folder.string());
        
        // load high_level_planner
//         TransitionGroup transitions = read_transitions_from_file(config_reader.get_path("transitions_backup_path"));
        Scenario scenario = load_scenario(config_reader);
        FailureAnalyzer excuse_generator(scenario);
        PlanningFailureOpt excuse_opt = excuse_generator.get_planning_failure();
        std::vector<std::string> actions_requiring_excuse = get_actions_requiring_excuse(scenario, excuse_generator);
        
        // Check excuse
        if ((excuse_opt) && (possible_expected_excuses.has_predicate_group(*excuse_opt)) ) {
            results.successful_n++;
        }
        else if ((excuse_opt) && (possible_partial_excuses.has_predicate_group(*excuse_opt)) ) {
            results.partial_n++;
        }
        else {
            LOG(WARNING) << START_COLOR_YELLOW << "Obtained excuse " << failure_analyzer_utils::planning_failure_to_string(excuse_opt) << ", when best excuse is " << possible_expected_excuses.front() << ". Test: " << folder << END_COLOR;
            ExcuseError possible_error(excuse_opt, possible_expected_excuses, folder);
            results.add_possible_error(possible_error);
            results.failed_n++;
        }
}


void ExcuseBenchmark::test_excuses(const std::string& config_file)
{
    boost::filesystem::path p(boost::filesystem::current_path());
    
    p = p / domain / problem;
    
    if (!boost::filesystem::is_directory(p)) {
        LOG(ERROR) << p << " does not exist!";
        return;
    }
    else {
        std::vector<std::string> failure_types;
        failure_types.push_back("domain");
        failure_types.push_back("problem");
        number_vars_per_configs = failure_types.size();
        current_vars_per_configs = 0;
        BOOST_FOREACH(const std::string& failure_type, failure_types) {
            boost::filesystem::path problem_path = p / failure_type;
            boost::filesystem::directory_iterator problem_iter(problem_path);
            boost::filesystem::directory_iterator p_iter_end;
            current_instances_per_var = 0;
            number_instances_per_var = std::distance(boost::filesystem::directory_iterator(problem_path),p_iter_end);
            for ( ; problem_iter !=p_iter_end; ++problem_iter) {
                if ((!instance) || (problem_iter->path().stem().string() == *instance) ) {
                    // Good ground truth excuses
                    boost::filesystem::path results_file = problem_iter->path() / "result.txt";
                    
                    // Partially good excuses
                    boost::filesystem::path partial_results_file = problem_iter->path() / "partial_results.txt";
                    
                    test_excuses(problem_iter->path(), 
                                 config_file, 
                                 ListPredicateGroup::read_from_file(results_file.string()), 
                                 ListPredicateGroup::read_from_file(partial_results_file.string()));
                }
                current_instances_per_var++;
                current_total_instances++;
                std::cout << "";
                
                print_progress(current_total_instances, number_configs*number_vars_per_configs*number_instances_per_var);
            }
            current_vars_per_configs++;
        }
        
        // analyze results
        if (ask_for_error_confirmation) {
            results.confirm_errors(config_file);
        }
        boost::filesystem::path p_results(boost::filesystem::current_path());
        p_results = p_results / domain / problem / "results";
        boost::filesystem::path config_dir(config_file);
        results.print_results(p_results, config_dir.stem());
    }
}

void ExcuseBenchmark::test_excuses_all_configs()
{
    boost::filesystem::path p(boost::filesystem::current_path());
    
    p = p / domain / problem / "configs";
    
    if (!boost::filesystem::is_directory(p)) {
        LOG(ERROR) << p << " does not exist!";
        return;
    }
    else {
        boost::filesystem::directory_iterator configs_iter(p);
        boost::filesystem::directory_iterator configs_end;
        
        number_configs = std::distance(boost::filesystem::directory_iterator(p),configs_end);
        current_config = 0;
        for ( ; configs_iter !=configs_end; ++configs_iter) { // iterate config files
            test_excuses(configs_iter->path().string());
            current_config++;
        }
    }
}

void ExcuseBenchmark::print_progress(const uint current_progress, const uint total)
{
    if (!instance) { // don't print progress for a simple instance
        float progress = float(current_progress) / float(total);
        int bar_width = 100;

        std::cout << "[";
        int pos = bar_width * progress;
        for (int i = 0; i < bar_width; ++i) {
            if (i < pos) {
                std::cout << "=";
            }
            else if (i == pos) {
                std::cout << ">";
            }
            else {
                std::cout << " ";
            }
        }
        std::cout << "] " << int(progress * 100.0) << " %" << std::endl;// << "\r";
//     std::cout.flush();
    }
}

void ExcuseBenchmarkResults::confirm_errors(const std::string& config_file)
{
    for (std::vector< ExcuseError >::iterator error_it = possible_errors.begin(); error_it != possible_errors.end(); ) {
        if (error_it->obtained_excuse_opt) {
            bool response_ok = false;
            bool response_partially;
            bool response_yes;
            do {
                ConfigReader config_reader;
                config_reader.init(config_file, error_it->test_id.string());
                Scenario scenario = load_scenario(config_reader);
                Typed::RuleSet rules_with_prec = scenario.get_rules().find_rules_with_a_precondition(error_it->actual_excuses.front());
                LOG(INFO) << "";
                LOG(INFO) << "Obtained excuse " << START_COLOR_YELLOW << failure_analyzer_utils::planning_failure_to_string(error_it->obtained_excuse_opt) << END_COLOR << 
                            ", when good excuses are " << START_COLOR_YELLOW << error_it->actual_excuses << END_COLOR << 
                            ". Test: " << error_it->test_id;
                LOG(INFO) << "Typed::Rules with excuse as precondition: " << rules_with_prec;
                LOG(INFO) << "Is " << error_it->test_id.string() << " with excuse " << failure_analyzer_utils::planning_failure_to_string(error_it->obtained_excuse_opt) << " ok (yes/partially/no)? [y/p/n]";
                std::string human_response = utils::read_string_from_stream(std::cin);
                if ( (utils::remove_spaces(human_response) == "y") ||
                    (utils::remove_spaces(human_response) == "yes") ) {
                    response_ok = true;
                    response_yes = true;
                    response_partially = true;
                }
                else if ( (utils::remove_spaces(human_response) == "p") ||
                        (utils::remove_spaces(human_response) == "partially") )
                {
                    response_ok = true;
                    response_yes = false;
                    response_partially = true;
                }
                else {
                    response_ok = true;
                    response_yes = false;
                    response_partially = true;
                }
            } while (!response_ok);
            
            if (response_yes) {
                // add excuse as valid
                boost::filesystem::path results_file_path = error_it->test_id / "result.txt";
                utils::append_string_to_file(results_file_path.string(), "\n" + failure_analyzer_utils::planning_failure_to_string(error_it->obtained_excuse_opt));
                error_it = possible_errors.erase(error_it);
                failed_n--;
                successful_n++;
            }
            else if (response_partially) {
                boost::filesystem::path results_file_path = error_it->test_id / "partial_results.txt";
                utils::append_string_to_file(results_file_path.string(), "\n" + failure_analyzer_utils::planning_failure_to_string(error_it->obtained_excuse_opt));
                // partial_errors.push_back(*error_it);
                error_it = possible_errors.erase(error_it);
                failed_n--;
                partial_n++;
            }
            else {
                ++error_it;
            }
        }
    }
}

void ExcuseBenchmarkResults::print_results(const boost::filesystem::path& results_folder, const boost::filesystem::path& config_name)
{
    boost::filesystem::path result_path = results_folder / config_name;
    result_path.replace_extension(".txt");
    
    std::string result;
    result = boost::lexical_cast<std::string>(successful_n) + " " + boost::lexical_cast<std::string>(failed_n);
    utils::write_string_to_file(result_path.string(), result);
    
    LOG(INFO) << "Succeeded: " << successful_n << " | Partial: " << partial_n << " | Failed: " << failed_n;
}


void generate_excuses(const std::string& domain, const std::string& problem, const bool confirm_errors, const std::string instance)
{
    ExcuseBenchmark excuses_benchmark(domain, problem, confirm_errors, instance);
    excuses_benchmark.test_excuses_all_configs();
}


void print_help()
{
    std::cout << "Usage: excuses_bench domain problem [--confirm_errors]" << std::endl;
}


int main(int argc, char** argv) {
    // generated random seed
    srand((unsigned)time(0));
    utils::init_loggers("logger.conf");

    if (argc < 3) {
        print_help();
    }
    else if (argc == 3) {
        generate_excuses(argv[1], argv[2], false);
    }
    else if (argc == 4) {
        generate_excuses(argv[1], argv[2], true);
    }
    else if (argc == 5) {
        generate_excuses(argv[1], argv[2], true, argv[4]);
    }

    return 0;
}
