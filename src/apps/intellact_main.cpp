#include "intellact_main.h"


using boost::asio::ip::tcp;
// using boost::timer::cpu_timer;


/**
 * Constructor
 */
IntellactServer::IntellactServer():
    slow_execution(false),
    current_vmin_increase_num(0),
    current_vmin_increase_episode(0)
{
    logging_file.open ("intellact_logging.txt", std::ios::app);
    logging_file << "reset start" << std::endl;
    init("rexd_config.cfg");

    if (config_reader.get_variable<bool>("auto_experiments")) {
        reset_command = "reset";
    }

    int port_vr = config_reader.get_variable<uint>("simulator_port");
    if (port_vr == 0) { // auto selection
        port_vr = utils::get_socket_from_current_dir_hash();
    }

    if (config_reader.get_variable<bool>("simulator")) {
        CINFO("application") << "IntellactServer: " << "Connecting to VR on port " << port_vr << "..." << NOENDL;
        tcp::endpoint endpoint_vr(tcp::v4(), port_vr);
        tcp::acceptor acceptor_vr(io_service, endpoint_vr);
        acceptor_vr.accept(*vr_server.rdbuf());
        CINFO("application") << " Connected!";
        vr_ostream = &vr_server;
        vr_istream = &vr_server;
    }
    else {
        vr_ostream = &std::cout;
        vr_istream = &std::cin;
    }

    info_ostream = &std::cout;

    if (config_reader.get_variable<bool>("simulator")) {
//         get_new_command();
    }
    else {
        CINFO("application") << "IntellactServer: " << "Press ENTER to start..." << NOENDL;
        std::cin.ignore(1);
    }
}

IntellactServer::~IntellactServer()
{
    logging_file.close();
}


std::string IntellactServer::load_next_episode()
{
    std::string command;
    current_episode++;
    command=reset_command;
    CINFO("application") << "Sending command " << command;

    // logging and save info
    std::stringstream ss_current_episode;
    ss_current_episode << std::setfill('0') << std::setw(3) << current_episode;
    scenario_ptr->get_rules().write_rules_to_file("rules" + boost::lexical_cast<std::string>(current_problem)  + "-" + ss_current_episode.str() + ".dat");
    logging_file << "Num: " << current_problem << "-" << current_episode << std::endl;
    return command;
}

std::string IntellactServer::load_next_problem_auto()
{
    std::string command;

    current_episode = 0;
    current_problem++;
    std::stringstream ss;
    ss << "reset" << current_problem;
    reset_command = ss.str();
    command = reset_command;
    if (current_problem > config_reader.get_variable<uint>("auto_experiments_auto_next_num")) {
        command=SIMULATOR_COMMAND_FINISH;
    }
    else {
        load_next_episode();
    }
    return command;
}

void IntellactServer::increase_vmin()
{
    if (current_vmin_increase_num < config_reader.get_variable<uint>("auto_experiments_increasing_vmin_num")) {
        current_vmin_increase_episode++;
        if (current_vmin_increase_episode >= config_reader.get_variable<uint>("auto_experiments_increasing_vmin_step")) {
            current_vmin_increase_episode = 0;
            current_vmin_increase_num++;
            // Note that this implies only one global ConfigReader should be present for the scenario
            scenario_ptr->config_reader.set_variable<float>("vmin_value", 
                                                            scenario_ptr->config_reader.get_variable<float>("vmin_value") +
                                                            scenario_ptr->config_reader.get_variable<float>("auto_experiments_increasing_vmin_quantity"));
            scenario_ptr->reset(); // this resets v_min value
            CINFO("application") << "Vmin is now " << config_reader.get_variable<float>("vmin_value");
            logging_file << "Vmin increased to " << config_reader.get_variable<float>("vmin_value") << std::endl;
        }
    }
}

std::string IntellactServer::load_next_problem_command()
{
    std::string command;

    current_episode = 0;
    command = utils::read_string_from_stream(std::cin);
    command.erase(std::remove(command.begin(), command.end(), '\n'), command.end());
    reset_command = command;

    load_next_episode();
    return command;
}

bool IntellactServer::reset_scenario()
{
    if (!config_reader.get_variable<bool>("simulator")) {
        LOG(INFO) << START_COLOR_RED << "Press ENTER to start..." << END_COLOR ;
        std::cin.ignore(1);
        return true;
    }

    CINFO("application") << START_COLOR_RED << "REQUEST USER COMMAND: " << END_COLOR;
    std::string command;

    if (config_reader.get_variable<bool>("auto_experiments")) {
        if (config_reader.get_variable<bool>("auto_experiments_increasing_vmin")) {
            increase_vmin();
        }
        
        if (current_episode < config_reader.get_variable<uint>("auto_experiments_num")) {
            command = load_next_episode();
        }
        else if (config_reader.get_variable<bool>("auto_experiments_auto_finish")) {
            command=SIMULATOR_COMMAND_FINISH;
        }
        else if (config_reader.get_variable<bool>("auto_experiments_auto_next")) {
            // ask new command after NUM_AUTO_EXPERIMENTS resets
            command=load_next_problem_auto();
        }
        else if (config_reader.get_variable<bool>("auto_experiments_command")) {
            command=load_next_problem_command();
        }
    }
    else {
        command = utils::read_string_from_stream(std::cin);
        command.erase(std::remove(command.begin(), command.end(), '\n'), command.end());
    }
    return send_command(command);
}

bool IntellactServer::send_command(std::string command)
{
    write_to_stream(*vr_ostream, command);
    logging_file << "Command: " << command << std::endl;

    if (command.find(SIMULATOR_COMMAND_FINISH)!=std::string::npos) {
        CINFO("application") << "Received command " << SIMULATOR_COMMAND_FINISH;
        return false;
    }
    else {
        return true;
    }
}

State IntellactServer::read_state()
{
    int DEBUG = 0;
    LOG(INFO) << START_COLOR_WHITE << "Reading state..." << END_COLOR << NOENDL;

    write_to_stream(*vr_ostream, REQUEST_STATE_MSG);
    std::string state_string = utils::read_string_from_stream(*vr_istream);
    std::string object_string = utils::read_string_from_stream(*vr_istream);

    State state(state_string);
    state.ppddl_object_manager = ppddl_parser::read_objects_from_simple_string(object_string);
    state.set_constants(scenario_ptr->get_state().get_constants());

    if (DEBUG > 0)
        LOG(INFO) << "State string is " << state_string << '\n' << "Objects string is " << object_string;
    else
        LOG(INFO) << " done!";
    
    return state;
}

float IntellactServer::read_reward()
{
    std::string reward_string = utils::read_string_from_stream(*vr_istream);
    return boost::lexical_cast<float>(reward_string);
}

void IntellactServer::show_info_request_state()
{
    write_to_monitoring_screen(InfoDisplay::REQ_STATE);
}

void IntellactServer::show_info_is_goal()
{
    write_to_monitoring_screen(InfoDisplay::IS_GOAL);
}

void IntellactServer::show_info_start_learning()
{
    info_displayer.generate_learning_html_info(true);
    write_to_monitoring_screen(InfoDisplay::LEARNING, previous_action->name);
}

void IntellactServer::show_info_end_learning(const Typed::RuleSet& removed_rules, const Typed::RuleSet& added_rules, const std::string context)
{
    info_displayer.generate_learning_html_info(false, previous_action->name, scenario_ptr->get_rules().size(), removed_rules, added_rules);
    write_to_monitoring_screen(InfoDisplay::LEARNING, previous_action->name);
}

void IntellactServer::show_info_end_planning(const TypedAtomOpt& planning_res, const std::string& excuse, const uint plan_length)
{
    info_displayer.generate_planning_html_info(false, planning_res, excuse, plan_length);
    write_to_monitoring_screen(InfoDisplay::PLANNING);
}

void IntellactServer::show_info_teacher_request(const std::string& excuse, const std::vector<std::string>& actions_requiring_excuse)
{
    write_to_monitoring_screen(InfoDisplay::REQ_TEACHER, excuse, actions_requiring_excuse);
}

void IntellactServer::show_info_start_planning()
{
    info_displayer.generate_planning_html_info(true);
    write_to_monitoring_screen(InfoDisplay::PLANNING);
}

void IntellactServer::execute_action(TypedAtom action_full, bool teacher)
{
    previous_action = action_full;

    std::string vr_action_string = boost::lexical_cast<std::string>(symbolic::transform_to_nontyped(*previous_action));
    write_to_monitoring_screen(InfoDisplay::EXEC_ACTION, vr_action_string);

    std::string sent_action_str;
    if (teacher) {
        std::stringstream ss;
        ss << vr_action_string << "+";
        sent_action_str = ss.str();
    }
    else {
        sent_action_str = vr_action_string;
    }
    write_to_stream(*vr_ostream, sent_action_str);

    // wait for VR
    if (config_reader.get_variable<bool>("simulator")) {
        std::string action_result = utils::read_string_from_stream(*vr_istream);
        utils::read_string_from_stream(*vr_istream);
        if (action_result.find("failed") != std::string::npos) {
            logging_file << "Failed: " << action_full << std::endl;
        }
        if (action_result.find("applied_not_success") != std::string::npos) {
            logging_file << "Noise: " << action_full << std::endl;
        }
    }

    logging_file << "Action: " << action_full << std::endl;
}

TypedAtomOpt IntellactServer::ask_confirmation_dangerous_action(const Scenario& scenario, const TypedAtomOpt& planning_res)
{
    assert(planning_res);
    TypedAtomOpt action_opt;
    LOG(INFO) << "Input action that should be executed (press enter if selected action is good already):";

    if (config_reader.get_variable<bool>("auto_teacher_dangerous_actions")) {
        // HACK: check repeated
//         if (last_confirmed_dangerous_action != *planning_res) {
            float actual_prob_success = auto_teacher_ptr->get_expected_action_success_probability(scenario, *planning_res);
            if (actual_prob_success < 1.0) {
//                 last_confirmed_dangerous_action = *planning_res;
                action_opt = auto_teacher_ptr->get_action_simple(scenario, 0);
                // if no action obtained, or is equal to proposed action, set action_ptr to null
                if (action_opt && ( (*action_opt == *planning_res) || (*action_opt == MAKE_TYPED_ATOM_TEACHER) ) ) {
                    action_opt = TypedAtomOpt(); // no new action
                }
                if (action_opt) {
                    logging_file << "Dangerous action confirmed" << std::endl;
                    CINFO("teacher") << "Auto teacher recommends " << *action_opt << " instead of dangerous action.";
                }
            }
//         }
//         else { // Dangerous action is being confirmed in a loop!
//             CINFO("teacher") << "Dangerous action " << *planning_res << " in a loop! Executing it instead of asking confirmation...";
//             logging_file << "Dangerous action " << *planning_res << " in a loop! Ignoring danger..." << std::endl;
//         }
    }
    else {
        action_opt = read_action();
    }

    if ((!action_opt) || (!action_opt->is_valid())) {
        LOG(INFO) << START_COLOR_GREEN << "Action is not dangerous or it is necessary." << END_COLOR;
        high_level_planner->set_acceptable_dead_end_probability();
        return planning_res;
    }
    else {
        LOG(INFO) << "Teacher selected action: " << START_COLOR_PURPLE << *action_opt << END_COLOR;
        logging_file << "Teacher: " << *action_opt << std::endl;
        high_level_planner->transition_context = "TeacherDangerous";
        return action_opt;
    }
}

TypedAtomOpt IntellactServer::read_action()
{
    utils::send_notification_msg("REX-D", "Requiring teacher demonstration! ");
    std::string action_string = utils::read_string_from_stream(std::cin);
    if (action_string.empty()) {
        return TypedAtomOpt();
    }
    else {
        NonTypedAtom non_typed_action(action_string);
        return TypedAtomOpt(scenario_ptr->get_state().ppddl_object_manager.get_typed_atom(non_typed_action));
    }
}

void IntellactServer::write_to_monitoring_screen(int status, std::string status_info, std::vector<std::string> additional_status_info)
{
    std::string html_string;
    info_displayer.generate_status_html_info(status, status_info, additional_status_info);

//     html_ss << traffic_light_color << std::endl;
    html_string = info_displayer.get_html_info();

    write_html_to_file(html_string);
}

void IntellactServer::write_html_to_file(std::string html_string) {
    std::ofstream html_file;
    html_file.open("info.html");
    if (html_file.is_open())
    {
        html_file << html_string << std::endl;
        html_file.close();
    }
    else std::cerr << "Unable to open file info.html";
}

void IntellactServer::write_to_stream(std::ostream& output_stream, std::string msg, bool debug_info)
{
    if (debug_info) {
        CINFO("application") << "Sending msg: " << msg;
    }
    output_stream << msg << std::endl;
}


int main(int argc, char** argv) {
    MT::initCmdLine(argc, argv);

//     try {
    IntellactServer int_server;
    bool is_running = true;
    while (is_running) {
        is_running = int_server.main_iteration();
    }
//     }
//     catch ( std::exception& e )
//     {
//         CINFO("application") << "Exception was caught:" << e.what() << "\nExiting.\n";
//         return 1;
//     }
    return 0;
}

