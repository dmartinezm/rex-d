#include "task_visualizers.h"

TaskVisualizer::Ptr TaskVisualizerFactory::get_task_visualizer(const ConfigReader& config_reader)
{
#if __cplusplus > 199711L // C++11
    if (config_reader.get_variable<std::string>("domain") == DOMAIN_ELEVATORS) {
        return TaskVisualizer::Ptr(new ElevatorsTaskVisualizer());
    }
    else if (config_reader.get_variable<std::string>("domain") == DOMAIN_CROSSING_TRAFFIC) {
        return TaskVisualizer::Ptr(new CrossingTaskVisualizer());
    }
    else if (config_reader.get_variable<std::string>("domain") == DOMAIN_TRIANGLE_TIREWORLD) {
        return TaskVisualizer::Ptr(new TriangleTaskVisualizer());
    }
    else 
#endif
    if (config_reader.get_variable<std::string>("domain") == DOMAIN_TABLEWARE) {
        return TaskVisualizer::Ptr(new TablewareTaskVisualizer());
    }
    else {
        return TaskVisualizer::Ptr(new TaskVisualizer());
    }
}

#if __cplusplus > 199711L // C++11

/**********************************/

#define PPDDL_TYPE_FLOOR "floor"
#define PPDDL_TYPE_ELEVATOR "elevator"

#define PPDDL_PREDICATE_ATFLOOR "elevatorAtFloor"
#define PPDDL_PREDICATE_CLOSED "elevatorClosed"
#define PPDDL_PREDICATE_DIRUP "elevatorDirUp"
#define PPDDL_PREDICATE_WAITINGUP "personWaitingUp"
#define PPDDL_PREDICATE_WAITINGDOWN "personWaitingDown"
#define PPDDL_PREDICATE_GOINGUP "personInElevatorGoingUp"
#define PPDDL_PREDICATE_GOINGDOWN "personInElevatorGoingDown"



ElevatorsTaskVisualizer::ElevatorsTaskVisualizer() 
{
    std::vector<std::string> elevator_names = {"e0","e1","e2","e3"};
    std::vector<std::string> floor_names = {"f0","f1","f2","f3","f4","f5"};
    
     // C++11
    std::for_each(elevator_names.begin(), elevator_names.end(), 
                  [this](const std::string& name) {elevator_objects.push_back(PPDDL::FullObject(name, PPDDL_TYPE_ELEVATOR));} );
    std::for_each(floor_names.begin(), floor_names.end(), 
                  [this](const std::string& name) {floor_objects.push_back(PPDDL::FullObject(name, PPDDL_TYPE_FLOOR));} );
}

void ElevatorsTaskVisualizer::draw_current_state(const Scenario& scenario) const
{
    int number_floors = scenario.get_state().ppddl_object_manager.get_type_objects(PPDDL_TYPE_FLOOR).size();
    int number_elevators = scenario.get_state().ppddl_object_manager.get_type_objects(PPDDL_TYPE_ELEVATOR).size();
    
    for (int floor_idx = number_floors - 1; floor_idx >= 0; --floor_idx) {
        if (person_waiting_up_at_floor(scenario, floor_idx)) {
            std::cout << "人↑";
        }
        else {
            std::cout << "   ";
        }
        if (person_waiting_down_at_floor(scenario, floor_idx)) {
            std::cout << "人↓";
        }
        else {
            std::cout << "   ";
        }
        std::cout << "\t- ";
        
        // elevators
        for (int elevator_idx = number_elevators - 1; elevator_idx >= 0; --elevator_idx) {
            if (elevator_at_floor(scenario, elevator_idx, floor_idx)) {
                if (elevator_closed(scenario, elevator_idx)) {
                    std::cout << "■";
                }
                else {
                    std::cout << "口";
                }
                if (elevator_dir_up(scenario, elevator_idx)) {
                    std::cout << "↑";
                }
                else {
                    std::cout << "↓";
                }
                if (person_in_elevator_going_up(scenario, elevator_idx)) {
                    std::cout << "人↑";
                }
                
                else {
                    std::cout << "   ";
                }
                if (person_in_elevator_going_down(scenario, elevator_idx)) {
                    std::cout << "人↓";
                }
                else {
                    std::cout << "   ";
                }
            }
            else {
                std::cout << "  " << "  " << "  ";
            }
            if (elevator_idx < (number_elevators - 1)) {
                std::cout << " , ";
            }
        }
        std::cout << std::endl;
    }
}


bool ElevatorsTaskVisualizer::elevator_dir_up(const Scenario& scenario, const uint elevator) const
{
    std::vector<PPDDL::FullObject> params = {elevator_objects[elevator]};
    Predicate<TypedAtom> required_predicate(TypedAtom(PPDDL_PREDICATE_DIRUP, params), true);
    return scenario.get_state().has_predicate(required_predicate);
}

bool ElevatorsTaskVisualizer::person_in_elevator_going_up(const Scenario& scenario, const uint elevator) const
{
    std::vector<PPDDL::FullObject> params = {elevator_objects[elevator]};
    Predicate<TypedAtom> required_predicate(TypedAtom(PPDDL_PREDICATE_GOINGUP, params), true);
    return scenario.get_state().has_predicate(required_predicate);
}

bool ElevatorsTaskVisualizer::person_in_elevator_going_down(const Scenario& scenario, const uint elevator) const
{
    std::vector<PPDDL::FullObject> params = {elevator_objects[elevator]};
    Predicate<TypedAtom> required_predicate(TypedAtom(PPDDL_PREDICATE_GOINGDOWN, params), true);
    return scenario.get_state().has_predicate(required_predicate);
}

bool ElevatorsTaskVisualizer::person_waiting_up_at_floor(const Scenario& scenario, const uint floor) const
{
    std::vector<PPDDL::FullObject> params = {floor_objects[floor]};
    Predicate<TypedAtom> required_predicate(TypedAtom(PPDDL_PREDICATE_WAITINGUP, params), true);
    return scenario.get_state().has_predicate(required_predicate);
}

bool ElevatorsTaskVisualizer::person_waiting_down_at_floor(const Scenario& scenario, const uint floor) const
{
    std::vector<PPDDL::FullObject> params = {floor_objects[floor]};
    Predicate<TypedAtom> required_predicate(TypedAtom(PPDDL_PREDICATE_WAITINGDOWN, params), true);
    return scenario.get_state().has_predicate(required_predicate);
}

bool ElevatorsTaskVisualizer::elevator_at_floor(const Scenario& scenario, const uint elevator, const uint floor) const
{
    std::vector<PPDDL::FullObject> params = {elevator_objects[elevator], floor_objects[floor]};
    Predicate<TypedAtom> required_predicate(TypedAtom(PPDDL_PREDICATE_ATFLOOR, params), true);
    return scenario.get_state().has_predicate(required_predicate);
}

bool ElevatorsTaskVisualizer::elevator_closed(const Scenario& scenario, const uint elevator) const
{
    std::vector<PPDDL::FullObject> params = {elevator_objects[elevator]};
    Predicate<TypedAtom> required_predicate(TypedAtom(PPDDL_PREDICATE_CLOSED, params), true);
    return scenario.get_state().has_predicate(required_predicate);
}


/*******************************/

#define PPDDL_TYPE_XPOS "xpos"
#define PPDDL_TYPE_YPOS "ypos"

#define PPDDL_PREDICATE_OBSTACLE_AT "obstacle-at"
#define PPDDL_PREDICATE_ROBOT_AT "robot-at"


CrossingTaskVisualizer::CrossingTaskVisualizer() {
    std::vector<std::string> xpos_names = {"x1","x2","x3","x4","x5","x6","x7"};
    std::vector<std::string> ypos_names = {"y1","y2","y3","y4","y5","y6","y7"};
    
     // C++11
    std::for_each(xpos_names.begin(), xpos_names.end(), 
                  [this](const std::string& name) {xpos_objects.push_back(PPDDL::FullObject(name, PPDDL_TYPE_XPOS));} );
    std::for_each(ypos_names.begin(), ypos_names.end(), 
                  [this](const std::string& name) {ypos_objects.push_back(PPDDL::FullObject(name, PPDDL_TYPE_YPOS));} );
}

void CrossingTaskVisualizer::draw_current_state(const Scenario& scenario) const
{
    int number_xpos = scenario.get_state().ppddl_object_manager.get_type_objects(PPDDL_TYPE_XPOS).size();
    int number_ypos = scenario.get_state().ppddl_object_manager.get_type_objects(PPDDL_TYPE_YPOS).size();
    assert(number_xpos == number_ypos);
    
    for (int xpos_idx = 0; xpos_idx < (number_xpos + 1); ++xpos_idx) {
        std::cout << "__"; // note that it is larger than a normal character
    }
    std::cout << std::endl;
    for (int ypos_idx = number_xpos - 1; ypos_idx >= 0; --ypos_idx) {
        std::cout << "│";
        for (int xpos_idx = 0; xpos_idx < number_xpos; ++xpos_idx) {
            if (robot_at(scenario, xpos_idx, ypos_idx) && obstacle_at(scenario, xpos_idx, ypos_idx)) {
                std::cout << START_COLOR_RED << "X" << END_COLOR;
            }
            else if (robot_at(scenario, xpos_idx, ypos_idx)) {
                std::cout << "r";
            }
            else if (obstacle_at(scenario, xpos_idx, ypos_idx)) {
                std::cout << "o";
            }
            else {
                std::cout << " ";
            }
            std::cout << " "; // empty space between tiles
        }
        std::cout << "│";
        std::cout << std::endl;
    }
    for (int xpos_idx = 0; xpos_idx < (number_xpos + 1); ++xpos_idx) {
        std::cout << "‾‾";
    }
    std::cout << std::endl;
}

bool CrossingTaskVisualizer::robot_at(const Scenario& scenario, const uint xpos, const uint ypos) const
{
    std::vector<PPDDL::FullObject> params = {xpos_objects[xpos], ypos_objects[ypos]};
    Predicate<TypedAtom> required_predicate(TypedAtom(PPDDL_PREDICATE_ROBOT_AT, params), true);
    return scenario.get_state().has_predicate(required_predicate);
}

bool CrossingTaskVisualizer::obstacle_at(const Scenario& scenario, const uint xpos, const uint ypos) const
{
    std::vector<PPDDL::FullObject> params = {xpos_objects[xpos], ypos_objects[ypos]};
    Predicate<TypedAtom> required_predicate(TypedAtom(PPDDL_PREDICATE_OBSTACLE_AT, params), true);
    return scenario.get_state().has_predicate(required_predicate);
}



/*******************************/

#define PPDDL_TYPE_LOCATION "location"

#define PPDDL_PREDICATE_VEHICLE_AT "vehicleAt"
#define PPDDL_PREDICATE_ROAD "road"
#define PPDDL_PREDICATE_ROAD2 "ROAD"
#define PPDDL_PREDICATE_SPARE_IN "spareIn"
#define PPDDL_PREDICATE_NOT_FLATTIRE "notFlattire"
#define PPDDL_PREDICATE_HASSPARE "hasspare"


TriangleTaskVisualizer::TriangleTaskVisualizer() {
}

void TriangleTaskVisualizer::draw_current_state(const Scenario& scenario) const
{
    int number_xpos = 1;
    while (position_exists(scenario, number_xpos + 1, 1)) { number_xpos++; }
    int number_ypos = 1;
    while (position_exists(scenario, 1, number_ypos + 1)) { number_ypos++; }
    assert(number_xpos == number_ypos);
    
    for (int ypos_idx = number_xpos; ypos_idx > 0; --ypos_idx) {
        std::cout << " ";
        for (int xpos_idx = 1; xpos_idx <= number_xpos; ++xpos_idx) {
            if (position_exists(scenario, xpos_idx, ypos_idx)) {
                if (robot_at(scenario, xpos_idx, ypos_idx)) {
                    if (has_spare(scenario)) {
                        if (!not_flattire(scenario)) {
                            std::cout << START_COLOR_RED << "杏" << END_COLOR;
                        }
                        else {
                            std::cout << "杏";
                        }
                    }
                    else if (!not_flattire(scenario)) {
                        if (tire_at(scenario, xpos_idx, ypos_idx)) {
                            std::cout << START_COLOR_RED << "困" << END_COLOR;
                        }
                        else {
                            std::cout << START_COLOR_RED << "木" << END_COLOR;
                        }
                    }
                    else {
                        if (tire_at(scenario, xpos_idx, ypos_idx)) {
                            std::cout << "困";
                        }
                        else {
                            std::cout << "木";
                        }
                    }
                }
                else if (tire_at(scenario, xpos_idx, ypos_idx)) {
                    std::cout << "▣";
                }
                else {
                    std::cout << "□";
                }
            }
            else {
                std::cout << " ";
            }
            std::cout << " ";
            
            if (road_between(scenario, xpos_idx, ypos_idx, xpos_idx+1, ypos_idx)) {
                std::cout << "→";
            }
            else if (road_between(scenario, xpos_idx+1, ypos_idx, xpos_idx, ypos_idx)) {
                std::cout << "←";
            }
            else {
                std::cout << " ";
            }
            std::cout << " ";
        }
        std::cout << std::endl;
        std::cout << " ";
        
        for (int xpos_idx = 1; xpos_idx <= number_xpos; ++xpos_idx) {
            if (road_between(scenario, xpos_idx, ypos_idx, xpos_idx, ypos_idx-1)) {
                std::cout << "↓";
            }
            else if (road_between(scenario, xpos_idx, ypos_idx-1, xpos_idx, ypos_idx)) {
                std::cout << "↑";
            }
            else {
                std::cout << " ";
            }
            std::cout << " ";
            
            if (road_between(scenario, xpos_idx, ypos_idx, xpos_idx+1, ypos_idx-1)) {
                std::cout << "↘";
            }
            else if (road_between(scenario, xpos_idx+1, ypos_idx-1, xpos_idx, ypos_idx)) {
                std::cout << "↖";
            }
            else if (road_between(scenario, xpos_idx, ypos_idx-1, xpos_idx+1, ypos_idx)) {
                std::cout << "↗";
            }
            else if (road_between(scenario, xpos_idx+1, ypos_idx, xpos_idx, ypos_idx-1)) {
                std::cout << "↙";
            }
            else {
                std::cout << " ";
            }
            std::cout << " ";
        }
        std::cout << std::endl;
    }
}

PPDDL::FullObject TriangleTaskVisualizer::get_position_object(const uint xpos, const uint ypos) const
{
    return PPDDL::FullObject("l" + boost::lexical_cast<std::string>(ypos) + boost::lexical_cast<std::string>(xpos), PPDDL_TYPE_LOCATION);
}

bool TriangleTaskVisualizer::position_exists(const Scenario& scenario, const uint xpos, const uint ypos) const
{
    PPDDL::FullObject position = get_position_object(xpos, ypos);
    std::vector<PPDDL::FullObject::Name> existing_locations = scenario.get_state().ppddl_object_manager.get_type_objects(PPDDL_TYPE_LOCATION);
    std::vector<PPDDL::FullObject::Name>::const_iterator location_it = std::find (existing_locations.begin(), existing_locations.end(), position.name);
    return (location_it != existing_locations.end());
}

bool TriangleTaskVisualizer::road_between(const Scenario& scenario, const uint xpos1, const uint ypos1, const uint xpos2, const uint ypos2) const
{
    std::vector<PPDDL::FullObject> params = {get_position_object(xpos1, ypos1), get_position_object(xpos2, ypos2)};
    Predicate<TypedAtom> required_predicate(TypedAtom(PPDDL_PREDICATE_ROAD, params), true);
    Predicate<TypedAtom> required_predicate2(TypedAtom(PPDDL_PREDICATE_ROAD2, params), true);
    return scenario.get_state().has_predicate(required_predicate) || scenario.get_state().has_predicate(required_predicate2);
}

bool TriangleTaskVisualizer::robot_at(const Scenario& scenario, const uint xpos, const uint ypos) const
{
    std::vector<PPDDL::FullObject> params = {get_position_object(xpos, ypos)};
    Predicate<TypedAtom> required_predicate(TypedAtom(PPDDL_PREDICATE_VEHICLE_AT, params), true);
    return scenario.get_state().has_predicate(required_predicate);
}

bool TriangleTaskVisualizer::tire_at(const Scenario& scenario, const uint xpos, const uint ypos) const
{
    std::vector<PPDDL::FullObject> params = {get_position_object(xpos, ypos)};
    Predicate<TypedAtom> required_predicate(TypedAtom(PPDDL_PREDICATE_SPARE_IN, params), true);
    return scenario.get_state().has_predicate(required_predicate);
}

bool TriangleTaskVisualizer::has_spare(const Scenario& scenario) const
{
    std::vector<PPDDL::FullObject> params;
    Predicate<TypedAtom> required_predicate(TypedAtom(PPDDL_PREDICATE_HASSPARE, params), true);
    return scenario.get_state().has_predicate(required_predicate);
}

bool TriangleTaskVisualizer::not_flattire(const Scenario& scenario) const
{
    std::vector<PPDDL::FullObject> params;
    Predicate<TypedAtom> required_predicate(TypedAtom(PPDDL_PREDICATE_NOT_FLATTIRE, params), true);
    return scenario.get_state().has_predicate(required_predicate);
}


#endif

/*******************************/

#define PPDDL_TYPE_POSITION "position"

#define PPDDL_PREDICATE_PLATE "plate"
#define PPDDL_PREDICATE_CUP "cup"
#define PPDDL_PREDICATE_FORK "fork"
#define PPDDL_PREDICATE_PICK_UP_POSITION "stuffPickUp"
#define PPDDL_PREDICATE_STUFF_ARRIVING "stuffArriving"
#define PPDDL_PREDICATE_STABLE "stable"


TablewareTaskVisualizer::TablewareTaskVisualizer() {
}

void TablewareTaskVisualizer::draw_current_state(const Scenario& scenario) const
{
    int number_pos = 0;
    while (position_exists(scenario, number_pos + 1)) { number_pos++; }
    
    for (int pos_idx = 0; pos_idx <= number_pos; ++pos_idx) {
        if (unstable_at(scenario, pos_idx)) { std::cout << START_COLOR_YELLOW; }
        if (fork_at(scenario, pos_idx)) {
            std::cout << "-";
        }
        else {
            std::cout << " ";
        }
        if (unstable_at(scenario, pos_idx)) { std::cout << END_COLOR; }
        std::cout << " ";
    }
    std::cout << std::endl;
    for (int pos_idx = 0; pos_idx <= number_pos; ++pos_idx) {
        if (unstable_at(scenario, pos_idx)) { std::cout << START_COLOR_YELLOW; }
        if (cup_at(scenario, pos_idx)) {
            std::cout << "v";
        }
        else {
            std::cout << " ";
        }
        if (unstable_at(scenario, pos_idx)) { std::cout << END_COLOR; }
        std::cout << " ";
    }
    std::cout << std::endl;
    for (int pos_idx = 0; pos_idx <= number_pos; ++pos_idx) {
        if (unstable_at(scenario, pos_idx)) { std::cout << START_COLOR_YELLOW; }
        if (plate_at(scenario, pos_idx)) {
            std::cout << "u";
        }
        else {
            std::cout << " ";
        }
        if (unstable_at(scenario, pos_idx)) { std::cout << END_COLOR; }
        std::cout << " ";
    }
    std::cout << std::endl;
    for (int pos_idx = 0; pos_idx <= number_pos; ++pos_idx) {
        if (stuff_arriving_at(scenario, pos_idx)) { std::cout << START_COLOR_RED; }
        if (pick_up_at(scenario, pos_idx)) { std::cout << START_COLOR_GREEN; }
        std::cout << pos_idx << " ";
        if (pick_up_at(scenario, pos_idx)) { std::cout << END_COLOR; }
        if (stuff_arriving_at(scenario, pos_idx)) { std::cout << END_COLOR; }
    }
    std::cout << std::endl;
}

PPDDL::FullObject TablewareTaskVisualizer::get_position_object(const uint pos) const
{
    return PPDDL::FullObject("pos" + boost::lexical_cast<std::string>(pos), PPDDL_TYPE_POSITION);
}

#if __cplusplus > 199711L // C++11
bool TablewareTaskVisualizer::position_exists(const Scenario& scenario, const uint pos) const
{
    PPDDL::FullObject position = get_position_object(pos);
    std::vector<PPDDL::FullObject::Name> existing_locations = scenario.get_state().ppddl_object_manager.get_type_objects(PPDDL_TYPE_POSITION);
    std::vector<PPDDL::FullObject::Name>::const_iterator location_it = std::find (existing_locations.begin(), existing_locations.end(), position.name);
    return (location_it != existing_locations.end());
}

bool TablewareTaskVisualizer::plate_at(const Scenario& scenario, const uint pos) const
{
    std::vector<PPDDL::FullObject> params = {get_position_object(pos)};
    Predicate<TypedAtom> required_predicate(TypedAtom(PPDDL_PREDICATE_PLATE, params), true);
    return scenario.get_state().has_predicate(required_predicate);
}

bool TablewareTaskVisualizer::cup_at(const Scenario& scenario, const uint pos) const
{
    std::vector<PPDDL::FullObject> params = {get_position_object(pos)};
    Predicate<TypedAtom> required_predicate(TypedAtom(PPDDL_PREDICATE_CUP, params), true);
    return scenario.get_state().has_predicate(required_predicate);
}

bool TablewareTaskVisualizer::fork_at(const Scenario& scenario, const uint pos) const
{
    std::vector<PPDDL::FullObject> params = {get_position_object(pos)};
    Predicate<TypedAtom> required_predicate(TypedAtom(PPDDL_PREDICATE_FORK, params), true);
    return scenario.get_state().has_predicate(required_predicate);
}
bool TablewareTaskVisualizer::pick_up_at(const Scenario& scenario, const uint pos) const
{
    std::vector<PPDDL::FullObject> params = {get_position_object(pos)};
    Predicate<TypedAtom> required_predicate(TypedAtom(PPDDL_PREDICATE_PICK_UP_POSITION, params), true);
    return scenario.get_state().has_predicate(required_predicate);
}
bool TablewareTaskVisualizer::stuff_arriving_at(const Scenario& scenario, const uint pos) const
{
    std::vector<PPDDL::FullObject> params = {get_position_object(pos)};
    Predicate<TypedAtom> required_predicate(TypedAtom(PPDDL_PREDICATE_STUFF_ARRIVING, params), true);
    return scenario.get_state().has_predicate(required_predicate);
}
bool TablewareTaskVisualizer::unstable_at(const Scenario& scenario, const uint pos) const
{
    std::vector<PPDDL::FullObject> params = {get_position_object(pos)};
    Predicate<TypedAtom> required_predicate(TypedAtom(PPDDL_PREDICATE_STABLE, params), true);
    return !scenario.get_state().has_predicate(required_predicate);
}
#else
bool TablewareTaskVisualizer::position_exists(const Scenario& scenario, const uint pos) const
{
    PPDDL::FullObject position = get_position_object(pos);
    std::vector<PPDDL::FullObject::Name> existing_locations = scenario.get_state().ppddl_object_manager.get_type_objects(PPDDL_TYPE_POSITION);
    std::vector<PPDDL::FullObject::Name>::const_iterator location_it = std::find (existing_locations.begin(), existing_locations.end(), position.name);
    return (location_it != existing_locations.end());
}

bool TablewareTaskVisualizer::plate_at(const Scenario& scenario, const uint pos) const
{
    PPDDL::FullObject position = get_position_object(pos);
    std::vector<PPDDL::FullObject> params;
    params.push_back(position);
    Predicate<TypedAtom> required_predicate(TypedAtom(PPDDL_PREDICATE_PLATE, params), true);
    return scenario.get_state().has_predicate(required_predicate);
}

bool TablewareTaskVisualizer::cup_at(const Scenario& scenario, const uint pos) const
{
    PPDDL::FullObject position = get_position_object(pos);
    std::vector<PPDDL::FullObject> params;
    params.push_back(position);
    Predicate<TypedAtom> required_predicate(TypedAtom(PPDDL_PREDICATE_CUP, params), true);
    return scenario.get_state().has_predicate(required_predicate);
}

bool TablewareTaskVisualizer::fork_at(const Scenario& scenario, const uint pos) const
{
    PPDDL::FullObject position = get_position_object(pos);
    std::vector<PPDDL::FullObject> params;
    params.push_back(position);
    Predicate<TypedAtom> required_predicate(TypedAtom(PPDDL_PREDICATE_FORK, params), true);
    return scenario.get_state().has_predicate(required_predicate);
}
bool TablewareTaskVisualizer::pick_up_at(const Scenario& scenario, const uint pos) const
{
    PPDDL::FullObject position = get_position_object(pos);
    std::vector<PPDDL::FullObject> params;
    params.push_back(position);
    Predicate<TypedAtom> required_predicate(TypedAtom(PPDDL_PREDICATE_PICK_UP_POSITION, params), true);
    return scenario.get_state().has_predicate(required_predicate);
}
bool TablewareTaskVisualizer::stuff_arriving_at(const Scenario& scenario, const uint pos) const
{
    PPDDL::FullObject position = get_position_object(pos);
    std::vector<PPDDL::FullObject> params;
    params.push_back(position);
    Predicate<TypedAtom> required_predicate(TypedAtom(PPDDL_PREDICATE_STUFF_ARRIVING, params), true);
    return scenario.get_state().has_predicate(required_predicate);
}
bool TablewareTaskVisualizer::unstable_at(const Scenario& scenario, const uint pos) const
{
    PPDDL::FullObject position = get_position_object(pos);
    std::vector<PPDDL::FullObject> params;
    params.push_back(position);
    Predicate<TypedAtom> required_predicate(TypedAtom(PPDDL_PREDICATE_STABLE, params), false);
    return scenario.get_state().has_predicate(required_predicate);
}
#endif
