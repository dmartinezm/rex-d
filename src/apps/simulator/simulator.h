/*
 * PPDDL simulator.
 * Copyright (C) 2016  David Martínez (dmartinez@iri.upc.edu)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef INTELLACT_SIMULATOR_H
#define INTELLACT_SIMULATOR_H

#include <symbolic/rules.h>
#include <symbolic/scenario.h>
#include <rexd/config_reader.h>
#include <apps/simulator/task_visualizers.h>


/**
 * \class Simulator
 * \brief A basic simulator based on PPDDL rules.
 * 
 * Simulator for relational domains.
 */
class Simulator
{
public:
    Simulator(const bool save_changes);
    
    void main_loop();

protected:
    ConfigReader config_reader;
    ScenarioPtr scenario_ptr;
    
    int num_resets; // needed only as info about completion
    float last_reward;
    
    void init();
    
    std::string get_objects() const;
    std::string get_state() const;
    
private:
    TaskVisualizer::Ptr task_visualizer_ptr;
    bool save_changes;
    
    void reset(const std::string& problem_path);
    
    void reset_parser(const std::string& input_command);
    bool parse_command(const std::string& input_command);
    
    // Virtual methods to interact with Client
    virtual std::string get_command() =0;
    virtual bool is_running() const =0;
    virtual void send_state() =0;
    virtual void send_action_result(const std::string& result) const =0;
    
    std::string apply_action(const TypedAtom& action, const bool teacher, const bool forced_outcome);
};

#endif
