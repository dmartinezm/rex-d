#include "tcp_simulator.h"

TcpSimulator::TcpSimulator(const std::string& host):
    Simulator(true)
{
    if (!config_reader.get_variable<bool>("simulator_use_std_cin")) {
        uint port_vr = config_reader.get_variable<uint>("simulator_port");
        if (port_vr == 0) { // auto selection
            port_vr = utils::get_socket_from_current_dir_hash();
        }
        std::string port = boost::lexical_cast<std::string>(port_vr);

        std::cout << "Connecting to host " << host << "... ";
        tcp_client.connect(host, port);
        if (!tcp_client)
        {
            std::cerr << "Unable to connect to host " << host << " on port " << port << std::endl;
            exit(-1);
        }
        client_istream = &tcp_client;
        client_ostream = &tcp_client;
        std::cout << "connected!" << std::endl;
    }
    else {
        client_ostream = &std::cout;
        client_istream = &std::cin;
    }
    
    init();
}

std::string TcpSimulator::get_command() {
    std::string input_command;
    std::getline(*client_istream, input_command);
    return input_command;
}

bool TcpSimulator::is_running() const
{
    return *client_istream;
}

void TcpSimulator::send_state()
{
    *client_ostream << get_state() << std::endl << get_objects() << std::endl;
    *client_ostream << last_reward << std::endl;
}

void TcpSimulator::send_action_result(const std::string& result) const
{
    *client_ostream << "action_finished_" << result << std::endl << std::endl;
}


int main(int argc, char** argv) {
    if (argc >= 3)
    {
        std::cerr << "Usage: simulator [host]" << std::endl;
        return 1;
    }

    if (argc >= 2)
    {
        if ((std::string(argv[1]) == "-h") || (std::string(argv[1]) == "--help")) {
            std::cerr << "Usage: simulator [host]" << std::endl;
            return 1;
        }
    }

    std::string host;
    if (argc >= 2)
        host = argv[1];
    else
        host = "127.0.0.1";

    TcpSimulator simulator(host);

    simulator.main_loop();
    std::cout << "Disconnected." << std::endl;
    return 0;
}
