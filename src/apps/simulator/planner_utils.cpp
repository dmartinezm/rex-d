#include "planner_utils.h"

PlannerUtils::PlannerUtils():
    Simulator(false),
    finished(false),
    limit_one_run(false),
    verbose_planning(1)
{
    init();
}

std::string PlannerUtils::get_command() {
    if (limit_one_run) {
        finished = true;
    }
    if (scenario_ptr->is_in_goal()) {
        finished = true;
    }
    
    TypedAtomOpt action_opt;
    if (auto_teacher_ptr) {
        std::vector<std::string> actions_requiring_excuse; // unused
        std::vector<TypedAtom> action_list; // empty -> choose every action as unknown
        action_opt = auto_teacher_ptr->get_action_simple(*scenario_ptr, verbose_planning);
    }
    else { // interactive
        action_opt = read_action();
        // check that action exists! (check for typos)
        if (action_opt) {
            TypedAtom symbolic_action = *action_opt;
            symbolic_action.make_symbolic();
            if (std::find(scenario_ptr->get_action_list().begin(), scenario_ptr->get_action_list().end(), symbolic_action) == scenario_ptr->get_action_list().end() ) {
                LOG(WARNING) << "No action \"" << symbolic_action << "\" was found";
            }
        }
    }
    
    if (action_opt && (action_opt->name != "dead")) {
        NonTypedAtom non_typed_action(action_opt->name, action_opt->get_param_names());
        return boost::lexical_cast<std::string>(non_typed_action);
    }
    else {
        return "finish_simulator";
    }
}

TypedAtomOpt PlannerUtils::read_action() const
{
    utils::send_notification_msg("REX-D", "Requiring action command! ");
    std::cout << "State is " << get_state() << ". Write action to execute (h for help): " << std::endl;
    
    std::string action_string;
    while ((action_string = utils::read_string_from_stream(std::cin)) == "h") {
        std::cout << "Possible actions are: " << scenario_ptr->domain.get_actions() << std::endl;
        std::cout << "Write action to execute (h for help): " << std::endl;
    }
    
    if (action_string.empty()) {
        return TypedAtomOpt();
    }
    else {
        NonTypedAtom non_typed_action(action_string);
        return TypedAtomOpt(scenario_ptr->get_state().ppddl_object_manager.get_typed_atom(non_typed_action));
    }
}

bool PlannerUtils::is_running() const
{
    return !finished;
}

void PlannerUtils::send_state()
{
}

void PlannerUtils::send_action_result(const std::string& result) const
{
}

void PlannerUtils::run_once(const int verbose)
{
    limit_one_run = true;
    verbose_planning = verbose;
    auto_teacher_ptr = AutoTeacherPtr(new AutoTeacher(config_reader));
    main_loop();
}

void PlannerUtils::run_until_goal()
{
    auto_teacher_ptr = AutoTeacherPtr(new AutoTeacher(config_reader));
    main_loop();
}

void PlannerUtils::run_interactive()
{
    main_loop();
}


void print_help()
{
    std::cout << "Usage: rexd_utils [option] [args]" << std::endl
              << "Options:" << std::endl
              << "\tplan \t\t\t- plans using current folder files" << std::endl
              << "\tplan_n \t\t\t- plans until goal is reached using current folder files" << std::endl
              << "\tinteractive \t\t- interactively solve the problem (no planning)" << std::endl;
}

int main(int argc, char** argv) {
    PlannerUtils planner_utils;
    
    if (argc < 2) {
        print_help();
    }
    else {
        std::string input_arg(argv[1]);
        int verbose = 1;
        if (argc >= 3) {
            verbose = boost::lexical_cast<int>(argv[2]);
        }
        if (input_arg == "plan") {
            planner_utils.run_once(verbose);
        }
        else if (input_arg == "plan_n") {
            planner_utils.run_until_goal();
        }
        else if (input_arg == "interactive") {
            planner_utils.run_interactive();
        }
        else {
            print_help();
        }
    }
    
    return 0;
}
