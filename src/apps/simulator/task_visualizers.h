/*
 * PPDDL tasks visualizers.
 * Copyright (C) 2016  David Martínez (dmartinez@iri.upc.edu)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef REXD_TASK_VISUALIZER_H
#define REXD_TASK_VISUALIZER_H

#include <symbolic/scenario.h>

/**
 * \class TaskVisualizer
 * \brief PPDDL task visualizer
 * 
 * PPDDL task visualizer that draws the state in a terminal.
 * Current tasks supported:
 * - IPPC Triangle Tireworld.
 * - IPPC Crossing Traffic.
 * - IPPC Elevators.
 * - Custom Tableware cleaning.
 */
class TaskVisualizer
{
public:
    typedef boost::shared_ptr<TaskVisualizer> Ptr;
    
    /** \brief Draws the current state of the task in the terminal */
    virtual void draw_current_state(const Scenario& scenario) const { std::cout << "New state is " << scenario.get_state() << std::endl; };
};
#if __cplusplus > 199711L // C++11
/**
 * \class ElevatorsTaskVisualizer
 * \brief IPPC elevators domain visualizer
 */
class ElevatorsTaskVisualizer : public TaskVisualizer
{
public:
    ElevatorsTaskVisualizer();
    virtual void draw_current_state(const Scenario& scenario) const;
    
private:
    std::vector<PPDDL::FullObject> elevator_objects;
    std::vector<PPDDL::FullObject> floor_objects;
    
    bool person_waiting_up_at_floor(const Scenario& scenario, const uint floor) const;
    bool person_waiting_down_at_floor(const Scenario& scenario, const uint floor) const;
    bool elevator_closed(const Scenario& scenario, const uint elevator) const;
    bool elevator_at_floor(const Scenario& scenario, const uint elevator, const uint floor) const;
    bool person_in_elevator_going_up(const Scenario& scenario, const uint elevator) const;
    bool person_in_elevator_going_down(const Scenario& scenario, const uint elevator) const;
    bool elevator_dir_up(const Scenario& scenario, const uint elevator) const;
};

/**
 * \class CrossingTaskVisualizer
 * \brief IPPC elevators domain visualizer
 */
class CrossingTaskVisualizer : public TaskVisualizer
{
public:
    CrossingTaskVisualizer();
    virtual void draw_current_state(const Scenario& scenario) const;
    
private:
    std::vector<PPDDL::FullObject> xpos_objects;
    std::vector<PPDDL::FullObject> ypos_objects;
    
    bool robot_at(const Scenario& scenario, const uint xpos, const uint ypos) const;
    bool obstacle_at(const Scenario& scenario, const uint xpos, const uint ypos) const;
};

/**
 * \class TriangleTaskVisualizer
 * \brief IPPC elevators domain visualizer
 */
class TriangleTaskVisualizer : public TaskVisualizer
{
public:
    TriangleTaskVisualizer();
    virtual void draw_current_state(const Scenario& scenario) const;
    
private:
    PPDDL::FullObject get_position_object(const uint xpos, const uint ypos) const;
    bool position_exists(const Scenario& scenario, const uint xpos, const uint ypos) const;
    bool road_between(const Scenario& scenario, const uint xpos1, const uint ypos1, const uint xpos2, const uint ypos2) const;
    bool robot_at(const Scenario& scenario, const uint xpos, const uint ypos) const;
    bool tire_at(const Scenario& scenario, const uint xpos, const uint ypos) const;
    bool has_spare(const Scenario& scenario) const;
    bool not_flattire(const Scenario& scenario) const;
};
#endif
/**
 * \class TablewareTaskVisualizer
 * \brief Tableware task visualizer
 */
class TablewareTaskVisualizer : public TaskVisualizer
{
public:
    TablewareTaskVisualizer();
    virtual void draw_current_state(const Scenario& scenario) const;
    
private:
    PPDDL::FullObject get_position_object(const uint pos) const;
    bool position_exists(const Scenario& scenario, const uint pos) const;
    bool plate_at(const Scenario& scenario, const uint pos) const;
    bool cup_at(const Scenario& scenario, const uint pos) const;
    bool fork_at(const Scenario& scenario, const uint pos) const;
    bool pick_up_at(const Scenario& scenario, const uint pos) const;
    bool stuff_arriving_at(const Scenario& scenario, const uint pos) const;
    bool unstable_at(const Scenario& scenario, const uint pos) const;
};


class TaskVisualizerFactory
{
public:
    static TaskVisualizer::Ptr get_task_visualizer(const ConfigReader& config_reader);
};

#endif
