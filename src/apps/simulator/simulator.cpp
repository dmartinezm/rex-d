#include "simulator.h"


/**
 * Constructor
 */
Simulator::Simulator(const bool save_changes):
    num_resets(0),
    last_reward(0),
    save_changes(save_changes)
{
    // load config file
    config_reader.init("rexd_config.cfg");
    utils::init_loggers("logger.conf");
    
    task_visualizer_ptr = TaskVisualizerFactory::get_task_visualizer(config_reader);

    srand((unsigned)time(NULL));
}

void Simulator::init()
{
    // load simulation files
    scenario_ptr = boost::make_shared<Scenario>(config_reader.get_path("ppddl_domain_file_ground_truth"), 
                                                config_reader.get_path("simulator_ppddl_current_problem_file"), 
                                                config_reader);
    std::cout << "\033[1;34m" << "Reset! (" << ++num_resets << ")" << "\033[0m" << std::endl;
    task_visualizer_ptr->draw_current_state(*scenario_ptr);
}

void Simulator::reset(const std::string& problem_path)
{
    last_reward = 0;
    scenario_ptr->reset_problem_file(problem_path);
    scenario_ptr->reset();
    if (save_changes) {
        scenario_ptr->write_PDDL_problem(config_reader.get_path("simulator_ppddl_current_problem_file"));
    }
    task_visualizer_ptr->draw_current_state(*scenario_ptr);
}

void Simulator::reset_parser(const std::string& input_command)
{
    std::cout << "\033[1;34m" << "Reset! (" << ++num_resets << ")" << "\033[0m" << std::endl;
    std::string reset_num = input_command.substr(input_command.find("reset")+5,1);
    if (!utils::is_number(reset_num)) reset_num = "";
    std::cout << reset_num;
    std::stringstream ss;
    ss << config_reader.get_path("simulator_ppddl_init_problem_file") << reset_num << ".pddl";
    reset(ss.str());
}


/**
 * Main infinite loop
 */
void Simulator::main_loop()
{
    bool continue_simulation = true;
    while (is_running() && continue_simulation) {
        std::string input_command = get_command();
        continue_simulation = parse_command(input_command);
    }

    std::cout << "Finished." << std::endl;
}

bool Simulator::parse_command(const std::string& input_command)
{
//     std::cout << " Input command is: " << input_command << std::endl;
    if (input_command.size() < 3) {
        std::cout << "Ignoring too short command" << std::endl;
    }
    else if (input_command.find("request_state")!=std::string::npos) {
        send_state();
    }
    else if (input_command.find("planning_started")!=std::string::npos) {
//         std::cout << "Planning started" << std::endl;
    }
    else if (input_command.find("planning_finished")!=std::string::npos) {
//         std::cout << "Planning finished" << std::endl;
    }
    else if (input_command.find("reset")!=std::string::npos) {
        reset_parser(input_command);
    }
    else if (input_command.find("finish_simulator")!=std::string::npos) {
        std::cout << "Finishing simulation..." << std::endl;
        return false;
    }
    else {
        std::string result;
        NonTypedAtom non_typed_action(input_command);
        TypedAtom typed_action = scenario_ptr->get_state().ppddl_object_manager.get_typed_atom(non_typed_action);
        // + triggers teacher action
        if (input_command.find("+")!=std::string::npos) {
//             std::cout << "Received action " << "\033[1;35m" <<  input_command << "\033[0m" << std::endl;
            if (config_reader.get_variable<bool>("teacher_can_fail")) {
                result = apply_action(typed_action, true, false);
            }
            else {
                result = apply_action(typed_action, true, true);
            }
        }
        else {
//             std::cout << "Received action " << input_command << std::endl;
            result = apply_action(typed_action, false, false);
        }
        send_action_result(result);
    }
    return true;
}


/**
 * Get state
 */
std::string Simulator::get_state() const
{
    return boost::lexical_cast<std::string>(scenario_ptr->get_state());
}

std::string Simulator::get_objects() const
{
    std::stringstream ss;
    ss << "(:objects ";
    BOOST_FOREACH(const PPDDLObjectManager<PPDDL::FullObject>::TypeObjectsMap::value_type& type_objects, scenario_ptr->get_state().ppddl_object_manager.get_ppddl_objects_per_type()) {
        BOOST_FOREACH(const std::string& object, type_objects.second) {
            ss << object << " ";
        }
        ss << "- " << type_objects.first << " ";
    }
    ss << ")";
    
    return ss.str();
}

/**
 * Add action
 */
std::string Simulator::apply_action(const TypedAtom& action, const bool teacher, const bool forced_outcome)
{
    bool success, applied;
    float rule_reward = 0;
    applied = scenario_ptr->transition_next_state(action, success, rule_reward, forced_outcome);
    last_reward = rule_reward;
    if (save_changes) {
        scenario_ptr->write_PDDL_problem(config_reader.get_path("simulator_ppddl_current_problem_file"));
    }
    
    std::string action_string = boost::lexical_cast<std::string>(action);
    if (teacher) {
        action_string = START_COLOR_PURPLE + action_string;
    }
    
    std::string action_result;
    std::stringstream scenario_status;
    scenario_status << " [ " << last_reward << " | " << scenario_ptr->get_reward() 
                    << " (" << scenario_ptr->get_planning_horizon() << "/" << config_reader.get_variable<uint>("planning_horizon") << ")" << " ]";
    if (applied) {
        if (success) {
            std::cout << START_COLOR_GREEN << "Action " << action_string << " was applied successfully" 
                      << scenario_status.str()
                      << END_COLOR << std::endl;
            action_result = "success";
        }
        else {
            std::cout << START_COLOR_YELLOW << "Action " << action_string << " was applied NOT successfully" 
                      << scenario_status.str()
                      << END_COLOR << std::endl;
            action_result = "applied_not_success";
        }
    }
    else {
        std::cout << START_COLOR_RED << "Action " << action_string << " couldn't be applied" 
                  << scenario_status.str()
                  << END_COLOR << std::endl;
        action_result = "failed";
    }
    task_visualizer_ptr->draw_current_state(*scenario_ptr);
    scenario_ptr->decrease_planning_horizon();
    return action_result;
}


