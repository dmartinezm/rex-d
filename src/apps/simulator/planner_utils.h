/*
 * Planner utils using a simulator.
 * Copyright (C) 2016  David Martínez (dmartinez@iri.upc.edu)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef REXD_PLANNER_UTILS_H
#define REXD_PLANNER_UTILS_H

#include <apps/simulator/simulator.h>
#include "teacher/auto_teacher.h"

/**
 * \class PlannerUtils
 * \brief Utils to execute the planner in REX-D scenarios.
 */
class PlannerUtils : public Simulator
{
public:
    PlannerUtils();
    void run_once(const int verbose);
    void run_until_goal();
    void run_interactive();
    
private:
    AutoTeacherPtr auto_teacher_ptr;
    bool finished;
    bool limit_one_run;
    int verbose_planning;
    
    virtual std::string get_command();
    virtual bool is_running() const;
    virtual void send_state();
    virtual void send_action_result(const std::string& result) const;
    
    TypedAtomOpt read_action() const;
};

#endif
