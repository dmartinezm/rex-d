/*
 * Planner utils
 * Copyright (C) 2015  David Martínez (dmartinez@iri.upc.edu)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef INTELLACT_TCP_SIMULATOR_H
#define INTELLACT_TCP_SIMULATOR_H

#include <apps/simulator/simulator.h>

#include <boost/asio.hpp>

using boost::asio::ip::tcp;

/**
 * \class TcpSimulator
 * \brief A basic simulator based on PPDDL rules that interacts with TCP connections.
 * 
 * Simulator for relational domains.
 */
class TcpSimulator : public Simulator
{
public:
    TcpSimulator(const std::string& host);
    
private:
    tcp::iostream tcp_client;
    std::istream* client_istream;
    std::ostream* client_ostream;
    
    virtual std::string get_command();
    virtual bool is_running() const;
    virtual void send_state();
    virtual void send_action_result(const std::string& result) const;
};

#endif
