/*
 * Standard REX-D application.
 * Copyright (C) 2016  David Martínez (dmartinez@iri.upc.edu)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef INTELLACT_INTELLACT_MAIN_H
#define INTELLACT_INTELLACT_MAIN_H

#include <iomanip>

#include <boost/asio.hpp>
#include <boost/timer.hpp>
//#ifdef BOOST_VERSION > 104700
// #include <boost/timer/timer.hpp>

#include <symbolic/symbols.h>
#include <symbolic/predicates.h>
#include <symbolic/transitions.h>
#include <symbolic/rules.h>

#include "rexd/rl_main.h"

#include "info_display.h"

using boost::asio::ip::tcp;
// using boost::timer::cpu_timer;

#define SIMULATOR_COMMAND_FINISH "finish_simulator"
#define SIMULATOR_COMMAND_RESET "reset"


/**
 * \class IntellactServer
 * \brief Application for IntellAct project. Can connect to a simulator, ROS wrapper was used for real robot.
 * 
 * Application used for IntellAct project and experiments in the REX-D and V-MIN papers.
 * Can be used with the simulator application (simulator.h).
 * 
 * Note: Previous versions worked with Aachen simulator, but current one use new incompatible features.
 */
class IntellactServer : public ReinforcementLearner
{
public:
    IntellactServer();
    ~IntellactServer();

private:
    virtual State read_state();
    virtual TypedAtomOpt read_action();
    virtual float read_reward();

    virtual void execute_action(TypedAtom action, bool teacher = false);
    virtual TypedAtomOpt ask_confirmation_dangerous_action(const Scenario& scenario, const TypedAtomOpt& planning_res);

    virtual void show_info_request_state();
    virtual void show_info_is_goal();
    virtual void show_info_start_learning();
    virtual void show_info_end_learning(const Typed::RuleSet& removed_rules, const Typed::RuleSet& added_rules, std::string context);
    virtual void show_info_start_planning();
    virtual void show_info_end_planning(const TypedAtomOpt& planning_res, const std::string& excuse, const uint plan_length = 10000);
    virtual void show_info_teacher_request(const std::string& excuse, const std::vector<std::string>& actions_requiring_excuse);

    // Reset scenario
    virtual bool reset_scenario();
    // TODO most of this stuff should be move to the simulator
    std::string load_next_episode();
    std::string load_next_problem_auto();
    std::string load_next_problem_command();
    bool send_command(std::string command);
    void increase_vmin();

    // Streams
    void write_to_stream(std::ostream& socket, std::string msg, bool debug_info = false);

    // monitoring methods
    void monitoring(State state);
    void write_to_monitoring_screen(int status, std::string status_info = "", std::vector<std::string> additional_status_info = std::vector<std::string>());
    void write_html_to_file(std::string html_string);
    

    /**
     * variables
     */
    bool slow_execution;
    
    uint current_vmin_increase_num, current_vmin_increase_episode;

    boost::asio::io_service io_service;
    tcp::iostream vr_server;
    std::ostream* vr_ostream, *info_ostream;
    std::istream* vr_istream;

    InfoDisplay info_displayer;

    std::string reset_command;
    TypedAtom last_confirmed_dangerous_action; // used to avoid dead loops

    const static std::string REQUEST_STATE_MSG;
};

const std::string IntellactServer::REQUEST_STATE_MSG = "request_state";


#endif
