#include "apps/applications_helper.h"
#include "lfit/lfit_learner.h"
#include "rexd/pasula_learner.h"
#include <lfit/lfit_types.h>

#define LOGA(Level_, Logger_, ...)   \
    while (Log::Enabled(Level_)) {  \
      Log_Write(Level_, Logger_, __func__, __FILE__, __LINE__, __VA_ARGS__);  \
      break;  \
    }

void Log_Write(uint level,
               char const* logger,
               char const* function,
               char const* file,
               int line,
               std::string msg)
{
    std::cout << msg;
}


Typed::RuleSet learn(std::string action_string = "")
{
    struct timeval timer_start = utils::start_timer();
    LOG(INFO) << "Learning";
    ConfigReader config_reader = load_config_reader();
    Scenario scenario = load_scenario(config_reader);
    TransitionGroup<TypedAtom> transitions = load_transitions(config_reader, scenario);
    
    std::string learner_type = config_reader.get_variable<std::string>("learner");
    LearnerPtr learner;
    if (learner_type == LEARNER_PASULA) {
        learner = boost::make_shared<PasulaLearner>(PasulaLearner(config_reader));
    }
    else if (learner_type == LEARNER_LFIT) {
        learner = boost::make_shared<LFITLearner>(LFITLearner(config_reader));
    }
    
    Typed::RuleSet rules;
    if (transitions.size_set() > 0) {
        CINFO("planning") << START_COLOR_BLUE << "Learning " << action_string << END_COLOR;
        Typed::RuleSet removed_rules, added_rules;
        if (action_string != "") {
            TypedAtom action(action_string);
            rules = learner->learn_action_rules(rules, transitions, action, removed_rules, added_rules);
        }
        else {
            if (learner_type == "pasula") {
                rules = learner->learn_quiet(transitions, rules);
            }
            else {
                rules = learner->learn(transitions, rules);
            }
        }
        CINFO("planning") << "Learned " << rules.size() << " rules.";
        rules.write_rules_to_file(config_reader.get_path("nid_rules_path"));
        // write PPDDL rules
        Scenario scenario(Scenario(rules, 
                                   config_reader.get_path("simulator_ppddl_current_problem_file"), 
                                   config_reader));
        scenario.write_PDDL_domain("learned_rules.ppddl");
        
        LOG(INFO) << "Typed::Rules are:";
        LOG(INFO) << rules;
    }
    else {
        CINFO("planning") << "Skipping learning (no transitions)";
    }
    
    LOG(INFO) << "Time taken: " << utils::get_elapsed_time(timer_start);
    return rules;
}

Typed::RuleSet read_and_parse_lfit_rules(char* path) {
    struct timeval timer_start = utils::start_timer();
    LOG(INFO) << "Reading and parsing LFIT rules";
    ConfigReader config_reader = load_config_reader();
    Scenario scenario = load_scenario(config_reader);
    TransitionGroup<TypedAtom> transitions = load_transitions(config_reader, scenario);
    TransitionGroup<TypedAtom> normalized_transitions = transitions.get_normalized_transitions();
    
    ReaderLFIT reader_lfit(path);
    Typed::RuleSet rules;
    reader_lfit.read_ruleset(rules, 
                             normalized_transitions,
                             config_reader);
    
    LOG(INFO) << "Typed::Rules are:";
    LOG(INFO) << rules;
    LOG(INFO) << "Time taken: " << utils::get_elapsed_time(timer_start);
    
    // write PPDDL rules
    scenario.set_rules(rules);
//     RexdScenario rexd_scenario(scenario);
    scenario.write_PDDL_domain("learned_rules.ppddl");
    return rules;
}

void generalte_lfit_rules() {
    struct timeval timer_start = utils::start_timer();
    LOG(INFO) << "Learning";
    ConfigReader config_reader = load_config_reader();
    Scenario scenario = load_scenario(config_reader);
    TransitionGroup<TypedAtom> transitions = load_transitions(config_reader, scenario);
    TransitionGroup<TypedAtom> normalized_transitions = transitions.get_normalized_transitions();
    
    LFITLearner learner(config_reader);
    if ((config_reader.get_variable<bool>("learning")) && (transitions.size_set() > 0) ) {
        CINFO("planning") << START_COLOR_BLUE << "Learning" << END_COLOR;
        learner.generate_lfit_rules(normalized_transitions);
    }
    LOG(INFO) << "Time taken: " << utils::get_elapsed_time(timer_start);
}

void generate_domain_transitions(const uint number_transitions) {
    struct timeval timer_start = utils::start_timer();
    LOG(INFO) << "Generating transitions for using a learner afterwards";
    ConfigReader config_reader = load_config_reader();

    CINFO("planning") << "Using ground truth rules";
    Scenario scenario(load_scenario_ground_truth(config_reader));
    
    TransitionGroup<TypedAtom> resulting_transitions = generate_random_transitions(config_reader, scenario, number_transitions);
    
    resulting_transitions.write_pasula_transitions_to_file(config_reader.get_path("transitions_backup_path"));
    LOG(INFO) << "";
    LOG(INFO) << "Time taken: " << utils::get_elapsed_time(timer_start);
}

void evaluate_rules(const std::string& rules_file, const uint number_action_tests)
{
    int verbose = 0;
    LOG(INFO) << "Testing ruleset";
    ConfigReader config_reader = load_config_reader();
    Scenario scenario(load_scenario(config_reader, rules_file));
    Scenario scenario_ground_truth(load_scenario_ground_truth(config_reader));
    TransitionGroup<TypedAtom> generated_transitions = generate_random_transitions(config_reader, scenario_ground_truth, number_action_tests);
    
    // Get default false literals
    std::set<TypedAtom> default_false_literals_evaluated;
    Typed::RuleSet rules_evaluated;
    BOOST_FOREACH(const Typed::RulePtr& rule_ptr, scenario.get_rules()) {
        if (rule_ptr->name == ACTION_NAME_SET_LITERAL_DEFAULT) {
            default_false_literals_evaluated.insert(RuleUtils::get_literal_default_from_rule(*rule_ptr));
        }
        else {
            rules_evaluated.push_back(rule_ptr);
        }
    }
    std::set<TypedAtom> default_false_literals_ground_truth;
    Typed::RuleSet rules_ground_truth;
    BOOST_FOREACH(const Typed::RulePtr& rule_ptr, scenario_ground_truth.get_rules()) {
        if (rule_ptr->name == ACTION_NAME_SET_LITERAL_DEFAULT) {
            default_false_literals_ground_truth.insert(RuleUtils::get_literal_default_from_rule(*rule_ptr));
        }
        else {
            rules_ground_truth.push_back(rule_ptr);
        }
    }
    
    float total_difference = 0.0;
    for(TransitionSet<TypedAtom>::const_iterator transition_it = generated_transitions.begin_set(); transition_it != generated_transitions.end_set(); ++transition_it) {
        // obtain changes of ground truth and normal rules
        CLOG_IF(verbose >=1, INFO, "trivial") << "---------------";
        CLOG_IF(verbose >=3, INFO, "trivial") << *transition_it;
        CLOG_IF(verbose >=2, INFO, "trivial") << "Changes are " << transition_it->get_changes();
        
        Transition<TypedAtom> transition_evaluated(transition_it->get_prev_state(),
                                                   transition_it->get_action(),
                                                   transition_it->get_post_state(),
                                                   transition_it->get_reward(),
                                                   default_false_literals_evaluated);
        
        Transition<TypedAtom> transition_ground_truth(transition_it->get_prev_state(),
                                                      transition_it->get_action(),
                                                      transition_it->get_post_state(),
                                                      transition_it->get_reward(),
                                                      default_false_literals_ground_truth);
        
        Transitions::Coverage ground_truth_coverage = rules_ground_truth.covers_transition_effects(transition_ground_truth, scenario_ground_truth.get_state().ppddl_object_manager);
        CLOG_IF(verbose >=3, INFO, "trivial") << "Coverage ground truth: " << ground_truth_coverage;
        Transitions::Coverage coverage = rules_evaluated.covers_transition_effects(transition_evaluated, scenario_ground_truth.get_state().ppddl_object_manager);
        CLOG_IF(verbose >=3, INFO, "trivial") << "Coverage tested rules: " << coverage;
        CLOG_IF(verbose >=1, INFO, "trivial") << "The difference is (" << ground_truth_coverage.get_coverage(true) << " - " << coverage.get_coverage(true)
                << ") = " << (ground_truth_coverage.get_coverage(true) - coverage.get_coverage(true));
        total_difference += std::abs(ground_truth_coverage.get_coverage(true) - coverage.get_coverage(true));
    }
    LOG(INFO) << "";
    LOG(INFO) << "Differences are " << total_difference/generated_transitions.size_historial();
    LOG(INFO) << "Num tests are " << generated_transitions.size_historial();
}

#define ACTION_NAME_ALL "action_name_all"
void check_rules_score(const std::string& rules_file)
{
    LOG(INFO) << "Checking scores";
    ConfigReader config_reader = load_config_reader();
    Scenario scenario(load_scenario(config_reader, rules_file));
    TransitionGroup<TypedAtom> transitions = load_transitions(config_reader, scenario);
    LFITLearner learner(config_reader);
    learner.get_rules_score(transitions, scenario.get_rules(), 2);
}

void hoeffding_inequality(const uint number_samples, const float confidence_interval)
{
    LOG(INFO) << "hoeffding conf: " << utils::get_hoeffding_inequality_confidence(number_samples, confidence_interval);
}


void print_help()
{
    std::cout << "Usage: rexd_utils [option] [args]" << std::endl
              << "Options:" << std::endl
              << "\tlearn [action] \t- learns using current folder files" << std::endl
              << "\tlfit lfit_rules \t- reads and parses lfit rules" << std::endl
              << "\tscore rules_file [action]\t- gets score of all rules [or only action rules]" << std::endl
              << "\ttransitions <number_per_rule> \t- creates random transitions for learning" << std::endl
              << "\tgenerate_lfit \t- Generates LFIT propositional rules" << std::endl
              << "\tevaluate_rules rules_file number_per_rule \t- Compares rule set agains ground truth" << std::endl
              << "\thoeffding number_samples confidence_interval \t- Gets hoeffding inequality confidence" << std::endl;
}


int main(int argc, char** argv) {
    // generated random seed
    srand((unsigned)time(0));
    utils::init_loggers("logger.conf");

    if (argc < 2) {
        print_help();
    }
    else {
        std::string input_arg(argv[1]);
        if (input_arg == "learn") {
            if (argc == 2) {
                learn();
            }
            else if (argc > 2) {
                learn(argv[2]);
            }
            else {
                learn();
            }
        }
        else if (input_arg == "lfit") {
            if (argc == 3) {
                read_and_parse_lfit_rules(argv[2]);
            }
            else {
                print_help();
            }
        }
        else if (input_arg == "score") {
            if (argc == 3) {
                check_rules_score(argv[2]);
            }
            else {
                print_help();
            }
        }
        else if (input_arg == "transitions") {
            if (argc == 3) {
                generate_domain_transitions(boost::lexical_cast<uint>(argv[2]));
            }
            else {
                print_help();
            }
        }
        else if (input_arg == "generate_lfit") {
            if (argc == 2) {
                generalte_lfit_rules();
            }
            else {
                print_help();
            }
        }
        else if (input_arg == "evaluate_rules") {
            if (argc == 4) {
                evaluate_rules(argv[2], boost::lexical_cast<uint>(argv[3]));
            }
            else {
                print_help();
            }
        }
        else if (input_arg == "hoeffding") {
            if (argc == 4) {
                hoeffding_inequality(boost::lexical_cast<uint>(argv[2]), boost::lexical_cast<float>(argv[3]));
            }
            else {
                print_help();
            }
        }
        else {
            print_help();
        }
    }

    return 0;
}
