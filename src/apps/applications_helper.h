/*
 * Utils to be used by applications.
 * Copyright (C) 2015  David Martínez (dmartinez@iri.upc.edu)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef IRI_APPLICATIONS_HELPER_H
#define IRI_APPLICATIONS_HELPER_H 1

#include <boost/timer.hpp>

#include <symbolic/rules.h>
#include <symbolic/predicates.h>
#include "symbolic/scenario.h"

/** \brief Load a ConfigReader from the given path (current path as default) */
ConfigReader load_config_reader(std::string config_path = "./");

/** \brief Load a standard scenario using the config reader. */
Scenario load_scenario(const ConfigReader& config_reader, const std::string& rules_file = "");

/** \brief Load a scenario with ground truth rules using the config reader. */
Scenario load_scenario_ground_truth(const ConfigReader& config_reader);

/** \brief Generate a random state for the given scenario. */
State generate_random_state(const ConfigReader& config_reader, const Scenario& initial_scenario);

/** 
 * \brief Generate random transitions by taking a random state and executing a random action. 
 * Transitions are biased so that at least half of the actions have a probability>0 of changing something.
 */
TransitionGroup<TypedAtom> generate_random_transitions(const ConfigReader& config_reader, const Scenario& scenario, const uint number_transitions);

/** \brief Load transitions from file indicated in config reader. */
TransitionGroup<TypedAtom> load_transitions(const ConfigReader& config_reader, Scenario& scenario);

/** \brief Check if the given grounded rules have a probability > 0 of changing the state when applied. */
bool do_rules_have_a_chance_of_change_the_state(const Typed::RuleSet& grounded_rules, const State& state);

/** \brief Get a random grounding for the action. */
TypedAtom get_random_grounding(const TypedAtom& action, const PPDDLObjectManager<PPDDL::FullObject>& ppddl_object_manager);

#endif