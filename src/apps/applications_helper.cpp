#include "applications_helper.h"


ConfigReader load_config_reader(std::string config_path)
{
    ConfigReader config_reader;
    config_reader.init(config_path + "rexd_config.cfg", config_path);
    return config_reader;
}

Scenario load_scenario(const ConfigReader& config_reader, const std::string& rules_file)
{
    std::string real_rules_file;
    if (rules_file == "") {
        real_rules_file = config_reader.get_path("ppddl_domain_file");
    }
    else {
        real_rules_file = rules_file;
    }
    return Scenario(real_rules_file, config_reader.get_path("simulator_ppddl_current_problem_file"), config_reader);
}

Scenario load_scenario_ground_truth(const ConfigReader& config_reader)
{
    return load_scenario(config_reader, config_reader.get_path("ppddl_domain_file_ground_truth"));
}

TransitionGroup<TypedAtom> load_transitions(const ConfigReader& config_reader, Scenario& scenario)
{
    TransitionGroup<TypedAtom> transitions =  read_transitions_from_file(config_reader.get_path("transitions_backup_path"), 
                                                                         scenario.get_state().ppddl_object_manager, 
                                                                         scenario.get_state().get_constants());
    scenario.domain.index_transitions(transitions);
    return transitions;
}

bool do_rules_have_a_chance_of_change_the_state(const Typed::RuleSet& grounded_rules, const State& state)
{
    bool has_a_chance_of_change_the_state = false;
    BOOST_FOREACH(Typed::RulePtr rule_ptr, grounded_rules) {
        Typed::GroundedRule grounded_rule = *static_cast<const Typed::GroundedRule*>(&(*rule_ptr));
        if (grounded_rule.satisfiesPreconditions(state)) {
            has_a_chance_of_change_the_state = true;
        }
    }
    return has_a_chance_of_change_the_state;
}

TypedAtom get_random_grounding(const TypedAtom& action, const PPDDLObjectManager<PPDDL::FullObject>& ppddl_object_manager)
{
    TypedAtom grounded_action(action);
    BOOST_FOREACH(PPDDL::FullObject& param, grounded_action.params) {
        std::vector<std::string> possible_objects = ppddl_object_manager.get_type_objects(param.type);
        param.name = *utils::get_random_element<std::vector<std::string>::const_iterator>(possible_objects.begin(),
                                                                                        possible_objects.end());
    }
    return grounded_action;
}

State generate_random_state(const ConfigReader& config_reader, const Scenario& initial_scenario)
{
    PredicateGroup<TypedAtom> constants = initial_scenario.get_state().get_constants();
    PredicateGroup<TypedAtom> non_constants = initial_scenario.get_state().get_non_constants();
    Domain non_constant_domain(config_reader);
    non_constant_domain.index_predicates(non_constants);
    non_constant_domain.index_objects(initial_scenario.get_state().ppddl_object_manager.get_ppddl_objects());
    non_constant_domain.make_predicate_group_no_closed_world(non_constants, true);
    
    State full_non_constant_state(non_constants, initial_scenario.get_state().ppddl_object_manager);
    State full_new_state;
    full_new_state.ppddl_object_manager = initial_scenario.get_state().ppddl_object_manager;
    BOOST_FOREACH(const Predicate<TypedAtom>& predicate, full_non_constant_state) {
        if ( (config_reader.get_variable<std::string>("domain") == DOMAIN_CROSSING_TRAFFIC) && 
                (predicate.name == "robot-at") ) // negate all robot-at (only one can be true)
        {
            // hacks for crossing traffic domain, force only one robot-at predicate
            Predicate<TypedAtom> new_predicate(predicate, false);
            full_new_state.insert(new_predicate);
        }
        else if ( (config_reader.get_variable<std::string>("domain") == DOMAIN_ELEVATORS) && 
                (predicate.name == "elevatorAtFloor") ) // negate all elevator-at (only one per elevator can be true)
        {
            // hacks for crossing traffic domain, force only one robot-at predicate
            Predicate<TypedAtom> new_predicate(predicate, false);
            full_new_state.insert(new_predicate);
        }
        else if ( (config_reader.get_variable<std::string>("domain") == DOMAIN_TRIANGLE_TIREWORLD) && 
                (predicate.name == "vehicleAt") ) // negate all elevator-at (only one per elevator can be true)
        {
            // hacks for crossing traffic domain, force only one robot-at predicate
            Predicate<TypedAtom> new_predicate(predicate, false);
            full_new_state.insert(new_predicate);
        }
        else if (config_reader.get_variable<std::string>("domain") == DOMAIN_TABLEWARE) {
            if (predicate.name == "pickUpPosition") {
                if (rand() > ((RAND_MAX/15)*14)) {
                    full_new_state.insert(Predicate<TypedAtom>(predicate,true));
                }
                else {
                    full_new_state.insert(Predicate<TypedAtom>(predicate,false));
                }
            }
            else if (predicate.name == "stuffArriving") {
                if (predicate.params[0].name != "pos6") {
                    full_new_state.insert(Predicate<TypedAtom>(predicate,false));
                }
                else if (rand() > ((RAND_MAX/10)*9)) {
                    full_new_state.insert(Predicate<TypedAtom>(predicate,true));
                }
                else {
                    full_new_state.insert(Predicate<TypedAtom>(predicate,false));
                }
            }
            else if (rand() > ((RAND_MAX/4)*3)) { // Only 25% of positive predicate
                full_new_state.insert(Predicate<TypedAtom>(predicate,true));
            }
            else {
                full_new_state.insert(Predicate<TypedAtom>(predicate,false));
            }
        }
        else if (rand() > (RAND_MAX/2)) {
            full_new_state.insert(predicate);
        }
        else {
            full_new_state.insert(predicate.get_negated());
        }
    }
    
    if (config_reader.get_variable<std::string>("domain") == DOMAIN_CROSSING_TRAFFIC) {
        // hacks for crossing traffic domain, force only one robot-at predicate
        PredicateGroup<TypedAtom>::const_iterator singleton_predicate;
        while ( (singleton_predicate = utils::get_random_element<PredicateGroup<TypedAtom>::const_iterator>(full_new_state.begin(), full_new_state.end()))->name != "robot-at");
        full_new_state.update_predicate(singleton_predicate->get_negated());
    }
    else if (config_reader.get_variable<std::string>("domain") == DOMAIN_ELEVATORS) {
        // hacks for elevators domain, only one position per elevator
        PredicateGroup<TypedAtom>::const_iterator singleton_predicate;
        while ( (singleton_predicate = utils::get_random_element<PredicateGroup<TypedAtom>::const_iterator>(full_new_state.begin(), full_new_state.end()))->name != "elevatorAtFloor");
        full_new_state.update_predicate(singleton_predicate->get_negated());
    }
    else if (config_reader.get_variable<std::string>("domain") == DOMAIN_TRIANGLE_TIREWORLD) {
        // hacks for elevators domain, only one position per elevator
        PredicateGroup<TypedAtom>::const_iterator singleton_predicate;
        while ( (singleton_predicate = utils::get_random_element<PredicateGroup<TypedAtom>::const_iterator>(full_new_state.begin(), full_new_state.end()))->name != "vehicleAt");
        full_new_state.update_predicate(singleton_predicate->get_negated());
    }
    
    full_new_state.insert(constants.begin(), constants.end());
    full_new_state.set_constants(constants);
    return full_new_state;
        
}

TransitionGroup<TypedAtom> generate_random_transitions(const ConfigReader& config_reader, const Scenario& scenario, const uint number_transitions)
{
    TransitionGroup<TypedAtom> resulting_transitions(scenario.get_state().ppddl_object_manager);
    std::vector<TypedAtom> action_list = scenario.get_executable_action_list();
    uint n_total = action_list.size() * number_transitions * 2;
    uint n_action = 0;
    BOOST_FOREACH(TypedAtom action, action_list) {
        LOG(INFO) << "Generating transitions for action " << action;
        TransitionGroup<TypedAtom> transitions_change(scenario.get_state().ppddl_object_manager);
        TransitionGroup<TypedAtom> transitions_no_change(scenario.get_state().ppddl_object_manager);
        
        while ( (transitions_change.size_historial() < number_transitions) || 
                ( (transitions_change.size_historial() + transitions_no_change.size_historial()) < (2*number_transitions) ) ) 
        {
            Scenario new_scenario(scenario);
            State full_new_state = generate_random_state(config_reader, scenario);
            new_scenario.set_state(full_new_state);
            TypedAtom grounded_action = get_random_grounding(action, scenario.get_state().ppddl_object_manager);
            
            // check if the rule has a chance of changing something (i.e. transitions are satisfied)
            Typed::RuleSet grounded_action_rules = scenario.get_rules().get_grounded_action_rules(grounded_action, full_new_state);
            bool has_a_chance_of_change_the_state = do_rules_have_a_chance_of_change_the_state(grounded_action_rules, full_new_state);
            
            new_scenario.transition_next_state_simple(grounded_action);
            
            full_new_state.remove_special_predicates();
            new_scenario.get_modificable_state().remove_special_predicates();
            Transition<TypedAtom> new_transition(full_new_state, grounded_action, new_scenario.get_state(), 0.0, true);
            if (grounded_action.name == ACTION_NAME_NOOP) {
                transitions_change.add_transition(new_transition);
            }
            else if (has_a_chance_of_change_the_state) {
                    transitions_change.add_transition(new_transition);
            }
            else {
                if (transitions_no_change.size_historial()  < number_transitions) {
                    transitions_no_change.add_transition(new_transition);
                }
            }
            LOG_EVERY_N(100,INFO) << "Completed " << transitions_change.size_historial() << "|" << transitions_no_change.size_historial() << ".";
            utils::print_progress(n_action*(number_transitions*2) + transitions_change.size_historial() + transitions_no_change.size_historial(), n_total);
        }
        resulting_transitions.add_transitions(transitions_change);
        resulting_transitions.add_transitions(transitions_no_change);
        ++n_action;
    }
    
    return resulting_transitions;
}


