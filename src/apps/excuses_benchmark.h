/*
 * Tools to benchmark excuse generation
 * Copyright (C) 2015  David Martínez (dmartinez@iri.upc.edu)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef IRI_TEST_HELPERS_H
#define IRI_TEST_HELPERS_H 1


#include <iostream>
#include <algorithm>
#include <boost/iostreams/device/mapped_file.hpp>

#include <symbolic/scenario.h>
#include <teacher/failure_analyzer.h>

Scenario load_scenario(const ConfigReader& config_reader);

std::vector<std::string> get_actions_requiring_excuse(const Scenario& scenario, FailureAnalyzer& failure_analyzer);

class ListPredicateGroup : public std::vector<PredicateGroup<TypedAtom> > {
public:
    ListPredicateGroup(std::string predicates_file_str);
    
    static ListPredicateGroup read_from_file(const std::string& file_path);
    
    bool has_predicate_group(const PredicateGroup<TypedAtom>& predicates) const;
};

typedef std::pair<ListPredicateGroup, std::string> ExcuseActionPair;
typedef std::map<std::string, ExcuseActionPair> FolderExcusePairs;


struct ExcuseError {
    ExcuseError(const PlanningFailureOpt& obtained_excuse_, const ListPredicateGroup& actual_excuses_, const boost::filesystem::path& test_id_):
        obtained_excuse_opt(obtained_excuse_),
        actual_excuses(actual_excuses_),
        test_id(test_id_)
    { }
    
    PlanningFailureOpt obtained_excuse_opt;
    ListPredicateGroup actual_excuses;
    boost::filesystem::path test_id;
};


class ExcuseBenchmarkResults {
public:
    ExcuseBenchmarkResults():
        successful_n(0),
        partial_n(0),
        failed_n(0)
    {}
    
    uint successful_n;
    uint partial_n;
    uint failed_n;
    
    void add_possible_error(const ExcuseError& possible_error) { possible_errors.push_back(possible_error); };
    void confirm_errors(const std::string& config_file);
    void print_results(const boost::filesystem::path& results_folder, const boost::filesystem::path& config_name);
    
private:
    std::vector< ExcuseError > possible_errors;
//     std::vector< ExcuseError > partial_errors;
};


class ExcuseBenchmark {
public:
    ExcuseBenchmark(const std::string& domain_, const std::string& problem_, const bool ask_for_error_confirmation_, const std::string instance_ = ""):
        domain(domain_),
        problem(problem_),
        current_total_instances(0),
        ask_for_error_confirmation(ask_for_error_confirmation_)
    { 
        if (instance_ != "") { instance = instance_; } 
    }
    
    void test_excuses_all_configs();
    
private:
    std::string domain;
    std::string problem;
    boost::optional<std::string> instance;
    
    uint number_configs;
    uint current_config;
    uint number_vars_per_configs;
    uint current_vars_per_configs;
    uint number_instances_per_var;
    uint current_instances_per_var;
    uint current_total_instances;
    
    ExcuseBenchmarkResults results;
    bool ask_for_error_confirmation;
    
    void test_excuses(const std::string& config_file);
    void test_excuses(const boost::filesystem::path& folder, 
                      const std::string& config_file, 
                      const ListPredicateGroup& possible_expected_excuses, 
                      const ListPredicateGroup& possible_partial_excuses);
    
    void print_progress(const uint current_progress, const uint total);
};




void generate_excuses(const std::string& domain, const std::string& problem, const bool confirm_errors, const std::string instance = "");

#endif
