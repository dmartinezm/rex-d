/*
 * Class to display info to a html file.
 * Copyright (C) 2016  David Martínez (dmartinez@iri.upc.edu)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef INTELLACT_INFO_DISPLAY_H
#define INTELLACT_INFO_DISPLAY_H

#include <symbolic/symbols.h>
#include <symbolic/predicates.h>
#include <symbolic/rules.h>


/**
 * \class InfoDisplay
 * 
 * \brief This class outputs information to an HTML file. 
 * The generated HTML file can be shown in any html browser with basic css support.
 * 
 * Use cases: Make HTML visualizators in rqt (ROS), visualize status in any web browser...
 */
class InfoDisplay
{
public:
    enum DMF_STATUS {
        REQ_STATE,
        PLANNING,
        LEARNING,
        REQ_TEACHER,
        EXEC_ACTION,
        WAITING_USER_ACTION,
        IS_GOAL
    };

    InfoDisplay();

    void set_green_light();
    void generate_status_html_info(int status, std::string status_info, std::vector<std::string> additional_status_info = std::vector<std::string>());
    void generate_additional_html_info(int number_rules);
    void generate_learning_html_info(bool still_learning, std::string action_name = "", int number_rules = 0, Typed::RuleSet removed_rules = Typed::RuleSet(), Typed::RuleSet added_rules = Typed::RuleSet());
    void generate_planning_html_info(bool still_planning, const TypedAtomOpt& planning_res = TypedAtomOpt(), std::string excuse = "", uint plan_length = 0);

    std::string get_html_info();

    // variables
    std::string traffic_light_color;


private:
    // variables
    uint last_plan_length;
    std::string last_status_html, last_planning_result_html, last_learning_result_html, last_additional_html;

    const static std::string RED_LIGHT;
    const static std::string YELLOW_LIGHT;
    const static std::string GREEN_LIGHT;

    const static std::string HTML_PANEL;
    const static std::string HTML_BIG_PANEL;
    const static std::string HTML_HIGHLIGHTED_PANEL;
    const static std::string HTML_PANEL_END;
    const static std::string HTML_TITLE;
    const static std::string HTML_TITLE_END;
    const static std::string HTML_LIST_BEGIN;
    const static std::string HTML_LIST_END;
    const static std::string HTML_ITEM_BEGIN;
    const static std::string HTML_IMPORTANT_ITEM_BEGIN;
    const static std::string HTML_HIGHLIGHTED_ITEM_BEGIN;
    const static std::string HTML_SUBITEM_BEGIN;
    const static std::string HTML_ITEM_END;
    const static std::string HTML_IMPORTANT_ITEM_END;
    const static std::string HTML_SIZE;
};




#endif
