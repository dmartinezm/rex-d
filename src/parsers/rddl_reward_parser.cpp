#include "rddl_reward_parser.h"
#include <parsers/prefix_notation_parser.h>

/**
 * RDDLGoal reader
 */

#define DEBUG 0


namespace rddl_parser
{
    std::vector<PPDDL::FullObject> parse_goal_parameters (const ParserPrefixStruct& rddl_reverse_struct);
    NonTypedAtom parse_atom (const ParserPrefixStruct& rddl_reverse_struct);
    Predicate<TypedAtom> parse_predicate (const ParserPrefixStruct& rddl_reverse_struct);
    
    // leafs
    RDDLGoalComponent::Ptr parse_goal_leaf_predicate (const ParserPrefixStruct& rddl_reverse_struct);
    // numbers don't have to be between (), so checking the iterator directly makes sense
    RDDLGoalComponent::Ptr parse_goal_leaf_numeric (const std::vector<ParserElement>::const_iterator& elem_it);
    RDDLGoalComponent::Ptr parse_goal_leaf_numeric (const ParserPrefixStruct& rddl_reverse_struct);
    
    // composite
    RDDLGoalComponent::Ptr parse_goal_composite_binary (const ParserPrefixStruct& rddl_reverse_struct);
    RDDLGoalComponent::Ptr parse_goal_composite_list (const ParserPrefixStruct& rddl_reverse_struct);
    RDDLGoalComponent::Ptr parse_goal_composite_symbolic (const ParserPrefixStruct& rddl_reverse_struct);
    RDDLGoalComponent::Ptr parse_goal_composite_if (const ParserPrefixStruct& rddl_reverse_struct);
    
    // component
    // numbers don't have to be between (), so checking the iterator directly makes sense
    RDDLGoalComponent::Ptr parse_goal_component (const std::vector<ParserElement>::const_iterator& elem_it);
    RDDLGoalComponent::Ptr parse_goal_component(const ParserPrefixStruct& rddl_reverse_struct);
};


// binary
#define RDDL_KEY_PLUS       "+"
#define RDDL_KEY_MINUS      "-"
#define RDDL_KEY_TIMES      "*"
#define RDDL_KEY_EQUAL      "="
#define RDDL_KEY_LEQUAL     "<="
#define RDDL_KEY_GEQUAL     ">="
#define RDDL_KEY_GREATER    "<"
#define RDDL_KEY_LESS       ">"
// list
#define RDDL_KEY_AND        "^"
#define RDDL_KEY_OR         "|"
// predicate
#define RDDL_KEY_NOT        "~"
// symbolic
#define RDDL_KEY_EXISTS     "exists"
#define RDDL_KEY_SUM        "sum"
// if
#define RDDL_KEY_IF         "if"
#define RDDL_KEY_THEN       "then"
#define RDDL_KEY_ELSE       "else"

namespace rddl_goal_utils {
RDDLGoalComponent::Ptr get_simple_predicate(const Predicate<TypedAtom>& precondition) {
    return RDDLGoalPredicateLeaf::Ptr(new RDDLGoalPredicateLeaf(precondition));
}

RDDLGoalComponent::Ptr get_simple_preconditions(const PredicateGroup<TypedAtom>& preconditions) {
    std::vector<RDDLGoalComponent::Ptr> preconditions_components;
    std::set<PPDDL::FullObject> symbolic_params;
    BOOST_FOREACH(const Predicate<TypedAtom>& precondition, preconditions) {
        BOOST_FOREACH(const PPDDL::FullObject& prec_param, precondition.params) {
            if (symbolic::is_param_symbolic(prec_param.name)) {
                symbolic_params.insert(prec_param);
            }
        }
        preconditions_components.push_back(RDDLGoalPredicateLeaf::Ptr(new RDDLGoalPredicateLeaf(precondition)));
    }
    RDDLGoalComponent::Ptr preconditions_component_ptr(new RDDLGoalCompositeListOperator(RDDLGoalCompositeListOperator::AND, 
                                                                                         preconditions_components));
    if (!symbolic_params.empty()) {
        std::vector<PPDDL::FullObject> symbolic_params_v(symbolic_params.begin(), symbolic_params.end());
        preconditions_component_ptr = RDDLGoalComponent::Ptr(new RDDLGoalCompositeSymbolicOperator(RDDLGoalCompositeSymbolicOperator::EXISTS, 
                                                                                                   preconditions_component_ptr, 
                                                                                                   symbolic_params_v));
    }
    return preconditions_component_ptr;
}

RDDLGoalComponent::Ptr get_simple_not_component(const RDDLGoalComponent::Ptr& component_ptr) {
    std::vector<RDDLGoalComponent::Ptr> component_list;
    component_list.push_back(component_ptr);
    RDDLGoalCompositeListOperator not_preconditions_component_list(RDDLGoalCompositeListOperator::NOT, component_list);
    return not_preconditions_component_list.make_shared();
}

RDDLGoalComponent::Ptr get_simple_if_component_reward(const RDDLGoalComponent::Ptr& preconditions_component_ptr, const double then_reward, const double else_reward) {
    RDDLGoalComponent::Ptr reward_then_component_ptr(new RDDLGoalNumericLeaf(then_reward));
    RDDLGoalComponent::Ptr reward_else_component_ptr(new RDDLGoalNumericLeaf(else_reward));
    RDDLGoalComponent::Ptr if_preconditions_component_ptr(new RDDLGoalCompositeIfOperator(preconditions_component_ptr, 
                                                                                          reward_then_component_ptr, 
                                                                                          reward_else_component_ptr));
    return if_preconditions_component_ptr->make_shared();
}

RDDLGoalComponent::Ptr get_simple_add(const RDDLGoalComponent::Ptr& component1, const RDDLGoalComponent::Ptr& component2){
    std::vector<RDDLGoalComponent::Ptr> component_list;
    component_list.push_back(component1);
    component_list.push_back(component2);
    RDDLGoalCompositeListOperator add_component_list(RDDLGoalCompositeListOperator::ADD, component_list);
    return add_component_list.make_shared();
}

}

/***
 * RDDLGoal
 */
void RDDLGoalCompositeListOperator::change_param_name(const std::string& orig_var, const std::string& new_var) {
    BOOST_FOREACH(RDDLGoalComponent::Ptr& goal_component_ptr, goal_components_) {
        goal_component_ptr->change_param_name(orig_var, new_var);
    }
}
    
void RDDLGoalCompositeListOperator::change_param_type(const std::string& orig_var, const std::string& new_type) {
    BOOST_FOREACH(RDDLGoalComponent::Ptr& goal_component_ptr, goal_components_) {
        goal_component_ptr->change_param_type(orig_var, new_type);
    }
}

PredicateGroup<TypedAtom> RDDLGoalCompositeListOperator::get_predicates() const {
    PredicateGroup<TypedAtom> result;
    BOOST_FOREACH(const RDDLGoalComponent::Ptr& goal_component_ptr, goal_components_) {
        result.add_predicates(goal_component_ptr->get_predicates());
    }
    return result;
}

PredicateGroup<TypedAtom> RDDLGoalCompositeListOperator::get_predicates_that_contribute_to_goal() const { 
    PredicateGroup<TypedAtom> result;
    if ((goal_operator_ == ADD) || (goal_operator_ == TIMES) || (goal_operator_ == AND) || (goal_operator_ == OR)) {
        BOOST_FOREACH(const RDDLGoalComponent::Ptr& goal_component_ptr, goal_components_) {
            result.add_predicates(goal_component_ptr->get_predicates_that_contribute_to_goal());
        }
    }
    else if (goal_operator_ == SUBTRACT) {
        result.add_predicates(goal_components_[0]->get_predicates_that_contribute_to_goal());
        result.add_predicates(goal_components_[1]->get_predicates_that_contribute_to_goal().get_negated());
    }
    else {
        LOG(WARNING) << "Not implemented get_predicates_that_contribute_to_goal for " << goal_operator_;
    }
    return result;
}

float RDDLGoalCompositeListOperator::get_reward(const State& state, const TypedAtom& action) const {
    float reward = 0;
    if (goal_operator_ == ADD) {
        reward = goal_components_[0]->get_reward(state, action) + goal_components_[1]->get_reward(state, action);
    }
    else if (goal_operator_ == SUBTRACT) {
        reward = goal_components_[0]->get_reward(state, action) - goal_components_[1]->get_reward(state, action);
    }
    else if (goal_operator_ == TIMES) {
        reward = goal_components_[0]->get_reward(state, action) * goal_components_[1]->get_reward(state, action);
    }
    else if (goal_operator_ == EQUAL) {
        reward = goal_components_[0]->get_reward(state, action) == goal_components_[1]->get_reward(state, action);
    }
    else if (goal_operator_ == GREATER_EQUAL) {
        reward = goal_components_[0]->get_reward(state, action) >= goal_components_[1]->get_reward(state, action);
    }
    else if (goal_operator_ == LESS_EQUAL) {
        reward = goal_components_[0]->get_reward(state, action) <= goal_components_[1]->get_reward(state, action);
    }
    else if (goal_operator_ == GREATER) {
        reward = goal_components_[0]->get_reward(state, action) > goal_components_[1]->get_reward(state, action);
    }
    else if (goal_operator_ == LESS) {
        reward = goal_components_[0]->get_reward(state, action) < goal_components_[1]->get_reward(state, action);
    }
    else if (goal_operator_ == AND) {
        reward = 1.0;
        BOOST_FOREACH(const RDDLGoalComponent::Ptr& goal_component_ptr, goal_components_) {
            reward *= goal_component_ptr->get_reward(state, action);
        }
    }
    else if (goal_operator_ == OR) {
        reward = 0.0;
        BOOST_FOREACH(const RDDLGoalComponent::Ptr& goal_component_ptr, goal_components_) {
            reward += goal_component_ptr->get_reward(state, action);
        }
        reward = std::min((float)1.0, reward);
    }
    else {
        LOG(ERROR) << "Not implemented get_reward for " << goal_operator_;
    }
    return reward;
}

void RDDLGoalCompositeListOperator::write_RDDL(std::ostream& out) const {
    out << "(";
    if (goal_operator_ == ADD) {
        out << RDDL_KEY_PLUS;
    }
    else if (goal_operator_ == SUBTRACT) {
        out << RDDL_KEY_MINUS;
    }
    else if (goal_operator_ == TIMES) {
        out << RDDL_KEY_TIMES;
    }
    else if (goal_operator_ == EQUAL) {
        out << RDDL_KEY_EQUAL;
    }
    else if (goal_operator_ == GREATER_EQUAL) {
        out << RDDL_KEY_GEQUAL;
    }
    else if (goal_operator_ == LESS_EQUAL) {
        out << RDDL_KEY_LEQUAL;
    }
    else if (goal_operator_ == GREATER) {
        out << RDDL_KEY_GREATER;
    }
    else if (goal_operator_ == LESS) {
        out << RDDL_KEY_LESS;
    }
    else if (goal_operator_ == AND) {
        out << RDDL_KEY_AND;
    }
    else if (goal_operator_ == OR) {
        out << RDDL_KEY_OR;
    }
    else if (goal_operator_ == NOT) {
        out << RDDL_KEY_NOT;
    }
    else {
        LOG(WARNING) << "Not implemented write_RDDL for " << goal_operator_;
    }
    BOOST_FOREACH(const RDDLGoalComponent::Ptr& goal_component_ptr, goal_components_) {
        out << " ";
        goal_component_ptr->write_RDDL(out);
    }
    out << ")";
}


float RDDLGoalCompositeSymbolicOperator::get_reward(const State& state, const TypedAtom& action) const {
    float reward = 0;
    std::vector<PPDDL::FullObject::Type> list_param_types;
    BOOST_FOREACH(const PPDDL::FullObject& param, goal_parameters_) { list_param_types.push_back(param.type); }
    PPDDLObjectManager<PPDDL::FullObject>::CombinationsPPDDLObjectsPtr combinations_grounded_params = state.ppddl_object_manager.get_combinations_of_objecs_for_given_params(list_param_types);
    
    BOOST_FOREACH(const std::vector<PPDDL::FullObject::Name> grounded_params, *combinations_grounded_params) {
        RDDLGoalComponent::Ptr grounded_component_ptr = goal_component_ptr_->make_shared();
        for (size_t i = 0; i < goal_parameters_.size(); ++i) {
            grounded_component_ptr->change_param_name(goal_parameters_[i].name, grounded_params[i]);
        }
        reward += grounded_component_ptr->get_reward(state, action);
    }
    
    if (goal_operator_ == EXISTS) { // exists is either 0.0 or 1.0
        reward = std::min(reward, (float)1.0);
    }
    return reward;
}

void RDDLGoalCompositeSymbolicOperator::write_RDDL(std::ostream& out) const {
    out << "(";
    if (goal_operator_ == SUM) {
        out << RDDL_KEY_SUM;
    }
    else if (goal_operator_ == EXISTS) {
        out << RDDL_KEY_EXISTS;
    }
    else {
        LOG(WARNING) << "Not implemented write_RDDL for " << goal_operator_;
    }
    out << " ( ";
    BOOST_FOREACH(const PPDDL::FullObject& param, goal_parameters_) {
        out << "(" << param.name << " : " << param.type << ") ";
    }
    out << ") ";
    goal_component_ptr_->write_RDDL(out);
    out << ")";
}

void RDDLGoalCompositeIfOperator::change_param_name(const std::string& orig_var, const std::string& new_var)
{ 
    preconditions_component_->change_param_name(orig_var, new_var);
    then_component_->change_param_name(orig_var, new_var);
    if (else_component_) else_component_->change_param_name(orig_var, new_var);
}
    
void RDDLGoalCompositeIfOperator::change_param_type(const std::string& orig_var, const std::string& new_type)
{
    preconditions_component_->change_param_type(orig_var, new_type);
    then_component_->change_param_type(orig_var, new_type);
    if (else_component_) else_component_->change_param_type(orig_var, new_type);
}

PredicateGroup<TypedAtom> RDDLGoalCompositeIfOperator::get_predicates() const
{
    PredicateGroup<TypedAtom> result;
    result.add_predicates(preconditions_component_->get_predicates());
    result.add_predicates(then_component_->get_predicates());
    if (else_component_) result.add_predicates(else_component_->get_predicates());
    return result;
}

PredicateGroup<TypedAtom> RDDLGoalCompositeIfOperator::get_predicates_that_contribute_to_goal() const
{
    LOG(WARNING) << "Not implemented get_predicates_that_contribute_to_goal for if operator";
    return PredicateGroup<TypedAtom>();
}

float RDDLGoalCompositeIfOperator::get_reward(const State& state, const TypedAtom& action) const
{
    if (preconditions_component_->get_reward(state, action) >= 1.0) {
        return then_component_->get_reward(state, action);
    }
    else {
        if (else_component_) {
            return else_component_->get_reward(state, action);
        }
        else {
            return 0.0;
        }
    }
}

void RDDLGoalCompositeIfOperator::write_RDDL(std::ostream& out) const
{
    out << "(if ";
    preconditions_component_->write_RDDL(out);
    out << " then ";
    then_component_->write_RDDL(out);
    if (else_component_) {
        out << " else ";
        else_component_->write_RDDL(out);
    }
    out << ")";
}

void RDDLGoalPredicateLeaf::change_param_type(const std::string& orig_var, const std::string& new_type) {
    BOOST_FOREACH(PPDDL::FullObject& param, goal_predicate_.params) {
        if (param.name == orig_var) {
            param.type = new_type;
        }
    }
}

float RDDLGoalPredicateLeaf::get_reward(const State& state, const TypedAtom& action) const {
    if (state.satisfies(goal_predicate_)) {
        return 1.0;
    }
    else if (action == goal_predicate_) {
        return 1.0;
    }
    else {
        return 0.0;
    }
}


RDDLGoal rddl_parser::parse_goal (const std::string& rddl_str)
{
    std::string cropped_rddl_str = rddl_str.substr(rddl_str.find("=")+1);
    return parse_goal(parse_prefix_notation(cropped_rddl_str));
}

RDDLGoal rddl_parser::parse_goal (const ParserPrefixStruct& rddl_reverse_struct)
{
    RDDLGoalComponent::Ptr component_ptr = parse_goal_component(rddl_reverse_struct);
    return RDDLGoal(component_ptr);
}

std::vector<PPDDL::FullObject> rddl_parser::parse_goal_parameters (const ParserPrefixStruct& rddl_reverse_struct)
{
    std::vector<PPDDL::FullObject> result;
    std::vector<ParserElement>::const_iterator elem_it = rddl_reverse_struct.children.begin();
    while (elem_it != rddl_reverse_struct.children.end()) {
        ParserPrefixStruct object_rdd_reverse_struct = boost::get<ParserPrefixStruct>(*(elem_it));
        std::vector<ParserElement>::const_iterator object_elem_it = object_rdd_reverse_struct.children.begin();
        
        std::string name = boost::get<std::string>(*(object_elem_it++));
        assert(boost::get<std::string>(*(object_elem_it)) == ":");
        ++object_elem_it;
        std::string type = boost::get<std::string>(*(object_elem_it));
        
        result.push_back(PPDDL::FullObject(name, type));
        ++elem_it;
    }
    return result;
}

RDDLGoalComponent::Ptr rddl_parser::parse_goal_leaf_numeric (const std::vector<ParserElement>::const_iterator& elem_it)
{
    try {
        std::string elem_str = boost::get<std::string>(*elem_it);
        float number = boost::lexical_cast<float>(elem_str);
        CLOG_IF(DEBUG >=2, INFO, "trivial") << "Number is " << number;
        return RDDLGoalComponent::Ptr(new RDDLGoalNumericLeaf(number));
    }
    catch (boost::bad_lexical_cast& e) {
        return RDDLGoalComponent::Ptr();
    }
    catch (boost::bad_get& e) {
        return RDDLGoalComponent::Ptr();
    }
}

RDDLGoalComponent::Ptr rddl_parser::parse_goal_leaf_numeric (const ParserPrefixStruct& rddl_reverse_struct)
{
    if (rddl_reverse_struct.children.size() != 1) { return RDDLGoalComponent::Ptr(); }
    std::vector<ParserElement>::const_iterator elem_it = rddl_reverse_struct.children.begin();
    return parse_goal_leaf_numeric(elem_it);
}

NonTypedAtom rddl_parser::parse_atom (const ParserPrefixStruct& rddl_reverse_struct)
{
    std::string name;
    std::vector<std::string> param_names;
    
    std::vector<ParserElement>::const_iterator elem_it = rddl_reverse_struct.children.begin();
    name = boost::get<std::string>(*elem_it); // get first element
    
    for(++elem_it; elem_it != rddl_reverse_struct.children.end(); ++elem_it) {
        if (boost::get<std::string>(*elem_it) == "-") { // was not a properly defined Atom
            throw PrefixNotationParserException("Couldn't parse Atom \"" + name + "\", it should be a NonTypedAtom but it is a TypedAtom.");
        }   
        param_names.push_back(boost::get<std::string>(*elem_it));
    }
    NonTypedAtom atom(name, param_names);
    return atom;
}

Predicate<TypedAtom> rddl_parser::parse_predicate (const ParserPrefixStruct& rddl_reverse_struct)
{
    bool positive;
    NonTypedAtom nontyped_atom;
    std::vector<ParserElement>::const_iterator elem_it = rddl_reverse_struct.children.begin();
    
    if (boost::get<std::string>(*elem_it) == RDDL_KEY_NOT) { // (not (pred1))
        positive = false;
        nontyped_atom = parse_atom(boost::get<ParserPrefixStruct>(*(elem_it+1)));
    }
    else { // (pred1)
        positive = true;
        nontyped_atom = parse_atom(boost::get<ParserPrefixStruct>(rddl_reverse_struct));
    }
    PPDDLObjectManager<PPDDL::FullObject> object_manager;
    Predicate<TypedAtom> predicate(object_manager.get_typed_atom(nontyped_atom), positive);
    
    CLOG_IF(DEBUG >=2, INFO, "trivial") << "Predicate<Atom> is " << predicate;
    return predicate;
}

RDDLGoalComponent::Ptr rddl_parser::parse_goal_leaf_predicate (const ParserPrefixStruct& rddl_reverse_struct)
{
    Predicate<TypedAtom> predicate = parse_predicate(rddl_reverse_struct);
    return RDDLGoalComponent::Ptr(new RDDLGoalPredicateLeaf(predicate));
}

RDDLGoalComponent::Ptr rddl_parser::parse_goal_composite_binary (const ParserPrefixStruct& rddl_reverse_struct)
{
    if (rddl_reverse_struct.children.size() != 3) { return RDDLGoalComponent::Ptr(); }
    
    std::vector<ParserElement>::const_iterator elem_it = rddl_reverse_struct.children.begin();
    std::string binary_operator = boost::get<std::string>(*elem_it);
    
    if ( (binary_operator == RDDL_KEY_PLUS) ||
         (binary_operator == RDDL_KEY_MINUS) ||
         (binary_operator == RDDL_KEY_TIMES) ||
         (binary_operator == RDDL_KEY_EQUAL) ||
         (binary_operator == RDDL_KEY_LEQUAL) ||
         (binary_operator == RDDL_KEY_GEQUAL) ||
         (binary_operator == RDDL_KEY_GREATER) ||
         (binary_operator == RDDL_KEY_LESS) )
    {
        CLOG_IF(DEBUG >=1, INFO, "trivial") << "Got " << binary_operator;
        std::vector<RDDLGoalComponent::Ptr> components;
        ++elem_it;
        components.push_back(parse_goal_component(elem_it));
        ++elem_it;
        components.push_back(parse_goal_component(elem_it));
        
        if (binary_operator == RDDL_KEY_PLUS) { // +
            return RDDLGoalComponent::Ptr(new RDDLGoalCompositeListOperator(RDDLGoalCompositeListOperator::ADD, components));
        }
        else if (binary_operator == RDDL_KEY_MINUS) { // -
            return RDDLGoalComponent::Ptr(new RDDLGoalCompositeListOperator(RDDLGoalCompositeListOperator::SUBTRACT, components));
        }
        else if (binary_operator == RDDL_KEY_TIMES) { // -
            return RDDLGoalComponent::Ptr(new RDDLGoalCompositeListOperator(RDDLGoalCompositeListOperator::TIMES, components));
        }
        else if (binary_operator == RDDL_KEY_EQUAL) { // -
            return RDDLGoalComponent::Ptr(new RDDLGoalCompositeListOperator(RDDLGoalCompositeListOperator::EQUAL, components));
        }
        else if (binary_operator == RDDL_KEY_LEQUAL) { // -
            return RDDLGoalComponent::Ptr(new RDDLGoalCompositeListOperator(RDDLGoalCompositeListOperator::LESS_EQUAL, components));
        }
        else if (binary_operator == RDDL_KEY_GEQUAL) { // -
            return RDDLGoalComponent::Ptr(new RDDLGoalCompositeListOperator(RDDLGoalCompositeListOperator::GREATER_EQUAL, components));
        }
        else if (binary_operator == RDDL_KEY_LESS) { // -
            return RDDLGoalComponent::Ptr(new RDDLGoalCompositeListOperator(RDDLGoalCompositeListOperator::LESS, components));
        }
        else if (binary_operator == RDDL_KEY_GREATER) { // -
            return RDDLGoalComponent::Ptr(new RDDLGoalCompositeListOperator(RDDLGoalCompositeListOperator::GREATER, components));
        }
        else {
            throw PrefixNotationExceptionIgnorable("Couldn't read reward (unimplemented operator " + boost::get<std::string>(*elem_it) + ")");
            return RDDLGoalComponent::Ptr();
        }
    }
    else {
        return RDDLGoalComponent::Ptr();
    }
}

RDDLGoalComponent::Ptr rddl_parser::parse_goal_composite_list (const ParserPrefixStruct& rddl_reverse_struct)
{
    if (rddl_reverse_struct.children.size() == 1) { return RDDLGoalComponent::Ptr(); }
    
    std::vector<ParserElement>::const_iterator elem_it = rddl_reverse_struct.children.begin();
    std::vector<RDDLGoalComponent::Ptr> components;
    std::string list_operator = boost::get<std::string>(*elem_it);
    if ((list_operator == RDDL_KEY_AND) || (list_operator == RDDL_KEY_OR)) {
        CLOG_IF(DEBUG >=1, INFO, "trivial") << "Got " << list_operator;
        ++elem_it;
        while (elem_it != rddl_reverse_struct.children.end()) {
            components.push_back(parse_goal_component(elem_it));
            ++elem_it;
        }
        if (list_operator == RDDL_KEY_AND) {
            return RDDLGoalComponent::Ptr(new RDDLGoalCompositeListOperator(RDDLGoalCompositeListOperator::AND, components));
        }
        else { // RDDL_KEY_OR
            return RDDLGoalComponent::Ptr(new RDDLGoalCompositeListOperator(RDDLGoalCompositeListOperator::OR, components));
        }
    }
    else {
        return RDDLGoalComponent::Ptr();
    }
}

RDDLGoalComponent::Ptr rddl_parser::parse_goal_composite_if (const ParserPrefixStruct& rddl_reverse_struct)
{
    std::vector<ParserElement>::const_iterator elem_it = rddl_reverse_struct.children.begin();
    std::string reward_operator = boost::get<std::string>(*elem_it);
    if (reward_operator == RDDL_KEY_IF) {
        RDDLGoalComponent::Ptr if_component, then_component, else_component;
        ++elem_it;
        if_component = parse_goal_component(elem_it);
        ++elem_it;
        assert(boost::get<std::string>(*elem_it) == "then");
        ++elem_it;
        then_component = parse_goal_component(elem_it);
        ++elem_it;
        if (elem_it != rddl_reverse_struct.children.end()) { // optional else
            assert(boost::get<std::string>(*elem_it) == "else");
            ++elem_it;
            else_component = parse_goal_component(elem_it);
        }
        return RDDLGoalComponent::Ptr(new RDDLGoalCompositeIfOperator(if_component, then_component, else_component));
    }
    return RDDLGoalComponent::Ptr();
}

RDDLGoalComponent::Ptr rddl_parser::parse_goal_composite_symbolic (const ParserPrefixStruct& rddl_reverse_struct)
{
    if (rddl_reverse_struct.children.size() != 3) { return RDDLGoalComponent::Ptr(); }
    
    std::vector<ParserElement>::const_iterator elem_it = rddl_reverse_struct.children.begin();
    std::string reward_operator = boost::get<std::string>(*elem_it);
    if ( (reward_operator == RDDL_KEY_SUM) || (reward_operator == RDDL_KEY_EXISTS)) {
        CLOG_IF(DEBUG >=1, INFO, "trivial") << "Got " << reward_operator;
        ++elem_it;
        std::vector<PPDDL::FullObject> goal_parameters = parse_goal_parameters(boost::get<ParserPrefixStruct>(*elem_it));
        ++elem_it;
        RDDLGoalComponent::Ptr component_ptr = parse_goal_component(boost::get<ParserPrefixStruct>(*elem_it));
        
        for (size_t i = 0; i < goal_parameters.size(); ++i) {
            component_ptr->change_param_type(goal_parameters[i].name, goal_parameters[i].type);
        }
        
        if (reward_operator == RDDL_KEY_SUM) {
            return RDDLGoalComponent::Ptr(new RDDLGoalCompositeSymbolicOperator(RDDLGoalCompositeSymbolicOperator::SUM, component_ptr, goal_parameters));
        }
        else { // exists
            return RDDLGoalComponent::Ptr(new RDDLGoalCompositeSymbolicOperator(RDDLGoalCompositeSymbolicOperator::EXISTS, component_ptr, goal_parameters));
        }
    }
    else {
        return RDDLGoalComponent::Ptr();
    }
}

RDDLGoalComponent::Ptr rddl_parser::parse_goal_component (const std::vector<ParserElement>::const_iterator& elem_it)
{
    RDDLGoalComponent::Ptr goal_component_ptr;
    goal_component_ptr = parse_goal_leaf_numeric(elem_it);
    if (!goal_component_ptr) { 
        goal_component_ptr = parse_goal_component(boost::get<ParserPrefixStruct>(*elem_it));
    }
    return goal_component_ptr;
}

RDDLGoalComponent::Ptr rddl_parser::parse_goal_component (const ParserPrefixStruct& rddl_reverse_struct)
{
    RDDLGoalComponent::Ptr goal_component_ptr;
    goal_component_ptr = parse_goal_composite_symbolic(rddl_reverse_struct);
    if (!goal_component_ptr) {
        goal_component_ptr = parse_goal_composite_if(rddl_reverse_struct);
    }
    if (!goal_component_ptr) {
        goal_component_ptr = parse_goal_composite_list(rddl_reverse_struct);
    }
    if (!goal_component_ptr) {
        goal_component_ptr = parse_goal_composite_binary(rddl_reverse_struct);
    }
    if (!goal_component_ptr) {
        goal_component_ptr = parse_goal_leaf_numeric(rddl_reverse_struct);
    }
    if (!goal_component_ptr) { 
        // Importnat note: parse_goal_leaf_predicate has to be the last one (default)
        try {
            goal_component_ptr = parse_goal_leaf_predicate(rddl_reverse_struct);
        }
        catch (boost::bad_get e) {
            throw PrefixNotationParserException("Couldn't parse goal component: " + std::string(e.what()));
        }
    }
    return goal_component_ptr;
}
                                                      