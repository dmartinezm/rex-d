/*
 * RDDL goal parser.
 * Copyright (C) 2015  dmartinez <email>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef REXD_RDDL_GOAL_PARSER_H
#define REXD_RDDL_GOAL_PARSER_H 1

#include <symbolic/goals.h>
#include <parsers/prefix_notation_parser.h>

namespace rddl_parser
{
    RDDLGoal parse_goal (const std::string& rddl_str);
    RDDLGoal parse_goal (const ParserPrefixStruct& rddl_reverse_struct);
};

namespace rddl_goal_utils {
    RDDLGoalComponent::Ptr get_simple_add(const RDDLGoalComponent::Ptr& component1, const RDDLGoalComponent::Ptr& component2);
    RDDLGoalComponent::Ptr get_simple_predicate(const Predicate<TypedAtom>& precondition);
    RDDLGoalComponent::Ptr get_simple_preconditions(const PredicateGroup<TypedAtom>& preconditions);
    RDDLGoalComponent::Ptr get_simple_not_component(const RDDLGoalComponent::Ptr& component_ptr);
    RDDLGoalComponent::Ptr get_simple_if_component_reward(const RDDLGoalComponent::Ptr& preconditions_component_ptr, const double then_reward, const double else_reward);
}


class RDDLGoalCompositeListOperator : public RDDLGoalComponent 
{
public:
    enum GoalListOperator { ADD, SUBTRACT, TIMES, EQUAL, LESS, GREATER, LESS_EQUAL, GREATER_EQUAL, AND, OR, NOT };
    
    RDDLGoalCompositeListOperator(const GoalListOperator& goal_operator,
                                  const std::vector<RDDLGoalComponent::Ptr>& goal_components):
        goal_operator_(goal_operator)
    { 
        BOOST_FOREACH(const RDDLGoalComponent::Ptr& goal_component, goal_components) {
            goal_components_.push_back(goal_component->make_shared());
        }
        assert( ((goal_operator_ == NOT) && (goal_components_.size() == 1)) || 
                (goal_operator_ == AND) || 
                (goal_operator_ == OR) || 
                (goal_components_.size() == 2));
    }
    
    #if __cplusplus > 199711L // C++11
    RDDLGoalCompositeListOperator(const RDDLGoalCompositeListOperator& other):
        RDDLGoalCompositeListOperator(other.goal_operator_, other.goal_components_)
    { }
    #else
    RDDLGoalCompositeListOperator(const RDDLGoalCompositeListOperator& other):
        goal_operator_(other.goal_operator_)
    { 
        BOOST_FOREACH(const RDDLGoalComponent::Ptr& goal_component, other.goal_components_) {
            goal_components_.push_back(goal_component->make_shared());
        }
        assert((goal_operator_ == AND) || (goal_operator_ == OR) || (goal_components_.size() == 2));
    }
    #endif
    
    virtual RDDLGoalComponent::Ptr make_shared() const { return RDDLGoalComponent::Ptr(new RDDLGoalCompositeListOperator(*this)); }
    
    virtual void change_param_name(const std::string& orig_var, const std::string& new_var);
    
    virtual void change_param_type(const std::string& orig_var, const std::string& new_type);
    
    virtual PredicateGroup<TypedAtom> get_predicates() const;
    
    virtual PredicateGroup<TypedAtom> get_predicates_that_contribute_to_goal() const;
    
    virtual float get_reward(const State& state, const TypedAtom& action) const;
    
    virtual void write_RDDL(std::ostream& out) const;
    
private:
    GoalListOperator goal_operator_;
    std::vector<RDDLGoalComponent::Ptr> goal_components_;
};


class RDDLGoalCompositeSymbolicOperator : public RDDLGoalComponent {
public:
    enum GoalSymbolicOperator { EXISTS, SUM };
    
    RDDLGoalCompositeSymbolicOperator(const GoalSymbolicOperator& goal_operator,
                                      const RDDLGoalComponent::Ptr& goal_component_ptr,
                                      const std::vector<PPDDL::FullObject>& goal_parameters) :
        goal_operator_(goal_operator),
        goal_component_ptr_(goal_component_ptr->make_shared()),
        goal_parameters_(goal_parameters)
    { }
    
    #if __cplusplus > 199711L // C++11
    RDDLGoalCompositeSymbolicOperator(const RDDLGoalCompositeSymbolicOperator& other):
        RDDLGoalCompositeSymbolicOperator(other.goal_operator_, other.goal_component_ptr_, other.goal_parameters_)
    { }
    #else
    RDDLGoalCompositeSymbolicOperator(const RDDLGoalCompositeSymbolicOperator& other) :
        goal_operator_(other.goal_operator_),
        goal_component_ptr_(other.goal_component_ptr_->make_shared()),
        goal_parameters_(other.goal_parameters_)
    { }
    #endif
    
    virtual RDDLGoalComponent::Ptr make_shared() const { return RDDLGoalComponent::Ptr(new RDDLGoalCompositeSymbolicOperator(*this)); }
    
    virtual void change_param_name(const std::string& orig_var, const std::string& new_var) { goal_component_ptr_->change_param_name(orig_var, new_var); }
    
    virtual void change_param_type(const std::string& orig_var, const std::string& new_type) { goal_component_ptr_->change_param_type(orig_var, new_type); }
    
    virtual PredicateGroup<TypedAtom> get_predicates() const { return goal_component_ptr_->get_predicates(); }
    
    virtual PredicateGroup<TypedAtom> get_predicates_that_contribute_to_goal() const { return goal_component_ptr_->get_predicates_that_contribute_to_goal(); }
    
    virtual float get_reward(const State& state, const TypedAtom& action) const;
    
    virtual void write_RDDL(std::ostream& out) const;
    
private:
    GoalSymbolicOperator goal_operator_;
    RDDLGoalComponent::Ptr goal_component_ptr_;
    std::vector<PPDDL::FullObject> goal_parameters_;
};


class RDDLGoalCompositeIfOperator : public RDDLGoalComponent {
public:
    enum GoalSymbolicOperator { EXISTS, SUM };
    
    RDDLGoalCompositeIfOperator(const RDDLGoalComponent::Ptr preconditions_component,
                                const RDDLGoalComponent::Ptr then_component,
                                const RDDLGoalComponent::Ptr else_component) :
        preconditions_component_(preconditions_component->make_shared()),
        then_component_(then_component->make_shared()),
        else_component_(else_component->make_shared())
    { }
    
    #if __cplusplus > 199711L // C++11
    RDDLGoalCompositeIfOperator(const RDDLGoalCompositeIfOperator& other):
        RDDLGoalCompositeIfOperator(other.preconditions_component_, other.then_component_, other.else_component_)
    { }
    #else
    RDDLGoalCompositeIfOperator(const RDDLGoalCompositeIfOperator& other) :
        preconditions_component_(other.preconditions_component_->make_shared()),
        then_component_(other.then_component_->make_shared()),
        else_component_(other.else_component_->make_shared())
    { }
    #endif
    
    virtual RDDLGoalComponent::Ptr make_shared() const { return RDDLGoalComponent::Ptr(new RDDLGoalCompositeIfOperator(*this)); }
    
    virtual void change_param_name(const std::string& orig_var, const std::string& new_var);
    
    virtual void change_param_type(const std::string& orig_var, const std::string& new_type);
    
    virtual PredicateGroup<TypedAtom> get_predicates() const;
    
    virtual PredicateGroup<TypedAtom> get_predicates_that_contribute_to_goal() const;
    
    virtual float get_reward(const State& state, const TypedAtom& action) const;
    
    virtual void write_RDDL(std::ostream& out) const;
    
private:
    RDDLGoalComponent::Ptr preconditions_component_;
    RDDLGoalComponent::Ptr then_component_;
    RDDLGoalComponent::Ptr else_component_;
};


class RDDLGoalPredicateLeaf : public RDDLGoalComponent {
public:
    RDDLGoalPredicateLeaf(const Predicate<TypedAtom>& goal_predicate) : goal_predicate_(goal_predicate) {};
    
    virtual RDDLGoalComponent::Ptr make_shared() const { return RDDLGoalComponent::Ptr(new RDDLGoalPredicateLeaf(*this)); }
    
    virtual void change_param_name(const std::string& orig_var, const std::string& new_var) { goal_predicate_.change_param_name(orig_var, new_var); }
    
    virtual void change_param_type(const std::string& orig_var, const std::string& new_type);
    
    virtual PredicateGroup<TypedAtom> get_predicates() const { PredicateGroup<TypedAtom> result; result.insert(goal_predicate_); return result; };
    
    virtual PredicateGroup<TypedAtom> get_predicates_that_contribute_to_goal() const { PredicateGroup<TypedAtom> result; result.insert(goal_predicate_); return result; };
    
    virtual float get_reward(const State& state, const TypedAtom& action) const;
    
    virtual void write_RDDL(std::ostream& out) const { goal_predicate_.write_RDDL(out); }
    
private:
    Predicate<TypedAtom> goal_predicate_;
};


class RDDLGoalNumericLeaf : public RDDLGoalComponent {
public:
    RDDLGoalNumericLeaf(const float reward):
        reward_(reward)
    { }
    
    virtual RDDLGoalComponent::Ptr make_shared() const { return RDDLGoalComponent::Ptr(new RDDLGoalNumericLeaf(*this)); }
    
    virtual void change_param_name(const std::string& orig_var, const std::string& new_var) { }
    
    virtual void change_param_type(const std::string& orig_var, const std::string& new_type) { }
    
    virtual PredicateGroup<TypedAtom> get_predicates() const { return PredicateGroup<TypedAtom>(); };
    
    virtual PredicateGroup<TypedAtom> get_predicates_that_contribute_to_goal() const { return PredicateGroup<TypedAtom>(); };
    
    virtual float get_reward(const State& state, const TypedAtom& action) const { return reward_; }
    
    virtual void write_RDDL(std::ostream& out) const { out << reward_; }
    
private:
    float reward_;
};


#endif