/*
 * Prefix notation parser using boost spirit qi implementation
 * Copyright (C) 2015  David Martínez <dmartinez@iri.upc.edu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "prefix_notation_parser.h"

namespace fusion = boost::fusion;
namespace phoenix = boost::phoenix;
namespace qi = boost::spirit::qi;
namespace ascii = boost::spirit::ascii;


template <typename Iterator, typename Skipper = qi::rule<Iterator> >
    struct pddl_grammar : qi::grammar<Iterator, ParserPrefixStruct(), Skipper>
    {
        pddl_grammar() : pddl_grammar::base_type(pddl_struct)
        {
            using qi::lit;
            using qi::lexeme;
            using ascii::char_;
            using ascii::string;
            using namespace qi::labels;

            using phoenix::at_c;
            using phoenix::push_back;
            
            text %= lexeme[+(char_ - '(' - ')' - ' ' - '\n')];
            node %= (pddl_struct | text);

            start_tag = lit('(');
            end_tag = lit(')');

            pddl_struct =
                    start_tag                   
                >>  *(node                       [push_back(at_c<0>(_val), _1)])
                >>  end_tag
            ;
        }

        qi::rule<Iterator, ParserPrefixStruct(), Skipper> pddl_struct;
        qi::rule<Iterator, ParserElement(), Skipper> node;
        qi::rule<Iterator, std::string(), Skipper> text;
        qi::rule<Iterator, std::string(), Skipper> start_tag;
        qi::rule<Iterator, std::string(), Skipper> end_tag;
    };


ParserPrefixStruct parse_prefix_notation(const std::string& str)
{
    using boost::spirit::ascii::space; 
    
    // Comment skipper
    qi::rule<std::string::const_iterator> skip_ws_and_comments 
            = space 
            | ";" >> *(ascii::char_-qi::eol) >> qi::eol
            ;
    
    ParserPrefixStruct pddl_reverse_struct;
    pddl_grammar<std::string::const_iterator, qi::rule<std::string::const_iterator> > g; // grammar
    bool r = phrase_parse(str.begin(), str.end(), g, skip_ws_and_comments, pddl_reverse_struct);
    if (!r) {
        LOG(WARNING) << "There was an error parsing " << str;
    }
    return pddl_reverse_struct;
}

