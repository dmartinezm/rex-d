/*
 * PPDDL file parser.
 * Copyright (C) 2015  dmartinez <email>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef PDDLPARSER_H
#define PDDLPARSER_H 1

#include <symbolic/predicates.h>
#include <symbolic/rules.h>
#include <symbolic/state.h>
#include <symbolic/transitions.h>


namespace ppddl_parser {
    NonTypedAtom  read_atom          (const std::string& str);
    TypedAtom     read_typed_atom    (const std::string& str);
    Predicate<TypedAtom> read_predicate (const std::string& str, const PPDDLObjectManager<PPDDL::FullObject>& object_manager);
    PredicateGroup<TypedAtom> read_predicate_group (const std::string& str, const PPDDLObjectManager<PPDDL::FullObject>& object_manager);
//     Outcome         read_outcome         (const std::string& str); // WARNING PDDL outcome != NID outcome
    OutcomeGroup<TypedAtom> read_outcome_group   (const std::string& str, const PPDDLObjectManager<PPDDL::FullObject>& object_manager);
    Typed::RulePtr read_symbolic_rule   (const std::string& str);
    Typed::RuleSet read_symbolic_ruleset(const std::string& str);
    
    State read_state (const std::string& str);
    std::vector<PPDDL::FullObject::Type> read_pddl_types    (const std::string& str);
    RewardFunctionGroup read_global_goals(const std::string& str, const PPDDLObjectManager<PPDDL::FullObject>& object_manager);
    
    PPDDLObjectManager<PPDDL::FullObject> read_objects_from_simple_string(const std::string& str);
    PPDDLObjectManager<PPDDL::FullObject> read_objects_from_problem_file (const std::string& str);
    PredicateGroup<TypedAtom> read_constants_from_problem_file(const std::string& str, const PPDDLObjectManager<PPDDL::FullObject>& object_manager);
    
    enum action_string_code {
        ePDDL_ACTION_PARAMETERS,
        ePDDL_ACTION_PRECONDITION,
        ePDDL_ACTION_EFFECT,
        ePDDL_KEY_DEFINE,
        ePDDL_KEY_DOMAIN,
        ePDDL_KEY_GOAL,
        ePDDL_KEY_GOAL_REWARD,
        ePDDL_KEY_INIT,
        ePDDL_KEY_METRIC,
        ePDDL_KEY_OBJECTS,
        ePDDL_KEY_PREDICATES,
        ePDDL_KEY_PROBLEM,
        ePDDL_KEY_REQUIREMENTS,
        ePDDL_KEY_TYPES,
        ePDDL_UNKNOWN
    };
    
    action_string_code enum_hash (std::string const& str);
};


class PDDLFileReader
{
public:
    explicit PDDLFileReader(const std::string& file_path_) : file_path(file_path_) {};
    ~PDDLFileReader();
    
    /**
     * \brief Read a PDDL domain
     * 
     * A PDDL domain contains:
     *  - requirements (not supported)
     *  - types 
     *  - predicates (not supported / used)
     *  - actions
     */
    void parse_domain_file(Typed::RuleSet& ruleset) const;
    void parse_domain_file(std::vector< PPDDL::FullObject::Type >& types) const;
    
    /**
     * \brief Read a PDDL problem file
     * 
     * A PDDL problem contains:
     *  - objects
     *  - init state
     *  - goal (not supported)
     */
    void parse_problem_file(State& state, RewardFunctionGroup& goals) const;
    
    void parse_problem_file_objects(PPDDLObjectManager<PPDDL::FullObject>& object_manager) const;
    
    void parse_problem_file_constants(PredicateGroup<TypedAtom>& constants, const PPDDLObjectManager<PPDDL::FullObject>& object_manager) const;
    
    /**
     * \brief Parse a transitions file
     */
    void parse_transitions_file(TransitionGroup<TypedAtom>& transitions, const PPDDLObjectManager<PPDDL::FullObject>& object_manager);
    
private:
    std::string file_path;
    
    std::ifstream pddl_file;
    
    void open_file();
    
    boost::optional<std::string> read_line();
};

#endif // PDDLPARSER_H
