/*
 * Prefix notation parser using boost spirit qi
 * Copyright (C) 2015  dmartinez <email>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef REXD_PREFIX_NOTATION_PARSER_H
#define REXD_PREFIX_NOTATION_PARSER_H 1

#include <boost/config/warning_disable.hpp>
#include <boost/spirit/include/qi.hpp>
#include <boost/spirit/include/phoenix_core.hpp>
#include <boost/spirit/include/phoenix_operator.hpp>
#include <boost/spirit/include/phoenix_fusion.hpp>
#include <boost/spirit/include/phoenix_stl.hpp>
#include <boost/fusion/include/adapt_struct.hpp>
#include <boost/variant/recursive_variant.hpp>

#include <symbolic/utils.h>


struct PrefixNotationExceptionIgnorable : public std::exception
{
    std::string pddl_type;
    
    explicit PrefixNotationExceptionIgnorable(std::string pddl_type_) throw(): std::exception(), pddl_type(pddl_type_) {};
    ~PrefixNotationExceptionIgnorable() throw() {};
    
    const char * what () const throw () {
        return std::string("Error parsing PDDL structure \"" + pddl_type + "\".").c_str();
    }
};

struct PrefixNotationParserException : public std::exception
{ 
    std::string problem;
    
    explicit PrefixNotationParserException(std::string problem_str) throw(): std::exception(), problem(problem_str) {};
    ~PrefixNotationParserException() throw() {};
    
    const char * what () const throw () {
        return std::string("Error parsing Prefix Notation structure: \"" + problem + "\".").c_str();
    }
};


struct ParserPrefixStruct;

typedef 
    boost::variant<
        boost::recursive_wrapper<ParserPrefixStruct>
        , std::string
    > 
    ParserElement;

struct ParserPrefixStruct
{
    std::vector< ParserElement > children;
};

BOOST_FUSION_ADAPT_STRUCT(
    ParserPrefixStruct,
    (std::vector<ParserElement>, children)
);

typedef boost::optional<ParserPrefixStruct> ParserPrefixStructOpt;

// functions
/**
 * \brief Parse string to get a ParserPrefixStruct
 */
ParserPrefixStruct parse_prefix_notation(const std::string& str);


#endif
