/*
 * PDDL file reader and parser
 * Copyright (C) 2014  David Martínez <dmartinez@iri.upc.edu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "pddl_parser.h"
#include <parsers/prefix_notation_parser.h>
#include <parsers/rddl_reward_parser.h>

#define PDDL_KEY_DEFINE       "define"
#define PDDL_KEY_PROBLEM      ":problem"
#define PDDL_KEY_DOMAIN       ":domain"
#define PDDL_KEY_REQUIREMENTS ":requirements"
#define PDDL_KEY_TYPES        ":types"
#define PDDL_KEY_PREDICATES   ":predicates"
#define PDDL_KEY_ACTION       ":action"
#define PDDL_KEY_OBJECTS      ":objects"
#define PDDL_KEY_CONSTANTS    ":constants" // WARNING, not standard!
#define PDDL_KEY_INIT         ":init"
#define PDDL_KEY_GOAL         ":goal"
#define PDDL_KEY_GOAL_REWARD  ":goal-reward"
#define PDDL_KEY_RDDL_REWARD  ":reward" // WARNING, not standard!
#define PDDL_KEY_METRIC       ":metric"

#define PDDL_ELEMENT_NEGATION      "not"
#define PDDL_ELEMENT_AND           "and"
#define PDDL_ELEMENT_EXISTS        "exists"
#define PDDL_ELEMENT_PROBABILISTIC "probabilistic"
#define PDDL_ELEMENT_WHEN          "when"
#define PDDL_ELEMENT_INCREASE      "increase"
#define PDDL_ELEMENT_DECREASE      "decrease"

#define PDDL_ACTION_IDENTIFIER   ":action"
#define PDDL_ACTION_PARAMETERS   ":parameters"
#define PDDL_ACTION_PRECONDITION ":precondition"
#define PDDL_ACTION_EFFECT       ":effect"


namespace ppddl_parser {

/**
 * Declarations
 */
ParserPrefixStructOpt find_domain_field(const std::string& pddl_str, const std::string& pddl_key);

std::vector<std::string> parse_string_list (const ParserPrefixStruct& pddl_reverse_struct);
std::vector<std::string> parse_string_list (const ParserPrefixStruct& pddl_reverse_struct, std::vector<ParserElement>::const_iterator elem_it);
void parse_typed_objects (const ParserPrefixStruct& pddl_reverse_struct, 
                          std::vector<ParserElement>::const_iterator elem_it, 
                          std::vector<PPDDL::FullObject>& ppddl_objects);

PPDDLObjectManager<PPDDL::FullObject>      parse_pddl_object_list (const ParserPrefixStruct& pddl_reverse_struct);

NonTypedAtom          parse_atom       (const ParserPrefixStruct& pddl_reverse_struct);
TypedAtom             parse_typed_atom (const ParserPrefixStruct& pddl_reverse_struct);
Predicate<TypedAtom>  parse_predicate    (const ParserPrefixStruct& pddl_reverse_struct, const PPDDLObjectManager<PPDDL::FullObject>& object_manager);
PredicateGroup<TypedAtom> parse_preconditions(const ParserPrefixStruct& pddl_reverse_struct, PPDDLObjectManager<PPDDL::FullObject>& object_manager );
PredicateGroup<TypedAtom> parse_predicate_group(const ParserPrefixStruct& pddl_reverse_struct, const PPDDLObjectManager<PPDDL::FullObject>& object_manager);

OutcomeGroup<TypedAtom> parse_outcome       (const ParserPrefixStruct& pddl_reverse_struct, const PPDDLObjectManager<PPDDL::FullObject>& object_manager);
OutcomeGroup<TypedAtom> parse_outcome_group (const ParserPrefixStruct& pddl_reverse_struct, const PPDDLObjectManager<PPDDL::FullObject>& object_manager);
Goal::Ptr              parse_goal      (const ParserPrefixStruct& pddl_reverse_struct, PPDDLObjectManager<PPDDL::FullObject>& object_manager);
std::vector<Goal::Ptr> parse_goal_group(const ParserPrefixStruct& pddl_reverse_struct, PPDDLObjectManager<PPDDL::FullObject>& object_manager);
Typed::RulePtr parse_symbolic_rule   (const ParserPrefixStruct& pddl_reverse_struct);
Typed::RuleSet parse_symbolic_ruleset(const ParserPrefixStruct& pddl_reverse_struct);

PredicateGroup<TypedAtom> parse_constants_from_problem_file(const ParserPrefixStruct& pddl_reverse_struct, const PPDDLObjectManager<PPDDL::FullObject>& object_manager);


/**
 * Function implementations
 */


ParserPrefixStructOpt find_domain_field(const std::string& pddl_str, const std::string& pddl_key)
{
    ParserPrefixStruct pddl_reverse_struct = parse_prefix_notation(pddl_str);
    
    std::vector<ParserElement>::const_iterator elem_it = pddl_reverse_struct.children.begin();
    if (!(boost::get<std::string>(*elem_it) == PDDL_KEY_DEFINE)) { // (define (problem/domain
        throw PrefixNotationExceptionIgnorable("PDDL file type not defined");
    }
    ++elem_it;
    // ( [problem/domain] [problem_name/domain_name] )
    // boost::get<PrefixReverseStruct>(*elem_it); 
    
    for(++elem_it ; elem_it != pddl_reverse_struct.children.end(); ++elem_it) {
        ParserPrefixStruct pddl_field = boost::get<ParserPrefixStruct>(*elem_it); 
        if (boost::get<std::string>(pddl_field.children.front()) == pddl_key) {
            return ParserPrefixStructOpt(pddl_field);
        }
    }
    return ParserPrefixStructOpt();
}

NonTypedAtom parse_atom (const ParserPrefixStruct& pddl_reverse_struct)
{
    std::string name;
    std::vector<std::string> param_names;
    
    std::vector<ParserElement>::const_iterator elem_it = pddl_reverse_struct.children.begin();
    name = boost::get<std::string>(*elem_it); // get first element
    
    for(++elem_it; elem_it != pddl_reverse_struct.children.end(); ++elem_it) {
        if (boost::get<std::string>(*elem_it) == "-") { // was not a properly defined Atom
            throw PrefixNotationParserException("Couldn't parse Atom \"" + name + "\", it should be a NonTypedAtom but it is a TypedAtom.");
        }   
        param_names.push_back(boost::get<std::string>(*elem_it));
    }
    
    NonTypedAtom atom(name, param_names);
    
//     LOG(DEBUG) << "Atom is " << atom;
    
    return atom;
}

std::vector<std::string> parse_string_list (const ParserPrefixStruct& pddl_reverse_struct)
{
    return parse_string_list(pddl_reverse_struct, pddl_reverse_struct.children.begin());
}

std::vector<std::string> parse_string_list (const ParserPrefixStruct& pddl_reverse_struct, std::vector<ParserElement>::const_iterator elem_it)
{
    std::vector<std::string> result;
    for(; elem_it != pddl_reverse_struct.children.end(); ++elem_it) {
        result.push_back(boost::get<std::string>(*elem_it));
    }
    return result;
}

void parse_typed_objects (const ParserPrefixStruct& pddl_reverse_struct, 
                          std::vector<ParserElement>::const_iterator elem_it, 
                          std::vector<PPDDL::FullObject>& ppddl_params)
{
    std::vector<std::string> current_objects;
    while (elem_it != pddl_reverse_struct.children.end()) {
        std::string current_elem = boost::get<std::string>(*elem_it);
        if (current_elem != "-") {
            current_objects.push_back(current_elem);
            ++elem_it;
        }
        else {
            std::string  current_type = boost::get<std::string>(*(elem_it+1));
            
            BOOST_FOREACH(std::string object, current_objects) {
                ppddl_params.push_back(PPDDL::FullObject(object, current_type));
            }
            
            current_objects.clear();
            elem_it = elem_it + 2;
        }
    }
    if (!current_objects.empty()) { // was not a properly defined TypedAtom
        throw PrefixNotationParserException("TypedAtom");
    }
}

TypedAtom parse_typed_atom(const ParserPrefixStruct& pddl_reverse_struct)
{
    std::string name;
    std::vector<PPDDL::FullObject> ppddl_params;
    std::vector<ParserElement>::const_iterator elem_it = pddl_reverse_struct.children.begin();
    name = boost::get<std::string>(*elem_it); // get first element
    parse_typed_objects(pddl_reverse_struct, ++elem_it, ppddl_params);
    
    TypedAtom typed_atom(name, ppddl_params);
    
//     LOG(DEBUG) << "TypedAtom is " << typedatom;
    
    return typed_atom;
}

Predicate<TypedAtom> parse_predicate (const ParserPrefixStruct& pddl_reverse_struct, const PPDDLObjectManager<PPDDL::FullObject>& object_manager)
{
    bool positive;
    NonTypedAtom nontyped_atom;
    std::vector<ParserElement>::const_iterator elem_it = pddl_reverse_struct.children.begin();
    
    if (boost::get<std::string>(*elem_it) == PDDL_ELEMENT_NEGATION) { // (not (pred1))
        positive = false;
        nontyped_atom = parse_atom(boost::get<ParserPrefixStruct>(*(elem_it+1)));
    }
    else { // (pred1)
        positive = true;
        nontyped_atom = parse_atom(boost::get<ParserPrefixStruct>(pddl_reverse_struct));
    }
    Predicate<TypedAtom> predicate(object_manager.get_typed_atom(nontyped_atom), positive);
    
//     LOG(DEBUG) << "Predicate<Atom> is " << predicate;
    return predicate;
}

PredicateGroup<TypedAtom> parse_predicate_group (const ParserPrefixStruct& pddl_reverse_struct, const PPDDLObjectManager<PPDDL::FullObject>& object_manager)
{
    PredicateGroup<TypedAtom> predicates;
    std::vector<ParserElement>::const_iterator elem_it = pddl_reverse_struct.children.begin();
    std::string first_element = boost::get<std::string>(*elem_it);
    if ( (first_element == PDDL_ELEMENT_AND) || // (and (pred1) (pred2) ... )
         (first_element == PDDL_KEY_INIT)    || // (:init (pred1) (pred2) ... )
         (first_element == PDDL_KEY_CONSTANTS) )  // (:constants (pred1) (pred2) ... )
    {
        for(++elem_it; elem_it != pddl_reverse_struct.children.end(); ++elem_it) {
            predicates.insert(parse_predicate(boost::get<ParserPrefixStruct>(*elem_it), object_manager));
        }
    }
    else { // (pred1)
        predicates.insert(parse_predicate(pddl_reverse_struct, object_manager));
    }
    
//     LOG(DEBUG) << "Predicates is " << predicates;
    return predicates;
}

PredicateGroup<TypedAtom> parse_preconditions(const ParserPrefixStruct& pddl_reverse_struct,
                                              PPDDLObjectManager<PPDDL::FullObject>& object_manager)
{
    PredicateGroup<TypedAtom> predicates;
    std::vector<ParserElement>::const_iterator elem_it = pddl_reverse_struct.children.begin();
    std::string first_element = boost::get<std::string>(*elem_it);
    if (first_element == PDDL_ELEMENT_EXISTS) { // (exits (var1 var2 - type1) (and (pred1) (pred2) ... ) )
        // (exits (var1 var2 - type1) ...
        ++elem_it;
        std::vector<PPDDL::FullObject> deictic_vars;
        ParserPrefixStruct deictic_pddl_struct = boost::get<ParserPrefixStruct>(*elem_it);
        parse_typed_objects(deictic_pddl_struct, deictic_pddl_struct.children.begin(), deictic_vars);
        object_manager.add_objects(deictic_vars.begin(), deictic_vars.end());
        // (exits ... (and (pred1) (pred2) ) )
        ++elem_it;
        predicates.add_predicates(parse_predicate_group(boost::get<ParserPrefixStruct>(*elem_it), object_manager));
    }
    else {
        predicates = parse_predicate_group(pddl_reverse_struct, object_manager);
    }
    
//     LOG(DEBUG) << "Preconditions are " << predicates;
    return predicates;
}

OutcomeGroup<TypedAtom> parse_outcome (const ParserPrefixStruct& pddl_reverse_struct, const PPDDLObjectManager<PPDDL::FullObject>& object_manager)
{
    OutcomeGroup<TypedAtom> outcomes;
    std::vector<ParserElement>::const_iterator elem_it = pddl_reverse_struct.children.begin();
    if (boost::get<std::string>(*elem_it) == PDDL_ELEMENT_WHEN) { // (when ...)
        // ignoring
    }
    else if (boost::get<std::string>(*elem_it) == PDDL_ELEMENT_PROBABILISTIC) { // (probabilistic 0.5 (pred_group))
        for (++elem_it; elem_it != pddl_reverse_struct.children.end(); ++elem_it) {
            float probability = boost::lexical_cast<float>(boost::get<std::string>(*elem_it));
            elem_it++;
            PredicateGroup<TypedAtom> predicates = parse_predicate_group(boost::get<ParserPrefixStruct>(*elem_it), object_manager);
            outcomes.insert(Outcome<TypedAtom>(predicates, probability));
        }
        outcomes.add_empty_outcome_to_fill_probability();
    }
    else { // (pred1)
        outcomes.insert(Outcome<TypedAtom>(parse_predicate_group(pddl_reverse_struct, object_manager), 1.0));
    }
    
//     LOG(DEBUG) << "PPDDL outcome is " << outcomes;
    
    return outcomes;
}

OutcomeGroup<TypedAtom> parse_outcome_group (const ParserPrefixStruct& pddl_reverse_struct, const PPDDLObjectManager<PPDDL::FullObject>& object_manager)
{
    OutcomeGroup<TypedAtom> nid_outcomes;
    std::vector<ParserElement>::const_iterator elem_it = pddl_reverse_struct.children.begin();
    if (boost::get<std::string>(*elem_it) == PDDL_ELEMENT_AND) { // (and (out1) (out2))
        nid_outcomes.insert(Outcome<TypedAtom>(1.0)); // add empty outcome with prob = 1.0 to start
        for(++elem_it; elem_it != pddl_reverse_struct.children.end(); ++elem_it) {
            OutcomeGroup<TypedAtom> pddl_outcomes = parse_outcome(boost::get<ParserPrefixStruct>(*elem_it), object_manager);
            // assert(sum of probabilities == 1);
            if (pddl_outcomes.size() == 1) {
                BOOST_FOREACH(const Predicate<TypedAtom>& new_pddl_pred, *pddl_outcomes.begin()) {
                    nid_outcomes.add_predicate_to_outcomes(new_pddl_pred);
                }
            }
            else if (pddl_outcomes.size() > 1) {
                OutcomeGroup<TypedAtom> new_nid_outcomes;
                BOOST_FOREACH(const Outcome<TypedAtom>& pddl_outcome, pddl_outcomes) {
                    OutcomeGroup<TypedAtom> copy_nid_outcomes;
                    BOOST_FOREACH(Outcome<TypedAtom> nid_outcome, nid_outcomes) {
                        nid_outcome.probability *= pddl_outcome.probability;
                        nid_outcome.add_predicates(pddl_outcome);
                        copy_nid_outcomes.insert(nid_outcome);
                    }
                    new_nid_outcomes.add_new_outcomes(copy_nid_outcomes);
                }
                nid_outcomes = new_nid_outcomes;
            }
        }
    }
    else { // (out1)
        OutcomeGroup<TypedAtom> outcomes = parse_outcome(pddl_reverse_struct, object_manager);
        nid_outcomes.add_new_outcomes(outcomes);
    }
    
//     LOG(DEBUG) << "Outcomes are " << nid_outcomes;
    return nid_outcomes;
}

Goal::Ptr parse_goal (const ParserPrefixStruct& pddl_reverse_struct, PPDDLObjectManager<PPDDL::FullObject>& object_manager)
{
    Goal::Ptr goal_ptr;
    
    std::vector<ParserElement>::const_iterator elem_it = pddl_reverse_struct.children.begin();
    if (boost::get<std::string>(*elem_it) == PDDL_ELEMENT_WHEN) { // (when (pred_group) (decrease/increase (reward) 1.0))
        elem_it++;
        PredicateGroup<TypedAtom> goal_predicates;
        goal_predicates = parse_preconditions(boost::get<ParserPrefixStruct>(*elem_it), object_manager);
        
        elem_it++;
        ParserPrefixStruct reward_pddl_struct = boost::get<ParserPrefixStruct>(*elem_it);
        std::vector<ParserElement>::const_iterator reward_pddl_struct_it = reward_pddl_struct.children.begin();
        std::string increase_or_decrease = boost::get<std::string>(*reward_pddl_struct_it);
        reward_pddl_struct_it++;
        // assert ("reward")
        std::string reward_string = boost::get<std::string>(boost::get<ParserPrefixStruct>(*reward_pddl_struct_it).children.front());
        assert(reward_string == "reward");
        // reward
        reward_pddl_struct_it++;
        float reward = boost::lexical_cast<float>(boost::get<std::string>(*reward_pddl_struct_it));
        
        if (increase_or_decrease == PDDL_ELEMENT_INCREASE) {
            goal_ptr = Goal::Ptr(new Goal(goal_predicates, reward));
        }
        else if (increase_or_decrease == PDDL_ELEMENT_DECREASE) {
            reward *= -1;
            goal_ptr = Goal::Ptr(new Goal(goal_predicates, reward));
        }
        else {
            LOG(WARNING) << "Failed to read PDDL goal";
        }
    }
    
    return goal_ptr;
}

std::vector<Goal::Ptr> parse_goal_group (const ParserPrefixStruct& pddl_reverse_struct, PPDDLObjectManager<PPDDL::FullObject>& object_manager)
{
    std::vector<Goal::Ptr> goals;
    std::vector<ParserElement>::const_iterator outcome_it = pddl_reverse_struct.children.begin();
    if (boost::get<std::string>(*outcome_it) == PDDL_ELEMENT_AND) { // (and (out1) (out2) ... (when reward1) (when reward2) ...)
        for(++outcome_it; outcome_it != pddl_reverse_struct.children.end(); ++outcome_it) {
            Goal::Ptr goal_ptr = parse_goal(boost::get<ParserPrefixStruct>(*outcome_it), object_manager);
            if (goal_ptr) {
                goals.push_back(goal_ptr);
            }
        }
    }
    else { // (out1)
        Goal::Ptr goal_ptr = parse_goal(pddl_reverse_struct, object_manager);
        if (goal_ptr) {
            goals.push_back(goal_ptr);
        }
    }
    
//     LOG(DEBUG) << "Goals are " << goals;
    return goals;
}

Typed::RulePtr parse_symbolic_rule (const ParserPrefixStruct& pddl_reverse_struct)
{
    std::string action_name;
    std::vector<PPDDL::FullObject> ppddl_params;
    PPDDLObjectManager<PPDDL::FullObject> ppddl_object_manager;
//     std::vector<PPDDL::FullObject> ppddl_vars; // includes deictic references
    PredicateGroup<TypedAtom> preconditions;
    OutcomeGroup<TypedAtom> outcomes;
    std::vector<Goal::Ptr> goals;
    std::vector<ParserElement>::const_iterator elem_it = pddl_reverse_struct.children.begin();
    if (!(boost::get<std::string>(*elem_it) == PDDL_ACTION_IDENTIFIER)) { // (:action action_name ...
        throw PrefixNotationExceptionIgnorable("SymbolicRule");
    }
    ++elem_it;
    action_name = boost::get<std::string>(*elem_it);
    
    for(++elem_it; elem_it != pddl_reverse_struct.children.end(); elem_it+=2) {
        ParserPrefixStruct params_pddl_struct = boost::get<ParserPrefixStruct>(*(elem_it+1));
        switch (enum_hash(boost::get<std::string>(*elem_it))) {
            case ePDDL_ACTION_PARAMETERS:
                try {
                    parse_typed_objects(params_pddl_struct, params_pddl_struct.children.begin(), ppddl_params);
                    ppddl_object_manager.add_objects(ppddl_params.begin(), ppddl_params.end());
                }
                catch (boost::bad_get& e) {
                    LOG(WARNING) << "Error reading rule " << action_name << " parameters: " << e.what();
                }
                catch (PrefixNotationParserException const& ex) {
                    LOG(ERROR) << "Exception when reading rule action parameters: " << ex.what();
                    throw ex;
                }
                break;
            case ePDDL_ACTION_PRECONDITION:
                try {
                    preconditions = parse_preconditions(params_pddl_struct, ppddl_object_manager);
                }
                catch (boost::bad_get& e) {
                    LOG(WARNING) << "Error reading rule " << action_name << " preconditions: " << e.what();
                }
                catch (PrefixNotationParserException const& ex) {
                    LOG(ERROR) << "Exception when reading rule preconditions: " << ex.what();
                    throw ex;
                }
                break;
                
            case ePDDL_ACTION_EFFECT:
                try {
                    try {
                        outcomes = parse_outcome_group(params_pddl_struct, ppddl_object_manager);
                    }
                    catch (PrefixNotationParserException const& ex) {
                        LOG(ERROR) << "Exception when reading rule effects: " << ex.what();
                        throw ex;
                    }
                    try {
                        goals = parse_goal_group(params_pddl_struct, ppddl_object_manager);
                    }
                    catch (PrefixNotationParserException const& ex) {
                        LOG(ERROR) << "Exception when reading rule goals: " << ex.what();
                        throw ex;
                    }
                }
                catch (boost::bad_get& e) {
                    LOG(WARNING) << "Error reading rule " << action_name << " effects: " << e.what();
                }
                break;
                
            default:
                LOG(WARNING) << "Didn't recognize action element: " << boost::get<std::string>(*elem_it);
                break;
        }
    }
    
    RewardFunctionGroup reward_function_group;
    BOOST_FOREACH(Goal::Ptr& goal_ptr, goals) {
        goal_ptr->add_predicates(preconditions);
        reward_function_group.add_reward_function(*goal_ptr);
    }
    
    #if __cplusplus > 199711L // C++11
    Typed::RulePtr resulting_rule(new Typed::SymbolicRule(TypedAtom(action_name, ppddl_params), preconditions, outcomes, reward_function_group));
    #else
    Typed::RulePtr resulting_rule = Typed::RulePtr(boost::shared_ptr<Typed::SymbolicRule>(new Typed::SymbolicRule(TypedAtom(action_name, ppddl_params), preconditions, outcomes, reward_function_group)));
    #endif
    
    return resulting_rule;
}

Typed::RuleSet parse_symbolic_ruleset(const ParserPrefixStruct& pddl_reverse_struct)
{
    Typed::RuleSet rules;
    
    BOOST_FOREACH(const ParserElement& pddl_element, pddl_reverse_struct.children) {
        try {
            ParserPrefixStruct pddl_possible_rule_element;
            try {
                pddl_possible_rule_element = boost::get<ParserPrefixStruct>(pddl_element);
            }
            catch (boost::bad_get const& ex) {
                // We are using this parser to read the whole domain file
                // Ignoring not PrefixReverseStruct elements. Example: the first "(define" field
                throw PrefixNotationExceptionIgnorable("Ignoring non-recursive element");
            }
            
            Typed::RulePtr rule = parse_symbolic_rule(pddl_possible_rule_element);
            rules.push_back(rule);
        }
        catch (PrefixNotationParserException const& ex) {
            LOG(ERROR) << "Exception when reading rule set: " << ex.what();
        }
        catch (PrefixNotationExceptionIgnorable const& ex) {
            // expected as we are passing all types of things, not just actions
            // LOG(ERROR) << "Expected exception when reading rule set: " << ex.what();
        }
    }
    return rules;
}

PPDDLObjectManager<PPDDL::FullObject> parse_pddl_object_list(const ParserPrefixStruct& pddl_reverse_struct)
{
    PPDDLObjectManager<PPDDL::FullObject> pddl_object_manager;
    std::vector<PPDDL::FullObject> ppddl_objects;
    // skip first element ":objects"
    parse_typed_objects(pddl_reverse_struct, pddl_reverse_struct.children.begin()+1, ppddl_objects);
    
    pddl_object_manager.add_objects(ppddl_objects.begin(), ppddl_objects.end());
    return pddl_object_manager;
}

PredicateGroup<TypedAtom> parse_constants_from_problem_file(const ParserPrefixStruct& pddl_reverse_struct, const PPDDLObjectManager<PPDDL::FullObject>& object_manager)
{
    PredicateGroup<TypedAtom> constants = parse_predicate_group(pddl_reverse_struct, object_manager);
    BOOST_FOREACH(const Predicate<TypedAtom>& constant, constants) { constant.set_constant(); }
    return constants;
}

action_string_code enum_hash (std::string const& str) {
    if (str == PDDL_ACTION_PARAMETERS) 
        return ePDDL_ACTION_PARAMETERS;
    else if (str == PDDL_ACTION_PRECONDITION) 
        return ePDDL_ACTION_PRECONDITION;
    else if (str == PDDL_ACTION_EFFECT) 
        return ePDDL_ACTION_EFFECT;
    else if (str == PDDL_KEY_DEFINE) 
        return ePDDL_KEY_DEFINE;
    else if (str == PDDL_KEY_DOMAIN) 
        return ePDDL_KEY_DOMAIN;
    else if (str == PDDL_KEY_GOAL) 
        return ePDDL_KEY_GOAL;
    else if (str == PDDL_KEY_GOAL_REWARD) 
        return ePDDL_KEY_GOAL_REWARD;
    else if (str == PDDL_KEY_INIT) 
        return ePDDL_KEY_INIT;
    else if (str == PDDL_KEY_METRIC) 
        return ePDDL_KEY_METRIC;
    else if (str == PDDL_KEY_OBJECTS) 
        return ePDDL_KEY_OBJECTS;
    else if (str == PDDL_KEY_PREDICATES) 
        return ePDDL_KEY_PREDICATES;
    else if (str == PDDL_KEY_PROBLEM) 
        return ePDDL_KEY_PROBLEM;
    else if (str == PDDL_KEY_REQUIREMENTS) 
        return ePDDL_KEY_REQUIREMENTS;
    else if (str == PDDL_KEY_TYPES) 
        return ePDDL_KEY_TYPES;
    else
        return ePDDL_UNKNOWN;
}

NonTypedAtom read_atom (const std::string& str)
{
    return parse_atom(parse_prefix_notation(str));
}

TypedAtom read_typed_atom (const std::string& str)
{
    return parse_typed_atom(parse_prefix_notation(str));
}

Predicate<TypedAtom> read_predicate (const std::string& str, const PPDDLObjectManager<PPDDL::FullObject>& object_manager)
{
    return parse_predicate(parse_prefix_notation(str), object_manager);
}

PredicateGroup<TypedAtom> read_predicate_group (const std::string& str, const PPDDLObjectManager<PPDDL::FullObject>& object_manager)
{
    return parse_predicate_group(parse_prefix_notation(str), object_manager);
}

OutcomeGroup<TypedAtom> read_outcome_group (const std::string& str, const PPDDLObjectManager<PPDDL::FullObject>& object_manager)
{
    return parse_outcome_group(parse_prefix_notation(str), object_manager);
}

Typed::RulePtr read_symbolic_rule (const std::string& str)
{
    return parse_symbolic_rule(parse_prefix_notation(str));
}

Typed::RuleSet read_symbolic_ruleset(const std::string& str)
{
    return parse_symbolic_ruleset(parse_prefix_notation(str));
}

PPDDLObjectManager<PPDDL::FullObject> read_objects_from_simple_string(const std::string& str)
{
    return parse_pddl_object_list(parse_prefix_notation(str));
}

PPDDLObjectManager<PPDDL::FullObject> read_objects_from_problem_file(const std::string& str)
{
    ParserPrefixStructOpt pddl_reverse_struct_opt = find_domain_field(str, PDDL_KEY_OBJECTS);
    return parse_pddl_object_list(*pddl_reverse_struct_opt);
}

PredicateGroup<TypedAtom> read_constants_from_problem_file(const std::string& str, const PPDDLObjectManager<PPDDL::FullObject>& object_manager)
{
    ParserPrefixStructOpt pddl_reverse_struct_opt = find_domain_field(str, PDDL_KEY_CONSTANTS);
    if (!pddl_reverse_struct_opt) {
        LOG(ERROR) << "Couldn't find domain constants!";
        return PredicateGroup<TypedAtom>();
    }
    return parse_constants_from_problem_file(*pddl_reverse_struct_opt, object_manager);
}

State read_state(const std::string& str)
{
    State state;
    
    try {
        ParserPrefixStructOpt pddl_reverse_struct_opt = find_domain_field(str, PDDL_KEY_OBJECTS);
        PPDDLObjectManager<PPDDL::FullObject> object_manager;
        if (pddl_reverse_struct_opt) {
            object_manager = parse_pddl_object_list(*pddl_reverse_struct_opt);
        }
        else {
            LOG(WARNING) << "Objects not found. Add \"(" << PDDL_KEY_OBJECTS << " obj1 <obj2> - type1 ... )\" to the PPDDL problem file.";
        }
        
        pddl_reverse_struct_opt = find_domain_field(str, PDDL_KEY_CONSTANTS);
        PredicateGroup<TypedAtom> constants;
        if (pddl_reverse_struct_opt) {
            constants = parse_constants_from_problem_file(*pddl_reverse_struct_opt, object_manager);
        }
        else {
            LOG(INFO) << "Constants not found. Add \"(" << PDDL_KEY_CONSTANTS << " (literal1) (literal2) ... )\" to the PPDDL problem file.";
        }
        
        pddl_reverse_struct_opt = find_domain_field(str, PDDL_KEY_INIT);
        if (pddl_reverse_struct_opt) {
            state = State( parse_predicate_group(*pddl_reverse_struct_opt, object_manager), object_manager );
            state.set_constants(constants);
        }
        else {
            LOG(WARNING) << "Initial state not found. Add \"(" << PDDL_KEY_INIT << " (literal1) (literal2) ... )\" to the PPDDL problem file.";
        }
    }
    catch (boost::bad_get const& ex) {
        throw PrefixNotationExceptionIgnorable("Couldn't read state (boost::bad_get)");
    }
    
    return state;
}


RewardFunctionGroup read_global_goals(const std::string& str, const PPDDLObjectManager<PPDDL::FullObject>& object_manager)
{
    RewardFunctionGroup goals;
    ParserPrefixStructOpt pddl_reverse_struct_opt;
    
    if ((pddl_reverse_struct_opt = find_domain_field(str, PDDL_KEY_RDDL_REWARD))) {
        ParserPrefixStruct reward_struct = boost::get<ParserPrefixStruct>(*(pddl_reverse_struct_opt->children.begin()+1));
        RDDLGoal rddl_goal = rddl_parser::parse_goal(reward_struct);
        goals.add_reward_function(rddl_goal);
    }
    else {
        float goal_reward = 1.0;
        if ((pddl_reverse_struct_opt = find_domain_field(str, PDDL_KEY_GOAL_REWARD))) {
            // Second element in pddl_reverse_struct_opt is the reward. boost::variant -> std::string -> float
            goal_reward = boost::lexical_cast<float>(boost::get<std::string>(*(pddl_reverse_struct_opt->children.begin()+1)));
        }
        else {
            LOG(WARNING) << "Goal reward not found. Add \"(" << PDDL_KEY_GOAL_REWARD << " <reward> )\" to the PPDDL problem file.";
        }
        
        if ((pddl_reverse_struct_opt = find_domain_field(str, PDDL_KEY_GOAL))) {
            // remove initial pddl key :goal, start in second field
            ParserPrefixStruct goal_predicates = boost::get<ParserPrefixStruct>(*(pddl_reverse_struct_opt->children.begin()+1));
            
            Goal goal( parse_predicate_group(goal_predicates, object_manager), goal_reward );
            if (!goal.empty()) {
                goals.add_reward_function(goal);
            }
        }
        else if ((pddl_reverse_struct_opt = find_domain_field(str, PDDL_KEY_RDDL_REWARD))) {
            ParserPrefixStruct reward_struct = boost::get<ParserPrefixStruct>(*(pddl_reverse_struct_opt->children.begin()+1));
            RDDLGoal rddl_goal = rddl_parser::parse_goal(reward_struct);
            goals.add_reward_function(rddl_goal);
        }
        else {
            LOG(WARNING) << "Goal not found. Add \"(" << PDDL_KEY_GOAL << " (and (literal1) (literal2) ... ) )\" to the PPDDL problem file.";
        }
    }
    
//     LOG(INFO) << "Goals are " << goals;
    return goals;
}

std::vector<PPDDL::FullObject::Type> read_pddl_types(const std::string& str)
{
    ParserPrefixStructOpt pddl_reverse_struct_opt = find_domain_field(str, PDDL_KEY_TYPES);
    if (pddl_reverse_struct_opt) {
        // remove pddl keyword
        pddl_reverse_struct_opt->children.erase(pddl_reverse_struct_opt->children.begin());
        return  parse_string_list(*pddl_reverse_struct_opt);
    }
    else {
        LOG(WARNING) << "Types not found. Add \"(" << PDDL_KEY_TYPES << " type1 type2 ... )\" to the PPDDL domain file.";
    }
    return std::vector<std::string>();
}

} // namespace ppddl_parser


/**
 * \class PDDLFileReader
 */

PDDLFileReader::~PDDLFileReader()
{
    if (pddl_file.is_open()) {
        pddl_file.close();
    }
}

void PDDLFileReader::parse_domain_file(Typed::RuleSet& ruleset) const
{
    std::string pddl_domain_file_content = utils::get_file_contents(file_path);
    
    ruleset = ppddl_parser::read_symbolic_ruleset(pddl_domain_file_content);
}

void PDDLFileReader::parse_problem_file(State& state, RewardFunctionGroup& goals) const
{
    std::string pddl_domain_file_content = utils::get_file_contents(file_path);
    
    state = ppddl_parser::read_state(pddl_domain_file_content);
    goals = ppddl_parser::read_global_goals(pddl_domain_file_content, state.ppddl_object_manager);
}

void PDDLFileReader::parse_problem_file_objects(PPDDLObjectManager<PPDDL::FullObject>& ppddl_object_manager) const
{
    std::string pddl_domain_file_content = utils::get_file_contents(file_path);
    
    ppddl_object_manager = ppddl_parser::read_objects_from_problem_file(pddl_domain_file_content);
}

void PDDLFileReader::parse_problem_file_constants(PredicateGroup<TypedAtom>& constants, const PPDDLObjectManager<PPDDL::FullObject>& object_manager) const
{
    std::string pddl_domain_file_content = utils::get_file_contents(file_path);
    
    constants = ppddl_parser::read_constants_from_problem_file(pddl_domain_file_content, object_manager);
}

void PDDLFileReader::parse_domain_file(std::vector< PPDDL::FullObject::Type >& types) const
{
    std::string pddl_domain_file_content = utils::get_file_contents(file_path);
    
    types = ppddl_parser::read_pddl_types(pddl_domain_file_content);
}

void PDDLFileReader::parse_transitions_file(TransitionGroup<TypedAtom>& transitions, const PPDDLObjectManager<PPDDL::FullObject>& object_manager)
{
    open_file();
    bool file_finished = false;
    while (!file_finished) {
        boost::optional<std::string> line_prev = read_line();
        boost::optional<std::string> line_action = read_line();
        boost::optional<std::string> line_next = read_line();
        boost::optional<std::string> line_reward = read_line();
        
        if (line_reward) { // checking last one is enough
            PredicateGroup<TypedAtom> transition_pre = ppddl_parser::read_predicate_group(*line_prev, object_manager);
            TypedAtom transition_action = object_manager.get_typed_atom(ppddl_parser::read_atom(*line_action));
            PredicateGroup<TypedAtom> transition_next = ppddl_parser::read_predicate_group(*line_next, object_manager);
            float transition_reward = boost::lexical_cast<float>(*line_reward);
            
            Transition<TypedAtom> transition(transition_pre,
                                             transition_action,
                                             transition_next,
                                             transition_reward,
                                             true
                                            );
            transitions.add_transition(transition);
        }
        else {
            file_finished = true;
        }
    }
    transitions.ppddl_object_manager = object_manager;
}

void PDDLFileReader::open_file()
{
    pddl_file.open(file_path.c_str());
    if (!pddl_file.is_open()) {
        LOG(ERROR) << "Couldn't open file " << file_path;
    }
}

boost::optional<std::string> PDDLFileReader::read_line()
{
    boost::optional<std::string> result;
    
    if (pddl_file.is_open()) {
        std::string line;

        if (pddl_file.good()) {
            getline(pddl_file, line);
        }
        result = line;
    }
    
    return result;
}
