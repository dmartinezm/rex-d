(define (domain triangle)
  (:requirements :typing :strips :equality :probabilistic-effects :rewards)
  (:types location)
  (:predicates (vehicleAt ?loc - location)
               (spareIn ?loc - location)
               (road ?from - location ?to - location)
               (notFlattire) (hasspare))
  (:action moveCar
    :parameters (?from - location ?to - location)
    :precondition (and (vehicleAt ?from) (road ?from ?to) (notFlattire))
    :effect (and (vehicleAt ?to) (not (vehicleAt ?from))
         (probabilistic 0.49 (not (notFlattire)))))
  (:action loadtire
    :parameters (?loc - location)
    :precondition (and (vehicleAt ?loc) (spareIn ?loc))
    :effect (and (hasspare) (not (spareIn ?loc))))
  (:action changetire
    :precondition (hasspare)
    :effect (and (not (hasspare)) (notFlattire)))
  (:action finish_action
    :parameters ()
    :precondition (and (not (finished)))
    :effect (and (finished) 
                (when (vehicleAt l13) (increase (reward) 5.0))
            )
  )

)
