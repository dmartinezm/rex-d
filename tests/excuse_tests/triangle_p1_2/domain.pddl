(define (domain triangle)
  (:requirements :typing :strips :equality :probabilistic-effects :rewards)
  (:types)
  (:predicates                (hasspare )
               (notFlattire )
  )
  (:action moveCar
   :parameters (?X - default ?Y - default)
   :precondition (and (notFlattire) (road ?X ?Y) (vehicleAt ?X) )
   :effect (and (probabilistic  0.593 (and (vehicleAt ?Y) (not (notFlattire)) (not (vehicleAt ?X)) ) 0.407 (and (vehicleAt ?Y) (not (vehicleAt ?X)) ))
 )
  )
  (:action moveCar
   :parameters (?X - default ?Y - default)
   :precondition (and (notFlattire) (road ?X ?Y) (not (vehicleAt ?X)) )
   :effect (and (probabilistic )
 )
  )
  (:action moveCar
   :parameters (?X - default ?Y - default)
   :precondition (and (notFlattire) (not (road ?X ?Y)) )
   :effect (and (probabilistic )
 )
  )
  (:action moveCar
   :parameters (?X - default ?Y - default)
   :precondition (and (not (notFlattire)) )
   :effect (and (probabilistic )
 )
  )
)
