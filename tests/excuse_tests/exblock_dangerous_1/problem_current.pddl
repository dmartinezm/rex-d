(define (problem dangerous)
  (:domain exblock)
  (:objects b1 b3 b4 b6 b7 - box)
  (:init (emptyhand) (onTable b4) (onTable b6) (on b3 b4) (on b7 b3) (not (on b1 b7)) (clear b6) (clear b1) (not (noDetonated b1)) (noDestroyed b1) (noDetonated b3) (noDestroyed b3) (noDetonated b4) (noDestroyed b4) (noDetonated b6) (noDestroyed b6) (noDetonated b7) (noDestroyed b7) (not (noDestroyedTable)) (clear b7) (onTable b1) )
  (:goal (and (onTable b6) (on b1 b3) (on b3 b6) (onTable b7) (onTable b4)) ) 
  (:goal-reward 10) 
  (:metric maximize (reward))
)

