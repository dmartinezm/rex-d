=============
=== TESTS ===
=============

To compile them, you have to enable the in cmake. 
Use "ccmake ." and enable tests in the build directory. Inside ccmake, press 'c' and 'g' to update cmake configuration and exit.

Run them in this folder:
$ ../bin/gtests

Use the argument --gtest_filter=*<keyword>* to execute just tests whose name has the keyword. Example:
$ ../bin/gtests --gtest_filter=*Rule*
