#include "KLF1T.h"

#include <sstream>
#include <iostream>

using namespace std;


// HACK David
list<KRule> KLF1T::k_remember;

//----------------
// Observers
//----------------

//----------------
// Methods
//----------------

    KProgram KLF1T::run_specialization(KInput & input, unsigned int max_conditions)
    {
        KProgram program;
        vector<string> const & variables = input.get_variables();
        vector<vector<string> > const & values = input.get_variables_values();
        
        list<KTrace> previous_traces;
        k_remember = list<KRule>();
        
        // DBG
        //cout << "Initialization..." << endl;
        
        // Initialization
        init_specialization(program,input);
        
        // DBG
        //cout << "Learning..." << endl;
        
        unsigned int analyzed = 0;

        while(!input.finish())
        {
            //DBG
            analyzed++;
            //cerr << analyzed << "/" << input.size() << endl;

            KTrace const & trace = input.get_next_trace();
            
            // DBG
            //cout << "New trace: " << trace.to_string(variables,values) << endl;
            
            // For each 1-step transition
            for(unsigned int state=0;state < trace.size()-1;state++)
            {
                vector<unsigned int> const & I = trace.get(state);
                vector<unsigned int> const & J = trace.get(state+1);
                
                // DBG
                //cout << "Analyze transition: " << state << endl;
                
                // For each variable...
                for(unsigned int variable=0;variable < J.size();variable++)
                {
                    // Except ignored ones
                    if(input.is_ignored(variable))
                    {
                        //DBG
                        //cout << "skip anti-rule of variable " << input.get_variable_name(variable) << endl;
                        continue;
                    }
                    
                    // For each value...
                    for(unsigned int value=0;value < values[variable].size();value++)
                    {
                        // Except the observed one...
                        if(value == KRule::NO_VALUE || value == J[variable])
                            continue;
                        
                        // Specialize each rule that match with I...
                        list<KRule> & rules = program.get(variable,value);
                        for(list<KRule>::iterator rule=rules.begin();rule != rules.end();rule++)
                        {
                            // DBG
                            //cout << "check rule: " << rule->to_string(variables,values) << endl;
                            
                            if(rule->matches(I))
                            {
                                // DBG
                                //cout << "Conflict detected..." <<endl;
                                
                                KRule conflict_rule(*rule);
                                rules.erase(rule);
                                rule--;
                                
                                // HACK David
                                //k_remember.push_back(conflict_rule);

                                //cout << "max cond " << max_conditions << ", rule size " << conflict_rule.size() << endl;

                                if(max_conditions > 0 && conflict_rule.size()+1 > max_conditions)
                                {
                                    //cout << "skip for cond max: " << conflict_rule.to_string(variables,values) << endl;
                                    k_remember.push_back(conflict_rule);
                                    continue;
                                }

                                // DBG
                                //cout << conflict_rule.to_string(variables,values) << " has to be specialized !" << endl;
                                
                                // By adding the negation of I...
                                for(unsigned int i=0;i < I.size();i++)
                                {
                                    // That are not in the rule...
                                    if(conflict_rule.member(i,I[i]))
                                        continue;
                                    
                                    // DBG
                                    //cout << "Can be specialized by adding not " << variables[i] << "=" << values[i][I[i]] << endl;
                                    
                                    // That correspond to all other possible value for the variable...
                                    for(unsigned int other_value=0;other_value < values[i].size();other_value++)
                                    {
                                        if(other_value != KRule::NO_VALUE && other_value != I[i])
                                        {
                                            KRule revised_rule(conflict_rule);
                                            
                                            revised_rule.add_condition(i,other_value);
                                            
                                            // DBG
                                            //cout << "Specialize into: " << revised_rule.to_string(variables,values) << endl;
                                            
                                            if(program.subsumes(revised_rule))
                                            {
                                                // DBG
                                                //cout << "Subsumed !" << endl;
                                                continue;
                                            }
                                            rules.push_back(revised_rule);
                                            
                                            
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            
            previous_traces.push_back(trace);
            
            // DBG
            //cout << program.to_string(variables,values) << endl;
        }
        
        k_remember.unique();
        
        for(list<KRule>::iterator i=k_remember.begin();i != k_remember.end();i++)
        {
            program.add(*i);
        }

        // 3: generates all generalizations
        //---------------------------------
        //DBG
        //cout << merging.to_string(variables,values) << endl;

        for(unsigned int variable=0;variable < variables.size();variable++)
        {
            for(unsigned int value=0;value < values[variable].size();value++)
            {
                list<KRule> & rules = program.get(variable,value);
                for(list<KRule>::iterator rule=rules.begin();rule != rules.end();rule++)
                {
                    list<std::pair<unsigned int, unsigned int> > body = rule->get_body();

                    for(list<std::pair<unsigned int, unsigned int> >::iterator i=body.begin();i != body.end();i++)
                    {
                        KRule generalization(*rule);
                        generalization.remove_condition(i->first);

                        if(!program.member(generalization))
                            rules.push_back(generalization);
                    }
                }
            }
        }
        
        // Compute probabilities
        for(list<KTrace>::iterator i=previous_traces.begin();i != previous_traces.end();i++)
        {
            update_probabilities(program,*i,input);
        }
        
        return program;
    }
    
    KProgram KLF1T::run_generalization(KInput & input)
    {
        KProgram program;
        vector<string> const & variables = input.get_variables();
        vector<vector<string> > const & values = input.get_variables_values();
        
        // DBG
        cout << "Initialization..." << endl;
        
        // Initialization
        program = KProgram(input.get_variables(),input.get_variables_values());
        
        // DBG
        cout << "Learning..." << endl;
        
        while(!input.finish())
        {
            KTrace const & trace = input.get_next_trace();
            
            // DBG
            cout << "New trace: " << trace.to_string(variables,values) << endl;
            
            // For each 1-step transition
            for(unsigned int state=0;state < trace.size()-1;state++)
            {
                vector<unsigned int> const & I = trace.get(state);
                vector<unsigned int> const & J = trace.get(state+1);
                
                // DBG
                cout << "Analyze transition: " << state << endl;
                
                // For each variable...
                for(unsigned int variable=0;variable < J.size();variable++)
                {
                    // for the observed value...
                    unsigned int value = J[variable];
                    
                    // infer a rule R...
                    KRule R(variable,value,variables.size());
                    
                    // with b(R) = I.
                    for(unsigned int var=0;var < I.size();var++)
                        R.add_condition(var,I[var]);
                    
                    //DBG
                    cout << "Infer " << R.to_string(variables,values) << endl;
                    
                    add_rule(R,program,input);
                }
            }
            
            // DBG
            cout << "P={" << program.to_string(variables,values) << "}" << endl;
        }
        
        return program;
    }
    
//-------------------
// Private Functions
//-------------------
    
    void KLF1T::init_specialization(KProgram & program, KInput const & input)
    {   
        // Empty program
        program = KProgram(input.get_variables(),input.get_variables_values());
        
        // Initialize the program with a fact rule for each variable value
        vector<string> const & variables = input.get_variables();
        vector<vector<string> > const & variables_values = input.get_variables_values();
        
        for(unsigned int variable=0;variable < variables.size();variable++)
        {
            // Except ignored ones
            if(input.is_ignored(variable))
            {
                //DBG
                //cout << "skip init of variable " << input.get_variable_name(variable) << endl;
                continue;
            }
            
            for(unsigned int value=0;value < variables_values[variable].size();value++)
            {
                if(value == KRule::NO_VALUE)
                    continue;
                
                KRule rule(variable,value,variables.size());
                program.add(rule);
            }
        }
    }
    
    void KLF1T::add_rule(KRule const & R, KProgram & P, KInput const & input)
    {
        vector<string> const & variables = input.get_variables();
        vector<vector<string> > const & values = input.get_variables_values();
        
        if(P.subsumes(R))
        {
            // DBG
            cout << "Subsumed" << endl;
            return;
        }
        
        // Generalize R by P
        list<KRule> generalizations = P.generalize(R);
        
        //DBG
        cout << R.to_string(variables,values) << " can be generalized into" << endl << "{";
        
        for(list<KRule>::const_iterator i=generalizations.begin();i != generalizations.end();i++)
            cout << i->to_string(variables,values) << endl;
        cout << "}" << endl;
        
        if(generalizations.size() > 0)
        {
            for(list<KRule>::const_iterator i=generalizations.begin();i != generalizations.end();i++)
                add_rule(*i,P,input);
            return;
        }
        
        // Remove from P the rules subsumed by R
        list<KRule> & rules = P.get(R.get_head_variable(),R.get_head_value());
        
        for(list<KRule>::iterator i=rules.begin();i != rules.end();i++)
        {
            if(R.subsumes(*i))
            {
                // HACK David
                k_remember.push_back(*i);
                
                rules.erase(i);
                i--;
            }
        }
        
        rules.push_back(R);
        
        // Generalize P by R
        // TODO ???
    }
    
    void KLF1T::update_probabilities(KProgram & program, KTrace const & trace, KInput const & input)
    {
        vector<string> const & variables = input.get_variables();
        vector<vector<string> > const & values = input.get_variables_values();
        
        // I is all states before J
        vector<unsigned int> const & J = trace.get(1);
        
        //DBG
        //cout << "occ: " << trace.get_occurence() << endl;
        
        for(unsigned int variable=0;variable < variables.size();variable++)
        {
            //DBG
            //cout << "Check rules: " << variable << "="; 
            for(unsigned int value=0;value < values[variable].size();value++)
            {
                if(value == KRule::NO_VALUE)
                    continue;
                
                //DBG
                //cout << value << endl;
                
                list<KRule> & rules = program.get(variable,value);
                
                // Update rules matching counters
                for(list<KRule>::iterator i=rules.begin();i != rules.end();i++)
                {
                    if(i->matches(trace,1))
                    {
                        i->set_matched(i->get_matched()+trace.get_occurence());
                    
                        if(i->get_head_value() == J[i->get_head_variable()])
                            i->set_realized(i->get_realized()+trace.get_occurence());
                    }
                }
            }
        }
    }
