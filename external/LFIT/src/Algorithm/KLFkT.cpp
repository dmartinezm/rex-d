#include "KLFkT.h"

#include <sstream>
#include <iostream>
#include <algorithm>

using namespace std;

// HACK David
list<KRule> KLFkT::k_remember;

//----------------
// Observers
//----------------

//----------------
// Methods
//----------------

    KProgram KLFkT::run(KInput & input)
    {
        // HACK David
        k_remember = list<KRule>();

        vector<string> const & variables = input.get_variables();
        vector<vector<string> > const & values = input.get_variables_values();
                
        list<KTrace> previous_traces;
        vector<KProgram> programs(1,KProgram(variables,values,"P1"));
        
        // DBG
        //cout << "Initialization..." << endl;
        
        // 1) Initialization
        //--------------------
        
        // Initialization: 1-step rules
        programs.push_back(KProgram());
        init(programs[1],input);
        
        // DBG
        //cout << "Learning..." << endl;
       
        // 2) Learning
        //--------------
        
        unsigned int current_delay = 1;
        
        while(!input.finish())
        {
            KTrace const & trace = input.get_next_trace();
            
            // DBG
            //cout << endl;
            //cout << "---------------------------------" << endl;
            //cout << "New trace reveived from input: " << endl << trace.to_string(variables,values) << endl;
            
            // 2.1) Delay check
            //--------------------
            
            unsigned int trace_delay = trace.delay();
            
            //cout << "Trace delay: " << trace_delay << endl;
            
            if(trace_delay > current_delay)
            {
                // 2.1.1) Delay update
                //----------------------
                
                // DBG
                //cout << "Delay conflict detected: current " << current_delay << endl;
                
                for(unsigned int i=current_delay+1;i <= trace_delay;i++)
                {
                    KProgram p;
                    init(p,input,i);
                    programs.push_back(p);
                    
                    // DBG
                    //cout  << "New program: " << p.to_string(variables,values) << endl;
                }
                
                // Learn from begining for these programs
                for(list<KTrace>::iterator i=previous_traces.begin();i != previous_traces.end();i++)
                {
                    // DBG
                    //cout << "Processing previous traces" << endl;
                    // For each new delay considered...
                    for(unsigned int k=current_delay+1;k < trace.size();k++)
                    {
                        // Take the sub-trace of this delay...
                        unsigned int sub_trace_size = k;
                        
                        //DBG
                        //cout << endl;
                        //cout << "---------------------------------" << endl;
                        //cout << "Analyze sub-trace of size " << sub_trace_size << ": " << endl << trace.to_string(variables,values,k) << endl;
                        
                        // For each rules with atmost this delay
                        for(unsigned int rule_delay=current_delay+1;rule_delay <= k && rule_delay < programs.size();rule_delay++)
                        {
                            KProgram & program = programs[rule_delay];
                            
                            // DBG
                            //cout << "(1)Revise program of delay " << rule_delay << endl;
                            
                            learn(input, program, rule_delay, *i, sub_trace_size);
                        }
                    }
                }
                
                // DBG
                //cout << "Increase delay from " << current_delay << " to " << trace_delay << endl;
                current_delay = trace_delay;
            }
            
            // 2.1.2) Trace conflict (no match)
            //----------------------------------
            bool conflict;
            
            do
            {
                conflict = false;
                
                for(unsigned int sub_trace_size=programs.size(); sub_trace_size < trace.size();sub_trace_size++)
                {
                    vector<unsigned int> const & J = trace.get(sub_trace_size);
                    
                    // Check if each variable value can be explain by a rule of the most delayed program
                    for(unsigned int variable=0;variable < J.size();variable++)
                    {
                        KProgram & program = programs.back();
                        
                        list<KRule> & rules = program.get(variable,J[variable]);
                        bool match = false;
                        
                        for(list<KRule>::iterator rule=rules.begin();rule != rules.end();rule++)
                        {
                            if(rule->matches(trace,sub_trace_size))
                            {
                                match = true;
                                break;
                            }
                        }
                        
                        if(!match)
                        {
                            // DBG
                            //cout << "Trace conflict !" << endl;
                            conflict = true;
                            break;
                        }
                    }
                    
                    // Case 1: Increase delay considered
                    if(conflict && input.get_system_type() != KInput::NON_DETERMINISTIC)
                    {
                        current_delay++;
                        
                        KProgram p;
                        init(p,input,current_delay);
                        programs.push_back(p);
                        
                        // Learn from begining for the new program
                        for(list<KTrace>::iterator i=previous_traces.begin();i != previous_traces.end();i++)
                        {
                            // DBG
                            //cout << "Processing previous traces" << endl;
                            // For each sub-trace bigger than the new delay considered...
                            for(unsigned int k=current_delay;k < trace.size();k++)
                            {
                                // Take the sub-trace of this delay...
                                unsigned int sub_trace_size = k;
                                
                                //DBG
                                //cout << endl;
                                //cout << "---------------------------------" << endl;
                                //cout << "Analyze sub-trace of size " << sub_trace_size << ": " << endl << trace.to_string(variables,values,k) << endl;
                                
                                KProgram & program = programs.back();
                                    
                                // DBG
                                //cout << "(2)Revise program of delay " << current_delay << endl;
                                
                                learn(input, program, current_delay, *i, sub_trace_size);
                            }
                        }
                        
                        // DBG
                        //cout  << "New program: " << programs.back().to_string(variables,values) << endl;
                        break;
                    }
                }
            
            }while(conflict);
            
            unsigned int max_delay = trace.size()-1;
            
            // 2.2) Analyze a sub-strace of each delay
            //------------------------------------------
            
            // For each delay considered...
            for(unsigned int k=1;k <= max_delay;k++)
            {
                // Take the sub-trace of this delay...
                unsigned int sub_trace_size = k;
                
                //DBG
                //cout << endl;
                //cout << "---------------------------------" << endl;
                //cout << "Analyze sub-trace of size " << sub_trace_size << ": " << endl << trace.to_string(variables,values,k) << endl;
                
                // For each rules with atmost this delay
                for(unsigned int rule_delay=1;rule_delay <= k && rule_delay < programs.size();rule_delay++)
                {
                    KProgram & program = programs[rule_delay];
                    
                    // DBG
                    //cout << "(3)Revise program of delay " << rule_delay << endl;
                    
                    learn(input, program, rule_delay, trace, sub_trace_size);
                }
                
            }
            
            // DBG
            //for(unsigned int delay=1;delay < programs.size();delay++)
                //cout << "Delay " << delay << ":" << endl << programs[delay].to_string(variables,values) << endl;
            
            previous_traces.push_back(trace);
        }
        
        // 3) Merging
        //-------------
        KProgram merging(programs[1]);
        
        for(unsigned int i=2;i < programs.size();i++)
        {
            // DBG
            //cout << "Merging rule of delay " << i << "..." << endl;
            
            KProgram & program = programs[i];
            
            for(unsigned int variable=0;variable < variables.size();variable++)
            {
                for(unsigned int value=0;value < values[variable].size();value++)
                {
                    list<KRule> const & rules = program.get(variable,value);
                    
                    for(list<KRule>::const_iterator rule=rules.begin();rule != rules.end();rule++)
                    {
                        if(!merging.subsumes(*rule))
                            merging.add(*rule);
                    }
                }
            }
        }

        // HACK David
        // {
            // Compute reverse program

            // Merge and remove redondant rules
            k_remember.unique();

            for(list<KRule>::iterator i=k_remember.begin();i != k_remember.end();i++)
            {
                merging.add(*i);
            }

            // Compute probabilities
            for(list<KTrace>::iterator i=previous_traces.begin();i != previous_traces.end();i++)
            {
                update_probabilities(merging,*i,input);
            }

        // }
        
        return merging;
    }
    
    vector<KProgram> KLFkT::run_non_deterministic(KInput & input, unsigned int delay)
    {
        vector<string> const & variables = input.get_variables();
        vector<vector<string> > const & values = input.get_variables_values();
                
        list<KTrace> previous_traces;
        vector<vector<KProgram> > programs(1, vector<KProgram>(1,KProgram(variables,values,"P1")));
        
        // DBG
        //cout << "Initialization..." << endl;
        
        // 1) Initialization
        //--------------------
        
        // Initialization: 1-step rules
        programs.at(0).push_back(KProgram(variables,values,"P1"));
        init(programs.at(0).at(1),input);
        
        for(unsigned int i=2;i <= delay;i++)
        {
            KProgram p;
            init(p,input,i);
            programs.at(0).push_back(p);
            
            // DBG
            //cout  << "New program: " << p.to_string(variables,values) << endl;
        }
        
        // DBG
        //cout << "Learning..." << endl;
        
        // 2) Learning
        //--------------
        
        while(!input.finish())
        {
            KTrace const & trace = input.get_next_trace();
            
            // DBG
            //cout << endl;
            //cout << "---------------------------------" << endl;
            //cout << "New trace reveived from input: " << endl << trace.to_string(variables,values) << endl;
            
            // 2.1) Delay check
            //--------------------
            
            unsigned int trace_delay = trace.delay();
            
            //cout << "Trace delay: " << trace_delay << endl;
            
            if(trace_delay > delay)
            {
                //cout << "Error: trace delay bigger than " << delay << ": (" << trace.to_string() << ") delay is " << trace_delay << endl;
                exit(0);
            }
            
            // 2.1.2) Trace conflict (no match)
            //----------------------------------
            vector<bool> conflicts(programs.size(),false);
            
            for(unsigned int world=0;world < programs.size();world++)
            {
                KProgram & program = programs.at(world).back();
                bool conflict = false;
                
                // DBG
                //cout << "Check world " << world << ": " << endl;
                //cout << program.to_string(variables,values) << endl;
                
                for(unsigned int sub_trace_size=delay; sub_trace_size < trace.size();sub_trace_size++)
                {
                    vector<unsigned int> const & J = trace.get(sub_trace_size);
                    
                    //DBG
                    //cout << "Check matching of sub-trace " << trace.to_string(variables,values,sub_trace_size) << endl;
                    
                    // Check if each variable value can be explain by a rule of the most delayed program
                    for(unsigned int variable=0;variable < J.size();variable++)
                    {
                        list<KRule> & rules = program.get(variable,J[variable]);
                        bool match = false;
                        
                        for(list<KRule>::iterator rule=rules.begin();rule != rules.end();rule++)
                        {
                            if(rule->matches(trace,sub_trace_size))
                            {
                                //DBG
                                //cout << rule->to_string(variables,values) << " matches " << trace.to_string(variables,values) << endl;
                                match = true;
                                break;
                            }
                            //else
                            //    cout << rule->to_string(variables,values) << " does not match " << trace.to_string(variables,values,sub_trace_size) << endl;
                        }
                        
                        if(!match)
                        {
                            // DBG
                            //cout << "Trace conflict !" << endl;
                            conflict = true;
                            conflicts[world] = true;
                            break;
                        }
                    }
                    
                    // Create a new world
                    if(conflict)
                    {
                        // DBG
                        //cout << "Creation of a new world..." << endl;
                        
                        vector<KProgram> new_world;
                        new_world.reserve(programs.at(world).size());
                        
                        for(unsigned int i=0;i < programs.at(world).size();i++)
                        {
                            new_world.push_back(programs.at(world).at(i));
                        }
                        
                        // Learn the new trace in the new_world
                        //--------------------------------------
                        unsigned int nb_variables = variables.size();
                        
                        // I is all states before J
                        vector<unsigned int> const & J = trace.get(trace.size()-1);
                        
                        for(unsigned int variable=0;variable < J.size();variable++)
                        {
                            KRule rule(variable,J[variable],nb_variables);
                            
                            for(unsigned int state=sub_trace_size-delay;state < sub_trace_size;state++)
                            {
                                for(unsigned int var=0;var < nb_variables;var++)
                                    rule.add_condition(var,trace.get_value(state,var));
                            }
                            
                            // DBG
                            //cout << "New rule: " << rule.to_string(variables,values)  << "(" << rule.to_string() << ")" << endl;
                            
                            // TODO: Check
                            add_rule(rule,new_world.back(),input);
                            
                            //if(!new_world.back().subsumes(rule))
                            //    new_world.back().add(rule);
                            //else
                            //    cout << "SUBSUMED" << endl;
                        }
                        
                        // DBG
                        //cout  << "New program: " << new_world.back().to_string(variables,values) << endl;
                        
                        programs.push_back(new_world);
                        conflicts.push_back(false);
                    }
                    
                    //cout << "PASS: delay " << sub_trace_size << endl;
                }
            }
            
            //DBG
            //cout << "PASS" << endl;
            
            unsigned int max_delay = trace.size()-1;
            
            // 2.2) Analyze a sub-strace of each delay
            //------------------------------------------
            
            // For each delay considered...
            for(unsigned int k=1;k <= max_delay;k++)
            {
                // Take the sub-trace of this delay...
                unsigned int sub_trace_size = k;
                
                //DBG
                //cout << endl;
                //cout << "---------------------------------" << endl;
                //cout << "Analyze sub-trace of size " << sub_trace_size << ": " << endl << trace.to_string(variables,values,k) << endl;
                
                // For each possible world
                for(unsigned int world=0;world < programs.size();world++)
                {
                    // Revise only consistent world
                    if(conflicts[world])
                        continue;
                    
                    // For each rules with atmost this delay
                    for(unsigned int rule_delay=1;rule_delay <= k && rule_delay < programs.at(world).size();rule_delay++)
                    {
                        KProgram & program = programs.at(world).at(rule_delay);
                        
                        // DBG
                        //cout << "(3)Revise program of delay " << rule_delay << endl;
                        
                        learn(input, program, rule_delay, trace, sub_trace_size);
                    }
                }
            }
            
            // DBG
            //for(unsigned int world=0;world < programs.size();world++)
            //    for(unsigned int delay=1;delay < programs.at(world).size();delay++)
            //        cout << "Delay " << delay << ":" << endl << programs.at(world)[delay].to_string(variables,values) << endl;
            
            previous_traces.push_back(trace);
        }
        
        // 3) Merging
        //-------------
        vector<KProgram> worlds;
        
        // For each possible world
        for(unsigned int world=0;world < programs.size();world++)
        {
            KProgram merging(programs.at(world)[1]);
            
            for(unsigned int i=2;i < programs.at(world).size();i++)
            {
                // DBG
                //cout << "Merging rule of delay " << i << "..." << endl;
                
                KProgram & program = programs.at(world)[i];
                
                for(unsigned int variable=0;variable < variables.size();variable++)
                {
                    for(unsigned int value=0;value < values[variable].size();value++)
                    {
                        list<KRule> const & rules = program.get(variable,value);
                        
                        for(list<KRule>::const_iterator rule=rules.begin();rule != rules.end();rule++)
                        {
                            if(!merging.subsumes(*rule))
                                merging.add(*rule);
                        }
                    }
                }
            }
            
            worlds.push_back(merging);
        }
        
        return worlds;
    }
    
    KProgram KLFkT::run_probabilistic(KInput & input)
    {
        vector<string> const & variables = input.get_variables();
        vector<vector<string> > const & values = input.get_variables_values();
                
        list<KTrace> previous_traces;
        vector<vector<KProgram> > programs(1, vector<KProgram>(1,KProgram(variables,values,"P1")));
        
        // DBG
        //cout << "Initialization..." << endl;
        
        // 1) Initialization
        //--------------------
        
        // Initialization: 1-step rules
        programs.at(0).push_back(KProgram(variables,values,"P1"));
        init(programs.at(0).at(1),input);
        
        //for(unsigned int i=2;i <= delay;i++)
        //{
            //KProgram p;
            //init(p,input,2);
            //programs.at(0).push_back(p);
            
            // DBG
            //cout  << "New program: " << p.to_string(variables,values) << endl;
        //}
        
        // DBG
        //cout << "Learning..." << endl;
        
        // 2) Learning
        //--------------
        
        while(!input.finish())
        {
            KTrace const & trace = input.get_next_trace();
            
            // DBG
            cout << endl;
            cout << "---------------------------------" << endl;
            cout << "New trace reveived from input (" << previous_traces.size() <<"): " << endl << trace.to_string(variables,values) << endl;
            
            /*/ 2.1) Delay check
            //--------------------
            
            unsigned int trace_delay = trace.delay();
            
            cout << "Trace delay: " << trace_delay << endl;
            
            if(trace_delay > delay)
            {
                cout << "Error: trace delay bigger than " << delay << ": (" << trace.to_string() << ") delay is " << trace_delay << endl;
                exit(0);
            }*/
            
            // 2.1.2) Trace conflict (no match)
            //----------------------------------
            
            unsigned int conflict = 0;
            bool realized = false;
            
            // HACK
            unsigned int delay=1;
            unsigned int sub_trace_size=1;
            vector<unsigned int> const & J = trace.get(sub_trace_size);
            
            for(unsigned int world=0;world < programs.size();world++)
            {
                KProgram & program = programs.at(world).back();
                bool all_match = true;
                
                // DBG
                cout << "Check world " << world << ": " << endl;
                cout << program.to_string(variables,values,true) << endl;
                
                //DBG
                cout << "Check matching of trace " << trace.to_string(variables,values,sub_trace_size) << endl;
                
                // Check if each variable value can be explain by a rule of the most delayed program
                for(unsigned int variable=0;variable < J.size();variable++)
                {
                    list<KRule> & rules = program.get(variable,J[variable]);
                    bool match = false;
                    
                    for(list<KRule>::iterator rule=rules.begin();rule != rules.end();rule++)
                    {
                        if(rule->matches(trace,sub_trace_size))
                        {
                            //DBG
                            cout << rule->to_string(variables,values,true) << " matches " << trace.to_string(variables,values) << endl;
                            match = true;
                            break;
                        }
                        else
                            cout << rule->to_string(variables,values) << " does not match " << trace.to_string(variables,values,sub_trace_size) << endl;
                    }
                    
                    if(!match)
                    {
                        // DBG
                        cout << "Trace conflict !" << endl;
                        conflict = world;
                        all_match = false;
                        break;
                    }
                }
                
                if(all_match)
                {
                    realized = true;
                    break;
                }
            }
            
            // Check if injection is possible
            // TODO:
            // - calculer un vecteur des transition realiser sur cet etat
            // - avec un mapping sur la list des program faisant cette transition
            // - Si une transition à plusieurs program qui la realise, on inject dans l'un d'eux
            // - Sinon creation d'un nouveau programme
                    
            // Create a new world
            if(!realized)
            {
                // DBG
                cout << "Creation of a new world (" << programs.size() << ")..." << endl;
                
                vector<KProgram> new_world;
                new_world.reserve(programs.at(conflict).size());
                
                for(unsigned int i=0;i < programs.at(conflict).size();i++)
                {
                    new_world.push_back(programs.at(conflict).at(i));
                }
                
                // Learn the new trace in the new_world
                //--------------------------------------
                unsigned int nb_variables = variables.size();
                
                // I is all states before J
                vector<unsigned int> const & J = trace.get(trace.size()-1);
                
                for(unsigned int variable=0;variable < J.size();variable++)
                {
                    KRule rule(variable,J[variable],nb_variables);
                    
                    for(unsigned int state=sub_trace_size-delay;state < sub_trace_size;state++)
                    {
                        for(unsigned int var=0;var < nb_variables;var++)
                            rule.add_condition(var,trace.get_value(state,var));
                    }
                    
                    // DBG
                    //cout << "New rule: " << rule.to_string(variables,values,true)  << "(" << rule.to_string() << ")" << endl;
                    
                    // TODO: Check
                    add_rule(rule,new_world.back(),input);
                    
                    //if(!new_world.back().subsumes(rule))
                    //    new_world.back().add(rule);
                    //else
                    //    cout << "SUBSUMED" << endl;
                }
                
                // DBG
                //cout  << "New program: " << new_world.back().to_string(variables,values) << endl;
                
                programs.push_back(new_world);
            }
                    
                    //cout << "PASS: delay " << sub_trace_size << endl;
                //}
            
            
            //DBG
            //cout << "PASS" << endl;
            
            unsigned int max_delay = trace.size()-1;
            
            // 2.2) Analyze a sub-strace of each delay
            //------------------------------------------
            
            // For each delay considered...
            for(unsigned int k=1;k <= max_delay;k++)
            {
                // Take the sub-trace of this delay...
                unsigned int sub_trace_size = k;
                
                //DBG
                //cout << endl;
                //cout << "---------------------------------" << endl;
                //cout << "Analyze sub-trace of size " << sub_trace_size << ": " << endl << trace.to_string(variables,values,k) << endl;
                
                // For each possible world
                for(unsigned int world=0;world < programs.size();world++)
                {
                    // Revise only consistent world
                    //if(world=conflict)
                    //    continue;
                    
                    // For each rules with atmost this delay
                    for(unsigned int rule_delay=1;rule_delay <= k && rule_delay < programs.at(world).size();rule_delay++)
                    {
                        KProgram & program = programs.at(world).at(rule_delay);
                        
                        // DBG
                        //cout << "(3)Revise program of delay " << rule_delay << endl;
                        
                        learn(input, program, rule_delay, trace, sub_trace_size);
                    }
                }
            }
            
            //DBG
            //cout << "PASS learn" << endl;
            
            // DBG
            //for(unsigned int world=0;world < programs.size();world++)
            //    for(unsigned int delay=1;delay < programs.at(world).size();delay++)
            //        cout << "Delay " << delay << ":" << endl << programs.at(world)[delay].to_string(variables,values,true) << endl;
            
            previous_traces.push_back(trace);
            
            /*// 3: update probabilities 
            //----------------------------
            
            //DBG
            cout << "Update Probabilities" << endl;
            
            for(unsigned int world=0;world < programs.size();world++)
            {
                KProgram & program = programs[world].back();
                update_probabilities(program,trace,input);
                
                // DBG
                cout << "world " << world << ":" << endl;
                cout << program.to_string(variables,values,true) << endl;
            }*/
        }
        
        //DBG
        // Show all worlds
        for(unsigned int world=0;world < programs.size();world++)
        {
            cout << "World " << world << ":" << endl;
            
            KProgram & program = programs.at(world).back();
            
            for(unsigned int variable=0;variable < variables.size();variable++)
            {
                for(unsigned int value=0;value < values[variable].size();value++)
                {
                    list<KRule> const & rules = program.get(variable,value);
                    
                    for(list<KRule>::const_iterator rule=rules.begin();rule != rules.end();rule++)
                    {
                        cout << rule->to_string(input.get_variables(),input.get_variables_values()) << endl;
                    }
                }
            };
        }
        
        // 4) Merging
        //-------------
        KProgram merging = programs[0].back();
        
        // For each possible world
        for(unsigned int world=0;world < programs.size();world++)
        {
            KProgram & program = programs.at(world).back();
            
            for(unsigned int variable=0;variable < variables.size();variable++)
            {
                for(unsigned int value=0;value < values[variable].size();value++)
                {
                    list<KRule> const & rules = program.get(variable,value);
                    
                    for(list<KRule>::const_iterator rule=rules.begin();rule != rules.end();rule++)
                    {
                        if(!merging.subsumes(*rule))
                            merging.add(*rule);
                    }
                }
            };
        }

        // 3: generates all generalizations
        //---------------------------------
        //DBG
        //cout << merging.to_string(variables,values) << endl;

        for(unsigned int variable=0;variable < variables.size();variable++)
        {
            for(unsigned int value=0;value < values[variable].size();value++)
            {
                list<KRule> & rules = merging.get(variable,value);
                for(list<KRule>::iterator rule=rules.begin();rule != rules.end();rule++)
                {
                    list<std::pair<unsigned int, unsigned int> > body = rule->get_body();

                    for(list<std::pair<unsigned int, unsigned int> >::iterator i=body.begin();i != body.end();i++)
                    {
                        KRule generalization(*rule);
                        generalization.remove_condition(i->first);

                        if(!merging.member(generalization))
                            rules.push_back(generalization);
                    }
                }
            }
        }

        // 4: update probabilities
        //----------------------------
        
        //DBG
        //cout << "Update Probabilities" << endl;
        
        for(list<KTrace>::iterator i=previous_traces.begin();i != previous_traces.end();i++)
        {
            update_probabilities(merging,*i,input);
        }
        
        return merging;
    }
    
//-------------------
// Private Functions
//-------------------
    void KLFkT::init(KProgram & program, KInput const & input)
    {   
        // Empty program
        program = KProgram(input.get_variables(),input.get_variables_values());
        
        // Initialize the program with a fact rule for each variable value
        vector<string> const & variables = input.get_variables();
        vector<vector<string> > const & variables_values = input.get_variables_values();
        
        for(unsigned int variable=0;variable < variables.size();variable++)
        {
            for(unsigned int value=0;value < variables_values[variable].size();value++)
            {
                if(value == KRule::NO_VALUE)
                    continue;
                
                KRule rule(variable,value,variables.size());
                program.add(rule);
            }
        }
    }
    
    void KLFkT::init(KProgram & program, KInput const & input, unsigned int delay)
    {   
        // Empty program
        program = KProgram(input.get_variables(),input.get_variables_values());
        
        // Initialize the program with a fact rule for each variable value
        vector<string> const & variables = input.get_variables();
        vector<vector<string> > const & variables_values = input.get_variables_values();
        
        for(unsigned int variable=0;variable < variables.size();variable++)
        {
            for(unsigned int value=0;value < variables_values[variable].size();value++)
            {
                if(value == KRule::NO_VALUE)
                    continue;
                
                KRule rule(variable,value,variables.size()*delay);
                
                // Generate all rule with delay
                for(unsigned int variable=0;variable < variables.size();variable++)
                {
                    for(unsigned int value=0;value < variables_values[variable].size();value++)
                    {
                        if(value != KRule::NO_VALUE)
                        {
                            rule.add_condition(variable+variables.size()*(delay-1),value);
                            program.add(rule);
                            // DBG
                            //cout << "ADD " << rule.to_string(input.get_variables(),input.get_variables_values()) << endl;
                            rule.pop_condition();
                        }
                    }
                }
            }
        }
    }

    void KLFkT::learn(KInput const & input, KProgram & program, unsigned int rule_delay, KTrace const & trace, unsigned int sub_trace_size)
    {
        vector<string> const & variables = input.get_variables();
        vector<vector<string> > const & values = input.get_variables_values();
        
        unsigned int nb_variables = variables.size();
        
        // I is all states before J
        vector<unsigned int> const & J = trace.get(sub_trace_size);
        
        // For each variable...
        for(unsigned int variable=0;variable < J.size();variable++)
        {
            // For each value...
            for(unsigned int value=1;value < values[variable].size();value++)
            {
                if(input.get_system_type() == KInput::ASYNCHRONOUS)
                {
                    // Except the non-observed ones...
                    if(value != J[variable])
                        continue;
                }
                else
                {
                    // Except the observed one...
                    if(value == J[variable])
                        continue;
                }
                
                // Specialize each rule that matches with I...
                list<KRule> & rules = program.get(variable,value);
                unsigned int checked = 0;
                unsigned int max_check = rules.size();
                
                for(list<KRule>::iterator rule=rules.begin();checked < max_check;rule++)
                {
                    checked++;
                    
                    // DBG
                    //cout << "check rule: " << rule->to_string(variables,values) << endl;
                    
                    if(rule->matches(trace,sub_trace_size))
                    {
                        // DBG
                        //cout << "Conflict detected..." <<endl;
                        
                        // 2.2.1) Specialize conflicting rule
                        //------------------------------------
                        
                        // HACK David
                        k_remember.push_back(*rule);

                        KRule conflict_rule(*rule);
                        rules.erase(rule);
                        rule--;
                        
                        // DBG
                        //cout << conflict_rule.to_string(variables,values) << " has to be specialized !" << endl;
                        
                        // By adding the negation of I...
                        unsigned int first_value = (sub_trace_size-rule_delay)*nb_variables;
                        unsigned int last_value = sub_trace_size*nb_variables;
                        for(unsigned int i= first_value;i < last_value;i++)
                        {
                            unsigned int state = i/nb_variables; // Which state in the trace
                            unsigned int trace_value = trace.get(state)[i % nb_variables]; // which value in that state
                            unsigned int rule_variable = (i % nb_variables) + nb_variables*(sub_trace_size-state-1); // which variable in the rule
                            
                            // That are not in the rule...
                            //if(conflict_rule.member(rule_variable,trace_value))
                            if(conflict_rule.get_condition_value(rule_variable) != KRule::NO_VALUE)
                                continue;
                            
                            // DBG
                            //cout << "Can be specialized by adding not " << variables[i % nb_variables] << "(" << values[i % nb_variables][trace_value] << ",T-" << sub_trace_size-state << ")" << endl;
                            
                            // That correspond to all other possible values for the variable...
                            for(unsigned int other_value=0;other_value < values[rule_variable].size();other_value++)
                            {
                                if(other_value != KRule::NO_VALUE && other_value != trace_value)
                                {
                                    KRule revised_rule(conflict_rule);
                                    revised_rule.add_condition(rule_variable,other_value);
                                    
                                    // DBG
                                    //cout << "Specialize into: " << revised_rule.to_string(variables,values) << endl;
                                    
                                    if(program.subsumes(revised_rule))
                                    {
                                        // DBG
                                        //cout << "Subsumed !" << endl;
                                        continue;
                                    }
                                    
                                    // Add the new rule
                                    rules.push_back(revised_rule);
                                }
                            }
                        }
                    }
                }

                // DBG
                //cout << "All rule of " << variables[variable] << "(" << values[variable][value] << ",T) with delay " << rule_delay << " are now consistents." << endl;                             
            }
        }
    }
    
    void KLFkT::add_rule(KRule const & R, KProgram & P, KInput const & input)
    {
        //vector<string> const & variables = input.get_variables();
        //vector<vector<string> > const & values = input.get_variables_values();
        
        //DBG
        //cout << "Adding " << R.to_string(variables,values) << "..." << endl;
        
        //DBG
        //cout << "Check if subsumed..." << endl;
        
        if(P.subsumes(R))
        {
            // DBG
            //cout << "Subsumed" << endl;
            return;
        }
        
        //DBG
        //cout << "Remove subsumed rules..." << endl;
        
        // Remove from P the rules subsumed by R
        list<KRule> & rules = P.get(R.get_head_variable(),R.get_head_value());
        
        for(list<KRule>::iterator i=rules.begin();i != rules.end();i++)
        {
            if(R.subsumes(*i))
            {
                // HACK David
                k_remember.push_back(*i);

                //DBG
                //cout << i->to_string(variables,values)  << " is SUBSUMED" << endl;
                rules.erase(i);
                i--;
            }
        }
        
        //DBG
        //cout << "Check generalization of the rule..." << endl;
        
        // Generalize R by P
        list<KRule> generalizations = P.generalize(R);
        
        //DBG
        //cout << R.to_string(variables,values) << " can be generalized into" << endl << "{";
        
        //for(list<KRule>::const_iterator i=generalizations.begin();i != generalizations.end();i++)
        //    cout << i->to_string(variables,values) << endl;
        //cout << "}" << endl;
        
        if(generalizations.size() > 0)
        {
            // DBG
            //cout << "Add the generalizations...{" << endl;
            
            for(list<KRule>::const_iterator i=generalizations.begin();i != generalizations.end();i++)
                add_rule(*i,P,input);
            
            //DBG
            //cout << "}";
            return;
        }
        
        rules.push_back(R);
        
        //DBG
        //cout << "Revised program: " << endl << P.to_string(variables,values) << endl;
        
        // Generalize P by R
        // TODO ???
    }
    
    void KLFkT::update_probabilities(KProgram & program, KTrace const & trace, KInput const & input)
    {
        vector<string> const & variables = input.get_variables();
        vector<vector<string> > const & values = input.get_variables_values();
        
        // I is all states before J
        vector<unsigned int> const & J = trace.get(1);
        
        for(unsigned int variable=0;variable < variables.size();variable++)
        {
            //DBG
            //cout << "Check rules: " << variable << "="; 
            for(unsigned int value=0;value < values[variable].size();value++)
            {
                if(value == KRule::NO_VALUE)
                    continue;
                
                //DBG
                //cout << value << endl;
                
                list<KRule> & rules = program.get(variable,value);
                
                // Update rules matching counters
                for(list<KRule>::iterator i=rules.begin();i != rules.end();i++)
                {
                    if(i->matches(trace,1))
                    {
                        i->set_matched(i->get_matched()+1);
                    
                        if(i->get_head_value() == J[i->get_head_variable()])
                            i->set_realized(i->get_realized()+1);
                    }
                }
            }
        }
    }
