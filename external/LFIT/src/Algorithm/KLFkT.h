/**
 * @class : KLFkT
 *
 * @description :
 * The LFkT algorithm, learn a logic program from traces of execution.
 * This program capture the dynamics of the markov(k) system that has produced the traces
 *
 * @author : Keijuro
 *
 * @create : 12/11/2014
 *
 * @update : 24/03/2015
 */

#ifndef KLFkT_H
#define KLFkT_H

#include "../Interface/KInput.h"
#include "../Model/KProgram.h"

class KLFkT
{
    // HACK David
    static std::list<KRule> k_remember;

    public :
    
//----------------
// Methods
//----------------
    
    /**
     * @brief learn a logic program that capture input trace using delays
     */
    static KProgram run(KInput & input);
    
    /**
     * @brief learn a set of logic program from input, that represent a non-deterministic model
     */
    static std::vector<KProgram> run_non_deterministic(KInput & input, unsigned int delay=1);
    
    /**
     * @brief learn a set of logic program from input, that represent a probabilistic model
     */
    static KProgram run_probabilistic(KInput & input);
    
//-------------------
// Private Functions
//-------------------
        
    private:
    
    /**
     * @brief initialize the program with fact rules
     */
    static void init(KProgram & program, KInput const & input);
    
    /**
     * @brief initialize the program with minimal rule of corresponding delay
     */
    static void init(KProgram & program, KInput const & input, unsigned int delay);
    
    /**
     * @brief learn the trace
     */
    static void learn(KInput const & input, KProgram & program, unsigned int rule_delay, KTrace const & trace, unsigned int sub_trace_size);
    
    /**
     * @brief add a rule in a program while ensuring its generality
     */
    static void add_rule(KRule const & rule,KProgram & program, KInput const & input);
    
    static void update_probabilities(KProgram & program, KTrace const & trace, KInput const & input);
};

#endif
