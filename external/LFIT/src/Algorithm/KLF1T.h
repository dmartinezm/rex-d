/**
 * @class : KLF1T
 *
 * @description : represents the LF1T algorithm
 *
 * @author : Keijuro
 *
 * @create : 10/11/2013
 *
 * @update : 11/11/2013
 */

#ifndef KLF1T_H
#define KLF1T_H

#include "../Interface/KInput.h"
#include "../Model/KProgram.h"

class KLF1T
{
    public :
    
    // Learning mode
    static const unsigned int GENERALIZATION = 0;
    static const unsigned int SPECIALIZATION = 1;
    
    // HACK David
    static std::list<KRule> k_remember;

//----------------
// Methods
//----------------
    
    public:
    
    /**
     * @brief learn new rules from input by specialization
     */
    static KProgram run_specialization(KInput & input, unsigned int max_conditions);
    
    /**
     * @brief learn new rules from input by generalization
     */
    static KProgram run_generalization(KInput & input);
    
//-------------------
// Private Functions
//-------------------
        
    private:
    
    /**
     * @brief initialize the program for learning by specialization
     */
    static void init_specialization(KProgram & program, KInput const & input);
    
    /**
     * @brief add a rule in a program while ensuring its generality
     */
    static void add_rule(KRule const & rule,KProgram & program, KInput const & input);
    
    static void update_probabilities(KProgram & program, KTrace const & trace, KInput const & input);
};

#endif
