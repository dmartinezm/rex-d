/**
 * @class : KTrace
 *
 * @description : represents a sequence of state transition of a markov(k) system
 *
 * @author : Keijuro
 *
 * @create : 06/11/2013
 *
 * @update : 12/11/2013
 */

#ifndef KTrace_H
#define KTrace_H

#include <string>
#include <vector>

class KTrace
{
    protected :

    /**
     * @brief Sequence of states
     */
    std::vector<std::vector<unsigned int> > k_states;

    /**
     * @brief consistency of the transitions
     **/
    bool k_consistent;
    
    unsigned int k_occurence;

    // Flag
    /**
     * @brief Consistency flag
     */
    bool k_flag_consistency_computed;

    public :

//----------------
// Constructors
//----------------

    /**
     * @brief Construct an empty trace
    */
    KTrace();

    /**
     * @brief Construct a copy of the trace
    */
    KTrace(KTrace const & trace);

    ~KTrace();

//----------------
// Observers
//----------------

    /**
     * @return the number of states
     */
    unsigned int size() const;
    
    /**
     * @return the number of variables per state
     */
    unsigned int nb_variables() const;

    /**
     * @return true if the literal has the corresponding value in the state
     */
    bool member(unsigned int state, unsigned int literal, unsigned int value) const;

    /**
     * @return true if the trace
     */
    bool is_consistent();

    /**
     * @return the minimal delay to consider to explain the trace
     * @return 0 if no delay can explain the trace
     */
    unsigned int delay() const;

//----------------
// Methods
//----------------

    /**
     * @brief add a state at the end of the sequence
     */
    void add(std::vector<unsigned int> const & state);

    /**
     * @brief insert a state in the sequence
     */
    void insert(unsigned int place, std::vector<unsigned int> const & state);

    /**
     * @return the trace as a raw string
     */
    std::string to_string() const;

    /**
     * @return the trace as a string with corresponding value for each literal
     */
    std::string to_string(std::vector<std::string> const & literals, std::vector<std::vector<std::string> > const & values) const;
    
    /**
     * @return the sub-trace as a string with corresponding value for each literal
     */
    std::string to_string(std::vector<std::string> const & literals, std::vector<std::vector<std::string> > const & values, unsigned int size) const;

//----------------
// Accessors
//----------------

    /**
    * @return the i^th state of the sequence
    */
    std::vector<unsigned int> const & get(unsigned int state) const;
    
    /**
     * @return the value of the literal in the corresponding state
     */
    unsigned int get_value(unsigned int state, unsigned int literal) const;
    
    
    /**
     * @return the number of occurence of the trace
     */
    unsigned int get_occurence() const;
//----------------
// Mutator
//----------------

    /**
     * @brief set the i^th state
     */
    void set(unsigned int i, std::vector<unsigned int> const & state);

    /**
     * @brief set the value of the literal in the corresponding state
     */
    void set_value(unsigned int state, unsigned int literal, unsigned int value);
    
    /**
     * @brief set the number of occurence of the trace
     */
    void set_occurence(unsigned int value);
    
//-------------------
// Private Functions
//-------------------
};

#endif
