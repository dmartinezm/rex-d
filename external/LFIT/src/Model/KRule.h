/**
 * @class : KRule
 *
 * @description : represents a transition rule rule of a markov(k) system
 *
 * @author : Keijuro
 *
 * @create : 10/11/2013
 *
 * @update : 24/03/2015
 */

#ifndef KRule_H
#define KRule_H

#include <string>
#include <vector>
#include <list>

#include "KTrace.h"

class KRule
{
    protected :

    /**
     * @brief Head of the rule: a variable and its value
     */
    std::pair<unsigned int, unsigned int> k_head;

    /**
     * @brief Body of the rule as an ordered list of variables that have a value.
     * Optimal for generalization check?
     */
    std::list<std::pair<unsigned int, unsigned int> > k_body_list;

    /**
     * @brief Body of the rule as a vector of value, each cell corresponds to a variable.
     * Optimal for subsumption check
     */
    std::vector<unsigned int> k_body_vector;
    
    /**
     * @brief number of transitions matched
     */
    unsigned int k_matched;
    
    /**
     * @brief number of transitions realized
     */
    unsigned int k_realized;
    
    public:
    
    // Constants
    static const unsigned int NO_VALUE = 0;

//----------------
// Constructors
//----------------

    /**
     * @brief Construct the empty Rule with corresponding head
    */
    KRule(unsigned int variable, unsigned int value, unsigned int nb_variables);

    /**
     * @brief Construct a copy of a rule
    */
    KRule(KRule const & rule);

    ~KRule();

//----------------
// Observers
//----------------

    /**
     * @return the number of variables in the body
     */
    unsigned int size() const;
    
    /**
     * @return true if the condition is in the body
     */
    bool member(unsigned int variable, unsigned int value) const;

    /**
     * @return true if rule is subsumed
     */
    bool subsumes(KRule const & rule) const;
    
    /**
     * @return true if the body match the state
     */
    bool matches(std::vector<unsigned int> const & state) const;
    
    /**
     * @return true if the body match the corresponding sub-trace
     */
    bool matches(KTrace const & trace, unsigned int size) const;

    /**
    * @return l > 0 if the rule is a complementary on l
    */
    int can_generalize(KRule const & rule) const;
    
//----------------
// Methods
//----------------

    /**
    * Add a variable and its value in the body
    */
    void add_condition(unsigned int variable, unsigned int value);

    /**
    * Remove from the body the condition at the corresponding place
    */
    void remove_condition(std::list<std::pair<unsigned int, unsigned int> >::iterator place);
    
    /**
    * Remove from the body the condition on the corresponding variable
    */
    void remove_condition(unsigned int variable);

    /**
     * @brief remove last condition of the body
     */
    void pop_condition();
    
    /**
     * @return the raw string representation of the rule
     */
    std::string to_string() const;
    
    /**
     * @return the string representation of the rule with variable name and value
     */
    std::string to_string(std::vector<std::string> const & variables, std::vector<std::vector<std::string> > const & values, bool ratio=false) const;

//----------------
// Operators
//----------------

    /**
     * @return true if the rules are equivalent
     */
    bool operator==(KRule const & rule) const;

//----------------
// Accessors
//----------------

    /**
     * @brief Construct a copy of a rule
    */
    /**
     * @return the head variable
     */
    unsigned int get_head_variable() const;

    /**
     * @return the value of the head variable
     */
    unsigned int get_head_value() const;
    
    /**
     * @return the value of the variable
     */
    unsigned int get_condition_value(unsigned int variable) const;
    
    /**
     * @return the number of transitions matched
     */
    unsigned int get_matched() const;
    
    /**
     * @return the number of transitions realized
     */
    unsigned int get_realized() const;

    /**
     * @return the rule body
     */
    std::list<std::pair<unsigned int, unsigned int> > const & get_body() const;

//----------------
// Mutators
//----------------

    /**
     * Set the head
     */
    void set_head(unsigned int variable, unsigned int value);
    
    /**
     * @brief the number of transitions matched
     */
    void set_matched(unsigned int value);
    
    /**
     * @brief the number of transitions realized
     */
    void set_realized(unsigned int value);
    
//-------------------
// Private Functions
//-------------------
};

#endif
