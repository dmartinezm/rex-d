#include "KRule.h"

#include <sstream>
#include <iostream>

using namespace std;

//----------------
// Constructors
//----------------

    KRule::KRule(unsigned int variable, unsigned int value, unsigned int nb_variables):
        k_head(variable,value),
        k_body_list(),
        k_body_vector(nb_variables,NO_VALUE),
        k_matched(0),
        k_realized(0)
    {}

    KRule::KRule(KRule const & rule):
        k_head(rule.k_head),
        k_body_list(rule.k_body_list),
        k_body_vector(rule.k_body_vector),
        k_matched(rule.k_matched),
        k_realized(rule.k_realized)
    {}

    KRule::~KRule()
    {}

//----------------
// Observers
//----------------

    unsigned int KRule::size() const
    {
        return k_body_list.size();
    }
    
    bool KRule::member(unsigned int variable, unsigned int value) const
    {
        //DBG
        if(variable > k_body_vector.size())
        {
            cout << "Variable unknown for the rule" << endl;
            return false;
        }
        
        return k_body_vector[variable] == value;
    }

    bool KRule::subsumes(KRule const & R2) const
    {
        if(k_head != R2.k_head)
            return false;
        
        for(list<pair<unsigned int, unsigned int> >::const_iterator i=k_body_list.begin();i != k_body_list.end();i++)
        {
            // If R1 is the current rule
            unsigned int R1_value = i->second;
            unsigned int R2_value = R2.get_condition_value(i->first);
            
            // Different value for a variable
            if( R2_value == NO_VALUE || R1_value != R2_value )
                return false;
        }
        
        return true;
    }
    
    bool KRule::matches(vector<unsigned int> const & state) const
    {
        for(list<pair<unsigned int, unsigned int> >::const_iterator i=k_body_list.begin();i != k_body_list.end();i++)
        {
            unsigned int rule_value = i->second;
            unsigned int state_value = state[i->first];
            
            // Different value for a variable
            if( rule_value != NO_VALUE && state_value != rule_value )
                return false;
        }
        
        return true;
    }
    
    bool KRule::matches(KTrace const & trace, unsigned int size) const
    {
        unsigned int nb_variables = trace.nb_variables();
        
        for(list<pair<unsigned int, unsigned int> >::const_iterator i=k_body_list.begin();i != k_body_list.end();i++)
        {
            // State to witch the condition refer, size is final state
            vector<unsigned int> const & state = trace.get(size-1 - ((i->first)/nb_variables));
            
            unsigned int rule_value = i->second;
            unsigned int state_value = state[i->first % nb_variables];
            
            // Different value for a variable
            if( rule_value != NO_VALUE && state_value != rule_value )
                return false;
        }
        
        return true;
    }

    int KRule::can_generalize(KRule const & R2) const
    {
        int complement = -1;
        
        for(list<pair<unsigned int, unsigned int> >::const_iterator i=k_body_list.begin();i != k_body_list.end();i++)
        {
            // If R1 is the current rule
            unsigned int R1_value = i->second;
            unsigned int R2_value = R2.get_condition_value(i->first);
            
            // R2 is more general
            if(R2_value == NO_VALUE)
                return -1;
            
            // Different value for the variable
            if(R1_value != R2_value)
            {
                // Two differences, no possible resolution
                if(complement != -1)
                    return -1;
                
                complement = i->first;
            }
        }
        
        return complement; 
    }

//----------------
// Methods
//----------------
    
    void KRule::add_condition(unsigned int variable, unsigned int value)
    {
        k_body_vector[variable] = value;
        k_body_list.push_back(pair<unsigned int, unsigned int>(variable,value));
    }

    void KRule::remove_condition(list<pair<unsigned int, unsigned int> >::iterator place)
    {
        k_body_vector[place->first] = NO_VALUE;
        k_body_list.erase(place);
    }
    
    void KRule::remove_condition(unsigned int variable)
    {
        k_body_list.remove(pair<unsigned int, unsigned int>(variable,k_body_vector[variable]));
        k_body_vector[variable] = NO_VALUE;
    }
    
    void KRule::pop_condition()
    {
        k_body_vector[k_body_list.back().first] = NO_VALUE;
        k_body_list.pop_back();
    }

    string KRule::to_string() const
    {
        ostringstream os;
        
        os << k_head.first << "=" << k_head.second;
        
        // DBG
        os << " :- ";
        for(list<pair<unsigned int,unsigned int> >::const_iterator i=k_body_list.begin();i != k_body_list.end();i++)
            os << i->first << "=" << i->second << ", ";
        string s = os.str();
        s = s.substr(0,s.size()-2);
        
        os.str("");
        os << s;
        /*
        if(k_body_list.size() > 0)
        {
            os << " :- ";
            for(unsigned int i=0;i < k_body_vector.size();i++)
            {
                if(k_body_vector[i] != NO_VALUE)
                {
                    os << i << "=" << k_body_vector[i] << ", ";
                }
            }
            
            string s = os.str();
            s = s.substr(0,s.size()-2);
            
            os.str("");
            os << s;
        }*/
        
        os << ".";
        
        return os.str(); 
    }
    
    string KRule::to_string(vector<string> const & variables, vector<vector<string> > const & values, bool ratio) const
    {
        ostringstream os;
        
        os << variables[k_head.first] << "(" << values[k_head.first][k_head.second] << ",T";
        
        if(ratio)
            os << "," << k_realized << "," << k_matched;
        
        os << ")";
        
        if(k_body_list.size() > 0)
        {
            os << " :- ";
            for(unsigned int i=0;i < k_body_vector.size();i++)
            {
                if(k_body_vector[i] != NO_VALUE)
                {
                    os << variables[i%variables.size()] << "(" << values[i%variables.size()][k_body_vector[i]] << ",T-" << i/variables.size()+1 << "), ";
                }
            }
            
            string s = os.str();
            s = s.substr(0,s.size()-2);
            
            os.str("");
            os << s;
        }
        
        os << ".";
        
        // DBG
        //os << " (" << to_string() << ")";
        
        return os.str(); 
    }

//----------------
// Operators
//----------------

    bool KRule::operator==(KRule const & rule) const
    {
        return k_body_vector == rule.k_body_vector;
    }

//----------------
// Accessors
//----------------

    unsigned int KRule::get_head_variable() const
    {
        return k_head.first;
    }

    unsigned int KRule::get_head_value() const
    {
        return k_head.second;
    }
    
    unsigned int KRule::get_condition_value(unsigned int variable) const
    {
        return k_body_vector[variable];
    }
    
    unsigned int KRule::get_matched() const
    {
        return k_matched;
    }
    
    unsigned int KRule::get_realized() const
    {
        return k_realized;
    }

    list<std::pair<unsigned int, unsigned int> > const & KRule::get_body() const
    {
        return k_body_list;
    }

//----------------
// Mutators
//----------------

    void KRule::set_head(unsigned int variable, unsigned int value)
    {
        k_head = pair<unsigned int, unsigned int>(variable,value);
    }
    
    void KRule::set_matched(unsigned int value)
    {
        k_matched = value;
    }
    
    void KRule::set_realized(unsigned int value)
    {
        k_realized = value;
    }

//-------------------
// Private Functions
//-------------------
