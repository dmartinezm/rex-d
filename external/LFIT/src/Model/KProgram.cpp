#include "KProgram.h"

#include <sstream>
#include <iostream>

using namespace std;

//----------------
// Constructors
//----------------

    KProgram::KProgram(string const & name):
    k_name(name),
    k_rules(),
    k_nb_rules(0),
    k_nb_conditions(0)
    {}

    KProgram::KProgram(vector<string> variables, vector<vector<string> > variables_values, string const & name):
        k_name(name),
        k_rules(variables.size(),vector<list<KRule> >()),
        k_nb_rules(0),
        k_nb_conditions(0)
    {
        // Initialize an empty list of rule for each variable value
        for(unsigned int variable=0;variable < variables_values.size();variable++)
            k_rules[variable] = vector<list<KRule> >(variables_values[variable].size(),list<KRule>());
    }

    KProgram::KProgram(KProgram const & program):
        k_name(program.k_name),
        k_rules(program.k_rules),
        k_nb_rules(program.k_nb_rules),
        k_nb_conditions(program.k_nb_conditions)
    {}

    KProgram::~KProgram()
    {}

//----------------
// Observers
//----------------

    unsigned int KProgram::nb_rules() const
    {
        return k_nb_rules;
    }
    
    unsigned int KProgram::nb_conditions() const
    {
        return k_nb_conditions;
    }

    bool KProgram::member(KRule const & rule) const
    {
        list<KRule> const & rules = k_rules[rule.get_head_variable()][rule.get_head_value()];
        
        for(list<KRule>::const_iterator i=rules.begin();i != rules.end();i++)
        {
            if(*i == rule)
                return true;
        }
        
        return false;
    }
    
    bool KProgram::subsumes(KRule const & rule) const
    {
        list<KRule> const & rules = k_rules[rule.get_head_variable()][rule.get_head_value()];
        
        for(list<KRule>::const_iterator i=rules.begin();i != rules.end();i++)
        {
            if(i->subsumes(rule))
                return true;
        }
        
        return false;
    }
    
    list<KRule> KProgram::generalize(KRule const & R) const
    {
        list<KRule> const & rules = get(R.get_head_variable(),R.get_head_value());
        
        // Store the complementary rules found
        vector<vector<bool> > complement_counter(k_rules.size(),vector<bool>());
        // Count the value covered
        vector<unsigned int> value_counter(k_rules.size(),0);
        
        // Initialize counter
        for(unsigned int variable=0;variable < k_rules.size();variable++)
        {
            complement_counter[variable] = vector<bool>(k_rules[variable].size()-1,false);
        }
        
        // Search for complementary rules
        list<KRule> generalizations;
        
        for(list<KRule>::const_iterator P_R=rules.begin();P_R != rules.end();P_R++)
        {
            int complement = P_R->can_generalize(R);
            
            if(complement != -1)
            {
                // DBG
                //cout << P_R->to_string() << " is complement of " << R.to_string() << " on " << complement << endl;
                
                if(complement_counter[complement][P_R->get_condition_value(complement)] == false)
                    value_counter[complement]++;
                complement_counter[complement][P_R->get_condition_value(complement)] = true;
            }
        }
        
        for(unsigned int variable=0;variable < value_counter.size();variable++)
        {
            // DBG
            //cout << "complement of " << variable << ": " << value_counter[variable] << endl;
            
            // All values are complemented for this variable
            if(value_counter[variable] == k_rules[variable].size()-2) // -2: NO_VALUE and R value
            {
                KRule generalization(R);
                generalization.remove_condition(variable);
                generalizations.push_back(generalization);
            }
        }
        
        return generalizations;
    }

//----------------
// Methods
//----------------

    void KProgram::add(KRule const & rule)
    {
       k_rules[rule.get_head_variable()][rule.get_head_value()].push_back(rule); 
       k_nb_rules++;
       k_nb_conditions += rule.size();
    }

    void KProgram::remove(KRule const & rule)
    {
        unsigned int previous_size = k_rules[rule.get_head_variable()][rule.get_head_value()].size();
        
        k_rules[rule.get_head_variable()][rule.get_head_value()].remove(rule);
        
        if(previous_size > k_rules[rule.get_head_variable()][rule.get_head_value()].size())
        {   
            k_nb_rules--;
            k_nb_conditions -= rule.size();
        }
    }

    string KProgram::to_string() const
    {
        ostringstream os;
        
        os << "Program";
        
        if(k_name != "")
            os << k_name;
        
        os << ":" << endl << "{" << endl;
        
        // For each variable...
        for(unsigned int variable=0;variable < k_rules.size();variable++)
        {
            // For each value...
            for(unsigned int value=0;value < k_rules[variable].size();value++)
            {
                // For each rule...
                for(list<KRule>::const_iterator rule=k_rules[variable][value].begin();rule != k_rules[variable][value].end();rule++)
                {
                    os << rule->to_string() << endl;
                }
            }
        }
        
        os << "}";
        
        return os.str();
    }
    
    string KProgram::to_string(vector<std::string> const & variables, vector<vector<string> > const & values, bool ratio) const
    {
        ostringstream os;
        
        //os << "Program";
        
        if(k_name != "")
            os << k_name;
        
        //os << ":" << endl << "{" << endl;
        
        // For each variable...
        for(unsigned int variable=0;variable < k_rules.size();variable++)
        {
            // For each value...
            for(unsigned int value=0;value < k_rules[variable].size();value++)
            {
                // For each rule...
                for(list<KRule>::const_iterator rule=k_rules[variable][value].begin();rule != k_rules[variable][value].end();rule++)
                {
                    // HACK DAVID: Avoid rule of proba 0
                    if(rule->get_realized() == 0)
                        continue;
                    
                    os << rule->to_string(variables,values,ratio) << endl;
                }
            }
        }
        
        //os << "}";
        
        return os.str();
    }

//----------------
// Operators
//----------------

//----------------
// Accessors
//----------------
    
    string KProgram::get_name() const
    {
        return k_name;
    }
    
    list<KRule> & KProgram::get(unsigned int variable, unsigned int value)
    {
        return k_rules[variable][value];
    }
    
    list<KRule> const & KProgram::get(unsigned int variable, unsigned int value) const
    {
        return k_rules[variable][value];
    }

    unsigned int KProgram::get_delay() const
    {
        return k_rules.size();
    }
//----------------
// Mutators
//----------------
    
    void KProgram::set_name(string const & name)
    {
        k_name = name;
    }
    
//-------------------
// Private Functions
//-------------------
