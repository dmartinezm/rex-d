#include "KTrace.h"

#include <sstream>
#include <iostream>
#include <algorithm>

using namespace std;

//----------------
// Constructors
//----------------

    KTrace::KTrace():
        k_states(),
        k_consistent(true),
        k_occurence(1),
        k_flag_consistency_computed(true)
    {}

    KTrace::KTrace(KTrace const & trace):
        k_states(trace.k_states),
        k_consistent(trace.k_consistent),
        k_occurence(trace.k_occurence),
        k_flag_consistency_computed(trace.k_flag_consistency_computed)
    {}

    KTrace::~KTrace()
    {}

//----------------
// Observers
//----------------

    unsigned int KTrace::size() const
    {
        return k_states.size();
    }
    
    unsigned int KTrace::nb_variables() const
    {
        if(k_states.size() == 0)
            return 0;
        
        return k_states[0].size();
    }

    bool KTrace::member(unsigned int state, unsigned int literal, unsigned int value) const
    {
        return k_states[state][literal] == value;
    }

    // TODO: Testé la méthode
    bool KTrace::is_consistent()
    {
        bool conflict = false;

        if(k_flag_consistency_computed)
            return k_consistent;

        for(unsigned int origin=1;origin < k_states.size()-2;origin++)
        {
            vector<unsigned int> const & origin_state = get(origin);

            //DBG
            //cout << "Checking state " << origin << "..." << endl;

            for(unsigned int current=origin+1;current < k_states.size()-1;current++)
            {
                vector<unsigned int> const & current_state = get(current);

                //DBG
                //cout << "Compare with state " << current << "..." << endl;

                // 2.1) Same state found
                if(origin_state == current_state)
                {
                    //cout << "Same state: (" << origin << "," << current << ")" << endl;

                    vector<unsigned int> const & origin_next = get(origin+1);
                    vector<unsigned int> const & current_next = get(current+1);

                    // 2.2) Same next state
                    if(origin_next == current_next)
                    {
                        //DBG
                        //cout << "Same conclusion: OK" << endl;
                        continue;
                    }

                    //DBG
                    //cout << "Not same conclusion: checking prefix..." << endl;

                    // 2.3) Check prefix subsequence
                    unsigned int k;

                    for(k=1;k <= origin;k++)
                    {
                         vector<unsigned int> origin_previous = get(origin-k);
                         vector<unsigned int> current_previous = get(current-k);

                         if(origin_previous != current_previous)
                         {
                             //DBG
                             //cout << "Difference found (" << origin-k << "," << current-k << ")" << ": OK" << endl;
                             break;
                         }
                    }

                    // 2.4) Non deterministic delay
                    if(k > origin)
                    {
                        //DBG
                        //cout << "State " << current << " cannot be explain: UNCONSISTENT" << endl;
                        conflict = true;
                        break;
                    }
                }
            }

            if(conflict)
                break;
        }

        k_consistent = !conflict;
        k_flag_consistency_computed = true;
        return k_consistent;
    }

    // TODO: Testé la méthode
    unsigned int KTrace::delay() const
    {
        unsigned int min_delay = 1;

        for(unsigned int origin=0;origin < k_states.size()-2;origin++)
        {
            vector<unsigned int> const & origin_state = get(origin);

            //DBG
            //cout << "Checking state " << origin << "..." << endl;

            for(unsigned int current=origin+1;current < k_states.size()-1;current++)
            {
                vector<unsigned int> const & current_state = get(current);

                //DBG
                //cout << "Compare with state " << current << "..." << endl;

                // 2.1) Same state found
                if(origin_state == current_state)
                {
                    //cout << "Same state: (" << origin << "," << current << ")" << endl;

                    vector<unsigned int> const & origin_next = get(origin+1);
                    vector<unsigned int> const & current_next = get(current+1);

                    // 2.2) Same next state
                    if(origin_next == current_next)
                    {
                        //DBG
                        //cout << "Same conclusion: OK" << endl;
                        continue;
                    }

                    //DBG
                    //cout << "Not same conclusion: checking prefix..." << endl;

                    // 2.3) Check prefix subsequence
                    unsigned int k;

                    for(k=1;k <= origin;k++)
                    {
                         vector<unsigned int> origin_previous = get(origin-k);
                         vector<unsigned int> current_previous = get(current-k);

                         if(origin_previous != current_previous)
                         {
                             //DBG
                             //cout << "Difference found (" << origin-k << "," << current-k << ")" << ": OK" << endl;
                             if(k+1 > min_delay)
                             {
                                 min_delay = k+1;
                                 // DBG
                                 //cout << "New delay: " << min_delay << endl;
                             }
                             break;
                         }
                    }

                    // 2.4) Non deterministic delay
                    if(k > origin)
                    {
                        //DBG
                        //cout << "State " << current+1 << " needs at least a delay of " << k+1 << " to be explained" << endl;
                        min_delay = k+1;

                        // Update consistency
                        //k_consistent = false;
                        //k_flag_consistency_computed = true;
                        break;
                    }
                }
            }
        }

        return min_delay;
    }

//----------------
// Methods
//----------------

    void KTrace::add(vector<unsigned int> const & state)
    {
        k_states.push_back(state);
        k_flag_consistency_computed = false;
    }

    void KTrace::insert(unsigned int place, std::vector<unsigned int> const & state)
    {
        k_states.insert(k_states.begin()+place,state);
        k_flag_consistency_computed = false;
    }

    // TODO: Testé la méthode
    string KTrace::to_string() const
    {
        ostringstream os;

        os << "{";

        for(unsigned int state=0;state < k_states.size()-1;state++)
        {
            os << "[";
            for(unsigned int literal=0;literal < k_states[state].size()-1;literal++)
            {
                os << k_states[state][literal] << ", ";
            }
            os << k_states[state].back();
            os << "], ";
        }

        // Last state
        if(k_states.size() > 0)
        {
            os << "[";
            for(unsigned int literal=0;literal < k_states[k_states.size()-1].size()-1;literal++)
            {
              os << k_states.back()[literal] << ", ";
            }
            os << k_states.back().back();
            os << "] ";
        }

        os << "}";

        return os.str();
    }

    // TODO: Testé la méthode
    string KTrace::to_string(vector<string> const & literals, vector<vector<string> > const & values) const
    {
        ostringstream os;

        os << "{";

        for(unsigned int state=0;state < k_states.size()-1;state++)
        {
            os << "[";
            for(unsigned int literal=0;literal < k_states[state].size()-1;literal++)
            {
                os << literals[literal] << "=" << values[literal][k_states[state][literal]] << ", ";
            }

            // Last element
            if(k_states.size() > 0)
            {
                unsigned int literal = k_states[state].size()-1;
                unsigned int value = k_states[state][literal];
                os << literals[literal] << "=" << values[literal][value];
            }

            os << "], ";
        }

        // Last state
        if(k_states.size() > 0)
        {
            os << "[";
            for(unsigned int literal=0;literal < k_states[k_states.size()-1].size()-1;literal++)
            {
              os << literals[literal] << "=" << values[literal][k_states[k_states.size()-1][literal]] << ", ";
            }

            // Last element
            if(k_states[k_states.size()-1].size() > 0)
            {
                unsigned int literal = k_states[k_states.size()-1].size()-1;
                unsigned int value = k_states[k_states.size()-1][literal];
                os << literals[literal] << "=" << values[literal][value];
            }
            os << "]";
        }

        os << "}";

        return os.str();
    }

    
    string KTrace::to_string(vector<string> const & literals, vector<vector<string> > const & values, unsigned int size) const
    {
        ostringstream os;
        unsigned int end = min(size+1,(unsigned int)k_states.size());

        os << "{";

        for(unsigned int state=0;state < end;state++)
        {
            os << "[";
            for(unsigned int literal=0;literal < k_states[state].size();literal++)
            {
                os << literals[literal] << "=" << values[literal][k_states[state][literal]] << ", ";
            }
            
            string s = os.str();
            os.str("");

            os << s.substr(0,s.size()-2) << "], ";
        }

        string s = os.str();
        os.str("");

        os << s.substr(0,s.size()-2) << "}";

        return os.str();
    }
//----------------
// Accessors
//----------------

    vector<unsigned int> const & KTrace::get(unsigned int state) const
    {
        return k_states[state];
    }

    unsigned int KTrace::get_value(unsigned int state, unsigned int literal) const
    {
        return k_states[state][literal];
    }
    
    unsigned int KTrace::get_occurence() const
    {
        return k_occurence;
    }

//----------------
// Mutator
//----------------

    void KTrace::set(unsigned int i, std::vector<unsigned int> const & state)
    {
        k_states[i] = state;
        k_flag_consistency_computed = false;
    }

    void KTrace::set_value(unsigned int state, unsigned int literal, unsigned int value)
    {
        k_states[state][literal] = value;
        k_flag_consistency_computed = false;
    }
    
    void KTrace::set_occurence(unsigned int value)
    {
        k_occurence = value;
    }

//-------------------
// Private Functions
//-------------------
