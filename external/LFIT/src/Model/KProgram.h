/**
 * @class : KProgram
 *
 * @description : represents a logic program, a set of rules
 *
 * @author : Keijuro
 *
 * @create : 10/11/2014
 *
 * @update : 29/01/20135
 */

#ifndef KProgram_H
#define KProgram_H

#include "KRule.h"

class KProgram
{
    protected :

    /**
     * @brief Name of the program
     */
    std::string k_name;
    
    /**
     * @brief Rules of the program, rules are first grouped by head variable and then by head values
     */
    std::vector<std::vector<std::list<KRule> > > k_rules;

    // Stats
    unsigned int k_nb_rules;
    unsigned int k_nb_conditions;
    
//----------------
// Constructors
//----------------
    
    public :
    
    /**
     * @brief Construct an empty program without structure
     */
    KProgram(std::string const & name = "");

    /**
     * @brief Construct an empty Program with varaible adapted structure
    */
    KProgram(std::vector<std::string> variables, std::vector<std::vector<std::string> > variables_values, std::string const & name = "");

    /**
     * @brief Construct a copy of a Program
    */
    KProgram(KProgram const & program);

    ~KProgram();

//----------------
// Observers
//----------------

    /**
     * @return the number of rules
     */
    unsigned int nb_rules() const;
    
    /**
     * @return the number of conditions
     */
    unsigned int nb_conditions() const;
    
    /**
     * @return true if the rule is in the program
     */
    bool member(KRule const & rule) const;

    /**
     * @return true if the rule is subsumed by a rule of the program
     */
    bool subsumes(KRule const & rule) const;
    
    /**
     * @return all generalizations of @rule by the rules of the program
     */
    std::list<KRule> generalize(KRule const & rule) const;

//----------------
// Methods
//----------------

    /**
    * Add the rule in the program
    */
    void add(KRule const & rule);

    /**
    * Remove the rule from the program
    */
    void remove(KRule const & rule);

    /**
     * @return the raw string representation of the Program
     */
    std::string to_string() const;
    
    /**
     * @return the string representation of the Program with variable name and value
     */
    std::string to_string(std::vector<std::string> const & variables, std::vector<std::vector<std::string> > const & values, bool ratio=false) const;

//----------------
// Operators
//----------------

//----------------
// Accessors
//----------------
    
    /**
     * @return the name of the program
     */
    std::string get_name() const;
    
    /**
     * @return the rule of with variable=value as head
     */
    std::list<KRule> & get(unsigned int variable, unsigned int value);
    
    /**
     * @return the rule of with variable=value as head
     */
    std::list<KRule> const & get(unsigned int variable, unsigned int value) const;
    
    /**
     * @return the maximum delay consider by the program
     */
    unsigned int get_delay() const;

//----------------
// Mutators
//----------------
    
    void set_name(std::string const & name);
    
//-------------------
// Private Functions
//-------------------
};

#endif
