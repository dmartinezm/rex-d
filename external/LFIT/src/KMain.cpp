/**
 * @Project : LFIT
 *
 * @author : Keijuro
 *
 * @description : Main file of the LFIT Framework
 * Manage user specified parameters
 *
 * @create : 06/11/2014
 *
 * @update : 21/03/2015
 */

#include <iostream>
#include <sstream>
#include <stdlib.h>

#include "Interface/KInput.h"
#include "Algorithm/KLF1T.h"
#include "Algorithm/KLFkT.h"

using namespace std;

/**
 * argv[1] = input file
 */
int main(int argc, char ** argv)
{    
    //---------------------
    // Program arguments
    //---------------------

    ostringstream error_argument;
    error_argument
    << "================================================================================================" << endl
    << "| Missing arguments, please use the following syntax :                                         |" << endl
    << "| ./LFIT [INPUT_FILE] [algorithm] [mode]* [system type]* [delay]*                              |" << endl
    << "| [algorithm] = (0: LF1T, 1: LFkT)                                                             |" << endl
    << "| LF1T [mode] = (0: generalization, 1: specialization)                                         |" << endl
    << "| LFkT [system type] = (0: synchronous, 1: asynchronous, 2: non-deterministic, 3: probilistic) |" << endl
    << "| ex:  ./LFIT file_name.txt 0 1                                                                |" << endl
    << "================================================================================================" << endl;

    if(argc < 3)
    {
        cout << error_argument.str();
        return 0;
    }

    // 1) Arguments setting
    //----------------------

    // Arguments
    string input_file_path;
    unsigned int algorithm;
    unsigned int mode;
    unsigned int system_type;
    unsigned int delay;
    unsigned max_conditions;
    
    // Values
    const unsigned int PARAMETER_ALGORITHM_LF1T = 0;
    const unsigned int PARAMETER_ALGORITHM_LFkT = 1;
    const unsigned int PARAMETER_MODE_LF1T_GENERALIZATION = KLF1T::GENERALIZATION;
    const unsigned int PARAMETER_MODE_LF1T_SPECIALIZATION = KLF1T::SPECIALIZATION;
    const unsigned int PARAMETER_SYSTEM_SYNCHRONOUS = KInput::SYNCHRONOUS;
    const unsigned int PARAMETER_SYSTEM_ASYNCHRONOUS = KInput::ASYNCHRONOUS;
    const unsigned int PARAMETER_SYSTEM_NON_DETERMINISTIC = KInput::NON_DETERMINISTIC;
    const unsigned int PARAMETER_SYSTEM_PROBABILISTIC = KInput::PROBABILISTIC;
    
    // Default
    mode = PARAMETER_MODE_LF1T_GENERALIZATION;
    system_type = PARAMETER_SYSTEM_SYNCHRONOUS;
    delay = 1;
    max_conditions = 0; // all

    // User setting
    input_file_path = argv[1];
    algorithm = atoi(argv[2]);
    
    if(argc > 3)
    {
        if(algorithm == PARAMETER_ALGORITHM_LF1T)
            mode = atoi(argv[3]);
        else
        if(algorithm == PARAMETER_ALGORITHM_LFkT)
        {
            system_type = atoi(argv[3]);
            if(system_type != PARAMETER_SYSTEM_SYNCHRONOUS && system_type != PARAMETER_SYSTEM_ASYNCHRONOUS
                    && system_type != PARAMETER_SYSTEM_NON_DETERMINISTIC && system_type != PARAMETER_SYSTEM_PROBABILISTIC)
            {
                cout << "Error: non-valide system type, should be either 0, 1 or 2 (currently " << system_type <<  ")" << endl;
                return 0;   
            }
        }
        else
        {
            cout << "Error: wrong algorithm parameter, should be either 0 or 1 (currently " << algorithm <<  ")" << endl;
            return 0;
        }
    }
    
    if(argc > 4)
    {
        delay = atoi(argv[4]);
    }

    if(argc > 5)
    {
        max_conditions = atoi(argv[5]);
    }
    
    // 2) Input file extraction
    //--------------------------
    
    KInput input(input_file_path,system_type);
    
    if(input.bad_input())
    {
        cout << "Input file does not exists" << endl;
        return 0;
    }

    // DBG
    //cout << input.to_string();

    // 3) Learning phase
    //-------------------
    
    KProgram output;
    
    // Select algorithm
    switch (algorithm)
    {
        // LF1T
        case PARAMETER_ALGORITHM_LF1T:
        {
            // DBG
            if(system_type != PARAMETER_SYSTEM_SYNCHRONOUS)
            {
                cout << "Error: This kind of system is not suported yet by LF1T" << endl;
                return 0;
            }
        
            switch (mode)
            {
                case PARAMETER_MODE_LF1T_GENERALIZATION:
                {
                    cout << "Running LF1T with generalization..." << endl;
                    cout << "DEBUG VERSION" << endl;
                    //return 0;
                    output = KLF1T::run_generalization(input);
                    break;
                }
                
                case PARAMETER_MODE_LF1T_SPECIALIZATION:
                {
                    //cout << "Running LF1T with specialization..." << endl;
                    //cout << "Experimental version..." << endl;
                    output = KLF1T::run_specialization(input,max_conditions);
                    break;
                }
                
                default:
                {
                    cout << error_argument.str();
                    return 0;
                }
            }
            
            break;
        }
        
        // LFKT
        case PARAMETER_ALGORITHM_LFkT:
        {
            //cout << "Running LFkT..." << endl;
            //cout << "Experimental version..." << endl;
            if(system_type == PARAMETER_SYSTEM_NON_DETERMINISTIC)
            {
                vector<KProgram> programs = KLFkT::run_non_deterministic(input,delay);
                cout << "OUTPUT: " << endl;
                for(unsigned int i=0;i < programs.size();i++)
                {
                    cout << "World " << i << ":" << endl;
                    cout << programs[i].to_string(input.get_variables(),input.get_variables_values()) << endl;
                }
                break;
            }
            else
            if(system_type == PARAMETER_SYSTEM_PROBABILISTIC)
            {
                KProgram program = KLFkT::run_probabilistic(input);
                //cout << "OUTPUT: " << endl;
                cout << program.to_string(input.get_variables(),input.get_variables_values(),true) << endl;
                break;
            }
            
            output = KLFkT::run(input);
            cout << output.to_string(input.get_variables(),input.get_variables_values()) << endl;
            break;
        }
        
        default:
        {
            cout << error_argument.str();
            return 0;
        }
    }

    cout << output.to_string(input.get_variables(),input.get_variables_values(),true);
    //cout << "Mission complete." << endl;

    return 0;
}
