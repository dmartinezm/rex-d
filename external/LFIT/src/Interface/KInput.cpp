#include "KInput.h"

#include <cstdlib>

#include <sstream>
#include <iostream>
#include <iterator>

using namespace std;

//----------------
// Constructors
//----------------

    KInput::KInput(std::string const & file_path, unsigned int system_type):
        k_system_type(system_type),
        k_traces(),
        k_next_trace(0),
        k_file_path(file_path),
        k_line(1),
        k_variables(),
        k_variables_values(),
        k_variables_ignore(),
        k_nb_traces_per_extraction(DEFAULT_NB_TRACES_PER_EXTRACTION),
        k_flag_bad_input(false),
        k_flag_eof_reached(false)
    {
        if(init())
        {
            extract(1000);
        }
        else
            k_flag_bad_input = true;
    }
    
    KInput::~KInput()
    {
        k_file.close();
    }

//----------------
// Observers
//----------------
    
    bool KInput::bad_input() const
    {
        return k_flag_bad_input;
    }

    unsigned int KInput::size() const
    {
        return k_traces.size();
    }
    
    bool KInput::exists(string const & variable) const
    {
        for(unsigned int i=0;i < k_variables.size();i++)
        {
            if(k_variables[i] == variable)
                return true;
        }
        
        return false;
    }
    
    bool KInput::exists(std::string const & variable, std::string const & value) const
    {
        unsigned int id = -1;
        bool found = false;
    
        for(unsigned int i=0;i < k_variables.size();i++)
        {
            // Variable found
            if(k_variables[i] == variable)
            {
                id = i;
                found = true;
                break;
            }
        }
    
        // Variable does not exists
        if(!found)
            return false;
    
        for(unsigned int i=0;i < k_variables_values[id].size();i++)
        {
            // Value found
            if(k_variables_values[id][i] == value)
                return true;
        }
    
        // Value does not exists
        return false;
    }
    
    unsigned int KInput::nb_variables() const
    {
        return k_variables.size();
    }
    
    bool KInput::finish() const
    {
        return (k_flag_eof_reached && k_next_trace == k_traces.size());
    }

    bool KInput::is_ignored(unsigned int variable) const
    {
        return k_variables_ignore[variable];
    }
    
//----------------
// Methods
//----------------

    bool KInput::init()
    {
        k_file.open(k_file_path.c_str());
        string line;
    
        if(!k_file)
        {
            //cout << "File " << k_file_path << " does not exist";
            return false;
        }
    
        // 1) 1) Extract variables
        //------------------------------
    
        do
        {
            streampos current_line = k_file.tellg();
            getline(k_file,line);
    
            //DBG
            //cout << "Check line: " << line << endl;
    
            // construct a stream from the string
            stringstream stream(line);
    
            // use stream iterators to copy the stream to the vector as whitespace separated strings
            istream_iterator<std::string> it(stream), end;
            vector<std::string> tokens(it,end);
    
            if( tokens.size() == 0 || (tokens[0] != "VAR" && tokens[0] != "VAR*") )
            {
                k_file.seekg(current_line);
                break;
            }
            
            if(tokens.size() < 3)
            {
                cerr << "ERROR line " << k_line << " variable declaration expected." << endl
                << "This line should declare a variable values: VAR variable_name [values]" << endl
                << "Example: VAR a 0 1 2" << endl
                << "Example: VAR weather rain sun cloud" << endl;
            }
            
            
            // No rule for this variable
            if(tokens[0] == "VAR*")
                k_variables_ignore.push_back(true);
            else
                k_variables_ignore.push_back(false);
    
            unsigned variable_id = k_variables.size();
            k_variables.push_back(tokens[1]);
            k_variables_values.push_back(vector<string>());
            k_variables_values[variable_id].push_back("*");
    
            for(unsigned int i=2;i < tokens.size();i++)
            {
                //DBG
                //cout << "token: " << tokens[i] << endl;
    
                k_variables_values[variable_id].push_back(tokens[i]);
            }
    
            k_line++;
    
        }while(!k_file.eof());
        
        return true;
    }
    
    
    bool KInput::extract(unsigned int max_traces)
    {
        unsigned int extracted = 0;
        string line;
        
        k_traces.reserve(max_traces);
    
        while(extracted < max_traces && !k_file.eof())
        {
            bool error = false;
    
            getline(k_file,line);
            if(line.length() == 0)
            {
                k_line++;
                continue;
            }
            
            // Observation multiplier
            if(line[0] == '#')
            {
                k_traces.back().set_occurence(atoi(line.substr(1).c_str()));
                k_line++;
                continue;
            }
            
            //DBG
            //cout << "LINE " << line << endl;
    
            // construct a stream from the string
            stringstream stream(line);
    
            // use stream iterators to copy the stream to the vector as whitespace separated strings
            istream_iterator<std::string> it(stream), end;
            vector<std::string> tokens(it, end);
    
            // 1) Extraction of the trace
            //----------------------------
            KTrace trace;
            vector<unsigned int> current_state(k_variables.size(),0);
    
            for(unsigned int i=0;i < tokens.size();i++)
            {
                string token = tokens[i];
    
                //DBG
                //cout << "READING \"" << token << "\"..." << endl;
    
                // 1.1) Transition extraction
                //-----------------------------
                if(tokens[i] == ":")
                {
                    // Error: unspecified variable value
                    for(unsigned int j=0;j < current_state.size();j++)
                    {
                        if(current_state[j] == 0)
                        {
                            cerr << "ERROR line " << k_line << " variable: \"" << k_variables[j] << "\", no specified value for state " << trace.size() << "." << endl;
                            error = true;
                        }
                    }
    
                    trace.add(current_state);
                    current_state = vector<unsigned int>(k_variables.size(),0); // Reset current state
    
                    continue;
                }
    
                // 1.2) Variable value extraction
                //---------------------------------
                size_t delimiter = token.find_first_of(string("="));
    
                if(delimiter == string::npos)
                {
                    cerr << "ERROR line " << k_line << " variable value expected, but \"" << token << "\" found." << endl;
                    continue;
                }
    
                string variable = token.substr(0,delimiter);
                string value = token.substr(delimiter+1);
    
                int variable_id = get_variable_id(variable);
    
                // ERROR: Not declared variable
                if(variable_id == -1)
                {
                    cerr << "ERROR line " << k_line << " unknown variable: \"" << variable << "\", should be declared at the begining, after the keyword VAR." << endl;
                    error = true;
                    continue;
                }
    
                //cout << "ID " << variable_id << endl;
    
                int value_id = get_value_id(variable_id,value);
    
                // ERROR: Not declared value
                if(value_id == -1)
                {
                    cerr << "ERROR line " << k_line << " unknown value: \"" << value << "\", should be declared at the begining, as a value of variable \"" << variable << "\"" << endl;
                    error = true;
                    continue;
                }
    
                // ERROR: Already specified value
                if(current_state[variable_id] != 0)
                {
                    cerr << "ERROR line " << k_line << " variable: \"" << variable << "\", with multiple values: \"" << k_variables_values[variable_id][current_state[variable_id]] << "\" and \"" << value << "\"" << endl;
                    error = true;
                    continue;
                }
    
                // DBG
                //cout << "Extracted variable \"" << variable << "\" (" << variable_id  << "),  value \"" << value << "\" (" << value_id <<  ")." << endl;
    
                // Add variable value to current state
                current_state[variable_id] = value_id;
            }
    
            trace.add(current_state);
    
            if(!error)
            {
                k_traces.push_back(trace);
            }
            else
            {
                cerr << "Line" << k_line << " trace ignored: error found." << endl;
            }
    
            k_line++;
            extracted++;
        }
        
        if(k_file.eof())
        {
            k_flag_eof_reached = true;
        }
    
        return true;
    }
    
    KTrace const & KInput::get_next_trace()
    {
        if(k_next_trace >= k_traces.size())
        {
            k_traces.clear();
            extract(k_nb_traces_per_extraction);
            k_next_trace = 0;
        }
        
        KTrace const &trace = k_traces[k_next_trace];
        k_next_trace++;
        
        return trace;
    }
    
    string KInput::to_string() const
    {
        ostringstream os;
        
        os << "System: ";
        
        if(k_system_type == SYNCHRONOUS)
            os << "Synchronous" << endl;
        if(k_system_type == ASYNCHRONOUS)
            os << "Asynchronous" << endl;
        if(k_system_type == NON_DETERMINISTIC)
            os << "Non-deterministic" << endl;
        
        os << "Variables: ";
        for(unsigned int i=0;i < k_variables.size()-1;i++)
        {
            os << k_variables[i] << ", ";
        }
    
        if(k_variables.size() > 0)
            os << k_variables.back();
        os << endl;
    
        os << "Values: " << endl;
        for(unsigned int i=0;i < k_variables.size();i++)
        {
            os << k_variables[i] << ":";
            for(unsigned int j=0;j < k_variables_values[i].size();j++)
            {
                os << " " << k_variables_values[i][j];
            }
            os << endl;
        }
    
        os << "Traces:" << endl;
        for(unsigned int i=0;i < k_traces.size();i++)
        {
            os << k_traces[i].to_string(k_variables,k_variables_values) << endl;
        }
    
        return os.str();
    }

//----------------
// Accessors
//----------------
    
    unsigned int KInput::get_system_type() const
    {
        return k_system_type;
    }

    int KInput::get_variable_id(std::string const & variable) const
    {
        for(unsigned int i=0;i < k_variables.size();i++)
        {
            if(k_variables[i] == variable)
                return i;
        }
    
        return -1;
    }
    
    int KInput::get_value_id(unsigned int variable, std::string const & value) const
    {
        if(variable > k_variables_values.size())
            return -1;
    
        for(unsigned int i=0;i < k_variables_values[variable].size();i++)
        {
            if(k_variables_values[variable][i] == value)
                return i;
        }
    
        return -1;
    }
    
    string KInput::get_variable_name(unsigned int id) const
    {
        return k_variables[id];
    }
    
    string KInput::get_value_name(unsigned int variable, unsigned int id) const
    {
        return k_variables_values[variable][id];
    }
    
    vector<string> const & KInput::get_variables() const
    {
        return k_variables;
    }
    
    vector<vector<string> > const & KInput::get_variables_values() const
    {
        return k_variables_values;
    }
    
//----------------
// Mutator
//----------------
    
    void KInput::set_nb_traces_per_extraction(unsigned int nb_traces)
    {
        k_nb_traces_per_extraction = nb_traces;
    }

//-------------------
// Private Functions
//-------------------
