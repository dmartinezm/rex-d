/**
 * @class : KInput
 *
 * @description : interface between the input file and the learner
 * - Extract data from the input file
 *
 * @author : Keijuro
 *
 * @create : 06/11/2014
 *
 * @update : 28/01/2014
 */

#ifndef KInput_H
#define KInput_H

#include "../Model/KTrace.h"

#include <string>
#include <vector>
#include <fstream>

class KInput
{
    public: 
    
    static const unsigned int SYNCHRONOUS = 0;
    static const unsigned int ASYNCHRONOUS = 1;
    static const unsigned int NON_DETERMINISTIC = 2;
    static const unsigned int PROBABILISTIC = 3;
    
    protected :
    
    /**
     * @brief Type of the system (Synchronous, asynchronous or completely non-deterministic)
     */
    unsigned int k_system_type;

    /**
     * @brief Last extracted traces
     */
    std::vector<KTrace> k_traces;

    /**
     * @brief Next trace to analyse
     */
    unsigned int k_next_trace;

    /**
    * @brief Input file path
    */
    std::string k_file_path;

    /**
    * @brief Input file stream
    */
    std::fstream k_file;

    /**
    * @brief Current line in the file
    */
    unsigned int k_line;
    
    /**
     * @brief Variables names
     */
    std::vector<std::string> k_variables;

    /**
     * @brief Variables values
     */
    std::vector<std::vector<std::string> > k_variables_values;
    
    /**
     * @brief variables to ignore as rule head
     */
    std::vector<bool> k_variables_ignore;
    
    // Parameters
    unsigned int k_nb_traces_per_extraction;

    // Flags
    bool k_flag_bad_input;
    bool k_flag_eof_reached;

    public :
    
    // Default parameter values
    static const unsigned int DEFAULT_NB_TRACES_PER_EXTRACTION = 1000;

//----------------
// Constructors
//----------------

    /**
     * Construct state transition from an input file
    */
    KInput(std::string const & file_path, unsigned int system_type=SYNCHRONOUS);

    ~KInput();

//----------------
// Observers
//----------------

    /**
     * @return true if the input is not correct
     */
    bool bad_input() const;
    
    /**
     * @return number of transitions
     */
    unsigned int size() const;
    
    /**
    * @return true if the variable exists
    */
    bool exists(std::string const & variable) const;

    /**
    * @return true if the value exists for this variable
    */
    bool exists(std::string const & variable, std::string const & value) const;
    
    /**
    * @return the number of variables
    */
    unsigned int nb_variables() const;
    
    /**
     * @return true if all traces has been returned
     */
    bool finish() const;
    
    /**
     * @return true if the variable is ignored
     */
    bool is_ignored(unsigned int variable) const;

//----------------
// Methods
//----------------

    /**
     * Initialize the connexion to the input
     */
    bool init();

    /**
     * Extract n traces from the input
     */
    bool extract(unsigned int n);
    
    /**
     * @return the next trace in the input
     */
    KTrace const & get_next_trace();

    /**
     * @return the current traces in a string
     */
    std::string to_string() const;

//----------------
// Accessors
//----------------
    
    /**
     * @return return the id of the system type
     */
    unsigned int get_system_type() const;

    /**
    * @return the id of the variable
    */
    int get_variable_id(std::string const & variable) const;

    /**
    * @return the id of the value
    */
    int get_value_id(unsigned int variable, std::string const & value) const;
    
    /**
    * @return the string of the variable
    */
    std::string get_variable_name(unsigned int varaible_id) const;

    /**
    * @return the string of the value
    */
    std::string get_value_name(unsigned int variable, unsigned int varaible_id) const;

    /**
    * @return the string of the variable value
    */
    std::string get_value(unsigned int variable_id, unsigned int value_id) const;

    /**
     * @return the variables
     */
    std::vector<std::string> const & get_variables() const;

    /**
     * @return the variables values
     */
    std::vector<std::vector<std::string> > const & get_variables_values() const;

//----------------
// Mutator
//----------------
    
    /**
     * @brief set the number of traces extracted at once
     */
    void set_nb_traces_per_extraction(unsigned int traces);
    
//-------------------
// Private Functions
//-------------------
};

#endif
