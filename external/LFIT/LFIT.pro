TEMPLATE = app
CONFIG += console
CONFIG += -o2
CONFIG -= qt
DESTDIR = bin
OBJECTS_DIR = build

SOURCES += \
    src/KMain.cpp \
	src/Model/KTrace.cpp \
    src/Interface/KInput.cpp \
    src/Model/KRule.cpp \
    src/Model/KProgram.cpp \
    src/Algorithm/KLF1T.cpp \
    src/Algorithm/KLFkT.cpp

HEADERS += \
	src/Model/KTrace.h \
    src/Interface/KInput.h \
    src/Model/KRule.h \
    src/Model/KProgram.h \
    src/Algorithm/KLF1T.h \
    src/Algorithm/KLFkT.h

