/* This file exists just so boost::shared_ptr variables appear in Doxygen. Not used anywhere in the code.
 * See:
 * http://stackoverflow.com/questions/2356475/getting-shared-ptr-refs-to-appear-in-doxygen-collaboration-diagrams
 */

#include <boost/shared_ptr.hpp>

/**
 * This exists just so boost::shared_ptr variables appear in Doxygen. Not used anywhere in the code.
 */
namespace boost { template<class T> class shared_ptr { T *dummy; }; }
