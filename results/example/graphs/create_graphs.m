close all
disp('*******');
disp('Displaying results...');

generated_times


%% TEACHER

n_iter = size(teacher_exp1,2);

figure('name','Teacher')
y=1:1:n_iter;
hold all
errorbar(teacher_exp1(y),teacher_exp1_E(y))
errorbar(teacher_exp2(y),teacher_exp2_E(y))
errorbar(teacher_exp3(y),teacher_exp3_E(y))
errorbar(teacher_exp4(y),teacher_exp4_E(y))
errorbar(teacher_exp5(y),teacher_exp5_E(y))
legend('exp1', 'exp2', 'exp3', 'exp4', 'exp5');
xlabel('Episode');
ylabel('Teacher requests');

display('Teacher results:');
display_sum_and_std(teacher_exp1_m, y, 'teacher exp1');
display_sum_and_std(teacher_exp2_m, y, 'teacher exp2');
display_sum_and_std(teacher_exp3_m, y, 'teacher exp3');
display_sum_and_std(teacher_exp4_m, y, 'teacher exp4');
display_sum_and_std(teacher_exp5_m, y, 'teacher exp5');


%% ACTIONS

figure('name','Actions')
y=1:1:n_iter;
hold all
errorbar(actions_exp1(y),actions_exp1_E(y))
errorbar(actions_exp2(y),actions_exp2_E(y))
errorbar(actions_exp3(y),actions_exp3_E(y))
errorbar(actions_exp4(y),actions_exp4_E(y))
errorbar(actions_exp5(y),actions_exp5_E(y))
legend('exp1', 'exp2', 'exp3', 'exp4', 'exp5');
xlabel('Episode');
ylabel('Actions');


%% SUCCESS

figure('name','Success')
y=1:1:n_iter;
hold all
plot(y,success_exp1(y))
plot(y,success_exp2(y))
plot(y,success_exp3(y))
plot(y,success_exp4(y))
plot(y,success_exp5(y))
legend('exp1', 'exp2', 'exp3', 'exp4', 'exp5');
xlabel('Episode');
ylabel('Success');


%% REWARD

figure('name','Reward')
y=1:1:n_iter;
hold all
plot(y,reward_exp1(y))
plot(y,reward_exp2(y))
plot(y,reward_exp3(y))
plot(y,reward_exp4(y))
plot(y,reward_exp5(y))
legend('exp1', 'exp2', 'exp3', 'exp4', 'exp5');
xlabel('Episode');
ylabel('Reward');

display('Reward results:');
display_sum_and_std(reward_exp1_m, y, 'reward exp1');
display_sum_and_std(reward_exp2_m, y, 'reward exp2');
display_sum_and_std(reward_exp3_m, y, 'reward exp3');
display_sum_and_std(reward_exp4_m, y, 'reward exp4');
display_sum_and_std(reward_exp5_m, y, 'reward exp5');
