function [] = plotBarStackGroups(stackData, groupLabels,name)
%% Plot a set of stacked bars, but group them according to labels provided.
%%
%% Params: 
%%      stackData is a 3D matrix (i.e., stackData(i, j, k) => (Group, Stack, StackElement)) 
%%      groupLabels is a CELL type (i.e., { 'a', 1 , 20, 'because' };)
%%
%% Copyright 2011 Evan Bollig (bollig at scs DOT fsu ANOTHERDOT edu
%%
%% 
NumGroupsPerAxis = size(stackData, 1);
NumStacksPerGroup = size(stackData, 2);


% Count off the number of bins
groupBins = 1:NumGroupsPerAxis;
MaxGroupWidth = 0.75; % Fraction of 1. If 1, then we have all bars in groups touching
groupOffset = MaxGroupWidth/NumStacksPerGroup;
figure('name',name)
    hold on; 
for i=1:NumStacksPerGroup
    
    Y = squeeze(stackData(:,i,:));
    
    % Center the bars:
    
    internalPosCount = i - ((NumStacksPerGroup+1) / 2);
    
    % Offset the group draw positions:
    groupDrawPos = (internalPosCount)* groupOffset + groupBins;
    
    h(i,:) = bar(Y, 'stacked');
    set(h(i,:),'BarWidth',groupOffset);
    set(h(i,:),'XData',groupDrawPos);
%     if (i==1),
%         set(h(1,3),'facecolor',[1.0 0 0]) % or use RGB triple
%         set(h(1,2),'facecolor',[0.7 0 0]) % or use RGB triple
%         set(h(1,1),'facecolor',[0.4 0 0]) % or use RGB triple
%     end
%     if (i==2),
%         set(h(2,3),'facecolor',[0 1.0 0]) % or use RGB triple
%         set(h(2,2),'facecolor',[0 0.7 0]) % or use RGB triple
%         set(h(2,1),'facecolor',[0 0.4 0]) % or use RGB triple
%     end
%     if (i==3),
%         set(h(3,3),'facecolor',[0 0 1.0]) % or use RGB triple
%         set(h(3,2),'facecolor',[0 0 0.7]) % or use RGB triple
%         set(h(3,1),'facecolor',[0 0 0.4]) % or use RGB triple
%     end
end
y=1:1:NumGroupsPerAxis;
baseline = ones(1,NumGroupsPerAxis)*9;
plot(y,baseline,'w');
hold off;
set(gca,'XTickMode','manual');
set(gca,'XTick',1:NumGroupsPerAxis);
set(gca,'XTickLabelMode','manual');
set(gca,'XTickLabel',groupLabels);
end 
