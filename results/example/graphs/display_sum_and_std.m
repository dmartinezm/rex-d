function display_sum_and_std( array_data, array_idx, text )
%DISPLAY_SUM_AND_STD Summary of this function goes here
%   Detailed explanation goes here

data_avg=sum(array_data(:,array_idx),1)/size(array_data(:,array_idx),1);
data_avg_total = sum(data_avg);
data_avg_std = std(sum(array_data(:,array_idx),2));
str_total_teacher_step1 = sprintf('Total %s: %f +- %f', text, data_avg_total, data_avg_std);
disp(str_total_teacher_step1);

end

